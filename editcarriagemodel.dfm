﻿inherited FormEditCarriageModel: TFormEditCarriageModel
  Caption = #1052#1086#1076#1077#1083#1100' '#1074#1072#1075#1086#1085#1072
  ClientHeight = 182
  ClientWidth = 371
  ExplicitWidth = 377
  ExplicitHeight = 210
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 141
    Width = 371
    TabOrder = 2
    inherited btnCancel: TButton
      Left = 255
    end
    inherited btnOk: TButton
      Left = 136
    end
  end
  object edModel: TDBEditEh [1]
    Left = 11
    Top = 29
    Width = 345
    Height = 24
    ControlLabel.Width = 50
    ControlLabel.Height = 16
    ControlLabel.Caption = #1052#1086#1076#1077#1083#1100
    ControlLabel.Visible = True
    DataField = 'model_code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object neLen: TDBNumberEditEh [2]
    Left = 12
    Top = 88
    Width = 121
    Height = 24
    ControlLabel.Width = 61
    ControlLabel.Height = 16
    ControlLabel.Caption = #1044#1083#1080#1085#1072', '#1084
    ControlLabel.Visible = True
    DataField = 'car_length'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 248
    Top = 80
  end
  inherited qrAux: TADOQuery
    Left = 160
    Top = 80
  end
end
