﻿unit editcounteragent;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, DBCtrlsEh, Data.DB, Data.Win.ADODB, DBSQLLookUp, Vcl.ComCtrls, functions;

type
  TFormEditCounteragent = class(TFormEdit)
    pc: TPageControl;
    TabSheet1: TTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label8: TLabel;
    edINN: TDBEditEh;
    edName: TDBEditEh;
    edLocation: TDBEditEh;
    edChief: TDBEditEh;
    edManager: TDBEditEh;
    edCode: TDBEditEh;
    edEmail: TDBEditEh;
    edContacts: TDBEditEh;
    edContractNumber: TDBEditEh;
    dtContractDate: TDBDateTimeEditEh;
    edELC: TDBEditEh;
    TabSheet2: TTabSheet;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditCounteragent: TFormEditCounteragent;

implementation

{$R *.dfm}

procedure TFormEditCounteragent.btnOkClick(Sender: TObject);
begin
  inherited;

  edCode.Refresh;
  edName.Refresh;

  if (dsLocal.DataSet.FieldByName('code').AsString = '')
     or
     (dsLocal.DataSet.FieldByName('name').AsString = '')
     or
     (dsLocal.DataSet.FieldByName('inn').AsString = '')
  then
  begin
    ShowMessage('Не заполнены обязательные поля!');
    self.ModalResult := mrNone;
  end;

end;

procedure TFormEditCounteragent.FormShow(Sender: TObject);
begin
  self.classif_panel := TabSheet2;
  //MakeClassifInput(TabSheet2, 'counteragents', self.current_id);
end;

end.
