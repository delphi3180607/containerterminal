﻿inherited FormRaportSettings: TFormRaportSettings
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1089#1090#1088#1086#1082' '#1088#1072#1087#1086#1088#1090#1072
  ClientHeight = 608
  ClientWidth = 914
  ExplicitWidth = 930
  ExplicitHeight = 646
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 567
    Width = 914
    ExplicitTop = 567
    ExplicitWidth = 914
    inherited btnOk: TButton
      Left = 679
      ExplicitLeft = 679
    end
    inherited btnCancel: TButton
      Left = 798
      ExplicitLeft = 798
    end
  end
  inherited plAll: TPanel
    Width = 914
    Height = 567
    ExplicitWidth = 914
    ExplicitHeight = 567
    object Splitter1: TSplitter [0]
      Left = 0
      Top = 292
      Width = 914
      Height = 4
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 285
      ExplicitWidth = 820
    end
    inherited plTop: TPanel
      Width = 914
      ExplicitWidth = 914
      inherited btTool: TPngSpeedButton
        Left = 862
        ExplicitLeft = 862
      end
    end
    inherited dgData: TDBGridEh
      Width = 914
      Height = 263
      ReadOnly = False
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'operation_title'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1085#1072#1089#1090#1088#1086#1081#1082#1080
          Width = 491
        end>
    end
    inherited plHint: TPanel
      TabOrder = 3
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object plSpec: TPanel
      Left = 0
      Top = 296
      Width = 914
      Height = 271
      Align = alBottom
      TabOrder = 2
      object Splitter2: TSplitter
        Left = 601
        Top = 1
        Width = 4
        Height = 269
        ExplicitLeft = 401
        ExplicitHeight = 180
      end
      object plLeft: TPanel
        Left = 1
        Top = 1
        Width = 600
        Height = 269
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 600
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object sbDeleteOp: TPngSpeedButton
            AlignWithMargins = True
            Left = 31
            Top = 0
            Width = 32
            Height = 29
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Align = alLeft
            Flat = True
            OnClick = sbDeleteOpClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
              610000001974455874536F6674776172650041646F626520496D616765526561
              647971C9653C000002554944415478DA95924D68D36018C79F7C746D92A6B5DD
              86B2B56C7810A141F0E8459C7810BC09A2CECFA907F13211C7BC7A134410069E
              046FC26EE2556D657EEC24F5D04C2F62B77569BA43ED47DA244BF2C6E74D6CB1
              743BEC8524BCE4FDFD9E27FF278CEFFBB09F65CFDD11A3AF5E767B7B6640B0F0
              680AEF7178FA44DD15BE793BE7BBCE19E2B84571F9F5CAA080C21C57A67BDFB6
              15F6F9B301897DE3560EDF9798E808ECD4B69751F4587EFBE64728F80743340A
              54E7B7DB402C4BE15F2C0512EBFA5C8E61D9129790814F26C1DEAC80A5EBF789
              E32C8582878B319F900E0802CBC83290560B48A3811253C17681C22CC2914412
              ACAD0AD855DD435848E5DF39FD4F20F30F62BEE7198C20702C1EF49A0D70FFD4
              C1470187D248320156658B56F6B0FD782AFFDE1A0AD1BD7B2F465CCF604591E3
              5307003C82667A795899C2B5102E7CB0769F4218164ADC8E3035CDC2C63A4A3C
              80CC24348ADF09711D29FD316FFD7F7E4860CE5EC3C098526C3203502E871DA0
              A0B9B6064EDB50C6BE7E52F7149897AFE600615E94303019613F689F4AAC5A0D
              4C5D879D765B3958FCA60E09BA97AE04954338016655A3104D9B89A5D2AC3471
              08BA551DBA9A86124399F8A9AA7D41F7E26C30675E14118E83A9E9606D53D88D
              6368804F43184D737236031DAD0A1D0C947692F9FD4B0D04C6F90B336C84CF47
              24095CA30326C238BEF8E897952030FDD8711CB16B48E3E31C3D63D089D4EBA7
              B31BE542FF139A67CFA12492775A4D8215250C6B206DEDC8D1E03F199165AE07
              0F85583F796A06E1D5B1D5CF03706F55A60F53C989ECE67A61CF31EE77FD05EB
              B7706A5FA737EA0000000049454E44AE426082}
            ExplicitLeft = 33
            ExplicitHeight = 23
          end
          object sbAddOp: TPngSpeedButton
            AlignWithMargins = True
            Left = 0
            Top = 0
            Width = 31
            Height = 29
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Align = alLeft
            Flat = True
            Layout = blGlyphTop
            ParentShowHint = False
            ShowHint = True
            OnClick = sbAddOpClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
              610000001974455874536F6674776172650041646F626520496D616765526561
              647971C9653C000002BE4944415478DAA5935D4893511CC61FA74E97390D6B9B
              1F516C2E334C2535295A8A792549908124A120D145A6303023233FA2444A08F5
              C6ECA2B430B01681994E4D29511321B134F3634A7EA382B9ED7DA7BEE7BC6FAF
              1B2E28BDA9FFB9389C8BE777FEFF739EC7451004FC4FB9FC09286BBBAEA13C4D
              2394A6134A5484A7E0285910CFB5E25E579EFACCB423A0AC2D378550BE72BF42
              E31FA28A80CC7D3736F80D2C5AE7D033DE8EC5E5A57942487675FA4BC35F0087
              98D64407C77969154761328F62C1360B8EE7E023F581421688AE512386C68719
              B1B38CE7971B0C4EC083D65CB5D87667A4FA648056198E0FF32D6019169C40E0
              E9E901415CEBFC3A42E561E81E69C3C8A4698E2344F73AAB75C20E2835EA0B7C
              7DF615C78724A377A913D60D060CC3202FB6C8DE5DC9E77CD85833246E6E8855
              E950DBF1088C852B6CC8E9B86307DC7D97331675382E785DB201D3F298383707
              8BD58C9284723B20CB7009DEBEEE203CC1216504D6AD0C1A7BDBC69BF59D5A3B
              A0B0E12A1B1F992C1BB10C61C5BA829BC7EF6DFB657A631AE4325F84F945A3AA
              E9B1ED7D6ECF2E3BE0D69B2BEC89F044D9B0791036BA8682A8FB3B02BCA57284
              2B6250F1B6CAF6F1469F039067C81CD31C0C0DE63D04CC58A6C0726B6016CDA8
              BE50EF146E96C44502ADDF11B06616AF7A1AC7BBF3FB1D23E8EBD30BD6246CF1
              D963A9E89A69C11AE520E55D507AE68913C0530152891BE2354978D85881558B
              B5B0EFF657C7235E7B91A6E628ED542AF606C4A84FA3EB47B3FDC1C4AF854037
              1102DC25AE885327C138D0884FDFBFCC899ED1F5177D9B701A29F3E979D148A4
              2650A5F4D28524627A7512B33F2721F03C82F6A811243F80A68106F40E0F32A2
              BD3344F16F236DD5C5EAA414D1EF95D495F8C7859E8252EEBF7939A657A660EC
              6F87D9CACE8BE2EC2DF1B6613A5799A01121699C334C440C13B58789F2A4AEBF
              6878E730FD4BFD027D1196F03509C5820000000049454E44AE426082}
            ExplicitLeft = 1
            ExplicitHeight = 27
          end
          object sbEditOp: TPngSpeedButton
            AlignWithMargins = True
            Left = 63
            Top = 0
            Width = 26
            Height = 29
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Align = alLeft
            Flat = True
            Layout = blGlyphTop
            ParentShowHint = False
            ShowHint = True
            OnClick = sbEditOpClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
              610000001974455874536F6674776172650041646F626520496D616765526561
              647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
              62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
              08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
              4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
              C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
              892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
              4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
              0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
              C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
              8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
              346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
              5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
              3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
              378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
              C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
              1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
              FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
              F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
              0049454E44AE426082}
            ExplicitLeft = 60
            ExplicitHeight = 43
          end
          object Bevel1: TBevel
            AlignWithMargins = True
            Left = 99
            Top = 0
            Width = 1
            Height = 29
            Margins.Left = 10
            Margins.Top = 0
            Margins.Bottom = 0
            Align = alLeft
            ExplicitLeft = 107
            ExplicitTop = 1
            ExplicitHeight = 41
          end
        end
        object dgOperations: TDBGridEh
          Left = 0
          Top = 29
          Width = 600
          Height = 240
          Align = alClient
          ColumnDefValues.Title.TitleButton = True
          ColumnDefValues.ToolTips = True
          Ctl3D = False
          DataSource = dsOperations
          DynProps = <>
          FixedColor = clCream
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          GridLineParams.BrightColor = 7708618
          GridLineParams.ColorScheme = glcsClassicEh
          GridLineParams.DarkColor = 11240555
          GridLineParams.DataBoundaryColor = clNavy
          GridLineParams.DataHorzColor = clSilver
          GridLineParams.DataVertColor = clSilver
          GridLineParams.VertEmptySpaceStyle = dessNonEh
          IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
          IndicatorParams.Color = clCream
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          OptionsEh = [dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          PopupMenu = pmGrid
          RowDetailPanel.Color = clBtnFace
          SearchPanel.Enabled = True
          SearchPanel.PersistentShowing = False
          SelectionDrawParams.SelectionStyle = gsdsClassicEh
          SelectionDrawParams.DrawFocusFrame = False
          SelectionDrawParams.DrawFocusFrameStored = True
          ShowHint = True
          SortLocal = True
          STFilter.Color = clWhite
          STFilter.HorzLineColor = clTeal
          STFilter.InstantApply = True
          STFilter.Local = True
          STFilter.VertLineColor = clMedGray
          STFilter.Visible = True
          TabOrder = 1
          TitleParams.FillStyle = cfstSolidEh
          TitleParams.Font.Charset = DEFAULT_CHARSET
          TitleParams.Font.Color = clWindowText
          TitleParams.Font.Height = -11
          TitleParams.Font.Name = 'Verdana'
          TitleParams.Font.Style = [fsBold]
          TitleParams.HorzLineColor = clGray
          TitleParams.ParentFont = False
          TitleParams.SecondColor = clCream
          TitleParams.VertLineColor = clMedGray
          OnActiveGroupingStructChanged = dgDataActiveGroupingStructChanged
          OnKeyPress = dgOperationsKeyPress
          Columns = <
            item
              AutoDropDown = True
              AutoFitColWidth = False
              CellButtons = <>
              DropDownBox.ListFieldNames = 'id'
              DropDownBox.ListSource = dsDlvTypes
              DynProps = <>
              EditButtons = <>
              FieldName = 'dlv_type_id'
              Footers = <>
              LookupParams.KeyFieldNames = 'dlv_type_id'
              LookupParams.LookupDataSet = meDlvTypes
              LookupParams.LookupDisplayFieldName = 'code'
              LookupParams.LookupKeyFieldNames = 'id'
              Title.Caption = #1058#1080#1087' '#1086#1087#1077#1088#1072#1094#1080#1080'/'#1076#1086#1089#1090#1072#1074#1082#1080
              Width = 176
            end
            item
              AutoDropDown = True
              AutoFitColWidth = False
              CellButtons = <>
              DropDownBox.ListFieldNames = 'id'
              DropDownBox.ListSource = dsStates
              DynProps = <>
              EditButtons = <>
              FieldName = 'object_state_id'
              Footers = <>
              LookupParams.KeyFieldNames = 'object_state_id'
              LookupParams.LookupDataSet = meStates
              LookupParams.LookupDisplayFieldName = 'code'
              LookupParams.LookupKeyFieldNames = 'id'
              Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
              Width = 171
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              Checkboxes = True
              DynProps = <>
              EditButtons = <>
              FieldName = 'isdefective'
              Footers = <>
              Title.Caption = #1053#1077#1080#1089#1087#1088#1072#1074#1077#1085'?'
              Width = 94
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              Checkboxes = True
              DynProps = <>
              EditButtons = <>
              FieldName = 'isempty'
              Footers = <>
              Title.Caption = #1055#1086#1088#1086#1078#1085#1080#1081'?'
              Width = 106
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
      object plRight: TPanel
        Left = 605
        Top = 1
        Width = 308
        Height = 269
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 308
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object PngSpeedButton7: TPngSpeedButton
            AlignWithMargins = True
            Left = 31
            Top = 0
            Width = 32
            Height = 29
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Align = alLeft
            Flat = True
            OnClick = PngSpeedButton7Click
            PngImage.Data = {
              89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
              610000001974455874536F6674776172650041646F626520496D616765526561
              647971C9653C000002554944415478DA95924D68D36018C79F7C746D92A6B5DD
              86B2B56C7810A141F0E8459C7810BC09A2CECFA907F13211C7BC7A134410069E
              046FC26EE2556D657EEC24F5D04C2F62B77569BA43ED47DA244BF2C6E74D6CB1
              743BEC8524BCE4FDFD9E27FF278CEFFBB09F65CFDD11A3AF5E767B7B6640B0F0
              680AEF7178FA44DD15BE793BE7BBCE19E2B84571F9F5CAA080C21C57A67BDFB6
              15F6F9B301897DE3560EDF9798E808ECD4B69751F4587EFBE64728F80743340A
              54E7B7DB402C4BE15F2C0512EBFA5C8E61D9129790814F26C1DEAC80A5EBF789
              E32C8582878B319F900E0802CBC83290560B48A3811253C17681C22CC2914412
              ACAD0AD855DD435848E5DF39FD4F20F30F62BEE7198C20702C1EF49A0D70FFD4
              C1470187D248320156658B56F6B0FD782AFFDE1A0AD1BD7B2F465CCF604591E3
              5307003C82667A795899C2B5102E7CB0769F4218164ADC8E3035CDC2C63A4A3C
              80CC24348ADF09711D29FD316FFD7F7E4860CE5EC3C098526C3203502E871DA0
              A0B9B6064EDB50C6BE7E52F7149897AFE600615E94303019613F689F4AAC5A0D
              4C5D879D765B3958FCA60E09BA97AE04954338016655A3104D9B89A5D2AC3471
              08BA551DBA9A86124399F8A9AA7D41F7E26C30675E14118E83A9E9606D53D88D
              6368804F43184D737236031DAD0A1D0C947692F9FD4B0D04C6F90B336C84CF47
              24095CA30326C238BEF8E897952030FDD8711CB16B48E3E31C3D63D089D4EBA7
              B31BE542FF139A67CFA12492775A4D8215250C6B206DEDC8D1E03F199165AE07
              0F85583F796A06E1D5B1D5CF03706F55A60F53C989ECE67A61CF31EE77FD05EB
              B7706A5FA737EA0000000049454E44AE426082}
            ExplicitLeft = 33
            ExplicitHeight = 23
          end
          object sbAddDet: TPngSpeedButton
            AlignWithMargins = True
            Left = 0
            Top = 0
            Width = 31
            Height = 29
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Align = alLeft
            Flat = True
            Layout = blGlyphTop
            ParentShowHint = False
            ShowHint = True
            OnClick = sbAddDetClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
              610000001974455874536F6674776172650041646F626520496D616765526561
              647971C9653C000002BE4944415478DAA5935D4893511CC61FA74E97390D6B9B
              1F516C2E334C2535295A8A792549908124A120D145A6303023233FA2444A08F5
              C6ECA2B430B01681994E4D29511321B134F3634A7EA382B9ED7DA7BEE7BC6FAF
              1B2E28BDA9FFB9389C8BE777FEFF739EC7451004FC4FB9FC09286BBBAEA13C4D
              2394A6134A5484A7E0285910CFB5E25E579EFACCB423A0AC2D378550BE72BF42
              E31FA28A80CC7D3736F80D2C5AE7D033DE8EC5E5A57942487675FA4BC35F0087
              98D64407C77969154761328F62C1360B8EE7E023F581421688AE512386C68719
              B1B38CE7971B0C4EC083D65CB5D87667A4FA648056198E0FF32D6019169C40E0
              E9E901415CEBFC3A42E561E81E69C3C8A4698E2344F73AAB75C20E2835EA0B7C
              7DF615C78724A377A913D60D060CC3202FB6C8DE5DC9E77CD85833246E6E8855
              E950DBF1088C852B6CC8E9B86307DC7D97331675382E785DB201D3F298383707
              8BD58C9284723B20CB7009DEBEEE203CC1216504D6AD0C1A7BDBC69BF59D5A3B
              A0B0E12A1B1F992C1BB10C61C5BA829BC7EF6DFB657A631AE4325F84F945A3AA
              E9B1ED7D6ECF2E3BE0D69B2BEC89F044D9B0791036BA8682A8FB3B02BCA57284
              2B6250F1B6CAF6F1469F039067C81CD31C0C0DE63D04CC58A6C0726B6016CDA8
              BE50EF146E96C44502ADDF11B06616AF7A1AC7BBF3FB1D23E8EBD30BD6246CF1
              D963A9E89A69C11AE520E55D507AE68913C0530152891BE2354978D85881558B
              B5B0EFF657C7235E7B91A6E628ED542AF606C4A84FA3EB47B3FDC1C4AF854037
              1102DC25AE885327C138D0884FDFBFCC899ED1F5177D9B701A29F3E979D148A4
              2650A5F4D28524627A7512B33F2721F03C82F6A811243F80A68106F40E0F32A2
              BD3344F16F236DD5C5EAA414D1EF95D495F8C7859E8252EEBF7939A657A660EC
              6F87D9CACE8BE2EC2DF1B6613A5799A01121699C334C440C13B58789F2A4AEBF
              6878E730FD4BFD027D1196F03509C5820000000049454E44AE426082}
            ExplicitLeft = 1
            ExplicitHeight = 27
          end
          object PngSpeedButton11: TPngSpeedButton
            AlignWithMargins = True
            Left = 63
            Top = 0
            Width = 26
            Height = 29
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Align = alLeft
            Flat = True
            Layout = blGlyphTop
            ParentShowHint = False
            ShowHint = True
            OnClick = PngSpeedButton11Click
            PngImage.Data = {
              89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
              610000001974455874536F6674776172650041646F626520496D616765526561
              647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
              62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
              08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
              4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
              C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
              892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
              4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
              0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
              C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
              8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
              346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
              5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
              3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
              378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
              C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
              1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
              FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
              F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
              0049454E44AE426082}
            ExplicitLeft = 60
            ExplicitHeight = 43
          end
          object Bevel3: TBevel
            AlignWithMargins = True
            Left = 99
            Top = 0
            Width = 1
            Height = 29
            Margins.Left = 10
            Margins.Top = 0
            Margins.Bottom = 0
            Align = alLeft
            ExplicitLeft = 107
            ExplicitTop = 1
            ExplicitHeight = 41
          end
        end
        object dgDetails: TDBGridEh
          Left = 0
          Top = 29
          Width = 308
          Height = 240
          Align = alClient
          ColumnDefValues.Title.TitleButton = True
          ColumnDefValues.ToolTips = True
          Ctl3D = False
          DataSource = dsDetails
          DynProps = <>
          FixedColor = clCream
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          GridLineParams.BrightColor = 7708618
          GridLineParams.ColorScheme = glcsClassicEh
          GridLineParams.DarkColor = 11240555
          GridLineParams.DataBoundaryColor = clNavy
          GridLineParams.DataHorzColor = clSilver
          GridLineParams.DataVertColor = clSilver
          GridLineParams.VertEmptySpaceStyle = dessNonEh
          IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
          IndicatorParams.Color = clCream
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          OptionsEh = [dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
          ParentCtl3D = False
          ParentFont = False
          ParentShowHint = False
          RowDetailPanel.Color = clBtnFace
          SearchPanel.Enabled = True
          SearchPanel.PersistentShowing = False
          SelectionDrawParams.SelectionStyle = gsdsClassicEh
          SelectionDrawParams.DrawFocusFrame = False
          SelectionDrawParams.DrawFocusFrameStored = True
          ShowHint = True
          SortLocal = True
          STFilter.Color = clWhite
          STFilter.HorzLineColor = clTeal
          STFilter.InstantApply = True
          STFilter.Local = True
          STFilter.VertLineColor = clMedGray
          STFilter.Visible = True
          TabOrder = 1
          TitleParams.FillStyle = cfstSolidEh
          TitleParams.Font.Charset = DEFAULT_CHARSET
          TitleParams.Font.Color = clWindowText
          TitleParams.Font.Height = -11
          TitleParams.Font.Name = 'Verdana'
          TitleParams.Font.Style = [fsBold]
          TitleParams.HorzLineColor = clGray
          TitleParams.MultiTitle = True
          TitleParams.ParentFont = False
          TitleParams.SecondColor = clCream
          TitleParams.VertLineColor = clMedGray
          OnActiveGroupingStructChanged = dgDataActiveGroupingStructChanged
          OnKeyPress = dgDataKeyPress
          Columns = <
            item
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'instruction_code'
              Footers = <>
              Title.Caption = #8470' '#1080#1085#1089#1090#1088#1091#1082#1094#1080#1080
              Width = 91
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'standard_time'
              Footers = <>
              Title.Caption = #1057#1090#1072#1085#1076'. '#1074#1088#1077#1084#1103
              Width = 147
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'operation_code'
              Footers = <>
              Title.Caption = #1050#1086#1076' '#1086#1087#1077#1088#1072#1094#1080#1080
              Width = 79
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'event_code'
              Footers = <>
              Title.Caption = #1052#1077#1089#1090#1086' '#1089#1086#1073#1099#1090#1080#1103
              Width = 92
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'depart_code'
              Footers = <>
              Title.Caption = #1052#1077#1089#1090#1086' '#1086#1090#1087#1088#1072#1074#1083#1077#1085#1080#1103
              Width = 95
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'dest_code'
              Footers = <>
              Title.Caption = #1052#1077#1089#1090#1086' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1103
              Width = 96
            end
            item
              AutoFitColWidth = False
              CellButtons = <>
              DynProps = <>
              EditButtons = <>
              FieldName = 'transp_code'
              Footers = <>
              Title.Caption = #1058#1080#1087' '#1090#1088#1072#1085#1089#1087#1086#1088#1090#1072
              Width = 97
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from raport_settings_header order by operation_title')
    UpdateCommand.CommandText.Strings = (
      'update raport_settings_header'
      'set'
      '  operation_title = :operation_title'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'operation_title'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into raport_settings_header'
      '  (operation_title)'
      'values'
      '  (:operation_title)')
    InsertCommand.Parameters = <
      item
        Name = 'operation_title'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from raport_settings_header where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from raport_settings_header where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited meData: TMemTableEh
    AfterOpen = meDataAfterScroll
    AfterScroll = meDataAfterScroll
  end
  object drvOperations: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select * from raport_settings_operations '
      'where head_id = :head_id'
      'order by id')
    SelectCommand.Parameters = <
      item
        Name = 'head_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update raport_settings_operations'
      'set'
      '  head_id = :head_id,'
      '  dlv_type_id = :dlv_type_id,'
      '  object_state_id = :object_state_id,'
      '  isdefective = :isdefective, '
      '  isempty = :isempty'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'head_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'object_state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isdefective'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into raport_settings_operations'
      '  (head_id, dlv_type_id, object_state_id, isdefective, isempty)'
      'values'
      
        '  (:head_id, :dlv_type_id, :object_state_id, :isdefective, :isem' +
        'pty)')
    InsertCommand.Parameters = <
      item
        Name = 'head_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'object_state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isdefective'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from raport_settings_operations where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from raport_settings_operations where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 88
    Top = 480
  end
  object dsOperations: TDataSource
    DataSet = meOperations
    Left = 192
    Top = 480
  end
  object meOperations: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvOperations
    BeforePost = meOperationsBeforePost
    AfterPost = meOperationsAfterPost
    Left = 136
    Top = 480
  end
  object drvDetails: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select * from raport_settings_details '
      'where head_id = :head_id'
      'order by standard_time')
    SelectCommand.Parameters = <
      item
        Name = 'head_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update raport_settings_details'
      'set'
      '  head_id = :head_id,'
      '  standard_time = :standard_time,'
      '  operation_code = :operation_code,'
      '  event_code = :event_code,'
      '  depart_code = :depart_code,'
      '  dest_code = :dest_code,'
      '  instruction_code = :instruction_code,'
      '  transp_code = :transp_code'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'head_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'standard_time'
        Size = -1
        Value = Null
      end
      item
        Name = 'operation_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'event_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'depart_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'dest_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'instruction_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'transp_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into raport_settings_details'
      
        '  (head_id, standard_time, operation_code, event_code, depart_co' +
        'de, dest_code, instruction_code, transp_code)'
      'values'
      
        '  (:head_id, :standard_time, :operation_code, :event_code, :depa' +
        'rt_code, :instruction_code, :dest_code, :transp_code)')
    InsertCommand.Parameters = <
      item
        Name = 'head_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'standard_time'
        Size = -1
        Value = Null
      end
      item
        Name = 'operation_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'event_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'depart_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'instruction_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'dest_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'transp_code'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from raport_settings_details where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from raport_settings_details '
      'where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 504
    Top = 488
  end
  object dsDetails: TDataSource
    DataSet = meDetails
    Left = 608
    Top = 488
  end
  object meDetails: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvDetails
    BeforePost = meDetailsBeforePost
    AfterPost = meDetailsAfterPost
    Left = 552
    Top = 488
  end
  object drvStates: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select * from objectstatekinds order by code')
    SelectCommand.Parameters = <>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 96
    Top = 416
  end
  object meStates: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvStates
    Left = 144
    Top = 416
  end
  object drvDlvTypes: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select * from deliverytypes order by code')
    SelectCommand.Parameters = <>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 96
    Top = 368
  end
  object meDlvTypes: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvDlvTypes
    Left = 144
    Top = 368
  end
  object dsStates: TDataSource
    DataSet = meStates
    Left = 200
    Top = 416
  end
  object dsDlvTypes: TDataSource
    DataSet = meDlvTypes
    Left = 200
    Top = 368
  end
end
