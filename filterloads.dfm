﻿inherited FormFilterLoads: TFormFilterLoads
  Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1087#1086#1075#1088#1091#1079#1082#1072#1084
  ClientHeight = 182
  ClientWidth = 663
  ExplicitWidth = 669
  ExplicitHeight = 210
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 12
    Top = 12
    Width = 80
    Height = 16
    Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103
  end
  object Label2: TLabel [1]
    Left = 12
    Top = 74
    Width = 280
    Height = 16
    Caption = #1055#1083#1072#1085' '#1087#1086#1075#1088#1091#1079#1082#1080' ('#1084#1086#1078#1085#1086' '#1086#1089#1090#1072#1074#1080#1090#1100' '#1087#1091#1089#1090#1099#1084')'
  end
  inherited plBottom: TPanel
    Top = 141
    Width = 663
    ExplicitTop = 130
    ExplicitWidth = 663
    inherited btnCancel: TButton
      Left = 547
      ExplicitLeft = 547
    end
    inherited btnOk: TButton
      Left = 428
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      ExplicitLeft = 428
    end
    object Button1: TButton
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 289
      Height = 35
      Align = alLeft
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100' '#1092#1080#1083#1100#1090#1088#1072#1094#1080#1102' '#1087#1086' '#1087#1086#1075#1088#1091#1079#1082#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = []
      ModalResult = 6
      ParentFont = False
      TabOrder = 2
    end
  end
  object dtPlanDate: TDBDateTimeEditEh [3]
    Left = 12
    Top = 34
    Width = 177
    Height = 24
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
    OnChange = dtPlanDateChange
    EditFormat = 'DD/MM/YYYY'
  end
  object luLoadPlan: TDBSQLLookUp [4]
    Left = 8
    Top = 94
    Width = 641
    Height = 22
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
    SqlSet = ssLoadPlan
    OnKeyValueChange = luLoadPlanKeyValueChange
  end
  inherited dsLocal: TDataSource
    Left = 248
    Top = 24
  end
  inherited qrAux: TADOQuery
    Left = 208
    Top = 24
  end
  object ssLoadPlan: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select top 100 '
      
        'p.id, convert(varchar, p.plan_date, 104)+'#39', '#39'+isnull(p.train_num' +
        ','#39#39')+'#39', '#39'+'
      
        'isnull((select code from dispatchkinds k where k.id = p.dispatch' +
        '_kind_id),'#39#39')+'#39','#39'+isnull(p.note,'#39#39')'
      'as load_plan_text'
      'from loadplan p'
      'where system_section = :system_section'
      'order by plan_date desc'
      '')
    DownSql.Strings = (
      'select top 100 '
      'p.id, '
      
        'convert(varchar, p.plan_date, 104)+'#39', '#39'+isnull(p.train_num,'#39#39')+'#39 +
        ', '#39'+'
      
        'isnull((select code from dispatchkinds k where k.id = p.dispatch' +
        '_kind_id),'#39#39')+'#39','#39'+isnull(p.note,'#39#39')'
      'as load_plan_text'
      'from loadplan p'
      'where system_section = :system_section'
      'order by plan_date desc'
      '')
    InitSql.Strings = (
      
        'select p.id, convert(varchar, p.plan_date, 104)+'#39', '#39'+isnull(p.tr' +
        'ain_num,'#39#39')+'#39', '#39'+'
      
        'isnull((select code from dispatchkinds k where k.id = p.dispatch' +
        '_kind_id),'#39#39')+'#39','#39'+isnull(p.note,'#39#39')'
      'as load_plan_text'
      'from loadplan p'
      'where p.id = @id'
      '')
    KeyName = 'id'
    DisplayName = 'load_plan_text'
    Connection = dm.connMain
    Left = 305
    Top = 26
  end
end
