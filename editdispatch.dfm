﻿inherited FormEditDispatch: TFormEditDispatch
  Caption = #1057#1100#1077#1084#1082#1072'/'#1086#1090#1087#1088#1072#1074#1082#1072
  ClientHeight = 306
  ClientWidth = 504
  ExplicitWidth = 510
  ExplicitHeight = 334
  PixelsPerInch = 96
  TextHeight = 16
  object Label8: TLabel [0]
    Left = 8
    Top = 70
    Width = 92
    Height = 16
    Caption = #1042#1080#1076' '#1086#1090#1087#1088#1072#1074#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel [1]
    Left = 167
    Top = 8
    Width = 110
    Height = 16
    Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object Label5: TLabel [2]
    Left = 8
    Top = 200
    Width = 127
    Height = 16
    Caption = #1044#1072#1090#1072' '#1091#1074#1077#1076#1086#1084#1083#1077#1085#1080#1103
  end
  object Label1: TLabel [3]
    Left = 8
    Top = 8
    Width = 119
    Height = 16
    Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object Label2: TLabel [4]
    Left = 301
    Top = 70
    Width = 94
    Height = 16
    Caption = #1053#1086#1084#1077#1088' '#1087#1086#1077#1079#1076#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel [5]
    Left = 182
    Top = 200
    Width = 86
    Height = 16
    Caption = #1044#1072#1090#1072' '#1089#1100#1077#1084#1082#1080
  end
  object Label10: TLabel [6]
    Left = 348
    Top = 200
    Width = 124
    Height = 16
    Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100' '#1091#1073#1086#1088#1082#1080
  end
  object Label6: TLabel [7]
    Left = 8
    Top = 137
    Width = 141
    Height = 16
    Caption = #1057#1090#1072#1085#1094#1080#1103' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1103
    Visible = False
  end
  object sbStations: TSpeedButton [8]
    Left = 391
    Top = 154
    Width = 52
    Height = 24
    Caption = '...'
    Visible = False
    OnClick = sbStationsClick
  end
  inherited plBottom: TPanel
    Top = 265
    Width = 504
    TabOrder = 8
    ExplicitTop = 265
    ExplicitWidth = 504
    inherited btnCancel: TButton
      Left = 388
      ExplicitLeft = 388
    end
    inherited btnOk: TButton
      Left = 269
      ExplicitLeft = 269
    end
  end
  object luDispatchKind: TDBLookupComboboxEh [10]
    Left = 8
    Top = 90
    Width = 280
    Height = 24
    DynProps = <>
    DataField = 'dispatchkind_id'
    DataSource = dsLocal
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    KeyField = 'id'
    ListField = 'code'
    ListSource = FormDispatchKinds.dsData
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object dtDocDate: TDBDateTimeEditEh [11]
    Left = 167
    Top = 30
    Width = 121
    Height = 24
    DataField = 'doc_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object dtNotifDate: TDBDateTimeEditEh [12]
    Left = 8
    Top = 218
    Width = 162
    Height = 24
    DataField = 'date_notification'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    Visible = True
    EditFormat = 'DD/MM/YYYY HH:NN:SS'
  end
  object edDocNumber: TDBEditEh [13]
    Left = 8
    Top = 30
    Width = 147
    Height = 24
    DataField = 'doc_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edTrainNumber: TDBEditEh [14]
    Left = 301
    Top = 90
    Width = 193
    Height = 24
    Color = clCream
    DataField = 'train_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object dtRemoveDate: TDBDateTimeEditEh [15]
    Left = 182
    Top = 218
    Width = 155
    Height = 24
    DataField = 'date_remove'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    Visible = True
    EditFormat = 'DD/MM/YYYY HH:NN:SS'
  end
  object edRemoveSheet: TDBEditEh [16]
    Left = 348
    Top = 218
    Width = 147
    Height = 24
    DataField = 'sheet_remove'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    Visible = True
  end
  object laStation: TDBSQLLookUp [17]
    Left = 8
    Top = 155
    Width = 377
    Height = 22
    DataField = 'station_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = False
    SqlSet = ssStations
  end
  inherited dsLocal: TDataSource
    Left = 328
    Top = 336
  end
  inherited qrAux: TADOQuery
    Left = 400
    Top = 336
  end
  object ssStations: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select * from stations where name like '#39'%@pattern%'#39' or code like' +
        ' '#39'%pattern%'#39' order by name')
    DownSql.Strings = (
      'select top 100 * from stations order by code')
    InitSql.Strings = (
      'select * from stations where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 172
    Top = 140
  end
end
