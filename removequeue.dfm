﻿inherited FormRemoveQueue: TFormRemoveQueue
  BorderStyle = bsDialog
  Caption = #1054#1095#1077#1088#1077#1076#1100' '#1074#1099#1074#1086#1079#1072
  ClientHeight = 521
  ClientWidth = 830
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 480
    Width = 830
    ExplicitTop = 480
    ExplicitWidth = 830
    inherited btnOk: TButton
      Left = 595
      Enabled = False
      Visible = False
      ExplicitLeft = 595
    end
    inherited btnCancel: TButton
      Left = 714
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ExplicitLeft = 714
    end
  end
  inherited plAll: TPanel
    Left = 291
    Width = 539
    Height = 480
    ExplicitLeft = 291
    ExplicitWidth = 539
    ExplicitHeight = 480
    inherited plTop: TPanel
      Width = 539
      ExplicitWidth = 539
      inherited btTool: TPngSpeedButton
        Left = 501
        ExplicitLeft = 501
      end
      inherited plCount: TPanel
        Left = 304
        ExplicitLeft = 304
      end
    end
    inherited dgData: TDBGridEh
      Width = 539
      Height = 451
      BorderStyle = bsSingle
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  object plLeft: TPanel [2]
    Left = 0
    Top = 0
    Width = 291
    Height = 480
    Align = alLeft
    Caption = 'plLeft'
    TabOrder = 2
  end
end
