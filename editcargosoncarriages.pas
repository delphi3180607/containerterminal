﻿unit editcargosoncarriages;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, DBCtrlsEh, DBSQLLookUp, Data.DB, Vcl.Buttons, functions, DBGridEh,
  DBLookupEh, Data.Win.ADODB, MemTableDataEh, MemTableEh, PngSpeedButton;

type
  TFormEditCargosOnCarriages = class(TFormEdit)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    sbContainer: TSpeedButton;
    sbCarriage: TSpeedButton;
    edSealNumber: TDBEditEh;
    Label4: TLabel;
    nuWeightSender: TDBNumberEditEh;
    Label5: TLabel;
    nuWeightDocument: TDBNumberEditEh;
    Label6: TLabel;
    nuWeightFact: TDBNumberEditEh;
    Label7: TLabel;
    Label8: TLabel;
    sbForwarder: TSpeedButton;
    Label9: TLabel;
    sbConsignee: TSpeedButton;
    edContainerNum: TDBEditEh;
    edCarriageNum: TDBEditEh;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    sbPayer: TSpeedButton;
    edTrainNumber: TDBEditEh;
    Label15: TLabel;
    Label18: TLabel;
    meText: TDBMemoEh;
    cbGuard: TDBCheckBoxEh;
    cbReturnCont: TDBCheckBoxEh;
    Label19: TLabel;
    edContFut: TDBEditEh;
    Label20: TLabel;
    edCarType: TDBEditEh;
    cbIsEmpty: TDBCheckBoxEh;
    sbCargoType: TSpeedButton;
    edDocNumber: TDBEditEh;
    Label21: TLabel;
    dtDocDate: TDBDateTimeEditEh;
    Label22: TLabel;
    laForwarder: TDBSQLLookUp;
    laConsignee: TDBSQLLookUp;
    laPayer: TDBSQLLookUp;
    laContainerOwner: TDBSQLLookUp;
    laCarriageOwner: TDBSQLLookUp;
    laCargoType: TDBSQLLookUp;
    ssDlvTypes: TADOLookUpSqlSet;
    laDlvTypes: TDBSQLLookUp;
    Label16: TLabel;
    sbCopy: TPngSpeedButton;
    leContainerKind: TDBSQLLookUp;
    GroupBox1: TGroupBox;
    dtPlugStart: TDBDateTimeEditEh;
    dtPlugEnd: TDBDateTimeEditEh;
    procedure sbContainerClick(Sender: TObject);
    procedure sbCarriageClick(Sender: TObject);
    procedure sbForwarderClick(Sender: TObject);
    procedure sbConsigneeClick(Sender: TObject);
    procedure sbPayerClick(Sender: TObject);
    procedure luCargoTypeButtonDown(Sender: TObject; TopButton: Boolean;
      var AutoRepeat, Handled: Boolean);
    procedure lyDeliveryTypeButtonDown(Sender: TObject; TopButton: Boolean;
      var AutoRepeat, Handled: Boolean);
    procedure btnOkClick(Sender: TObject);
    procedure sbCargoTypeClick(Sender: TObject);
    procedure cbIsEmptyClick(Sender: TObject);
    procedure edContainerNumKeyPress(Sender: TObject; var Key: Char);
    procedure sbCopyClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditCargosOnCarriages: TFormEditCargosOnCarriages;

implementation

{$R *.dfm}

uses counteragents, trains, containers, Contracts, carriages, cargotypes, deliverytypes;

procedure TFormEditCargosOnCarriages.btnOkClick(Sender: TObject);
begin

{  if (dsLocal.DataSet.FieldByName('forwarder_id').AsString = '') or
  (dsLocal.DataSet.FieldByName('consignee_id').AsString = '') or
  (dsLocal.DataSet.FieldByName('payer_id').AsString = '') or
  (dsLocal.DataSet.FieldByName('dlv_type_id').AsString = '') or
  (dsLocal.DataSet.FieldByName('container_num').AsString = '') or
  (dsLocal.DataSet.FieldByName('carriage_num').AsString = '') or
  (dsLocal.DataSet.FieldByName('container_owner_id').AsString = '') or
  (dsLocal.DataSet.FieldByName('carriage_owner_id').AsString = '') or
  (dsLocal.DataSet.FieldByName('weight_doc').AsString = '') or
  ((not self.cbIsEmpty.Checked) and (dsLocal.DataSet.FieldByName('cargotype_id').AsString = ''))
  then
  begin
    ShowMessage('Не заполнены обязательные поля!');
    self.ModalResult := mrNone;
  end;}

end;

procedure TFormEditCargosOnCarriages.cbIsEmptyClick(Sender: TObject);
begin
  if not self.Visible then exit;
  if cbIsEmpty.Checked then self.laCargoType.Value := null;
  self.laCargoType.Enabled := not cbIsEmpty.Checked;
  self.sbCargoType.Enabled := not cbIsEmpty.Checked;
  TMemTableEh(dsLocal.DataSet).FieldByName('cargotype_id').Required := not cbIsEmpty.Checked;
  self.laCargoType.Refresh;
end;

procedure TFormEditCargosOnCarriages.edContainerNumKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Ord(Key)<32 then exit;
  if not (Key in [#8,'0'..'9','A'..'Z','a'..'z']) then Key := #0;
end;

procedure TFormEditCargosOnCarriages.luCargoTypeButtonDown(Sender: TObject; TopButton: Boolean; var AutoRepeat, Handled: Boolean);
begin
  FormCargoTypes.meData.Open;
end;

procedure TFormEditCargosOnCarriages.lyDeliveryTypeButtonDown(Sender: TObject;
  TopButton: Boolean; var AutoRepeat, Handled: Boolean);
begin
  FormDeliveryTypes.meData.Open;
end;

procedure TFormEditCargosOnCarriages.sbCargoTypeClick(Sender: TObject);
begin
  SFDE(TFormCargotypes,self.laCargoType);
end;

procedure TFormEditCargosOnCarriages.sbCarriageClick(Sender: TObject);
begin
  SFDE(TFormCounteragents,self.laCarriageOwner);
end;

procedure TFormEditCargosOnCarriages.sbConsigneeClick(Sender: TObject);
begin
  SFDE(TFormCounteragents,self.laConsignee);
end;

procedure TFormEditCargosOnCarriages.sbContainerClick(Sender: TObject);
begin
  SFDE(TFormCounteragents,self.laContainerOwner);
end;

procedure TFormEditCargosOnCarriages.sbCopyClick(Sender: TObject);
begin
  laPayer.KeyValue := laConsignee.KeyValue;
  laPayer.Refresh;
end;

procedure TFormEditCargosOnCarriages.sbForwarderClick(Sender: TObject);
begin
  SFDE(TFormCounteragents,self.laForwarder);
end;

procedure TFormEditCargosOnCarriages.sbPayerClick(Sender: TObject);
begin
  SFDE(TFormCounteragents,self.laPayer);
end;

end.
