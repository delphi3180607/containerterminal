﻿inherited FormContainersOnGo: TFormContainersOnGo
  ActiveControl = dgData
  Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088#1099' '#1074' '#1088#1072#1073#1086#1090#1077
  ClientHeight = 585
  ClientWidth = 1084
  ExplicitWidth = 1100
  ExplicitHeight = 623
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 544
    Width = 1084
    ExplicitTop = 544
    ExplicitWidth = 1084
    inherited btnOk: TButton
      Left = 849
      Caption = #1042#1099#1073#1088#1072#1090#1100
      Font.Style = []
      ExplicitLeft = 849
    end
    inherited btnCancel: TButton
      Left = 968
      ExplicitLeft = 968
    end
  end
  inherited plAll: TPanel
    Width = 1084
    Height = 544
    ExplicitWidth = 1084
    ExplicitHeight = 544
    inherited plTop: TPanel
      Width = 1084
      ExplicitWidth = 1084
      inherited sbDelete: TPngSpeedButton
        Left = 252
        ExplicitLeft = 31
      end
      inherited btFilter: TPngSpeedButton
        Left = 361
        OnClick = btFilterClick
        ExplicitLeft = 147
        ExplicitHeight = 29
      end
      inherited btExcel: TPngSpeedButton
        Left = 327
        ExplicitLeft = 103
        ExplicitHeight = 29
      end
      inherited sbAdd: TPngSpeedButton
        Left = 218
        ExplicitLeft = 218
      end
      inherited sbEdit: TPngSpeedButton
        Left = 286
        ExplicitLeft = 63
      end
      inherited Bevel2: TBevel
        Left = 323
        ExplicitLeft = 99
      end
      inherited btTool: TPngSpeedButton
        Left = 1046
        ExplicitLeft = 823
      end
      object sbClearFilter: TPngSpeedButton [7]
        AlignWithMargins = True
        Left = 395
        Top = 0
        Width = 34
        Height = 29
        Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1080#1083#1100#1090#1088
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbClearFilterClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000002334944415478DA63FCFFFF3F032580
          91A1FD0A7E13FEA3A8C66A8005903E7E26499541969785285B1F7FFEC36032EF
          3688690931136AC8C9445506691EA021606F01A518FF33349F7C0F56526326C8
          C008147AFAE50F83F9BC3B20694B864A9D130847810CF9FBF7F8E1247506492E
          16B873BBCF400C2833166478F6ED0F83EDBC9B0C0CCCCC60CD98BE0219F2F3C7
          F13D49DA0CE2DC2C60C9C6A3AFC02E4A3714667059709D8181950DAE197BB080
          0CF9F2F9F896140386EBA7CF322C58B78FE1C6DD67C0B08484A68682C4774B5D
          A56573EBE253188CD3B0852BC4109B67878F5FBD729321CEDB92212DD096414A
          949FE1DAFDE70CF3361E6558B8E5388393A9C69D5D27AEA9623520A676EEFA95
          BBCE041C9C55C2A0F6EF13039B800003AF820258EED981030CA7BFB13244B7AF
          640872345C8FD50059AFF2DFD9618E2CE5F1EE0C6F2F5C60D81910C0E0BE6103
          C3CD050B187E7DF8C0E000A40BFB5631ACDF7FE10F5603184DD2FFBFDFDFCFC0
          CFC309E6830CD9ECE0C02005C46E408340E0FCCDC70CA6B16D0C380D78BABD93
          4152841FCC3F909000B6F90DD020904B840D0C188E5DBACB6097D2C380D30BA1
          2EC62CBD85A160CD20007236B2774A365E64D877FAC61F9C81B8E9E0C580355D
          190CAEE69A28727FFFFD6398B1E61043C594750C810E38021114BF36663AFFCF
          5EBCC9E065ADCB90E46FCDA02E2FCE70F9CE538659EB0E3300A38FC1D90C1A8D
          D8B233300CC074B2BFF5ECE397EF45DF78F0821326A72E2FF1DD4A4F69293021
          A5822C02009244E8A4AC1C3E1E0000000049454E44AE426082}
        ExplicitLeft = 204
        ExplicitTop = 1
        ExplicitHeight = 26
      end
      inherited plCount: TPanel
        Left = 849
        TabOrder = 1
        ExplicitLeft = 849
      end
      object Panel6: TPanel
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 208
        Height = 29
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 10
        Margins.Bottom = 0
        Align = alLeft
        TabOrder = 0
        object dtStart: TDBDateTimeEditEh
          AlignWithMargins = True
          Left = 88
          Top = 4
          Width = 116
          Height = 21
          ControlLabel.Width = 81
          ControlLabel.Height = 13
          ControlLabel.Caption = #1053#1072#1095#1080#1085#1072#1103' '#1089' '#1076#1072#1090#1099
          ControlLabel.Visible = True
          ControlLabelLocation.Position = lpLeftCenterEh
          Align = alRight
          DynProps = <>
          EditButtons = <>
          Kind = dtkDateEh
          TabOrder = 0
          Visible = True
          OnChange = dtStartChange
        end
      end
      object edContainerNum: TDBEditEh
        AlignWithMargins = True
        Left = 519
        Top = 3
        Width = 202
        Height = 23
        Margins.Left = 90
        Align = alLeft
        ControlLabel.Width = 76
        ControlLabel.Height = 13
        ControlLabel.Caption = #8470' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        DynProps = <>
        EditButtons = <>
        TabOrder = 2
        Visible = True
        OnKeyPress = edContainerNumKeyPress
        ExplicitHeight = 21
      end
      object btRequery: TButton
        AlignWithMargins = True
        Left = 727
        Top = 1
        Width = 170
        Height = 25
        Margins.Top = 1
        Align = alLeft
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1080#1083#1100#1090#1088
        TabOrder = 3
        OnClick = btRequeryClick
      end
    end
    inherited dgData: TDBGridEh
      Top = 46
      Width = 1084
      Height = 498
      AllowedOperations = []
      Font.Height = -12
      Font.Name = 'Calibri'
      TitleParams.Font.Height = -12
      TitleParams.Font.Name = 'Calibri'
      TitleParams.MultiTitle = True
      OnDblClick = nil
      OnKeyPress = nil
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'order_num'
          Footers = <>
          Title.Caption = #8470' '#1086#1089#1085'.'
          Width = 56
        end
        item
          CellButtons = <>
          DisplayFormat = 'DD.MM.YYYY'
          DynProps = <>
          EditButtons = <>
          FieldName = 'order_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1089#1085'.'
          Width = 61
        end
        item
          Alignment = taCenter
          CellButtons = <>
          Color = 14079702
          DynProps = <>
          EditButtons = <>
          FieldName = 'used_amount_text'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Footers = <>
          Title.Caption = #1059#1078#1077' '#1074' '#1087#1083#1072#1085#1077'?'
          Width = 46
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnum'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
          Width = 95
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          Title.Caption = #1058#1080#1087
          Width = 46
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isempty'
          Footers = <>
          TextEditing = True
          Title.Caption = #1055#1086#1088'. ?'
          Width = 36
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'seal_number'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = [fsItalic]
          Footers = <>
          Title.Caption = #1055#1083#1086#1084#1073#1072
          Width = 70
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = False
          DynProps = <>
          EditButtons = <>
          FieldName = 'issealwaste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Footers = <>
          Title.Caption = #1043#1086#1076'- '#1085#1072#1103'?'
          Width = 41
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'forwarder_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          Width = 107
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'external_order_num'
          Footers = <>
          Title.Caption = #1043#1059'-12'
          Width = 58
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
          Width = 82
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dlv_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1086#1087#1077#1088#1072#1094#1080#1080
          Width = 87
        end
        item
          CellButtons = <>
          Color = 10348092
          DynProps = <>
          EditButtons = <>
          FieldName = 'station_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1085#1094#1080#1103' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1103
          Width = 98
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 83
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_factexecution'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1089#1090#1072#1090#1091#1089#1072
          Width = 66
        end
        item
          CellButtons = <>
          Color = 16744703
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_fact'
          Footers = <>
          Title.Caption = #1042#1077#1089
          Width = 57
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'carrying'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1055'- '#1089#1090#1100
          Width = 56
        end
        item
          CellButtons = <>
          Color = 16550789
          DynProps = <>
          EditButtons = <>
          FieldName = 'extra_services'
          Footers = <>
          Title.Caption = #1044#1086#1087'. '#1091#1089#1083#1091#1075#1080
          Width = 108
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'task_note'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Width = 112
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'task_id'
          Footers = <>
          Title.Caption = 'TASK ID'
        end>
    end
    inherited plHint: TPanel
      TabOrder = 3
    end
    object plStatus: TPanel
      Left = 0
      Top = 29
      Width = 1084
      Height = 17
      Align = alTop
      BevelOuter = bvNone
      Color = 15921906
      ParentBackground = False
      TabOrder = 2
      StyleElements = [seFont, seBorder]
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select c.id, c.cnum, c.carrying, los.task_id, '
      'd.code as dlv_code,  t.station_id, c.kind_id, o.owner_id,'
      
        '(select footsize from containerkinds ck where ck.id = c.kind_id)' +
        ' as footsize, '
      
        '(select note from docorder ord where ord.id = t.basedoc_id) as t' +
        'ask_note,'
      'dc.doc_number as order_num,'
      'dc.doc_date as order_date,'
      'r.note as task_note,'
      'r.extra_services,'
      'sp.id as orderspec_id,'
      'sp.used_amount,'
      'sp.external_order_num, '
      'sp.external_order_signed,'
      
        '(case when isnull(sp.used_amount,0)>0 then '#39#1044#1072#39' else '#39#39' end) as ' +
        'used_amount_text,'
      
        '(select code from counteragents cr where cr.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from counteragents cr where cr.id = t.forwarder_id)' +
        ' as forwarder_code,'
      
        '(select code from containerkinds k where k.id  = c.kind_id) as k' +
        'ind_code,'
      'isnull(cc.isempty,0) as isempty, cc.issealwaste,'
      
        '(case when cc.seal_number like '#39'{%'#39' then '#39#39' else cc.seal_number ' +
        'end) as seal_number,'
      
        '(select name from stations st where st.id = t.station_id) as sta' +
        'tion_code,'
      
        '(select isnull(isnull(weight_fact, weight_doc), weight_sender) f' +
        'rom cargos cc where cc.id = t.object_id) as weight_fact,'
      'sk.code as state_code, sk.inplacesign, los.date_factexecution '
      'from tasks t '
      'left outer join docorderspec sp on (sp.doc_id = t.basedoc_id) '
      'left outer join docorder r on (r.id = t.basedoc_id) '
      'left outer join documents dc on (dc.id = t.basedoc_id)'
      ',deliverytypes d, containers c, objects o '
      
        'left outer join v_lastobjectrealstates los on (los.linked_object' +
        '_id = o.id and los.task_id = o.current_task_id)'
      'left outer join cargos cc on (cc.id = los.object_id)'
      'left outer join objects o1 on (cc.id = o1.id)'
      
        'left outer join objectstatekinds sk on (sk.id = los.object_state' +
        '_id)'
      'where o.id = c.id and t.dlv_type_id = d.id '
      
        'and t.id = o.current_task_id and (sp.container_id = o.id or sp.i' +
        'd is null)'
      
        'and ((sk.inplacesign = 1 and sk.inworksign = 0 and isnull(sk.sto' +
        'p_sign,0)=0) or sk.id is null)'
      'and  (charindex(:sections, '#39','#39'+d.global_section+'#39','#39')>0)'
      'and isnull(los.task_id,0)<>0'
      'and dbo.filter_objects(:guid, c.cnum) = 1'
      'and dc.doc_date >= :start_date'
      'order by los.date_factexecution')
    SelectCommand.Parameters = <
      item
        Name = 'sections'
        Attributes = [paSigned]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'guid'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'start_date'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select c.id, c.cnum, c.carrying, los.task_id, '
      'd.code as dlv_code,  t.station_id, c.kind_id, o.owner_id,'
      
        '(select footsize from containerkinds ck where ck.id = c.kind_id)' +
        ' as footsize, '
      
        '(select note from docorder ord where ord.id = t.basedoc_id) as t' +
        'ask_note,'
      'dc.doc_number as order_num,'
      'dc.doc_date as order_date,'
      'r.note as task_note,'
      'r.extra_services,'
      'sp.id as orderspec_id,'
      'sp.used_amount,'
      'sp.external_order_num, '
      'sp.external_order_signed,'
      
        '(case when isnull(sp.used_amount,0)>0 then '#39#1044#1072#39' else '#39#39' end) as ' +
        'used_amount_text,'
      
        '(select code from counteragents cr where cr.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from counteragents cr where cr.id = t.forwarder_id)' +
        ' as forwarder_code,'
      
        '(select code from containerkinds k where k.id  = c.kind_id) as k' +
        'ind_code,'
      'isnull(cc.isempty,0) as isempty, cc.issealwaste,'
      
        '(case when cc.seal_number like '#39'{%'#39' then '#39#39' else cc.seal_number ' +
        'end) as seal_number,'
      
        '(select name from stations st where st.id = t.station_id) as sta' +
        'tion_code,'
      
        '(select isnull(isnull(weight_fact, weight_doc), weight_sender) f' +
        'rom cargos cc where cc.id = t.object_id) as weight_fact,'
      'sk.code as state_code, sk.inplacesign, los.date_factexecution '
      'from tasks t '
      'left outer join docorderspec sp on (sp.doc_id = t.basedoc_id) '
      'left outer join docorder r on (r.id = t.basedoc_id) '
      'left outer join documents dc on (dc.id = t.basedoc_id)'
      ',deliverytypes d, containers c, objects o '
      
        'left outer join v_lastobjectstates los on (los.linked_object_id ' +
        '= o.id and los.task_id = o.current_task_id)'
      'left outer join cargos cc on (cc.id = los.object_id)'
      'left outer join objects o1 on (cc.id = o1.id)'
      'left outer join objectstatekinds sk on (sk.id = o1.state_id)'
      'where o.id = c.id and t.dlv_type_id = d.id '
      
        'and t.id = o.current_task_id and (sp.container_id = o.id or sp.i' +
        'd is null)'
      
        'and ((sk.inplacesign = 1 and sk.inworksign = 0) or sk.id is null' +
        ')'
      'and  (charindex(:sections, '#39','#39'+d.global_section+'#39','#39')>0)'
      'and isnull(los.task_id,0)<>0'
      'and c.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'sections'
        Size = -1
        Value = Null
      end
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
    inherited aDelete: TAction
      Enabled = False
    end
    inherited aEdit: TAction
      Enabled = False
    end
  end
end
