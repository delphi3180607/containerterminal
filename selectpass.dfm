﻿inherited FormSelectPass: TFormSelectPass
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1087#1088#1086#1087#1091#1089#1082
  ClientHeight = 489
  ClientWidth = 846
  ExplicitWidth = 852
  ExplicitHeight = 517
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 448
    Width = 846
    inherited btnCancel: TButton
      Left = 730
    end
    inherited btnOk: TButton
      Left = 611
      Caption = #1042#1099#1073#1088#1072#1090#1100
    end
  end
  object dgSpec: TDBGridEh [1]
    Left = 0
    Top = 0
    Width = 846
    Height = 448
    Align = alClient
    AllowedOperations = []
    Border.Color = clSilver
    ColumnDefValues.Title.TitleButton = True
    Ctl3D = False
    DataSource = dsSpec
    DynProps = <>
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Calibri'
    Font.Style = []
    GridLineParams.BrightColor = 15395041
    GridLineParams.DarkColor = 15395041
    GridLineParams.DataBoundaryColor = 15395041
    GridLineParams.DataHorzColor = clSilver
    GridLineParams.DataVertColor = clSilver
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
    IndicatorParams.HorzLineColor = clSilver
    IndicatorParams.VertLineColor = clSilver
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    SelectionDrawParams.SelectionStyle = gsdsClassicEh
    SelectionDrawParams.DrawFocusFrame = False
    SelectionDrawParams.DrawFocusFrameStored = True
    ShowHint = True
    SortLocal = True
    STFilter.Color = 15725813
    STFilter.Font.Charset = DEFAULT_CHARSET
    STFilter.Font.Color = clWindowText
    STFilter.Font.Height = -11
    STFilter.Font.Name = 'Verdana'
    STFilter.Font.Style = []
    STFilter.HorzLineColor = clSilver
    STFilter.Local = True
    STFilter.ParentFont = False
    STFilter.VertLineColor = clSilver
    STFilter.Visible = True
    TabOrder = 1
    TitleParams.Color = clBtnFace
    TitleParams.FillStyle = cfstThemedEh
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -13
    TitleParams.Font.Name = 'Calibri'
    TitleParams.Font.Style = [fsBold]
    TitleParams.HorzLineColor = 11645361
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    TitleParams.SecondColor = 15987699
    TitleParams.SortMarkerStyle = smstDefaultEh
    TitleParams.VertLineColor = 13421772
    Columns = <
      item
        AutoFitColWidth = False
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'pass_number'
        Footers = <>
        Title.Caption = #1053#1086#1084#1077#1088' '#1087#1088#1086#1087#1091#1089#1082#1072
        Width = 80
      end
      item
        AutoFitColWidth = False
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'pass_date'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072' '#1087#1088#1086#1087#1091#1089#1082#1072
        Width = 70
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'op_kind_code'
        Footers = <>
        Title.Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        Width = 125
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'cnum'
        Footers = <>
        Title.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
        Width = 129
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'seal_number'
        Footers = <>
        Title.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
        Width = 135
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'cont_kind_code'
        Footers = <>
        Title.Caption = #1058#1080#1087' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
        Width = 109
      end
      item
        CellButtons = <>
        Checkboxes = True
        DynProps = <>
        EditButtons = <>
        FieldName = 'isempty'
        Footers = <>
        Title.Caption = #1055#1086#1088#1086#1078#1085#1080#1081'?'
        Width = 101
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'state_code'
        Footers = <>
        Title.Caption = #1057#1090#1072#1090#1091#1089
        Width = 146
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        Footers = <>
        Title.Caption = #1040#1082#1090' '#1087#1088#1080#1077#1084#1082#1080'/'#1074#1099#1076#1072#1095#1080
        Width = 172
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'declare @task_id int;'
      ''
      'select @task_id = :task_id;'
      ''
      'select p.id as pass_id, p.pass_date, p.pass_number,'
      'o.*,  isnull(c.cnum, o.container_num) as cnum,'
      
        '(case when passoperation_type = 0 then '#39#1055#1088#1080#1077#1084#39' when passoperatio' +
        'n_type = 1 then '#39#1042#1099#1076#1072#1095#1072#39' else '#39'???'#39' end) as passoptype_text,'
      
        '(select code from containerkinds ck where ck.id = isnull(c.kind_' +
        'id,o.container_kind_id)) as cont_kind_code,'
      'ok.direction, ok.code as op_kind_code,'
      
        '(case when isnull(state,0) = 0 then '#39#39' when isnull(state,0) = 1 ' +
        'then '#39#1047#1072#1074#1077#1088#1096#1077#1085#1086#39' when isnull(state,0) = 2 then '#39#1055#1088#1080#1089#1090#1072#1085#1086#1074#1083#1077#1085#1086#39' w' +
        'hen isnull(state,0) = 3 then '#39#1040#1085#1085#1091#1083#1080#1088#1086#1074#1072#1085#1086#39' end) as state_code'
      'from passoperations o'
      'inner join passes p on (p.id = o.parent_id)'
      
        'left outer join passoperationkinds ok on (ok.id = o.passoperatio' +
        'n_id)'
      'left outer join containers c on (c.id =o.container_id)  '
      'where isnull(o.higher_id,0)<=0 and o.task_id = @task_id')
    SelectCommand.Parameters = <
      item
        Name = 'task_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.CommandText.Strings = (
      'declare @parent_id int;'
      ''
      'select @parent_id = :parent_id;'
      ''
      
        'select o.*,  0 as isparent, 0 as ischild, isnull(c.cnum, o.conta' +
        'iner_num) as cnum,'
      
        '(case when passoperation_type = 0 then '#39#1055#1088#1080#1077#1084#39' when passoperatio' +
        'n_type = 1 then '#39#1042#1099#1076#1072#1095#1072#39' else '#39'???'#39' end) as passoptype_text,'
      
        '(select code from containerkinds ck where ck.id = isnull(c.kind_' +
        'id,o.container_kind_id)) as cont_kind_code,'
      'ok.direction, ok.code as op_kind_code,'
      
        '(case when isnull(state,0) = 0 then '#39#39' when isnull(state,0) = 1 ' +
        'then '#39#1047#1072#1074#1077#1088#1096#1077#1085#1086#39' when isnull(state,0) = 2 then '#39#1055#1088#1080#1089#1090#1072#1085#1086#1074#1083#1077#1085#1086#39' w' +
        'hen isnull(state,0) = 3 then '#39#1040#1085#1085#1091#1083#1080#1088#1086#1074#1072#1085#1086#39' end) as state_code'
      'from passoperations o'
      
        'left outer join passoperationkinds ok on (ok.id = o.passoperatio' +
        'n_id)'
      'left outer join containers c on (c.id =o.container_id)  '
      'where o.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'parent_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 160
    Top = 112
  end
  object meSpec: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvSpec
    TreeList.Active = True
    TreeList.KeyFieldName = 'id'
    TreeList.RefParentFieldName = 'higher_id'
    TreeList.DefaultNodeExpanded = True
    Left = 208
    Top = 112
  end
  object dsSpec: TDataSource
    DataSet = meSpec
    Left = 264
    Top = 112
  end
end
