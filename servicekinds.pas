﻿unit servicekinds;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, MemTableDataEh, Data.DB,
  Data.Win.ADODB, MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport, functions;

type
  TFormServiceKinds = class(TFormGrid)
    Panel1: TPanel;
    Bevel1: TBevel;
    dgFolders: TDBGridEh;
    plTool: TPanel;
    cbAll: TCheckBox;
    Splitter1: TSplitter;
    drvFolders: TADODataDriverEh;
    meFolders: TMemTableEh;
    dsFolders: TDataSource;
    pmFolders: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem10: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    procedure MenuItem10Click(Sender: TObject);
    procedure meFoldersBeforePost(DataSet: TDataSet);
    procedure cbAllClick(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure meDataBeforePost(DataSet: TDataSet);
    procedure btExcelClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormServiceKinds: TFormServiceKinds;

implementation

{$R *.dfm}

uses editservice, editcatalog, selectfolder, dmu;


procedure TFormServiceKinds.btExcelClick(Sender: TObject);
begin
  dgData.SetFocus;
  inherited;
end;

procedure TFormServiceKinds.cbAllClick(Sender: TObject);
begin

  if cbAll.Checked then
  begin
    meData.MasterSource := nil;
  end else
  begin
    meData.MasterSource := dsFolders;
  end;

end;

procedure TFormServiceKinds.Init;
begin
  inherited;
  tablename := 'servicekinds';

  self.drvFolders.SelectCommand.Parameters.ParamByName('folder_section').Value := 'servicekinds';
  self.meFolders.Open;

  meFolders.Open;
  meData.Open;
  self.AddEditForm(dgData,FormEditService,meFolders);
  self.AddEditForm(dgFolders,FormEditCatalog,nil,meData);
end;


procedure TFormServiceKinds.meDataBeforePost(DataSet: TDataSet);
begin
  meData.FieldByName('folder_id').Value := meFolders.FieldByName('id').AsInteger;
  inherited;
end;

procedure TFormServiceKinds.meFoldersBeforePost(DataSet: TDataSet);
begin
  inherited;
  meFolders.FieldByName('folder_section').Value := 'servicekinds';
end;

procedure TFormServiceKinds.MenuItem10Click(Sender: TObject);
begin
  meFolders.Close;
  meFolders.Open;
end;

procedure TFormServiceKinds.MenuItem1Click(Sender: TObject);
begin
  FormEditCatalog.system_section := 'servicekinds';
  EA(self, FormEditCatalog,meFolders,'servicekinds');
end;

procedure TFormServiceKinds.MenuItem2Click(Sender: TObject);
begin

  dm.qrAux.Close;
  dm.qrAux.SQL.Clear;
  dm.qrAux.SQL.Add('select 1 where exists (select 1 from servicekinds where folder_id = '''+self.meFolders.FieldByName('id').AsString+''')');
  dm.qrAux.Open;

  if dm.qrAux.RecordCount>0 then
  begin
    ShowMessage('Не могу удалить непустой каталог.');
    exit;
  end;

  if fQYN('Удалить запись ?') then
  begin
     self.meFolders.Delete;
  end;

end;

procedure TFormServiceKinds.MenuItem3Click(Sender: TObject);
begin
  FormEditCatalog.system_section := 'servicekinds';
  EE(self, FormEditCatalog,meFolders,'servicekinds');
end;

procedure TFormServiceKinds.N8Click(Sender: TObject);
begin
  FormSelectFolder.meFolders.Close;
  FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('folder_section').Value := 'servicekinds';
  FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('startid').Value := 0;
  FormSelectFolder.meFolders.Open;
  FormSelectFolder.ShowModal;
  if FormSelectFolder.ModalResult = mrOk then
  begin
      self.meData.Edit;
      self.meData.FieldByName('folder_id').Value := FormSelectFolder.meFolders.FieldByName('id').AsInteger;
      self.meData.Post;
      self.meData.Refresh;
  end;
end;

end.
