﻿unit matrix;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  Vcl.ExtCtrls, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand, EXLReport,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Mask, DBCtrlsEh,
  Vcl.Buttons, PngSpeedButton, Vcl.ComCtrls, matrixsector, functions, JvgPage;

type
  TFormMatrix = class(TFormGrid)
    SplitterVert: TSplitter;
    Panel1: TPanel;
    Bevel4: TBevel;
    plTool: TPanel;
    btnDeleteContainer: TPngSpeedButton;
    Bevel1: TBevel;
    PngSpeedButton4: TPngSpeedButton;
    edContainerNum: TDBEditEh;
    drvSectors: TADODataDriverEh;
    meSectors: TMemTableEh;
    N8: TMenuItem;
    Timer1: TTimer;
    plLegend: TPanel;
    Shape1: TShape;
    Label1: TLabel;
    Label2: TLabel;
    Shape2: TShape;
    Label3: TLabel;
    Shape3: TShape;
    Label4: TLabel;
    Shape4: TShape;
    Shape5: TShape;
    Label5: TLabel;
    Shape6: TShape;
    btRemoveQueue: TPngSpeedButton;
    sbMinus: TSpeedButton;
    sbPlus: TSpeedButton;
    pg: TProgressBar;
    sbWrap: TSpeedButton;
    pc: TJvgPageControl;
    procedure btnAddContainerClick(Sender: TObject);
    procedure pcChange(Sender: TObject);
    procedure edContainerNumKeyPress(Sender: TObject; var Key: Char);
    procedure N8Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure btnDeleteContainerClick(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure sbRefreshClick(Sender: TObject);
    procedure dgDataColumns2CellButtons0Click(Sender: TObject;
      var Handled: Boolean);
    procedure dgDataColumns2EditButtonClick(Sender: TObject;
      var Handled: Boolean);
    procedure dgDataColumns2CellDataLinkClick(Grid: TCustomDBGridEh;
      Column: TColumnEh);
    procedure btRemoveQueueClick(Sender: TObject);
    procedure sbMinusClick(Sender: TObject);
    procedure sbPlusClick(Sender: TObject);
  private
    oldCursor: TCursor;
    function FocusOnContainer(sectorid, rownum, stacknum, levelnum, cellnum: integer): boolean;
  public
    procedure Init; override;
    procedure PartialInit; override;
    function FindContainer(cnum: string): boolean;
  end;

var
  FormMatrix: TFormMatrix;

implementation

{$R *.dfm}

uses RemoveQueue;

procedure TFormMatrix.PartialInit;
begin
  inherited;
  meData.Close;
  meData.Open;
end;

procedure TFormMatrix.btnDeleteContainerClick(Sender: TObject);
var t: TTabSheet; fs: TFormMatrixSector;
begin

    if pc.TabIndex>=0 then
    begin
      t := pc.Pages[pc.TabIndex];
      fs := TFormMatrixSector(t.Controls[0]);
      fs.DeleteContainer;
    end;

end;

procedure TFormMatrix.btRemoveQueueClick(Sender: TObject);
begin
  FormRemoveQueue.ShowModal;
end;

procedure TFormMatrix.dgDataColumns2CellButtons0Click(Sender: TObject;
  var Handled: Boolean);
begin
  N8Click(nil);
end;

procedure TFormMatrix.dgDataColumns2CellDataLinkClick(Grid: TCustomDBGridEh; Column: TColumnEh);
begin
  N8Click(nil);
end;

procedure TFormMatrix.dgDataColumns2EditButtonClick(Sender: TObject; var Handled: Boolean);
begin
  N8Click(Sender);
end;

procedure TFormMatrix.dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
var i: integer; fn, formal_mark, state_mark, next_mark, loadplanned: string; isempty, defect: boolean;
var textRect, cellRect: TRect; pnum, s: string; marks, maininfo: array of string;
begin

  fn := Column.FieldName;
  textRect := System.Classes.rect(Rect.Left+2, Rect.Top+2, Rect.Right-1, Rect.Bottom-1);
  dgData.Canvas.Font.Style := Column.Font.Style;

  state_mark := meData.FieldByName('state_mark').AsString;
  formal_mark := meData.FieldByName('formalstate_mark').AsString;
  loadplanned := meData.FieldByName('loadplanned').AsString;
  next_mark := meData.FieldByName('nextstate_mark').AsString;
  isempty := meData.FieldByName('isempty').AsBoolean;
  defect := meData.FieldByName('isdefective').AsBoolean;

  if fn = 'defect_column' then
    if defect then
    dgData.Canvas.Brush.Color := clRed;


  if fn = 'container_code' then
  begin

    if isempty then
      dgData.Canvas.Font.Style := [fsItalic]
    else
      dgData.Canvas.Font.Style := [fsBold];

      if (next_mark='>') then
      begin
        dgData.Canvas.Brush.Style := bsSolid;
        dgData.Canvas.Brush.Color := clAqua;
      end;

      if (formal_mark='Е') then
      begin
        dgData.Canvas.Brush.Style := bsSolid;
        dgData.Canvas.Brush.Color := clBlue;
        dgData.Canvas.Font.Color := clWhite;
      end;

      if (state_mark='П') or (state_mark='^') or (next_mark='<') then
      begin
        dgData.Canvas.Brush.Style := bsSolid;
        dgData.Canvas.Brush.Color := clYellow;
      end;

      if (loadplanned='1') then
      begin
        dgData.Canvas.Brush.Style := bsSolid;
        dgData.Canvas.Brush.Color := clLime;
      end;

      // Есть заявка на отправку
      if (loadplanned='2') then
      begin
        dgData.Canvas.Brush.Style := bsSolid;
        dgData.Canvas.Brush.Color := $00C1F0FF;
      end;

      if (state_mark='>') or (state_mark='<') then
      begin
        dgData.Canvas.Brush.Style := bsSolid;
        dgData.Canvas.Brush.Color := clRed;
        dgData.Canvas.Font.Color := clWhite;
      end;

  end;

  dgData.DefaultDrawColumnCell(Rect,DataCol,Column,State);

  if gdSelected in State then begin

    dgData.Canvas.Brush.Style := bsClear;
    dgData.Canvas.Font.Style := Column.Font.Style;
    dgData.Canvas.Pen.Color := clBlue;
    dgData.Canvas.Pen.Width := 1;
    dgData.Canvas.Rectangle(Rect.Left, Rect.Top, Rect.Right, Rect.Bottom);
  end;

end;

procedure TFormMatrix.edContainerNumKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    if edContainerNum.Text<>'' then
    begin
      if not FindContainer(trim(edContainerNum.Text)) then
      ShowMessage('Не найден.');
    end;
    Key := #0;
  end;
end;

function TFormMatrix.FindContainer(cnum: string): boolean;
begin

  result := true;

  qrAux.Close;
  qrAux.Parameters.ParamByName('cnum').Value := cnum;
  qrAux.Open;

  if qrAux.RecordCount = 0 then
  begin
    result := false;
  end else
  begin
    result := FocusOnContainer(
      qrAux.FieldByName('sector_id').AsInteger,
      qrAux.FieldByName('row_num').AsInteger,
      qrAux.FieldByName('stack_num').AsInteger,
      qrAux.FieldByName('level_num').AsInteger,
      qrAux.FieldByName('cell_num').AsInteger
  );
  end;

end;



function TFormMatrix.FocusOnContainer(sectorid, rownum, stacknum, levelnum, cellnum: integer): boolean;
var t: TTabSheet; fs: TFormMatrixSector; i: integer; found: boolean; c: TColumnEh; fn: string;
begin

  result := true;

  found := false;

  for i := 0 to pc.PageCount-1 do
  begin
    t := pc.Pages[i];
    fs := TFormMatrixSector(t.Controls[0]);
    if fs.sectorid_stored = sectorid then
    begin
      found := true;
      break;
    end;
  end;

  if not found then
  begin
    result := false;
    exit;
  end else
  begin
    pc.ActivePage := t;
    if not fs.meSector.Locate('row_num;level_num', VarArrayOf([rownum, levelnum]), []) then
    begin
      result := false;
      exit;
    end else
    begin

      fn := 's'+IntToStr(stacknum)+'_'+IntToStr(cellnum);
      c := fs.dgView.FindFieldColumn(fn);

      if c=nil then
      begin
        result := false;
        exit;
      end else
      begin
        if fs.dgView.Columns.Count>c.Index then
        fs.dgView.Col := c.Index+1;
        fs.meSectorAfterScroll(fs.meSector);
      end;

    end;

  end;

end;

procedure TFormMatrix.Init;
var t: TTabSheet; fs: TFormMatrixSector; i: integer;
begin

  if not isinited then
  begin

      pc.Hide;

      meSectors.Close;
      meSectors.Open;

      pg.Min := 0;
      pg.Max := meSectors.RecordCount+1;
      pg.Position := 1;
      pg.Show;
      Screen.Cursor := crHourGlass;
      Application.ProcessMessages;

      while not meSectors.Eof do
      begin

        t := TTabSheet.Create(pc);
        t.AlignWithMargins := false;
        t.Caption := meSectors.FieldByName('code').AsString;
        t.PageControl := pc;
        t.Show;

        Application.CreateForm(TFormMatrixSector,fs);
        fs.Parent := t;
        fs.BorderStyle := bsNone;
        fs.Align := alClient;
        fs.Show;
        fs.Init(meSectors.FieldByName('id').AsInteger, meSectors.FieldByName('code').AsString, meSectors.FieldByName('rowreverse').AsBoolean);

        pg.Position := pg.Position + 1;
        Application.ProcessMessages;

        meSectors.Next;

      end;

      if pc.PageCount>0 then
        pc.TabIndex := 0;

      pc.Show;
      pg.Hide;
      Screen.Cursor := crDefault;

  end else
  begin

      for i := 0 to pc.PageCount-1 do
      begin
        t := pc.Pages[i];
        fs := TFormMatrixSector(t.Controls[0]);
        fs.Init(-1, '', false);
      end;

      if pc.PageCount>0 then
        pc.TabIndex := 0;

  end;

  inherited;

  hidehourglass := true;

end;

procedure TFormMatrix.N8Click(Sender: TObject);
var t: TTabSheet; fs: TFormMatrixSector;
begin

  Timer1.Enabled := false;


  try
    drvData.GetrecCommand.Parameters.ParamByName('current_id').Value := meData.FieldByName('id').AsInteger;
    meData.RefreshRecord;
  except
    ShowMessage('Такой записи уже не существует.');
    meData.Close;
    meData.Open;
    exit;
  end;



  if meData.FieldByName('operation_type').AsInteger = 1 then
  begin
    if fQYN('Разместить этот контейнер в выбранной ячейке?') then
    begin
      t := pc.Pages[pc.TabIndex];
      fs := TFormMatrixSector(t.Controls[0]);

      if fs.AddContainerById(meData.FieldByName('container_id').AsInteger) then
      begin

        meData.Edit;
        meData.FieldByName('iscompleted').Value := true;
        try
          meData.Post;
        except
          //--
        end;

        meData.Close;
        meData.Open;

      end;

    end;
  end;

  if meData.FieldByName('operation_type').AsInteger = 2 then
  begin
    if fQYN('Удалить выбранный контейнер из матрицы?') then
    begin
      FindContainer(meData.FieldByName('cnum').AsString);
      t := pc.Pages[pc.TabIndex];
      fs := TFormMatrixSector(t.Controls[0]);

      if fs.DeleteContainerById(meData.FieldByName('container_id').AsInteger) then
      begin
        meData.Edit;
        meData.FieldByName('iscompleted').Value := true;
        try
          meData.Post;
        except
          //--
        end;
      end;

      meData.Close;
      meData.Open;
    end;
  end;

  meData.Close;
  meData.Open;

  Timer1.Enabled := true;

end;

procedure TFormMatrix.pcChange(Sender: TObject);
var t: TTabSheet; fs: TFormMatrixSector;
begin

    if pc.TabIndex>=0 then
    begin
      t := pc.Pages[pc.TabIndex];
      fs := TFormMatrixSector(t.Controls[0]);
      fs.Init(-1, '', false);
    end;

end;

procedure TFormMatrix.sbMinusClick(Sender: TObject);
var t: TTabSheet; fs: TFormMatrixSector;
begin

    if pc.TabIndex>=0 then
    begin
      t := pc.Pages[pc.TabIndex];
      fs := TFormMatrixSector(t.Controls[0]);
      fs.DecreaseScale;
    end;

end;

procedure TFormMatrix.sbPlusClick(Sender: TObject);
var t: TTabSheet; fs: TFormMatrixSector;
begin

    if pc.TabIndex>=0 then
    begin
      t := pc.Pages[pc.TabIndex];
      fs := TFormMatrixSector(t.Controls[0]);
      fs.IncreaseScale;
    end;

end;

procedure TFormMatrix.sbRefreshClick(Sender: TObject);
var t: TTabSheet; fs: TFormMatrixSector;
begin

  meData.Close;
  meData.Open;

  if pc.TabIndex>=0 then
  begin
    t := pc.Pages[pc.TabIndex];
    fs := TFormMatrixSector(t.Controls[0]);
    fs.RestorePosition;
  end;

end;

procedure TFormMatrix.btnAddContainerClick(Sender: TObject);
var t: TTabSheet; fs: TFormMatrixSector;
begin

    if pc.TabIndex>=0 then
    begin
      t := pc.Pages[pc.TabIndex];
      fs := TFormMatrixSector(t.Controls[0]);
      fs.AddContainer(0);
    end;

end;

procedure TFormMatrix.Timer1Timer(Sender: TObject);
var t: TTabSheet; fs: TFormMatrixSector;
begin

  exit;

  if not self.Visible then
  exit;

  meData.Close;
  meData.Open;

  if pc.TabIndex>=0 then
  begin
    t := pc.Pages[pc.TabIndex];
    fs := TFormMatrixSector(t.Controls[0]);
    fs.RestorePosition;
  end;

end;

end.
