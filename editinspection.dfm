﻿inherited FormEditInspection: TFormEditInspection
  Caption = #1054#1090#1084#1077#1090#1082#1072' '#1086' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1080
  ClientHeight = 122
  ClientWidth = 633
  ExplicitWidth = 639
  ExplicitHeight = 150
  PixelsPerInch = 96
  TextHeight = 16
  object Label5: TLabel [0]
    Left = 16
    Top = 17
    Width = 128
    Height = 13
    Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1087#1077#1088#1077#1076#1072#1095#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBtnText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    StyleElements = [seClient, seBorder]
  end
  object Label4: TLabel [1]
    Left = 196
    Top = 17
    Width = 88
    Height = 13
    Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbPersons: TSpeedButton [2]
    Left = 579
    Top = 33
    Width = 31
    Height = 24
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = sbPersonsClick
  end
  inherited plBottom: TPanel
    Top = 81
    Width = 633
    TabOrder = 2
    inherited btnCancel: TButton
      Left = 517
    end
    inherited btnOk: TButton
      Left = 398
    end
  end
  object dtFactDate: TDBDateTimeEditEh [4]
    Left = 16
    Top = 34
    Width = 164
    Height = 24
    DataField = 'fact_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateTimeEh
    TabOrder = 0
    Visible = True
  end
  object luPerson: TDBLookupComboboxEh [5]
    Left = 194
    Top = 34
    Width = 379
    Height = 24
    DynProps = <>
    DataField = 'inspector_id'
    DataSource = dsLocal
    EditButtons = <>
    KeyField = 'id'
    ListField = 'person_name'
    ListSource = FormPersons.dsData
    TabOrder = 1
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 80
    Top = 64
  end
  inherited qrAux: TADOQuery
    Left = 16
    Top = 64
  end
end
