﻿unit EditContainerInDoc;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBSQLLookUp, DBCtrlsEh,
  Vcl.StdCtrls, Vcl.Mask, Vcl.ExtCtrls, Data.DB, Data.Win.ADODB;

type
  TFormEditContainerInDoc = class(TFormEdit)
    Shape1: TShape;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    Label17: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label13: TLabel;
    edContNum: TDBEditEh;
    leContainerKind: TDBSQLLookUp;
    nuWeight: TDBNumberEditEh;
    nuCarrying: TDBNumberEditEh;
    dtDateMade: TDBDateTimeEditEh;
    dtDateStiker: TDBDateTimeEditEh;
    cbPicture: TDBCheckBoxEh;
    dtDateInspect: TDBComboBoxEh;
    edContainerId: TDBEditEh;
    cbTall: TDBCheckBoxEh;
    ssContainerKind: TADOLookUpSqlSet;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditContainerInDoc: TFormEditContainerInDoc;

implementation

{$R *.dfm}

end.
