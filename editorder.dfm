﻿inherited FormEditOrder: TFormEditOrder
  Caption = #1047#1072#1103#1074#1082#1072' '#1075#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1103
  ClientHeight = 623
  ClientWidth = 781
  Font.Height = -11
  OnActivate = FormActivate
  ExplicitWidth = 787
  ExplicitHeight = 651
  PixelsPerInch = 96
  TextHeight = 13
  object sbPayer: TSpeedButton [0]
    Left = 283
    Top = 229
    Width = 33
    Height = 23
    Caption = '...'
    OnClick = sbPayerClick
  end
  object Label9: TLabel [1]
    Left = 8
    Top = 211
    Width = 64
    Height = 13
    Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
  end
  object Label16: TLabel [2]
    Left = 8
    Top = 57
    Width = 123
    Height = 13
    Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080'/'#1054#1087#1077#1088#1072#1094#1080#1103
  end
  object sbForwarder: TSpeedButton [3]
    Left = 283
    Top = 126
    Width = 33
    Height = 23
    Caption = '...'
    OnClick = sbForwarderClick
  end
  object Label2: TLabel [4]
    Left = 8
    Top = 110
    Width = 37
    Height = 13
    Caption = #1050#1083#1080#1077#1085#1090
  end
  object Label8: TLabel [5]
    Left = 335
    Top = 57
    Width = 89
    Height = 13
    Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
  end
  object sbConsignee: TSpeedButton [6]
    Left = 731
    Top = 74
    Width = 36
    Height = 23
    Caption = '...'
    OnClick = sbConsigneeClick
  end
  object Label6: TLabel [7]
    Left = 335
    Top = 160
    Width = 25
    Height = 13
    Caption = #1055#1086#1088#1090
  end
  object Label3: TLabel [8]
    Left = 165
    Top = 4
    Width = 84
    Height = 13
    Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object Label1: TLabel [9]
    Left = 8
    Top = 4
    Width = 89
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object sbDirections: TSpeedButton [10]
    Left = 731
    Top = 178
    Width = 36
    Height = 23
    Caption = '...'
    OnClick = sbDirectionsClick
  end
  object Label13: TLabel [11]
    Left = 335
    Top = 262
    Width = 31
    Height = 13
    Caption = #1040#1076#1088#1077#1089
  end
  object Label14: TLabel [12]
    Left = 335
    Top = 314
    Width = 43
    Height = 13
    Caption = #1050#1086#1085#1090#1072#1082#1090
  end
  object Label11: TLabel [13]
    Left = 8
    Top = 262
    Width = 43
    Height = 13
    Caption = #1044#1086#1075#1086#1074#1086#1088
  end
  object Label12: TLabel [14]
    Left = 8
    Top = 160
    Width = 126
    Height = 13
    Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082' '#1046#1044' '#1090#1072#1088#1080#1092#1072
  end
  object SpeedButton1: TSpeedButton [15]
    Left = 283
    Top = 178
    Width = 33
    Height = 23
    Caption = '...'
    OnClick = sbPayerClick
  end
  object Label17: TLabel [16]
    Left = 8
    Top = 314
    Width = 59
    Height = 13
    Caption = #1058#1080#1087' '#1090#1072#1088#1080#1092#1072
    Visible = False
  end
  object Label18: TLabel [17]
    Left = 335
    Top = 367
    Width = 58
    Height = 13
    Caption = #1044#1086#1087'.'#1091#1089#1083#1091#1075#1080
  end
  object sbStations: TSpeedButton [18]
    Left = 731
    Top = 126
    Width = 36
    Height = 23
    Caption = '...'
    OnClick = sbStationsClick
  end
  object Label10: TLabel [19]
    Left = 335
    Top = 108
    Width = 137
    Height = 14
    Caption = #1057#1090#1072#1085#1094#1080#1103' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    StyleElements = [seClient, seBorder]
  end
  object sbEndPoint: TSpeedButton [20]
    Left = 731
    Top = 228
    Width = 36
    Height = 23
    Caption = '...'
    OnClick = sbEndPointClick
  end
  object Label4: TLabel [21]
    Left = 335
    Top = 211
    Width = 114
    Height = 13
    Caption = #1050#1086#1085#1077#1095#1085#1099#1081' '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
  end
  object Label5: TLabel [22]
    Left = 8
    Top = 468
    Width = 98
    Height = 13
    Caption = #1044#1072#1090#1072' '#1089#1086#1075#1083#1072#1089#1086#1074#1072#1085#1080#1103
  end
  inherited plBottom: TPanel
    Top = 582
    Width = 781
    TabOrder = 19
    ExplicitTop = 582
    ExplicitWidth = 781
    inherited btnCancel: TButton
      Left = 665
      ExplicitLeft = 665
    end
    inherited btnOk: TButton
      Left = 546
      OnClick = btnOkClick
      ExplicitLeft = 546
    end
  end
  object dtDocDate: TDBDateTimeEditEh [24]
    Left = 165
    Top = 23
    Width = 121
    Height = 22
    DataField = 'doc_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object edNote: TDBEditEh [25]
    Left = 8
    Top = 549
    Width = 758
    Height = 21
    Color = clCream
    ControlLabel.Width = 61
    ControlLabel.Height = 13
    ControlLabel.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 18
    Visible = True
  end
  object laDlvTypes: TDBSQLLookUp [26]
    Left = 8
    Top = 75
    Width = 308
    Height = 22
    Color = 13695741
    DataField = 'dlv_type_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 2
    Visible = True
    SqlSet = ssDlvTypes
    RowCount = 0
    OnKeyValueChange = laDlvTypesKeyValueChange
  end
  object edExtraServices: TDBEditEh [27]
    Left = 335
    Top = 502
    Width = 433
    Height = 22
    Color = clCream
    DataField = 'extra_services'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 14
    Visible = False
    OnChange = laPayerKeyValueChange
  end
  object laTarifType: TDBSQLLookUp [28]
    Left = 8
    Top = 332
    Width = 308
    Height = 22
    DataField = 'tarif_type_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 12
    Visible = False
    SqlSet = ssTarifType
    RowCount = 0
  end
  object laDirection: TDBSQLLookUp [29]
    Left = 332
    Top = 179
    Width = 393
    Height = 22
    DataField = 'direction_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 7
    Visible = True
    SqlSet = ssDirections
    RowCount = 0
    OnKeyValueChange = laPayerKeyValueChange
  end
  object laConsignee: TDBSQLLookUp [30]
    Left = 335
    Top = 75
    Width = 393
    Height = 22
    DataField = 'consignee_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 4
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object laForwarder: TDBSQLLookUp [31]
    Left = 8
    Top = 127
    Width = 271
    Height = 22
    Color = 13695741
    DataField = 'forwarder_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 3
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
    OnKeyValueChange = laForwarderKeyValueChange
  end
  object laELSPayer: TDBSQLLookUp [32]
    Left = 8
    Top = 179
    Width = 271
    Height = 22
    DataField = 'els_payer'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 5
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object laPayer: TDBSQLLookUp [33]
    Left = 8
    Top = 230
    Width = 271
    Height = 22
    Color = 13695741
    DataField = 'payer_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 8
    Visible = True
    OnChange = luPayerChange
    SqlSet = dm.ssCounteragents
    RowCount = 0
    OnKeyValueChange = laPayerKeyValueChange
  end
  object laContract: TDBSQLLookUp [34]
    Left = 8
    Top = 280
    Width = 308
    Height = 22
    DataField = 'contract_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 10
    Visible = True
    SqlSet = ssContracts
    RowCount = 0
  end
  object laStation: TDBSQLLookUp [35]
    Left = 335
    Top = 127
    Width = 393
    Height = 22
    DataField = 'station_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 6
    Visible = True
    SqlSet = ssStations
    RowCount = 0
    OnKeyValueChange = laPayerKeyValueChange
  end
  object edDlvContact: TDBEditEh [36]
    Left = 335
    Top = 332
    Width = 431
    Height = 21
    DataField = 'consignee_contact'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 13
    Visible = True
  end
  object edDlvAddress: TDBEditEh [37]
    Left = 335
    Top = 280
    Width = 431
    Height = 21
    DataField = 'consignee_address'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 11
    Visible = True
  end
  object edDocNumber: TDBEditEh [38]
    Left = 8
    Top = 23
    Width = 147
    Height = 22
    DataField = 'doc_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object laEndPointConsignee: TDBSQLLookUp [39]
    Left = 335
    Top = 229
    Width = 393
    Height = 22
    DataField = 'endpoint_consignee_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 9
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
    OnKeyValueChange = laEndPointConsigneeKeyValueChange
  end
  object luShippingOptions: TDBSQLLookUp [40]
    Left = 8
    Top = 384
    Width = 308
    Height = 22
    ControlLabel.Width = 93
    ControlLabel.Height = 13
    ControlLabel.Caption = #1042#1072#1088#1080#1072#1085#1090' '#1076#1086#1089#1090#1072#1074#1082#1080
    ControlLabel.Visible = True
    DataField = 'shippingoption_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 15
    Visible = True
    SqlSet = ssShippingOption
    RowCount = 0
    OnKeyValueChange = luShippingOptionsKeyValueChange
  end
  object cbServices: TCheckListBox [41]
    Left = 335
    Top = 384
    Width = 432
    Height = 153
    OnClickCheck = cbServicesClickCheck
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ItemHeight = 13
    Items.Strings = (
      '')
    ParentFont = False
    TabOrder = 20
    OnClick = cbServicesClickCheck
  end
  object cbInWait: TDBCheckBoxEh [42]
    Left = 8
    Top = 432
    Width = 308
    Height = 17
    Caption = #1053#1072' '#1089#1086#1075#1083#1072#1089#1086#1074#1072#1085#1080#1080
    DataField = 'inwait'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 16
    OnClick = cbInWaitClick
  end
  object dtInWaitStop: TDBDateTimeEditEh [43]
    Left = 137
    Top = 465
    Width = 179
    Height = 21
    DataField = 'datestopinwait'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 17
    Visible = True
    OnChange = dtInWaitStopChange
  end
  inherited dsLocal: TDataSource
    Left = 476
    Top = 408
  end
  inherited qrAux: TADOQuery
    SQL.Strings = (
      'select * from servicekinds k where k.isextra=1 order by scode')
    Left = 332
    Top = 411
  end
  object ssContracts: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select c.*, cnum+'#39' '#1086#1090'  '#39'+convert(varchar, cdate, 104) as fullnam' +
        'e '
      
        'from contracts c where c.customer_id = :customerid order by cdat' +
        'e desc')
    DownSql.Strings = (
      
        'select c.*, cnum+'#39' '#1086#1090'  '#39'+convert(varchar, cdate, 104) as fullnam' +
        'e '
      
        'from contracts c where c.customer_id = :customerid order by cdat' +
        'e desc'
      '')
    InitSql.Strings = (
      
        'select c.*, cnum+'#39' '#1086#1090'  '#39'+convert(varchar, cdate, 104) as fullnam' +
        'e from contracts c where id = @id')
    KeyName = 'id'
    DisplayName = 'fullname'
    Connection = dm.connMain
    Left = 136
    Top = 261
  end
  object ssDirections: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select * from directions where code like '#39'%@pattern%'#39' or name li' +
        'ke '#39'%@pattern%'#39' order by code')
    DownSql.Strings = (
      'select * from directions order by code')
    InitSql.Strings = (
      'select * from directions where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 496
    Top = 161
  end
  object ssTarifType: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from tariftypes order by code')
    DownSql.Strings = (
      'select * from tariftypes order by code')
    InitSql.Strings = (
      'select * from tariftypes where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 136
    Top = 313
  end
  object ssDlvTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from deliverytypes d where exists '
      '('
      'select 1 from docroutes r, doctypes t '
      
        'where r.end_doctype_id = t.id and t.system_section = '#39'outcome_ap' +
        'ps'#39
      'and r.dlv_type_id = d.id'
      ')'
      'order by code')
    DownSql.Strings = (
      'select * from deliverytypes d where exists '
      '('
      'select 1 from docroutes r, doctypes t '
      
        'where r.end_doctype_id = t.id and t.system_section = '#39'outcome_ap' +
        'ps'#39
      'and r.dlv_type_id = d.id'
      ')'
      'order by code')
    InitSql.Strings = (
      'select * from deliverytypes where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 112
    Top = 65
  end
  object ssStations: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select * from stations where name like '#39'%@pattern%'#39' or code like' +
        ' '#39'%pattern%'#39' order by name')
    DownSql.Strings = (
      'select top 100 * from stations order by code')
    InitSql.Strings = (
      'select * from stations where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 496
    Top = 111
  end
  object ssShippingOption: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from shippingoptions order by code')
    DownSql.Strings = (
      'select * from shippingoptions order by code')
    InitSql.Strings = (
      'select * from shippingoptions where id = @id'
      '')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 162
    Top = 388
  end
  object qrAux2: TADOQuery
    Connection = dm.connMain
    Parameters = <
      item
        Name = 'station_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'direction_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'payer_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'extra_services'
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      
        'declare @station_code varchar(80), @direction_code varchar(80), ' +
        '@payer_code varchar(80), @extra_services varchar(450);'
      ''
      'select @station_code = isnull( :station_code,'#39#39'), '
      '@direction_code = isnull( :direction_code,'#39#39'), '
      '@payer_code = isnull( :payer_code,'#39#39'), '
      '@extra_services = isnull( :extra_services,'#39#39');'
      ''
      
        'select top 1 shor.shippingoption_id from shippingoptionrules sho' +
        'r'
      
        'where (charindex(lower(direction_code), lower(@direction_code))>' +
        '0 or isnull(ltrim(direction_code),'#39'*'#39')='#39'*'#39' or (@direction_code='#39 +
        #39' and direction_code='#39'#'#39') or (@direction_code<>'#39#39' and direction_' +
        'code='#39'!'#39'))'
      
        'and (charindex(lower(station_code), lower(@station_code))>0 or i' +
        'snull(ltrim(station_code),'#39'*'#39')='#39'*'#39' or (@station_code<>'#39#39' and sta' +
        'tion_code='#39'!'#39'))'
      
        'and (charindex(lower(payer_code), lower(@payer_code))>0 or isnul' +
        'l(ltrim(payer_code),'#39'*'#39')='#39'*'#39')'
      'and ('
      
        '   (exclude_sign=0 and not exists (select 1 from dbo.StringSplit' +
        '(extra_services,'#39','#39') where not Value in (select Value from dbo.S' +
        'tringSplit(@extra_services,'#39','#39')) ))'
      '   or '
      
        '   (exclude_sign=1 and not exists (select 1 from dbo.StringSplit' +
        '(extra_services,'#39','#39') where Value<>'#39#39' and Value in (select Value ' +
        'from dbo.StringSplit(@extra_services,'#39','#39')) ) )'
      ')'
      'order by priority')
    Left = 404
    Top = 419
  end
  object qrAux1: TADOQuery
    Connection = dm.connMain
    Parameters = <>
    SQL.Strings = (
      'select * from servicekinds k where k.isextra=1 order by scode')
    Left = 372
    Top = 475
  end
end
