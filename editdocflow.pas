﻿unit editdocflow;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, DBCtrlsEh, DBSQLLookUp, Data.DB, Vcl.Buttons, functions, DBGridEh,
  DBLookupEh;

type
  TFormEditDocFlow = class(TFormEdit)
    Label1: TLabel;
    dsLocal: TDataSource;
    Label2: TLabel;
    Label3: TLabel;
    luContainer: TDBLookupComboboxEh;
    sbContainer: TSpeedButton;
    sbCarriage: TSpeedButton;
    edSealNumber: TDBEditEh;
    Label4: TLabel;
    nuWeightSender: TDBNumberEditEh;
    Label5: TLabel;
    nuWeightDocument: TDBNumberEditEh;
    Label6: TLabel;
    nuWeightFact: TDBNumberEditEh;
    Label7: TLabel;
    Label8: TLabel;
    luConsignee: TDBLookupComboboxEh;
    SpeedButton3: TSpeedButton;
    Label9: TLabel;
    luContract: TDBLookupComboboxEh;
    SpeedButton4: TSpeedButton;
    sbTrain: TSpeedButton;
    luTrain: TDBLookupComboboxEh;
    stState: TDBEditEh;
    luCarriage: TDBLookupComboboxEh;
    procedure sbTrainClick(Sender: TObject);
    procedure sbContainerClick(Sender: TObject);
    procedure sbCarriageClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDocFlow: TFormEditDocFlow;

implementation

{$R *.dfm}

uses counteragents, trains, containers, Contracts, carriages;

procedure TFormEditDocFlow.sbContainerClick(Sender: TObject);
begin
  SFD(FormContainers, self.luContainer);
end;

procedure TFormEditDocFlow.sbCarriageClick(Sender: TObject);
begin
  SFD(FormCarriages, self.luCarriage);
end;

procedure TFormEditDocFlow.sbTrainClick(Sender: TObject);
begin
  SFD(FormTrains, self.luTrain);
end;

procedure TFormEditDocFlow.SpeedButton3Click(Sender: TObject);
begin
  SFD(FormCounteragents, self.luConsignee);
end;

procedure TFormEditDocFlow.SpeedButton4Click(Sender: TObject);
begin
  SFD(FormContracts, self.luContract);
end;

end.
