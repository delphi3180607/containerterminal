﻿unit containersinspect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, inspects, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls;

type
  TFormContainersInspects = class(TFormInspects)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormContainersInspects: TFormContainersInspects;

implementation

{$R *.dfm}

procedure TFormContainersInspects.FormCreate(Sender: TObject);
begin
  inherited;
  self.objecttype := 'container';
end;

end.
