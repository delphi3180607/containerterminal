﻿unit editdoctype;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls, DBSQLLookUp;

type
  TFormEditDocType = class(TFormEdit)
    edCode: TDBEditEh;
    Label2: TLabel;
    edName: TDBEditEh;
    Label3: TLabel;
    edProcedure: TDBEditEh;
    Label1: TLabel;
    Label4: TLabel;
    cbSection: TDBComboBoxEh;
    edGlobalSection: TDBEditEh;
    Label5: TLabel;
    ssDlvTypes: TADOLookUpSqlSet;
    Label16: TLabel;
    laDlvTypes: TDBSQLLookUp;
    cbAvoidUpdate: TDBCheckBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDocType: TFormEditDocType;

implementation

{$R *.dfm}

end.
