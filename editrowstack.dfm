﻿inherited FormEditRowStack: TFormEditRowStack
  Caption = #1056#1103#1076
  ClientHeight = 219
  ClientWidth = 452
  ExplicitWidth = 458
  ExplicitHeight = 247
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 178
    Width = 452
    TabOrder = 4
    ExplicitTop = 178
    ExplicitWidth = 452
    inherited btnCancel: TButton
      Left = 336
      ExplicitLeft = 336
    end
    inherited btnOk: TButton
      Left = 217
      ExplicitLeft = 217
    end
  end
  object cbActive: TDBCheckBoxEh [1]
    Left = 8
    Top = 136
    Width = 169
    Height = 17
    Caption = #1048#1089#1087#1086#1083#1100#1079#1091#1077#1090#1089#1103
    DataField = 'isactive'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 3
  end
  object neOrder: TDBNumberEditEh [2]
    Left = 8
    Top = 30
    Width = 121
    Height = 24
    ControlLabel.Width = 42
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088
    ControlLabel.Visible = True
    DataField = 'order_num'
    DataSource = dsLocal
    DynProps = <>
    Enabled = False
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
    Visible = True
  end
  object edNote: TDBEditEh [3]
    Left = 8
    Top = 86
    Width = 433
    Height = 24
    ControlLabel.Width = 82
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object cbNarrow: TDBCheckBoxEh [4]
    Left = 152
    Top = 34
    Width = 169
    Height = 17
    Caption = #1042' '#1086#1076#1085#1091' '#1096#1080#1088#1080#1085#1091
    DataField = 'isnarrow'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 1
  end
  inherited dsLocal: TDataSource
    Left = 256
    Top = 112
  end
  inherited qrAux: TADOQuery
    Left = 200
    Top = 112
  end
end
