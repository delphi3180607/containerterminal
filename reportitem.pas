﻿unit ReportItem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, MemTableDataEh, Data.DB,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, MemTableEh, EXLReportExcelTLB,
  EXLReportBand, EXLReport, Data.Win.ADODB;

type
  TFormReportItem = class(TForm)
    qrReport: TADOQuery;
    dsReport: TDataSource;
    exReport: TEXLReport;
    meReport: TMemTableEh;
    dpReports: TDBGridEh;
    qrAux: TADOQuery;
  private
    { Private declarations }
  public
    procedure MakeGroupStructure(group_fields: string);
    procedure GenerateColumns;
    procedure FormatColumns;
    procedure MakeReport;
  end;

var
  FormReportItem: TFormReportItem;

implementation

{$R *.dfm}

uses ParamsFormUnit, reports;

procedure TFormReportItem.MakeReport;
begin
  qrReport.Close;
  meReport.Close;
  FormReports.sbUnwrap.Hide;
  FormReports.sbWrap.Hide;
  dpReports.Columns.Clear;
  meReport.Filter := '';
  dpReports.ClearFilter;
  dpReports.DataGrouping.GroupLevels.Clear;
  dpReports.DataGrouping.Footers.Clear;
  dpReports.DataGrouping.Active := false;
  dpReports.OddRowColor := $00F0F4F4;
  dpReports.GridLineParams.DataHorzColor := $00DEE2E2;
  dpReports.GridLineParams.DataVertColor := $00DEE2E2;
  FormReports.FillParamsForm();
  if FormReports.GotParams() then
  begin
    qrReport.SQL.Text := FormReports.meData.FieldByName('sql_text').AsString;
    FormReports.AssignParameters(ParamsForm.meLocal);
    try
      GenerateColumns;
      FormReports.plMessage.Show;
      Application.ProcessMessages;
      Screen.Cursor := crHourGlass;
      qrReport.Open;
      meReport.Close;
      meReport.LoadFromDataSet(qrReport, 0, lmCopy, false);
      FormatColumns;

      meReport.TreeList.Active := false;
      meReport.TreeList.RefParentFieldName := '';
      meReport.TreeList.KeyFieldName := '';

      if (FormReports.meData.FieldByName('tree_parent_id_name').AsString<>'')
      and (FormReports.meData.FieldByName('tree_child_id_name').AsString<>'')
      then begin
        meReport.TreeList.RefParentFieldName := FormReports.meData.FieldByName('tree_parent_id_name').AsString;
        meReport.TreeList.KeyFieldName := FormReports.meData.FieldByName('tree_child_id_name').AsString;
        meReport.TreeList.Active := true;
      end;

      meReport.Open;

      if (FormReports.meData.FieldByName('group_fields').AsString<>'')
      then begin
        MakeGroupStructure(FormReports.meData.FieldByName('group_fields').AsString);
      end;

      dpReports.Show;
      FormReports.btExport.Enabled := true;
      FormReports.sbClose.Enabled := true;
    except
      on E : Exception do
      begin
        FormReports.plMessage.Hide;
        Screen.Cursor := crDefault;
        ShowMessage('Ошибка при выполнении запроса: '+E.Message);
      end;
    end;

    FormReports.plMessage.Hide;
    Screen.Cursor := crDefault;

  end;
end;


procedure TFormReportItem.GenerateColumns;
var c: TColumnEh;
begin

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select * from report_fields where report_id = :report_id order by field_order');
  qrAux.Parameters.ParamByName('report_id').Value := FormReports.meData.FieldByName('id').AsInteger;
  qrAux.Open;

  dpReports.Columns.Clear;

  while not qrAux.Eof do
  begin

    c := dpReports.Columns.Add;

    c.Title.Caption := qrAux.FieldByName('field_caption').AsString;
    c.FieldName := qrAux.FieldByName('field_name').AsString;
    c.Width := qrAux.FieldByName('field_width').AsInteger;
    c.Title.TitleButton := true;
    c.TextEditing := true;

    if qrAux.FieldByName('field_align').AsInteger>0 then
      c.Alignment := TAlignment(qrAux.FieldByName('field_align').AsInteger - 1);

    qrAux.Next;

  end;

end;


procedure TFormReportItem.FormatColumns;
var i: integer; c: TColumnEh;
begin

   for i := 0 to dpReports.Columns.Count-1 do
   begin

    c := dpReports.Columns[i];

    if c.Field.DataType = ftFloat then
    begin
      c.DisplayFormat := '### ### ##0.00';
    end;


   end;

end;


procedure TFormReportItem.MakeGroupStructure(group_fields: string);
var sl: TStringList; grp1, grp2: string;

procedure MakeGroupLevel(fieldname: string);
var gl1: TGridDataGroupLevelEh;
begin

    gl1 := dpReports.DataGrouping.GroupLevels.Add;
    gl1.Column := dpReports.FindFieldColumn(fieldname);
    gl1.Color := $00DCEDED;
    gl1.Font.Style := [fsBold];

    dpReports.OddRowColor := clWindow;
    dpReports.EvenRowColor := clWindow;

    dpReports.GridLineParams.DataHorzColor := clSilver;
    dpReports.GridLineParams.DataVertColor := clSilver;

    FormReports.sbUnwrap.Show;
    FormReports.sbWrap.Show;

end;

procedure MakeFooter;
var ft1: TGridDataGroupFooterEh; i, j: integer;
begin

    ft1 := dpReports.DataGrouping.Footers.Add;
    ft1.Color := $00DCEDED;
    ft1.Font.Style := [fsBold];

    qrAux.First;

    while not qrAux.Eof do
    begin
      if qrAux.FieldByName('is_total').AsBoolean then
      begin

        i := ft1.ColumnItems.ItemIndexByColumn(dpReports.FindFieldColumn(qrAux.FieldByName('field_name').AsString));

        if meReport.FieldByName(qrAux.FieldByName('field_name').AsString).DataType = ftString then
          ft1.ColumnItems[i].ValueType := gfvCountEh
        else
          ft1.ColumnItems[i].ValueType := gfvSumEh;

      end;
      qrAux.Next;
    end;

    ft1.Visible := true;

end;


begin

  sl := TStringList.Create;
  sl.Delimiter := ',';
  sl.StrictDelimiter := true;
  sl.DelimitedText := FormReports.meData.FieldByName('group_fields').AsString;

  grp1 := '';
  grp2 := '';

  if sl.Count>0 then grp1 := sl[0];
  if sl.Count>1 then grp2 := sl[1];

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select * from report_fields where report_id = :report_id order by field_order');
  qrAux.Parameters.ParamByName('report_id').Value := FormReports.meData.FieldByName('id').AsInteger;
  qrAux.Open;

  dpReports.DataGrouping.Active := true;
  dpReports.DataGrouping.GroupPanelVisible := false;

  if grp1<>'' then
  begin
    MakeGroupLevel(grp1);
    MakeFooter;
  end;

  if grp2<>'' then
  begin
    MakeGroupLevel(grp2);
  end;

end;


end.
