﻿unit editowner;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEh, Vcl.Buttons, functions,
  Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, DBLookupEh, Data.DB, Vcl.ExtCtrls,
  Data.Win.ADODB;

type
  TFormEditOwner = class(TFormEdit)
    luCounteragent: TDBLookupComboboxEh;
    Label8: TLabel;
    sbCounteragent: TSpeedButton;
    dtChange: TDBDateTimeEditEh;
    Label1: TLabel;
    procedure sbCounteragentClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditOwner: TFormEditOwner;

implementation

{$R *.dfm}

uses counteragents;

procedure TFormEditOwner.sbCounteragentClick(Sender: TObject);
begin
  SFD(FormCounteragents,self.luCounteragent);
end;

end.
