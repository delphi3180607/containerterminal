﻿inherited FormDocLinkedInvoices: TFormDocLinkedInvoices
  Caption = #1057#1095#1077#1090#1072
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 75
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 66
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'object_num'
          Footers = <>
          Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
          Width = 123
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'service_name'
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1091#1089#1083#1091#1075#1080
          Width = 170
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'customer_name'
          Footers = <>
          Title.Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
          Width = 207
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'tarif_code'
          Footers = <>
          Title.Caption = #1058#1072#1088#1080#1092
          Width = 102
        end
        item
          Alignment = taRightJustify
          CellButtons = <>
          DisplayFormat = '### ### ##0.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'summa'
          Footers = <>
          Title.Caption = #1057#1091#1084#1084#1072
          Width = 109
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'declare @docid int;'
      ''
      'select @docid = :doc_id;'
      ''
      'select s.*, '
      
        '(select name from counteragents c where c.id = s.customer_id) as' +
        ' customer_name,'
      
        '(select sname from servicekinds k where k.id = s.service_id) as ' +
        'service_name,'
      
        '(select code from tariftypes t where t.id = s.tarif_id) as tarif' +
        '_code,'
      
        '(select cnum from v_objects where id = s.object_id) as object_nu' +
        'm '
      'from doccargoservices s '
      
        'where (doc_id = @docid or task_id in (select task_id from object' +
        's2docspec where doc_id = @docid))'
      'order by s.doc_date')
    SelectCommand.Parameters = <
      item
        Name = 'doc_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update doccargoservices'
      'set'
      '  doc_number = :doc_number,'
      '  doc_date = :doc_date,'
      '  service_id = :service_id,'
      '  customer_id = :customer_id,'
      '  tarif_id = :tarif_id,'
      '  summa = :summa'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'service_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'customer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'tarif_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'summa'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into doccargoservices'
      
        '  (doc_id, doc_number, doc_date, service_id, customer_id, tarif_' +
        'id, summa)'
      'values'
      
        '  (:doc_id, :doc_number, :doc_date, :service_id, :customer_id, :' +
        'tarif_id, '
      '   :summa)')
    InsertCommand.Parameters = <
      item
        Name = 'doc_id'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'service_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'customer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'tarif_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'summa'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from doccargoservices where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select s.*, '
      
        '(select name from counteragents c where c.id = s.customer_id) as' +
        ' customer_name,'
      
        '(select sname from servicekinds k where k.id = s.service_id) as ' +
        'service_name,'
      
        '(select code from tariftypes t where t.id = s.tarif_id) as tarif' +
        '_code,'
      
        '(select cnum from v_objects where id = s.object_id) as object_nu' +
        'm '
      'from doccargoservices s where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
end
