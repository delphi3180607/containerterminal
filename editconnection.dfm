﻿inherited FormEditConnection: TFormEditConnection
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103' '#1082' '#1041#1044
  ClientHeight = 286
  ClientWidth = 419
  Position = poScreenCenter
  ExplicitWidth = 425
  ExplicitHeight = 314
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 245
    Width = 419
    TabOrder = 4
    inherited btnCancel: TButton
      Left = 303
    end
    inherited btnOk: TButton
      Left = 184
    end
  end
  object edServer: TDBEditEh [1]
    Left = 16
    Top = 32
    Width = 385
    Height = 24
    ControlLabel.Width = 49
    ControlLabel.Height = 16
    ControlLabel.Caption = #1057#1077#1088#1074#1077#1088
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object edLogin: TDBEditEh [2]
    Left = 16
    Top = 88
    Width = 385
    Height = 24
    ControlLabel.Width = 40
    ControlLabel.Height = 16
    ControlLabel.Caption = #1051#1086#1075#1080#1085
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edPassword: TDBEditEh [3]
    Left = 16
    Top = 144
    Width = 385
    Height = 24
    ControlLabel.Width = 48
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1072#1088#1086#1083#1100
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object edDataBase: TDBEditEh [4]
    Left = 16
    Top = 200
    Width = 385
    Height = 24
    ControlLabel.Width = 84
    ControlLabel.Height = 16
    ControlLabel.Caption = #1041#1072#1079#1072' '#1076#1072#1085#1085#1099#1093
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 16
    Top = 232
  end
  inherited qrAux: TADOQuery
    Left = 56
    Top = 232
  end
end
