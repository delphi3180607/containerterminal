﻿unit editmatrixsector;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask;

type
  TFormEditMatrixSector = class(TFormEdit)
    edCode: TDBEditEh;
    edName: TDBEditEh;
    neOrder: TDBNumberEditEh;
    edRows: TDBNumberEditEh;
    edStaks: TDBNumberEditEh;
    edLevels: TDBNumberEditEh;
    cbIsActive: TDBCheckBoxEh;
    cbRowOrder: TDBCheckBoxEh;
    cbLevelOrder: TDBCheckBoxEh;
    cbStackOrder: TDBCheckBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditMatrixSector: TFormEditMatrixSector;

implementation

{$R *.dfm}

end.
