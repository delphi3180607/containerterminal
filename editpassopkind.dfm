﻿inherited FormEditPassOpKind: TFormEditPassOpKind
  Caption = #1042#1080#1076' '#1086#1087#1077#1088#1072#1094#1080#1080' '#1087#1086' '#1087#1088#1086#1087#1091#1089#1082#1072#1084
  ClientHeight = 432
  ClientWidth = 546
  ExplicitWidth = 552
  ExplicitHeight = 460
  PixelsPerInch = 96
  TextHeight = 16
  inherited Label1: TLabel
    Top = 9
    ExplicitTop = 9
  end
  inherited Label2: TLabel
    Top = 127
    ExplicitTop = 127
  end
  object Label3: TLabel [2]
    Left = 264
    Top = 9
    Width = 106
    Height = 16
    Caption = #1050#1086#1076' '#1076#1083#1103' '#1086#1093#1088#1072#1085#1099
  end
  object Label4: TLabel [3]
    Left = 8
    Top = 195
    Width = 201
    Height = 16
    Caption = #1058#1080#1087' '#1089#1086#1079#1076#1072#1074#1072#1077#1084#1086#1075#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object Label5: TLabel [4]
    Left = 8
    Top = 257
    Width = 126
    Height = 16
    Caption = #1057#1080#1089#1090#1077#1084#1085#1099#1081' '#1088#1072#1079#1076#1077#1083
  end
  object Label6: TLabel [5]
    Left = 8
    Top = 65
    Width = 89
    Height = 16
    Caption = #1053#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077
  end
  object Label16: TLabel [6]
    Left = 8
    Top = 327
    Width = 166
    Height = 16
    Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080'/'#1054#1087#1077#1088#1072#1094#1080#1103
  end
  inherited plBottom: TPanel
    Top = 391
    Width = 546
    TabOrder = 7
    ExplicitTop = 325
    ExplicitWidth = 545
    inherited btnCancel: TButton
      Left = 430
      ExplicitLeft = 429
    end
    inherited btnOk: TButton
      Left = 311
      ExplicitLeft = 310
    end
  end
  inherited edCode: TDBEditEh
    Top = 31
    ControlLabel.ExplicitTop = 15
    ExplicitTop = 31
  end
  inherited edName: TDBEditEh
    Top = 149
    Width = 525
    ControlLabel.ExplicitTop = 133
    TabOrder = 3
    ExplicitTop = 149
    ExplicitWidth = 525
  end
  object edCodeShort: TDBEditEh [10]
    Left = 264
    Top = 31
    Width = 269
    Height = 24
    DataField = 'short_code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object leEndDocType: TDBSQLLookUp [11]
    Left = 8
    Top = 217
    Width = 505
    Height = 24
    DataField = 'end_doctype_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = True
    SqlSet = ssDocTypes
    RowCount = 0
  end
  object cbSection: TDBComboBoxEh [12]
    Left = 8
    Top = 279
    Width = 505
    Height = 24
    DataField = 'system_section'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Items.Strings = (
      'income_sheet'
      'income_unload'
      'income_self_removal'
      'income_car_removal'
      'income_extra_service'
      'income_empty'
      'outcome_apps'
      'outcome_empty'
      'outcome_load'
      'outcome_dispatch'
      '')
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
  object cbDirection: TDBComboBoxEh [13]
    Left = 8
    Top = 87
    Width = 525
    Height = 24
    DataField = 'direction'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Items.Strings = (
      #1047#1072#1074#1086#1079
      #1042#1099#1074#1086#1079)
    KeyItems.Strings = (
      '0'
      '1')
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object laDlvTypes: TDBSQLLookUp [14]
    Left = 8
    Top = 347
    Width = 308
    Height = 22
    Color = clWhite
    DataField = 'end_dlv_type_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 6
    Visible = True
    SqlSet = ssDlvTypes
    RowCount = 0
  end
  inherited dsLocal: TDataSource
    Left = 48
    Top = 401
  end
  inherited qrAux: TADOQuery
    Left = 104
    Top = 404
  end
  object ssDocTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select * from doctypes where system_section = '#39'income_empty'#39' ord' +
        'er by code')
    DownSql.Strings = (
      'select * from doctypes where system_section = '#39'income_empty'#39' ')
    InitSql.Strings = (
      'select * from doctypes where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 186
    Top = 201
  end
  object ssDlvTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from deliverytypes d where exists '
      '('
      'select 1 from docroutes r, doctypes t '
      
        'where r.end_doctype_id = t.id and t.system_section = '#39'outcome_ap' +
        'ps'#39
      'and r.dlv_type_id = d.id'
      ')'
      'order by code')
    DownSql.Strings = (
      'select * from deliverytypes d where exists '
      '('
      'select 1 from docroutes r, doctypes t '
      
        'where r.end_doctype_id = t.id and t.system_section = '#39'outcome_ap' +
        'ps'#39
      'and r.dlv_type_id = d.id'
      ')'
      'order by code')
    InitSql.Strings = (
      'select * from deliverytypes where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 112
    Top = 337
  end
end
