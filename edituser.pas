﻿unit edituser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  DBCtrlsEh, Vcl.Mask, Data.DB, Data.Win.ADODB, DBSQLLookUp;

type
  TFormEditUser = class(TFormEdit)
    edLogin: TDBEditEh;
    edPassword: TDBEditEh;
    edName: TDBEditEh;
    Label5: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    cbIsAdmin: TDBCheckBoxEh;
    Label1: TLabel;
    edPost: TDBEditEh;
    Label6: TLabel;
    edContact: TDBEditEh;
    edShortName: TDBEditEh;
    Label7: TLabel;
    cbIsBlocked: TDBCheckBoxEh;
    cbIsSupervisor: TDBCheckBoxEh;
    ssRoles: TADOLookUpSqlSet;
    luRole: TDBSQLLookUp;
    procedure cbIsAdminClick(Sender: TObject);
  private
    flag: boolean;
  public
    { Public declarations }
  end;

var
  FormEditUser: TFormEditUser;

implementation

{$R *.dfm}

procedure TFormEditUser.cbIsAdminClick(Sender: TObject);
begin
  if flag then exit;
  flag := true;
  if cbIsAdmin.Checked then
  begin
    self.luRole.KeyValue := null;
  end;
  self.luRole.Enabled := not cbIsAdmin.Checked;
  flag := false;
end;

end.
