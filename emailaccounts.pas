﻿unit emailaccounts;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand, EXLReport;

type
  TFormEmailAccounts = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormEmailAccounts: TFormEmailAccounts;

implementation

{$R *.dfm}

uses editemailaccount;


procedure TFormEmailAccounts.Init;
begin
  inherited;
  tablename := 'emailaccounts';
  self.formEdit := FormEditEmailAccount;
end;

end.
