﻿inherited FormEditUser: TFormEditUser
  Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
  ClientHeight = 494
  ClientWidth = 543
  ExplicitWidth = 549
  ExplicitHeight = 522
  PixelsPerInch = 96
  TextHeight = 16
  object Label5: TLabel [0]
    Left = 15
    Top = 190
    Width = 31
    Height = 16
    Caption = #1056#1086#1083#1100
  end
  object Label4: TLabel [1]
    Left = 15
    Top = 126
    Width = 48
    Height = 16
    Caption = #1055#1072#1088#1086#1083#1100
  end
  object Label3: TLabel [2]
    Left = 15
    Top = 69
    Width = 26
    Height = 16
    Caption = #1048#1084#1103
  end
  object Label2: TLabel [3]
    Left = 15
    Top = 14
    Width = 40
    Height = 16
    Caption = #1051#1086#1075#1080#1085
  end
  object Label1: TLabel [4]
    Left = 15
    Top = 254
    Width = 75
    Height = 16
    Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
  end
  object Label6: TLabel [5]
    Left = 15
    Top = 384
    Width = 63
    Height = 16
    Caption = #1050#1086#1085#1090#1072#1082#1090#1099
  end
  object Label7: TLabel [6]
    Left = 15
    Top = 318
    Width = 78
    Height = 16
    Caption = #1048#1084#1103' '#1082#1088#1072#1090#1082#1086
  end
  inherited plBottom: TPanel
    Top = 453
    Width = 543
    TabOrder = 9
    ExplicitTop = 453
    ExplicitWidth = 543
    inherited btnCancel: TButton
      Left = 427
      ExplicitLeft = 427
    end
    inherited btnOk: TButton
      Left = 308
      ExplicitLeft = 308
    end
  end
  object edLogin: TDBEditEh [8]
    Left = 15
    Top = 32
    Width = 305
    Height = 24
    DataField = 'user_login'
    DataSource = FormUsers.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object edPassword: TDBEditEh [9]
    Left = 15
    Top = 144
    Width = 305
    Height = 24
    DataField = 'password'
    DataSource = FormUsers.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object edName: TDBEditEh [10]
    Left = 15
    Top = 88
    Width = 505
    Height = 24
    DataField = 'user_name'
    DataSource = FormUsers.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object cbIsAdmin: TDBCheckBoxEh [11]
    Left = 352
    Top = 208
    Width = 168
    Height = 17
    Caption = #1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1086#1088
    DataField = 'isAdmin'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 4
    OnClick = cbIsAdminClick
  end
  object edPost: TDBEditEh [12]
    Left = 15
    Top = 276
    Width = 505
    Height = 24
    DataField = 'post_name'
    DataSource = FormUsers.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 6
    Visible = True
  end
  object edContact: TDBEditEh [13]
    Left = 15
    Top = 406
    Width = 505
    Height = 24
    DataField = 'contact'
    DataSource = FormUsers.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 8
    Visible = True
  end
  object edShortName: TDBEditEh [14]
    Left = 15
    Top = 340
    Width = 505
    Height = 24
    DataField = 'short_name'
    DataSource = FormUsers.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 7
    Visible = True
  end
  object cbIsBlocked: TDBCheckBoxEh [15]
    Left = 352
    Top = 148
    Width = 168
    Height = 17
    Caption = #1047#1072#1073#1083#1086#1082#1080#1088#1086#1074#1072#1085'?'
    DataField = 'isblocked'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 3
    OnClick = cbIsAdminClick
  end
  object cbIsSupervisor: TDBCheckBoxEh [16]
    Left = 352
    Top = 240
    Width = 168
    Height = 17
    Caption = #1057#1091#1087#1077#1088#1074#1080#1079#1086#1088
    DataField = 'issupervisor'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 5
    OnClick = cbIsAdminClick
  end
  object luRole: TDBSQLLookUp [17]
    Left = 15
    Top = 212
    Width = 305
    Height = 24
    DataField = 'user_role'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 10
    Visible = True
    SqlSet = ssRoles
    KeyValue = Null
  end
  inherited dsLocal: TDataSource
    Left = 248
    Top = 72
  end
  inherited qrAux: TADOQuery
    Left = 176
    Top = 96
  end
  object ssRoles: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from roles order by name')
    DownSql.Strings = (
      'select * from roles order by name')
    InitSql.Strings = (
      'select * from roles where id = @id')
    KeyName = 'id'
    DisplayName = 'Name'
    Connection = dm.connMain
    Left = 216
    Top = 208
  end
end
