﻿inherited FormDispatchMonitor: TFormDispatchMonitor
  Caption = #1052#1086#1085#1080#1090#1086#1088' '#1086#1090#1087#1088#1072#1074#1086#1082
  ClientHeight = 581
  ClientWidth = 1296
  ExplicitWidth = 1312
  ExplicitHeight = 619
  PixelsPerInch = 96
  TextHeight = 13
  object SplitterHor: TSplitter [0]
    Left = 0
    Top = 212
    Width = 1296
    Height = 4
    Cursor = crVSplit
    Align = alBottom
    Color = clBtnFace
    ParentColor = False
    ExplicitTop = 211
  end
  inherited plBottom: TPanel
    Top = 540
    Width = 1296
    ExplicitTop = 540
    ExplicitWidth = 1296
    inherited btnOk: TButton
      Left = 1061
      ExplicitLeft = 1061
    end
    inherited btnCancel: TButton
      Left = 1180
      ExplicitLeft = 1180
    end
  end
  inherited plAll: TPanel
    Width = 1296
    Height = 212
    ExplicitWidth = 1296
    ExplicitHeight = 212
    inherited plTop: TPanel
      Width = 1296
      Height = 44
      ExplicitWidth = 1296
      ExplicitHeight = 44
      inherited sbDelete: TPngSpeedButton
        Left = 1127
        Height = 42
        Enabled = False
        Visible = False
        ExplicitLeft = 863
        ExplicitHeight = 44
      end
      inherited btFilter: TPngSpeedButton
        Left = 1237
        Height = 42
        Visible = False
        ExplicitLeft = 1226
        ExplicitTop = -3
        ExplicitHeight = 43
      end
      inherited btExcel: TPngSpeedButton
        Left = 1205
        Height = 42
        ExplicitLeft = 936
        ExplicitHeight = 43
      end
      inherited sbAdd: TPngSpeedButton
        Left = 1095
        Height = 42
        Enabled = False
        Visible = False
        ExplicitLeft = 832
        ExplicitHeight = 44
      end
      inherited sbEdit: TPngSpeedButton
        Left = 1159
        Height = 42
        Enabled = False
        Visible = False
        ExplicitLeft = 895
        ExplicitHeight = 44
      end
      inherited Bevel2: TBevel
        Left = 1201
        Height = 44
        ExplicitLeft = 931
        ExplicitHeight = 44
      end
      inherited btTool: TPngSpeedButton
        Left = 1256
        Width = 37
        Height = 38
        ExplicitLeft = 1256
        ExplicitWidth = 37
        ExplicitHeight = 38
      end
      inherited plCount: TPanel
        Left = 1059
        Height = 44
        TabOrder = 7
        ExplicitLeft = 1059
        ExplicitHeight = 44
        inherited edCount: TDBEditEh
          Height = 38
          ExplicitHeight = 38
        end
      end
      object Panel3: TPanel
        Left = 115
        Top = 0
        Width = 189
        Height = 44
        Align = alLeft
        TabOrder = 0
        object luCustomer: TDBSQLLookUp
          AlignWithMargins = True
          Left = 4
          Top = 18
          Width = 181
          Height = 22
          Align = alBottom
          ControlLabel.Width = 60
          ControlLabel.Height = 13
          ControlLabel.Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
          ControlLabel.Visible = True
          DynProps = <>
          EditButtons = <
            item
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          Visible = True
          OnKeyDown = edCnumKeyDown
          SqlSet = dm.ssCounteragents
          RowCount = 0
        end
      end
      object Panel1: TPanel
        Left = 304
        Top = 0
        Width = 144
        Height = 44
        Align = alLeft
        TabOrder = 1
        object edCnum: TDBEditEh
          AlignWithMargins = True
          Left = 4
          Top = 19
          Width = 136
          Height = 21
          Align = alBottom
          ControlLabel.Width = 131
          ControlLabel.Height = 13
          ControlLabel.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088'('#1095#1072#1089#1090#1100' '#1085#1086#1084#1077#1088#1072')'
          ControlLabel.Visible = True
          DynProps = <>
          EditButtons = <>
          TabOrder = 0
          Visible = True
          OnKeyDown = edCnumKeyDown
        end
      end
      object Panel2: TPanel
        Left = 448
        Top = 0
        Width = 119
        Height = 44
        Align = alLeft
        TabOrder = 2
        object edPnum: TDBEditEh
          AlignWithMargins = True
          Left = 4
          Top = 19
          Width = 111
          Height = 21
          Align = alBottom
          ControlLabel.Width = 105
          ControlLabel.Height = 13
          ControlLabel.Caption = #1042#1072#1075#1086#1085'('#1095#1072#1089#1090#1100' '#1085#1086#1084#1077#1088#1072')'
          ControlLabel.Visible = True
          DynProps = <>
          EditButtons = <>
          TabOrder = 0
          Visible = True
          OnKeyDown = edCnumKeyDown
        end
      end
      object Panel4: TPanel
        Left = 825
        Top = 0
        Width = 144
        Height = 44
        Align = alLeft
        TabOrder = 3
        object edDispatchNumber: TDBEditEh
          AlignWithMargins = True
          Left = 4
          Top = 19
          Width = 136
          Height = 21
          Align = alBottom
          ControlLabel.Width = 126
          ControlLabel.Height = 13
          ControlLabel.Caption = #1054#1090#1087#1088#1072#1074#1082#1072'('#1095#1072#1089#1090#1100' '#1085#1086#1084#1077#1088#1072')'
          ControlLabel.Visible = True
          DynProps = <>
          EditButtons = <>
          TabOrder = 0
          Visible = True
          OnKeyDown = edCnumKeyDown
        end
      end
      object Panel5: TPanel
        Left = 696
        Top = 0
        Width = 129
        Height = 44
        Align = alLeft
        TabOrder = 4
        object edSealNumber: TDBEditEh
          AlignWithMargins = True
          Left = 4
          Top = 19
          Width = 121
          Height = 21
          Align = alBottom
          ControlLabel.Width = 113
          ControlLabel.Height = 13
          ControlLabel.Caption = #1055#1083#1086#1084#1073#1072'('#1095#1072#1089#1090#1100' '#1085#1086#1084#1077#1088#1072')'
          ControlLabel.Visible = True
          DynProps = <>
          EditButtons = <>
          TabOrder = 0
          Visible = True
          OnKeyDown = edCnumKeyDown
        end
      end
      object Panel6: TPanel
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 105
        Height = 44
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 10
        Margins.Bottom = 0
        Align = alLeft
        TabOrder = 5
        object dtStart: TDBDateTimeEditEh
          AlignWithMargins = True
          Left = 4
          Top = 19
          Width = 97
          Height = 21
          ControlLabel.Width = 81
          ControlLabel.Height = 13
          ControlLabel.Caption = #1053#1072#1095#1080#1085#1072#1103' '#1089' '#1076#1072#1090#1099
          ControlLabel.Visible = True
          Align = alBottom
          DynProps = <>
          EditButtons = <>
          Kind = dtkDateEh
          TabOrder = 0
          Visible = True
        end
      end
      object Panel7: TPanel
        Left = 567
        Top = 0
        Width = 129
        Height = 44
        Align = alLeft
        TabOrder = 6
        object edGU12: TDBEditEh
          AlignWithMargins = True
          Left = 4
          Top = 19
          Width = 121
          Height = 21
          Align = alBottom
          ControlLabel.Width = 108
          ControlLabel.Height = 13
          ControlLabel.Caption = #1043#1059'-12 ('#1095#1072#1089#1090#1100' '#1085#1086#1084#1077#1088#1072')'
          ControlLabel.Visible = True
          DynProps = <>
          EditButtons = <>
          TabOrder = 0
          Visible = True
          OnKeyDown = edCnumKeyDown
        end
      end
      object sbRun: TButton
        AlignWithMargins = True
        Left = 971
        Top = 2
        Width = 121
        Height = 40
        Margins.Left = 2
        Margins.Top = 2
        Margins.Bottom = 2
        Align = alLeft
        Caption = #1055#1086#1083#1091#1095#1080#1090#1100' '#1086#1090#1095#1077#1090
        TabOrder = 8
        OnClick = sbRunClick
      end
    end
    inherited dgData: TDBGridEh
      Top = 68
      Width = 1296
      Height = 144
      Font.Height = -13
      Font.Name = 'Calibri'
      GridLineParams.DataHorzColor = clSkyBlue
      GridLineParams.DataVertColor = clSkyBlue
      OptionsEh = [dghHighlightFocus, dghAutoSortMarking, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      TitleParams.Font.Height = -12
      TitleParams.Font.Name = 'Calibri'
      TitleParams.MultiTitle = True
      OnMouseDown = nil
      OnMouseMove = nil
      OnMouseUp = nil
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_factexecution'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 119
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Footers = <>
          Title.Caption = #8470' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
          Width = 89
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          Title.Caption = #1058#1080#1087
          Width = 39
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          Color = clSilver
          DynProps = <>
          EditButtons = <>
          FieldName = 'isempty'
          Footers = <>
          Title.Caption = #1055#1086#1088'. ?'
          Width = 35
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_fact'
          Footers = <>
          Title.Caption = #1042#1077#1089' '#1073#1088#1091#1090#1090#1086
          Width = 53
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'pnum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Footers = <>
          Title.Caption = #8470' '#1074#1072#1075#1086#1085#1072
          Width = 77
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Color = 7138631
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 92
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'forwarder_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          Width = 126
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 103
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer_code'
          Footers = <>
          Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
          Width = 108
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'station_name'
          Footers = <>
          Title.Caption = #1057#1090#1072#1085#1094#1080#1103
          Width = 162
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          Color = 9540095
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_serve'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 5578348
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Footers = <>
          Title.Caption = #1044#1040#1058#1040' '#1054#1058#1055#1056#1040#1042#1050#1048
          Width = 99
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dispatch_numbers'
          Footers = <>
          Title.Caption = #8470' '#1086#1090#1087#1088#1072#1074#1082#1080
          Width = 74
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cargotype_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1075#1088#1091#1079#1072
          Width = 74
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cargotype_name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1075#1088#1091#1079#1072
          Width = 104
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'seal_number'
          Footers = <>
          Title.Caption = #8470' '#1087#1083#1086#1084#1073#1099
          Width = 73
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'external_order_num'
          Footers = <>
          Title.Caption = #8470' '#1043#1059'-12'
          Width = 74
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'receipt_summa'
          Footers = <>
          Title.Caption = #1057#1091#1084#1084#1072' '#1082#1074#1080#1090#1072#1085#1094#1080#1080
          Width = 89
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'docorder_text'
          Footers = <>
          Title.Caption = #1047#1072#1103#1074#1082#1072
          Width = 144
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object Panel9: TPanel
      Left = 0
      Top = 44
      Width = 1296
      Height = 24
      Align = alTop
      BevelOuter = bvNone
      Caption = #1050#1086#1085#1077#1095#1085#1099#1081' '#1089#1090#1072#1090#1091#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
  end
  object dgHistory: TDBGridEh [3]
    Left = 0
    Top = 240
    Width = 1296
    Height = 300
    Align = alBottom
    AllowedOperations = []
    Border.Color = clSilver
    ColumnDefValues.Title.TitleButton = True
    ColumnDefValues.ToolTips = True
    Ctl3D = False
    DataSource = dsHistory
    DynProps = <>
    FixedColor = clCream
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    GridLineParams.BrightColor = clSilver
    GridLineParams.ColorScheme = glcsClassicEh
    GridLineParams.DarkColor = clSilver
    GridLineParams.DataBoundaryColor = clSilver
    GridLineParams.DataHorzColor = clSkyBlue
    GridLineParams.DataVertColor = clSkyBlue
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    IndicatorParams.Color = clBtnFace
    IndicatorParams.HorzLineColor = clSilver
    IndicatorParams.VertLineColor = clSilver
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghHighlightFocus, dghAutoSortMarking, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SearchPanel.Enabled = True
    SearchPanel.PersistentShowing = False
    SelectionDrawParams.SelectionStyle = gsdsClassicEh
    SelectionDrawParams.DrawFocusFrame = False
    SelectionDrawParams.DrawFocusFrameStored = True
    ShowHint = True
    SortLocal = True
    STFilter.Color = clWhite
    STFilter.HorzLineColor = clSilver
    STFilter.InstantApply = True
    STFilter.Local = True
    STFilter.VertLineColor = clSilver
    TabOrder = 2
    TitleParams.Color = clBtnFace
    TitleParams.FillStyle = cfstThemedEh
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -11
    TitleParams.Font.Name = 'Verdana'
    TitleParams.Font.Style = [fsBold]
    TitleParams.HorzLineColor = clSilver
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    TitleParams.SecondColor = clCream
    TitleParams.VertLineColor = clSilver
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'delivery_code'
        Footers = <>
        HideDuplicates = True
        Title.Caption = #1058#1080#1087' '#1086#1087#1077#1088#1072#1094#1080#1080
        Width = 113
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'state_code'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Footers = <>
        Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
        Width = 132
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'date_factexecution'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072' '#1087#1088#1086#1074#1086#1076#1082#1080
        Width = 160
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'doc_code'
        Footers = <>
        Title.Caption = #1044#1086#1082#1091#1084#1077#1085#1090
        Visible = False
        Width = 185
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'forwarder_code'
        Footers = <>
        HideDuplicates = True
        Title.Caption = #1050#1083#1080#1077#1085#1090
        Width = 163
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'payer_code'
        Footers = <>
        HideDuplicates = True
        Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
        Width = 146
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'consignee_code'
        Footers = <>
        HideDuplicates = True
        Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
        Width = 158
      end
      item
        Alignment = taCenter
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'train_num'
        Footers = <>
        Title.Caption = #1050#1055
        Width = 64
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'station_code'
        Footers = <>
        HideDuplicates = True
        Title.Caption = #1057#1090#1072#1085#1094#1080#1103
        Width = 199
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'task_id'
        Footers = <>
        Visible = False
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object Panel8: TPanel [4]
    Left = 0
    Top = 216
    Width = 1296
    Height = 24
    Align = alBottom
    BevelOuter = bvNone
    Caption = 
      #1048#1089#1090#1086#1088#1080#1103' '#1086#1087#1077#1088#1072#1094#1080#1081' ('#1076#1072#1085#1085#1099#1077' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1099' '#1074' '#1086#1073#1088#1072#1090#1085#1086#1084' '#1093#1088#1086#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1086#1084' '#1087 +
      #1086#1088#1103#1076#1082#1077')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBtnText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'declare @contragentid int, @pnum varchar(50), @cnum varchar(50),' +
        ' @seal_number varchar(50), @gu12_number varchar(50), @dispatch_n' +
        'umber varchar(50);'
      ''
      
        'select @contragentid =:contragentid , @pnum =:pnum , @cnum =:cnu' +
        'm , @seal_number =:seal_number , @gu12_number =:gu12_number , @d' +
        'ispatch_number =:dispatch_number;'
      ''
      'select c.id, c.cnum, '
      'ls.date_factexecution, '
      'isnull(s1.docorder_text, s0.docorder_text) as docorder_text,'
      
        '(select code from containerkinds k where k.id = c.kind_id) as ki' +
        'nd_code,'
      
        '(select code from objectstatekinds sk where sk.id = ls.object_st' +
        'ate_id) as state_code, '
      
        '(select isnull(code,name) from counteragents c1 where c1.id = t.' +
        'consignee_id) as consignee_code,'
      
        '(select isnull(code,name) from counteragents c2 where c2.id = t.' +
        'forwarder_id) as forwarder_code,'
      
        '(select isnull(code,name) from counteragents c2 where c2.id = t.' +
        'payer_id) as payer_code,'
      
        '(select name from stations st where st.id = t.station_id) as sta' +
        'tion_name, '
      'isnull(cr.isempty,0) as isempty, cr.weight_fact, '
      'ct.code as cargotype_code, ct.name as cargotype_name, '
      
        '(case when cr.seal_number like '#39'{%'#39' then '#39#39' else cr.seal_number ' +
        'end) seal_number,'
      'car.pnum, '
      'dds1.date_serve,'
      
        'isnull(s1.external_order_num, s0.external_order_num) as external' +
        '_order_num,'
      'l.dispatch_numbers, '
      'l.receipt_summa'
      'from v_lastobjectstates ls, containers c, cargos cr'
      'left outer join'
      '('
      ' select s.cargo_id, '
      
        ' d.doc_number+'#39' '#1086#1090' '#39'+convert(varchar(10), d.doc_date, 104)+'#39'/'#39'+c' +
        'onvert(varchar(10),isnull(s.amount,1)) as docorder_text,'
      ' s.external_order_num '
      ' from documents d, docorderspec s '
      ' where s.doc_id = d.id'
      ') as s0 on (s0.cargo_id = cr.id)'
      'left outer join cargotypes ct on (ct.id = cr.type_id),'
      'tasks t'
      'left outer join '
      '('
      
        ' select o2s.task_id, dds.date_serve from docdispatchspec dds, ob' +
        'jects2docspec o2s where o2s.id = dds.o2s_id'
      ') as dds1 on (dds1.task_id = t.id)'
      'left outer join docload l on (l.task_id = t.id)'
      'left outer join '
      '('
      ' select s.id, '
      
        ' d.doc_number+'#39' '#1086#1090' '#39'+convert(varchar(10), d.doc_date, 104)+'#39'/'#39'+c' +
        'onvert(varchar(10),isnull(s.amount,1)) as docorder_text,'
      ' s.external_order_num '
      ' from documents d, docorderspec s '
      ' where s.doc_id = d.id'
      ') as s1 on (s1.id = l.orderspec_id)'
      'left outer join docloadjoint j on (j.id = l.joint_id)'
      'left outer join carriages car on (j.carriage_id = car.id)'
      'where ls.task_id = t.id'
      'and cr.id = ls.object_id'
      'and c.id = ls.linked_object_id'
      'and t.dlv_type_id = 6'
      'and (ls.date_factexecution >= :startdate)'
      
        'and ( @contragentid is null or (t.consignee_id = @contragentid) ' +
        'or (t.forwarder_id = @contragentid) or (t.payer_id = @contragent' +
        'id))'
      'and ( @pnum = '#39#39' or charindex( @pnum, car.pnum) >0 )'
      'and ( @cnum = '#39#39' or charindex( @cnum, c.cnum) >0 )'
      
        'and ( @seal_number = '#39#39' or charindex( @seal_number, cr.seal_numb' +
        'er) >0 )'
      
        'and ( @gu12_number = '#39#39' or charindex( @gu12_number, s1.external_' +
        'order_num) >0 )'
      
        'and ( @dispatch_number = '#39#39' or charindex( @dispatch_number, l.di' +
        'spatch_numbers) >0 )'
      'order by 1 desc, 3')
    SelectCommand.Parameters = <
      item
        Name = 'contragentid'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'pnum'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'cnum'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'seal_number'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'gu12_number'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'dispatch_number'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'startdate'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end>
  end
  inherited meData: TMemTableEh
    AfterScroll = meDataAfterScroll
  end
  object drvHistory: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select odh.*,'
      
        '(select code from doctypes t where t.id = d.doctype_id)+'#39' '#8470' '#39'+d.' +
        'doc_number as doc_code, '
      
        '(select code from objectstatekinds k where k.id = odh.object_sta' +
        'te_id) as state_code,'
      
        '(select p.train_num from docload l, docloadjoint j, loadplan p w' +
        'here l.joint_id = j.id and j.load_plan_id = p.id and l.id = d.id' +
        ') as train_num,'
      'odh.date_history,'
      
        '(select name from stations c where c.id = t.station_id) as stati' +
        'on_code,'
      
        '(select code from counteragents c where c.id = t.forwarder_id) a' +
        's forwarder_code,'
      
        '(select code from counteragents c where c.id = t.consignee_id) a' +
        's consignee_code,'
      
        '(select code from counteragents c where c.id = t.payer_id) as pa' +
        'yer_code,'
      
        '(select code from deliverytypes tp where tp.id = t.dlv_type_id) ' +
        'as delivery_code '
      'from documents d,  objectstatehistory odh'
      'left outer join tasks t on (t.id = odh.task_id) '
      'where d.id = odh.doc_id and odh.object_id = :object_id'
      'order by odh.date_factexecution desc')
    SelectCommand.Parameters = <
      item
        Name = 'object_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 328
    Top = 472
  end
  object meHistory: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvHistory
    BeforeOpen = meDataBeforeOpen
    AfterInsert = meDataAfterInsert
    BeforeEdit = meDataBeforeEdit
    AfterEdit = meDataAfterEdit
    BeforePost = meDataBeforePost
    AfterPost = meDataAfterPost
    Left = 376
    Top = 472
  end
  object dsHistory: TDataSource
    DataSet = meHistory
    Left = 432
    Top = 472
  end
end
