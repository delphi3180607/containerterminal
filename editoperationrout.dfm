﻿inherited FormEditOperationRoute: TFormEditOperationRoute
  Caption = #1055#1077#1088#1077#1093#1086#1076
  ClientHeight = 136
  ExplicitHeight = 164
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 15
    Top = 18
    Width = 140
    Height = 16
    Caption = #1048#1089#1093#1086#1076#1085#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
  end
  inherited plBottom: TPanel
    Top = 95
    ExplicitTop = 95
  end
  object leStateKind: TDBSQLLookUp [2]
    Left = 15
    Top = 40
    Width = 505
    Height = 24
    DataField = 'state_id'
    DataSource = FormOperationKinds.dsState
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    SqlSet = ssServiceKind
  end
  object ssServiceKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from statekinds order by code')
    DownSql.Strings = (
      'select * from statekinds order by code')
    InitSql.Strings = (
      'select * from statekinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 192
    Top = 24
  end
end
