﻿unit grid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, Vcl.Menus, MemTableDataEh, Data.DB,
  Data.Win.ADODB, MemTableEh, DataDriverEh, ADODataDriverEh, Functions, edit,
  Vcl.StdCtrls, EhLibADO, EhLibMTE, IniFiles, System.Actions, Vcl.ActnList,
  Vcl.ComCtrls, System.Generics.Defaults, System.Generics.Collections,
  EXLReportExcelTLB, EXLReportBand, EXLReport, Vcl.Mask, DBCtrlsEh;

type

  TFakeDBGridEh = class(TDBGridEh);

  TFormEditListNode = class(TPersistent)
  public
    meData: TMemTableEh;
    formEdit: TFormEdit;
    dataGrid: TDBGridEh;
    parentDataSet: TDataSet;
    childDataSet: TDataSet;
  end;

  TFormGrid = class(TForm)
    pmGrid: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    Excel1: TMenuItem;
    drvData: TADODataDriverEh;
    meData: TMemTableEh;
    dsData: TDataSource;
    N4: TMenuItem;
    N5: TMenuItem;
    plBottom: TPanel;
    btnOk: TButton;
    btnCancel: TButton;
    qrAux: TADOQuery;
    plAll: TPanel;
    plTop: TPanel;
    sbDelete: TPngSpeedButton;
    btFilter: TPngSpeedButton;
    btExcel: TPngSpeedButton;
    sbAdd: TPngSpeedButton;
    sbEdit: TPngSpeedButton;
    Bevel2: TBevel;
    btTool: TPngSpeedButton;
    drvForms: TADODataDriverEh;
    meForms: TMemTableEh;
    dsForms: TDataSource;
    al: TActionList;
    aAdd: TAction;
    aDelete: TAction;
    aEdit: TAction;
    dgData: TDBGridEh;
    exReport: TEXLReport;
    plHint: TPanel;
    lbText: TLabel;
    plCount: TPanel;
    edCount: TDBEditEh;

    procedure sbDeleteClick(Sender: TObject);
    procedure sbAddClick(Sender: TObject);
    procedure sbEditClick(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure dgDataColWidthsChanged(Sender: TObject);
    procedure dgDataColumnMoved(Sender: TObject; FromIndex, ToIndex: Integer);
    procedure dgDataActiveGroupingStructChanged(Sender: TCustomDBGridEh);
    procedure FormCreate(Sender: TObject);
    procedure meDataAfterPost(DataSet: TDataSet);
    procedure meDataBeforePost(DataSet: TDataSet);
    procedure meDataBeforeEdit(DataSet: TDataSet);
    procedure btExcelClick(Sender: TObject);
    procedure meDataAfterInsert(DataSet: TDataSet);
    procedure meDataAfterEdit(DataSet: TDataSet);
    procedure dgDataKeyPress(Sender: TObject; var Key: Char);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure dgDataSelectionChanged(Sender: TObject);
    procedure dgDataMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dgDataMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dgDataMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure btToolClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure meDataAfterOpen(DataSet: TDataSet);
    procedure dgDataApplyFilter(Sender: TObject);
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure dgDataTitleBtnClick(Sender: TObject; ACol: Integer;
      Column: TColumnEh);
  private
    FFormEdit: TFormEdit;
    procedure RemoveDataSet(dsLocal: TDataSource);
  protected

    g: string;
    gu: TGuid;

    old_current_mode: string;

    AvoidKey: boolean;
    update_rectangle: boolean;

    selfield: string;
    selvalue: variant;
    seltextvalue: string;

    currect: TRect;
    isinited: boolean;

    hidehourglass: boolean;

    procedure InitIniFile;
    procedure RestoreSort(dg: TDBGridEh);
    procedure ClearSTFilter(dg: TDBGridEh);
    procedure SetFormEdit(value: TFormEdit);
    function CheckChildBeforeAppend(id: string): boolean;
    function CheckChildBeforeDelete(id: string): boolean;
    function CheckBeforeDelete(id: integer; guid: string): boolean; virtual;
    procedure AssignSelField(colname: string); virtual;
    procedure ShowSpecialHint(c: TColumnEh; p: TPoint; hint: string);
    procedure SystemTool;
  public

    current_mode: string;

    formsEdit: TList<TFormEditListNode>;
    formId: integer;
    SaveOriginalEditForm: boolean;

    current_edit_form: TFormEdit;

    IniLayout: TIniFile;
    sqlText: string;
    sqlOperationText: string;
    docKind: string;

    current_id: integer;
    parent_id: integer;
    tablename: string;

    pairparam: TPairParam;
    allow_edit: boolean;
    isbiglist : boolean;

    function CheckBeforeAppend: boolean; virtual;
    procedure SetReadOnly; virtual;
    procedure Init; virtual;
    procedure PartialInit; virtual;
    procedure InitNullCheck(f: TField);
    procedure RestoreDataGrouping;
    procedure RegisterForm;
    procedure ClearCells;
    procedure RefreshRecord(ds: TMemTableEh; id: integer = 0);
    procedure FullRefresh(DataSet: TMemTableEh; hidehourglass: boolean = false);

    function FindEditNodeByGrid(g: TDbGridEh): TFormEditListNode;
    function FindEditNodeByForm(f: TFormEdit): TFormEditListNode;

    procedure AddEditForm(g: TDbGridEh; f: TFormEdit; p: TDataSet = nil; c: TDataSet = nil);

    procedure FillSelectListAll(g: string; defaultid:string = 'id');

    procedure FillStringColumn(fn: string);

    procedure ScrollActiveToRow(Grid : TDBGridEh; ARow : Integer);

    procedure CheckMissingFilterValues(fl: TStringList; fn: string);

  published
    property formEdit: TFormEdit read FFormEdit write SetFormEdit;
  end;

var
  FormGrid: TFormGrid;

implementation

{$R *.dfm}

uses tool, main, dmu, SetMark, MissingValues;

procedure TFormGrid.InitNullCheck(f: TField);
begin
  if f.Value = null then f.Value := 0;
end;

procedure TFormGrid.ShowSpecialHint(c: TColumnEh; p: TPoint; hint: string);
begin

  plHint.Left := p.X+25;
  plHint.Top := p.Y;
  lbText.Caption := hint;

  if p.Y > (TFakeDBGridEh(dgData).FTitleHeightFull+dgData.Top) then
  plHint.Show;

  currect := dgData.CellRectAbs(c.Index+1, dgData.Row,false);

end;

procedure TFormGrid.AddEditForm(g: TDbGridEh; f: TFormEdit; p: TDataSet = nil; c: TDataSet = nil);
var n: TFormEditListNode; m: TMemTableEh;
begin
  m := TMemTableEh(g.DataSource.DataSet);
  n := TFormEditListNode.Create;
  n.meData := m;
  n.formEdit := f;
  n.dataGrid := g;
  n.parentDataSet := p;
  n.childDataSet := c;
  formsEdit.Add(n);
end;

function TFormGrid.CheckChildBeforeAppend(id: string): boolean;
begin
  result := true;
  if id = '' then
  begin
    result := false;
    ShowMessage('Нет родительской записи. Добавление невозможно.');
  end;
end;


function TFormGrid.CheckChildBeforeDelete(id: string): boolean;
begin
  result := true;
  if id <> '' then
  begin
    result := false;
    ShowMessage('Есть дочерние записи. Удаление невозможно.');
  end;
end;

function TFormGrid.CheckBeforeDelete(id: integer; guid: string): boolean;
begin
  result := true;
end;

function TFormGrid.FindEditNodeByGrid(g: TDbGridEh): TFormEditListNode;
var n: TFormEditListNode; i: integer;
begin
  n := nil;
  for i := 0 to formsEdit.Count-1 do
  begin
    n := formsEdit[i];
    if n.dataGrid=g then
    begin
      result := n;
      exit;
    end;
  end;
  result := nil;
end;


function TFormGrid.FindEditNodeByForm(f: TFormEdit): TFormEditListNode;
var n: TFormEditListNode; i: integer;
begin
  n := nil;
  for i := 0 to formsEdit.Count-1 do
  begin
    n := formsEdit[i];
    if n.formEdit=f then
    begin
      result := n;
      exit;
    end;
  end;
  result := nil;
end;



procedure TFormGrid.SetFormEdit(value: TFormEdit);
var n: TFormEditListNode;
begin
  FFormEdit := value;

  n := nil;
  FindEditNodeByForm(value);

  if not Assigned(n) then
  begin
    n := TFormEditListNode.Create;
    n.meData := meData;
    n.formEdit := value;
    n.dataGrid := dgData;
    formsEdit.Add(n);
  end;
end;

procedure TFormGrid.RefreshRecord(ds: TMemTableEh; id: integer = 0);
var drv: TADODataDriverEh; isrefreshed: boolean;
begin
  drv := TADODataDriverEh(ds.DataDriver);
  isrefreshed := false;
  if drv.GetrecSQL.Text <> '' then
  begin
    if id = 0 then id := self.meData.FieldByName('id').AsInteger;
    if id = 0 then exit;
    drv.GetrecCommand.Parameters.ParamByName('current_id').Value := id;
    ds.RefreshRecord;
    isrefreshed := true;
  end;
  if (not isrefreshed) then
  begin
    id := self.meData.FieldByName('id').AsInteger;
    meData.Close;
    meData.Open;
    meData.Locate('id',id,[]);
  end;
end;

procedure TFormGrid.RestoreSort(dg: TDBGridEh);
var ds: TMemTableEh; so, sd: string;
begin
  ds := TMemTableEh(dg.DataSource.DataSet);
  if dg.SortMarkedColumns.Count>0 then
  begin
     so := dg.SortMarkedColumns[0].FieldName;
     if dg.SortMarkedColumns[0].Title.SortMarker = smUpEh
     then sd := ' asc' else sd := ' desc';
     ds.SortOrder := so+sd;
  end else
  begin
     ds.SortOrder := '';
  end;
end;


procedure TFormGrid.ClearSTFilter(dg: TDBGridEh);
var i: integer;
begin
  for i := 0 to dg.Columns.Count -1 do
  begin
    dg.Columns[i].STFilter.Clear;
  end;

end;

procedure TFormGrid.sbEditClick(Sender: TObject);
var cdgData: TDBGridEh; nformEdit, cformEdit: TFormEdit; cmeData: TMemTableEh; n: TFormEditListNode; pDataSet: TDataSet;
begin

  cformEdit := nil;

  If Assigned(Screen.ActiveControl)
  And (Screen.ActiveControl Is TDBGridEh)
  then begin
    cdgData := TDbGridEh(Screen.ActiveControl);
    n:= FindEditNodeByGrid(cdgData);
    if Assigned(n) then
    begin
      nformEdit := n.formEdit;
      cmeData := n.meData;
      pDataSet := n.parentDataSet;
    end else
    begin
      nformEdit := formEdit;
      cmeData := meData;
      pDataSet := nil;
    end;
  end else
  begin
    nformEdit := formEdit;
    cmeData := meData;
    pDataSet := nil;
  end;

  if nformEdit = nil then
  begin
    nformEdit := formEdit;
    cmeData := meData;
    pDataSet := nil;
  end;

  if not Assigned(nformEdit) then
  begin
    cformEdit := formEdit;
    cmeData := meData;
    pDataSet := nil;
  end;

  if not Assigned(nformEdit) then
  begin
    cmeData.Edit;
    exit;
  end;

  if cmeData.FieldByName('id').AsInteger = 0 then exit;

  if not self.SaveOriginalEditForm then
  Application.CreateForm(TComponentClass(nformEdit.ClassType),cformEdit);

  if not Assigned(cformEdit.dsLocal.DataSet) then
    cformEdit.dsLocal.DataSet := cmeData;

  cmeData.ReadOnly := false;
  cmeData.Edit;
  cformEdit.current_id := meData.FieldByName('id').AsInteger;

  cformEdit.ParentFormGrid := self;
  cformEdit.ShowModal;

  if cformEdit.ModalResult = mrOk then
  begin
     try
       if not cmeData.ReadOnly then
       begin
         cmeData.Post;
         cformEdit.SaveClassifications(cmeData.FieldByName('id').AsInteger);
       end else
         cmeData.Cancel;

       if not self.SaveOriginalEditForm then cformEdit.Free;

     except
        on E : Exception do
        begin
          if not self.SaveOriginalEditForm then cformEdit.Free;
          if cmeData.State = (dsEdit) then cmeData.Cancel;
          ShowMessage(E.Message);
          exit;
        end;
     end;
  end else
  begin
     if not self.SaveOriginalEditForm then cformEdit.Free;
     cmeData.Cancel;
  end;

end;

procedure TFormGrid.N5Click(Sender: TObject);
begin
  FullRefresh(meData, hidehourglass);
end;

procedure TFormGrid.FullRefresh(DataSet: TMemTableEh; hidehourglass: boolean = false);
var oldSortOrder: string; oldid, oldrow: integer;
begin

  if not DataSet.Active then
  begin
    DataSet.Open;
    exit;
  end;

  oldid := DataSet.FieldByName('id').AsInteger;
  oldSortOrder := DataSet.SortOrder;

  OldRow := TFakeDBGridEh(dgData).TopRow;
  if (not hidehourglass) then Screen.Cursor := crHourGlass;
  if hidehourglass then DataSet.DisableControls;
  DataSet.Close;
  DataSet.Open;
  if (not hidehourglass) then Screen.Cursor := crDefault;

  //if not isbiglist then
  TFakeDBGridEh(dgData).TopRow := OldRow;

  DataSet.Locate('id',oldid,[]);
  DataSet.EnableControls;

end;

procedure TFormGrid.sbAddClick(Sender: TObject);
var oldRow: integer; b: TBookmark; cdgData: TDBGridEh; nformEdit, cformEdit: TFormEdit; cmeData: TMemTableEh; n: TFormEditListNode; pDataSet: TDataSet;
begin

  if pos(self.tablename,FormMain.readonly_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  cmeData := nil;

  if Screen.ActiveControl <> nil then
  If Assigned(Screen.ActiveControl)
  And (Screen.ActiveControl Is TDBGridEh)
  then begin
    cdgData := TDbGridEh(Screen.ActiveControl);
    n:= FindEditNodeByGrid(cdgData);
    if Assigned(n) then
    begin
      nformEdit := n.formEdit;
      cmeData := n.meData;
      pDataSet := n.parentDataSet;
    end else
    begin
      nformEdit := formEdit;
      cmeData := meData;
      pDataSet := nil;
    end;
  end else
  begin
    nformEdit := formEdit;
    cmeData := meData;
    pDataSet := nil;
  end;

  if cmeData = nil then
  begin
    cmeData := meData;
    nformEdit := formEdit;
  end;

  if not Assigned(nformEdit) then
  begin
    cmeData.Append;
    exit;
  end;

  cmeData.ReadOnly := false;

  if Assigned(pDataSet) then
    if not self.CheckChildBeforeAppend(pDataSet.FieldByName('id').AsString) then exit;

  if not Assigned(nformEdit) then exit;

  Application.CreateForm(TComponentClass(nformEdit.ClassType),cformEdit);

  if not Assigned(cformEdit.dsLocal.DataSet) then
    cformEdit.dsLocal.DataSet := cmeData;

  b := cmeData.Bookmark;
  if Assigned(cdgData) then
  begin
    try
      oldRow := TFakeDBGridEh(cdgData).TopRow;
    except
      //--
    end;
  end;

  cmeData.Append;

  if not CheckBeforeAppend then
  begin
    cmeData.Cancel;
    exit;
  end;

  cformEdit.ParentFormGrid := self;
  cformEdit.current_mode := 'insert';
  cformEdit.ShowModal;

  if cformEdit.ModalResult = mrOk then
  begin
     try

       current_mode := 'insert';
       if cmeData.State = dsInsert then
       cmeData.Post;

       cformEdit.SaveClassifications(cmeData.FieldByName('id').AsInteger);

     except
        on E : Exception do
        begin
          cformEdit.Free;
          if cmeData.State = (dsEdit) then cmeData.Cancel;
          if cmeData.State = (dsInsert) then cmeData.Cancel;
          ShowMessage(E.Message);
          exit;
        end;
     end;
     cformEdit.Free;
  end else
  begin
     cformEdit.Free;
     cmeData.Cancel;
     cmeData.Bookmark := b;
     if Assigned(cdgData) then
     begin
       try
         TFakeDBGridEh(cdgData).TopRow := oldRow;
       except
         //--
       end;
     end;

  end;

end;

procedure TFormGrid.sbDeleteClick(Sender: TObject);
var id, i: integer; g: string; guid: TGuid;
var cdgData: TDBGridEh; cmeData: TMemTableEh; n: TFormEditListNode; cDataSet: TDataSet;
begin

  if pos(self.tablename,FormMain.readonly_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  if meData.State = dsEdit then exit;

  If Assigned(Screen.ActiveControl)
  And (Screen.ActiveControl Is TDBGridEh)
  then begin
    cdgData := TDbGridEh(Screen.ActiveControl);
    n:= FindEditNodeByGrid(cdgData);
    if Assigned(n) then
    begin
      cmeData := n.meData;
      cDataSet := n.childDataSet;
    end else
    begin
      cmeData := meData;
      cDataSet := nil;
    end;
  end else
  begin
    cmeData := meData;
    cDataSet := nil;
  end;

  if Assigned(cDataSet) then
    if not self.CheckChildBeforeDelete(cDataSet.FieldByName('id').AsString) then exit;

  if fQYN('Удалить запись(и) ?') then
  begin

     id := cmeData.FieldByName('id').AsInteger;

     if not (cdgData = nil) and (cdgData.Selection.SelectionType in [gstRecordBookmarks, gstAll]) then
     begin

      CreateGUID(guid);
      g := GuidToString(guid);
      FillSelectListAll(g);

      if not CheckBeforeDelete(0,g) then
      begin
        dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
        dm.spClearSelectList.ExecProc;
        exit;
      end;

      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('delete from '+self.tablename+' where id in (select id from selectlist where rtrim(guid) = '''+trim(g)+''')');
      qrAux.ExecSQL;

      dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
      dm.spClearSelectList.ExecProc;

      cmeData.Close;
      cmeData.Open;
      cmeData.Locate('id',id,[]);

     end else
     begin
       if not CheckBeforeDelete(id,'') then exit;
       cmeData.Delete;
     end;

     meData.ReadOnly := false;

  end;
end;


procedure TFormGrid.btExcelClick(Sender: TObject);
var cdgData: TDBGridEh; cformEdit: TFormEdit; cmeData: TMemTableEh; n: TFormEditListNode;
begin
  if Assigned(Screen.ActiveControl) and (Screen.ActiveControl is TDBGridEh)
  then cdgData := TDBGridEh(Screen.ActiveControl)
  else cdgData := self.dgData;

  if fQYN('Экспортировать список в Excel ?') then
    ExportExcel(cdgData, self.Caption);
end;

procedure TFormGrid.btToolClick(Sender: TObject);
begin
  //--
end;

procedure TFormGrid.SystemTool;
begin

  FormTool.formId := self.formId;

  FormTool.meColumns.Close;
  FormTool.drvColumns.SelectCommand.Parameters.ParamByName('form_id').Value := self.formId;
  FormTool.meColumns.Open;

  FormTool.dsLocal.DataSet := self.meForms;

  self.meForms.Close;
  self.drvForms.SelectCommand.Parameters.ParamByName('id').Value := self.formId;
  self.meForms.Open;
  FormTool.seSelect.Text := self.meForms.FieldByName('select_sql').AsString;
  FormTool.seInsert.Text := self.meForms.FieldByName('insert_sql').AsString;
  FormTool.seUpdate.Text := self.meForms.FieldByName('update_sql').AsString;
  FormTool.seDelete.Text := self.meForms.FieldByName('delete_sql').AsString;

  self.meForms.Edit;

  FormTool.ShowModal;

  if FormTool.ModalResult = mrOk then
  begin
     self.meForms.FieldByName('select_sql').Value := FormTool.seSelect.Text;
     self.meForms.FieldByName('update_sql').Value := FormTool.seUpdate.Text;
     self.meForms.FieldByName('insert_sql').Value := FormTool.seInsert.Text;
     self.meForms.FieldByName('delete_sql').Value := FormTool.seDelete.Text;
     self.meForms.Post;
     self.Init;
   end else
     self.meForms.Cancel;

end;

procedure TFormGrid.FormCreate(Sender: TObject);
begin
  formsEdit := TList<TFormEditListNode>.Create;
  InitIniFile;
  RegisterForm;
end;


procedure TFormGrid.InitIniFile;
var path: string;
begin
  path := ExtractFilePath(Application.ExeName);

  if not DirectoryExists(path+'layouts') then
    CreateDir(path+'layouts');

  path := path+'layouts\';

  if not Assigned(IniLayout) then
  begin
    IniLayout := TIniFile.Create(path+'layout.ini');
  end;
end;


procedure TFormGrid.meDataAfterEdit(DataSet: TDataSet);
var i: integer;
begin
  for i := 0 to meData.FieldDefs.Count-1 do
  begin
    if meData.FieldDefs[i].DataType = ftBoolean then
      if meData.Fields[i].Value = null then meData.Fields[i].Value := 0;
  end;
end;

procedure TFormGrid.meDataAfterInsert(DataSet: TDataSet);
var i: integer;
begin
  for i := 0 to meData.FieldDefs.Count-1 do
  begin
    if meData.FieldDefs[i].DataType = ftBoolean then
      if meData.Fields[i].Value <> 1 then meData.Fields[i].Value := 0;
  end;
end;

procedure TFormGrid.meDataAfterOpen(DataSet: TDataSet);
begin
  if (not hidehourglass) then Screen.Cursor := crDefault;
  edCount.Text := IntToStr(meData.RecordCount);
end;

procedure TFormGrid.meDataAfterPost(DataSet: TDataSet);
var cmeData: TMemTableEh; cdrvData: TADODataDriverEh; col: string;
begin

   cmeData := TMemTableEh(DataSet);
   cdrvData := TADODataDriverEh(cmeData.DataDriver);

   if (current_mode = 'insert') then
   begin

     if cdrvData.GetrecCommand.Parameters.FindParam('current_id') <> nil then
     begin
       qrAux.Close;
       qrAux.SQL.Clear;

       if tablename<>'' then
         qrAux.SQL.Add('select IDENT_CURRENT('''+tablename+''')')
       else
         qrAux.SQL.Add('select @@IDENTITY;');

       qrAux.Open;
       current_id := qrAux.Fields[0].AsInteger;

       if current_id <> 0 then
       begin
         cdrvData.GetrecCommand.Parameters.ParamByName('current_id').Value := current_id;
         if Assigned(cmeData.BeforeRefresh) then cmeData.BeforeRefresh(cmeData);
         RefreshRecord(cmeData,current_id);

         if not cmeData.Locate('id',current_id,[]) then
         begin
           ShowMessage('Добавленная запись не отвечает заданным условиям отбора и не может быть отображена в списке.');
           exit;
         end;

       end;

     end;

   end;

   if (current_mode = 'update') then
   begin

     if cdrvData.GetrecCommand.Parameters.FindParam('current_id') <> nil then
     begin
       current_id := cmeData.FieldByName('id').AsInteger;
       cdrvData.GetrecCommand.Parameters.ParamByName('current_id').Value := current_id;
       if Assigned(cmeData.BeforeRefresh) then cmeData.BeforeRefresh(cmeData);
       RefreshRecord(cmeData,current_id);
     end;

   end;

   old_current_mode := current_mode;
   current_mode := 'none';

    col := dgData.Columns[dgData.Col-1].FieldName;
    AssignSelField(col);

    if selfield<>'' then
    begin
      selvalue := meData.FieldByName(selfield).Value;
      seltextvalue := meData.FieldByName(col).AsString;
    end;

end;


procedure TFormGrid.meDataAfterScroll(DataSet: TDataSet);
begin
  SetReadOnly;
end;

procedure TFormGrid.ClearCells;
var fixfield, col: string;
begin

  col := dgData.Columns[dgData.Col-1].FieldName;
  AssignSelField(col);

  if selfield = '' then
  begin
    exit;
  end;

  fixfield := selfield;

  if dgData.Selection.SelectionType = (gstRectangle) then
  begin

    if not fQYN('Очистить ячейки?') then
    begin
      exit;
    end;

    meData.DisableControls;

    meData.Bookmark := dgData.Selection.Rect.TopRow;

    Screen.Cursor := crHourGlass;

    while true do
    begin

      try
        allow_edit := true;
        meData.ReadOnly := false;
        meData.Edit;
        meData.FieldByName(fixfield).Value := null;
        meData.ReadOnly := false;
        meData.Post;
      except
        //--
        Screen.Cursor := crDefault;
        meData.EnableControls;
      end;

      if meData.CompareBookmarks(meData.Bookmark , dgData.Selection.Rect.BottomRow) = 0
      then break;

      meData.Next;

    end;

    Screen.Cursor := crDefault;
    meData.EnableControls;

    FullRefresh(meData, hidehourglass);

  end else
  begin
    meData.EnableControls;
    allow_edit := true;
    self.meData.Edit;
    self.meData.FieldByName(fixfield).Value := null;
    self.meData.Post;
  end;

  allow_edit := false;
  selfield := '';

  meData.EnableControls;
  dgData.Selection.Clear;

end;

procedure TFormGrid.meDataBeforeEdit(DataSet: TDataSet);
begin
  RefreshRecord(TMemTableEh(DataSet), DataSet.FieldByName('id').AsInteger);
  SetReadOnly;
end;

procedure TFormGrid.meDataBeforeOpen(DataSet: TDataSet);
begin
  if (not hidehourglass) then Screen.Cursor := crHourGlass;
end;

procedure TFormGrid.meDataBeforePost(DataSet: TDataSet);
begin
 if DataSet.State in [dsInsert] then current_mode := 'insert';
 if DataSet.State in [dsEdit] then current_mode := 'update';
end;

procedure TFormGrid.FormShow(Sender: TObject);
begin
  dgData.SetFocus;
end;

procedure TFormGrid.Init;
begin
  meData.Open;
  SaveOriginalEditForm := false;
  self.dgData.Style.SelectionInactiveColor := clSkyBlue;
  AvoidKey := false;
  isbiglist := false;
  isinited := true;
  hidehourglass := false;
end;

function TFormGrid.CheckBeforeAppend: boolean;
begin
 result := true;
end;


procedure TFormGrid.PartialInit;
begin
  dgData.SetFocus;
end;

procedure TFormGrid.RegisterForm;
begin
  formId := FormMain.formIdCounter;
  FormMain.formIdCounter := FormMain.formIdCounter + 1;
end;


procedure TFormGrid.dgDataActiveGroupingStructChanged(Sender: TCustomDBGridEh);
var c: string; i: integer; path: string;
begin
  InitIniFile;
  IniLayout.EraseSection('group'+IntToStr(self.formId));
  for i  := 0 to self.dgData.DataGrouping.GroupLevels.Count-1 do
  begin
    c := TDbGridColumnEh(self.dgData.DataGrouping.GroupLevels.Items[0].Column).FieldName;
    IniLayout.WriteString('group'+IntToStr(self.formId),IntToStr(i),c);
  end;
end;

procedure TFormGrid.dgDataApplyFilter(Sender: TObject);
begin
  dgData.DefaultApplyFilter;
  RestoreSort(dgData);
  edCount.Text := IntToStr(meData.RecordCount);
end;

procedure TFormGrid.dgDataColumnMoved(Sender: TObject; FromIndex, ToIndex: Integer);
begin
  //self.dgData.SaveColumnsLayout(IniLayout, 'grid'+IntToStr(self.formId));
end;

procedure TFormGrid.dgDataColWidthsChanged(Sender: TObject);
begin
  //self.dgData.SaveColumnsLayout(IniLayout, 'grid'+IntToStr(self.formId));
end;

procedure TFormGrid.dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
begin

    dgData.DefaultDrawColumnCell(Rect,DataCol,Column,State);

    if gdSelected in State then begin

      //dgData.Canvas.Pen.Color := $00C49DA7;
      //dgData.Canvas.Pen.Style := psClear;
      //dgData.Canvas.Pen.Color := $00C8C8C8;

      if dgData.Selection.SelectionType = (gstRectangle) then
      if (Column.Index = dgData.Col-1)then
        dgData.Canvas.Pen.Color := clBlue;

      dgData.Canvas.Brush.Style := bsClear;
      dgData.Canvas.Font.Style := Column.Font.Style;
      dgData.Canvas.Pen.Width := 1;
      dgData.Canvas.Rectangle(Rect.Left, Rect.Top, Rect.Right, Rect.Bottom);
    end;


end;

procedure TFormGrid.dgDataKeyPress(Sender: TObject; var Key: Char);
begin

  if Key = #13 then
  begin
    if meData.State in [dsInsert, dsEdit] then meData.Post
  end;

  if dgData.EditorMode then exit;

  if AvoidKey then
  begin
    AvoidKey := false;
    Key := #0;
    exit;
  end;

  if Key = #13 then
  begin
    if meData.State in [dsInsert, dsEdit] then meData.Post
    else sbEditClick(self);
  end;

end;

procedure TFormGrid.AssignSelField(colname: string);
begin
  selfield := '';
end;

procedure TFormGrid.dgDataMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var col: string;
var ldgData: TDBGridEh; lmeData: TMemTableEh;
begin

    if Shift<>[ssLeft] then exit;

    ldgData := TDBGridEh(Sender);
    lmeData := TMemTableEh(ldgData.DataSource.DataSet);

    try
      col := ldgData.Columns[ldgData.Col-1].FieldName;

      AssignSelField(col);

      if selfield<>'' then
      begin
        selvalue := lmeData.FieldByName(selfield).Value;
        seltextvalue := lmeData.FieldByName(col).AsString;
      end;
    except
      //--
    end;

end;

procedure TFormGrid.dgDataMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if plHint.Visible then
  begin
    if X> currect.Right then plHint.Hide;
    if X< currect.Left then plHint.Hide;
    if Y< (TFakeDBGridEh(dgData).FTitleHeightFull+dgData.Top) then
    plHint.Hide;
  end;
end;

procedure TFormGrid.dgDataMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var i: integer; tr, br: TArray<System.Byte>;
var ldgData: TDBGridEh; lmeData: TMemTableEh;
begin

  if not update_rectangle then exit;

  ldgData := TDBGridEh(Sender);
  lmeData := TMemTableEh(ldgData.DataSource.DataSet);

  if ldgData.Selection.SelectionType = (gstRectangle) then
  begin

    if selfield = '' then exit;

    if not fQYN('Заместить ячейки значением: `'+seltextvalue+'` ?') then
    begin
       dgData.Selection.Clear;
       exit;
    end;

    Screen.Cursor := crHourGlass;

    tr := ldgData.Selection.Rect.TopRow;
    br := ldgData.Selection.Rect.BottomRow;

    lmeData.DisableControls;

    lmeData.Bookmark := tr;

    i := 0;

    while true do
    begin

      if lmeData.FieldByName(selfield).Value <> selvalue then
      begin
        try
          Screen.Cursor := crHourGlass;
          lmeData.Edit;
          lmeData.FieldByName(selfield).Value := selvalue;
          lmeData.Post;
        except
          //--
          Screen.Cursor := crDefault;
          lmeData.EnableControls;
        end;
      end;

      if lmeData.CompareBookmarks(lmeData.Bookmark, br)=0 then
      break;

      lmeData.Next;

      i := i+1;

    end;

    Screen.Cursor := crDefault;
    lmeData.EnableControls;


  end;

  update_rectangle := false;
  lmeData.EnableControls;
  dgData.Selection.Clear;


end;

procedure TFormGrid.dgDataSelectionChanged(Sender: TObject);
begin
  if dgData.Selection.SelectionType = (gstRectangle) then
  begin
    update_rectangle := true;
  end else
  begin
    update_rectangle := false;
  end;
end;

procedure TFormGrid.dgDataTitleBtnClick(Sender: TObject; ACol: Integer; Column: TColumnEh);
begin
  if Column.Title.SortMarker = smNoneEh then
    Column.Title.SortMarker := smDownEh
  else if Column.Title.SortMarker = smDownEh then
    Column.Title.SortMarker := smUpEh
  else if Column.Title.SortMarker = smUpEh then
    Column.Title.SortMarker := smNoneEh;
  RestoreSort(dgData);
end;

procedure TFormGrid.RestoreDataGrouping;
var s: TStringList; i, p: integer; l: TGridDataGroupLevelEh; v: string; c: TColumnEh;
begin

  s := TStringList.Create;

  IniLayout.ReadSectionValues('group'+IntToStr(self.formId),s);

  for i := 0 to s.Count-1 do
  begin
    p := pos('=',s[i]);
    v := copy(s[i],p+1,255);

    try
      c := self.dgData.FieldColumns[v]
    except
      c:= nil;
    end;
    if Assigned(c) then
    begin
      l := self.dgData.DataGrouping.GroupLevels.Add;
      l.Column := c;
    end;

  end;

end;



procedure TFormGrid.FillSelectListAll(g: string; defaultid:string = 'id');
var i: integer; e: integer;
begin

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    if self.dgData.Selection.SelectionType = (gstAll) then
    begin

      self.meData.First;
      while not self.meData.Eof do
      begin

        dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := self.meData.FieldByName(defaultid).AsInteger;
        dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
        dm.spAdd2SelectList.ExecProc;

        self.meData.Next;

      end;
      //meData.Close;
      //meData.Open;

    end else
    if self.dgData.Selection.SelectionType = (gstRecordBookmarks) then
    begin

      for i := 0 to self.dgData.Selection.Rows.Count-1 do
      begin
        self.meData.Bookmark := self.dgData.Selection.Rows[i];

        dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := self.meData.FieldByName(defaultid).AsInteger;
        dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
        dm.spAdd2SelectList.ExecProc;

      end;
      //meData.Close;
      //meData.Open;

    end else
    begin

      if drvData.GetrecCommand.Parameters.FindParam('current_id') <> nil then
      begin

        dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := self.meData.FieldByName('id').AsInteger;
        dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
        dm.spAdd2SelectList.ExecProc;

      end else
      begin
         //meData.Close;
         //meData.Open;
      end;
    end;

end;


procedure TFormGrid.FillStringColumn(fn: string);
var gg: TGuid; g: string; ft: string; col: TColumnEh;
begin

  col := dgData.FindFieldColumn(fn);
  ft := col.Title.Caption;

  FormSetMark.Caption := ft;
  FormSetMark.leNote.EditLabel.Caption := ft;

  FormSetMark.ShowModal;

  if FormSetMark.ModalResult = mrOk then
  begin

    CreateGUID(gg);
    g := GuidToString(gg);
    FillSelectListAll(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('update '+self.tablename+' set '+fn+' = :smark where id in (');
    qrAux.SQL.Add('select id from selectlist where rtrim(guid) = '''+trim(g)+'''');
    qrAux.SQL.Add(')');
    qrAux.Parameters.ParamByName('smark').Value := FormSetMark.leNote.Text;
    qrAux.ExecSQL;

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    FullRefresh(meData, hidehourglass);

  end;

end;



procedure TFormGrid.ScrollActiveToRow(Grid : TDBGridEh; ARow : Integer);
 var FTitleOffset, SDistance : Integer;
     NewRect : TRect;
     RowHeight : Integer;
     NewRow : Integer;
begin
 with TFakeDBGridEh(Grid) do begin
   NewRow:= Row;
   FTitleOffset:= 0;
   if dgTitles in Options then inc(FTitleOffset);
   if ARow = NewRow then Exit;
   with DataLink, DataSet do
    try
      BeginUpdate;
      Scroll(NewRow - ARow);
      if (NewRow - ARow) < 0 then ActiveRecord:= 0
                             else ActiveRecord:= VisibleRowCount - 1;
      SDistance:= MoveBy(NewRow - ARow);
      NewRow:= NewRow - SDistance;
      MoveBy(ARow - ActiveRecord - FTitleOffset);
      RowHeight:= DefaultRowHeight;
      NewRect:= BoxRect(0, FTitleOffset, ColCount - 1, 1000);
      ScrollWindowEx(Handle, 0, - RowHeight * SDistance, @NewRect, @NewRect, 0, nil, SW_Invalidate);
      MoveColRow(Col, NewRow, False, False);
    finally
      EndUpdate;
    end;
 end;
end;


procedure TFormGrid.CheckMissingFilterValues(fl: TStringList; fn: string);
var i, j: integer; fv: TStringList;
begin

  Screen.Cursor := crHourGlass;
  meData.DisableControls;

  fv := TStringList.Create;
  meData.First;

  while not meData.Eof do
  begin
    fv.Add(meData.FieldByName(fn).AsString);
    meData.Next;
  end;

  for i:= 0 to fv.Count-1 do
  begin
    j := 0;

    while j < fl.Count do
    begin

      if pos(fl[j],fv[i])>0 then fl.Delete(j)
      else j := j+1;

    end;

  end;

  meData.EnableControls;
  Screen.Cursor := crDefault;

  FormMissingValues.meList.Lines.Clear;
  FormMissingValues.meList.Lines.AddStrings(fl);

  if FormMissingValues.meList.Lines.Count>0 then
  FormMissingValues.ShowModal;

end;


procedure TFormGrid.RemoveDataSet(dsLocal: TDataSource);
begin

  try
    dsLocal.DataSet := nil;
  except
    //--
  end;

  try
    dsLocal.DataSet := nil;
  except
    //--
  end;

end;


procedure TFormGrid.SetReadOnly;
begin

  if pos(self.tablename,FormMain.readonly_sections)>0 then
  begin
    meData.ReadOnly := true;
    exit;
  end;


end;



end.
