﻿inherited FormEditOperation: TFormEditOperation
  Caption = #1055#1088#1086#1074#1077#1076#1077#1085#1080#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
  ClientHeight = 571
  ClientWidth = 752
  OnActivate = FormActivate
  ExplicitWidth = 758
  ExplicitHeight = 599
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 11
    Top = 94
    Width = 150
    Height = 16
    Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1086#1087#1077#1088#1072#1094#1080#1080
  end
  object Label2: TLabel [1]
    Left = 11
    Top = 292
    Width = 143
    Height = 16
    Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#1086#1089#1085#1086#1074#1072#1085#1080#1077
  end
  object Label5: TLabel [2]
    Left = 14
    Top = 426
    Width = 82
    Height = 16
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
  end
  object SpeedButton1: TSpeedButton [3]
    Left = 541
    Top = 314
    Width = 97
    Height = 25
    Caption = #1057#1086#1079#1076#1072#1090#1100
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object SpeedButton2: TSpeedButton [4]
    Left = 644
    Top = 314
    Width = 97
    Height = 25
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object Label3: TLabel [5]
    Left = 11
    Top = 356
    Width = 102
    Height = 16
    Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#1089#1095#1077#1090
  end
  object SpeedButton3: TSpeedButton [6]
    Left = 541
    Top = 378
    Width = 97
    Height = 25
    Caption = #1057#1086#1079#1076#1072#1090#1100
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object SpeedButton4: TSpeedButton [7]
    Left = 642
    Top = 378
    Width = 97
    Height = 25
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object Label6: TLabel [8]
    Left = 11
    Top = 163
    Width = 47
    Height = 16
    Caption = #1059#1089#1083#1091#1075#1072
  end
  object Label4: TLabel [9]
    Left = 11
    Top = 227
    Width = 141
    Height = 16
    Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086
  end
  object Label7: TLabel [10]
    Left = 186
    Top = 94
    Width = 85
    Height = 16
    Caption = #1042#1080#1076' '#1089#1086#1073#1099#1090#1080#1103
  end
  object Label8: TLabel [11]
    Left = 386
    Top = 94
    Width = 94
    Height = 16
    Caption = #1044#1072#1090#1072' '#1089#1086#1073#1099#1090#1080#1103
  end
  object Label9: TLabel [12]
    Left = 14
    Top = 12
    Width = 94
    Height = 16
    Caption = #1042#1080#1076' '#1086#1087#1077#1088#1072#1094#1080#1080
  end
  object Label10: TLabel [13]
    Left = 398
    Top = 12
    Width = 134
    Height = 16
    Caption = #1062#1077#1083#1077#1074#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
  end
  object Bevel1: TBevel [14]
    Left = 1
    Top = 75
    Width = 751
    Height = 5
  end
  inherited plBottom: TPanel
    Top = 530
    Width = 752
    TabOrder = 10
    ExplicitTop = 530
    ExplicitWidth = 752
    inherited btnCancel: TButton
      Left = 636
      ExplicitLeft = 636
    end
    inherited btnOk: TButton
      Left = 517
      ExplicitLeft = 517
    end
  end
  object dtOperationDate: TDBDateTimeEditEh [16]
    Left = 11
    Top = 118
    Width = 148
    Height = 24
    DataField = 'operation_datetime'
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    Kind = dtkDateTimeEh
    TabOrder = 2
    Visible = True
  end
  object edDocBase: TDBEditEh [17]
    Left = 11
    Top = 314
    Width = 524
    Height = 24
    DataField = 'doc_base_id'
    DynProps = <>
    EditButtons = <>
    Enabled = False
    TabOrder = 6
    Visible = True
  end
  object edDocInvoice: TDBEditEh [18]
    Left = 11
    Top = 378
    Width = 524
    Height = 24
    DataField = 'doc_invoice_id'
    DynProps = <>
    EditButtons = <>
    Enabled = False
    TabOrder = 8
    Visible = True
  end
  object edNote: TDBMemoEh [19]
    Left = 11
    Top = 448
    Width = 728
    Height = 70
    AutoSize = False
    DataField = 'note'
    DynProps = <>
    EditButtons = <>
    TabOrder = 9
    Visible = True
  end
  object leServiceKind: TDBSQLLookUp [20]
    Left = 11
    Top = 184
    Width = 521
    Height = 24
    DataField = 'service_id'
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 5
    Visible = True
    SqlSet = ssServiceKind
  end
  object lePerson: TDBSQLLookUp [21]
    Left = 11
    Top = 249
    Width = 521
    Height = 24
    DataField = 'person_id'
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 7
    Visible = True
    SqlSet = ssPerson
  end
  object cbEventKind: TDBComboBoxEh [22]
    Left = 186
    Top = 118
    Width = 177
    Height = 24
    DataField = 'event_type'
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object dtEvent: TDBDateTimeEditEh [23]
    Left = 386
    Top = 118
    Width = 146
    Height = 24
    DataField = 'event_date'
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 4
    Visible = True
  end
  object edOperationKind: TDBEditEh [24]
    Left = 14
    Top = 34
    Width = 349
    Height = 24
    DataField = 'k_code'
    DynProps = <>
    EditButtons = <>
    Enabled = False
    TabOrder = 0
    Visible = True
  end
  object edStateKind: TDBEditEh [25]
    Left = 398
    Top = 34
    Width = 341
    Height = 24
    DataField = 's_code'
    DynProps = <>
    EditButtons = <>
    Enabled = False
    TabOrder = 1
    Visible = True
  end
  object ssServiceKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from servicekinds order by scode')
    DownSql.Strings = (
      'select * from servicekinds order by scode')
    InitSql.Strings = (
      'select * from servicekinds where id = @id')
    KeyName = 'id'
    DisplayName = 'scode'
    Connection = dm.connMain
    Left = 188
    Top = 169
  end
  object ssPerson: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from persons order by person_name')
    DownSql.Strings = (
      'select * from persons order by person_name')
    InitSql.Strings = (
      'select * from persons where id = @id')
    KeyName = 'id'
    DisplayName = 'person_name'
    Connection = dm.connMain
    Left = 188
    Top = 233
  end
end
