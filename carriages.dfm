﻿inherited FormCarriages: TFormCarriages
  ActiveControl = dgData
  Caption = #1042#1072#1075#1086#1085#1099'/'#1087#1083#1072#1090#1092#1086#1088#1084#1099
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited plTop: TPanel
      inherited btFilter: TPngSpeedButton
        Left = 157
        ExplicitLeft = 164
      end
      inherited btExcel: TPngSpeedButton
        Left = 111
        ExplicitLeft = 118
      end
      inherited sbEdit: TPngSpeedButton
        Width = 33
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000015000000150806000000A917A5
          960000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E3360000040E4944415478DA95957B4C53571CC7
          BFB72D6D270A9DAEDD32758B6CAD053A1822DBC29B160A6232624C84051F9059
          190F37630C3845A2B02C06E764AC918DCC17C970230B284E44DC03A3C032B731
          14DC3A68C5F2909696AE404BDFDD29658973B56CBFBFEEB9B9E773BEBFC7F95E
          4A204AA0EB669CCCA9F159804EC15F3083989455F3A3F9EFF5CBEBE3563AED8E
          0D149DA663B058577FE9E99CF3BCA7629333CA1C4EC4D2C8C205FFE17439D96B
          439EBCFA79C3A6CB89A90D3C30589F2DE37005568B0946ED83A63F3563BB0695
          0A23959C96A98C097F2A647D04174E42F71756AB13A9B12B61B539B165F725F0
          D68421F7500D66F57A5C3E558D31457FB3C9A8DF4E89D337DE39208B10493687
          926D8E45B4B261548C23EFDD4ED0B861304E8C40189384C46C19F4E36AB49EAC
          826E64F8BD79E8DBB9A1A2AC2D02C0F218A56E37C06163B45F8F82B20E040924
          C8D85600656F0FDACF1C47B47413D2771493E78F5D7D9D6D038B43090F410150
          1360E1FE6B08E4A7202D271F0E8B19769B159323F7D0F9653D84AF26615CA9C0
          E4A8AA7C71E81206866F4FA2B8E23A96F1932021C0699D06DF34D4821F1D8B94
          EC37D1D9740E3D97CE1301F6F36E974DF678A867BAD8740CFFAA4149E54D040B
          C5487B43862952C78ED31F82BB3A0492AD85D04D4C9026D52269AD19078B6276
          073CB747EE1BEA49399081FB04585CD98DE5A1C9906E2B8456AD425BFD513CFD
          3C1FD21D7B3031328C8BF535908A6C385C1A8F07FAB95A695EFB5EDF501A05E5
          800EA527FAC0112422356727C606FB71415E8555FC5064EEDA0FCD881ACD75C7
          09D08A235512C060416EE90D6363E3EF21BEA14B19D8B7EF065ABF55214B5682
          D8AC6CDC6A6BC1D8D05DA4E414C0A8D3E22BF93188C3E670A4424CBE67424BEA
          9E77B00B57BE56F2FE0DF5D49249475EE1F7C013ABD07BAB07D2ED25884CC904
          9D1600DDE87DB4D47D8078BE09950712482303C85573634A65447E79175A2F0C
          F980B2E8D00E1A905FF1038E9E3C83EB1DEDF8E4440D5E2F2AC36A41049AE5D5
          887B610687F7BE0604B3BD7B4843A794FEA064269BCEF6E35A2F03EF7F746CDE
          633E959F42634323D84B39C88A0B4279519417685F700B963FA8D50B7DA7E83B
          CC054620322A1C7F0CF4413DD48FD9E9692444F370E8AD28502B08D0F690FDF8
          8592DAC0624749793714F70C58F3EC12AC0BE7216EDD337829923B7F204C0EEF
          C83D1CACC5D227E3A4FA4D0F4E2013CB851CE2B174A2CAE9CDC2E586CF205003
          6994A7FBBEA10B57735E8D3FD03FCC8B8E59F50CB696DDC4C596411E25C9D878
          A774A74824DD1CE6B9BB8B031E0DCA63EF345CF9E22EAA4F0F187A158617A978
          F1869F38C1ACE815412CE270FF41D5A3CC853FD0A8C68CE9597B098349D551AF
          C4A7479BCC0EE10C69004551FF1BEAB50AB73B80491FE0312CB7BB7FEE72FF05
          B2F0FB725C4E113F0000000049454E44AE426082}
        ExplicitLeft = 69
        ExplicitWidth = 33
        ExplicitHeight = 27
      end
      inherited Bevel2: TBevel
        Left = 106
        ExplicitLeft = 113
      end
      inherited sbClearFilter: TPngSpeedButton
        Left = 203
        ExplicitLeft = 203
      end
    end
    inherited dgData: TDBGridEh
      ReadOnly = False
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          EditMask = 'dd.mm.yyyy hh:nn:ss'
          FieldName = 'date_income'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1087#1086#1076#1072#1095#1080
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 94
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_paper_number'
          Footers = <>
          Title.Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086#1076#1072#1095#1080
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 93
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'pnum'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 112
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          Title.Caption = #1042#1080#1076
          Width = 135
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          Title.Caption = #1058#1077#1082#1091#1097#1080#1081' '#1089#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
          Width = 128
        end
        item
          CellButtons = <>
          Color = 13499846
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1058#1077#1082#1091#1097#1077#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 124
        end
        item
          CellButtons = <>
          Color = 13499846
          DynProps = <>
          EditButtons = <>
          FieldName = 'real_state_code'
          Footers = <>
          Title.Caption = #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 105
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_next_repair'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1095#1077#1088#1077#1076'. '#1088#1077#1084#1086#1085#1090#1072
          Width = 70
        end
        item
          CellButtons = <>
          Color = 13036283
          DynProps = <>
          EditButtons = <>
          FieldName = 'techcond_code'
          Footers = <>
          Title.Caption = #1058#1077#1093'. '#1089#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 104
        end
        item
          CellButtons = <>
          Checkboxes = False
          Color = 13036283
          DisplayFormat = 'dd.mm.yyyy'
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_inspection'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1090#1077#1093'. '#1089#1086#1089#1090#1086#1103#1085#1080#1103
          Width = 104
        end
        item
          CellButtons = <>
          Checkboxes = True
          Color = 13036283
          DynProps = <>
          EditButtons = <>
          FieldName = 'isdefective'
          Footers = <>
          Title.Caption = #1053#1077'- '#1080#1089#1087#1088#1072'- '#1074#1077#1085'?'
          Width = 57
        end
        item
          CellButtons = <>
          Checkboxes = True
          Color = 8454143
          DynProps = <>
          EditButtons = <>
          FieldName = 'need_inspection'
          Footers = <>
          Title.Caption = #1054#1089#1084#1086#1090#1088
          Width = 62
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'model_code'
          Footers = <>
          Title.Caption = #1052#1086#1076#1077#1083#1100
          Width = 103
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'start_datetime'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103
          Width = 82
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    inherited plObjects: TPanel
      inherited dgObjects: TDBGridEh
        Font.Height = -11
        TitleParams.Font.Height = -11
      end
    end
  end
  inherited Panel1: TPanel
    inherited dgStates: TDBGridEh
      Font.Height = -11
      TitleParams.Font.Height = -11
    end
    inherited Panel3: TPanel
      inherited DBGridEh1: TDBGridEh
        Font.Height = -11
        TitleParams.Font.Height = -11
      end
    end
  end
  inherited pmGrid: TPopupMenu
    object N8: TMenuItem [3]
      Caption = '-'
    end
    object N9: TMenuItem [4]
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1103#1095#1077#1081#1082#1080
      ShortCut = 46
      OnClick = N9Click
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select c.*, o.start_datetime,  o.id as object_id, '
      
        '(select isnull(code,name) from counteragents c1 where c1.id = o.' +
        'owner_id) as owner_code,'
      
        '(select code from carriagekinds k where k.id = c.kind_id) as kin' +
        'd_code,'
      
        '(select model_code from carriagemodels m where m.id = c.model_id' +
        ') as model_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select code from objectstatekinds s where s.id = o.real_state_i' +
        'd) as real_state_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code'
      
        'from objects o, carriages c where o.id = c.id and o.object_type ' +
        '= '#39'carriage'#39
      'and dbo.filter_objects(:guid, c.pnum) = 1')
    SelectCommand.Parameters = <
      item
        Name = 'guid'
        Attributes = [paSigned]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update carriages'
      'set'
      '  pnum = :pnum,'
      '  kind_id = :kind_id,'
      '  techcond_id = :techcond_id,'
      '  model_id = :model_id,'
      '  note = :note,'
      '  isdefective = :isdefective,'
      '  need_inspection = :need_inspection,'
      '  date_income= :date_income,'
      '  car_paper_number= :car_paper_number,'
      '  date_next_repair = :date_next_repair'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'pnum'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'techcond_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'model_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'isdefective'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'need_inspection'
        Size = -1
        Value = Null
      end
      item
        Name = 'date_income'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_paper_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'date_next_repair'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'begin'
      ''
      'declare @newid int;'
      ''
      'exec InsertObject '#39'carriage'#39', @newid output;'
      ''
      'insert into carriages'
      
        '  (id, pnum, kind_id, techcond_id, model_id, note, isdefective, ' +
        'need_inspection, date_next_repair)'
      'values'
      
        '  (@newid, :pnum, :kind_id, :techcond_id, :model_id, :note, :isd' +
        'efective, :need_inspection, :date_next_repair)'
      ''
      'end;')
    InsertCommand.Parameters = <
      item
        Name = 'pnum'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'techcond_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'model_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'isdefective'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'need_inspection'
        Size = -1
        Value = Null
      end
      item
        Name = 'date_next_repair'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from objects where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select c.*, o.start_datetime,  o.id as object_id, '
      
        '(select isnull(code,name) from counteragents c1 where c1.id = o.' +
        'owner_id) as owner_code,'
      
        '(select code from carriagekinds k where k.id = c.kind_id) as kin' +
        'd_code,'
      
        '(select model_code from carriagemodels m where m.id = c.model_id' +
        ') as model_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code'
      
        'from objects o, carriages c where o.id = c.id and o.object_type ' +
        '= '#39'carriage'#39
      'and o.id = :current_id'
      '')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited meStates: TMemTableEh
    DetailFields = 'object_id'
    MasterFields = 'object_id'
  end
  inherited meOwner: TMemTableEh
    MasterFields = 'object_id'
  end
end
