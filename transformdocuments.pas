﻿unit transformdocuments;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Vcl.Mask, DBCtrlsEh;

type
  TFormTransformDocuments = class(TFormGrid)
    sbClearAll: TPngSpeedButton;
    sbCheckAll: TPngSpeedButton;
    qrData: TADOQuery;
    plInfo: TPanel;
    edContainerNum: TDBEditEh;
    btRequery: TButton;
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure sbCheckAllClick(Sender: TObject);
    procedure sbClearAllClick(Sender: TObject);
    procedure btFilterClick(Sender: TObject);
    procedure dgDataCellClick(Column: TColumnEh);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure edContainerNumKeyPress(Sender: TObject; var Key: Char);
    procedure btRequeryClick(Sender: TObject);
  private
    { Private declarations }
  public
    g: string;
  end;

var
  FormTransformDocuments: TFormTransformDocuments;

implementation

{$R *.dfm}

uses filterlite, dmu, main;

procedure TFormTransformDocuments.btFilterClick(Sender: TObject);
begin
  FormFilterLite.ShowModal;
  if (FormFilterLite.ModalResult = mrOk) or (FormFilterLite.ModalResult = mrYes) then
  begin

    sbClearAllClick(nil);

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := '-1';
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := FormFilterLite.conditions;
    dm.spCreateFilter.ExecProc;

    qrData.Close;
    qrData.Open;

    sbCheckAllClick(nil);

    btFilter.Down := true;

  end;
end;

procedure TFormTransformDocuments.btnOkClick(Sender: TObject);
begin
  inherited;
  qrData.Filter := 'checkcolumn=1';
  qrData.Filtered := true;
  if qrData.RecordCount = 0 then
  begin
    qrData.Filter := '';
    qrData.Filtered := false;
    ShowMessage('Необходимо отметить хотя бы один контейнер');
    self.ModalResult := mrNone;
  end;
  qrData.Filter := '';
  qrData.Filtered := false;
end;

procedure TFormTransformDocuments.btRequeryClick(Sender: TObject);
begin
  qrData.Filter := '';
  qrData.Filtered := false;
  edContainerNum.Text := '';

  dm.spCreateFilter.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
  dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := '-1';
  dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := '';
  dm.spCreateFilter.ExecProc;

  qrData.Close;
  qrData.Open;

  btFilter.Down := false;

end;

procedure TFormTransformDocuments.dgDataCellClick(Column: TColumnEh);
begin
  if Column.FieldName = 'checkcolumn' then
  begin
    dgData.DataSource.DataSet.Edit;

    if dgData.DataSource.DataSet.FieldByName('checkcolumn').AsInteger = 0 then
      dgData.DataSource.DataSet.FieldByName('checkcolumn').Value := 1
    else
      dgData.DataSource.DataSet.FieldByName('checkcolumn').Value := 0;

    dgData.DataSource.DataSet.Post;

  end;
end;

procedure TFormTransformDocuments.dgDataDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

  if dgData.DataSource.DataSet.FieldByName('docexists').AsInteger = 1 then
    dgData.Canvas.Brush.Color := $00BFC2FF;

   dgData.DefaultDrawColumnCell(Rect, DataCol, Column, State);

end;

procedure TFormTransformDocuments.edContainerNumKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin

    if edContainerNum.Text<>'' then
    begin
      qrData.Filter := 'cont_num like ''%'+edContainerNum.Text+'%''';
      qrData.Filtered := true;
      dgData.SetFocus;
    end else
    begin
      qrData.Filter := '';
      qrData.Filtered := false;
    end;

    Key := #0;

  end;
end;

procedure TFormTransformDocuments.FormShow(Sender: TObject);
begin

  qrData.Filter := '';
  qrData.Filtered := false;
  edContainerNum.Text := '';

  dm.spCreateFilter.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
  dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := '-1';
  dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := '';
  dm.spCreateFilter.ExecProc;

  qrData.Close;
  qrData.Open;

  btFilter.Down := false;

  if qrData.RecordCount>1 then
  begin
    plInfo.Show;
    sbClearAllClick(self);
  end else
  begin
    plInfo.Hide;
  end;
end;

procedure TFormTransformDocuments.sbCheckAllClick(Sender: TObject);
begin
  dsData.DataSet.First;
  while not dsData.DataSet.Eof do
  begin
    dsData.DataSet.Edit;
    dsData.DataSet.FieldByName('checkcolumn').Value := 1;
    dsData.DataSet.Post;
    dsData.DataSet.Next;
  end;
  dsData.DataSet.First;
end;

procedure TFormTransformDocuments.sbClearAllClick(Sender: TObject);
begin
  dsData.DataSet.First;
  while not dsData.DataSet.Eof do
  begin
    dsData.DataSet.Edit;
    dsData.DataSet.FieldByName('checkcolumn').Value := 0;
    dsData.DataSet.Post;
    dsData.DataSet.Next;
  end;
  dsData.DataSet.First;
end;

end.
