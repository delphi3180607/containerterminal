﻿inherited FormEditOrderSpec: TFormEditOrderSpec
  Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
  ClientHeight = 513
  ClientWidth = 756
  Font.Height = -12
  ExplicitWidth = 762
  ExplicitHeight = 541
  PixelsPerInch = 96
  TextHeight = 14
  object sbContainer: TSpeedButton [0]
    Left = 562
    Top = 26
    Width = 31
    Height = 24
    Caption = '...'
    OnClick = sbContainerClick
  end
  object sbCargoType: TSpeedButton [1]
    Left = 532
    Top = 302
    Width = 31
    Height = 22
    Caption = '...'
    OnClick = sbCargoTypeClick
  end
  object nuWeightCargo: TDBNumberEditEh [2]
    Left = 200
    Top = 361
    Width = 120
    Height = 22
    ControlLabel.Width = 96
    ControlLabel.Height = 14
    ControlLabel.Caption = #1042#1077#1089' '#1073#1088#1091#1090#1090#1086', '#1082#1075'.'
    ControlLabel.Visible = True
    DataField = 'weight_cargo'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    TabOrder = 8
    Visible = True
  end
  object neAmount: TDBNumberEditEh [3]
    Left = 606
    Top = 28
    Width = 120
    Height = 22
    ControlLabel.Width = 75
    ControlLabel.Height = 14
    ControlLabel.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
    ControlLabel.Visible = True
    DataField = 'amount'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    TabOrder = 3
    Visible = True
  end
  object laCargoType: TDBSQLLookUp [4]
    Left = 200
    Top = 303
    Width = 328
    Height = 22
    ControlLabel.Width = 62
    ControlLabel.Height = 14
    ControlLabel.Caption = #1058#1080#1087' '#1075#1088#1091#1079#1072
    ControlLabel.Visible = True
    DataField = 'cargotype_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    HighlightRequired = True
    TabOrder = 6
    Visible = True
    SqlSet = dm.ssCargoType
    RowCount = 0
  end
  object cbIsEmpty: TDBCheckBoxEh [5]
    Left = 574
    Top = 306
    Width = 97
    Height = 17
    Caption = #1055#1086#1088#1086#1078#1085#1080#1081
    DataField = 'isempty'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 7
    OnClick = cbIsEmptyClick
  end
  object edContainerNum: TDBEditEh [6]
    Left = 8
    Top = 28
    Width = 166
    Height = 22
    CharCase = ecUpperCase
    Color = 14810109
    ControlLabel.Width = 123
    ControlLabel.Height = 14
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
    ControlLabel.Visible = True
    DataField = 'container_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    MaxLength = 11
    StyleElements = [seFont, seBorder]
    TabOrder = 0
    Visible = True
    OnExit = edContainerNumExit
    OnGetFieldData = edContainerNumGetFieldData
    OnKeyPress = edContainerNumKeyPress
    EditMask = 'cccc9999999;0;?'
  end
  object edSealNumber: TDBEditEh [7]
    Left = 8
    Top = 303
    Width = 182
    Height = 22
    ControlLabel.Width = 93
    ControlLabel.Height = 14
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
    ControlLabel.Visible = True
    DataField = 'seal_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    TabOrder = 5
    Visible = True
  end
  object Panel1: TPanel [8]
    Left = 0
    Top = 64
    Width = 756
    Height = 201
    BevelOuter = bvNone
    TabOrder = 4
    Visible = False
    object nuWeight: TDBNumberEditEh
      Left = 8
      Top = 23
      Width = 98
      Height = 22
      ControlLabel.Width = 47
      ControlLabel.Height = 14
      ControlLabel.Caption = #1042#1077#1089', '#1082#1075'.'
      ControlLabel.Visible = True
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Visible = True
    end
    object dtInspect: TDBDateTimeEditEh
      Left = 229
      Top = 80
      Width = 154
      Height = 22
      ControlLabel.Width = 92
      ControlLabel.Height = 14
      ControlLabel.Caption = #1044#1072#1090#1072' '#1086#1089#1074#1080#1076#1077#1090'.'
      ControlLabel.Visible = True
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      Kind = dtkDateEh
      ParentFont = False
      TabOrder = 4
      Visible = True
    end
    object cbPicture: TDBCheckBoxEh
      Left = 160
      Top = 83
      Width = 63
      Height = 17
      Caption = #1060#1086#1090#1086
      DynProps = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      State = cbGrayed
      TabOrder = 3
    end
    object dtDateStiker: TDBDateTimeEditEh
      Left = 394
      Top = 82
      Width = 139
      Height = 22
      ControlLabel.Width = 183
      ControlLabel.Height = 14
      ControlLabel.Caption = #1044#1072#1090#1072' '#1089#1083#1077#1076'.'#1086#1089#1074#1080#1076#1077#1090'. ('#1089#1090#1080#1082#1077#1088')'
      ControlLabel.Visible = True
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      Kind = dtkDateEh
      ParentFont = False
      TabOrder = 5
      Visible = True
    end
    object dtDateMade: TDBDateTimeEditEh
      Left = 8
      Top = 81
      Width = 143
      Height = 22
      ControlLabel.Width = 124
      ControlLabel.Height = 14
      ControlLabel.Caption = #1044#1072#1090#1072' '#1080#1079#1075#1086#1090#1086#1074#1083#1077#1085#1080#1103
      ControlLabel.Visible = True
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      Kind = dtkDateEh
      ParentFont = False
      TabOrder = 2
      Visible = True
    end
    object nuCarrying: TDBNumberEditEh
      Left = 120
      Top = 23
      Width = 124
      Height = 22
      ControlLabel.Width = 144
      ControlLabel.Height = 14
      ControlLabel.Caption = #1043#1088#1091#1079#1086#1087#1086#1076#1100#1077#1084#1085#1086#1089#1090#1100', '#1082#1075'.'
      ControlLabel.Visible = True
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      Visible = True
    end
    object cbIsDefective: TDBCheckBoxEh
      Left = 9
      Top = 124
      Width = 181
      Height = 17
      Caption = #1044#1077#1092#1077#1082#1090#1085#1099#1081' '#1082#1086#1085#1090#1077#1081#1085#1077#1088
      DynProps = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbGrayed
      TabOrder = 7
    end
    object edNote: TDBEditEh
      Left = 9
      Top = 173
      Width = 627
      Height = 22
      ControlLabel.Width = 194
      ControlLabel.Height = 14
      ControlLabel.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077' '#1082' '#1085#1077#1080#1089#1087#1088#1072#1074#1085#1086#1089#1090#1080
      ControlLabel.Visible = True
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      Visible = True
    end
    object cbNotReady: TDBCheckBoxEh
      Left = 229
      Top = 124
      Width = 201
      Height = 17
      Caption = #1053#1077' '#1075#1086#1090#1086#1074' '#1082' '#1087#1077#1088#1077#1074#1086#1079#1082#1077
      DynProps = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbGrayed
      TabOrder = 9
    end
    object cbACEP: TDBCheckBoxEh
      Left = 562
      Top = 83
      Width = 140
      Height = 17
      Caption = 'ACEP'
      DynProps = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbGrayed
      TabOrder = 6
    end
  end
  object edExternalOrderNum: TDBEditEh [9]
    Left = 8
    Top = 426
    Width = 328
    Height = 22
    ControlLabel.Width = 143
    ControlLabel.Height = 14
    ControlLabel.Caption = #1053#1086#1084#1077#1088' ('#1085#1086#1084#1077#1088#1072') '#1043#1059'-12'
    ControlLabel.Visible = True
    DataField = 'external_order_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    TabOrder = 10
    Visible = True
  end
  object leContainerKind: TDBSQLLookUp [10]
    Left = 181
    Top = 28
    Width = 155
    Height = 22
    Color = 14810109
    ControlLabel.Width = 106
    ControlLabel.Height = 14
    ControlLabel.Caption = #1042#1080#1076' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
    ControlLabel.Visible = True
    DataField = 'container_kind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    StyleElements = [seFont, seBorder]
    TabOrder = 1
    Visible = True
    SqlSet = ssContainerKind
    RowCount = 0
  end
  object laContainerOwner: TDBSQLLookUp [11]
    Left = 341
    Top = 28
    Width = 218
    Height = 22
    Color = 14810109
    ControlLabel.Width = 84
    ControlLabel.Height = 14
    ControlLabel.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
    ControlLabel.Visible = True
    DataField = 'container_owner_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    HighlightRequired = True
    StyleElements = [seFont, seBorder]
    TabOrder = 2
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  inherited plBottom: TPanel
    Top = 472
    Width = 756
    TabOrder = 12
    ExplicitTop = 472
    ExplicitWidth = 756
    inherited btnCancel: TButton
      Left = 640
      ExplicitLeft = 640
    end
    inherited btnOk: TButton
      Left = 521
      OnClick = btnOkClick
      ExplicitLeft = 521
    end
  end
  object cbIsSigned: TDBCheckBoxEh [13]
    Left = 360
    Top = 427
    Width = 129
    Height = 17
    Caption = #1055#1086#1076#1087#1080#1089#1072#1085#1072' ?'
    DataField = 'external_order_signed'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 11
  end
  object plState: TPanel [14]
    Left = -1
    Top = 56
    Width = 754
    Height = 149
    BevelOuter = bvNone
    TabOrder = 13
    object lbWarning: TLabel
      Left = 1
      Top = 59
      Width = 753
      Height = 94
      AutoSize = False
      Color = clRed
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = False
      Visible = False
      WordWrap = True
      StyleElements = [seBorder]
    end
    object txRealState: TDBEditEh
      Left = 9
      Top = 25
      Width = 230
      Height = 20
      Color = 11064319
      ControlLabel.Width = 213
      ControlLabel.Height = 14
      ControlLabel.Caption = #1052#1077#1089#1090#1086#1085#1072#1093#1086#1078#1076#1077#1085#1080#1077' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
      ControlLabel.Font.Charset = DEFAULT_CHARSET
      ControlLabel.Font.Color = clWindowText
      ControlLabel.Font.Height = -12
      ControlLabel.Font.Name = 'Verdana'
      ControlLabel.Font.Style = [fsBold, fsItalic]
      ControlLabel.ParentFont = False
      ControlLabel.Visible = True
      Ctl3D = False
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsItalic]
      HighlightRequired = True
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      StyleElements = [seFont]
      TabOrder = 0
      Visible = True
    end
    object txFormalState: TDBEditEh
      Left = 255
      Top = 25
      Width = 230
      Height = 20
      Color = 11064319
      ControlLabel.Width = 169
      ControlLabel.Height = 14
      ControlLabel.Caption = #1059#1095#1072#1089#1090#1080#1077' '#1074' '#1085#1086#1074#1086#1081' '#1079#1072#1103#1074#1082#1077
      ControlLabel.Font.Charset = DEFAULT_CHARSET
      ControlLabel.Font.Color = clWindowText
      ControlLabel.Font.Height = -12
      ControlLabel.Font.Name = 'Verdana'
      ControlLabel.Font.Style = [fsBold, fsItalic]
      ControlLabel.ParentFont = False
      ControlLabel.Visible = True
      Ctl3D = False
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsItalic]
      HighlightRequired = True
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      StyleElements = [seFont]
      TabOrder = 1
      Visible = True
    end
  end
  object cbSealWaste: TDBComboBoxEh [15]
    Left = 9
    Top = 359
    Width = 58
    Height = 26
    ControlLabel.Width = 108
    ControlLabel.Height = 14
    ControlLabel.Caption = #1055#1083#1086#1084#1073#1072' '#1075#1086#1076#1085#1072#1103' ?'
    ControlLabel.Visible = True
    DataField = 'issealwaste'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Items.Strings = (
      '+'
      '-'
      '?'
      ' ')
    KeyItems.Strings = (
      '+'
      '-'
      '?'
      ' ')
    LimitTextToListValues = True
    ParentFont = False
    TabOrder = 9
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 303
    Top = 470
  end
  inherited qrAux: TADOQuery
    Left = 348
    Top = 470
  end
  object ssContainerKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from containerkinds order by code')
    DownSql.Strings = (
      'select * from containerkinds order by code')
    InitSql.Strings = (
      'select * from containerkinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 275
    Top = 16
  end
  object drvContainers: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'declare @cnum varchar(50), @docid int, @extraorder int;'
      ''
      'select @cnum = :cnum, @docid = :docid, @extraorder = 0;'
      ''
      'select @extraorder = 1 where exists ('
      '  select 1 '
      '  from documents d'
      '  inner join docorder o on (o.id = d.id )'
      '  inner join docorderspec os on (os.doc_id = d.id )'
      '  inner join containers c on (c.id = os.container_id) '
      
        '  left outer join objectstatehistory osh on (osh.doc_id = d.id a' +
        'nd osh.object_id = c.id)'
      '  where os.doc_id <> @docid and c.cnum = @cnum'
      
        '  and o.dlv_type_id = (select dlv_type_id from docorder o1 where' +
        ' o1.id = @docid)'
      '  and ('
      '      isnull(d.isconfirmed,0) = 0 or'
      
        '      (select count(*) from objectstatehistory osh1 where osh1.t' +
        'ask_id = osh.task_id and osh1.object_id = osh.object_id)=1'
      '   )'
      ');'
      ''
      'select @extraorder = 2 where exists ('
      '  select 1 '
      '  from documents d'
      '  inner join docorder o on (o.id = d.id )'
      '  inner join docorderspec os on (os.doc_id = d.id )'
      '  inner join containers c on (c.id = os.container_id) '
      
        '  left outer join objectstatehistory osh on (osh.doc_id = d.id a' +
        'nd osh.object_id = c.id)'
      '  where os.doc_id = @docid and c.cnum = @cnum'
      ');'
      ''
      
        'select top 1 c.*, o.start_datetime,  o.id as object_id, o.owner_' +
        'id,'
      
        '(select code from counteragents c1 where c1.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from containerkinds k where k.id = c.kind_id) as ki' +
        'nd_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' formal_state_code,'
      
        '(select code from objectstatekinds s where s.id = o.real_state_i' +
        'd) as real_state_code,'
      '@extraorder as extraorder'
      
        'from objects o, containers c where o.id = c.id and o.object_type' +
        ' = '#39'container'#39
      'and ((c.cnum = @cnum) or (@cnum = '#39'-1-'#39'))')
    SelectCommand.Parameters = <
      item
        Name = 'cnum'
        DataType = ftString
        Size = 1
        Value = ' '
      end
      item
        Name = 'docid'
        DataType = ftInteger
        Size = -1
        Value = 1
      end>
    UpdateCommand.CommandText.Strings = (
      'update containers'
      'set'
      '  cnum = :cnum,'
      '  kind_id = :kind_id,'
      '  size = :size,'
      '  container_weight = :container_weight,'
      '  carrying = :carrying,'
      '  date_made = :date_made,'
      '  date_inspection = :date_inspection,'
      '  date_stiker = :date_stiker,'
      '  picture = :picture,'
      '  note = :note,'
      '  isdefective = :isdefective'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'cnum'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'size'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_weight'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'carrying'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'date_made'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_inspection'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_stiker'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'picture'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 850
        Value = Null
      end
      item
        Name = 'isdefective'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.CommandText.Strings = (
      
        'select top 1 c.*, o.start_datetime,  o.id as object_id, o.owner_' +
        'id,'
      
        '(select code from counteragents c1 where c1.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from containerkinds k where k.id = c.kind_id) as ki' +
        'nd_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' formal_state_code,'
      
        '(select code from objectstatekinds s where s.id = o.real_state_i' +
        'd) as real_state_code,'
      '0 as extraorder'
      'from objects o, containers c '
      'where o.id = c.id and o.object_type = '#39'container'#39
      'and c.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 160
    Top = 467
  end
  object meContainers: TMemTableEh
    Params = <>
    DataDriver = drvContainers
    Left = 232
    Top = 467
  end
  object drvCargo: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select c.*, o.start_datetime,  o.id as object_id, c.weight_doc, ' +
        'c.weight_fact, c.isempty, c.seal_number,'
      
        '(select code from counteragents c1 where c1.id = t.forwarder_id)' +
        ' as forwarder_code,'
      
        '(select code from counteragents c2 where c2.id = t.consignee_id)' +
        ' as consignee_code,'
      
        '(case when  c.isempty=1 then '#39'{'#1087#1091#1089#1090#1086#1081'}'#39' else  (select code from ' +
        'cargotypes t where t.id = c.type_id) end) as type_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code'
      
        'from cargos c, objects o left outer join tasks t on (t.id = o.cu' +
        'rrent_task_id)'
      'where o.id = c.id and o.object_type = '#39'cargo'#39' and c.id = :id'
      '')
    SelectCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update cargos'
      'set'
      '  cnum = :cnum,'
      '  type_id = :type_id,'
      '  isempty = :isempty,'
      '  note = :note,'
      '  weight_sender = :weight_sender, '
      '  weight_doc = :weight_doc, '
      '  weight_fact = :weight_fact,'
      '  seal_number = :seal_number'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'cnum'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'weight_sender'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_doc'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_fact'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'begin'
      'declare @newid int;'
      'exec InsertObject '#39'cargo'#39', @newid output;'
      'insert into cargos'
      
        '  (id, cnum, type_id, note, weight_sender, weight_doc, weight_fa' +
        'ct, seal_number,  isempty)'
      'values'
      
        '  (@newid,  :cnum, :type_id, :note, :weight_sender, :weight_doc,' +
        ' :weight_fact, :seal_number, :isempty);'
      'end;')
    InsertCommand.Parameters = <
      item
        Name = 'cnum'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'weight_sender'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_doc'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_fact'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from objects where  id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select c.*, o.start_datetime,  o.id as object_id, c.weight_doc, ' +
        'c.weight_fact, c.isempty, c.seal_number,'
      
        '(select code from counteragents c1 where c1.id = t.forwarder_id)' +
        ' as forwarder_code,'
      
        '(select code from counteragents c2 where c2.id = t.consignee_id)' +
        ' as consignee_code,'
      
        '(case when  c.isempty=1 then '#39'{'#1087#1091#1089#1090#1086#1081'}'#39' else  (select code from ' +
        'cargotypes t where t.id = c.type_id) end) as type_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code'
      
        'from cargos c, objects o left outer join tasks t on (t.id = o.cu' +
        'rrent_task_id)'
      'where o.id = c.id and o.object_type = '#39'cargo'#39
      'and o.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 32
    Top = 465
  end
  object meCargo: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvCargo
    Left = 96
    Top = 465
  end
end
