﻿unit tool;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, SynEdit, SynMemo, Vcl.StdCtrls,
  Vcl.ExtCtrls, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ComCtrls, MemTableDataEh,
  Data.DB, Data.Win.ADODB, MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Mask,
  DBCtrlsEh, Vcl.Buttons, PngSpeedButton, DBSQLLookUp;

type
  TFormTool = class(TForm)
    plBottom: TPanel;
    Button1: TButton;
    btSave: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    seSelect: TSynMemo;
    dgColumns: TDBGridEh;
    TabSheet3: TTabSheet;
    dgTableLinks: TDBGridEh;
    drvColumns: TADODataDriverEh;
    meColumns: TMemTableEh;
    dcColumns: TDataSource;
    drvLinks: TADODataDriverEh;
    meLinks: TMemTableEh;
    dsLinks: TDataSource;
    plTop: TPanel;
    edName: TDBEditEh;
    Label1: TLabel;
    Panel1: TPanel;
    sbDelete: TPngSpeedButton;
    sbAdd: TPngSpeedButton;
    sbEdit: TPngSpeedButton;
    Panel2: TPanel;
    PngSpeedButton1: TPngSpeedButton;
    PngSpeedButton2: TPngSpeedButton;
    PngSpeedButton3: TPngSpeedButton;
    Label2: TLabel;
    neStartKind: TDBNumberEditEh;
    dsLocal: TDataSource;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Label3: TLabel;
    cbForms: TDBComboBoxEh;
    seInsert: TSynMemo;
    seUpdate: TSynMemo;
    TabSheet7: TTabSheet;
    seDelete: TSynMemo;
    procedure sbAddClick(Sender: TObject);
    procedure sbDeleteClick(Sender: TObject);
    procedure sbEditClick(Sender: TObject);
  private
    { Private declarations }
  public
    formId: integer;
  end;

var
  FormTool: TFormTool;


implementation

{$R *.dfm}

uses dmu, editcolumn, functions;

procedure TFormTool.sbAddClick(Sender: TObject);
begin

  self.meColumns.Append;
  self.meColumns.FieldByName('form_id').Value := self.formId;
  FormEditColumn.ShowModal;

  if FormEditColumn.ModalResult = mrOk then
    self.meColumns.Post
  else
    self.meColumns.Cancel;

end;

procedure TFormTool.sbDeleteClick(Sender: TObject);
begin
  if fQYN('Удалить колонку ?') then
  begin
     self.meColumns.Delete;
  end;
end;

procedure TFormTool.sbEditClick(Sender: TObject);
begin

  self.meColumns.Edit;
  FormEditColumn.ShowModal;

  if FormEditColumn.ModalResult = mrOk then
    self.meColumns.Post
  else
    self.meColumns.Cancel;

end;

end.
