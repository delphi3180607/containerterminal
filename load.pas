﻿unit load;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, docflow, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, Functions, Vcl.Mask, DBCtrlsEh, grid, Vcl.ComCtrls,
  System.ImageList, Vcl.ImgList, PngImageList;

type
  TFormLoads = class(TFormDocFlow)
    N20: TMenuItem;
    sbFilterPlan: TPngSpeedButton;
    sbAppendDoc: TPngSpeedButton;
    N17: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    sbAll: TPngSpeedButton;
    N23: TMenuItem;
    N24: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N25: TMenuItem;
    sbInspection: TPngSpeedButton;
    sbWagon: TPngSpeedButton;
    drvCarriages: TADODataDriverEh;
    meCarriages: TMemTableEh;
    N26: TMenuItem;
    procedure dgDataCellClick(Column: TColumnEh);
    procedure N20Click(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure sbFilterPlanClick(Sender: TObject);
    procedure sbAppendDocClick(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure N17Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure meDataAfterOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure dgDataApplyFilter(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure dgDataColEnter(Sender: TObject);
    procedure dgDataColumns15EditButtonClick(Sender: TObject;
      var Handled: Boolean);
    procedure meDataBeforePost(DataSet: TDataSet);
    procedure dgDataKeyPress(Sender: TObject; var Key: Char);
    procedure sbEditClick(Sender: TObject);
    procedure sbAllClick(Sender: TObject);
    procedure N24Click(Sender: TObject);
    procedure meDataBeforeEdit(DataSet: TDataSet);
    procedure sbCancelConfirmClick(Sender: TObject);
    procedure dgDataDblClick(Sender: TObject);
    procedure dgDataColumns0HintShowPause(Sender: TCustomDBGridEh;
      CursorPos: TPoint; Cell: TGridCoord; InCellCursorPos: TPoint;
      Column: TColumnEh; var HintPause: Integer; var Processed: Boolean);
    procedure sbDeleteClick(Sender: TObject);
    procedure sbInspectionClick(Sender: TObject);
    procedure dgDataColumns4HintShowPause(Sender: TCustomDBGridEh;
      CursorPos: TPoint; Cell: TGridCoord; InCellCursorPos: TPoint;
      Column: TColumnEh; var HintPause: Integer; var Processed: Boolean);
    procedure sbWagonClick(Sender: TObject);
    procedure sbConfirmClick(Sender: TObject);
    procedure N26Click(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure AssignSelField(colname: string); override;
  public
    planid: integer;
    plantext: string;
    showall: boolean;
    UnicVals: TStringList;
    procedure Init; override;
    procedure PartialInit; override;
  end;

var
  FormLoads: TFormLoads;

implementation

{$R *.dfm}

uses editload, EditPlaceContainers, selectobjects, loadschess, loadplan,
  DispatchDocsList, ChangeCarriages, dmu, SelectPath, filterlite,
  transformdocuments, main, editmtuandpicture, filter, filterloads, setcheck,
  editcarriage, ContainersInOrders;

procedure TFormLoads.dgDataApplyFilter(Sender: TObject);
var i: integer; pnum: string;
begin
  inherited;
 self.meDataAfterScroll(meData);

 UnicVals.Clear;

 for i := 0 to meData.RecordsView.Count-1 do
 begin
   pnum := meData.RecordsView.Rec[i].DataValues['pnum', TDataValueVersionEh.dvvCurValueEh];
   UnicVals.Add(pnum);
 end;

 dgData.FieldColumns['pnum'].Title.Caption := 'Вaгон/'+ UnicVals.Count.ToString();

end;


procedure TFormLoads.PartialInit;
begin
 meData.Close;
 meData.Open;
end;


procedure TFormLoads.dgDataCellClick(Column: TColumnEh);
begin
  if Column.FieldName = 'cnum' then sbEditClick(self);
  dgDataColEnter(self);
end;

procedure TFormLoads.dgDataColEnter(Sender: TObject);
begin

  if
  (dgData.Columns[dgData.Col-1].FieldName = 'dispatch_numbers') or
  (dgData.Columns[dgData.Col-1].FieldName = 'receipt_number') or
  (dgData.Columns[dgData.Col-1].FieldName = 'receipt_summa') or
  (dgData.Columns[dgData.Col-1].FieldName = 'mtu_code') or
  (dgData.Columns[dgData.Col-1].FieldName = 'picture_code') or
  (dgData.Columns[dgData.Col-1].FieldName = 'fulfilled')
  then
  begin
    allow_edit := true;
    meData.ReadOnly := false;
  end else
    allow_edit := false;

end;

procedure TFormLoads.dgDataColumns0HintShowPause(Sender: TCustomDBGridEh;
  CursorPos: TPoint; Cell: TGridCoord; InCellCursorPos: TPoint;
  Column: TColumnEh; var HintPause: Integer; var Processed: Boolean);
var p: TPoint;
begin
  p := CursorPos;
  ShowSpecialHint(Column, p,'Двойной клик мышкой, чтобы поставить/снять отметку. Также, можно использовать протяжку.');
  Processed := true;
end;

procedure TFormLoads.dgDataColumns15EditButtonClick(Sender: TObject;
  var Handled: Boolean);
begin
  allow_edit := true;
  meData.ReadOnly := false;
end;

procedure TFormLoads.dgDataColumns4HintShowPause(Sender: TCustomDBGridEh;
  CursorPos: TPoint; Cell: TGridCoord; InCellCursorPos: TPoint;
  Column: TColumnEh; var HintPause: Integer; var Processed: Boolean);
var p: TPoint;
begin
  p := CursorPos;
  ShowSpecialHint(Column, p,'Снимите отметку на любом контейнере и нажмите F5, если вагон прошел осмотр. Отметка снимется на всем вагоне, при наличии погрузки.');
  Processed := true;
end;

procedure TFormLoads.dgDataDblClick(Sender: TObject);
begin
  inherited;

  if pos('outcome_dispatch',FormMain.confirmrestrict_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  if dgData.SelectedField.FieldName = 'fulfilled' then
  begin
    if meData.FieldByName('fulfilled').AsString <> '' then
    begin
      allow_edit := true;
      meData.ReadOnly := false;
      meData.Edit;
      meData.FieldByName('fulfilled').Value := '';
      meData.Post;
      allow_edit := false;
    end else
    begin
      allow_edit := true;
      meData.ReadOnly := false;
      meData.Edit;
      meData.FieldByName('fulfilled').Value := '+';
      meData.Post;
      allow_edit := false;
    end;
  end;
end;

procedure TFormLoads.dgDataDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
var i: integer; pnum, pnum1: string;
begin

    pnum := meData.FieldByName('pnum').AsString;
    dgData.Canvas.Font.Color := clBlack;

    i:= meData.RecNo-1;
    i := i-1;

    if i >=0 then
    begin

      pnum1 := meData.RecordsView.Rec[i].DataValues['pnum', TDataValueVersionEh.dvvCurValueEh];
      if pnum1 = pnum then
      begin
          if Column.FieldName = 'pnum' then dgData.Canvas.Font.Color := clSilver;
      end;

    end;

    if Column.FieldName = 'need_inspection' then
    begin
      if meData.FieldByName('need_inspection').AsBoolean then
        dgData.Canvas.Brush.Color := clYellow;

      if meData.FieldByName('isdefective').AsInteger>0 then
        dgData.Canvas.Brush.Color := clRed;

    end;

    inherited;

    i:= meData.RecNo-1;
    i := i+1;

    if (i < meData.RecordsView.Count) and (i >= 0 ) then
    begin

      pnum1 := meData.RecordsView.Rec[i].DataValues['pnum', TDataValueVersionEh.dvvCurValueEh];
      if pnum1 <> pnum then
      begin
          dgData.Canvas.Pen.Width := 1;
          dgData.Canvas.Pen.Color := clGray;
          dgData.Canvas.MoveTo(Rect.Left, Rect.Bottom);
          dgData.Canvas.LineTo(Rect.Right, Rect.Bottom);
      end;

    end else
    begin
          dgData.Canvas.Pen.Width := 1;
          dgData.Canvas.Pen.Color := clGray;
          dgData.Canvas.MoveTo(Rect.Left, Rect.Bottom);
          dgData.Canvas.LineTo(Rect.Right, Rect.Bottom);
    end;

    if Column.FieldName = 'pnum' then
    begin

      if meData.FieldByName('urgent_repair_sign').AsInteger>0 then
      begin

          dgData.Canvas.Pen.Color := clRed;
          dgData.Canvas.Pen.Width := 1;
          dgData.Canvas.Brush.Color := clRed;
          dgData.Canvas.Rectangle(Rect.Right-8,Rect.Top+2,Rect.Right-2,Rect.Top+15);
          dgData.Canvas.Brush.Color := clWindow;

      end;

    end;

    dgData.Canvas.Pen.Width := 1;
    dgData.Canvas.Pen.Color := clSilver;

end;

procedure TFormLoads.dgDataKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if dgData.Columns[dgData.Col-1].FieldName = 'dispatch_numbers' then
    allow_edit := true
end;

procedure TFormLoads.AssignSelField(colname: string);
begin

  selfield := '';
  if colname = 'fulfilled' then selfield := 'fulfilled';
  if colname = 'dispatch_numbers' then selfield := 'dispatch_numbers';
  if colname = 'receipt_summa' then selfield := 'receipt_summa';
  if colname = 'train_num' then selfield := 'train_num';
  if colname = 'mtu_code' then selfield := 'mtukind_id';
  if colname = 'picture_code' then selfield := 'picturekind_id';

end;

procedure TFormLoads.FormCreate(Sender: TObject);
begin
 inherited;
 UnicVals := TStringList.Create;
 UnicVals.Sorted := True;
 UnicVals.Duplicates := TDuplicates.dupIgnore;
end;

procedure TFormLoads.Init;
begin
  self.system_section := 'outcome_loads';
  inherited;
  self.formEdit := FormEditLoad;
  self.meDataAfterScroll(meData);
  planid := 0;
  plantext := '';
  printid := 'joint_id';
  sbAll.Down := false;
  showall := false;
  Screen.Cursor := crDefault;
end;

procedure TFormLoads.meDataAfterOpen(DataSet: TDataSet);
var i: integer; pnum: string;
begin

 inherited;
 self.meDataAfterScroll(meData);

 UnicVals.Clear;

 for i := 0 to meData.RecordsView.Count-1 do
 begin
   pnum := meData.RecordsView.Rec[i].DataValues['pnum', TDataValueVersionEh.dvvCurValueEh];
   UnicVals.Add(pnum);
 end;

  dgData.FieldColumns['pnum'].Title.Caption := 'Вaгон/'+ UnicVals.Count.ToString();

  Screen.Cursor := crDefault;

end;

procedure TFormLoads.meDataBeforeEdit(DataSet: TDataSet);
begin
  dgDataColEnter(nil);
  inherited;
end;

procedure TFormLoads.meDataBeforeOpen(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  inherited;
  drvData.SelectCommand.Parameters.ParamByName('showall').Value := self.showall;
end;

procedure TFormLoads.meDataBeforePost(DataSet: TDataSet);
begin
  drvData.UpdateCommand.Parameters.ParamByName('allowedit').Value := allow_edit;
  inherited;
end;

procedure TFormLoads.N17Click(Sender: TObject);
begin
   with FormChangeCarriages do
   begin
    ssCarFromLoad.Parameters.SetPairValue('load_plan_id', meData.FieldByName('load_plan_id').Value);
    cbNew.Checked := false;
    laCarriage1.KeyValue := meData.FieldByName('carriage_id').AsInteger;
    laCarriage2.KeyValue := null;
    laCarriage2.Enabled := true;
    laCarriageNew.KeyValue := null;
    laCarriageNew.Enabled := false;
    ShowModal;
     if FormChangeCarriages.ModalResult = mrOk then
     begin

        dm.spChangeCarriage.Parameters.ParamByName('@planid').Value := meData.FieldByName('load_plan_id').AsString;

        dm.spChangeCarriage.Parameters.ParamByName('@car1').Value := laCarriage1.KeyValue;

        if cbNew.Checked then
        dm.spChangeCarriage.Parameters.ParamByName('@car2').Value := laCarriageNew.KeyValue
        else
        dm.spChangeCarriage.Parameters.ParamByName('@car2').Value := laCarriage2.KeyValue;

        dm.spChangeCarriage.Parameters.ParamByName('@setnew').Value := cbNew.Checked;

        dm.spChangeCarriage.ExecProc;

        FullRefresh(meData);
     end;
   end;
end;

procedure TFormLoads.N20Click(Sender: TObject);
begin
  dgData.DataGrouping.GroupLevels[0].CollapseNodes;
end;

procedure TFormLoads.N21Click(Sender: TObject);
var g: string; gu: TGuid; id: integer;
begin

  id := meData.FieldByName('id').AsInteger;
  FormSelectPath.qrAux.Open;
  FormSelectPath.ShowModal;
  if FormSelectPath.ModalResult = mrOk then
  begin

    CreateGuid(gu);
    g := GuidToString(gu);
    FillSelectListAll(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('update docloadjoint set path_id = :path_id where id in');
    qrAux.SQL.Add('(select l.joint_id from selectlist sl, docload l where sl.guid = '''+g+''' and l.id = sl.id)');
    qrAux.Parameters.ParamByName('path_id').Value := FormSelectPath.qrAux.FieldByName('id').AsInteger;
    qrAux.ExecSQL;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from selectlist where guid = '''+g+'''');
    qrAux.ExecSQL;

    meData.Close;
    meData.Open;
    meData.Locate('id', id, []);

  end;
end;

procedure TFormLoads.N22Click(Sender: TObject);
var g: string; gu: TGUID; id: integer;
begin

  id := meData.FieldByName('id').AsInteger;
  FormEditMtuAndPicture.luMtuKind.Text := '';
  FormEditMtuAndPicture.luPictureKind.Text := '';
  FormEditMtuAndPicture.ShowModal;

  if FormEditMtuAndPicture.ModalResult = mrOk then
  begin

    CreateGUID(gu);
    g:= GuidToString(gu);

    FillSelectListAll(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('SET CONTEXT_INFO 0x001;');
    qrAux.SQL.Add('update docload set mtukind_id = :mtukind_id, picturekind_id = :picturekind_id');
    qrAux.SQL.Add('where id in (select id from selectlist where guid = '''+g+''');');
    qrAux.Parameters.ParamByName('mtukind_id').Value := FormEditMtuAndPicture.luMtuKind.KeyValue;
    qrAux.Parameters.ParamByName('picturekind_id').Value := FormEditMtuAndPicture.luPictureKind.KeyValue;
    qrAux.ExecSQL;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from selectlist where guid = '''+g+''';');
    qrAux.SQL.Add('SET CONTEXT_INFO 0x0;');
    qrAux.ExecSQL;

    FullRefresh(meData);
    meData.Locate('id', id, []);

  end;

end;

procedure TFormLoads.N24Click(Sender: TObject);
begin
  ClearCells;
end;

procedure TFormLoads.N26Click(Sender: TObject);
var pv: TPairValues; pp: TPairparam; fGrid: TForm;
var gg: TGuid;
begin

  fGrid := nil;

  pv := SFDV(FormContainersInOrders, 0, 'id', pp, fGrid);

  if pv.keyvalue>0 then
  begin

    if not fQYN('Вы действительно хотите заменить исходную заявку (заявки)? Все параметры отправки будут также изменены!') then
    begin
      exit;
    end;

    CreateGUID(gg);
    g := GuidToString(gg);
    FillSelectListEx(g, dgData, 'id');

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('exec dbo.ChangeOrder :guid, :neworder');
    qrAux.Parameters.ParamByName('guid').Value := g;
    qrAux.Parameters.ParamByName('neworder').Value := pv.keyvalue;
    qrAux.ExecSQL;

    ShowMessage('Готово.');

    FullRefresh(meData);

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

  end;

end;

procedure TFormLoads.sbAllClick(Sender: TObject);
begin

  self.showall := sbAll.Down;

  meData.Close;
  meData.Open;

end;

procedure TFormLoads.sbAppendDocClick(Sender: TObject);
var gg: TGuid;
begin
  FormDispatchDocsList.Init;
  FormDispatchDocsList.ShowModal;
  if FormDispatchDocsList.ModalResult = mrOk  then
  begin

    CreateGUID(gg);
    g := GuidToString(gg);

    FillSelectList(g);

    FormTransformDocuments.dsData.DataSet := nil;
    dm.spPrepareTransformDocuments.Close;
    dm.spPrepareTransformDocuments.Parameters.ParamByName('@selectguid').Value := g;
    dm.spPrepareTransformDocuments.Parameters.ParamByName('@end_doctype_id').Value := dm.spGetPurposeDocumentTypes.FieldByName('end_doctype_id').AsInteger;
    dm.spPrepareTransformDocuments.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
    dm.spPrepareTransformDocuments.ExecProc;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from temp_filter where doctypes = ''-1'' and userid = '+IntToStr(FormMain.currentUserId));
    qrAux.ExecSQL;

    FormTransformDocuments.g := g;
    FormTransformDocuments.qrData.Close;
    FormTransformDocuments.qrData.Parameters.ParamByName('selectguid').Value := g;
    FormTransformDocuments.qrData.Parameters.ParamByName('userid').Value := FormMain.currentUserId;
    FormTransformDocuments.qrData.Parameters.ParamByName('userid1').Value := FormMain.currentUserId;
    FormTransformDocuments.qrData.Open;

    if FormTransformDocuments.qrData.RecordCount = 0 then
    begin
      ShowMessage('Нет объектов, доступных для включения в новый документ.');
      exit;
    end;

    if FormTransformDocuments.qrData.RecordCount > 1 then
    begin
      FormTransformDocuments.dsData.DataSet := FormTransformDocuments.qrData;
      FormTransformDocuments.ShowModal;
    end else
    begin
      FormTransformDocuments.ModalResult := mrOk;
    end;

    if FormTransformDocuments.ModalResult = mrOk then
    begin

        dm.spTransformDocumentAdd.Parameters.ParamByName('@docid').Value := FormDispatchDocsList.meData.FieldByName('id').AsInteger;
        dm.spTransformDocumentAdd.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
        dm.spTransformDocumentAdd.ExecProc;

        FullRefresh(meData);

    end else
    begin
      ShowMessage('Отменено!');
    end;

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    self.dgData.Selection.Clear;

    meDataAfterScroll(meData);
    self.meLinkedObjects.Close;
    self.meLinkedObjects.Open;

  end;
end;

procedure TFormLoads.sbCancelConfirmClick(Sender: TObject);
begin

  self.RefreshRecord(meData);

  if meData.FieldByName('fulfilled').AsString <> ''  then
  begin
    ShowMessage('Контейнер прошел оформление, изменения запрещены. Для снятия проведения должна быть удалена отметка об оформлении!');
    exit;
  end;


  if meData.FieldByName('docexists').AsInteger <> 0  then
  begin
    ShowMessage('На основании погрузки уже создан документ уборки (уведомление для станции).'+chr(10)+' Для снятия проведения контейнер должен быть удален из документа уборки.');
    exit;
  end;

  allowcancel := true;
  inherited;
end;

procedure TFormLoads.sbConfirmClick(Sender: TObject);
begin

  self.RefreshRecord(meData);

  if meData.FieldByName('need_inspection').AsBoolean then
  begin
    ShowMessage('Вагон на осмотре. Погрузка запрещена.');
    exit;
  end;

  if meData.FieldByName('isdefective').AsInteger>0 then
  begin
    ShowMessage('Вагон неисправен. Погрузка запрещена.');
    exit;
  end;

  inherited;

end;

procedure TFormLoads.sbDeleteClick(Sender: TObject);
var guid: TGuid;
begin

  self.RefreshRecord(meData);

  if meData.FieldByName('fulfilled').AsString <> ''  then
  begin
    ShowMessage('Контейнер прошел оформление, изменения запрещены. Для очистки погрузки должна быть удалена отметка об оформлении!');
    exit;
  end;


  //inherited;
  if fQYN('Вы действительно хотите очистить погрузки (не удалить) по выбранным записям?') then
  begin

    CreateGuid(guid);
    g := GuidToString(guid);
    FillSelectListAll(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select 1 from docload l, documents d, selectlist sl');
    qrAux.SQL.Add('where d.id = l.id and sl.id = l.id and sl.guid = '''+g+'''');
    qrAux.SQL.Add('and (');
    qrAux.SQL.Add('isnull(fulfilled,'''') <> ''''');
    qrAux.SQL.Add('or isnull(d.isconfirmed,0) = 1');
    qrAux.SQL.Add(')');
    qrAux.Open;

    if qrAux.RecordCount>0 then
    begin
      ShowMessage('При выполнении операции проведенные или оформленные погрузки учитываться не будут.');
    end;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('update l set container_id = null from docload l, documents d, selectlist sl');
    qrAux.SQL.Add('where d.id = l.id and sl.id = l.id and sl.guid = '''+g+'''');
    qrAux.SQL.Add('and isnull(fulfilled,'''') = ''''');
    qrAux.SQL.Add('and isnull(d.isconfirmed,0) = 0');
    qrAux.ExecSQL;

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    FullRefresh(meData);

  end;

end;

procedure TFormLoads.sbEditClick(Sender: TObject);
begin

  self.RefreshRecord(meData);

  if meData.FieldByName('fulfilled').AsString <> ''  then
  begin
    ShowMessage('Контейнер прошел оформление, изменения запрещены.');
    exit;
  end;

  if meData.FieldByName('need_inspection').AsBoolean then
  begin
    ShowMessage('Вагон на осмотре. Погрузка запрещена.');
    exit;
  end;

  if meData.FieldByName('isdefective').AsInteger>0 then
  begin
    ShowMessage('Вагон неисправен. Погрузка запрещена.');
    exit;
  end;

  allow_edit := false;
  inherited;

end;

procedure TFormLoads.sbFilterPlanClick(Sender: TObject);
var lp: TPairValues; pp: TPairParam; fGrid: TForm;
begin

  fGrid := nil;

  pp.paramname := '';
  lp := SFDV(FormLoadPlan, meData.FieldByName('load_plan_id').AsInteger, 'load_plan_text', pp, fGrid);

  Screen.Cursor := crHourGlass;

  if lp.keyvalue>0 then
  begin
    self.planid := lp.keyvalue;
    self.plantext := lp.displayvalue;
    meData.Close;
    meData.Open;
    sbFilterPlan.Down := true;
    SetFilter(' план погрузки - '+self.plantext);
  end;

  if lp.keyvalue<0 then
  begin
    self.planid := 0;
    meData.Close;
    meData.Open;
    FormFilter.meNumbers.Clear;
    FormFilter.ComposeConditions;
    SetFilter(FormFilter.conditions);
    sbFilterPlan.Down := false;
  end;

  Screen.Cursor := crDefault;

end;

procedure TFormLoads.sbInspectionClick(Sender: TObject);
var guid: TGuid; id: integer;
begin

  FormSetCheck.cbCheck.Caption := 'Требуется осмотр';
  FormSetCheck.ShowModal;
  if FormSetCheck.ModalResult = mrOk then
  begin

    CreateGUID(guid);
    g := GuidToString(guid);
    FillSelectListAll(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('SET CONTEXT_INFO 0x001;');
    qrAux.SQL.Add('update carriages set need_inspection = :need_inspection ');
    qrAux.SQL.Add('where id in (');
    qrAux.SQL.Add('select j.carriage_id from docloadjoint j, docload l, selectlist sl where sl.id = l.id and l.joint_id = j.id and sl.guid = :guid');
    qrAux.SQL.Add(');');
    qrAux.Parameters.ParamByName('need_inspection').Value := FormSetCheck.cbCheck.Checked;
    qrAux.Parameters.ParamByName('guid').Value := g;
    qrAux.ExecSQL;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from selectlist where guid = '''+g+''';');
    qrAux.SQL.Add('SET CONTEXT_INFO 0x0;');
    qrAux.ExecSQL;

    FullRefresh(meData);

  end;

end;

procedure TFormLoads.sbWagonClick(Sender: TObject);
begin

  drvCarriages.SelectCommand.Parameters.ParamByName('carriage_id').Value := meData.FieldByName('carriage_id').AsInteger;
  meCarriages.Close;
  meCarriages.Open;
  EE(nil, FormEditCarriage, meCarriages, 'carriages');
  self.RefreshRecord(meData,meData.FieldByName('id').AsInteger);

end;

end.
