﻿inherited FormCustomerOperations: TFormCustomerOperations
  ActiveControl = dgData
  Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1087#1086' '#1082#1083#1080#1077#1085#1090#1072#1084
  ClientHeight = 594
  ClientWidth = 864
  ExplicitWidth = 880
  ExplicitHeight = 632
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 553
    Width = 864
    ExplicitTop = 553
    ExplicitWidth = 864
    inherited btnOk: TButton
      Left = 629
      ExplicitLeft = 629
    end
    inherited btnCancel: TButton
      Left = 748
      ExplicitLeft = 748
    end
  end
  inherited plAll: TPanel
    Width = 864
    Height = 553
    ExplicitWidth = 864
    ExplicitHeight = 553
    inherited plTop: TPanel
      Width = 864
      ExplicitWidth = 864
      inherited btTool: TPngSpeedButton
        Left = 812
        ExplicitLeft = 274
      end
    end
    inherited dgData: TDBGridEh
      Width = 864
      Height = 524
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1050#1083#1080#1077#1085#1090
          Width = 157
        end
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1054#1073#1100#1077#1082#1090
          Width = 130
        end
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1086#1087#1077#1088#1072#1094#1080#1080
          Width = 154
        end
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1044#1086#1082#1091#1084#1077#1085#1090
          Width = 140
        end
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1103
          Width = 141
        end
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1059#1089#1083#1091#1075#1072
          Width = 135
        end
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1062#1077#1085#1072
          Width = 85
        end
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
          Width = 93
        end
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1057#1091#1084#1084#1072
          Width = 100
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from objectstatehistory')
  end
  inherited al: TActionList
    Left = 272
    Top = 184
  end
  inherited exReport: TEXLReport
    Left = 422
    Top = 184
  end
end
