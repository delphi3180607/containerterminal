﻿unit EditDeliveryComplete;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBSQLLookUp, DBCtrlsEh,
  Vcl.StdCtrls, Vcl.Mask, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEditDeliveryComplete = class(TFormEdit)
    cbObjectType: TDBSQLLookUp;
    Label2: TLabel;
    Label6: TLabel;
    leStartState: TDBSQLLookUp;
    Label3: TLabel;
    cbRelationStart: TDBComboBoxEh;
    ssObjectTypes: TADOLookUpSqlSet;
    ssStateKind: TADOLookUpSqlSet;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDeliveryComplete: TFormEditDeliveryComplete;

implementation

{$R *.dfm}

end.
