﻿unit editunloaddoc;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBGridEh, Vcl.Buttons, DBLookupEh, DBCtrlsEh,
  Vcl.Mask, Functions;

type
  TFormEditUnloadDoc = class(TFormEdit)
    edDocNumber: TDBEditEh;
    Label1: TLabel;
    dtDocDate: TDBDateTimeEditEh;
    Label3: TLabel;
    dtAlertDate: TDBDateTimeEditEh;
    Label4: TLabel;
    luPerson: TDBLookupComboboxEh;
    Label2: TLabel;
    sbForwarder: TSpeedButton;
    procedure sbForwarderClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditUnloadDoc: TFormEditUnloadDoc;

implementation

{$R *.dfm}

uses persons;

procedure TFormEditUnloadDoc.sbForwarderClick(Sender: TObject);
begin
  SFD(FormPersons, self.luPerson);
end;

end.
