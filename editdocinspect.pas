﻿unit editdocinspect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask, DBGridEh, DBLookupEh,
  Vcl.Buttons, functions;

type
  TFormEditInspect = class(TFormEdit)
    Label11: TLabel;
    Label12: TLabel;
    cbDocNumber: TDBEditEh;
    cbDocDate: TDBDateTimeEditEh;
    Label1: TLabel;
    cdDateEnd: TDBDateTimeEditEh;
    sbPersons: TSpeedButton;
    Label9: TLabel;
    luResponsible: TDBLookupComboboxEh;
    procedure sbPersonsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditInspect: TFormEditInspect;

implementation

{$R *.dfm}

uses persons;

procedure TFormEditInspect.sbPersonsClick(Sender: TObject);
begin
  SFD(FormPersons,self.luResponsible);
end;

end.
