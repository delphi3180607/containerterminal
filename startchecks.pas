﻿unit StartChecks;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.Mask, DBCtrlsEh;

type
  TFormStartChecks = class(TFormGrid)
    Splitter1: TSplitter;
    Panel2: TPanel;
    dgUsers: TDBGridEh;
    Panel1: TPanel;
    PngSpeedButton1: TPngSpeedButton;
    PngSpeedButton3: TPngSpeedButton;
    PngSpeedButton4: TPngSpeedButton;
    PngSpeedButton5: TPngSpeedButton;
    Bevel1: TBevel;
    PngSpeedButton6: TPngSpeedButton;
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormStartChecks: TFormStartChecks;

implementation

{$R *.dfm}

uses EditStartCheck;

procedure TFormStartChecks.Init;
begin
  inherited;
  self.formEdit := FormEditStartCheck;
end;

end.
