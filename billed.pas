﻿unit billed;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.Mask, DBCtrlsEh, Functions;

type
  TFormBilled = class(TFormGrid)
    plStatus: TPanel;
    Panel6: TPanel;
    dtStart: TDBDateTimeEditEh;
    sbClearFilter: TPngSpeedButton;
    btRequery: TButton;
    procedure sbAddClick(Sender: TObject);
    procedure btFilterClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure sbRequeryClick(Sender: TObject);
    procedure sbClearFilterClick(Sender: TObject);
    procedure meDataAfterInsert(DataSet: TDataSet);
    procedure meDataAfterEdit(DataSet: TDataSet);
    procedure meDataAfterPost(DataSet: TDataSet);
    procedure sbDeleteClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure SetFilter(conditions: string);
    procedure Init; override;
  end;

var
  FormBilled: TFormBilled;

implementation

{$R *.dfm}

uses editbilled, filterlite, dmu, main;


procedure TFormBilled.Init;
begin
  if dtStart.Value = null then dtStart.Value := IncMonth(now(),-1);
  self.tablename := 'passes';
  inherited;
end;


procedure TFormBilled.meDataAfterEdit(DataSet: TDataSet);
begin
  inherited;
  meData.FieldByName('user_id').Value := FormMain.currentUserId;
end;

procedure TFormBilled.meDataAfterInsert(DataSet: TDataSet);
begin
  inherited;
  meData.FieldByName('user_id').Value := FormMain.currentUserId;
end;

procedure TFormBilled.meDataAfterPost(DataSet: TDataSet);
begin
  // -- ОТМЕНЯЮ СТАНДАРТНОЕ ПОВЕДЕНИЕ ПОСЛЕ ДОБАВЛЕНИЯ
  if current_mode = 'insert' then current_mode := 'none';
  inherited;
end;

procedure TFormBilled.meDataBeforeOpen(DataSet: TDataSet);
begin
  drvData.SelectCommand.Parameters.ParamByName('datestart').Value := dtStart.Value;
  drvData.SelectCommand.Parameters.ParamByName('guid').Value := g;
  inherited;
end;

procedure TFormBilled.btFilterClick(Sender: TObject);
begin
  FormFilterLite.ShowModal;
  if FormFilterLite.ModalResult in [mrOk, mrYes] then
  begin
    CreateGuid(gu);
    g := GuidToString(gu);
    SetFilter(FormFilterLite.conditions);
  end;
end;

procedure TFormBilled.sbAddClick(Sender: TObject);
var meBuffer: TMemTableEh; bill_id: integer;
begin

  exit;

  FormEditBilled.pcLayout.ActivePageIndex := 0;
  FormEditBilled.meData.Close;
  FormEditBilled.qrAux.Close;
  FormEditBilled.ShowModal;

  if FormEditBilled.ModalResult = mrOk then
  begin

    //ShowMessage('Записи с нулевыми суммами и записи без отметок не учитываются.');

    Screen.Cursor := crHourGlass;

    meBuffer :=  FormEditBilled.meData;
    meBuffer.First;

    while not meBuffer.Eof do
    begin

      if (meBuffer.FieldByName('summa').Value = null)
      or (meBuffer.FieldByName('summa').Value = 0)
      then begin
        meBuffer.Next;
        continue;
      end;

      meData.Append;
      meData.FieldByName('container_id').Value := meBuffer.FieldByName('container_id').Value;
      meData.FieldByName('task_id').Value := meBuffer.FieldByName('task_id').Value;
      meData.FieldByName('invoice_amount').Value := meBuffer.FieldByName('summa').Value;
      meData.FieldByName('invoice_date').Value := FormEditBilled.edDocDate.Value;
      meData.FieldByName('invoice_number').Value := FormEditBilled.edDocNumber.Text;
      meData.FieldByName('user_id').Value := FormMain.currentUserId;
      meData.Post;
      meBuffer.Next;

    end;

    qrAux.Close;
    qrAux.SQL.Clear;

    if tablename<>'' then
     qrAux.SQL.Add('select IDENT_CURRENT(''billed'')')
    else
     qrAux.SQL.Add('select @@IDENTITY;');

    qrAux.Open;
    bill_id := qrAux.Fields[0].AsInteger;

    meData.Close;
    meData.Open;
    meData.Locate('bill_id', bill_id, []);

    Screen.Cursor := crDefault;

  end;

end;


procedure TFormBilled.sbClearFilterClick(Sender: TObject);
begin
  SetFilter('');
end;

procedure TFormBilled.sbDeleteClick(Sender: TObject);
begin
  if fQYN('Очистить данные о счете?') then
  begin
    qrAux.Parameters.ParamByName('bill_id').Value := meData.FieldByName('bill_id').AsInteger;
    qrAux.ExecSQL;
    RefreshRecord(meData);
  end;
end;

procedure TFormBilled.sbRequeryClick(Sender: TObject);
begin
  meData.Close;
  meData.Open;
end;

procedure TFormBilled.SetFilter(conditions: string);
begin

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := conditions;
    dm.spCreateFilter.ExecProc;

    meData.Close;
    meData.Open;

    if conditions = '' then
    begin
      plStatus.Caption := ' Фильтр снят.';
      g := '';
    end else
    begin
      plStatus.Caption := ' Установлен фильтр по списку номеров.';
    end;

end;


end.
