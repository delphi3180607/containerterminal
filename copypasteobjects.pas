﻿unit copypasteobjects;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Data.DB,
  Data.Win.ADODB, Vcl.ExtCtrls, Vcl.ComCtrls, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, MemTableEh, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.Buttons, PngSpeedButton;

type
  TFormCopyPasteObjects = class(TFormEdit)
    pcPages: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    meNumbers: TMemo;
    dgSelected: TDBGridEh;
    meSelected: TMemTableEh;
    dsSelected: TDataSource;
    Panel1: TPanel;
    sbCheck: TPngSpeedButton;
    procedure meNumbersChange(Sender: TObject);
  private
    { Private declarations }
  public
    objecttype: string;
    procedure Prepare(objecttype: string);
  end;

var
  FormCopyPasteObjects: TFormCopyPasteObjects;

implementation

{$R *.dfm}

procedure TFormCopyPasteObjects.Prepare(objecttype: string);
begin
  meSelected.Close;
  meSelected.Open;
  meSelected.EmptyTable;
  pcPages.ActivePageIndex := 0;
  self.objecttype := objecttype;
end;


procedure TFormCopyPasteObjects.meNumbersChange(Sender: TObject);
var i: integer;
begin
  meSelected.Close;
  meSelected.Open;
  meSelected.EmptyTable;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select o.*,');
  qrAux.SQL.Add('(select code from counteragents c where c.id = owner_id) as owner_code,');
  qrAux.SQL.Add('(select code from objectstatekinds k where k.id = state_id) as state_code');
  qrAux.SQL.Add('from v_objects o');
  qrAux.SQL.Add('where cnum = :cnum and object_type = :objecttype');
  qrAux.SQL.Add('order by cnum');

  for i := 0 to meNumbers.Lines.Count-1 do
  begin
    qrAux.Close;
    qrAux.Parameters.ParamByName('cnum').Value := meNumbers.Lines[i];
    qrAux.Parameters.ParamByName('objecttype').Value := objecttype;
    qrAux.Open;
    if qrAux.RecordCount>0 then
    begin
      meSelected.Append;
      meSelected.FieldByName('id').Value := qrAux.FieldByName('id').AsInteger;
      meSelected.FieldByName('cnum').Value := qrAux.FieldByName('cnum').AsString;
      meSelected.FieldByName('state_code').Value := qrAux.FieldByName('state_code').AsString;
      meSelected.Post;
    end else
    begin
      meSelected.Append;
      meSelected.FieldByName('cnum').Value := meNumbers.Lines[i];
      meSelected.FieldByName('note').Value := 'НЕ НАЙДЕН';
      meSelected.Post;
    end;

  end;


  pcPages.ActivePageIndex := 1;

end;

end.
