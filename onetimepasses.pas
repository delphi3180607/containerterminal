﻿unit OneTimePasses;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Mask, DBCtrlsEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton;

type
  TFormOneTimePasses = class(TFormGrid)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormOneTimePasses: TFormOneTimePasses;

implementation

{$R *.dfm}

end.
