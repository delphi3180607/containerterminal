﻿inherited FormEditLinkedInvoice: TFormEditLinkedInvoice
  Caption = #1042#1099#1089#1090#1072#1074#1083#1077#1085#1099#1081' '#1089#1095#1077#1090' '#1085#1072' '#1082#1086#1085#1090#1077#1081#1085#1077#1088
  ClientHeight = 338
  ClientWidth = 500
  ExplicitWidth = 506
  ExplicitHeight = 366
  PixelsPerInch = 96
  TextHeight = 16
  object sbService: TSpeedButton [0]
    Left = 456
    Top = 80
    Width = 35
    Height = 24
    Caption = '...'
    OnClick = sbServiceClick
  end
  object Label17: TLabel [1]
    Left = 8
    Top = 178
    Width = 80
    Height = 16
    Caption = #1058#1080#1087' '#1090#1072#1088#1080#1092#1072
  end
  inherited plBottom: TPanel
    Top = 297
    Width = 500
    TabOrder = 6
    inherited btnCancel: TButton
      Left = 384
    end
    inherited btnOk: TButton
      Left = 265
    end
  end
  object edDocNumber: TDBEditEh [3]
    Left = 8
    Top = 28
    Width = 147
    Height = 22
    ControlLabel.Width = 119
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    ControlLabel.Visible = True
    DataField = 'doc_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object dtDocDate: TDBDateTimeEditEh [4]
    Left = 165
    Top = 28
    Width = 121
    Height = 22
    ControlLabel.Width = 110
    ControlLabel.Height = 16
    ControlLabel.Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    ControlLabel.Visible = True
    DataField = 'doc_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object laDealer: TDBSQLLookUp [5]
    Left = 8
    Top = 138
    Width = 441
    Height = 22
    ControlLabel.Width = 47
    ControlLabel.Height = 16
    ControlLabel.Caption = #1050#1083#1080#1077#1085#1090
    ControlLabel.Transparent = True
    ControlLabel.Visible = True
    DataField = 'customer_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 3
    Visible = True
    SqlSet = dm.ssCounteragents
  end
  object neSumma: TDBNumberEditEh [6]
    Left = 8
    Top = 259
    Width = 121
    Height = 24
    ControlLabel.Width = 43
    ControlLabel.Height = 16
    ControlLabel.Caption = #1057#1091#1084#1084#1072
    ControlLabel.Visible = True
    DataField = 'summa'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
  object luService: TDBLookupComboboxEh [7]
    Left = 8
    Top = 80
    Width = 441
    Height = 24
    ControlLabel.Width = 47
    ControlLabel.Height = 16
    ControlLabel.Caption = #1059#1089#1083#1091#1075#1072
    ControlLabel.Visible = True
    DynProps = <>
    DataField = 'service_id'
    DataSource = dsLocal
    EditButtons = <>
    KeyField = 'id'
    ListField = 'scode'
    ListSource = dm.dsServices
    TabOrder = 2
    Visible = True
  end
  object laTarifType: TDBSQLLookUp [8]
    Left = 8
    Top = 196
    Width = 308
    Height = 22
    DataField = 'tarif_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 4
    Visible = True
    SqlSet = ssTarifType
  end
  inherited dsLocal: TDataSource
    Left = 288
    Top = 264
  end
  inherited qrAux: TADOQuery
    Left = 232
    Top = 264
  end
  object ssTarifType: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from tariftypes order by code')
    DownSql.Strings = (
      'select * from tariftypes order by code')
    InitSql.Strings = (
      'select * from tariftypes where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 144
    Top = 177
  end
end
