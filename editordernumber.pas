﻿unit editordernumber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEditOrderNumber = class(TFormEdit)
    edNumber: TDBEditEh;
    cbIsSigned: TDBCheckBoxEh;
    Label1: TLabel;
    cbSealWaste: TDBComboBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditOrderNumber: TFormEditOrderNumber;

implementation

{$R *.dfm}

end.
