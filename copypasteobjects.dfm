﻿inherited FormCopyPasteObjects: TFormCopyPasteObjects
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1086#1073#1100#1077#1082#1090#1086#1074' '#1087#1086' '#1085#1086#1084#1077#1088#1072#1084
  ClientHeight = 404
  ClientWidth = 721
  ExplicitWidth = 727
  ExplicitHeight = 432
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 363
    Width = 721
    ExplicitTop = 363
    ExplicitWidth = 721
    inherited btnCancel: TButton
      Left = 605
      ExplicitLeft = 605
    end
    inherited btnOk: TButton
      Left = 486
      ExplicitLeft = 486
    end
  end
  object pcPages: TPageControl [1]
    Left = 0
    Top = 0
    Width = 721
    Height = 363
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = #1053#1086#1084#1077#1088#1072
      object meNumbers: TMemo
        AlignWithMargins = True
        Left = 3
        Top = 39
        Width = 707
        Height = 290
        Align = alClient
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 713
        Height = 36
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object sbCheck: TPngSpeedButton
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 62
          Height = 30
          Hint = #1055#1088#1086#1074#1077#1088#1080#1090#1100
          Align = alLeft
          OnClick = meNumbersChange
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000003174944415478DA75536B489351187ECE2E15258146145D
            84A8CC202B98FD892E3F8A8A72F6432A57E60C8D9A615ED269439BCB322DEC82
            65A1DDDDBCE09C8A964B6AB5C0505B65865046AD24D6C550BACC62DBB7D339DF
            B22CEAC0FBE7FBDEE739CF79DEE7259DF71DF8DF2184402693C1E9749675773F
            D650BF1F3E41C0FCF0F0738F7B7A34128904648480522A02461FDE2097CBAAAB
            AA6A63B559FBE072BDC5E0D0202C0D8D2209EBD7FC22E060A9542A824629386B
            3255EDD61FC8C5E1C26250C15334F4F96B8E52A9C4F59616DE4F448211B0D95C
            2F4A269C8429120481A6A7A5E2F8C953282C304432CEFE9292928259B3E7EEB2
            5AADBCF73701073634343105045C0365E5F3F968EC96585CA9AC84DB3D0CF757
            3711041F554647A3ED46EB9F0AE472B94820954A2030B3FCCC2CAEC0E3F1A220
            3F1F89097B306E7C10A2A2D6A3E59A05219382CDC1C1219B4402FE6EAEC06269
            1409FC7ECA6F6725D0FCBC5CEC884FC6C71517B13214E8EA0AC5AAE0834DE9DA
            84BD470CA5FDC4F1F09168182328ABAB338BA3A17E02BF97E0802E176AF54E7C
            700D806C6AC2199516A67B4771A71735132642C5AD2275F516848585551B2B8D
            B1D9D95ABC7F3780CAAB35085AEA406B6F33530448D9743D1EA022C180C1E1A7
            687B528D8E17A81923838A3435B798DBDBEFC564EE4B43D9D9727C1BFE8ED52B
            D6E160CF32187795E28BE706D327157D194BD6E0F6F3142C9E9980B69ECBE878
            867AA2CBD3D30C36AAD2D36760D0E7456A92531D9F06BEC1B5A41C27D429E872
            96FECA05B3063C6B5CBA62462274C60B2039BA5CBA55A582C978B578614444CE
            B6B8384556BAC1F160BA1EFAED2BE11CB40792EA07046F80883FABDF05DC6D87
            8D64646A699472036EDDB461FEBC39595DF7BB8F2D5AA040F3EBFD704F7C0D09
            0D80D950A0DE2CE60BAFDE00F63B68BB7D081A62AAAE35DBEDF6181E4FAFD70B
            8FD7076B732B148B97B7A664C4E78DC85F5B04873A0EE8EB63609B08DEBDF104
            9C7C7B422BCE5F3ADED9D9118340FA3075CA345B515141D23AD630068158B2CF
            74E932E0D675D818388983C57DF979018B08268F5AC421562FFFDA6EC5BFFEFD
            006B8763A14657C5EE0000000049454E44AE426082}
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1054#1073#1098#1077#1082#1090#1099
      ImageIndex = 1
      object dgSelected: TDBGridEh
        Left = 0
        Top = 0
        Width = 713
        Height = 332
        Align = alClient
        AutoFitColWidths = True
        DataSource = dsSelected
        DynProps = <>
        Flat = True
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
        TabOrder = 0
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -13
        TitleParams.Font.Name = 'Verdana'
        TitleParams.Font.Style = [fsBold]
        TitleParams.ParentFont = False
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088
            Width = 164
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'state_code'
            Footers = <>
            Title.Caption = #1057#1090#1072#1090#1091#1089
            Width = 152
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'note'
            Footers = <>
            Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
            Width = 228
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object meSelected: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    Options = [mtoPersistentStructEh]
    Left = 136
    Top = 160
    object MemTableData: TMemTableDataEh
      object DataStruct: TMTDataStructEh
        object id: TMTNumericDataFieldEh
          FieldName = 'id'
          NumericDataType = fdtLargeintEh
          AutoIncrement = False
          DisplayWidth = 20
          currency = False
          Precision = 15
        end
        object cnum: TMTStringDataFieldEh
          FieldName = 'cnum'
          StringDataType = fdtStringEh
          DisplayWidth = 20
        end
        object state_code: TMTStringDataFieldEh
          FieldName = 'state_code'
          StringDataType = fdtStringEh
          DisplayWidth = 50
          Size = 50
        end
        object note: TMTStringDataFieldEh
          FieldName = 'note'
          StringDataType = fdtStringEh
          DisplayWidth = 50
          Size = 50
        end
      end
      object RecordsList: TRecordsListEh
      end
    end
  end
  object dsSelected: TDataSource
    DataSet = meSelected
    Left = 210
    Top = 160
  end
end
