﻿unit editcatalog;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Vcl.ExtCtrls, Data.DB, Data.Win.ADODB, DBGridEh, DBLookupEh, Vcl.Buttons,
  DBSQLLookUp;

type
  TFormEditCatalog = class(TFormEdit)
    Label1: TLabel;
    edFolderName: TDBEditEh;
    Label2: TLabel;
    sbParentFolders: TSpeedButton;
    leParentCatalog: TDBSQLLookUp;
    ssParentCatalog: TADOLookUpSqlSet;
    procedure sbParentFoldersClick(Sender: TObject);
  private
    Fsystem_section: string;
    procedure SetSystemSection(value: string);
  public
    property system_section: string read Fsystem_section write SetSystemSection;
  end;

var
  FormEditCatalog: TFormEditCatalog;

implementation

{$R *.dfm}

uses selectfolder;

procedure TFormEditCatalog.SetSystemSection(value: string);
begin
 Fsystem_section := value;
 ssParentCatalog.Parameters.SetPairValue('folder_section',value);
end;


procedure TFormEditCatalog.sbParentFoldersClick(Sender: TObject);
begin

  FormSelectFolder.meFolders.Close;
  FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('folder_section').Value := self.system_section;
  if (self.dsLocal.DataSet.FieldByName('id').AsInteger>0) then
  begin
    FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('startid').Value := self.dsLocal.DataSet.FieldByName('id').AsInteger;
  end else
  begin
    FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('startid').Value := 0;
  end;
  FormSelectFolder.meFolders.Open;

  FormSelectFolder.meFolders.Filter := '';
  FormSelectFolder.meFolders.Filtered := false;

  FormSelectFolder.ShowModal;

  if FormSelectFolder.ModalResult = mrOk then
  begin
    self.leParentCatalog.KeyValue := FormSelectFolder.meFolders.FieldByName('id').AsInteger;
  end;

end;

end.
