﻿unit EditClassification;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, DBCtrlsEh, Vcl.Mask,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEditClassification = class(TFormEdit)
    edTableName: TDBEditEh;
    edClassifName: TDBEditEh;
    cbMulti: TDBCheckBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditClassification: TFormEditClassification;

implementation

{$R *.dfm}

end.
