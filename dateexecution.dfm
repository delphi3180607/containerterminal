﻿inherited FormDateExecution: TFormDateExecution
  BorderStyle = bsToolWindow
  Caption = #1055#1088#1086#1074#1077#1076#1077#1085#1080#1077
  ClientHeight = 170
  ClientWidth = 397
  Font.Height = -11
  ExplicitWidth = 403
  ExplicitHeight = 194
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 39
    Top = 20
    Width = 151
    Height = 13
    Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1103': '
  end
  object Label2: TLabel [1]
    Left = 99
    Top = 57
    Width = 91
    Height = 13
    Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100': '
  end
  object Label17: TLabel [2]
    Left = 5
    Top = 96
    Width = 185
    Height = 13
    Caption = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1072#1103' '#1076#1072#1090#1072' '#1080#1089#1087#1086#1083#1085#1077#1085#1080#1103':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 137
    Width = 397
    Height = 33
    TabOrder = 3
    ExplicitTop = 137
    ExplicitWidth = 397
    ExplicitHeight = 33
    inherited btnCancel: TButton
      Left = 281
      Height = 27
      ExplicitLeft = 281
      ExplicitHeight = 27
    end
    inherited btnOk: TButton
      Left = 162
      Height = 27
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
      Font.Height = -11
      ExplicitLeft = 162
      ExplicitHeight = 27
    end
  end
  object dtFactExecution: TDBDateTimeEditEh [4]
    Left = 198
    Top = 93
    Width = 184
    Height = 24
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
    EditFormat = 'DD/MM/YYYY HH:NN:SS'
  end
  object edUserName: TEdit [5]
    Left = 198
    Top = 54
    Width = 186
    Height = 21
    Color = 15263976
    Enabled = False
    TabOrder = 2
    StyleElements = [seFont, seBorder]
  end
  object dtConfirm: TDBDateTimeEditEh [6]
    Left = 198
    Top = 15
    Width = 182
    Height = 24
    DynProps = <>
    Enabled = False
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
    EditFormat = 'dd.mm.yyyy hh:nn:ss'
  end
  inherited dsLocal: TDataSource
    Left = 96
    Top = 112
  end
  inherited qrAux: TADOQuery
    Left = 144
    Top = 112
  end
end
