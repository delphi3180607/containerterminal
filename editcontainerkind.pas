﻿unit editcontainerkind;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Vcl.ExtCtrls, Data.DB, Data.Win.ADODB;

type
  TFormEditContainerKind = class(TFormEdit)
    Label2: TLabel;
    Label3: TLabel;
    edCode: TDBEditEh;
    edName: TDBEditEh;
    Label1: TLabel;
    neFootSize: TDBNumberEditEh;
    cbDefault: TDBCheckBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditContainerKind: TFormEditContainerKind;

implementation

{$R *.dfm}

end.
