﻿unit sqlquery;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls,
  DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, SynEdit, SynMemo;

type
  TFormSQLQuery = class(TFormEdit)
    se: TSynMemo;
    dg: TDBGridEh;
    plTool: TPanel;
    btExec: TButton;
    btSelect: TButton;
    Splitter1: TSplitter;
    procedure btSelectClick(Sender: TObject);
    procedure btExecClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSQLQuery: TFormSQLQuery;

implementation

{$R *.dfm}

procedure TFormSQLQuery.btExecClick(Sender: TObject);
begin
  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Text := se.Lines.Text;
  qrAux.ExecSQL;
end;

procedure TFormSQLQuery.btSelectClick(Sender: TObject);
begin
  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Text := se.Lines.Text;
  qrAux.Open;
end;

end.
