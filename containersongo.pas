﻿unit ContainersOnGo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Vcl.Mask, DBCtrlsEh;

type
  TFormContainersOnGo = class(TFormGrid)
    sbClearFilter: TPngSpeedButton;
    plStatus: TPanel;
    Panel6: TPanel;
    dtStart: TDBDateTimeEditEh;
    edContainerNum: TDBEditEh;
    btRequery: TButton;
    procedure btFilterClick(Sender: TObject);
    procedure sbClearFilterClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure dtStartChange(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure edContainerNumKeyPress(Sender: TObject; var Key: Char);
    procedure btRequeryClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    var g: string;
    gu: TGuid;
    procedure SetFilter(conditions: string);
    procedure Init; override;
  end;

var
  FormContainersOnGo: TFormContainersOnGo;

implementation

{$R *.dfm}

uses filterlite, dmu;

procedure TFormContainersOnGo.btRequeryClick(Sender: TObject);
begin
  meData.Filter := '';
  meData.Filtered := false;
  edContainerNum.Text := '';
  ClearSTFilter(dgData);
end;

procedure TFormContainersOnGo.dgDataDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

  if Column.FieldName = 'external_order_num' then
  if meData.FieldByName('external_order_signed').AsBoolean then
  begin
    dgData.Canvas.Brush.Color := clLime;
  end;

  inherited;

end;

procedure TFormContainersOnGo.dtStartChange(Sender: TObject);
begin
  drvData.SelectCommand.Parameters.ParamByName('start_date').Value := dtStart.Value;
  meData.Close;
  meData.Open;
end;

procedure TFormContainersOnGo.edContainerNumKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin

    if edContainerNum.Text<>'' then
    begin
      meData.Filter := 'cnum like ''%'+edContainerNum.Text+'%''';
      meData.Filtered := true;
      dgData.SetFocus;
      plStatus.Caption := 'Установлен фильтр по номеру.';
    end else
    begin
      meData.Filter := '';
      meData.Filtered := false;
    end;

    Key := #0;

  end;
end;

procedure TFormContainersOnGo.FormShow(Sender: TObject);
begin
  inherited;
  self.RestoreSort(dgData);
end;

procedure TFormContainersOnGo.Init;
begin
  if dtStart.Value = null then dtStart.Value := IncMonth(now(),-12);
  drvData.SelectCommand.Parameters.ParamByName('start_date').Value := dtStart.Value;
  inherited;
  if not meData.Active then meData.Open;
end;


procedure TFormContainersOnGo.btFilterClick(Sender: TObject);
begin
  FormFilterLite.ShowModal;
  if FormFilterLite.ModalResult in [mrOk, mrYes] then
  begin
    CreateGuid(gu);
    g := GuidToString(gu);
    SetFilter(FormFilterLite.conditions);
  end;
end;

procedure TFormContainersOnGo.meDataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  if Assigned(drvData.SelectCommand.Parameters.FindParam('guid')) then
      drvData.SelectCommand.Parameters.ParamByName('guid').Value := g;
end;

procedure TFormContainersOnGo.sbClearFilterClick(Sender: TObject);
begin
  SetFilter('');
end;


procedure TFormContainersOnGo.SetFilter(conditions: string);
begin

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := conditions;
    dm.spCreateFilter.ExecProc;

    meData.Close;
    meData.Open;

    if conditions = '' then
    begin
      plStatus.Caption := ' Фильтр снят.';
      g := '';
    end else
    begin
      plStatus.Caption := ' Установлен фильтр по списку номеров.';
    end;

end;


end.
