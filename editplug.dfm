﻿inherited FormEditPlug: TFormEditPlug
  Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1080
  ClientHeight = 164
  ClientWidth = 292
  ExplicitWidth = 298
  ExplicitHeight = 192
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 123
    Width = 292
    TabOrder = 2
    inherited btnCancel: TButton
      Left = 176
      ExplicitLeft = 764
    end
    inherited btnOk: TButton
      Left = 57
    end
  end
  object dtPlugStart: TDBDateTimeEditEh [1]
    Left = 8
    Top = 29
    Width = 209
    Height = 23
    ControlLabel.Width = 126
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1072#1095#1072#1083#1086' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103
    ControlLabel.Visible = True
    AutoSize = False
    DataField = 'plugstart'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object dtPlugEnd: TDBDateTimeEditEh [2]
    Left = 8
    Top = 80
    Width = 209
    Height = 22
    ControlLabel.Width = 147
    ControlLabel.Height = 16
    ControlLabel.Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103
    ControlLabel.Visible = True
    AutoSize = False
    DataField = 'plugend'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 72
    Top = 128
  end
  inherited qrAux: TADOQuery
    Left = 24
    Top = 128
  end
end
