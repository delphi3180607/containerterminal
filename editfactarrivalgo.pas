﻿unit EditFactArrivalGo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls, DBGridEh, DBLookupEh, Vcl.Buttons,
  DBSQLLookUp, Vcl.ComCtrls;

type
  TFormEditFactArrivalGo = class(TFormEdit)
    cbExtraPlace: TDBCheckBoxEh;
    Label8: TLabel;
    luDealer: TDBLookupComboboxEh;
    laDriver: TDBSQLLookUp;
    Label1: TLabel;
    edCarNumber: TDBEditEh;
    Label27: TLabel;
    Label18: TLabel;
    edCarData: TDBEditEh;
    ssPersons: TADOLookUpSqlSet;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label6: TLabel;
    Label7: TLabel;
    dtArrivalFact: TDBDateTimeEditEh;
    dtGoFact: TDBDateTimeEditEh;
    edReceptAdress: TDBEditEh;
    edDlvAddress: TDBEditEh;
    edExtraAddress: TDBEditEh;
    Label5: TLabel;
    dtExtraArrivalFact: TDBDateTimeEditEh;
    dtExtraGoFact: TDBDateTimeEditEh;
    Label23: TLabel;
    Label22: TLabel;
    Label2: TLabel;
    Label3: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditFactArrivalGo: TFormEditFactArrivalGo;

implementation

{$R *.dfm}

end.
