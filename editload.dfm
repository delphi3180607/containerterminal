﻿inherited FormEditLoad: TFormEditLoad
  Caption = #1055#1086#1075#1088#1091#1079#1082#1072
  ClientHeight = 177
  ClientWidth = 551
  ExplicitWidth = 557
  ExplicitHeight = 205
  PixelsPerInch = 96
  TextHeight = 16
  object luContainerId: TDBSQLLookUp [0]
    Left = 105
    Top = 8
    Width = 327
    Height = 24
    ControlLabel.Width = 71
    ControlLabel.Height = 16
    ControlLabel.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
    ControlLabel.Visible = True
    ControlLabelLocation.Position = lpLeftCenterEh
    DataField = 'container_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 0
    Visible = True
    OnChange = luContainerIdChange
    SqlSet = ssContainers
    RowCount = 0
    OnKeyValueChange = luContainerIdKeyValueChange
  end
  object luKind: TDBSQLLookUp [1]
    Left = 105
    Top = 38
    Width = 327
    Height = 24
    ControlLabel.Width = 25
    ControlLabel.Height = 16
    ControlLabel.Caption = #1058#1080#1087
    ControlLabel.Visible = True
    ControlLabelLocation.Position = lpLeftCenterEh
    DataField = 'kind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    ReadOnly = True
    TabOrder = 2
    Visible = True
    SqlSet = ssContainerKind
    RowCount = 0
  end
  object luOwner: TDBSQLLookUp [2]
    Left = 105
    Top = 68
    Width = 327
    Height = 24
    ControlLabel.Width = 88
    ControlLabel.Height = 16
    ControlLabel.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
    ControlLabel.Visible = True
    ControlLabelLocation.Position = lpLeftCenterEh
    DataField = 'owner_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    ReadOnly = True
    TabOrder = 3
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object plSelectCont: TPanel [3]
    AlignWithMargins = True
    Left = 442
    Top = 6
    Width = 98
    Height = 45
    Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsUnderline]
    ParentFont = False
    TabOrder = 1
    StyleElements = [seClient]
    OnClick = plSelectContClick
  end
  object edNote: TDBEditEh [4]
    Left = 104
    Top = 98
    Width = 327
    Height = 24
    ControlLabel.Width = 82
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
    ControlLabel.Visible = True
    ControlLabelLocation.Position = lpLeftCenterEh
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    ReadOnly = True
    TabOrder = 4
    Visible = True
  end
  inherited plBottom: TPanel
    Top = 136
    Width = 551
    TabOrder = 5
    ExplicitTop = 136
    ExplicitWidth = 551
    inherited btnCancel: TButton
      Left = 435
      ExplicitLeft = 435
    end
    inherited btnOk: TButton
      Left = 316
      OnClick = btnOkClick
      ExplicitLeft = 316
    end
  end
  inherited dsLocal: TDataSource
    Left = 160
    Top = 123
  end
  inherited qrAux: TADOQuery
    Left = 208
    Top = 123
  end
  object ssContainers: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select top 20 c.id, c.cnum, c.kind_id, '
      
        '(select footsize from containerkinds ck where ck.id = c.kind_id)' +
        ' as footsize, '
      'o.owner_id,'
      'isnull(cc.isempty,0) as isempty'
      'from'
      
        '(select top 10 * from containers c where c.cnum like '#39'%@pattern%' +
        #39') c,'
      'objects o'
      
        'left outer join v_lastobjectstates los on (los.linked_object_id ' +
        '= o.id and los.task_id = o.current_task_id)'
      'left outer join cargos cc on (cc.id = los.object_id)'
      'left outer join tasks t on (t.id = o.current_task_id)'
      'left outer join deliverytypes d on (t.dlv_type_id = d.id)'
      
        'left outer join objectstatekinds sk on (sk.id = los.object_state' +
        '_id)'
      'where o.id = c.id'
      'and (sk.inplacesign = 1 or sk.id is null)'
      'and ('
      '(d.global_section like '#39'%output-store%'#39')'
      'or t.id is null'
      ')'
      'order by cnum')
    DownSql.Strings = (
      'select top 20 c.id, c.cnum, c.kind_id, '
      
        '(select footsize from containerkinds ck where ck.id = c.kind_id)' +
        ' as footsize, '
      'o.owner_id,'
      'isnull(cc.isempty,0) as isempty'
      'from'
      
        '(select top 10 * from containers c where c.cnum like '#39'%@pattern%' +
        #39') c,'
      'objects o'
      
        'left outer join v_lastobjectstates los on (los.linked_object_id ' +
        '= o.id and los.task_id = o.current_task_id)'
      'left outer join cargos cc on (cc.id = los.object_id)'
      'left outer join tasks t on (t.id = o.current_task_id)'
      'left outer join deliverytypes d on (t.dlv_type_id = d.id)'
      
        'left outer join objectstatekinds sk on (sk.id = los.object_state' +
        '_id)'
      'where o.id = c.id'
      'and (sk.inplacesign = 1 or sk.id is null)'
      'and ('
      
        '(d.global_section like '#39'%output%'#39' or d.global_section like '#39'%sto' +
        're%'#39')'
      'or t.id is null'
      ')'
      'order by cnum'
      '')
    InitSql.Strings = (
      'select top 20 c.id, c.cnum, c.kind_id, '
      
        '(select footsize from containerkinds ck where ck.id = c.kind_id)' +
        ' as footsize, '
      'o.owner_id,'
      'isnull(cc.isempty,0) as isempty'
      'from'
      '(select * from containers c where id = @id) c,'
      'objects o'
      
        'left outer join v_lastobjectstates los on (los.linked_object_id ' +
        '= o.id and los.task_id = o.current_task_id)'
      'left outer join cargos cc on (cc.id = los.object_id)'
      'left outer join tasks t on (t.id = o.current_task_id)'
      'left outer join deliverytypes d on (t.dlv_type_id = d.id)'
      
        'left outer join objectstatekinds sk on (sk.id = los.object_state' +
        '_id)'
      'where o.id = c.id'
      'and (sk.inplacesign = 1 or sk.id is null)'
      'and ('
      
        '(d.global_section like '#39'%output%'#39' or d.global_section like '#39'%sto' +
        're%'#39')'
      'or t.id is null'
      ')'
      'order by cnum')
    KeyName = 'id'
    DisplayName = 'cnum'
    Connection = dm.connMain
    Left = 317
  end
  object ssContainerKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from containerkinds order by code')
    DownSql.Strings = (
      'select * from containerkinds order by code')
    InitSql.Strings = (
      'select * from containerkinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 165
    Top = 32
  end
end
