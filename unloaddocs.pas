﻿unit unloaddocs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, docflow, System.Actions, Vcl.ActnList, EXLReportExcelTLB,
  EXLReportBand, EXLReport, Functions, Vcl.ComCtrls, Vcl.Mask, DBCtrlsEh,
  System.ImageList, Vcl.ImgList, PngImageList;

type
  TFormUnloadDocs = class(TFormDocFlow)
    dgSpec: TDBGridEh;
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    pmSpec: TPopupMenu;
    MenuItem14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    sbFilterPlan: TPngSpeedButton;
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure MenuItem14Click(Sender: TObject);
    procedure sbChangeClick(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure meDataAfterOpen(DataSet: TDataSet);
    procedure sbConfirmClick(Sender: TObject);
    procedure sbCancelConfirmClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure sbFilterPlanClick(Sender: TObject);
    procedure sbClearFilterClick(Sender: TObject);
  private
    { Private declarations }
  public
    planid: integer;
    procedure Init; override;
  end;

var
  FormUnloadDocs: TFormUnloadDocs;

implementation

{$R *.dfm}

uses editunloaddoc, deliverytypes, selectdeliverytype, dmu, editdatealert,
  unloadplan;


procedure TFormUnloadDocs.Init;
begin
  self.system_section := 'income_unload';
  inherited;
  self.formEdit := FormEditUnloadDoc;
  planid := 0;
end;


procedure TFormUnloadDocs.meDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  drvSpec.SelectCommand.Parameters.ParamByName('doc_id').Value := DataSet.FieldByName('id').AsInteger;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormUnloadDocs.meDataAfterScroll(DataSet: TDataSet);
begin
  inherited;
  drvSpec.SelectCommand.Parameters.ParamByName('doc_id').Value := DataSet.FieldByName('id').AsInteger;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormUnloadDocs.meDataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  drvData.SelectCommand.Parameters.ParamByName('planid').Value := self.planid;
end;

procedure TFormUnloadDocs.MenuItem14Click(Sender: TObject);
begin
  if fQYN('Экспортировать список в Excel ?') then
    ExportExcel(self.dgSpec, self.Caption);
end;

procedure TFormUnloadDocs.N17Click(Sender: TObject);
begin
  allow_edit := true;
  meData.ReadOnly := false;
  EE(nil, FormDateAlert, meData, 'docunload');
  allow_edit := false;
end;

procedure TFormUnloadDocs.sbCancelConfirmClick(Sender: TObject);
begin
  inherited;
  drvSpec.SelectCommand.Parameters.ParamByName('doc_id').Value := meData.FieldByName('id').AsInteger;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormUnloadDocs.sbChangeClick(Sender: TObject);
begin

  FormSelectDeliveryType.laDlvTypes.KeyValue := meSpec.FieldByName('dlv_type_id').AsInteger;
  FormSelectDeliveryType.ShowModal;

  if FormSelectDeliveryType.ModalResult = mrOk then
  begin

    dm.spUpdateDeliveryType.Parameters.ParamByName('@taskid').Value := meSpec.FieldByName('current_task_id').AsInteger;
    dm.spUpdateDeliveryType.Parameters.ParamByName('@dlvtypeid').Value := FormSelectDeliveryType.laDlvTypes.KeyValue;
    dm.spUpdateDeliveryType.ExecProc;

  end;

  FormDeliveryTypes.meData.Filter := '';
  FormDeliveryTypes.meData.Filtered := false;

  meSpec.Close;
  meSpec.Open;

end;

procedure TFormUnloadDocs.sbClearFilterClick(Sender: TObject);
begin
  self.planid := 0;
  sbFilterPlan.Down := false;
  inherited;
end;

procedure TFormUnloadDocs.sbConfirmClick(Sender: TObject);
begin
  inherited;
  drvSpec.SelectCommand.Parameters.ParamByName('doc_id').Value := meData.FieldByName('id').AsInteger;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormUnloadDocs.sbFilterPlanClick(Sender: TObject);
var lp: TPairValues; pp: TPairParam; fGrid: TForm;
begin

  fGrid := nil;

  if self.planid > 0 then
  begin

    self.planid := 0;
    meData.Close;
    meData.Open;
    sbFilterPlan.Down := false;
    exit;

  end else
  begin

    pp.paramname := '';
    lp := SFDV(FormUnloadPlan, meData.FieldByName('load_plan_id').AsInteger, 'note', pp, fGrid);
    if lp.keyvalue>0 then
    begin
      self.planid := lp.keyvalue;
      meData.Close;
      meData.Open;
      sbFilterPlan.Down := true;
    end else
    begin
      self.planid := 0;
      sbFilterPlan.Down := false;
    end;

  end;

end;

end.
