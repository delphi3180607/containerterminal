﻿unit editload;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBGridEh, DBLookupEh, Vcl.Mask, DBCtrlsEh,
  Vcl.Buttons, Functions, grid, DBSQLLookUp, Vcl.DBCtrls;

type
  TFormEditLoad = class(TFormEdit)
    plSelectCont: TPanel;
    luContainerId: TDBSQLLookUp;
    luKind: TDBSQLLookUp;
    luOwner: TDBSQLLookUp;
    edNote: TDBEditEh;
    ssContainers: TADOLookUpSqlSet;
    ssContainerKind: TADOLookUpSqlSet;
    procedure luCarriageChange(Sender: TObject);
    procedure plSelectContClick(Sender: TObject);
    procedure luContainerIdChange(Sender: TObject);
    procedure luContainerIdKeyValueChange(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    function CheckValid(source_dataset: TDataSet): boolean;
  end;

var
  FormEditLoad: TFormEditLoad;

implementation

{$R *.dfm}

uses counteragents, carriages, dmu, directions, ContainersOnGo;

procedure TFormEditLoad.luCarriageChange(Sender: TObject);
begin
  inherited;

  if not self.Visible then exit;

end;

procedure TFormEditLoad.luContainerIdChange(Sender: TObject);
var oldkeyid: integer;
begin
  oldkeyid := luContainerId.KeyValue;
end;

procedure TFormEditLoad.luContainerIdKeyValueChange(Sender: TObject);
var oldkeyid: integer; ds: TDataSet;
begin

  ds := dsLocal.DataSet;

  oldkeyid := luContainerId.KeyValue;

  // ds.FieldByName('owner_id').Value := ssContainers.ListDataSet.FieldByName('owner_id').Value;
  // ds.FieldByName('kind_id').Value := ssContainers.ListDataSet.FieldByName('kind_id').Value;

end;

procedure TFormEditLoad.plSelectContClick(Sender: TObject);
var pv: TPairValues; pp: TPairParam; ds: TDataSet; fGrid: TForm;
begin

  fGrid := nil;

  ds := dsLocal.DataSet;

  pp.paramname := 'sections';
  pp.paramvalue := 'output-store';

  pv := SFDV(FormContainersOnGo, 0, 'cnum', pp, fGrid);

  if pv.keyvalue>0 then
  begin

      if CheckValid(TFormGrid(fGrid).meData) then
      begin
        ds.FieldByName('container_id').Value := pv.keyvalue;
        // СЕРДАШЕНКО - добавить поле isempty
        // ds.FieldByName('isempty').Value := TFormGrid(fGrid).meData.FieldByName('isempty').Value;
        //
        ds.FieldByName('owner_id').Value := TFormGrid(fGrid).meData.FieldByName('owner_id').Value;
        ds.FieldByName('kind_id').Value := TFormGrid(fGrid).meData.FieldByName('kind_id').Value;
      end else
      begin
        ds.FieldByName('container_id').Value := null;
      end;
  end;

end;


procedure TFormEditLoad.btnOkClick(Sender: TObject);
begin
  if CheckValid(ssContainers.ListDataSet) then
    self.ModalResult := mrOk
  else
    self.ModalResult := mrNone;
end;

function TFormEditLoad.CheckValid(source_dataset: TDataSet): boolean;
var ds: TDataSet;
begin

      ds := dsLocal.DataSet;
      result := true;

      if ds.FieldByName('isfreeload').AsBoolean then
      if not source_dataset.FieldByName('isempty').AsBoolean then
      begin
        ShowMessage('Выбранный контейнер не отмечен как порожний. Свободная погрузка возможна только для порожних контейнеров.');
        result := false;
        exit;
      end;

      if ds.FieldByName('footsize').AsInteger<>0 then
      if source_dataset.FieldByName('footsize').AsInteger<>ds.FieldByName('footsize').AsInteger then
      begin
          ShowMessage('ВНИМАНИЕ!!! ФУТОВОСТЬ КОНТЕЙНЕРА ПО ПЛАНУ НЕ СООТВЕТСТВУЕТ ФУТОВОСТИ ВЫБРАННОГО КОНТЕЙНЕРА. ПОГРУЗКА НЕВОЗМОЖНА!');
          result := false;
          exit;
      end;

      if ds.FieldByName('kind_id').AsInteger<>0 then
      if source_dataset.FieldByName('kind_id').AsInteger<>ds.FieldByName('kind_id').AsInteger then
      begin
        if fQYN('Тип выбранного контейнера не соответствует заданному. Выполнять?') then
        begin
          ds.FieldByName('kind_id').Value := ssContainers.ListDataSet.FieldByName('kind_id').Value;
          result := true;
        end else
        begin
          result := false;
          exit;
        end;
      end;

      if ds.FieldByName('owner_id').AsInteger<>0 then
      if source_dataset.FieldByName('owner_id').AsInteger<>ds.FieldByName('owner_id').AsInteger then
      begin

        ShowMessage('ВНИМАНИЕ!!! ВЫБРАН КОНТЕЙНЕР С ДРУГИМ СОБСТВЕННИКОМ. ПОГРУЗКА НЕВОЗМОЖНА!');
        result := false;
        exit;

      end;

end;

end.
