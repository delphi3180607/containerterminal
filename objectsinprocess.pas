﻿unit ObjectsInProcess;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, docflow, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls,
  Vcl.ComCtrls, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, System.ImageList, Vcl.ImgList, PngImageList,
  Vcl.Mask, DBCtrlsEh;

type
  TFormObjectsInProcess = class(TFormDocFlow)
  private
    { Private declarations }
  public
    //
  end;

var
  FormObjectsInProcess: TFormObjectsInProcess;

implementation

{$R *.dfm}


end.
