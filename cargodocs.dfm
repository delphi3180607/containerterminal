﻿inherited FormCargoDocs: TFormCargoDocs
  ActiveControl = dgData
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1075#1088#1091#1079#1086#1076#1086#1089#1090#1072#1074#1082#1080
  ClientHeight = 611
  ClientWidth = 743
  ExplicitWidth = 759
  ExplicitHeight = 649
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 415
    Width = 743
    Height = 7
    Cursor = crVSplit
    Align = alBottom
    Beveled = True
    ExplicitTop = 406
  end
  inherited SplitterVert: TSplitter
    Height = 415
    ExplicitHeight = 407
  end
  inherited plBottom: TPanel
    Top = 578
    Width = 743
    ExplicitTop = 578
    ExplicitWidth = 743
    inherited btnOk: TButton
      Left = 508
      ExplicitLeft = 508
    end
    inherited btnCancel: TButton
      Left = 627
      ExplicitLeft = 627
    end
  end
  object DBGridEh1: TDBGridEh [3]
    Left = 0
    Top = 422
    Width = 743
    Height = 156
    Align = alBottom
    BorderStyle = bsNone
    DataSource = dsSpec
    DynProps = <>
    Flat = True
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
    TabOrder = 3
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -13
    TitleParams.Font.Name = 'Verdana'
    TitleParams.Font.Style = [fsBold]
    TitleParams.ParentFont = False
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'object_type'
        Footers = <>
        Title.Caption = #1057#1086#1089#1090#1072#1074'/'#1058#1080#1087
        Width = 210
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'cnum'
        Footers = <>
        Title.Caption = #1053#1086#1084#1077#1088
        Width = 157
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'kind_code'
        Footers = <>
        Title.Caption = #1042#1080#1076
        Width = 144
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'proper_state_code'
        Footers = <>
        Title.Caption = #1062#1077#1083#1077#1074#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
        Width = 195
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited plAll: TPanel
    Width = 543
    Height = 415
    ExplicitWidth = 543
    ExplicitHeight = 415
    inherited SplitterHor: TSplitter
      Top = 223
      Width = 543
      ExplicitLeft = -1
      ExplicitTop = 218
      ExplicitWidth = 544
    end
    inherited plTop: TPanel
      Width = 543
      ExplicitWidth = 543
      inherited btTool: TPngSpeedButton
        Left = 345
        ExplicitLeft = 776
      end
      inherited plCount: TPanel
        Left = 382
        inherited edCount: TDBEditEh
          ControlLabel.ExplicitLeft = 0
          ControlLabel.ExplicitTop = -16
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 543
      Height = 160
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 100
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'service_code'
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1086#1087#1077#1088#1072#1094#1080#1080
          Width = 123
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doctype_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 100
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 77
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 78
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'forwarder_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          Width = 129
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 142
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer_code'
          Footers = <>
          Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
          Width = 161
        end
        item
          Alignment = taRightJustify
          CellButtons = <>
          DisplayFormat = '### ### ###.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'summa'
          Footers = <>
          Title.Caption = #1057#1091#1084#1084#1072
          Width = 113
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_plan'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1087#1083#1072#1085
          Width = 83
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_fact'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1092#1072#1082#1090
          Width = 135
        end>
    end
    inherited plStatus: TPanel
      Width = 543
      ExplicitWidth = 543
    end
    inherited plLinkedObjects: TPanel
      Top = 233
      Width = 537
      ExplicitTop = 233
      ExplicitWidth = 537
      inherited dgLinkedObjects: TDBGridEh
        Width = 537
      end
    end
    inherited plSearch: TPanel
      Width = 543
      ExplicitWidth = 543
      inherited edSearch: TEdit
        Width = 361
        ExplicitWidth = 361
      end
    end
  end
  inherited plLeft: TPanel
    Height = 415
    ExplicitHeight = 415
    inherited Bevel4: TBevel
      Height = 390
      ExplicitHeight = 382
    end
    inherited dgFolders: TDBGridEh
      Height = 390
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select h.*,'
      
        '(select code from doctypes dt where dt.id = h.doctype_id) as doc' +
        'type_code,'
      
        '(select code from counteragents c1 where c1.id = h.forwarder_id)' +
        ' as forwarder_code,'
      
        '(select code from counteragents c2 where c2.id = h.consignee_id)' +
        ' as consignee_code,'
      
        '(select code from counteragents c3 where c3.id = h.payer_id) as ' +
        'payer_code,'
      
        '(select scode from servicekinds k where k.id = h.service_id) as ' +
        'service_code'
      'from doccargoheads h order by h.doc_date')
    UpdateCommand.CommandText.Strings = (
      'update doccargoheads'
      'set'
      '  doctype_id = :doctype_id,'
      '  basedoc_id = :basedoc_id,'
      '  doc_number = :doc_number,'
      '  doc_date = :doc_date,'
      '  doc_plan = :doc_plan,'
      '  doc_fact = :doc_fact,'
      '  forwarder_id = :forwarder_id,'
      '  consignee_id = :consignee_id,'
      '  payer_id = :payer_id,'
      '  service_id = :service_id,'
      '  summa = :summa'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'doctype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'basedoc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'doc_plan'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'doc_fact'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'forwarder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'consignee_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'payer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'service_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'summa'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into doccargoheads'
      
        '  (doctype_id, basedoc_id, doc_number, doc_date, doc_plan, doc_f' +
        'act, forwarder_id, '
      '   consignee_id, payer_id, service_id, summa)'
      'values'
      
        '  (:doctype_id, :basedoc_id, :doc_number, :doc_date, :doc_plan, ' +
        ':doc_fact, '
      
        '   :forwarder_id, :consignee_id, :payer_id, :service_id,  :summa' +
        ')')
    InsertCommand.Parameters = <
      item
        Name = 'doctype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'basedoc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'doc_plan'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'doc_fact'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'forwarder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'consignee_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'payer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'service_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'summa'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from doccargoheads where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select h.*,'
      
        '(select code from doctypes dt where dt.id = h.doctype_id) as doc' +
        'type_code,'
      
        '(select code from counteragents c1 where c1.id = h.forwarder_id)' +
        ' as forwarder_code,'
      
        '(select code from counteragents c2 where c2.id = h.consignee_id)' +
        ' as consignee_code,'
      
        '(select code from counteragents c3 where c3.id = h.payer_id) as ' +
        'payer_code,'
      
        '(select scode from servicekinds k where k.id = h.service_id) as ' +
        'service_code'
      'from doccargoheads h where h.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  object drvSpec: TADODataDriverEh [20]
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select s.*, o.cnum, o.kind_code,'
      
        '(select code from statekinds k where k.id = o.state_id) as state' +
        '_code,'
      
        '(select code from statekinds k where k.id = s.proper_state_id) a' +
        's proper_state_code'
      'from doccargospecs s, v_objects o '
      'where s.object_id = o.id and s.object_type = o.object_type'
      '')
    SelectCommand.Parameters = <>
    UpdateCommand.CommandText.Strings = (
      'update doccargospecs'
      'set'
      '  proper_state_id = :proper_state_id'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'proper_state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into doccargospecs'
      
        '  (dochead_id, object_type, object_id, parentobject_id, proper_s' +
        'tate_id)'
      'values'
      
        '  (:dochead_id, :object_type, :object_id, :parentobject_id, :pro' +
        'per_state_id)')
    InsertCommand.Parameters = <
      item
        Name = 'dochead_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'object_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'object_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'parentobject_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'proper_state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from doccargospecs where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select s.*, o.cnum, o.kind_code,'
      
        '(select code from statekinds k where k.id = o.state_id) as state' +
        '_code,'
      
        '(select code from statekinds k where k.id = s.proper_state_id) a' +
        's proper_state_code'
      'from doccargospecs s, v_objects o '
      'where s.object_id = o.id and s.object_type = o.object_type'
      'and s.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 128
    Top = 464
  end
  object meSpec: TMemTableEh [21]
    Params = <>
    DataDriver = drvSpec
    Left = 176
    Top = 464
  end
  object dsSpec: TDataSource [22]
    DataSet = meSpec
    Left = 232
    Top = 464
  end
  inherited IL: TPngImageList
    Bitmap = {}
  end
end
