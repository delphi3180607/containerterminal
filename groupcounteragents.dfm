﻿inherited FormGroupCounteragents: TFormGroupCounteragents
  Caption = #1043#1088#1091#1087#1087#1080#1088#1086#1074#1082#1072' '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090#1086#1074
  ClientHeight = 143
  ExplicitHeight = 171
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 20
    Top = 18
    Width = 137
    Height = 16
    Caption = #1043#1083#1072#1074#1085#1099#1081' '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090
  end
  object sbRemoveLink: TSpeedButton [1]
    Left = 455
    Top = 40
    Width = 74
    Height = 24
    Caption = #1054#1090#1074#1103#1079#1072#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = sbRemoveLinkClick
  end
  inherited plBottom: TPanel
    Top = 102
    TabOrder = 1
    ExplicitTop = 102
  end
  object luCounteragent1: TDBSQLLookUp [3]
    Left = 20
    Top = 40
    Width = 429
    Height = 24
    DataField = 'id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 0
    Visible = True
    SqlSet = ssCounteragents
    RowCount = 0
  end
  inherited dsLocal: TDataSource
    DataSet = meTable
    Left = 168
    Top = 88
  end
  inherited qrAux: TADOQuery
    SQL.Strings = (
      'select 0 as id')
    Left = 32
    Top = 88
  end
  object meTable: TMemTableEh
    Params = <>
    Left = 96
    Top = 88
  end
  object ssCounteragents: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from counteragents '
      'where (name like '#39'%@pattern%'#39' or code like '#39'%@pattern%'#39') '
      'and id <> :selfid'
      'order by code')
    DownSql.Strings = (
      'select top 50 * from counteragents '
      'where id <> :selfid'
      'order by code')
    InitSql.Strings = (
      'select * from counteragents where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 248
    Top = 91
  end
end
