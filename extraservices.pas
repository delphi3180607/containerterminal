﻿unit ExtraServices;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, Vcl.StdCtrls, docflow, EXLReportExcelTLB,
  EXLReportBand, EXLReport, functions, Vcl.Mask, DBCtrlsEh;

type
  TFormExtraServices = class(TFormGrid)
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    plSpec: TPanel;
    dgSpec: TDBGridEh;
    Panel2: TPanel;
    sbDeleteSpec: TPngSpeedButton;
    sbExcelSpec: TPngSpeedButton;
    sbAddSpec: TPngSpeedButton;
    sbEditSpec: TPngSpeedButton;
    Bevel1: TBevel;
    pmSpec: TPopupMenu;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    JvxSplitter1: TSplitter;
    procedure sbAddSpecClick(Sender: TObject);
    procedure sbDeleteSpecClick(Sender: TObject);
    procedure sbEditSpecClick(Sender: TObject);
    procedure sbExcelSpecClick(Sender: TObject);
    procedure meSpecBeforePost(DataSet: TDataSet);
    procedure meDataAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormExtraServices: TFormExtraServices;

implementation

{$R *.dfm}

uses editdocspecservice, editextraservice, containers;

procedure TFormExtraServices.Init;
begin
  self.formEdit := FormEditExtraService;
  inherited;
  meSpec.Open;
  FormContainers.meData.Open;
end;

procedure TFormExtraServices.meDataAfterInsert(DataSet: TDataSet);
begin
  inherited;
  meData.FieldByName('doc_date').Value := now();
  meData.FieldByName('date_factexecution').Value := now();
end;

procedure TFormExtraServices.meSpecBeforePost(DataSet: TDataSet);
begin
  inherited;
  meSpec.FieldByName('doc_id').Value := meData.FieldByName('id').AsInteger;
end;

procedure TFormExtraServices.sbAddSpecClick(Sender: TObject);
begin
  EA(self, FormEditDocSpecService,meSpec,'docspecextraservices','doc_id',meData.FieldByName('id').AsInteger);
end;

procedure TFormExtraServices.sbDeleteSpecClick(Sender: TObject);
begin
  ED(meSpec);
end;

procedure TFormExtraServices.sbEditSpecClick(Sender: TObject);
begin
  inherited;
  EE(self, FormEditDocSpecService,meSpec,'docspecextraservices');
end;

procedure TFormExtraServices.sbExcelSpecClick(Sender: TObject);
begin
  dgSpec.SetFocus;
  btExcelClick(Sender);
end;

end.
