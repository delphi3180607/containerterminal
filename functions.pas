﻿unit Functions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, edit,
  PngSpeedButton, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  GridsEh, DBAxisGridsEh, DBGridEh, MemTableDataEh, Data.DB,
  MemTableEh, DataDriverEh, DBXDataDriverEh, Data.SqlExpr, Vcl.Menus,
  Vcl.ComCtrls, Vcl.ToolWin, Vcl.StdCtrls, Vcl.FileCtrl, Data.Win.ADODB,
  ADODataDriverEh, EhLibVCL, DateUtils, IdAllFTPListParsers, StrUtils, QYN,
  IOUtils, DBCtrlsEh, DBLookupEh, ComObj, DBSQLLookUp, Math, Vcl.CheckLst;

type TPairValues = record
  keyvalue: integer;
  displayvalue: string;
end;

type TPairParam = record
  paramname: string;
  paramvalue: string;
end;

function fQYN(mess: string):boolean;
procedure SFD(t: TForm; ef: TDBLookupComboboxEh);
function SFDE(c: TComponentClass; ef: TDBSQLLookUp; maximize: boolean = false): boolean;
function SFDLE(n: TForm; ef: TDBSQLLookUp): boolean;
function SFDV(t: TForm; startkeyvalue: integer; displayfieldname: string; pl:TPairParam; var form: TForm): TPairValues;
function SFDVMulti(t: TForm; startkeyvalue: integer; displayfieldname: string; pl:TPairParam; var form: TForm): TPairList;
function ValidPath (const Path:string):string;
function DeleteAnyFile (const FileName:string ):Integer;
function ClearEmptyDirs(const Path : string):Integer;
function ClearDir (const Path : string):Integer;
function ClearSubDir(const Path: string; DelEmpty: Boolean): Integer;
function ExecAndWait(const FileName, Params: ShortString; const WinState: Word): boolean; export;
function EA(parent_grid_form: TForm; f: TFormEdit; ds: TMemTableEh; tablename: string; pid_name:string =''; pid:integer =0): boolean;
function EE(parent_grid_form: TForm; f: TFormEdit; ds: TMemTableEh; tablename: string): boolean;
function ED(ds: TMemTableEh): boolean;
function ColumnSelectList(dg: TDbGridEh; var text: string): integer;
procedure RefreshRecord(ds: TMemTableEh; id:integer);
procedure ExportExcel(grid: TDbGridEh; title: string; formats: TStringList = nil);
function GetMyVersion:string;
function VarToInt(v: Variant):integer;
function VarToReal(v: Variant): Real;
procedure EmptyKeyQueue;
procedure ShowBigMessage(mess: string);
function XorEncode(Source, Key: AnsiString): AnsiString;
function XorDecode(Source, Key: AnsiString): AnsiString;
procedure ClearControls(panel: TWinControl);
procedure MakeClassifFilterSet(panel: TWinControl; table_name: string; start_y: integer; delegate_event: TNotifyEvent);
procedure MakeClassifInput(panel: TWinControl; table_name: string; table_id: integer; start_y: integer = 5; delegate_event: TNotifyEvent = nil);
procedure MakeClassifOptionsControl(panel: TWinControl; classification_id: integer; table_id: integer; caption: string ;ismulty: boolean; var last_y: integer; delegate_event: TNotifyEvent = nil);
procedure FillClassifCheckListBox(c: TCheckListBox; classification_id: integer; table_id: integer);
function Split(const StringParam: String; const DelimChar: String): Variant;


implementation

uses grid, seek, bigmessage, dmu;

procedure EmptyKeyQueue;
var
 msg: TMsg;
begin
 while PeekMessage(msg, 0, WM_KEYFIRST, WM_KEYLAST, PM_REMOVE or PM_NOYIELD) do
  ;
end;


function VarToInt(v: Variant):integer;
begin
  if v = null then
  begin
    result := 0;
    exit;
  end;
  if v <= 0 then
  begin
    result := 0;
    exit;
  end;

  result := v;

end;

function VarToReal(v: Variant): Real;
begin
  if v = null then
  begin
    result := 0;
    exit;
  end;
  if VarToStr(v)='' then
  begin
    result := 0;
    exit;
  end;
  result := Real(v);
end;

function GetMyVersion:string;
type
  TVerInfo=packed record
    Nevazhno: array[0..47] of byte; // ненужные нам 48 байт
    Minor,Major,Build,Release: word; // а тут версия
  end;
var
  s:TResourceStream;
  v:TVerInfo;
begin
  result:='';
  try
    s:=TResourceStream.Create(HInstance,'#1',RT_VERSION); // достаём ресурс
    if s.Size>0 then begin
      s.Read(v,SizeOf(v)); // читаем нужные нам байты
      result:=IntToStr(v.Major)+'.'+IntToStr(v.Minor)+'.'+ // вот и версия...
              IntToStr(v.Release)+'.'+IntToStr(v.Build);
    end;
  s.Free;
  except; end;
end;


procedure SFD(t: TForm; ef: TDBLookupComboboxEh);
var n: TFormGrid;
begin
  n := TFormGrid(t).CreateNew(Application);
  n.Init;
  n.Position := poMainFormCenter;
  n.Width := Screen.Width div 2;
  n.Height := Screen.Height div 2;
  n.Parent := nil;
  n.plBottom.Show;
  n.ShowModal;
  if n.ModalResult = mrOk then
  begin
     ef.KeyValue := n.meData.FieldByName('id').AsInteger;
     ef.Refresh;
     ef.ListSource.DataSet.Close;
     ef.ListSource.DataSet.Open;
  end;
end;


function SFDV(t: TForm; startkeyvalue: integer; displayfieldname: string; pl: TPairParam; var form: TForm): TPairValues;
var n: TFormGrid;
begin

  result.keyvalue := 0;
  result.displayvalue := '';

  if not (Assigned(form) and (form <> nil) )then
  begin

    Application.CreateForm(TComponentClass(t.ClassType),n);
    form := n;
    n.Align := alNone;

    Screen.Cursor := crHourGlass;
    n.meData.Close;

    if pl.paramname<>'' then
      if Assigned(n.drvData.SelectCommand.Parameters.FindParam(pl.paramname)) then
        n.drvData.SelectCommand.Parameters.ParamByName(pl.paramname).Value := pl.paramvalue;

    n.Caption := n.Caption + ' - Выберете или Создайте';
    n.Init;

    Screen.Cursor := crDefault;

    n.pairparam := pl;

    n.Position := poMainFormCenter;
    n.Width := Screen.Width-100 ;
    n.Height := Trunc(Screen.Height/1.5);
    n.Parent := nil;
    n.plBottom.Show;
    n.meData.Locate('id', startkeyvalue, []);

  end else
    n := TFormGrid(form);

  n.ShowModal;
  if n.ModalResult = mrOk then
  begin
     if n.meData.Eof then
     begin
       n.meData.Prior;
       n.meData.Next;
     end;
     result.keyvalue := n.meData.FieldByName('id').AsInteger;
     result.displayvalue := n.meData.FieldByName(displayfieldname).AsString;
  end;

  if n.ModalResult = mrYes then
  begin
     result.keyvalue := -1;
     result.displayvalue := '';
  end;

  form := n;
end;



function SFDVMULTI(t: TForm; startkeyvalue: integer; displayfieldname: string; pl: TPairParam; var form: TForm): TPairList;
var n: TFormGrid; l: TPairList; i: integer;
begin

  result := TPairList.Create;

  if not Assigned(form) then
  begin
    Application.CreateForm(TComponentClass(t.ClassType),n);
    form := n;
    n.Init;
    n.Align := alNone;

    if pl.paramname<>'' then
      if Assigned(n.drvData.SelectCommand.Parameters.FindParam(pl.paramname)) then
        n.drvData.SelectCommand.Parameters.ParamByName(pl.paramname).Value := pl.paramvalue;

    n.Caption := n.Caption + ' - Выберете или Создайте';
    n.pairparam := pl;

    n.Position := poMainFormCenter;
    n.Width := (Screen.Width*3) div 4;
    n.Height := (Screen.Height*3) div 4;
    n.Parent := nil;
    n.plBottom.Show;
    n.meData.Locate('id', startkeyvalue, []);

  end else
    n := TFormGrid(form);

  n.ShowModal;
  if n.ModalResult = mrOk then
  begin

    if n.dgData.Selection.SelectionType = (gstAll) then
    begin

      ShowMessage('Нельзя скопировать все записи. Выделите вручную с помощью Shift+Мышь.');

    end else
    if n.dgData.Selection.SelectionType = (gstRecordBookmarks) then
    begin

      for i := 0 to n.dgData.Selection.Rows.Count-1 do
      begin

       n.meData.Bookmark := n.dgData.Selection.Rows[i];
       result.Keys.Add(n.meData.FieldByName('id').AsString);
       result.DisplayValues.Add(n.meData.FieldByName(displayfieldname).AsString);

      end;

    end else
    begin

       result.Keys.Add(n.meData.FieldByName('id').AsString);
       result.DisplayValues.Add(n.meData.FieldByName(displayfieldname).AsString);

    end;

  end else
  begin
    //result.Keys.Add('0');
    //result.DisplayValues.Add('');
  end;
end;

function SFDE(c: TComponentClass; ef: TDBSQLLookUp; maximize: boolean = false): boolean;
var n: TFormGrid;
begin
  Application.CreateForm(c,n);
  n.Init;
  n.Position := poMainFormCenter;
  n.Width := Screen.Width-200;
  n.Height := Screen.Height-300;
  n.Parent := nil;
  n.plBottom.Show;
  n.meData.Locate('id', VarToInt(ef.KeyValue),[]);
  result := false;
  if maximize then
  n.WindowState := wsMaximized;
  n.ShowModal;
  if n.ModalResult = mrOk then
  begin
     ef.KeyValue := n.meData.FieldByName('id').AsInteger;
     ef.Refresh;
     result := true;
  end;
  n.Free;
end;


function SFDLE(n: TForm; ef: TDBSQLLookUp): boolean;
begin
  TFormGrid(n).Init;
  n.Position := poMainFormCenter;
  n.Width := Screen.Width-200;
  n.Height := Screen.Height-300;
  n.Parent := nil;
  TFormGrid(n).plBottom.Show;
  TFormGrid(n).meData.Locate('id', VarToInt(ef.KeyValue),[]);
  result := false;
  n.ShowModal;
  if n.ModalResult = mrOk then
  begin
     ef.KeyValue := TFormGrid(n).meData.FieldByName('id').AsInteger;
     ef.Refresh;
     result := true;
  end;
end;

function EA(parent_grid_form: TForm; f: TFormEdit; ds: TMemTableEh; tablename: string; pid_name:string =''; pid:integer =0): boolean;
var drv: TADODataDriverEh; i: integer; f1: TFormEdit;
begin

  drv := TADODataDriverEh(ds.DataDriver);

  if not ds.Active then ds.Open;

  Application.CreateForm(TComponentClass(f.ClassType),f1);

  if not Assigned(f1.dsLocal.DataSet) then
    f1.dsLocal.DataSet := ds;

  if Assigned(parent_grid_form) then
    TFormGrid(parent_grid_form).current_edit_form := f1;

  ds.Append;

  for i := 0 to ds.FieldDefs.Count-1 do
  begin
    if ds.FieldDefs[i].DataType = ftBoolean then
      if ds.Fields[i].Value = null then ds.Fields[i].Value := 0;
  end;

  f1.ParentFormGrid := parent_grid_form;
  f1.current_mode := 'insert';
  f1.ShowModal;

  if f1.ModalResult = mrOk then
  begin

     result := true;

     if pid_name<>''  then
     begin
       ds.FieldByName(pid_name).AsInteger := pid;
     end;

     if ds.State = dsInsert then
     ds.Post;

     f1.qrAux.Close;
     f1.qrAux.SQL.Clear;
     if tablename<>'' then
       f1.qrAux.SQL.Add('select IDENT_CURRENT('''+tablename+''')')
     else
       f1.qrAux.SQL.Add('select @@IDENTITY');

     f1.qrAux.Open;

     if drv.GetrecCommand.Parameters.FindParam('current_id') <> nil then
     begin
       drv.GetrecCommand.Parameters.ParamByName('current_id').Value := f1.qrAux.Fields[0].AsInteger;
       ds.RefreshRecord;
     end else
     begin
       RefreshRecord(ds,f1.qrAux.Fields[0].AsInteger);
     end;

  end else
  begin
     //f1.dsLocal.DataSet := nil;
     result := false;
     ds.Cancel;
  end;

  f1.Free;

end;


function EE(parent_grid_form: TForm; f: TFormEdit; ds: TMemTableEh; tablename: string): boolean;
var drv: TADODataDriverEh; i: integer; f1: TFormEdit;
begin

  drv := TADODataDriverEh(ds.DataDriver);

  //if Assigned(f) then f.Free;
  Application.CreateForm(TComponentClass(f.ClassType),f1);

  ds.ReadOnly := false;

  f1.dsLocal.DataSet := ds;

  if Assigned(parent_grid_form) then
    TFormGrid(parent_grid_form).current_edit_form := f1;

  ds.Edit;

  if not ds.ReadOnly then
  begin
    for i := 0 to ds.FieldDefs.Count-1 do
    begin
      if ds.FieldDefs[i].DataType = ftBoolean then
        if ds.Fields[i].Value = null then ds.Fields[i].Value := 0;
    end;
  end;

  f1.current_id := ds.FieldByName('id').AsInteger;
  f1.ParentFormGrid := parent_grid_form;
  f1.current_mode := 'update';
  f1.ShowModal;

  if f1.ModalResult = mrOk then
  begin

     result := true;

     if not ds.ReadOnly then
     begin

       if ds.State = dsEdit then
       ds.Post;

       if drv.GetrecCommand.Parameters.FindParam('current_id') <> nil then
       begin
         drv.GetrecCommand.Parameters.ParamByName('current_id').Value := ds.FieldByName('id').AsInteger;
         ds.RefreshRecord;
       end else
       begin
         RefreshRecord(ds,ds.FieldByName('id').AsInteger);
       end;
     end else
      ds.Cancel;

  end else
  begin
     //if not ds.ReadOnly then ds.Cancel;
     result := false;
     ds.Cancel;
     //f.dsLocal.DataSet := nil;
  end;

  //f.dsLocal.DataSet := nil;
  f1.Free;

end;


function ED(ds: TMemTableEh): boolean;
begin
  result := false;
  if fQYN('Удалить запись ?') then
  begin
     result := true;
     ds.Delete;
  end;
end;


procedure RefreshRecord(ds: TMemTableEh; id: integer);
begin
  ds.Close;
  ds.Open;
  ds.Locate('id',id,[]);
end;



function fQYN(mess: string): boolean;
begin
  FormQYN.Label1.Caption := mess;
  FormQYN.Height := FormQYN.Label1.Height + 100;
  FormQYN.ShowModal;
  result := (FormQYN.ModalResult = mrOk);
end;


//-----------------------------------------------------------------------
function ValidPath ( const Path : string ) : string;
begin
  Result := Trim ( Path );
  if  ( Result <> '' )
  and ( Result [ Length ( Result )] <> '\' ) then
    Result := Result + '\';
end;

// удаление любого файла ( Read-Only, System, Hidden )
//-----------------------------------------------------------------------------
function DeleteAnyFile ( const FileName : string ) : Integer;
var LastError: integer;
begin
  LastError := 0;
  FileSetAttr ( FileName, faArchive );
  if not DeleteFile ( PChar ( FileName )) then
    LastError := GetLastError;
  Result := LastError;
end;

function ClearEmptyDirs(const Path: string): Integer;
var
  SearchRec : TSearchRec;
  Code      : Integer;
begin
  // искать подкаталоги
  Code := FindFirst (ValidPath(Path) + '*.*', faDirectory, SearchRec);
  try
    while Code = 0 do begin
      // пропуск системных каталогов
      if SearchRec.Name[1] = '.' then begin
        Code := FindNext( SearchRec);
        Continue;
      end;
      // очистить подкаталог
      if TDirectory.Exists(ValidPath(Path)+SearchRec.Name) then
       if TDirectory.IsEmpty(ValidPath(Path)+SearchRec.Name) then
       begin
          TDirectory.Delete(ValidPath(Path)+SearchRec.Name);
       end;
       Code := FindNext(SearchRec);
    end;
  finally
    FindClose (SearchRec);
  end;
end;

function ClearDir (const Path: string): Integer;
var
  SearchRec  : TSearchRec;
  ErrorCount : Integer;
  LastError: integer;
begin
  ErrorCount := 0;
  Result     := FindFirst ( ValidPath ( Path ) + '*.*', faAnyFile, SearchRec );
  // NT bug on FindClose
  if Result = 18 then begin
    Result := 0;
    Exit;
  end;
  while Result = 0 do begin
    try
      // удаление найденного файла
        if DeleteAnyFile ( ValidPath ( Path ) + SearchRec.Name ) <> 0 then begin
          Inc ( ErrorCount );
          LastError := GetLastError;
        end;
    except
    end;
    Result := FindNext ( SearchRec );
  end;
  FindClose ( SearchRec );
  Result := ErrorCount;
  if Result > 0 then Result := LastError;
end;

function ClearSubDir(const Path: string; DelEmpty: Boolean): Integer;
var
  SearchRec : TSearchRec;
  Code      : Integer;
begin
  // удалить все файлы из каталога
  Result := ClearDir(Path);
  // искать подкаталоги
  Code := FindFirst ( ValidPath ( Path ) + '*.*', faDirectory, SearchRec );
  try
    while Code = 0 do begin
      // пропуск системных каталогов
      if SearchRec.Name [1] = '.' then begin
        Code := FindNext ( SearchRec );
        Continue;
      end;
      // очистить подкаталог
      Result := ClearSubDir ( ValidPath ( ValidPath ( Path ) + SearchRec.Name ), DelEmpty);
      // удалить подкаталог
      if DelEmpty then
        try
          RmDir ( ValidPath ( Path ) + SearchRec.Name );
          Result := IOResult;
        except
          Result := IOResult;
        end;
      Code := FindNext ( SearchRec );
    end;
  finally
    FindClose (SearchRec);
  end;
end;


function ExecAndWait(const FileName,
                     Params: ShortString;
                     const WinState: Word): boolean; export;
var
  StartInfo: TStartupInfo;
  ProcInfo: TProcessInformation;
  CmdLine: ShortString;
begin
  { Помещаем имя файла между кавычками, с соблюдением всех пробелов в именах Win9x }
  CmdLine := '"' + Filename + '" ' + Params;
  FillChar(StartInfo, SizeOf(StartInfo), #0);
  with StartInfo do
  begin
    cb := SizeOf(StartInfo);
    dwFlags := STARTF_USESHOWWINDOW;
    wShowWindow := WinState;
  end;
  Result := CreateProcess(nil, PChar( String( CmdLine ) ), nil, nil, false,
                          CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS, nil,
                          PChar(ExtractFilePath(Application.ExeName)),StartInfo,ProcInfo);
  { Ожидаем завершения приложения }
  if Result then
  begin
    WaitForSingleObject(ProcInfo.hProcess, INFINITE);
    { Free the Handles }
    CloseHandle(ProcInfo.hProcess);
    CloseHandle(ProcInfo.hThread);
  end;
end;



procedure ExportExcel(grid: TDbGridEh; title: string; formats: TStringList = nil);
var i,j,index: Integer; ds: TDataSet;
ExcelApp,sheet: Variant;
Range, Cell1, Cell2, ArrayData: Variant;
BeginCol, BeginRow: integer;
RowCount, ColCount : integer;
begin
 Screen.Cursor := crHourGlass;
 try
   Application.ProcessMessages;
   ds := grid.DataSource.DataSet;
   ds.DisableControls;
   ExcelApp := CreateOleObject('Excel.Application');
   ExcelApp.Visible := False;
   ExcelApp.WorkBooks.Add(-4167);
   ExcelApp.WorkBooks[1].WorkSheets[1].name := 'Export';
   sheet:=ExcelApp.WorkBooks[1].WorkSheets['Export'];

   sheet.cells[1,1] := title;
   sheet.cells[1,1].Font.Bold:=1;
   sheet.cells[1,1].Font.Size:=14;

   for i := 1 to grid.Columns.Count do
   begin
     sheet.columns.columns[i].ColumnWidth := IntToStr(trunc(grid.Columns[i-1].Width/8));
     //if formats<>nil then
     sheet.columns.columns[i].NumberFormatLocal := '##'; //formats[i-1];
     sheet.columns.columns[i].WrapText:= true;
     sheet.cells[2,i] := grid.Columns[i-1].Title.Caption;
     sheet.cells[2,i].Interior.Color:=$00DDFFFF;
     sheet.cells[2,i].WrapText:= true;
     sheet.cells[2,i].Borders.LineStyle:=1;
     sheet.cells[2,i].Borders.Weight:=2;
     sheet.cells[2,i].VerticalAlignment := -4108;
   end;

   sheet.cells[1,1].WrapText:= false;

   index := 3;
   ds.First;

   BeginRow := 3;
   BeginCol := 1;

   RowCount := ds.RecordCount;
   ColCount := grid.Columns.Count;

  // Создаем Вариантный Массив, который заполним выходными данными
  ArrayData := VarArrayCreate([1, RowCount, 1, ColCount], varVariant);

   // Заполняем массив
   for i:=1 to  ds.RecordCount do
   begin
      for j:=1 to grid.Columns.Count do
      begin
        {sheet.cells[index,j].WrapText:= true;
        sheet.cells[index,j].Borders.LineStyle:=1;
        sheet.cells[index,j].Borders.Weight:=2;
        sheet.cells[index,j].VerticalAlignment := -4108;
        sheet.cells[index,j]:=ds.DataSet.FieldByName(grid.Columns[j-1].FieldName).AsString;}
        try
          if grid.Columns[j-1].FieldName<>'' then ArrayData[i, j] := ds.FieldByName(grid.Columns[j-1].FieldName).AsString;
        except
         //
        end;
        // красим в желтый цвет

        {Cell1 := sheet.Cells[i, 1];
        Cell2 := sheet.Cells[i, ColCount];
        Range := sheet.Range[Cell1, Cell2];
        Range.Interior.Color := clYellow;}

      end;
      inc(index);
      ds.Next;
   end;

  // Левая верхняя ячейка области, в которую будем выводить данные
    Cell1 := sheet.Cells[BeginRow, BeginCol];

  // Правая нижняя ячейка области, в которую будем выводить данные
    Cell2 := sheet.Cells[BeginRow  + RowCount - 1, BeginCol + ColCount - 1];

  // Область, в которую будем выводить данные
    Range := sheet.Range[Cell1, Cell2];

  // А вот и сам вывод данных
    // Намного быстрее поячеечного присвоения
    Range.Value := ArrayData;
    Range.Borders.LineStyle:=1;
    Range.Borders.Weight:=2;
    Range.VerticalAlignment := -4108;

    ExcelApp.Visible := true;
    ds.EnableControls;

 except
    on E : Exception do begin
      ds.EnableControls;
      ShowMessage('Ошибка: '+E.Message);
    end;
 end;
 Screen.Cursor := crDefault;
end;



function ColumnSelectList(dg: TDbGridEh; var text: string): integer;
var pt, ptGrid: TPoint;
begin
   ptGrid.X:=dg.Left;
   ptGrid.Y:=dg.Top;
   ptGrid:=dg.ClientToScreen(ptGrid);

   pt.X:=ptGrid.X+dg.CellRect(dg.Col, dg.Row).Left; //1
   pt.Y:=ptGrid.Y+dg.CellRect(dg.Col, dg.Row).Top;  //2

   FormSeek.Left:=pt.X+2; //3
   FormSeek.Top:=pt.Y-40; //4

   if FormSeek.Left+FormSeek.Width > Screen.Width then
   begin
      FormSeek.Left := Screen.Width - FormSeek.Width - 10;
   end;

   FormSeek.InitList(text);
   if FormSeek.ProcessOdds then
   begin
     result := FormSeek.qrAux.FieldByName('id').AsInteger;
     exit;
   end;

   FormSeek.ShowModal;

   result := 0;

   if FormSeek.ModalResult = mrOk then
   begin
     result := FormSeek.qrAux.FieldByName('id').AsInteger;
   end;

end;

procedure ShowBigMessage(mess: string);
begin
  FormBigMessage.lbMessage.Caption := mess;
  FormBigMessage.ShowModal;
end;


function XorEncode(Source, Key: AnsiString): AnsiString;
var
  I: Integer;
  C: Byte;
begin
  Result := '';
  for I := 1 to Length(Source) do begin
    if Length(Key) > 0 then
      C := Byte(Key[1 + ((I - 1) mod Length(Key))]) xor Byte(Source[I])
    else
      C := Byte(Source[I]);
    Result := Result + AnsiLowerCase(IntToHex(C, 2));
  end;
end;

function XorDecode(Source, Key: AnsiString): AnsiString;
var
  I: Integer;
  C: AnsiChar;

begin
  Result := '';
  for I := 0 to Length(Source) div 2 - 1 do begin
    C := AnsiChar(StrToIntDef('$' + Copy(Source, (I * 2) + 1, 2), Ord(' ')));
    if Length(Key) > 0 then
      C := AnsiChar(Byte(Key[1 + (I mod Length(Key))]) xor Byte(C));
    Result := Result + C;
  end;
end;

procedure ClearControls(panel: TWinControl);
begin
  while panel.ControlCount>0 do
  begin
    panel.Controls[0].Free;
  end;
end;


procedure MakeClassifInput(panel: TWinControl; table_name: string; table_id: integer; start_y: integer = 5; delegate_event: TNotifyEvent = nil);
var y: integer;
begin
  ClearControls(panel);
  dm.qrAux.Close;
  dm.qrAux.SQL.Clear;
  dm.qrAux.SQL.Add('select * from classifications where table_name = '''+table_name+'''');
  dm.qrAux.Open;
  y := start_y;
  while not dm.qrAux.Eof do
  begin
    MakeClassifOptionsControl(panel, dm.qrAux.FieldByName('id').AsInteger, table_id, dm.qrAux.FieldByName('classification_name').AsString, dm.qrAux.FieldByName('ismulty').AsBoolean, y, delegate_event);
    dm.qrAux.Next;
  end;
end;

procedure MakeClassifOptionsControl(panel: TWinControl; classification_id: integer; table_id: integer; caption: string ;ismulty: boolean; var last_y: integer; delegate_event: TNotifyEvent = nil);
var cr: TCheckListBox; cb: TComboBox; l: TLabel; h,x,y: integer;
begin

  exit;


  x := 5; y:= last_y;

  l := TLabel.Create(panel);
  l.Top := y; l.Left := x;
  l.Caption := caption;
  l.Parent := panel;

  dm.qrCommon.Close;
  dm.qrCommon.SQL.Clear;
  dm.qrCommon.SQL.Add('select * from classificationvalues where classification_id = '+IntToStr(classification_id));
  dm.qrCommon.Open;

  if ismulty then
  begin
   cr := TCheckListBox.Create(panel);
   cr.Name := 'c_'+IntToStr(classification_id);
   cr.Parent := panel;

   h := 0;
   while not dm.qrCommon.Eof do
   begin
     cr.Items.Add(dm.qrCommon.FieldByName('string_value').AsString);
     dm.qrCommon.Next;
     h := h+18;
   end;

   cr.Top := y+18;
   cr.Width := Math.Min(panel.Width - 5, 250);
   cr.Anchors := [akLeft,akTop,akRight];
   cr.Height := h;
   y := y+18+h+10;

   FillClassifCheckListBox(cr, classification_id, table_id);

   cr.OnClickCheck := delegate_event;

  end else
  begin
   cb := TComboBox.Create(panel);
   cr.Name := 'c_'+IntToStr(classification_id);
   cb.Parent := panel;
  end;

  last_y := y;

end;

procedure FillClassifCheckListBox(c: TCheckListBox; classification_id: integer; table_id: integer);
var i, n: integer;
begin

  exit;

  dm.qrCommon.Close;
  dm.qrCommon.SQL.Clear;
  dm.qrCommon.SQL.Add('select cv.string_value from classifvaluesets vs, classificationvalues cv ');
  dm.qrCommon.SQL.Add('where vs.classification_id = '+IntToStr(classification_id)+' and vs.parent_table_id='+IntToStr(table_id));
  dm.qrCommon.SQL.Add('and vs.classif_value_id = cv.id');
  dm.qrCommon.Open;

  while not dm.qrCommon.Eof do
  begin

    n := c.Items.IndexOf(dm.qrCommon.FieldByName('string_value').AsString);
    c.Checked[n] := true;
    dm.qrCommon.Next;

  end;


end;


procedure MakeClassifFilterSet(panel: TWinControl; table_name: string; start_y: integer; delegate_event: TNotifyEvent);
begin

  exit;

  MakeClassifInput(panel, table_name, 0, start_y, delegate_event);
end;


function Split(const StringParam: String; const DelimChar: String): Variant;
var
  sTMP  : string;
  sa      : array of string;
  n,i,y   : Integer;
begin
  try
      sTMP:=StringParam;
      n:=Pos(DelimChar, sTMP);
      i:=0;
      y:=Length(DelimChar);

      while n>0 do begin

        i:=i+1;
        SetLength(sa,i);
        sa[i-1]:=Copy(sTMP,1,n-1);

        Delete(sTMP,1,n+y-1);

        n:=Pos(DelimChar, sTMP);

      end;

      i:=i+1;
      n:=Length(sTMP);
      SetLength(sa,i);
      sa[i-1]:=Copy(sTMP,1,n);
      Result:=sa;

  finally
      sTMP:='';
      sa:=nil;
  end;


end;



end.


