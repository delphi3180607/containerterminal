﻿inherited FormSelectFolder: TFormSelectFolder
  Caption = #1050#1072#1090#1072#1083#1086#1075#1080
  ClientHeight = 514
  ClientWidth = 479
  ExplicitWidth = 485
  ExplicitHeight = 542
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 468
    Width = 479
    Height = 46
    ExplicitTop = 468
    ExplicitWidth = 479
    ExplicitHeight = 46
    inherited btnCancel: TButton
      Left = 363
      Height = 40
      ExplicitLeft = 363
      ExplicitHeight = 40
    end
    inherited btnOk: TButton
      Left = 244
      Height = 40
      Caption = #1042#1099#1073#1088#1072#1090#1100
      ExplicitLeft = 244
      ExplicitHeight = 40
    end
  end
  object DBGridEh1: TDBGridEh [1]
    Left = 0
    Top = 0
    Width = 479
    Height = 468
    Align = alClient
    AutoFitColWidths = True
    BorderStyle = bsNone
    DataSource = dsFolders
    DynProps = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -13
    TitleParams.Font.Name = 'Tahoma'
    TitleParams.Font.Style = [fsBold]
    TitleParams.ParentFont = False
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'folder_name'
        Footers = <>
        TextEditing = False
        Title.Caption = #1050#1072#1090#1072#1083#1086#1075
        Width = 428
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object drvFolders: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select * from folders f where f.folder_section = :folder_section' +
        ' '
      'and f.id not in (select id from f_folders_tree(:startid))'
      'order by f.folder_name')
    SelectCommand.Parameters = <
      item
        Name = 'folder_section'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'startid'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update counteragents set '
      'INN= :INN, '
      'code = :code, '
      'name = :name, '
      'location = :location, '
      'chief = :chef, '
      'manager = :manager'
      'where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'INN'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 14
        Value = Null
      end
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'location'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'chef'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'manager'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into folders (folder_name, folder_section)'
      'values ( :folder_name, :folder_section )')
    InsertCommand.Parameters = <
      item
        Name = 'folder_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'folder_section'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from counteragents where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.Parameters = <>
    Left = 160
    Top = 144
  end
  object meFolders: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvFolders
    TreeList.Active = True
    TreeList.KeyFieldName = 'id'
    TreeList.RefParentFieldName = 'parent_id'
    Left = 208
    Top = 144
  end
  object dsFolders: TDataSource
    DataSet = meFolders
    Left = 264
    Top = 144
  end
end
