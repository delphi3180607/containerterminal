﻿inherited FormLoadPlan: TFormLoadPlan
  ActiveControl = dgData
  Caption = #1047#1072#1076#1072#1085#1080#1103' '#1085#1072' '#1087#1086#1075#1088#1091#1079#1082#1091
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    inherited btnOk: TButton
      Caption = #1042#1099#1073#1088#1072#1090#1100
    end
    object btClear: TButton
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 175
      Height = 35
      Align = alLeft
      Caption = #1057#1073#1088#1086#1089#1080#1090#1100
      ModalResult = 6
      TabOrder = 2
    end
  end
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'plan_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1042#1088#1077#1084#1103
          Width = 164
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'sheet_serve'
          Footers = <>
          Title.Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100
          Width = 125
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1087#1086#1077#1079#1076#1072
          Width = 128
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dispatch_code'
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1086#1090#1087#1088#1072#1074#1082#1080
          Width = 133
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Width = 249
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select p.*,'
      
        '(select code from dispatchkinds k where k.id = p.dispatch_kind_i' +
        'd) as dispatch_code,'
      
        'convert(varchar, p.plan_date, 104)+'#39', '#39'+isnull(p.train_num,'#39#39')+'#39 +
        ', '#39'+isnull((select code from dispatchkinds k where k.id = p.disp' +
        'atch_kind_id),'#39#39')+'#39','#39'+isnull(p.note,'#39#39') load_plan_text'
      'from loadplan p'
      'where system_section = :system_section'
      'order by plan_date desc')
    SelectCommand.Parameters = <
      item
        Name = 'system_section'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update loadplan'
      'set'
      '  plan_date = :plan_date,'
      '  note = :note,'
      ' train_num = :train_num, '
      ' dispatch_kind_id = :dispatch_kind_id,'
      ' sheet_serve = :sheet_serve'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'plan_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 850
        Value = Null
      end
      item
        Name = 'train_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'dispatch_kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'sheet_serve'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into loadplan'
      
        '  (plan_date, note, train_num,  system_section, dispatch_kind_id' +
        ', sheet_serve)'
      'values'
      
        '  (:plan_date, :note, :train_num, :system_section, :dispatch_kin' +
        'd_id, :sheet_serve)')
    InsertCommand.Parameters = <
      item
        Name = 'plan_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 850
        Value = Null
      end
      item
        Name = 'train_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'system_section'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'dispatch_kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'sheet_serve'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from loadplan where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select p.*,'
      
        '(select code from dispatchkinds k where k.id = p.dispatch_kind_i' +
        'd) as dispatch_code,'
      
        'convert(varchar, p.plan_date, 104)+'#39', '#39'+isnull(p.train_num,'#39#39')+'#39 +
        ', '#39'+isnull((select code from dispatchkinds k where k.id = p.disp' +
        'atch_kind_id),'#39#39')+'#39','#39'+isnull(p.note,'#39#39') load_plan_text'
      'from loadplan p'
      'where p.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
end
