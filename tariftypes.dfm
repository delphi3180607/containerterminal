﻿inherited FormTarifTypes: TFormTarifTypes
  Caption = #1058#1080#1087#1099' '#1090#1072#1088#1080#1092#1086#1074
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076
          Width = 84
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 338
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'tarif_value'
          Footers = <>
          Title.Caption = #1058#1072#1088#1080#1092
          Width = 126
        end>
    end
  end
  inherited pmGrid: TPopupMenu
    Left = 248
    Top = 184
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from tariftypes order by code')
    UpdateCommand.CommandText.Strings = (
      'update tariftypes'
      'set'
      '  code = :code,'
      '  name = :name,'
      '  tarif_value = :tarif_value'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'tarif_value'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into tariftypes'
      '  (code, name, tarif_value)'
      'values'
      '  (:code, :name, :tarif_value)')
    InsertCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'tarif_value'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from tariftypes where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from tariftypes order by code'
      '  id = :OLD_id')
    GetrecCommand.Parameters = <
      item
        Name = 'OLD_id'
        Size = -1
        Value = Null
      end>
  end
  inherited qrAux: TADOQuery
    Left = 304
  end
  inherited drvForms: TADODataDriverEh
    Left = 420
    Top = 186
  end
  inherited meForms: TMemTableEh
    Left = 468
    Top = 186
  end
  inherited dsForms: TDataSource
    Left = 516
    Top = 186
  end
  inherited exReport: TEXLReport
    Left = 358
    Top = 184
  end
end
