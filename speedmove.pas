﻿unit speedmove;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, MemTableDataEh, MemTableEh,
  Vcl.Buttons;

type
  TFormSpeedMove = class(TFormEdit)
    edCarriageNum: TDBEditEh;
    lbMark: TLabel;
    meCach: TMemTableEh;
    plButtons: TPanel;
    btPlace: TButton;
    lbEnter: TLabel;
    lbcars: TLabel;
    plTop: TPanel;
    lbMessage: TLabel;
    sbClear: TSpeedButton;
    sbDelete: TSpeedButton;
    meCars: TListBox;
    procedure edCarriageNumChange(Sender: TObject);
    procedure btPlaceClick(Sender: TObject);
    procedure edCarriageNumKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure sbClearClick(Sender: TObject);
    procedure meCarsExit(Sender: TObject);
    procedure meCarsEnter(Sender: TObject);
    procedure sbDeleteClick(Sender: TObject);
  private
    parentDataSet: TDataSet;
    procedure OnButtonsClick(Sender: TObject);
    procedure OnButtonCheck(Sender: TObject);
  public
    isfound: boolean;
    car: string;
    path: string;
    slJointIds: TStringList;
    procedure Init(parentData: TDataSet);
    procedure AllButtonsUp;
  end;

var
  FormSpeedMove: TFormSpeedMove;

implementation

{$R *.dfm}

uses paths, loadschess;

procedure TFormSpeedMove.btPlaceClick(Sender: TObject);
var i: integer;
begin

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update docloadjoint set path_id = (select id from paths where code = :code )');
  qrAux.SQL.Add('where id = :id');


  for i := 0 to slJointIds.Count-1 do
  begin

      qrAux.Parameters.ParamByName('id').Value := StrToInt(slJointIds[i]);
      qrAux.Parameters.ParamByName('code').Value := '-1';
      qrAux.ExecSQL;

  end;


  for i := 0 to slJointIds.Count-1 do
  begin

      qrAux.Parameters.ParamByName('id').Value := StrToInt(slJointIds[i]);
      qrAux.Parameters.ParamByName('code').Value := path;
      qrAux.ExecSQL;

  end;


  FormLoadsChess.meData.Locate('pnum', meCars.Items[0], []);

  edCarriageNum.Text := '';

  parentDataSet.Close;
  parentDataSet.Open;
  edCarriageNum.SetFocus;

  lbMark.Caption := '';
  lbMark.Font.Color := clGray;
  meCars.Items.Clear;

  AllButtonsUp;

end;

procedure TFormSpeedMove.edCarriageNumChange(Sender: TObject);
begin
  inherited;
  isfound := false;
  lbEnter.Hide;
  lbMark.Caption := '';
  //plButtons.Enabled := false;
  meCach.Filter := 'pnum like''%'+edCarriageNum.Text.Trim+'%''';
  meCach.Filtered := true;
  car := '';
  if meCach.RecordCount = 1 then
  begin
    lbMark.Font.Color := clGreen;
    lbMark.Caption := 'Найден: '+meCach.FieldByName('pnum').AsString;
    car := meCach.FieldByName('pnum').AsString;
    isfound := true;
    lbEnter.Show;
  end;
end;

procedure TFormSpeedMove.edCarriageNumKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    if car = '' then exit;
    meCars.Items.Add(car);
    slJointIds.Add(meCach.FieldByName('id').AsString);
    edCarriageNum.Text := '';
    lbEnter.Hide;
  end;
end;

procedure TFormSpeedMove.FormCreate(Sender: TObject);
begin
  inherited;
  car := '';
  path := '';
  slJointIds := TStringList.Create;
end;

procedure TFormSpeedMove.Init(parentData: TDataSet);
var i: integer; Item: TControl; b: TSpeedButton; bk: TBookMark;
begin

 bk := parentData.Bookmark;
 parentData.DisableControls;
 btPlace.Caption := '';
 lbEnter.Hide;
 meCars.Items.Clear;
 slJointIds.Clear;
 car := '';

 for i := (plButtons.ControlCount - 1) downto 0 do
  begin
    Item := plButtons.Controls[i];

    if Item is TSpeedButton then
     Item.Free;

  end;
  parentDataSet := parentData;
  FormPaths.Init;
  FormPaths.meData.SortOrder := 'code';
  FormPaths.meData.First;
  while not FormPaths.meData.Eof do
  begin
    b:= TSpeedButton.Create(plButtons);
    b.Font.Size := 16;
    b.Caption := FormPaths.meData.FieldByName('code').AsString;
    b.Height := 45;
    b.Align := alTop;
    b.AlignWithMargins := true;
    b.Margins.SetBounds(1, 1, 1, 1);
    b.Parent := plButtons;
    b.Top := self.Height + 10;
    b.OnClick := OnButtonCheck;
    b.AllowAllUp := true;
    b.GroupIndex := 1;
    b.Show;
    b.Enabled := true;
    b.Flat := false;
    FormPaths.meData.Next;
  end;
  meCach.EmptyTable;
  meCach.LoadFromDataSet(parentData, 0, lmCopy, false);
  meCach.Open;
  plButtons.Enabled := true;

  parentData.Bookmark := bk;
  parentData.EnableControls;

  AllButtonsUp;

end;

procedure TFormSpeedMove.meCarsEnter(Sender: TObject);
begin
  sbDelete.Enabled := true;
end;

procedure TFormSpeedMove.meCarsExit(Sender: TObject);
begin
  sbDelete.Enabled := false;
end;

procedure TFormSpeedMove.OnButtonsClick(Sender: TObject);
var mess: string;
begin

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update docloadjoint set path_id = (select id from paths where code = :code )');
  qrAux.SQL.Add('where id = '+meCach.FieldByName('id').AsString);
  qrAux.Parameters.ParamByName('code').Value := path;
  qrAux.ExecSQL;

  FormLoadsChess.meData.Locate('pnum', meCach.FieldByName('pnum').AsString, []);

  mess := meCach.FieldByName('pnum').AsString+' - размещен.';

  edCarriageNum.Text := '';
  parentDataSet.Close;
  parentDataSet.Open;
  edCarriageNum.SetFocus;

  lbMark.Caption := mess;
  lbMark.Font.Color := clGray;

end;


procedure TFormSpeedMove.sbClearClick(Sender: TObject);
begin
  meCars.Items.Clear;
end;

procedure TFormSpeedMove.sbDeleteClick(Sender: TObject);
begin
  meCars.Items.Delete(meCars.ItemIndex);
end;

procedure TFormSpeedMove.OnButtonCheck(Sender: TObject);
begin
  path := TButton(Sender).Caption;
  btPlace.Caption := 'На '+path+' >>';
  lbMessage.Hide;
  plTop.Color := clBtnFace;

  btPlace.Enabled := true;
  edCarriageNum.Enabled := true;

end;


procedure TFormSpeedMove.AllButtonsUp;
var i: integer; Item: TSpeedButton; c: TControl;
begin

  lbMessage.Show;
  plTop.Color := $00DFFFFF;

 for i := (plButtons.ControlCount - 1) downto 0 do
  begin
    c:= plButtons.Controls[i];
    if c is TSpeedButton then
    begin
      Item := TSpeedButton(c);
      Item.Down := false;
    end;
  end;

  slJointIds.Clear;

  btPlace.Enabled := false;
  edCarriageNum.Enabled := false;

end;


end.
