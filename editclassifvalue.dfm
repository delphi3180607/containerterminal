﻿inherited FormEditClassifValue: TFormEditClassifValue
  Caption = #1047#1085#1072#1095#1077#1085#1080#1077' '#1082#1083#1072#1089#1089#1080#1092#1080#1082#1072#1090#1086#1088#1072
  ClientHeight = 202
  ClientWidth = 459
  ExplicitWidth = 465
  ExplicitHeight = 230
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 161
    Width = 459
    inherited btnCancel: TButton
      Left = 343
    end
    inherited btnOk: TButton
      Left = 224
    end
  end
  object edStringValue: TDBEditEh [1]
    Left = 24
    Top = 32
    Width = 409
    Height = 24
    ControlLabel.Width = 64
    ControlLabel.Height = 16
    ControlLabel.Caption = #1047#1085#1072#1095#1077#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'string_value'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edExcludeIDS: TDBEditEh [2]
    Left = 24
    Top = 96
    Width = 409
    Height = 24
    ControlLabel.Width = 126
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1077#1089#1086#1074#1084#1077#1089#1090#1080#1084#1099#1077' ID'
    ControlLabel.Visible = True
    DataField = 'exclude_values_ids'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
end
