﻿unit operationedit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, DBCtrlsEh, Vcl.Buttons, DBSQLLookUp, Data.DB, Data.Win.ADODB;

type
  TFormEditOperation = class(TFormEdit)
    Label1: TLabel;
    dtOperationDate: TDBDateTimeEditEh;
    Label2: TLabel;
    Label5: TLabel;
    edDocBase: TDBEditEh;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Label3: TLabel;
    edDocInvoice: TDBEditEh;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    edNote: TDBMemoEh;
    leServiceKind: TDBSQLLookUp;
    Label6: TLabel;
    ssServiceKind: TADOLookUpSqlSet;
    lePerson: TDBSQLLookUp;
    Label4: TLabel;
    ssPerson: TADOLookUpSqlSet;
    cbEventKind: TDBComboBoxEh;
    Label7: TLabel;
    Label8: TLabel;
    dtEvent: TDBDateTimeEditEh;
    Label9: TLabel;
    edOperationKind: TDBEditEh;
    Label10: TLabel;
    edStateKind: TDBEditEh;
    Bevel1: TBevel;
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditOperation: TFormEditOperation;

implementation

{$R *.dfm}

uses stateshistory;

procedure TFormEditOperation.FormActivate(Sender: TObject);
begin
  inherited;
  FormStatesHistory.meData.FieldByName('operation_datetime').Required := true;
end;

end.
