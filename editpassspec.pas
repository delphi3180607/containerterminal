﻿unit EditPassSpec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBCtrlsEh, Vcl.Buttons, DBSQLLookUp, Vcl.Mask,
  MemTableDataEh, MemTableEh, DataDriverEh, ADODataDriverEh;

type
  TFormEditPassSpec = class(TFormEdit)
    laContainerOwner: TDBSQLLookUp;
    ssContainerKind: TADOLookUpSqlSet;
    leContainerKind: TDBSQLLookUp;
    edContainerNum: TDBEditEh;
    sbContainer: TSpeedButton;
    edSealNumber: TDBEditEh;
    cbIsEmpty: TDBCheckBoxEh;
    drvContainers: TADODataDriverEh;
    meContainers: TMemTableEh;
    procedure edContainerNumExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditPassSpec: TFormEditPassSpec;

implementation

{$R *.dfm}

procedure TFormEditPassSpec.edContainerNumExit(Sender: TObject);
begin

  if edContainerNum.Text<>'' then
  begin

    meContainers.Close;
    drvContainers.SelectCommand.Parameters.ParamByName('cnum').Value := edContainerNum.Text;
    meContainers.Open;

  end;

  if (self.leContainerKind.KeyValue = -1) or (self.leContainerKind.KeyValue = null) then
    self.leContainerKind.KeyValue := meContainers.FieldByName('kind_id').AsInteger;

  if (self.laContainerOwner.KeyValue = -1) or (self.laContainerOwner.KeyValue = null) then
    self.laContainerOwner.KeyValue := meContainers.FieldByName('owner_id').AsInteger;

end;

end.
