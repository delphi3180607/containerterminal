﻿inherited FormEditFactArrivalGo: TFormEditFactArrivalGo
  Caption = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1086#1077' '#1087#1088#1080#1073#1099#1090#1080#1077' '#1080' '#1091#1073#1099#1090#1080#1077
  ClientHeight = 470
  ClientWidth = 514
  ExplicitWidth = 520
  ExplicitHeight = 498
  PixelsPerInch = 96
  TextHeight = 16
  object Label8: TLabel [0]
    Left = 8
    Top = 5
    Width = 70
    Height = 13
    Caption = #1055#1077#1088#1077#1074#1086#1079#1095#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 102
    Width = 56
    Height = 13
    Caption = #1042#1086#1076#1080#1090#1077#1083#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label27: TLabel [2]
    Left = 219
    Top = 52
    Width = 108
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1072#1074#1090#1086#1084#1086#1073#1080#1083#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label18: TLabel [3]
    Left = 7
    Top = 53
    Width = 109
    Height = 13
    Caption = #1052#1072#1088#1082#1072' '#1072#1074#1090#1086#1084#1086#1073#1080#1083#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 429
    Width = 514
    TabOrder = 5
    ExplicitTop = 469
    ExplicitWidth = 514
    inherited btnCancel: TButton
      Left = 398
      ExplicitLeft = 398
    end
    inherited btnOk: TButton
      Left = 279
      ExplicitLeft = 279
    end
  end
  object cbExtraPlace: TDBCheckBoxEh [5]
    Left = 8
    Top = 406
    Width = 155
    Height = 17
    Caption = #1047#1072#1077#1079#1076' '#1085#1072' '#1076#1086#1087'.'#1084#1077#1089#1090#1086
    DataField = 'extra_place_sign'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 0
  end
  object luDealer: TDBLookupComboboxEh [6]
    Left = 8
    Top = 24
    Width = 434
    Height = 21
    DynProps = <>
    DataField = 'dealer_id'
    DataSource = dsLocal
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    KeyField = 'id'
    ListField = 'code'
    ListSource = dm.dsCounteragents
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object laDriver: TDBSQLLookUp [7]
    Left = 8
    Top = 121
    Width = 434
    Height = 22
    AutoSize = False
    Color = 14810109
    DataField = 'driver_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end
      item
        Style = ebsPlusEh
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 2
    Visible = True
    SqlSet = ssPersons
    RowCount = 20
  end
  object edCarNumber: TDBEditEh [8]
    Left = 219
    Top = 71
    Width = 174
    Height = 21
    Color = 14810109
    DataField = 'car_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 3
    Visible = True
  end
  object edCarData: TDBEditEh [9]
    Left = 7
    Top = 72
    Width = 201
    Height = 21
    DataField = 'car_data'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = True
  end
  object PageControl1: TPageControl [10]
    Left = 8
    Top = 174
    Width = 497
    Height = 227
    ActivePage = TabSheet1
    TabOrder = 6
    object TabSheet1: TTabSheet
      Caption = #1040#1076#1088#1077#1089
      ExplicitHeight = 218
      object Label6: TLabel
        Left = 16
        Top = 15
        Width = 72
        Height = 13
        Caption = #1055#1088#1080#1077#1084' '#1075#1088#1091#1079#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 16
        Top = 64
        Width = 74
        Height = 13
        Caption = #1057#1076#1072#1095#1072' '#1075#1088#1091#1079#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label23: TLabel
        Left = 17
        Top = 122
        Width = 98
        Height = 13
        Caption = #1055#1088#1080#1073#1099#1090#1080#1077' - '#1092#1072#1082#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        StyleElements = [seClient, seBorder]
      end
      object Label22: TLabel
        Left = 198
        Top = 122
        Width = 84
        Height = 13
        Caption = #1059#1073#1099#1090#1080#1077' - '#1092#1072#1082#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        StyleElements = [seClient, seBorder]
      end
      object dtArrivalFact: TDBDateTimeEditEh
        Left = 17
        Top = 141
        Width = 162
        Height = 21
        DataField = 'arrival_datetime_fact'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Kind = dtkDateTimeEh
        ParentFont = False
        TabOrder = 0
        Visible = True
      end
      object dtGoFact: TDBDateTimeEditEh
        Left = 198
        Top = 141
        Width = 166
        Height = 21
        DataField = 'go_datetime_fact'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Kind = dtkDateTimeEh
        ParentFont = False
        TabOrder = 1
        Visible = True
      end
      object edReceptAdress: TDBEditEh
        Left = 16
        Top = 34
        Width = 434
        Height = 21
        DataField = 'recept_address'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Visible = True
      end
      object edDlvAddress: TDBEditEh
        Left = 16
        Top = 82
        Width = 434
        Height = 21
        DataField = 'delivery_address'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        Visible = True
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1044#1086#1087'.'#1072#1076#1088#1077#1089
      ImageIndex = 1
      ExplicitHeight = 218
      object Label5: TLabel
        Left = 17
        Top = 18
        Width = 36
        Height = 13
        Caption = #1040#1076#1088#1077#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 17
        Top = 77
        Width = 98
        Height = 13
        Caption = #1055#1088#1080#1073#1099#1090#1080#1077' - '#1092#1072#1082#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        StyleElements = [seClient, seBorder]
      end
      object Label3: TLabel
        Left = 198
        Top = 77
        Width = 84
        Height = 13
        Caption = #1059#1073#1099#1090#1080#1077' - '#1092#1072#1082#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        StyleElements = [seClient, seBorder]
      end
      object edExtraAddress: TDBEditEh
        Left = 17
        Top = 38
        Width = 428
        Height = 21
        DataField = 'extra_address'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        Visible = True
      end
      object dtExtraArrivalFact: TDBDateTimeEditEh
        Left = 17
        Top = 93
        Width = 161
        Height = 21
        DataField = 'extra_go_datetime_fact'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Kind = dtkDateTimeEh
        ParentFont = False
        TabOrder = 1
        Visible = True
      end
      object dtExtraGoFact: TDBDateTimeEditEh
        Left = 198
        Top = 93
        Width = 166
        Height = 21
        DataField = 'extra_arrival_datetime_fact'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Kind = dtkDateTimeEh
        ParentFont = False
        TabOrder = 2
        Visible = True
      end
    end
  end
  inherited dsLocal: TDataSource
    Left = 112
    Top = 329
  end
  inherited qrAux: TADOQuery
    Left = 168
    Top = 329
  end
  object ssPersons: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select * from persons where isnull(isblocked,0) = 0 and person_k' +
        'ind = 1'
      'and person_name like '#39'%@pattern%'#39
      'order by person_name')
    DownSql.Strings = (
      
        'select * from persons where isnull(isblocked,0) = 0  and person_' +
        'kind = 1'
      'order by person_name')
    InitSql.Strings = (
      'select * from persons where id = @id')
    KeyName = 'id'
    DisplayName = 'person_name'
    Connection = dm.connMain
    Left = 171
    Top = 113
  end
end
