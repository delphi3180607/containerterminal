﻿unit filterloads;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBSQLLookUp, DBCtrlsEh, Vcl.Mask;

type
  TFormFilterLoads = class(TFormEdit)
    Label1: TLabel;
    dtPlanDate: TDBDateTimeEditEh;
    luLoadPlan: TDBSQLLookUp;
    ssLoadPlan: TADOLookUpSqlSet;
    Label2: TLabel;
    Button1: TButton;
    procedure luLoadPlanKeyValueChange(Sender: TObject);
    procedure dtPlanDateChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormFilterLoads: TFormFilterLoads;

implementation

{$R *.dfm}

procedure TFormFilterLoads.dtPlanDateChange(Sender: TObject);
begin
  if flag then exit;
  flag := true;
  if dtPlanDate.Value <> null then luLoadPlan.KeyValue := null;
  flag := false;
end;

procedure TFormFilterLoads.luLoadPlanKeyValueChange(Sender: TObject);
begin
  flag := true;
  if luLoadPlan.KeyValue <> null then
  begin
    dtPlanDate.Value := null;
    //dtPlanDate.Enabled := false;
  end else
  begin
    //dtPlanDate.Enabled := true;
  end;
  flag := false;
end;

end.
