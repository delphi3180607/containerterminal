﻿inherited FormTasksIncome: TFormTasksIncome
  Caption = #1047#1072#1076#1072#1095#1080' - '#1087#1088#1080#1077#1084' '#1075#1088#1091#1079#1086#1074
  PixelsPerInch = 96
  TextHeight = 13
  inherited Splitter3: TSplitter
    Top = 309
    Height = 8
    ExplicitTop = 308
    ExplicitWidth = 933
    ExplicitHeight = 8
  end
  inherited plAll: TPanel
    Height = 309
    inherited dgData: TDBGridEh
      Height = 280
      OnDblClick = nil
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select '
      
        ' (select code from counteragents c1 where c1.id =   t.forwarder_' +
        'id) as  forwarder,'
      
        ' (select code from counteragents c2 where c2.id =   t.consignee_' +
        'id) as  consignee,'
      
        ' (select code from counteragents c3 where c3.id =   t.payer_id) ' +
        'as  payer,'
      
        ' (select code from deliverytypes t4 where t4.id =   t.dlv_type_i' +
        'd) as  deliverytype,'
      ' o.cnum,'
      ' o.object_type,'
      ' t.dlv_type_id,'
      ' t.start_date,'
      ' t.plan_date,'
      ' t.fact_date,'
      ' t.forwarder_id,'
      ' t.consignee_id,'
      ' t.payer_id,'
      ' t.object_id'
      'from tasks t '
      'left outer join v_objects o on (t.object_id = o.id)'
      'order by t.start_date')
    GetrecCommand.CommandText.Strings = (
      'select '
      
        ' (select code from counteragents c1 where c1.id =   t.forwarder_' +
        'id) as  forwarder,'
      
        ' (select code from counteragents c2 where c2.id =   t.consignee_' +
        'id) as  consignee,'
      
        ' (select code from counteragents c3 where c3.id =   t.payer_id) ' +
        'as  payer,'
      
        ' (select code from deliverytypes t4 where t4.id =   t.dlv_type_i' +
        'd) as  deliverytype,'
      ' o.cnum,'
      ' o.object_type,'
      ' t.root_doc_id,'
      ' t.dlv_type_id,'
      ' t.start_date,'
      ' t.plan_date,'
      ' t.fact_date,'
      ' t.forwarder_id,'
      ' t.consignee_id,'
      ' t.payer_id,'
      ' t.object_id'
      'from tasks t '
      'left outer join v_objects o on (t.object_id = o.id)'
      'where t.id = :current_id')
  end
end
