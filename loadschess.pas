﻿unit loadschess;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls, Functions, Vcl.Mask, DBCtrlsEh, DBSQLLookUp;

type
  TFormLoadsChess = class(TFormGrid)
    sbMassAdd: TPngSpeedButton;
    Bevel1: TBevel;
    Bevel3: TBevel;
    nuLen: TDBNumberEditEh;
    nuLen1: TDBNumberEditEh;
    N8: TMenuItem;
    spWaste: TPngSpeedButton;
    sbRestore: TPngSpeedButton;
    dgOrder: TDBGridEh;
    sbFixedSelect: TPngSpeedButton;
    meOrder: TMemTableEh;
    dsOrder: TDataSource;
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    drvOrder: TADODataDriverEh;
    Bevel4: TBevel;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    sbClearFilter: TPngSpeedButton;
    plStatus: TPanel;
    sbSpeedMove: TPngSpeedButton;
    Bevel6: TBevel;
    btMark: TPngSpeedButton;
    Bevel8: TBevel;
    N21: TMenuItem;
    N22: TMenuItem;
    drvCarriages: TADODataDriverEh;
    meCarriages: TMemTableEh;
    sbWagon: TPngSpeedButton;
    nuAmount: TDBNumberEditEh;
    cbEmptyOnly: TDBCheckBoxEh;
    N20: TMenuItem;
    N19: TMenuItem;
    N23: TMenuItem;
    N24: TMenuItem;
    dgOrderInfo: TDBGridEh;
    cdEnlightEqual: TDBCheckBoxEh;
    drvOrderInfo: TADODataDriverEh;
    meOrderInfo: TMemTableEh;
    dsOrderInfo: TDataSource;
    Bevel7: TBevel;
    Bevel9: TBevel;
    procedure sbMassAddClick(Sender: TObject);
    procedure meDataAfterEdit(DataSet: TDataSet);
    procedure sbAddClick(Sender: TObject);
    procedure meDataAfterInsert(DataSet: TDataSet);
    procedure meDataAfterDelete(DataSet: TDataSet);
    procedure sbEditClick(Sender: TObject);
    procedure meDataAfterOpen(DataSet: TDataSet);
    procedure sbPathClick(Sender: TObject);
    procedure spWasteClick(Sender: TObject);
    procedure sbDeleteClick(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure sbRestoreClick(Sender: TObject);
    procedure sbFixedSelectClick(Sender: TObject);
    procedure dgDataMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dgDataColumns2EditButtonClick(Sender: TObject;
      var Handled: Boolean);
    procedure N9Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure btFilterClick(Sender: TObject);
    procedure sbClearFilterClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure sbSpeedMoveClick(Sender: TObject);
    procedure meSpecAfterPost(DataSet: TDataSet);
    procedure dgDataSelectionChanged(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure btMarkClick(Sender: TObject);
    procedure cbEmptyOnlyClick(Sender: TObject);
    procedure N19Click(Sender: TObject);
    procedure N24Click(Sender: TObject);
    procedure dgDataCellClick(Column: TColumnEh);
    procedure cdEnlightEqualClick(Sender: TObject);
    procedure dgDataApplyFilter(Sender: TObject);
    procedure dgOrderDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure dgOrderInfoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure dgDataColumns5CellButtons0Click(Sender: TObject;
      var Handled: Boolean);
  private
    fGridCarriages: TForm;
    fGridByNumber: TForm;
    current_orderspecid: integer;
  protected
    procedure AssignSelField(colname: string); override;
  public
    folder_id: integer;
    fixedOrderId: integer;
    foots: string;
    stations: string;
    allowminus: boolean;
    procedure Init; override;
    procedure SetFilter(conditions: string);
    procedure RecalcLen;
  end;

var
  FormLoadsChess: TFormLoadsChess;

implementation

{$R *.dfm}

uses EditPlaceContainers, selectobjects, containers, carriages, CarriagesOnGo,
  SelectPath, dmu, ContainersInOrders, ContainersOnGo, filterlite, speedmove,
  loadplan, date, FilterChessExtra, SetMark, editcarriage, setcheck;

procedure TFormLoadsChess.btFilterClick(Sender: TObject);
var gu: TGuid;
begin
  FormFilterChessExtra.Init;
  FormFilterChessExtra.ShowModal;
  if FormFilterChessExtra.ModalResult in [mrOk, mrYes] then
  begin

    CreateGuid(gu);
    g := GuidToString(gu);

    if FormFilterChessExtra.stationsids.DelimitedText = '' then
      self.stations := ''
    else
     self.stations := ','+FormFilterChessExtra.stationsids.DelimitedText+',';

    if FormFilterChessExtra.carkinds = '' then
      self.foots := ''
    else
      self.foots := ','+FormFilterChessExtra.carkinds+',';

    SetFilter(FormFilterChessExtra.conditions);

  end;
end;

procedure TFormLoadsChess.btMarkClick(Sender: TObject);
begin
  FillStringColumn('mark');
end;

procedure TFormLoadsChess.AssignSelField(colname: string);
begin

  selfield := '';
  if colname = 'train_num_source' then selfield := 'train_num_source';
  if colname = 'train_num' then selfield := 'train_num';
  if colname = 'mark' then selfield := 'mark';

end;


procedure TFormLoadsChess.cbEmptyOnlyClick(Sender: TObject);
begin
  meData.Close;
  meData.Open;
end;

procedure TFormLoadsChess.cdEnlightEqualClick(Sender: TObject);
begin
  dgData.Refresh;
end;

procedure TFormLoadsChess.dgDataApplyFilter(Sender: TObject);
begin
  inherited;
  self.RecalcLen;
end;

procedure TFormLoadsChess.dgDataCellClick(Column: TColumnEh);
var contnum: string;
begin

  if pos('container',Column.FieldName)>0 then
  begin
    contnum := StringReplace(Column.FieldName,'container','', [rfReplaceAll]);
    meOrderInfo.Close;
    drvOrderInfo.SelectCommand.Parameters.ParamByName('jointid').Value := meData.FieldByName('id').AsInteger;
    drvOrderInfo.SelectCommand.Parameters.ParamByName('ordernum').Value := contnum;
    meOrderInfo.Open;
    current_orderspecid := meOrderInfo.FieldByName('id').AsInteger;

    if cdEnlightEqual.Checked then
    begin
      dgData.Refresh;
    end;

  end else
  begin
    meOrderInfo.Close;
    current_orderspecid := 0;

    if cdEnlightEqual.Checked then
    begin
      dgData.Refresh;
    end;

  end;

end;

procedure TFormLoadsChess.dgDataColumns2EditButtonClick(Sender: TObject; var Handled: Boolean);
var ordernum: integer;
begin

  if dgData.SelectedField.FieldName = 'container1' then ordernum := 1;
  if dgData.SelectedField.FieldName = 'container2' then ordernum := 2;
  if dgData.SelectedField.FieldName = 'container3' then ordernum := 3;
  if dgData.SelectedField.FieldName = 'container4' then ordernum := 4;

  meSpec.Close;
  drvSpec.SelectCommand.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
  drvSpec.SelectCommand.Parameters.ParamByName('order_num').Value := ordernum;
  meSpec.Open;

  if meSpec.RecordCount > 0 then
  begin

    if not fQYN('Удалить контейнер?') then
    begin
      Handled := true;
      exit;
    end;

    drvSpec.DeleteCommand.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
    drvSpec.DeleteCommand.Parameters.ParamByName('order_num').Value := ordernum;
    meSpec.Delete;

    {qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('update docload set joint_id = joint_id where joint_id = :joint_id');
    qrAux.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
    qrAux.ExecSQL;}

    drvData.GetRecCommand.Parameters.ParamByName('current_id').Value := meData.FieldByName('id').AsInteger;
    meData.RefreshRecord;
    //dgData.Refresh;

    meOrder.Close;
    meOrder.DataDriver := drvOrder;
    drvOrder.SelectCommand.Parameters.ParamByName('current_id').Value := fixedOrderId;
    meOrder.Open;

    if Assigned(fGridByNumber) then
    begin
      TFormGrid(fGridByNumber).meData.Refresh;
    end;

  end;

  Handled := true;

end;

procedure TFormLoadsChess.dgDataColumns5CellButtons0Click(Sender: TObject; var Handled: Boolean);
begin
  N21Click(self);
  Handled := true;
end;

procedure TFormLoadsChess.dgDataDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
var fn: string; textRect: TRect; contnum: string; orderspecid: integer;
begin

  if meData.FieldByName('isdeleted').AsBoolean then
  begin
    dgData.Canvas.Font.Color := clRed;
  end;

  if Column.FieldName = 'pnum' then
  begin

    if meData.FieldByName('isdefective').AsInteger>0 then
    dgData.Canvas.Brush.Color := $00E3D9FD;

    if meData.FieldByName('need_inspection').AsBoolean then
    dgData.Canvas.Brush.Color := clYellow;

  end;


  if pos('container',Column.FieldName)>0 then
  begin

    if cdEnlightEqual.Checked then
    begin

      contnum := StringReplace(Column.FieldName,'container','', [rfReplaceAll]);
      orderspecid := meData.FieldByName('orderspecid'+contnum).AsInteger;

      if (orderspecid = current_orderspecid) and (current_orderspecid<>0) then
      begin
        dgData.Canvas.Brush.Color := clGreen;
        dgData.Canvas.Font.Color := clWhite;
      end;

      dgData.Canvas.RoundRect(Rect.Left+4,Rect.Top+2,Rect.Right-2,Rect.Bottom-2, 4, 4);
      dgData.Canvas.Brush.Style := bsClear;

      textRect.Left := Rect.Left+4;
      textRect.Top := Rect.Top;
      textRect.Right := Rect.Right-10;
      textRect.Bottom := Rect.Bottom;
      dgData.Canvas.TextRect(textRect, Rect.Left+9, Rect.Top+2 , meData.FieldByName(Column.FieldName).AsString);

      dgData.Canvas.Pen.Style := psSolid;
      dgData.Canvas.Pen.Width := 1;

    end else
    begin

      fn := StringReplace(Column.FieldName,'container','cont', [rfReplaceAll])+'marks';

      if pos('*',meData.FieldByName(fn).AsString)>0 then
      begin
        dgData.Canvas.Pen.Style := psClear;
        dgData.Canvas.Brush.Color := $00EEE0C8;
      end;

      if pos('=',meData.FieldByName(fn).AsString)>0 then
      begin
        dgData.Canvas.Pen.Style := psClear;
        dgData.Canvas.Brush.Color := $00CADFEC;
      end;

      dgData.Canvas.Pen.Style := psSolid;

      if gdSelected in State then
        if (Column.Index = dgData.Col-1)then
          dgData.Canvas.Pen.Style := psClear;

      dgData.Canvas.RoundRect(Rect.Left+4,Rect.Top+2,Rect.Right-2,Rect.Bottom-2, 3, 3);
      dgData.Canvas.Brush.Style := bsClear;

      textRect.Left := Rect.Left+4;
      textRect.Top := Rect.Top;
      textRect.Right := Rect.Right-10;
      textRect.Bottom := Rect.Bottom;
      dgData.Canvas.TextRect(textRect, Rect.Left+9, Rect.Top+2 , meData.FieldByName(Column.FieldName).AsString);

      dgData.Canvas.Pen.Style := psSolid;
      dgData.Canvas.Pen.Width := 1;

    end;

  end else
  begin
    dgData.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    dgData.Canvas.Pen.Style := psSolid;
    dgData.Canvas.Pen.Width := 1;
    dgData.Canvas.Pen.Color := $00CDCFC7;
    dgData.Canvas.MoveTo(Rect.Right-1, Rect.Top);
    dgData.Canvas.LineTo(Rect.Right-1, Rect.Bottom);
  end;

  if pos('container',Column.FieldName)>0 then
  begin

      fn := StringReplace(Column.FieldName,'container','cont', [rfReplaceAll])+'marks';

      if pos('~',meData.FieldByName(fn).AsString)>0 then
      begin
        dgData.Canvas.Pen.Color := clRed;
        dgData.Canvas.Pen.Width := 1;
        dgData.Canvas.Brush.Color := clYellow;
        dgData.Canvas.Rectangle(Rect.Left+3,Rect.Top+4,Rect.Left+8,Rect.Top+15);
        dgData.Canvas.Brush.Color := clWindow;
      end;

      if pos('!',meData.FieldByName(fn).AsString)>0 then
      begin
        dgData.Canvas.Pen.Color := clCream;
        dgData.Canvas.Pen.Width := 1;
        dgData.Canvas.Brush.Color := $000CA570;
        dgData.Canvas.Rectangle(Rect.Left+1,Rect.Top+4,Rect.Left+7,Rect.Top+15);
        dgData.Canvas.Brush.Color := clWindow;
      end;

  end;

  if Column.FieldName = 'pnum' then
  begin

    if meData.FieldByName('urgent_repair_sign').AsInteger>0 then
    begin

        dgData.Canvas.Pen.Color := clRed;
        dgData.Canvas.Pen.Width := 1;
        dgData.Canvas.Brush.Color := clRed;
        dgData.Canvas.Rectangle(Rect.Right-8,Rect.Top+2,Rect.Right-2,Rect.Top+15);
        dgData.Canvas.Brush.Color := clWindow;

    end;

  end;

  if gdSelected in State then
  begin

    dgData.Canvas.Brush.Style := bsClear;
    dgData.Canvas.Font.Style := Column.Font.Style;
    dgData.Canvas.Pen.Color := $00218326;
    dgData.Canvas.Pen.Width := 1;
    dgData.Canvas.MoveTo(Rect.Left, Rect.Top-1);
    dgData.Canvas.LineTo(Rect.Right, Rect.Top-1);
    dgData.Canvas.MoveTo(Rect.Left, Rect.Bottom);
    dgData.Canvas.LineTo(Rect.Right, Rect.Bottom);

    if (Column.Index = dgData.Col-1)then
    begin
      dgData.Canvas.Pen.Color := $00419366;
      dgData.Canvas.Pen.Width := 2;
      dgData.Canvas.Brush.Style := bsClear;
      dgData.Canvas.Font.Style := Column.Font.Style;
      dgData.Canvas.Rectangle(Rect.Left+1, Rect.Top, Rect.Right, Rect.Bottom+1);
    end;

  end;

end;

procedure TFormLoadsChess.dgDataMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var ordernum: integer;
begin

   if X<50 then exit;

   inherited;

  // работает только если нажат Ctrl
  if ssCtrl in Shift then
  begin

    if meData.FieldByName('isdefective').AsInteger>0 then
    begin
      ShowMessage('Вагон неисправен. Погрузка запрещена.');
      exit;
    end;

    ordernum := 0;
    if dgData.SelectedField.FieldName = 'container1' then ordernum := 1;
    if dgData.SelectedField.FieldName = 'container2' then ordernum := 2;
    if dgData.SelectedField.FieldName = 'container3' then ordernum := 3;
    if dgData.SelectedField.FieldName = 'container4' then ordernum := 4;

    if ordernum = 0 then exit;

    if not ((meOrder.Active) and (meOrder.RecordCount > 0)) then
    begin
      ShowMessage('Для массового заполнения сначала выберите заявку.');
      exit;
    end;

    if (meOrder.FieldByName('rest_amount').AsInteger <= 0) and not (allowminus) then
    begin
      ShowMessage('Внимание! Заявка исчерпана. Можете продолжить, если необходимо.');
      allowminus := true;
      exit;
    end;

    meSpec.Close;
    drvSpec.SelectCommand.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
    drvSpec.SelectCommand.Parameters.ParamByName('order_num').Value := ordernum;
    meSpec.Open;

    RefreshRecord(meData);

    if meData.FieldByName('container'+IntToStr(ordernum)).AsString <> '' then
    begin

      if fQYN('Хотите заместить контейнер?') then
      begin

        drvSpec.DeleteCommand.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
        drvSpec.DeleteCommand.Parameters.ParamByName('order_num').Value := ordernum;
        meSpec.Delete;

        meSpec.Append;

      end
      else exit;
    end else
    begin

      if meSpec.RecordCount >0 then
      begin
        drvSpec.DeleteCommand.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
        drvSpec.DeleteCommand.Parameters.ParamByName('order_num').Value := ordernum;
        meSpec.Delete;
      end;

      meSpec.Open;

      meSpec.Append;

    end;

    meSpec.FieldByName('joint_id').Value := meData.FieldByName('id').AsInteger;
    meSpec.FieldByName('order_num').Value := ordernum;
    meSpec.FieldByName('orderspec_id').Value := meOrder.FieldByName('id').AsInteger;
    // СЕРДАШЕНКО - добавить поле isempty
    // meSpec.FieldByName('isempty').Value := 1;
    // --
    meSpec.FieldByName('isfreeload').Value := 1;
    meSpec.FieldByName('kind_id').Value := meOrder.FieldByName('container_kind_id').Value;
    meSpec.FieldByName('owner_id').Value := meOrder.FieldByName('container_owner_id').Value;
    meSpec.FieldByName('task_id').Value := null;
    meSpec.FieldByName('station_id').Value := meOrder.FieldByName('station_id').Value;
    meSpec.FieldByName('note').Value := meOrder.FieldByName('task_note').Value;

    try
      meSpec.Post;
    except
      on E : Exception do
      begin
        if meSpec.State = (dsEdit) then meSpec.Cancel;
        ShowMessage(E.Message);
        exit;
      end;
    end;

    drvData.GetRecCommand.Parameters.ParamByName('current_id').Value := meData.FieldByName('id').AsInteger;
    meData.RefreshRecord;
    //dgData.Refresh;

    meOrder.Close;
    meOrder.DataDriver := drvOrder;
    drvOrder.SelectCommand.Parameters.ParamByName('current_id').Value := fixedOrderId;
    meOrder.Open;

  end;
end;

procedure TFormLoadsChess.dgDataSelectionChanged(Sender: TObject);
begin
  inherited;
  dgDataCellClick(dgData.Columns[dgData.Col-1]);
  RecalcLen;
end;

procedure TFormLoadsChess.dgOrderDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

  if Column.FieldName = 'rest_amount' then
    if meOrder.FieldByName('rest_amount').AsInteger<0 then
      dgOrder.Canvas.Font.Color := clMaroon;

  dgOrder.DefaultDrawColumnCell(Rect, DataCol, Column, State);

end;

procedure TFormLoadsChess.dgOrderInfoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

  dgOrderInfo.Canvas.Brush.Color := $00DDFFFF;
  dgOrderInfo.Canvas.Font.Color := clBlack;

  if Column.FieldName = 'rest_amount' then
    if meOrderInfo.FieldByName('rest_amount').AsInteger<0 then
      dgOrderInfo.Canvas.Font.Color := clMaroon;

  dgOrderInfo.DefaultDrawColumnCell(Rect, DataCol, Column, State);

end;

procedure TFormLoadsChess.Init;
begin

  meData.Close;

  inherited;
  self.formEdit := FormEditPlaceContainers;
  meData.DisableControls;

  meData.First;
  meData.EnableControls;
  SaveOriginalEditForm := true;
  plBottom.Show;
  btnOk.Show;
  btnOk.Left := 4000;

  self.tablename := 'docloadjoint';
  allowminus := false;

  cbEmptyOnly.Checked := false;

end;

procedure TFormLoadsChess.meDataAfterDelete(DataSet: TDataSet);
begin
  inherited;
  RecalcLen;
end;

procedure TFormLoadsChess.meDataAfterEdit(DataSet: TDataSet);
begin
  inherited;
  {FormEditPlaceContainers.meSpec.Close;
  FormEditPlaceContainers.joint_id := meData.FieldByName('id').AsInteger;
  FormEditPlaceContainers.drvSpec.SelectCommand.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
  FormEditPlaceContainers.meSpec.Open;}
end;

procedure TFormLoadsChess.meDataAfterInsert(DataSet: TDataSet);
begin
  inherited;
  RecalcLen;
end;

procedure TFormLoadsChess.meDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  RecalcLen;
end;

procedure TFormLoadsChess.meDataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  if Assigned(drvData.SelectCommand.Parameters.FindParam('guid')) then
      drvData.SelectCommand.Parameters.ParamByName('guid').Value := g;

  if Assigned(drvData.SelectCommand.Parameters.FindParam('stations')) then
      drvData.SelectCommand.Parameters.ParamByName('stations').Value := stations;

  if Assigned(drvData.SelectCommand.Parameters.FindParam('foots')) then
      drvData.SelectCommand.Parameters.ParamByName('foots').Value := foots;

  if Assigned(drvData.SelectCommand.Parameters.FindParam('onlyempty')) then
      drvData.SelectCommand.Parameters.ParamByName('onlyempty').Value := cbEmptyOnly.Checked;

end;

procedure TFormLoadsChess.meSpecAfterPost(DataSet: TDataSet);
begin
  inherited;
  {
  dm.spRegisterDataChange.Parameters.ParamByName('@tablename').Value := 'documents';
  dm.spRegisterDataChange.Parameters.ParamByName('@tableid').Value := meSpec.FieldByName('id').AsInteger;
  dm.spRegisterDataChange.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
  dm.spRegisterDataChange.Parameters.ParamByName('@EventType').Value := old_current_mode;
  dm.spRegisterDataChange.ExecProc;
  }
end;

procedure TFormLoadsChess.N10Click(Sender: TObject);
begin

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update docload set order_num = order_num-1');
  qrAux.SQL.Add('where joint_id = :joint_id and not exists ');
  qrAux.SQL.Add('(select 1 from docload dl where dl.joint_id = docload.joint_id and dl.order_num<2)');
  qrAux.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
  qrAux.ExecSQL;

  drvData.GetRecCommand.Parameters.ParamByName('current_id').Value := meData.FieldByName('id').AsInteger;
  meData.RefreshRecord;
  //dgData.Refresh;

end;

procedure TFormLoadsChess.N13Click(Sender: TObject);
var pv: TPairValues; pp: TPairParam; ordernum: integer;
begin

  if meData.FieldByName('isdefective').AsInteger>0 then
  begin
    ShowMessage('Вагон неисправен. Погрузка запрещена.');
    exit;
  end;


  if dgData.SelectedField.FieldName = 'container1' then ordernum := 1;
  if dgData.SelectedField.FieldName = 'container2' then ordernum := 2;
  if dgData.SelectedField.FieldName = 'container3' then ordernum := 3;
  if dgData.SelectedField.FieldName = 'container4' then ordernum := 4;

  pp.paramname := 'sections';
  pp.paramvalue := ',output,';

  pv := SFDV(FormContainersOnGo, 0, 'cnum', pp, fGridByNumber);

  if pv.keyvalue>0 then
  begin

      meSpec.Close;
      drvSpec.SelectCommand.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
      drvSpec.SelectCommand.Parameters.ParamByName('order_num').Value := ordernum;
      meSpec.Open;

      RefreshRecord(meData);

      if meData.FieldByName('container'+IntToStr(ordernum)).AsString <> '' then
      begin

        if not fQYN('Заместить контейнер?') then
        begin
          exit;
        end;

        if meSpec.RecordCount >0 then
        begin
          drvSpec.DeleteCommand.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
          drvSpec.DeleteCommand.Parameters.ParamByName('order_num').Value := ordernum;
          meSpec.Delete;
        end;

      end;

      meSpec.Close;
      drvSpec.SelectCommand.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
      drvSpec.SelectCommand.Parameters.ParamByName('order_num').Value := ordernum;
      meSpec.Open;

      if TFormGrid(fGridByNumber).meData.FieldByName('station_id').AsInteger = 0 then
      begin
        ShowMessage('Не указана станция назначения для выбраного контейнера.');
        exit;
      end;

      if meSpec.FieldByName('isfreeload').AsBoolean then
      if not TFormGrid(fGridByNumber).meData.FieldByName('isempty').AsBoolean then
      begin
        ShowMessage('Выбранный контейнер не отмечен как порожний. Свободная погрузка возможна только для порожних контейнеров.');
        exit;
      end;


      if meSpec.RecordCount>0 then
      begin

        drvSpec.DeleteCommand.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
        drvSpec.DeleteCommand.Parameters.ParamByName('order_num').Value := ordernum;
        meSpec.Delete;

        meSpec.Append;

      end else
      begin

        meSpec.Append;

      end;

      meSpec.FieldByName('joint_id').Value := meData.FieldByName('id').AsInteger;
      meSpec.FieldByName('order_num').Value := ordernum;
      meSpec.FieldByName('container_id').Value := pv.keyvalue;
      meSpec.FieldByName('orderspec_id').Value := TFormGrid(fGridByNumber).meData.FieldByName('orderspec_id').Value;
      meSpec.FieldByName('isfreeload').Value := 0;
      meSpec.FieldByName('kind_id').Value := TFormGrid(fGridByNumber).meData.FieldByName('kind_id').Value;
      meSpec.FieldByName('owner_id').Value := TFormGrid(fGridByNumber).meData.FieldByName('owner_id').Value;
      meSpec.FieldByName('task_id').Value := TFormGrid(fGridByNumber).meData.FieldByName('task_id').Value;
      meSpec.FieldByName('station_id').Value := TFormGrid(fGridByNumber).meData.FieldByName('station_id').Value;
      meSpec.FieldByName('note').Value := TFormGrid(fGridByNumber).meData.FieldByName('task_note').Value;

      try
        meSpec.Post;
      except
        on E : Exception do
        begin
          if meSpec.State in [dsEdit, dsInsert] then meSpec.Cancel;
          ShowMessage(E.Message);
          exit;
        end;
      end;

      drvData.GetRecCommand.Parameters.ParamByName('current_id').Value := meData.FieldByName('id').AsInteger;
      meData.RefreshRecord;

      if Assigned(TFormGrid(fGridByNumber).drvData.GetrecCommand.Parameters.FindParam('current_id')) then
      begin
        TFormGrid(fGridByNumber).drvData.GetrecCommand.Parameters.ParamByName('current_id').Value := pv.keyvalue;
        TFormGrid(fGridByNumber).drvData.GetrecCommand.Parameters.ParamByName('sections').Value := ',output,';
        TFormGrid(fGridByNumber).meData.RefreshRecord;
      end;

  end;

end;

procedure TFormLoadsChess.N14Click(Sender: TObject);
var ordernum: integer;
begin

  ordernum := 1;
  if dgData.SelectedField.FieldName = 'container1' then ordernum := 1;
  if dgData.SelectedField.FieldName = 'container2' then ordernum := 2;
  if dgData.SelectedField.FieldName = 'container3' then ordernum := 3;
  if dgData.SelectedField.FieldName = 'container4' then ordernum := 4;

  if ordernum > 3 then exit;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update docload set order_num = order_num+1');
  qrAux.SQL.Add('where joint_id = :joint_id and order_num = :order_num');
  qrAux.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
  qrAux.Parameters.ParamByName('order_num').Value := ordernum;
  qrAux.ExecSQL;

  drvData.GetRecCommand.Parameters.ParamByName('current_id').Value := meData.FieldByName('id').AsInteger;
  meData.RefreshRecord;
  //dgData.Refresh;

  dgData.Col := dgData.Col + 1;

end;

procedure TFormLoadsChess.N15Click(Sender: TObject);
var ordernum: integer;
begin

  ordernum := 1;
  if dgData.SelectedField.FieldName = 'container1' then ordernum := 1;
  if dgData.SelectedField.FieldName = 'container2' then ordernum := 2;
  if dgData.SelectedField.FieldName = 'container3' then ordernum := 3;
  if dgData.SelectedField.FieldName = 'container4' then ordernum := 4;

  if ordernum<2 then exit;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update docload set order_num = order_num-1');
  qrAux.SQL.Add('where joint_id = :joint_id and order_num = :order_num');
  qrAux.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
  qrAux.Parameters.ParamByName('order_num').Value := ordernum;
  qrAux.ExecSQL;

  RefreshRecord(meData);

  dgData.Col := dgData.Col - 1;

end;

procedure TFormLoadsChess.N17Click(Sender: TObject);
begin

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update docloadjoint set order_on_path = order_on_path-1');
  qrAux.SQL.Add('where id = :id');
  qrAux.Parameters.ParamByName('id').Value := meData.FieldByName('id').AsInteger;
  qrAux.ExecSQL;

  FullRefresh(meData, true);

end;

procedure TFormLoadsChess.N18Click(Sender: TObject);
begin

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update docloadjoint set order_on_path = order_on_path+1');
  qrAux.SQL.Add('where id = :id');
  qrAux.Parameters.ParamByName('id').Value := meData.FieldByName('id').AsInteger;
  qrAux.ExecSQL;

  FullRefresh(meData, true);

end;

procedure TFormLoadsChess.N19Click(Sender: TObject);
var g: string; guid: TGuid; id: integer;
begin
  FormSetCheck.cbCheck.Caption := 'Требуется осмотр';
  FormSetCheck.ShowModal;
  if FormSetCheck.ModalResult = mrOk then
  begin

    CreateGUID(guid);
    g := GuidToString(guid);
    FillSelectListAll(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('update carriages set need_inspection = :need_inspection ');
    qrAux.SQL.Add('where id in (');
    qrAux.SQL.Add('select j.carriage_id from docloadjoint j, selectlist sl where sl.id = j.id and sl.guid = :guid');
    qrAux.SQL.Add(')');
    qrAux.Parameters.ParamByName('need_inspection').Value := FormSetCheck.cbCheck.Checked;
    qrAux.Parameters.ParamByName('guid').Value := g;
    qrAux.ExecSQL;

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    FullRefresh(meData);

  end;
end;

procedure TFormLoadsChess.N21Click(Sender: TObject);
begin
  drvCarriages.SelectCommand.Parameters.ParamByName('carriage_id').Value := meData.FieldByName('carriage_id').AsInteger;
  meCarriages.Close;
  meCarriages.Open;
  EE(nil, FormEditCarriage, meCarriages, 'carriages');
  self.RefreshRecord(meData,meData.FieldByName('id').AsInteger);
end;

procedure TFormLoadsChess.N22Click(Sender: TObject);
var col: TColumnEh;
begin
  col := dgData.Columns[dgData.Col-1];
  if col.ReadOnly then exit;
  FillStringColumn(col.FieldName);
end;

procedure TFormLoadsChess.N24Click(Sender: TObject);
begin
  ClearCells;
end;

procedure TFormLoadsChess.N9Click(Sender: TObject);
begin

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update docload set order_num = order_num+1');
  qrAux.SQL.Add('where joint_id = :joint_id and not exists ');
  qrAux.SQL.Add('(select 1 from docload dl where dl.joint_id = docload.joint_id and dl.order_num>3)');
  qrAux.Parameters.ParamByName('joint_id').Value := meData.FieldByName('id').AsInteger;
  qrAux.ExecSQL;

  drvData.GetRecCommand.Parameters.ParamByName('current_id').Value := meData.FieldByName('id').AsInteger;
  meData.RefreshRecord;
  //dgData.Refresh;

end;

procedure TFormLoadsChess.sbAddClick(Sender: TObject);
var pv: TPairList; pp: TPairParam; i: integer;
begin

  pp.paramname := '';
  pv := SFDVMulti(FormCarriagesOnGo, 0, 'train_num', pp, fGridCarriages);

  for i := 0 to pv.Keys.Count-1 do
  begin
    meData.Append;
    meData.FieldByName('carriage_id').Value := StrToInt(pv.Keys[i]);
    meData.FieldByName('folder_id').Value := self.folder_id;
    meData.FieldByName('train_num_source').Value := pv.DisplayValues[i];
    try
      meData.Post;
      RecalcLen;
    except
      on E : Exception do
      begin
        if meData.State = (dsEdit) then meData.Cancel;
        if meData.State = (dsInsert) then meData.Cancel;
        ShowMessage(E.Message);
        exit;
      end;
    end;

  end;

  meData.Close;
  meData.Open;

  nuLen1.Value := VarToReal(nuLen.Value)/14;

end;

procedure TFormLoadsChess.sbClearFilterClick(Sender: TObject);
begin
  self.stations := '';
  self.foots := '';
  SetFilter('');
end;

procedure TFormLoadsChess.sbDeleteClick(Sender: TObject);
var g: string; guid: TGuid; id: integer;
begin
  if fQYN('Пометить записи на удаление ?') then
  begin

     try
         if dgData.Selection.SelectionType in [gstAll, gstRecordBookmarks] then
         begin

          CreateGUID(guid);
          g := GuidToString(guid);
          FillSelectListAll(g);

          qrAux.Close;
          qrAux.SQL.Clear;
          qrAux.SQL.Add('update docloadjoint set isdeleted=1 where id in (select id from selectlist where rtrim(guid) = '''+trim(g)+''')');
          qrAux.ExecSQL;

          dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
          dm.spClearSelectList.ExecProc;

         end else
         begin
            meData.Edit;
            meData.FieldByName('isdeleted').Value := true;
            meData.Post;
         end;
     except
      on E: Exception do
      begin
        ShowMessage(E.Message);
        meData.Cancel;
      end;
     end;

      id := meData.FieldByName('id').AsInteger;
      meData.Close;
      meData.Open;
      meData.Locate('id', id, []);

  end;
end;

procedure TFormLoadsChess.sbEditClick(Sender: TObject);
var id: integer;
begin

  if meData.RecordCount=0 then exit;
  FormEditPlaceContainers.WindowState := wsMaximized;
  inherited;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select 1 from docload l where l.joint_id = '+meData.FieldByName('id').AsString);
  qrAux.SQL.Add('and l.task_id is null');
  qrAux.Open;
  if qrAux.RecordCount>0 then
  begin
    //ShowMessage('Незавершенные погрузки будут удалены.');
    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from docload where joint_id = '+meData.FieldByName('id').AsString);
    qrAux.SQL.Add('and task_id is null and orderspec_id is null');
    qrAux.ExecSQL;

    id := meData.FieldByName('id').AsInteger;
    meData.Close;
    meData.Open;
    meData.Locate('id',id,[]);
  end;

  meOrder.Close;
  meOrder.DataDriver := drvOrder;
  drvOrder.SelectCommand.Parameters.ParamByName('current_id').Value := fixedOrderId;
  meOrder.Open;

end;

procedure TFormLoadsChess.sbFixedSelectClick(Sender: TObject);
var pv: TPairValues; pp: TPairparam; fGrid: TForm;
begin
  inherited;

  fGrid := nil;

  pv := SFDV(FormContainersInOrders, 0, 'id', pp, fGrid);
  if pv.keyvalue>0 then
  begin
    self.fixedOrderId := pv.keyvalue;
    meOrder.Close;
    meOrder.DataDriver := nil;
    meOrder.EmptyTable;
    meOrder.LoadFromDataSet(TFormGrid(fGrid).meData,1,lmCopy,true);
    meOrder.Open;
    allowminus := false;
    if (meOrder.FieldByName('rest_amount').AsInteger <= 0) and not (allowminus) then
    begin
      ShowMessage('Внимание! Заявка исчерпана. Можете продолжить, если необходимо.');
      allowminus := true;
    end;
  end;
end;

procedure TFormLoadsChess.sbMassAddClick(Sender: TObject);
begin
  FormSelectObjects.ShowModal;
end;

procedure TFormLoadsChess.sbPathClick(Sender: TObject);
var g: string; gu: TGuid; id: integer;
begin

  FormSelectPath.qrAux.Open;
  FormSelectPath.ShowModal;
  if FormSelectPath.ModalResult = mrOk then
  begin

    CreateGuid(gu);
    g := GuidToString(gu);
    FillSelectListAll(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('update docloadjoint set path_id = :path_id');
    qrAux.SQL.Add('where id in (select id from selectlist where guid = '''+g+''')');
    qrAux.Parameters.ParamByName('path_id').Value := FormSelectPath.qrAux.FieldByName('id').AsInteger;
    qrAux.ExecSQL;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from selectlist where guid = '''+g+'''');
    qrAux.ExecSQL;

    id := meData.FieldByName('id').AsInteger;
    meData.Close;
    meData.Open;
    meData.Locate('id', id, []);

  end;
end;

procedure TFormLoadsChess.sbRestoreClick(Sender: TObject);
var id, i: integer; g: string; guid: TGuid;
begin

  if fQYN('Восстановить выбранные записи ?') then
  begin

     if dgData.Selection.SelectionType in [gstAll, gstRecordBookmarks] then
     begin

      CreateGUID(guid);
      g := GuidToString(guid);
      FillSelectListAll(g);

      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('update docloadjoint set isdeleted=0 where id in (select id from selectlist where rtrim(guid) = '''+trim(g)+''')');
      qrAux.ExecSQL;

      dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
      dm.spClearSelectList.ExecProc;

     end else
     begin
        meData.Edit;
        meData.FieldByName('isdeleted').Value := false;
        meData.Post;
     end;

      id := meData.FieldByName('id').AsInteger;
      meData.Close;
      meData.Open;
      meData.Locate('id', id, []);

  end;

end;

procedure TFormLoadsChess.sbSpeedMoveClick(Sender: TObject);
begin
  FormSpeedMove.Init(meData);
  FormSpeedMove.ShowModal;
end;

procedure TFormLoadsChess.spWasteClick(Sender: TObject);
var id, i: integer; g: string; guid: TGuid;
begin
  if fQYN('Внимание! Вы собираетесь удалить записи окончательно, безвозвратно. Вы уверены?') then
  begin

     if dgData.Selection.SelectionType in [gstAll, gstRecordBookmarks] then
     begin

      CreateGUID(guid);
      g := GuidToString(guid);
      FillSelectListAll(g);

      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('delete from docloadjoint where isdeleted=1 and id in (select id from selectlist where rtrim(guid) = '''+trim(g)+''')');
      qrAux.ExecSQL;

      dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
      dm.spClearSelectList.ExecProc;

     end else
     begin
       meData.Delete;
     end;

      id := meData.FieldByName('id').AsInteger;
      meData.Close;
      meData.Open;
      meData.Locate('id', id, []);

      meOrder.Close;
      meOrder.DataDriver := drvOrder;
      drvOrder.SelectCommand.Parameters.ParamByName('current_id').Value := fixedOrderId;
      meOrder.Open;

  end;
end;



procedure TFormLoadsChess.SetFilter(conditions: string);
begin

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := conditions;
    dm.spCreateFilter.ExecProc;

    meData.Close;
    meData.Open;

    if (conditions = '') and (foots = '') and (stations = '') then
    begin
      plStatus.Caption := ' Фильтр снят.';
      g := '';
    end else
    begin
      plStatus.Caption := ' Установлен фильтр.';
    end;

end;


procedure TFormLoadsChess.RecalcLen;
var i, k: integer; nLen: real; b: TBookMark;
begin

  nLen := 0;

  b := meData.Bookmark;
  meData.DisableControls;

  if dgData.Selection.SelectionType = (gstRecordBookmarks) then
  begin

    for i := 0 to dgData.SelectedRows.Count - 1 do begin
      meData.GotoBookmark(dgData.SelectedRows.Items[i]);
      nLen := nLen + meData.FieldByName('car_length').AsFloat;
    end;

    nuLen.Font.Color := clBlue;
    nuLen1.Font.Color := clBlue;
    nuAmount.Value := dgData.SelectedRows.Count;

  end else
  begin

    for i:= 0 to meData.RecordsView.Count-1 do
    begin
      nLen := nLen + VarToReal(meData.RecordsView.Rec[i].DataValues['car_length', TDataValueVersionEh.dvvCurValueEh]);
    end;

    nuLen.Font.Color := clGray;
    nuLen1.Font.Color := clGray;
    nuAmount.Value := meData.RecordsView.Count;

  end;

  nuLen.Value := nLen;
  nuLen1.Value := VarToReal(nuLen.Value)/14;

  meData.GotoBookmark(b);
  meData.EnableControls;


end;


end.
