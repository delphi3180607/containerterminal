﻿inherited FormReports: TFormReports
  Caption = #1054#1090#1095#1077#1090#1099
  ClientHeight = 654
  ClientWidth = 853
  ExplicitWidth = 869
  ExplicitHeight = 692
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 613
    Width = 853
    ExplicitTop = 613
    ExplicitWidth = 853
    inherited btnOk: TButton
      Left = 618
      ExplicitLeft = 618
    end
    inherited btnCancel: TButton
      Left = 737
      ExplicitLeft = 737
    end
  end
  inherited plAll: TPanel
    Top = 47
    Width = 853
    Height = 566
    Caption = ''
    Color = 11249306
    StyleElements = [seFont, seBorder]
    ExplicitTop = 47
    ExplicitWidth = 853
    ExplicitHeight = 566
    object Splitter1: TSplitter [0]
      Left = 246
      Top = 0
      Height = 566
      Color = clBtnFace
      ParentColor = False
      ExplicitLeft = 249
    end
    inherited plTop: TPanel
      Width = 41
      Height = 566
      Align = alLeft
      ExplicitWidth = 41
      ExplicitHeight = 566
      inherited sbDelete: TPngSpeedButton
        Left = 0
        Top = 29
        Width = 41
        Height = 32
        Align = alTop
        ExplicitLeft = 3
        ExplicitTop = 36
        ExplicitWidth = 226
        ExplicitHeight = 32
      end
      inherited btFilter: TPngSpeedButton
        Left = 0
        Top = 131
        Width = 41
        Height = 44
        Align = alTop
        Visible = False
        ExplicitLeft = 1
        ExplicitTop = 148
        ExplicitWidth = 116
        ExplicitHeight = 44
      end
      inherited btExcel: TPngSpeedButton
        Left = 0
        Top = 87
        Width = 41
        Height = 44
        Align = alTop
        Visible = False
        ExplicitLeft = 1
        ExplicitTop = 102
        ExplicitWidth = 162
        ExplicitHeight = 44
      end
      inherited sbAdd: TPngSpeedButton
        Top = 1
        Width = 41
        Height = 28
        Align = alTop
        Flat = True
        ExplicitTop = 2
        ExplicitWidth = 260
        ExplicitHeight = 28
      end
      inherited sbEdit: TPngSpeedButton
        Left = 0
        Top = 61
        Width = 41
        Height = 26
        Align = alTop
        ExplicitLeft = 1
        ExplicitTop = 72
        ExplicitWidth = 190
        ExplicitHeight = 26
      end
      inherited Bevel2: TBevel
        Left = 3
        Width = 35
        Height = 1
        Align = alTop
        ExplicitLeft = 106
        ExplicitTop = 0
        ExplicitWidth = 265
        ExplicitHeight = 1
      end
      inherited btTool: TPngSpeedButton
        Left = 3
        Top = 178
        Height = 49
        Align = alTop
        ExplicitLeft = 636
        ExplicitTop = 196
        ExplicitWidth = 66
        ExplicitHeight = 49
      end
      inherited plCount: TPanel
        Left = -153
        Top = 230
        Height = 336
        ExplicitLeft = -153
        ExplicitTop = 230
        ExplicitHeight = 336
        inherited edCount: TDBEditEh
          Height = 330
          ControlLabel.ExplicitLeft = 16
          ControlLabel.ExplicitTop = 161
          ControlLabel.ExplicitWidth = 103
          Visible = False
          ExplicitHeight = 330
        end
      end
    end
    inherited dgData: TDBGridEh
      Left = 41
      Top = 0
      Width = 205
      Height = 566
      Align = alLeft
      AutoFitColWidths = True
      Border.Color = clSilver
      Ctl3D = True
      DataGrouping.Active = True
      DataGrouping.Color = 15592150
      DataGrouping.DefaultStateExpanded = True
      DataGrouping.Font.Color = 9918500
      DataGrouping.GroupLevels = <
        item
          ColumnName = 'Column_0_folder_name'
        end>
      DataGrouping.ParentColor = False
      DataGrouping.ParentFont = False
      Flat = False
      Font.Style = [fsBold]
      GridLineParams.BrightColor = 15395041
      GridLineParams.DarkColor = 15395041
      GridLineParams.DataBoundaryColor = 15395041
      IndicatorParams.FillStyle = cfstGradientEh
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghFrozen3D, dghFooter3D, dghData3D, dghHighlightFocus, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      TitleParams.FillStyle = cfstGradientEh
      OnDblClick = sbStartClick
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'folder_name'
          Footers = <>
          Title.Caption = ':'
          Visible = False
          Width = 89
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'report_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          Footers = <>
          TextEditing = False
          Title.Caption = #1048#1084#1103' '#1086#1090#1095#1077#1090#1072
          Width = 162
        end>
    end
    inherited plHint: TPanel
      Left = 51
      Width = 191
      ExplicitLeft = 51
      ExplicitWidth = 191
      inherited lbText: TLabel
        Width = 183
        Height = 49
      end
    end
    object dpReports: TDBGridEh
      Left = 249
      Top = 0
      Width = 604
      Height = 566
      Align = alClient
      AllowedOperations = [alopUpdateEh]
      Border.Color = 15782790
      Border.ExtendedDraw = True
      Color = clWhite
      ColumnDefValues.ToolTips = True
      Ctl3D = False
      DataGrouping.Color = clWindow
      DataGrouping.Font.Charset = DEFAULT_CHARSET
      DataGrouping.Font.Color = clWindowText
      DataGrouping.Font.Height = -12
      DataGrouping.Font.Name = 'Arial'
      DataGrouping.Font.Style = [fsBold]
      DataGrouping.GroupRowDefValues.FooterInGroupRow = True
      DataGrouping.ParentColor = False
      DataGrouping.ParentFont = False
      DataSource = dsReport
      DrawMemoText = True
      DynProps = <>
      EditActions = [geaCutEh, geaCopyEh, geaPasteEh, geaDeleteEh, geaSelectAllEh]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = []
      FooterParams.Color = clSilver
      FooterParams.FillStyle = cfstThemedEh
      FooterParams.HorzLineColor = clSilver
      FooterParams.VertLineColor = clSilver
      GridLineParams.BrightColor = clSilver
      GridLineParams.DarkColor = clSilver
      GridLineParams.DataBoundaryColor = clSilver
      GridLineParams.DataHorzColor = 14607074
      GridLineParams.DataVertColor = 14607074
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      ImeMode = imDisable
      IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
      IndicatorParams.Color = clBtnFace
      IndicatorParams.FillStyle = cfstGradientEh
      IndicatorParams.HorzLineColor = 14540253
      IndicatorParams.VertLineColor = 12303291
      EmptyDataInfo.Text = #1053#1072#1078#1084#1080#1090#1077' Ctrl-Insert '#1076#1083#1103' '#1089#1086#1079#1076#1072#1085#1080#1103' '#1085#1086#1074#1086#1075#1086' '#1086#1090#1095#1077#1090#1072
      OddRowColor = 15791348
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      SelectionDrawParams.SelectionStyle = gsdsGridThemedEh
      SelectionDrawParams.DrawFocusFrame = False
      SelectionDrawParams.DrawFocusFrameStored = True
      ShowHint = True
      SortLocal = True
      STFilter.Color = clWhite
      STFilter.Font.Charset = DEFAULT_CHARSET
      STFilter.Font.Color = clNavy
      STFilter.Font.Height = -13
      STFilter.Font.Name = 'Arial'
      STFilter.Font.Style = []
      STFilter.HorzLineColor = 14540253
      STFilter.Local = True
      STFilter.ParentFont = False
      STFilter.VertLineColor = 12303291
      STFilter.Visible = True
      TabOrder = 3
      TitleParams.Color = 15790320
      TitleParams.FillStyle = cfstThemedEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -12
      TitleParams.Font.Name = 'Calibri'
      TitleParams.Font.Style = [fsBold]
      TitleParams.HorzLineColor = clSilver
      TitleParams.MultiTitle = True
      TitleParams.ParentFont = False
      TitleParams.SecondColor = clBtnFace
      TitleParams.SortMarkerStyle = smstDefaultEh
      TitleParams.VertLineColor = clSilver
      TreeViewParams.GlyphStyle = tvgsClassicEh
      Visible = False
      OnDrawColumnCell = dpReportsDrawColumnCell
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object plMessage: TPanel
      Left = 660
      Top = 8
      Width = 185
      Height = 41
      Anchors = [akTop, akRight]
      Caption = #1060#1086#1088#1084#1080#1088#1091#1102' '#1086#1090#1095#1077#1090'...'
      Color = 15593455
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 4
      Visible = False
    end
  end
  object plTool: TPanel [2]
    Left = 0
    Top = 0
    Width = 853
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    Color = 15790320
    Padding.Top = 3
    Padding.Bottom = 3
    ParentBackground = False
    TabOrder = 2
    StyleElements = [seFont, seBorder]
    object sbStart: TPngSpeedButton
      AlignWithMargins = True
      Left = 42
      Top = 5
      Width = 46
      Height = 38
      Hint = #1047#1072#1087#1091#1089#1090#1080#1090#1100' '#1086#1090#1095#1077#1090
      Margins.Left = 42
      Margins.Top = 2
      Margins.Bottom = 1
      Align = alLeft
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = sbStartClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D494844520000001C0000001C0806000000720DDF
        940000000473424954080808087C086488000000097048597300000B1200000B
        1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
        65776F726B732043533571B5E336000001C34944415478DAE596CD2B44511887
        E7FAB6B0522CE4230B4BE4A329946221D31452B3A3B11ACBF117306465434ACD
        46D88D521349143352986C6C506ACA1445590CBB31C6785E736F4942EE3D63E1
        D4D3EF9C33D37DEED739F7D5D2E9B4CD68D5AB2FA3440794DBAC69715889B972
        368C09CD10223B276A2102490B641A94420D5C400FE247AD2A90CA637006CFD0
        C9E4BD4557675C481DB105B71CBB5D849D0CB619145929FA449C20EC225CA153
        81B05BB1F0980889F0502610B62916AE139A0883D241D8F7BF851E8FA79F08FB
        FDFE78B68461A201BC4897B321DC236497B0C3258C21DE572DDC01F9FF3438E0
        084610C75409E5194EEA6359B733500FF3E0FBEAF99A16BE9B1F2216200513FC
        3EAB4CC85C31310CE3F0A40B972C17D2CFA7DF0553D0A4E72CBF3D58794B7709
        792B6549CC8113E43BE755F5D288301764CFBD01B7EA6511221AF55BE7FBA9C8
        8CD04D04B3B6B5996D7F2A5CA35388D0A958B8492445E8A7D382B059B1F09488
        885036E145B023FD763DFD52D66ACBAC59D75B5DAA173852B50D228D5A2C2B23
        0E20CAB11D86506A47D941A410BE826B5BA69035D30AC407952045F600C23BED
        43A92F9F9C5E90B32A31294CE8271E4074624CBE028F3B311375DCAD7D000000
        0049454E44AE426082}
      ExplicitLeft = 3
      ExplicitTop = 6
    end
    object btExport: TPngSpeedButton
      AlignWithMargins = True
      Left = 94
      Top = 5
      Width = 50
      Height = 38
      Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
      Margins.Top = 2
      Margins.Bottom = 1
      Align = alLeft
      Enabled = False
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = btExportClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F40000001974455874536F6674776172650041646F626520496D616765526561
        647971C9653C000005474944415478DAB5576B4C1C5514FEEECC020BB52049A1
        318DC496288F96D682B5514994056BCB5AA568D2A62D10A3B18DD168A22D1116
        767926AD7F4CFAC360A2495B4B4BA3C5D0024D5AE81FF587AD62D3F0306A4D48
        34428D080576D9DD99EBB933B3ECCEBA747705CF663633DFDCC777BE73CEBD77
        18004B716BFA971E75CEAEA87E8043332E7E5C7F608C41FCF40740962CB04AAB
        7ABF724C5510E2C7324C8CBA7A5B73D2CCB70D9EB83A3EDE9284EB8DDE54BABD
        BB5C02195B5DD2E4F74E051B6A191264FD8597FCFAF5B8AEC086A3845BF4C63E
        05F8E518476193842117CF24E8CE7209643EEA6413432E150FD1445FBCD143A0
        8CBBEE291C3A7D1012B5E8A8EED4C3C215D47C5AA5112B74310C35612DC193CB
        26B0A511133F34E9DEAE2712F6A2AD34B18C17B6BC89239DAFA1647311FC8A1F
        976E7C87DB862A854E22D0BC42043613819B0601619B1C0CE5DB4BA0A80A6C8F
        EC4343D73B24BD17B75A826D0AA8CD4FEC1BBA9B2569FC468A726D481E327838
        26144D90801499F7FEE1B45744242006B75A9251F6C4D344C2876D0F3E07D7D9
        A3186E0BB6C9A967F831E4395E4B73F663A6B93CF55F21C83EC2E0AC76E0E7BF
        6EE2F2E00076DA4A497E1F72D614E1F8B936CD2D893CF053698EBEF80A65E53C
        394999092FEAA63E467BFAEB749F684C138689B256A9DA8A2F22A5A11FEED6F2
        4C53126653FCDFAB7E1BBFCFDC864C3920CB322EF6F563D78E1D50291C59E939
        E8387F02ED873FC2BB1D8770ABA2C6204025C37C70D1642E31194F3062108609
        1F1537F0640F921DFDF0B495AF355701799F44E52689BECCB88C30BE545949CF
        323257ADC3C9F31DF0710F0E14CF62CE3B4DAF1522ACC2C238FC9C41E192367F
        38A6AA240085B465F77AA438FAE06EB39B09DCCB1E2675122D6215A4A5D3C268
        208E1E27C79C4757D642EBC785C13154DA7229647A9F708CBA682214ACA62474
        F41281E775028F35254E5C6F5C882B89F2EA18FA5D1CF31E7DD9B6C80CDD83C3
        D863DB48937183801913A485AC05690CC9F5BDF0B4EB0432B6BF9FDAAD58DD4F
        F928D9B821B960ECF151C8B81E0629B01588BD80AEFC07CAF1E1611AC41B2470
        8126AB0C23108A0508E4A79A099020C8A26B8DE863389890E8B8D1BFD0528409
        830043908088F0D92BE3A878260B3E632BA27CC5E7032378B9341F8A12190B84
        203B05260262D264BAAC21F3A45BEB07C6DCAD367CD03BA6792F31B628BF4480
        2845854655547511930914CF9AA7113095732D116B776F32E54024CBB4D6F54E
        B8DBCA31EE311408EEC69A675D0363D85B9A6BF2361A165020CB6A5660090297
        88801DE36EE8696E882388881DB3EBEA08F696E56BBBA316B31830ED7C410364
        25C741E0370F160F263A017DCB3E4703EF0B9B2C1A26C611FDD7C5A3C01DBF5E
        15A12110F5DD797514FBCBF24C351F0D530D21332CB110A82702AD769CB83C6C
        647EB0A94CAB914C09A6D0888AA2C68CA9DA310F786BE7465A8A89405B0C0ACC
        A808ECA84105A8023E23CF0E0ACF8C0534162C904AA9521C2198F6432B9FF01C
        38736504079E35C73B1A26724094735A4C213008CC2A91099CA281ABC3268B86
        0508DC27C741C087C593BA66621514E7D653D746515D92072590173160AA1142
        B159C79C849F501921641564CB48427D9FE17895CA32E624449802810EA7AF8D
        A1AA24D7D4291A1692CBD1154822021E838012F28219613839388A1A5B1E02A7
        8858306E84459895082CDC8B40328560BED58EA5ECF4E008AA6CF9FF09131675
        334A6DEC9B986EDA85FFCB8C53F1920432326B3BBB3D49F7D32145851A76FA66
        813F0EF337400C98F65D40C9695DF8FBEBC963FBF72C4520D22165254DEC307F
        D235BE148148879495342188F81C77FF03930A11682F4FB2440000000049454E
        44AE426082}
      ExplicitLeft = 59
      ExplicitTop = 6
    end
    object sbClose: TPngSpeedButton
      AlignWithMargins = True
      Left = 800
      Top = 5
      Width = 50
      Height = 38
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1090#1095#1077#1090
      Margins.Top = 2
      Margins.Bottom = 1
      Align = alRight
      Enabled = False
      Flat = True
      ParentShowHint = False
      ShowHint = True
      OnClick = sbCloseClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F4000000097048597300000B1300000B1301009A9C1800000A4F694343505068
        6F746F73686F70204943432070726F66696C65000078DA9D53675453E9163DF7
        DEF4424B8880944B6F5215082052428B801491262A2109104A8821A1D91551C1
        114545041BC8A088038E8E808C15512C0C8A0AD807E421A28E83A3888ACAFBE1
        7BA36BD6BCF7E6CDFEB5D73EE7ACF39DB3CF07C0080C9648335135800CA9421E
        11E083C7C4C6E1E42E40810A2470001008B3642173FD230100F87E3C3C2B22C0
        07BE000178D30B0800C04D9BC0301C87FF0FEA42995C01808401C07491384B08
        801400407A8E42A600404601809D98265300A0040060CB6362E300502D006027
        7FE6D300809DF8997B01005B94211501A09100201365884400683B00ACCF568A
        450058300014664BC43900D82D00304957664800B0B700C0CE100BB200080C00
        305188852900047B0060C8232378008499001446F2573CF12BAE10E72A000078
        99B23CB9243945815B082D710757572E1E28CE49172B14366102619A402EC279
        99193281340FE0F3CC0000A0911511E083F3FD78CE0EAECECE368EB60E5F2DEA
        BF06FF226262E3FEE5CFAB70400000E1747ED1FE2C2FB31A803B06806DFEA225
        EE04685E0BA075F78B66B20F40B500A0E9DA57F370F87E3C3C45A190B9D9D9E5
        E4E4D84AC4425B61CA577DFE67C25FC057FD6CF97E3CFCF7F5E0BEE22481325D
        814704F8E0C2CCF44CA51CCF92098462DCE68F47FCB70BFFFC1DD322C44962B9
        582A14E35112718E449A8CF332A52289429229C525D2FF64E2DF2CFB033EDF35
        00B06A3E017B912DA85D6303F64B27105874C0E2F70000F2BB6FC1D428080380
        6883E1CF77FFEF3FFD47A02500806649927100005E44242E54CAB33FC7080000
        44A0812AB0411BF4C1182CC0061CC105DCC10BFC6036844224C4C24210420A64
        801C726029AC82422886CDB01D2A602FD4401D34C051688693700E2EC255B80E
        3D700FFA61089EC128BC81090441C808136121DA8801628A58238E08179985F8
        21C14804128B2420C9881451224B91354831528A542055481DF23D720239875C
        46BA913BC8003282FC86BC47319481B2513DD40CB543B9A8371A8446A20BD064
        74319A8F16A09BD072B41A3D8C36A1E7D0AB680FDA8F3E43C730C0E8180733C4
        6C302EC6C342B1382C099363CBB122AC0CABC61AB056AC03BB89F563CFB17704
        128145C0093604774220611E4148584C584ED848A8201C243411DA0937090384
        51C2272293A84BB426BA11F9C4186232318758482C23D6128F132F107B8843C4
        37241289433227B9900249B1A454D212D246D26E5223E92CA99B34481A2393C9
        DA646BB20739942C202BC885E49DE4C3E433E41BE421F25B0A9D624071A4F853
        E22852CA6A4A19E510E534E5066598324155A39A52DDA8A15411358F5A42ADA1
        B652AF5187A81334759A39CD8316494BA5ADA295D31A681768F769AFE874BA11
        DD951E4E97D057D2CBE947E897E803F4770C0D861583C7886728199B18071867
        197718AF984CA619D38B19C754303731EB98E7990F996F55582AB62A7C1591CA
        0A954A9526951B2A2F54A9AAA6AADEAA0B55F355CB548FA95E537DAE46553353
        E3A909D496AB55AA9D50EB531B5367A93BA887AA67A86F543FA47E59FD890659
        C34CC34F43A451A0B15FE3BCC6200B6319B3782C216B0DAB86758135C426B1CD
        D97C762ABB98FD1DBB8B3DAAA9A13943334A3357B352F394663F07E39871F89C
        744E09E728A797F37E8ADE14EF29E2291BA6344CB931655C6BAA96979658AB48
        AB51AB47EBBD36AEEDA79DA6BD45BB59FB810E41C74A275C2747678FCE059DE7
        53D953DDA70AA7164D3D3AF5AE2EAA6BA51BA1BB4477BF6EA7EE989EBE5E809E
        4C6FA7DE79BDE7FA1C7D2FFD54FD6DFAA7F5470C5806B30C2406DB0CCE183CC5
        35716F3C1D2FC7DBF151435DC34043A561956197E18491B9D13CA3D5468D460F
        8C69C65CE324E36DC66DC6A326062621264B4DEA4DEE9A524DB9A629A63B4C3B
        4CC7CDCCCDA2CDD699359B3D31D732E79BE79BD79BDFB7605A785A2CB6A8B6B8
        6549B2E45AA659EEB6BC6E855A3959A558555A5DB346AD9DAD25D6BBADBBA711
        A7B94E934EAB9ED667C3B0F1B6C9B6A9B719B0E5D806DBAEB66DB67D61676217
        67B7C5AEC3EE93BD937DBA7D8DFD3D070D87D90EAB1D5A1D7E73B472143A563A
        DE9ACE9CEE3F7DC5F496E92F6758CF10CFD833E3B613CB29C4699D539BD34767
        1767B97383F3888B894B82CB2E973E2E9B1BC6DDC8BDE44A74F5715DE17AD2F5
        9D9BB39BC2EDA8DBAFEE36EE69EE87DC9FCC349F299E593373D0C3C843E051E5
        D13F0B9F95306BDFAC7E4F434F8167B5E7232F632F9157ADD7B0B7A577AAF761
        EF173EF63E729FE33EE33C37DE32DE595FCC37C0B7C8B7CB4FC36F9E5F85DF43
        7F23FF64FF7AFFD100A78025016703898141815B02FBF87A7C21BF8E3F3ADB65
        F6B2D9ED418CA0B94115418F82AD82E5C1AD2168C8EC90AD21F7E798CE91CE69
        0E85507EE8D6D00761E6618BC37E0C2785878557863F8E7088581AD131973577
        D1DC4373DF44FA449644DE9B67314F39AF2D4A352A3EAA2E6A3CDA37BA34BA3F
        C62E6659CCD5589D58496C4B1C392E2AAE366E6CBEDFFCEDF387E29DE20BE37B
        17982FC85D7079A1CEC2F485A716A92E122C3A96404C884E3894F041102AA816
        8C25F21377258E0A79C21DC267222FD136D188D8435C2A1E4EF2482A4D7A92EC
        91BC357924C533A52CE5B98427A990BC4C0D4CDD9B3A9E169A76206D323D3ABD
        31839291907142AA214D93B667EA67E66676CBAC6585B2FEC56E8BB72F1E9507
        C96BB390AC05592D0AB642A6E8545A28D72A07B267655766BFCD89CA3996AB9E
        2BCDEDCCB3CADB90379CEF9FFFED12C212E192B6A5864B572D1D58E6BDAC6A39
        B23C7179DB0AE315052B865606AC3CB88AB62A6DD54FABED5797AE7EBD267A4D
        6B815EC1CA82C1B5016BEB0B550AE5857DEBDCD7ED5D4F582F59DFB561FA869D
        1B3E15898AAE14DB1797157FD828DC78E51B876FCABF99DC94B4A9ABC4B964CF
        66D266E9E6DE2D9E5B0E96AA97E6970E6E0DD9DAB40DDF56B4EDF5F645DB2F97
        CD28DBBB83B643B9A3BF3CB8BC65A7C9CECD3B3F54A454F454FA5436EED2DDB5
        61D7F86ED1EE1B7BBCF634ECD5DB5BBCF7FD3EC9BEDB5501554DD566D565FB49
        FBB3F73FAE89AAE9F896FB6D5DAD4E6D71EDC703D203FD07230EB6D7B9D4D51D
        D23D54528FD62BEB470EC71FBEFE9DEF772D0D360D558D9CC6E223704479E4E9
        F709DFF71E0D3ADA768C7BACE107D31F761D671D2F6A429AF29A469B539AFB5B
        625BBA4FCC3ED1D6EADE7AFC47DB1F0F9C343C59794AF354C969DAE982D39367
        F2CF8C9D959D7D7E2EF9DC60DBA2B67BE763CEDF6A0F6FEFBA1074E1D245FF8B
        E73BBC3BCE5CF2B874F2B2DBE51357B8579AAF3A5F6DEA74EA3CFE93D34FC7BB
        9CBB9AAEB95C6BB9EE7ABDB57B66F7E91B9E37CEDDF4BD79F116FFD6D59E393D
        DDBDF37A6FF7C5F7F5DF16DD7E7227FDCECBBBD97727EEADBC4FBC5FF440ED41
        D943DD87D53F5BFEDCD8EFDC7F6AC077A0F3D1DC47F7068583CFFE91F58F0F43
        058F998FCB860D86EB9E383E3939E23F72FDE9FCA743CF64CF269E17FEA2FECB
        AE17162F7EF8D5EBD7CED198D1A197F29793BF6D7CA5FDEAC0EB19AFDBC6C2C6
        1EBEC97833315EF456FBEDC177DC771DEFA3DF0F4FE47C207F28FF68F9B1F553
        D0A7FB93199393FF040398F3FC63332DDB0000062C4944415478DAED956D4C53
        5718C79FDB7BDB020381BED0F2AA8561A1608903819868D4C4B9CC0D8D6E401C
        6AA2C9161759947DD85C66962DD1254BFCB0258B1F36239A4D27EA964C51177C
        C992259BF2D217E4A52055DE5A288542A550A0F7DE3DE7F6162988D37DF10BA7
        39CDBDE79EF3FC7FE739CFF31C8AE77978998D5A025802980B505151F1E40345
        C1D8980706065D9B57E896EB3896335140D7E37058E325BCD0C3C6D026C7B2EB
        243C954D01DF0A34F39784932C10AFA9A9990FB07BAE19989A9EDC97929A724C
        AD522B1D4EA7DDD1E738C030D23BCF02C037E002EC8EB4E4D4139A044DB26BC8
        D5DFD7D75F2901FAEA7F02ECDA553EFB3C3313D06764A69B73727222267C3E58
        B62C16CC66D36887ED41A9542AAB7B1A001167A703E5B9869C6A43B641FE78DC
        0B91115160329B9CDD8F7A8D0CCDB89F09505ABE73F69965D9D2F5EBD65F0074
        F9F8F83844454682569B8810164F87ADB32C041102088AB3E5C69CDCEA6C43B6
        DCE17080DF3F090A8512463DA3F0CFDDBB8572B9BC3E0CE0978BF300CADE9D7D
        0E0402FAAC2CBD39DB9015E1743A853134005A4D22984C268FADBDA34CCAC8EA
        781A5D2EE1849DE7E51AAB73720D72327F7A7A9A441224252541434383E361DF
        2323C330C373012EFD7C791E4069695820F114575550907F223939195CAEC159
        084D82169A9A9A3CED6DB6325AC6D4CDB03365AB57E59D59B52A57EE1C088A93
        58D568B4D0D1D1196869B1BEC7D054CDFC18387FFEB7C50148E32816388E3D52
        5C58743C25250530A0E64068A0FE5EC35073CBFD0B05F9F9FBF2F2F2A20606C5
        9DA33A81B4B5DB0226AB65BF8CA1CED214C00B03F0E85A8E6781E7F823C545C5
        C7D3D2D2604884884088F878050C0EBA2031510BEE61F7AC78825A036D6DED28
        6EDE4FD3F45906C724C085D926697EEEDCAF0BEB009E7DD0FDA44B3821B6398E
        23EF47D616AF3D9EB63C08410C104FC4C4C480D7EBC5AC9911C4D5AA04686D6D
        0B34594C82389947A30D02203CD334482414F87C9370E9D2957000A3D1083A9D
        0E7717012CEE9CA283C9251CC713882F535353A59ED111C12069128944008E8F
        8B87F676DBE3464BD3419A66CE86BE1300292DA436C6925BE8767B374C4EFAC3
        01C8028C5448C474DBFAD656F08C8D40800BCC0A616A924D5EDEB279CB8EA969
        3FF8262680A6B0C4E09AD8D865E01BF7C1CD5BB74FCAE4F20F436B28F2A37870
        F6F7437DBD0953D31F16E80B004893C9645075B80ABAFBBA612A30258C07CB2B
        B7DBA0CFFA2E252539CEE3F1E018274880004E634CC403D608A7BDEBE15E8696
        D6053F056D36363642575757788C2D06108945A7F26025F43A7A050071F77B72
        B30CA774E9E98C1B6380E383674A606730F8881946CA805AA906CC0C4F57A75D
        2856219B66B31953B2E3C501FC013FD9B9209E9191C19054E4311E24B404E2E2
        E2108C13761FF4082F1CA14AA506ABD5EAB1773D128A1531FBBF007A1C3D30E9
        9FD86340F19599AF3243EE2121186914572A54D0DBD737D5DED17EDD986BDCAE
        D1686078987C1721942AB0582C9E87F66EC1132F0410151505870F1D864E7BE7
        2EDD8AE567325766326E14277369C1B8121CFD0EEEDA8D1B1F8C0C0FFFA8542A
        3F2D2929F93A41AD862790B490152D2DAD9E9E9EDE9DCDD6FB779E1B00AF5C78
        E3CDD753F5FA9516BD5E1F3F8C45861825E9A6C49D0D0C0CB0B5D76A2BEFFDDD
        7012D7938B9E2B5E5B7474FBB66D5F29940A08CD279E502814781CCD9D7F5CAF
        2B4000EFF300903F5E97AE7BE7C081F72FCAE432BC0D1FE399931D2950DCC9D5
        D65EAB6AA86FFA16D33B5A48738C51EC9345C56BBE28D9F6F651224A6282E558
        88C56BDC3BE685D3A7CF6CE87A60FF93948D5071416D7E3E00116788D1A4A4C4
        A2DD7B2B6EA5A7EB68E13AC663C12B96BFF2FBD5CFCD26EBF738472A1A0A1914
        7A4161FE6708F131C98689491F44BF120DAD6D6DE367AB7FDA8850369C832513
        A6457DFE691E909130C067664D61C1271B376DF808F35B866EF7DEBE7DE71BAB
        B9F994E8A569B187AE19A9D825056B5E3BB469D3C64A954A153138E49AB85977
        EB98C564FD4184F461F78B008B1E815C0461D013ABA363A2D346463C9DEE2177
        AB3875266464CE91CE5D4769B59ABCD8B8D8CCD1D1D18EC10197459C3B25AEE3
        9E198462A345F7B2E282D03B278E2DD69EB62E142761EB1600BC8CB604B004F0
        2F49B488EE35C90E9C0000000049454E44AE426082}
      ExplicitLeft = 171
      ExplicitTop = 6
    end
    object sbUnWrap: TPngSpeedButton
      AlignWithMargins = True
      Left = 744
      Top = 5
      Width = 50
      Height = 38
      Hint = #1056#1072#1079#1074#1077#1088#1085#1091#1090#1100' '#1074#1089#1077
      Margins.Top = 2
      Margins.Bottom = 1
      Align = alRight
      Flat = True
      ParentShowHint = False
      ShowHint = True
      Visible = False
      OnClick = sbUnWrapClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D494844520000002000000020080300000044A48A
        C60000000373424954080808DBE14FE00000004B504C5445767676A8D5E4CC33
        00DF7F5FD8D8D8BCBCBCD24C1FB5B5B5C4C4C4A8A8A8999999F0F0F0ECB29FD9
        653FE5997FF5D9CF858585CF3F0FD5592FE9E9E9ADADADDEDEDE7D7D7D8F8F8F
        FFFFFFEC8519E40000001974524E53FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0001340AB7000000097048597300000B1200000B1201D2DD7E
        FC0000001C74455874536F6674776172650041646F62652046697265776F726B
        732043533571B5E33600000016744558744372656174696F6E2054696D650031
        322F32302F31395FEF2C52000000BA4944415478DAD591DD0E82300C855B073A
        407E0C31EEFDDFCE440338C630801882E0320646E28DBDE94EF6AD393D4306CB
        850C783D2A5FFA532071EE8F11B8D04807AC2D2AAF2AE9110DA04DACEA04B511
        B88383AAAF10AF026EBD08D64F58026C2B54759DEB5B1CB1B1DF927B42CB1259
        BA71F910A5156493241954A21D00420C7FD177011E18EBD7C059BF3E7D0BFCB1
        C9A2EB25D0AEB9264092F425A2C6319AE465DB9F6931E721B73308ABFD824949
        C0305ECD61B63E024FF8A73F21C39CE4340000000049454E44AE426082}
      ExplicitLeft = 760
    end
    object sbWrap: TPngSpeedButton
      AlignWithMargins = True
      Left = 688
      Top = 5
      Width = 50
      Height = 38
      Hint = #1057#1074#1077#1088#1085#1091#1090#1100' '#1074#1089#1077
      Margins.Top = 2
      Margins.Bottom = 1
      Align = alRight
      Flat = True
      ParentShowHint = False
      ShowHint = True
      Visible = False
      OnClick = sbWrapClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D494844520000002000000020080300000044A48A
        C60000000373424954080808DBE14FE000000051504C5445767676A8D5E4CC33
        00DF7F5FC4C4C4AFB7BAD24C1FB5B5B5A8A8A8DEDEDE999999ECB29FD8D8D8D9
        653FBB8471E5997FF0F0F0858585CF3F0FF5D9CFD5592FBCBCBCADADAD7D7D7D
        E9E9E98F8F8FFFFFFFD2BF48210000001B74524E53FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0027340B35000000097048597300000B12
        00000B1201D2DD7EFC0000001C74455874536F6674776172650041646F626520
        46697265776F726B732043533571B5E33600000016744558744372656174696F
        6E2054696D650031322F32302F31395FEF2C52000000C44944415478DAD591DB
        0E82300C863B05160867A3097BFF7733414170113245A63044320646E28DBD58
        FBA7DF9A1E10817943048AB257DED51A038955F21E88B59D0CD8B531F8559F79
        20015AA0B741E6B72EB94B251086CD10B881BE08C884F097579803B6341CEA2A
        95A7305DB47ACB0233699788C4869DBB9DA2C161B44902F571FD024EB6E216AD
        63083806950920C719F8CC9B020A2ACE891C4B0930947622CC2B291D35C0A589
        4A309F2FA72A400C088EB2C79F037B391D7D0BFC7B9393F6117800E73C4661D2
        6063760000000049454E44AE426082}
      ExplicitLeft = 760
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'declare @role varchar(50), @isadmin bit;'
      ''
      'select @role = :role, @isadmin = :isadmin;'
      ''
      
        'select * from reports r left outer join folders f on (f.id = r.c' +
        'atalog_id) '
      
        'where (charindex(@role, access_roles)>0 or isnull(ltrim(access_r' +
        'oles),'#39#39') = '#39#39' or  isnull(@isadmin,0) = 1)'
      'order by report_title')
    SelectCommand.Parameters = <
      item
        Name = 'role'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'isadmin'
        DataType = ftBoolean
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update reports'
      'set'
      '  report_name = :report_name,'
      '  report_title = :report_title,'
      '  catalog_id = :catalog_id,'
      '  group_fields = :group_fields,'
      '  sql_text = :sql_text,'
      '  access_roles =:access_roles, '
      '  tree_parent_id_name =:tree_parent_id_name, '
      '  tree_child_id_name =:tree_child_id_name '
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'report_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'report_title'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'catalog_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'group_fields'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2000
        Value = Null
      end
      item
        Name = 'sql_text'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'access_roles'
        Size = -1
        Value = Null
      end
      item
        Name = 'tree_parent_id_name'
        Size = -1
        Value = Null
      end
      item
        Name = 'tree_child_id_name'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into reports'
      
        '  (report_name, report_title, catalog_id, group_fields, sql_text' +
        ', access_roles, tree_parent_id_name, tree_child_id_name)'
      'values'
      
        '  (:report_name, :report_title, :catalog_id, :group_fields, :sql' +
        '_text, :access_roles, :tree_parent_id_name, :tree_child_id_name)'
      ''
      '')
    InsertCommand.Parameters = <
      item
        Name = 'report_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'report_title'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'catalog_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'group_fields'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2000
        Value = Null
      end
      item
        Name = 'sql_text'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'access_roles'
        Size = -1
        Value = Null
      end
      item
        Name = 'tree_parent_id_name'
        Size = -1
        Value = Null
      end
      item
        Name = 'tree_child_id_name'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from reports where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select * from reports r left outer join folders f on (f.id = r.c' +
        'atalog_id) where r.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 72
    Top = 176
  end
  inherited meData: TMemTableEh
    Left = 120
    Top = 176
  end
  inherited dsData: TDataSource
    Left = 176
    Top = 176
  end
  inherited qrAux: TADOQuery
    Left = 376
    Top = 352
  end
  inherited drvForms: TADODataDriverEh
    Left = 596
  end
  inherited meForms: TMemTableEh
    Left = 644
  end
  inherited dsForms: TDataSource
    Left = 692
  end
  inherited al: TActionList
    Left = 400
    Top = 104
  end
  inherited exReport: TEXLReport
    DataSet = meReport
    Left = 502
  end
  object dsReport: TDataSource
    DataSet = meReport
    Left = 424
    Top = 183
  end
  object qrReport: TADOQuery
    Connection = dm.connMain
    Parameters = <>
    Left = 368
    Top = 183
  end
  object meReport: TMemTableEh
    Params = <>
    Left = 384
    Top = 263
  end
end
