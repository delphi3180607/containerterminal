﻿unit editloadplan;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, DBGridEh, DBLookupEh;

type
  TFormEditLoadPlan = class(TFormEdit)
    Label1: TLabel;
    dtPlanDate: TDBDateTimeEditEh;
    Label2: TLabel;
    edNote: TDBEditEh;
    Label3: TLabel;
    edTrainNumber: TDBEditEh;
    luDispatchKind: TDBLookupComboboxEh;
    Label8: TLabel;
    Label10: TLabel;
    edSheetServe: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditLoadPlan: TFormEditLoadPlan;

implementation

{$R *.dfm}

end.
