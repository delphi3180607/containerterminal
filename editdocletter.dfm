﻿inherited FormEditDocLetter: TFormEditDocLetter
  Caption = #1055#1080#1089#1100#1084#1086
  ClientHeight = 454
  ClientWidth = 698
  ExplicitWidth = 704
  ExplicitHeight = 482
  PixelsPerInch = 96
  TextHeight = 16
  object edText: TDBMemoEh [0]
    Left = 8
    Top = 147
    Width = 681
    Height = 260
    ControlLabel.Width = 29
    ControlLabel.Height = 13
    ControlLabel.Caption = #1058#1077#1082#1089#1090
    ControlLabel.Visible = True
    AutoSize = False
    DataField = 'text'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object luSender: TDBSQLLookUp [1]
    Left = 11
    Top = 27
    Width = 505
    Height = 24
    ControlLabel.Width = 68
    ControlLabel.Height = 13
    ControlLabel.Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
    ControlLabel.Visible = True
    DataField = 'sender_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    SqlSet = dm.ssCounteragents
  end
  object dtRegister: TDBDateTimeEditEh [2]
    Left = 11
    Top = 85
    Width = 201
    Height = 24
    ControlLabel.Width = 64
    ControlLabel.Height = 13
    ControlLabel.Caption = #1044#1072#1090#1072' '#1087#1080#1089#1100#1084#1072
    ControlLabel.Visible = True
    DataField = 'dateregister'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 0
    Visible = True
  end
  inherited plBottom: TPanel
    Top = 413
    Width = 698
    TabOrder = 3
    ExplicitTop = 413
    ExplicitWidth = 698
    inherited btnCancel: TButton
      Left = 582
      ExplicitLeft = 582
    end
    inherited btnOk: TButton
      Left = 463
      ExplicitLeft = 463
    end
  end
  inherited dsLocal: TDataSource
    Left = 147
    Top = 255
  end
  inherited qrAux: TADOQuery
    Left = 219
    Top = 263
  end
end
