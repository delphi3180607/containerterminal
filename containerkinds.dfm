﻿inherited FormContainerKinds: TFormContainerKinds
  Caption = #1058#1080#1087#1099' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074
  ExplicitWidth = 836
  ExplicitHeight = 549
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076' '#1090#1080#1087#1072
          Width = 179
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1090#1080#1087#1072
          Width = 313
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'footsize'
          Footers = <>
          Title.Caption = #1056#1072#1079#1084#1077#1088', '#1092#1091#1090
          Width = 127
        end
        item
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isdefault'
          Footers = <>
          Title.Caption = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
          Width = 114
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from containerkinds')
    UpdateCommand.CommandText.Strings = (
      'update containerkinds '
      
        'set code = :code, name = :name, footsize = :footsize, isdefault ' +
        '= :isdefault'
      'where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'footsize'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isdefault'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      
        'insert into containerkinds (code, name, footsize, isdefault) val' +
        'ues ( :code, :name, :footsize, :isdefault)')
    InsertCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'footsize'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isdefault'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from containerkinds where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
end
