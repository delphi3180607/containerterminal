﻿unit DispatchMonitor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, DBSQLLookUp, EXLReportExcelTLB,
  EXLReportBand, EXLReport, System.Actions, Vcl.ActnList, MemTableEh,
  DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton;

type
  TFormDispatchMonitor = class(TFormGrid)
    Panel3: TPanel;
    luCustomer: TDBSQLLookUp;
    Panel1: TPanel;
    edCnum: TDBEditEh;
    Panel2: TPanel;
    edPnum: TDBEditEh;
    Panel4: TPanel;
    edDispatchNumber: TDBEditEh;
    Panel5: TPanel;
    edSealNumber: TDBEditEh;
    Panel6: TPanel;
    dtStart: TDBDateTimeEditEh;
    Panel7: TPanel;
    edGU12: TDBEditEh;
    dgHistory: TDBGridEh;
    drvHistory: TADODataDriverEh;
    meHistory: TMemTableEh;
    dsHistory: TDataSource;
    SplitterHor: TSplitter;
    Panel8: TPanel;
    Panel9: TPanel;
    sbRun: TButton;
    procedure sbRunClick(Sender: TObject);
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure meDataAfterOpen(DataSet: TDataSet);
    procedure edCnumKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormDispatchMonitor: TFormDispatchMonitor;

implementation

{$R *.dfm}

procedure TFormDispatchMonitor.edCnumKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin

  if Key = 13 then
  begin
    sbRunClick(nil);
  end;

end;

procedure TFormDispatchMonitor.Init;
begin
  meData.Close;
  if dtStart.Value = null then dtStart.Value := IncMonth(now(),-3);
end;


procedure TFormDispatchMonitor.meDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  meDataAfterScroll(meData);
end;

procedure TFormDispatchMonitor.meDataAfterScroll(DataSet: TDataSet);
begin
  meHistory.Close;
  drvHistory.SelectCommand.Parameters.ParamByName('object_id').Value := meData.FieldByName('id').AsInteger;
  meHistory.Open;
  Screen.Cursor := crDefault;
end;

procedure TFormDispatchMonitor.sbRunClick(Sender: TObject);
begin

  Screen.Cursor := crHourGlass;
  meData.Close;

  drvData.SelectCommand.Parameters.ParamByName('startdate').Value := self.dtStart.Value;
  drvData.SelectCommand.Parameters.ParamByName('contragentid').Value := self.luCustomer.KeyValue;
  drvData.SelectCommand.Parameters.ParamByName('pnum').Value := self.edPnum.Text.Trim;
  drvData.SelectCommand.Parameters.ParamByName('cnum').Value := self.edCnum.Text.Trim;
  drvData.SelectCommand.Parameters.ParamByName('seal_number').Value := self.edSealNumber.Text.Trim;
  drvData.SelectCommand.Parameters.ParamByName('gu12_number').Value := self.edGU12.Text.Trim;
  drvData.SelectCommand.Parameters.ParamByName('dispatch_number').Value := self.edDispatchNumber.Text.Trim;

  meData.Open;
  Screen.Cursor := crDefault;

end;

end.
