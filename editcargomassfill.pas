﻿unit editcargomassfill;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, editcargosoncarriages, DBGridEh,
  Data.DB, Data.Win.ADODB, DBCtrlsEh, DBLookupEh, Vcl.StdCtrls, Vcl.Mask,
  Vcl.ExtCtrls, Vcl.Buttons, DBSQLLookUp, PngSpeedButton;

type
  TFormEditCargoMassFill = class(TFormEditCargosOnCarriages)
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditCargoMassFill: TFormEditCargoMassFill;

implementation

{$R *.dfm}

procedure TFormEditCargoMassFill.btnOkClick(Sender: TObject);
begin
  self.ModalResult := mrOk;
end;

end.
