﻿unit Splash;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Imaging.pngimage,
  Vcl.Imaging.jpeg, Vcl.StdCtrls, functions, Vcl.ComCtrls;

type
  TFormSplash = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    plProgress: TPanel;
    imgProgress: TImage;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    procedure IncreasePG;
  end;

var
  FormSplash: TFormSplash;

implementation

{$R *.dfm}

procedure TFormSplash.FormShow(Sender: TObject);
begin
  Label2.Caption := GetMyVersion();
end;

procedure TFormSplash.IncreasePG;
begin
  //Image2.Left := Image2.Left +1;
  imgProgress.Left := imgProgress.Left-3;
  Application.ProcessMessages;
end;


end.
