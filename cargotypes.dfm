﻿inherited FormCargoTypes: TFormCargoTypes
  Caption = #1058#1080#1087#1099' '#1075#1088#1091#1079#1086#1074
  ExplicitWidth = 320
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076
          Width = 192
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 251
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from cargotypes order by code')
    UpdateCommand.CommandText.Strings = (
      'update cargotypes'
      'set'
      '  code = :code,'
      '  name = :name'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'code'
        Size = -1
        Value = Null
      end
      item
        Name = 'name'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into cargotypes'
      '  (code, name)'
      'values'
      '  (:code, :name)')
    InsertCommand.Parameters = <
      item
        Name = 'code'
        Size = -1
        Value = Null
      end
      item
        Name = 'name'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from cargotypes'
      'where'
      '  id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from cargotypes where  id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
end
