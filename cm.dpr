﻿program CM;

uses
  Vcl.Forms,
  Vcl.Controls,
  SysUtils,
  main in 'main.pas' {FormMain},
  Vcl.Themes,
  Vcl.Styles,
  tool in 'tool.pas' {FormTool},
  Splash in 'Splash.pas' {FormSplash},
  dmu in 'dmu.pas' {dm: TDataModule},
  login in 'login.pas' {FormLogin},
  edit in 'edit.pas' {FormEdit},
  editcolumn in 'editcolumn.pas' {FormEditColumn},
  tablelinks in 'tablelinks.pas' {FormEditTableLinks},
  grid in 'grid.pas' {FormGrid},
  users in 'users.pas' {FormUsers},
  DocFlowRules in 'DocFlowRules.pas' {FormDocFlowRules},
  counteragents in 'counteragents.pas' {FormCounteragents},
  servicekinds in 'servicekinds.pas' {FormServiceKinds},
  carriagekinds in 'carriagekinds.pas' {FormCarriageKinds},
  containerkinds in 'containerkinds.pas' {FormContainerKinds},
  stations in 'stations.pas' {FormStations},
  edituser in 'edituser.pas' {FormEditUser},
  editstation in 'editstation.pas' {FormEditStation},
  editcounteragent in 'editcounteragent.pas' {FormEditCounteragent},
  editservice in 'editservice.pas' {FormEditService},
  QYN in 'QYN.pas' {FormQYN},
  Functions in 'Functions.pas',
  editcontainerkind in 'editcontainerkind.pas' {FormEditContainerKind},
  editcarriagekinds in 'editcarriagekinds.pas' {FormEditCarriageKind},
  statekinds in 'statekinds.pas' {FormStateKinds},
  editstatekind in 'editstatekind.pas' {FormEditStateKind},
  editoperationkind in 'editoperationkind.pas' {FormEditOperationKind},
  operationkinds in 'operationkinds.pas' {FormOperationKinds},
  persons in 'persons.pas' {FormPersons},
  operationedit in 'operationedit.pas' {FormEditOperation},
  doctypeselect in 'doctypeselect.pas' {FormDocTypeSelect},
  editperson in 'editperson.pas' {FormEditPerson},
  editoperationrout in 'editoperationrout.pas' {FormEditOperationRoute},
  editcargosoncarriages in 'editcargosoncarriages.pas' {FormEditCargosOnCarriages},
  selectfolder in 'selectfolder.pas' {FormSelectFolder},
  editcatalog in 'editcatalog.pas' {FormEditCatalog},
  containers_old in 'containers_old.pas' {FormContainersOld},
  carriages_old in 'carriages_old.pas' {FormCarriagesOld},
  Invoices in 'Invoices.pas' {FormInvoices},
  trains in 'trains.pas' {FormTrains},
  InvoiceEdit in 'InvoiceEdit.pas' {FormEditInvoice},
  InvoiceSpecEdit in 'InvoiceSpecEdit.pas' {FormEditInvoiceSpec},
  Contracts in 'Contracts.pas' {FormContracts},
  editcontract in 'editcontract.pas' {FormEditContract},
  editcontainer in 'editcontainer.pas' {FormEditContainer},
  editcarriage in 'editcarriage.pas' {FormEditCarriage},
  stateshistory in 'stateshistory.pas' {FormStatesHistory},
  emailaccounts in 'emailaccounts.pas' {FormEmailAccounts},
  editemailaccount in 'editemailaccount.pas' {FormEditEmailAccount},
  editdocroute2objects in 'editdocroute2objects.pas' {FormEditDocroute2Object},
  editorder in 'editorder.pas' {FormEditOrder},
  cargodocs in 'cargodocs.pas' {FormCargoDocs},
  menu in 'menu.pas' {FormMenu},
  editmenu in 'editmenu.pas' {FormEditMenu},
  editdocspecservice in 'editdocspecservice.pas' {FormEditDocSpecService},
  cargosheet in 'cargosheet.pas' {FormCargoSheet},
  docflow in 'docflow.pas' {FormDocFlow},
  deliverytypes in 'deliverytypes.pas' {FormDeliveryTypes},
  cargotypes in 'cargotypes.pas' {FormCargoTypes},
  editcargotype in 'editcargotype.pas' {FormEditCargoType},
  editdeliverytype in 'editdeliverytype.pas' {FormEditDeliveryType},
  editowner in 'editowner.pas' {FormEditOwner},
  docgenerate in 'docgenerate.pas' {FormDocGenerate},
  doctypes in 'doctypes.pas' {FormDocTypes},
  editdoctype in 'editdoctype.pas' {FormEditDocType},
  objects in 'objects.pas' {FormObjects},
  carriages in 'carriages.pas' {FormCarriages},
  containers in 'containers.pas' {FormContainers},
  cargos in 'cargos.pas' {FormCargos},
  editcargo in 'editcargo.pas' {FormEditCargo},
  selectobjecttype in 'selectobjecttype.pas' {FormSelectObjectType},
  addresscontainers in 'addresscontainers.pas' {FormAddressContainers},
  addresscarriages in 'addresscarriages.pas' {FormAddressCarriages},
  editcodename in 'editcodename.pas' {FormEditCodeName},
  unloaddocs in 'unloaddocs.pas' {FormUnloadDocs},
  objectmove in 'objectmove.pas' {FormObjectMove},
  inspects in 'inspects.pas' {FormInspects},
  settings in 'settings.pas' {FormSettings},
  docroutes in 'docroutes.pas' {FormDocRoutes},
  editdocroute in 'editdocroute.pas' {FormEditDocRoute},
  objectstatekinds in 'objectstatekinds.pas' {FormObjectStateKinds},
  restrictions in 'restrictions.pas' {FormRestrictions},
  tasksrestrictcontrol in 'tasksrestrictcontrol.pas' {FormTasksRestrictControl},
  tasks in 'tasks.pas' {FormTasks},
  tasksincome in 'tasksincome.pas' {FormTasksIncome},
  transformdocuments in 'transformdocuments.pas' {FormTransformDocuments},
  editunloaddoc in 'editunloaddoc.pas' {FormEditUnloadDoc},
  childdocs in 'childdocs.pas' {FormChildDocs},
  removaldocs in 'removaldocs.pas' {FormRemovalDocs},
  editremovaldoc in 'editremovaldoc.pas' {FormEditRemovalDoc},
  editcargomassfill in 'editcargomassfill.pas' {FormEditCargoMassFill},
  orders in 'orders.pas' {FormDocOrders},
  load in 'load.pas' {FormLoads},
  directions in 'directions.pas' {FormDirections},
  editorderspec in 'editorderspec.pas' {FormEditOrderSpec},
  editload in 'editload.pas' {FormEditLoad},
  dispatch in 'dispatch.pas' {FormDocDispatch},
  editdispatch in 'editdispatch.pas' {FormEditDispatch},
  dispatchkinds in 'dispatchkinds.pas' {FormDispatchKinds},
  reports in 'reports.pas' {FormReports},
  editreport in 'editreport.pas' {FormEditReport},
  editsql in 'editsql.pas' {FormEditSql},
  AlertQue in 'AlertQue.pas' {FormAlertQue},
  report1 in 'report1.pas' {FormReport1},
  filter in 'filter.pas' {FormFilter},
  ObjectsInProcess in 'ObjectsInProcess.pas' {FormObjectsInProcess},
  StorePrices in 'StorePrices.pas' {FormStorePrices},
  SPTypes in 'SPTypes.pas' {FormEditSPTypes},
  SPCust in 'SPCust.pas' {FormEditSPCusts},
  SPDays in 'SPDays.pas' {FormEditSPDays},
  ServicePrices in 'ServicePrices.pas' {FormServicePrices},
  ContainersOnGo in 'ContainersOnGo.pas' {FormContainersOnGo},
  CarriagesOnGo in 'CarriagesOnGo.pas' {FormCarriagesOnGo},
  CustomerOperations in 'CustomerOperations.pas' {FormCustomerOperations},
  docemptyincome in 'docemptyincome.pas' {FormDocEmptyIncome},
  ExtraServices in 'ExtraServices.pas' {FormExtraServices},
  EditDocEmptyIncome in 'EditDocEmptyIncome.pas' {FormEditDocEmptyIncome},
  techconditions in 'techconditions.pas' {FormTechConditions},
  dateexecution in 'dateexecution.pas' {FormDateExecution},
  editextraservice in 'editextraservice.pas' {FormEditExtraService},
  ContainersList in 'ContainersList.pas' {FormContainersList},
  CarriagesList in 'CarriagesList.pas' {FormCarriagesList},
  Import in 'Import.pas' {FormImport},
  externaldatalinks in 'externaldatalinks.pas' {FormExternalDatalinks},
  editcontractspec in 'editcontractspec.pas' {FormEditContractSpec},
  EditDocTemplate in 'EditDocTemplate.pas' {FormEditDocTemplate},
  selectdoctemplate in 'selectdoctemplate.pas' {FormSelectDocTemplate},
  SelectDocForm in 'SelectDocForm.pas' {FormSelectDocForm},
  editdocinspect in 'editdocinspect.pas' {FormEditInspect},
  containersinspect in 'containersinspect.pas' {FormContainersInspects},
  carriagesinspect in 'carriagesinspect.pas' {FormCarriagesInspect},
  selectobjects in 'selectobjects.pas' {FormSelectObjects},
  copypasteobjects in 'copypasteobjects.pas' {FormCopyPasteObjects},
  selecttechcondition in 'selecttechcondition.pas' {FormSelectTechCondition},
  editspecinspect in 'editspecinspect.pas' {FormEditSpecInspect},
  edittechcondition in 'edittechcondition.pas' {FormEditTechCondition},
  editexternaldatalink in 'editexternaldatalink.pas' {FormEditExternalDataLink},
  selectdeliverytype in 'selectdeliverytype.pas' {FormSelectDeliveryType},
  setprefix in 'setprefix.pas' {FormSetPrefix},
  filterlite in 'filterlite.pas' {FormFilterLite},
  AddSelect in 'AddSelect.pas' {FormAddSelect},
  EditContainerInDoc in 'EditContainerInDoc.pas' {FormEditContainerInDoc},
  tariftypes in 'tariftypes.pas' {FormTarifTypes},
  edittariftype in 'edittariftype.pas' {FormEditTarifType},
  editdatealert in 'editdatealert.pas' {FormDateAlert},
  EditFactArrivalGo in 'EditFactArrivalGo.pas' {FormEditFactArrivalGo},
  importload in 'importload.pas' {FormImportLoad},
  editloadplan in 'editloadplan.pas' {FormEditLoadPlan},
  loadplan in 'loadplan.pas' {FormLoadPlan},
  editdocincomespec in 'editdocincomespec.pas' {FormEditInComeSpec},
  editdateregister in 'editdateregister.pas' {FormEditDateRegister},
  mtukinds in 'mtukinds.pas' {FormMtuKinds},
  picturekinds in 'picturekinds.pas' {FormPictureKinds},
  editdispatchspec in 'editdispatchspec.pas' {FormEditDispatchSpec},
  sqlquery in 'sqlquery.pas' {FormSQLQuery},
  seek in 'seek.pas' {FormSeek},
  ExtraInfo in 'ExtraInfo.pas' {FormExtraInfo},
  DocRestrictions in 'DocRestrictions.pas' {FormDocRestrictions},
  docletters in 'docletters.pas' {FormDocLetters},
  linkedinvoices in 'linkedinvoices.pas' {FormDocLinkedInvoices},
  editdocrestrict in 'editdocrestrict.pas' {FormEditDocRestriction},
  editdocletter in 'editdocletter.pas' {FormEditDocLetter},
  editlinkedinvoice in 'editlinkedinvoice.pas' {FormEditLinkedInvoice},
  Catalogs in 'Catalogs.pas' {FormCatalogs},
  carriagemodels in 'carriagemodels.pas' {FormCarriageModels},
  editcarriagemodel in 'editcarriagemodel.pas' {FormEditCarriageModel},
  EditPlaceContainers in 'EditPlaceContainers.pas' {FormEditPlaceContainers},
  loadschess in 'loadschess.pas' {FormLoadsChess},
  editreportparam in 'editreportparam.pas' {FormEditReportParam},
  ParamsFormUnit in 'ParamsFormUnit.pas' {ParamsForm},
  selecttemplate in 'selecttemplate.pas' {FormSelectTemplate},
  Clock in 'Clock.pas' {FormClock},
  ContainersInOrders in 'ContainersInOrders.pas' {FormContainersInOrders},
  ObjectStatesHistory in 'ObjectStatesHistory.pas' {FormObjectStatesHistory},
  GroupCounteragents in 'GroupCounteragents.pas' {FormGroupCounteragents},
  EditCheck in 'EditCheck.pas' {FormEditCheck},
  NoteFilter in 'NoteFilter.pas' {FormNoteFilter},
  docfeed in 'docfeed.pas' {FormDocFeed},
  EditDocFeed in 'EditDocFeed.pas' {FormEditDocFeed},
  importdocfeed in 'importdocfeed.pas' {FormImportDocFeed},
  importcargosheet in 'importcargosheet.pas' {FormImportCargoSheet},
  setprefixshort in 'setprefixshort.pas' {FormSetPrefixShort},
  unloadplan in 'unloadplan.pas' {FormUnLoadPlan},
  mtu2pictures in 'mtu2pictures.pas' {FormMtu2Pictures},
  editmtu2picture in 'editmtu2picture.pas' {FormMtu2Picture},
  editinspection in 'editinspection.pas' {FormEditInspection},
  editcarincome in 'editcarincome.pas' {FormEditCarIncome},
  unloadfronts in 'unloadfronts.pas' {FormUnloadFronts},
  editordernumber in 'editordernumber.pas' {FormEditOrderNumber},
  editmtuandpicture in 'editmtuandpicture.pas' {FormEditMtuAndPicture},
  editcarmodel in 'editcarmodel.pas' {FormEditCarModel},
  editreturndoc in 'editreturndoc.pas' {FormEditReturnDoc},
  DispatchDocsList in 'DispatchDocsList.pas' {FormDispatchDocsList},
  log in 'log.pas' {FormLog},
  roles in 'roles.pas' {FormRoles},
  selectcounteragent in 'selectcounteragent.pas' {FormSelectCounteragent},
  SelectTrainNum in 'SelectTrainNum.pas' {FormSelectTrainNum},
  placecarriages in 'placecarriages.pas' {FormPlaceCarriages},
  paths in 'paths.pas' {FormPaths},
  SelectPath in 'SelectPath.pas' {FormSelectPath},
  ChangeCarriages in 'ChangeCarriages.pas' {FormChangeCarriages},
  setcheck in 'setcheck.pas' {FormSetCheck},
  EditMtuKind in 'EditMtuKind.pas' {FormEditMtuKind},
  ReportByCounteragent in 'ReportByCounteragent.pas' {FormReportByCounteragent},
  About in 'About.pas' {FormAbout},
  SetListCarriages in 'SetListCarriages.pas' {FormSetListCarriages},
  speedmove in 'speedmove.pas' {FormSpeedMove},
  filterloads in 'filterloads.pas' {FormFilterLoads},
  EditReceipt in 'EditReceipt.pas' {FormEditReceipt},
  ReplaceContainer in 'ReplaceContainer.pas' {FormEdit2},
  FilterChessExtra in 'FilterChessExtra.pas' {FormFilterChessExtra},
  date in 'date.pas' {FormDate},
  SetMark in 'SetMark.pas' {FormSetMark},
  DispatchMonitor in 'DispatchMonitor.pas' {FormDispatchMonitor},
  raportsettings in 'raportsettings.pas' {FormRaportSettings},
  UserColumns in 'UserColumns.pas' {FormUserColumns},
  BigMessage in 'BigMessage.pas' {FormBigMessage},
  psvzhrep in 'psvzhrep.pas' {FormPsVzhRep},
  EnterpriseMonitor in 'EnterpriseMonitor.pas' {FormEnterpriseMonitor},
  MissingValues in 'MissingValues.pas' {FormMissingValues},
  editrole in 'editrole.pas' {FormEditRole},
  useralerts in 'useralerts.pas' {FormUserAlerts},
  UserAlertsPopupWindow in 'UserAlertsPopupWindow.pas' {FormAlertPopupWindow},
  BigMessageReaded in 'BigMessageReaded.pas' {FormBigMessReaded},
  edituseralert in 'edituseralert.pas' {FormEditUserAlert},
  DispatchInfo in 'DispatchInfo.pas' {FormDispatchInfo},
  payinfo in 'payinfo.pas' {FormPayInfo},
  dlvallow in 'dlvallow.pas' {FormDlvAllow},
  setguard in 'setguard.pas' {FormSetGuard},
  StartChecks in 'StartChecks.pas' {FormStartChecks},
  EditStartCheck in 'EditStartCheck.pas' {FormEditStartCheck},
  start in 'start.pas' {FormStart},
  editconnection in 'editconnection.pas' {FormEditConnection},
  adddriver in 'adddriver.pas' {FormAddDriver},
  ShippingOptions in 'ShippingOptions.pas' {FormShippingOptions},
  billed in 'billed.pas' {FormBilled},
  editbilled in 'editbilled.pas' {FormEditBilled},
  ShippingOptionsRules in 'ShippingOptionsRules.pas' {FormShippingOptionsRules},
  EditShippingOptionRule in 'EditShippingOptionRule.pas' {FromEditShippingOptionRule},
  archivefeeds in 'archivefeeds.pas' {FormArchiveFeeds},
  MoveOrderSpec in 'MoveOrderSpec.pas' {FormMoveOrderSpec},
  ReportItem in 'ReportItem.pas' {FormReportItem},
  EditDeliveryComplete in 'EditDeliveryComplete.pas' {FormEditDeliveryComplete},
  Passes in 'Passes.pas' {FormPasses},
  OneTimePasses in 'OneTimePasses.pas' {FormOneTimePasses},
  PassOperations in 'PassOperations.pas' {FormPassOperations},
  EditPassOpSettings in 'EditPassOpSettings.pas' {FormEditPassOpSettings},
  EditPass in 'EditPass.pas' {FormEditPass},
  AddPassSpec in 'AddPassSpec.pas' {FormAddPassSpec},
  EditPassOpKind in 'EditPassOpKind.pas' {FormEditPassOpKind},
  PassTypeSelect in 'PassTypeSelect.pas' {FormPassTypeSelect},
  EditPassSpec1 in 'EditPassSpec1.pas' {FormEditPassSpec1},
  EditPassSpec in 'EditPassSpec.pas' {FormEditPassSpec},
  SelectPassOpKind in 'SelectPassOpKind.pas' {FormSelectPassOpKind},
  SelectPass in 'SelectPass.pas' {FormSelectPass},
  EditNote in 'EditNote.pas' {FormEditNote},
  Classifications in 'Classifications.pas' {FormClassifications},
  EditClassification in 'EditClassification.pas' {FormEditClassification},
  EditClassifValue in 'EditClassifValue.pas' {FormEditClassifValue},
  matrix in 'matrix.pas' {FormMatrix},
  matrixcoords in 'matrixcoords.pas' {FormMatrixCoords},
  editmatrixsector in 'editmatrixsector.pas' {FormEditMatrixSector},
  EditRowStack in 'EditRowStack.pas' {FormEditRowStack},
  matrixsector in 'matrixsector.pas' {FormMatrixSector},
  EditMatrixExclude in 'EditMatrixExclude.pas' {FormEditMatrixExclude},
  SelectContainer in 'SelectContainer.pas' {FormSelectContainer},
  EditOrderInWait in 'EditOrderInWait.pas' {FormEditOrderInWait},
  MatrixAssigns in 'MatrixAssigns.pas' {FormMatrixAssigns},
  RemoveQueue in 'RemoveQueue.pas' {FormRemoveQueue},
  editplug in 'editplug.pas' {FormEditPlug};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Win10IDE_Light');
  Application.Title := 'Управление контейнерным терминалом';
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TFormEditConnection, FormEditConnection);
  Application.CreateForm(TFormShippingOptionsRules, FormShippingOptionsRules);
  Application.CreateForm(TFromEditShippingOptionRule, FromEditShippingOptionRule);
  Application.CreateForm(TFormArchiveFeeds, FormArchiveFeeds);
  Application.CreateForm(TFormMoveOrderSpec, FormMoveOrderSpec);
  Application.CreateForm(TFormReportItem, FormReportItem);
  Application.CreateForm(TFormEditDeliveryComplete, FormEditDeliveryComplete);
  Application.CreateForm(TFormPasses, FormPasses);
  Application.CreateForm(TFormOneTimePasses, FormOneTimePasses);
  Application.CreateForm(TFormPassOperations, FormPassOperations);
  Application.CreateForm(TFormEditPassOpSettings, FormEditPassOpSettings);
  Application.CreateForm(TFormEditPass, FormEditPass);
  Application.CreateForm(TFormAddPassSpec, FormAddPassSpec);
  Application.CreateForm(TFormEditPassOpKind, FormEditPassOpKind);
  Application.CreateForm(TFormPassTypeSelect, FormPassTypeSelect);
  Application.CreateForm(TFormEditPassSpec1, FormEditPassSpec1);
  Application.CreateForm(TFormEditPassSpec, FormEditPassSpec);
  Application.CreateForm(TFormSelectPassOpKind, FormSelectPassOpKind);
  Application.CreateForm(TFormSelectPass, FormSelectPass);
  Application.CreateForm(TFormEditNote, FormEditNote);
  Application.CreateForm(TFormEditClassification, FormEditClassification);
  Application.CreateForm(TFormEditClassifValue, FormEditClassifValue);
  Application.CreateForm(TFormMatrix, FormMatrix);
  Application.CreateForm(TFormMatrixCoords, FormMatrixCoords);
  Application.CreateForm(TFormEditMatrixSector, FormEditMatrixSector);
  Application.CreateForm(TFormEditRowStack, FormEditRowStack);
  Application.CreateForm(TFormMatrixSector, FormMatrixSector);
  Application.CreateForm(TFormEditMatrixExclude, FormEditMatrixExclude);
  Application.CreateForm(TFormSelectContainer, FormSelectContainer);
  Application.CreateForm(TFormEditOrderInWait, FormEditOrderInWait);
  Application.CreateForm(TFormMatrixAssigns, FormMatrixAssigns);
  Application.CreateForm(TFormRemoveQueue, FormRemoveQueue);
  Application.CreateForm(TFormEditPlug, FormEditPlug);
  if trim(ParamStr(1))='config' then
  begin
    Application.ShowMainForm := false;
    Application.CreateForm(TFormEditConnection, FormEditConnection);
    FormEditConnection.ShowModal;
    if FormEditConnection.ModalResult = mrOk then
    begin
      FormMain.SaveConfig(
          FormEditConnection.edDataBase.Text,
          FormEditConnection.edServer.Text,
          FormEditConnection.edLogin.Text,
          FormEditConnection.edPassword.Text
        );
    end;
    Application.Terminate;
    Application.Run;
    exit;
  end;

  Application.CreateForm(TFormSplash, FormSplash);
  Application.CreateForm(TFormSelectTrainNum, FormSelectTrainNum);
  Application.CreateForm(TFormPaths, FormPaths);
  Application.CreateForm(TFormSelectPath, FormSelectPath);
  Application.CreateForm(TFormChangeCarriages, FormChangeCarriages);
  Application.CreateForm(TFormSetCheck, FormSetCheck);
  Application.CreateForm(TFormEditMtuKind, FormEditMtuKind);
  Application.CreateForm(TFormRaportSettings, FormRaportSettings);
  Application.CreateForm(TFormUserColumns, FormUserColumns);
  Application.CreateForm(TFormBigMessage, FormBigMessage);
  Application.CreateForm(TFormPsVzhRep, FormPsVzhRep);
  Application.CreateForm(TFormEnterpriseMonitor, FormEnterpriseMonitor);
  Application.CreateForm(TFormMissingValues, FormMissingValues);
  Application.CreateForm(TFormEditRole, FormEditRole);
  Application.CreateForm(TFormUserAlerts, FormUserAlerts);
  Application.CreateForm(TFormAlertPopupWindow, FormAlertPopupWindow);
  Application.CreateForm(TFormBigMessReaded, FormBigMessReaded);
  Application.CreateForm(TFormEditUserAlert, FormEditUserAlert);
  Application.CreateForm(TFormDispatchInfo, FormDispatchInfo);
  Application.CreateForm(TFormPayInfo, FormPayInfo);
  Application.CreateForm(TFormDlvAllow, FormDlvAllow);
  Application.CreateForm(TFormSetGuard, FormSetGuard);
  Application.CreateForm(TFormStartChecks, FormStartChecks);
  Application.CreateForm(TFormEditStartCheck, FormEditStartCheck);
  Application.CreateForm(TFormStart, FormStart);
  //Application.CreateForm(TFormReportByCounteragent, FormReportByCounteragent);
  Application.CreateForm(TFormAbout, FormAbout);
  Application.CreateForm(TFormSetListCarriages, FormSetListCarriages);
  Application.CreateForm(TFormSpeedMove, FormSpeedMove);
  Application.CreateForm(TFormFilterLoads, FormFilterLoads);
  Application.CreateForm(TFormEditReceipt, FormEditReceipt);
  Application.CreateForm(TFormEdit2, FormEdit2);
  Application.CreateForm(TFormFilterChessExtra, FormFilterChessExtra);
  Application.CreateForm(TFormDate, FormDate);
  Application.CreateForm(TFormSetMark, FormSetMark);
  FormSplash.Show;
  Application.CreateForm(TFormEditDispatch, FormEditDispatch);
  Application.CreateForm(TFormDispatchKinds, FormDispatchKinds);
  Application.CreateForm(TFormReports, FormReports);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditReport, FormEditReport);
  Application.CreateForm(TFormEditSql, FormEditSql);
  Application.CreateForm(TFormAlertQue, FormAlertQue);
  Application.CreateForm(TFormReport1, FormReport1);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormFilter, FormFilter);
  Application.CreateForm(TFormObjectsInProcess, FormObjectsInProcess);
  //Application.CreateForm(TFormStorePrices, FormStorePrices);
  Application.CreateForm(TFormImport, FormImport);
  Application.CreateForm(TFormImportCargoSheet, FormImportCargoSheet);
  Application.CreateForm(TFormEditSPTypes, FormEditSPTypes);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditSPCusts, FormEditSPCusts);
  Application.CreateForm(TFormEditSPDays, FormEditSPDays);
  Application.CreateForm(TFormServicePrices, FormServicePrices);
  Application.CreateForm(TFormContainersOnGo, FormContainersOnGo);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormCarriagesOnGo, FormCarriagesOnGo);
  Application.CreateForm(TFormEditExtraService, FormEditExtraService);
  Application.CreateForm(TFormEditDocEmptyIncome, FormEditDocEmptyIncome);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormTechConditions, FormTechConditions);
  Application.CreateForm(TFormDateExecution, FormDateExecution);
  Application.CreateForm(TFormExternalDatalinks, FormExternalDatalinks);
  Application.CreateForm(TFormEditContractSpec, FormEditContractSpec);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditDocTemplate, FormEditDocTemplate);
  Application.CreateForm(TFormSelectDocTemplate, FormSelectDocTemplate);
  Application.CreateForm(TFormSelectDocForm, FormSelectDocForm);
  Application.CreateForm(TFormEditInspect, FormEditInspect);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormSelectObjects, FormSelectObjects);
  Application.CreateForm(TFormCopyPasteObjects, FormCopyPasteObjects);
  Application.CreateForm(TFormSelectTechCondition, FormSelectTechCondition);
  Application.CreateForm(TFormEditSpecInspect, FormEditSpecInspect);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditTechCondition, FormEditTechCondition);
  Application.CreateForm(TFormEditExternalDataLink, FormEditExternalDataLink);
  Application.CreateForm(TFormSelectDeliveryType, FormSelectDeliveryType);
  Application.CreateForm(TFormSetPrefix, FormSetPrefix);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormFilterLite, FormFilterLite);
  Application.CreateForm(TFormAddSelect, FormAddSelect);
  Application.CreateForm(TFormEditContainerInDoc, FormEditContainerInDoc);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormTarifTypes, FormTarifTypes);
  Application.CreateForm(TFormEditTarifType, FormEditTarifType);
  Application.CreateForm(TFormDateAlert, FormDateAlert);
  Application.CreateForm(TFormEditFactArrivalGo, FormEditFactArrivalGo);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormImportLoad, FormImportLoad);
  Application.CreateForm(TFormEditLoadPlan, FormEditLoadPlan);
  Application.CreateForm(TFormLoadPlan, FormLoadPlan);
  Application.CreateForm(TFormEditInComeSpec, FormEditInComeSpec);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditDateRegister, FormEditDateRegister);
  Application.CreateForm(TFormMtuKinds, FormMtuKinds);
  Application.CreateForm(TFormPictureKinds, FormPictureKinds);
  Application.CreateForm(TFormEditDispatchSpec, FormEditDispatchSpec);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormSQLQuery, FormSQLQuery);
  Application.CreateForm(TFormSeek, FormSeek);
  Application.CreateForm(TFormExtraInfo, FormExtraInfo);
  Application.CreateForm(TFormDocRestrictions, FormDocRestrictions);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormDocLetters, FormDocLetters);
  Application.CreateForm(TFormDocLinkedInvoices, FormDocLinkedInvoices);
  Application.CreateForm(TFormEditDocRestriction, FormEditDocRestriction);
  Application.CreateForm(TFormEditDocLetter, FormEditDocLetter);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditLinkedInvoice, FormEditLinkedInvoice);
  Application.CreateForm(TFormCatalogs, FormCatalogs);
  Application.CreateForm(TFormCarriageModels, FormCarriageModels);
  Application.CreateForm(TFormEditCarriageModel, FormEditCarriageModel);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditPlaceContainers, FormEditPlaceContainers);
  //Application.CreateForm(TFormLoadsChess, FormLoadsChess);
  Application.CreateForm(TFormEditReportParam, FormEditReportParam);
  Application.CreateForm(TParamsForm, ParamsForm);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormSelectTemplate, FormSelectTemplate);
  Application.CreateForm(TFormClock, FormClock);
  Application.CreateForm(TFormContainersInOrders, FormContainersInOrders);
  Application.CreateForm(TFormObjectStatesHistory, FormObjectStatesHistory);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormGroupCounteragents, FormGroupCounteragents);
  Application.CreateForm(TFormEditCheck, FormEditCheck);
  Application.CreateForm(TFormNoteFilter, FormNoteFilter);
  Application.CreateForm(TFormDocFeed, FormDocFeed);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditDocFeed, FormEditDocFeed);
  Application.CreateForm(TFormEditLoad, FormEditLoad);
  Application.CreateForm(TFormSetPrefixShort, FormSetPrefixShort);
  Application.CreateForm(TFormUnLoadPlan, FormUnLoadPlan);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormMtu2Picture, FormMtu2Picture);
  Application.CreateForm(TFormEditInspection, FormEditInspection);
  Application.CreateForm(TFormEditCarIncome, FormEditCarIncome);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormUnloadFronts, FormUnloadFronts);
  Application.CreateForm(TFormEditOrderNumber, FormEditOrderNumber);
  Application.CreateForm(TFormEditMtuAndPicture, FormEditMtuAndPicture);
  Application.CreateForm(TFormEditCarModel, FormEditCarModel);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditReturnDoc, FormEditReturnDoc);
  Application.CreateForm(TFormDispatchDocsList, FormDispatchDocsList);
  Application.CreateForm(TFormLog, FormLog);
  Application.CreateForm(TFormRoles, FormRoles);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormSelectCounteragent, FormSelectCounteragent);
  Application.CreateForm(TFormRestrictions, FormRestrictions);
  Application.CreateForm(TFormTasksRestrictControl, FormTasksRestrictControl);
  Application.CreateForm(TFormTasks, FormTasks);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormTasksIncome, FormTasksIncome);
  Application.CreateForm(TFormTransformDocuments, FormTransformDocuments);
  Application.CreateForm(TFormChildDocs, FormChildDocs);
  Application.CreateForm(TFormEditRemovalDoc, FormEditRemovalDoc);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditCargoMassFill, FormEditCargoMassFill);
  Application.CreateForm(TFormEditOrder, FormEditOrder);
  Application.CreateForm(TFormImportDocFeed, FormImportDocFeed);
  Application.CreateForm(TFormEditOwner, FormEditOwner);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormDocGenerate, FormDocGenerate);
  Application.CreateForm(TFormDocTypes, FormDocTypes);
  Application.CreateForm(TFormEditDocType, FormEditDocType);
  Application.CreateForm(TFormObjects, FormObjects);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormCarriages, FormCarriages);
  Application.CreateForm(TFormContainers, FormContainers);
  Application.CreateForm(TFormCargos, FormCargos);
  Application.CreateForm(TFormEditCargo, FormEditCargo);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormSelectObjectType, FormSelectObjectType);
  Application.CreateForm(TFormAddressContainers, FormAddressContainers);
  Application.CreateForm(TFormAddressCarriages, FormAddressCarriages);
  Application.CreateForm(TFormEditCodeName, FormEditCodeName);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormObjectMove, FormObjectMove);
  Application.CreateForm(TFormInspects, FormInspects);
  Application.CreateForm(TFormSettings, FormSettings);
  Application.CreateForm(TFormDocRoutes, FormDocRoutes);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditDocRoute, FormEditDocRoute);
  Application.CreateForm(TFormObjectStateKinds, FormObjectStateKinds);
  Application.CreateForm(TFormEditDocroute2Object, FormEditDocroute2Object);
  Application.CreateForm(TFormDeliveryTypes, FormDeliveryTypes);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormCargoTypes, FormCargoTypes);
  Application.CreateForm(TFormEditCargoType, FormEditCargoType);
  Application.CreateForm(TFormEditDeliveryType, FormEditDeliveryType);
  Application.CreateForm(TFormTool, FormTool);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditContainerKind, FormEditContainerKind);
  Application.CreateForm(TFormEditCarriageKind, FormEditCarriageKind);
  Application.CreateForm(TFormStateKinds, FormStateKinds);
  Application.CreateForm(TFormEditStateKind, FormEditStateKind);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditOperationKind, FormEditOperationKind);
  Application.CreateForm(TFormOperationKinds, FormOperationKinds);
  Application.CreateForm(TFormPersons, FormPersons);
  Application.CreateForm(TFormEditOperation, FormEditOperation);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditPerson, FormEditPerson);
  Application.CreateForm(TFormEditOperationRoute, FormEditOperationRoute);
  Application.CreateForm(TFormEditCargosOnCarriages, FormEditCargosOnCarriages);
  Application.CreateForm(TFormSelectFolder, FormSelectFolder);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditCatalog, FormEditCatalog);
  Application.CreateForm(TFormContainers, FormContainers);
  Application.CreateForm(TFormCarriages, FormCarriages);
  Application.CreateForm(TFormInvoices, FormInvoices);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormTrains, FormTrains);
  Application.CreateForm(TFormEditInvoice, FormEditInvoice);
  Application.CreateForm(TFormEditInvoiceSpec, FormEditInvoiceSpec);
  Application.CreateForm(TFormContracts, FormContracts);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditContract, FormEditContract);
  Application.CreateForm(TFormEditContainer, FormEditContainer);
  Application.CreateForm(TFormEditCarriage, FormEditCarriage);
  Application.CreateForm(TFormStatesHistory, FormStatesHistory);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEmailAccounts, FormEmailAccounts);
  Application.CreateForm(TFormEditEmailAccount, FormEditEmailAccount);
  Application.CreateForm(TFormEditDocSpecService, FormEditDocSpecService);
  Application.CreateForm(TFormCargoSheet, FormCargoSheet);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditTableLinks, FormEditTableLinks);
  Application.CreateForm(TFormUsers, FormUsers);
  Application.CreateForm(TFormDocFlowRules, FormDocFlowRules);
  //Application.CreateForm(TFormCounteragents, FormCounteragents);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormServiceKinds, FormServiceKinds);
  Application.CreateForm(TFormCarriageKinds, FormCarriageKinds);
  Application.CreateForm(TFormContainerKinds, FormContainerKinds);
  //Application.CreateForm(TFormStations, FormStations);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormEditUser, FormEditUser);
  Application.CreateForm(TFormEditStation, FormEditStation);
  Application.CreateForm(TFormEditCounteragent, FormEditCounteragent);
  Application.CreateForm(TFormEditService, FormEditService);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormQYN, FormQYN);
  Application.CreateForm(TFormLogin, FormLogin);
  Application.CreateForm(TFormDocTypeSelect, FormDocTypeSelect);
  Application.CreateForm(TFormEditUnloadDoc, FormEditUnloadDoc);
  FormSplash.IncreasePG;
  Application.CreateForm(TFormAddDriver, FormAddDriver);
  Application.CreateForm(TFormShippingOptions, FormShippingOptions);
  Application.CreateForm(TFormDirections, FormDirections);
  Application.CreateForm(TFormEditOrderSpec, FormEditOrderSpec);
  Application.CreateForm(TFormBilled, FormBilled);
  Application.CreateForm(TFormEditBilled, FormEditBilled);
  FormSplash.IncreasePG;

  if not FormMain.Init then
  begin
    Application.Terminate;
    Application.Run;
    exit;
  end;
  FormSplash.IncreasePG;
  FormDispatchKinds.Init;
  FormDeliveryTypes.Init;
  FormDocTypes.Init;
  FormStateKinds.Init;
  FormPersons.Init;
  FormDirections.Init;
  FormSplash.IncreasePG;
  //FormStations.Init;
  FormTechConditions.Init;
  dm.qrCounteragents.Open;
  dm.qrServices.Open;
  FormSplash.IncreasePG;
  FormSplash.Hide;

  if (not FormMain.Login) then
  begin
    Application.ShowMainForm := false;
    Application.Terminate;
  end;
  Application.Run;

end.
