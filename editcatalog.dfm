﻿inherited FormEditCatalog: TFormEditCatalog
  Caption = #1050#1072#1090#1072#1083#1086#1075
  ClientHeight = 202
  ClientWidth = 402
  ExplicitWidth = 408
  ExplicitHeight = 230
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 18
    Top = 91
    Width = 92
    Height = 16
    Caption = #1048#1084#1103' '#1082#1072#1090#1072#1083#1086#1075#1072
  end
  object Label2: TLabel [1]
    Left = 18
    Top = 18
    Width = 92
    Height = 16
    Caption = #1048#1084#1103' '#1082#1072#1090#1072#1083#1086#1075#1072
  end
  object sbParentFolders: TSpeedButton [2]
    Left = 351
    Top = 40
    Width = 28
    Height = 24
    Caption = '...'
    OnClick = sbParentFoldersClick
  end
  inherited plBottom: TPanel
    Top = 161
    Width = 402
    TabOrder = 1
    ExplicitTop = 161
    ExplicitWidth = 402
    inherited btnCancel: TButton
      Left = 286
      ExplicitLeft = 286
    end
    inherited btnOk: TButton
      Left = 167
      ExplicitLeft = 167
    end
  end
  object edFolderName: TDBEditEh [4]
    Left = 18
    Top = 113
    Width = 361
    Height = 24
    DataField = 'folder_name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object leParentCatalog: TDBSQLLookUp [5]
    Left = 18
    Top = 40
    Width = 330
    Height = 24
    DataField = 'parent_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
    Visible = True
    SqlSet = ssParentCatalog
  end
  inherited dsLocal: TDataSource
    Left = 272
    Top = 80
  end
  inherited qrAux: TADOQuery
    Left = 216
    Top = 80
  end
  object ssParentCatalog: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from folders where folder_section = :folder_section')
    DownSql.Strings = (
      'select * from folders where folder_section = :folder_section')
    InitSql.Strings = (
      'select * from folders where id = @id')
    KeyName = 'id'
    DisplayName = 'folder_name'
    Connection = dm.connMain
    Left = 264
    Top = 25
  end
end
