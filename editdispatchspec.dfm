﻿inherited FormEditDispatchSpec: TFormEditDispatchSpec
  Caption = #1054#1090#1087#1088#1072#1074#1082#1072
  ClientHeight = 410
  ClientWidth = 852
  ExplicitWidth = 858
  ExplicitHeight = 438
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 846
    Height = 16
    Align = alTop
    Caption = #1042#1072#1075#1086#1085
    ExplicitWidth = 38
  end
  object Label2: TLabel [1]
    AlignWithMargins = True
    Left = 3
    Top = 55
    Width = 846
    Height = 16
    Align = alTop
    Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
    ExplicitWidth = 71
  end
  object Label3: TLabel [2]
    Left = 3
    Top = 198
    Width = 25
    Height = 14
    Caption = #1052#1058#1059
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel [3]
    Left = 3
    Top = 259
    Width = 52
    Height = 14
    Caption = #1056#1080#1089#1091#1085#1086#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel [4]
    AlignWithMargins = True
    Left = 3
    Top = 318
    Width = 114
    Height = 14
    Caption = #1053#1086#1084#1077#1088#1072' '#1086#1090#1087#1088#1072#1074#1086#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel [5]
    Left = 4
    Top = 123
    Width = 86
    Height = 16
    Caption = #1044#1072#1090#1072' '#1087#1086#1076#1072#1095#1080
  end
  object Label7: TLabel [6]
    Left = 192
    Top = 123
    Width = 124
    Height = 16
    Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086#1076#1072#1095#1080
  end
  inherited plBottom: TPanel
    Top = 369
    Width = 852
    TabOrder = 7
    ExplicitTop = 369
    ExplicitWidth = 852
    inherited btnCancel: TButton
      Left = 736
      ExplicitLeft = 736
    end
    inherited btnOk: TButton
      Left = 617
      ExplicitLeft = 617
    end
  end
  object edCarriage: TDBEditEh [8]
    AlignWithMargins = True
    Left = 3
    Top = 25
    Width = 846
    Height = 24
    Align = alTop
    DataField = 'car_text'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
    Visible = True
  end
  object edDispatchNumbers: TDBEditEh [9]
    AlignWithMargins = True
    Left = 3
    Top = 337
    Width = 529
    Height = 22
    DataField = 'dispatch_numbers'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    Visible = True
  end
  object luMtuKind: TDBSQLLookUp [10]
    Left = 3
    Top = 218
    Width = 777
    Height = 22
    DataField = 'mtukind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = True
    SqlSet = ssMtuKind
    OnKeyValueChange = luMtuKindKeyValueChange
  end
  object luPictureKind: TDBSQLLookUp [11]
    Left = 3
    Top = 278
    Width = 777
    Height = 22
    DataField = 'picturekind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    Visible = True
    SqlSet = ssPictureKinds
  end
  object edContainer: TDBEditEh [12]
    AlignWithMargins = True
    Left = 3
    Top = 77
    Width = 846
    Height = 24
    Align = alTop
    DataField = 'cnum'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    Visible = True
  end
  object dtServeDate: TDBDateTimeEditEh [13]
    Left = 3
    Top = 143
    Width = 172
    Height = 24
    DataField = 'car_feed_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
    EditFormat = 'DD/MM/YYYY HH:NN:SS'
  end
  object edServeSheet: TDBEditEh [14]
    Left = 192
    Top = 143
    Width = 147
    Height = 24
    DataField = 'car_paper_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 136
    Top = 366
  end
  inherited qrAux: TADOQuery
    Left = 200
    Top = 374
  end
  object ssPictureKinds: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select id, name as codename from picturekinds '
      'where code like '#39'%@pattern%'#39' or name like '#39'%@pattern%'#39
      'order by code')
    DownSql.Strings = (
      'select id, name as codename from picturekinds'
      'order by code'
      '')
    InitSql.Strings = (
      'select id, name as codename from picturekinds where id = @id'
      '')
    KeyName = 'id'
    DisplayName = 'codename'
    Connection = dm.connMain
    Left = 270
    Top = 272
  end
  object ssMtuKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select id, rtrim(code)+'#39' =  '#39'+ltrim(rtrim(name)) as codename, pi' +
        'cture_id from mtukinds '
      'where code like '#39'%@pattern%'#39' or name like '#39'%@pattern%'#39
      'order by code')
    DownSql.Strings = (
      
        'select id, rtrim(code)+'#39' =  '#39'+ltrim(rtrim(name)) as codename, pi' +
        'cture_id '
      'from mtukinds order by code'
      '')
    InitSql.Strings = (
      
        'select id, rtrim(code)+'#39' =  '#39'+ltrim(rtrim(name)) as codename, pi' +
        'cture_id '
      'from mtukinds where id = @id')
    KeyName = 'id'
    DisplayName = 'codename'
    Connection = dm.connMain
    Left = 257
    Top = 214
  end
end
