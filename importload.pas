﻿unit importload;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Grids, Data.DB,
  Data.Win.ADODB, Vcl.ExtCtrls, ClipBrd, Vcl.ComCtrls, EhLibVCL, GridsEh,
  PivotGridsEh, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  DBAxisGridsEh, DBGridEh, MemTableDataEh, MemTableEh, DataDriverEh,
  ADODataDriverEh;

type
  TFormImportLoad = class(TFormEdit)
    pc: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    grid: TStringGrid;
    plTop: TPanel;
    btLink: TButton;
    btPaste: TButton;
    DBGridEh1: TDBGridEh;
    drvData: TADODataDriverEh;
    meData: TMemTableEh;
    dsData: TDataSource;
    procedure btPasteClick(Sender: TObject);
    procedure btLinkClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure InsertRowsFromClpBrd(SG: TStringGrid;  RowIndex: Integer);
  end;

var
  FormImportLoad: TFormImportLoad;

implementation

{$R *.dfm}

uses dmu;


procedure TFormImportLoad.btLinkClick(Sender: TObject);
var i, start: integer;
begin
  start := 1;
  for i := 1 to grid.RowCount-1 do
  begin
    dm.spImportCarLinks.Parameters.ParamByName('@start').Value := start;
    start := 0;
    dm.spImportCarLinks.Parameters.ParamByName('@col1').Value := grid.Rows[i][0];
    dm.spImportCarLinks.Parameters.ParamByName('@col2').Value := grid.Rows[i][1];
    dm.spImportCarLinks.Parameters.ParamByName('@col3').Value := grid.Rows[i][2];
    dm.spImportCarLinks.Parameters.ParamByName('@col4').Value := grid.Rows[i][3];
    dm.spImportCarLinks.Parameters.ParamByName('@col5').Value := grid.Rows[i][4];
    dm.spImportCarLinks.Parameters.ParamByName('@col6').Value := grid.Rows[i][5];
    dm.spImportCarLinks.Parameters.ParamByName('@col7').Value := grid.Rows[i][6];
    dm.spImportCarLinks.ExecProc;
  end;
  meData.Close;
  meData.Open;
  pc.ActivePageIndex := 1;
  if meData.RecordCount>0 then btnOk.Enabled := true;
end;

procedure TFormImportLoad.btPasteClick(Sender: TObject);
begin
  InsertRowsFromClpBrd(grid, 1);
end;

procedure TFormImportLoad.InsertRowsFromClpBrd(SG: TStringGrid;  RowIndex: Integer);
var
  i: Integer;
  sL: TStringList;
begin
  if not Clipboard.HasFormat(CF_TEXT) then Exit;

  sL := TStringList.Create;
  try
    sL.Text := Clipboard.AsText;

    SG.RowCount := 2;
    RowIndex := 1;

    for i := 0 to sL.Count - 1 do
    begin
      SG.RowCount := 1+RowIndex+i;
      SG.Rows[RowIndex+i].StrictDelimiter := True;
      SG.Rows[RowIndex+i].Delimiter := #9;
      SG.Rows[RowIndex+i].DelimitedText := sL[i];
    end;
  finally
    sL.Free;
  end;
end;

end.
