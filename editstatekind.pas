﻿unit editstatekind;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, DBCtrlsEh, Data.DB, Data.Win.ADODB;

type
  TFormEditStateKind = class(TFormEdit)
    Label2: TLabel;
    Label3: TLabel;
    edCode: TDBEditEh;
    edName: TDBEditEh;
    cbInplaceSign: TDBCheckBoxEh;
    cbInWork: TDBCheckBoxEh;
    cbIsFormal: TDBCheckBoxEh;
    cbAllowRepeat: TDBCheckBoxEh;
    cbStop: TDBCheckBoxEh;
    edMark: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditStateKind: TFormEditStateKind;

implementation

{$R *.dfm}

end.
