﻿inherited FormEditReturnDoc: TFormEditReturnDoc
  Caption = #1042#1086#1079#1074#1088#1072#1090' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
  ClientHeight = 157
  ClientWidth = 241
  ExplicitWidth = 247
  ExplicitHeight = 185
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 14
    Top = 49
    Width = 185
    Height = 16
    Caption = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
  end
  inherited plBottom: TPanel
    Top = 116
    Width = 241
    TabOrder = 2
    ExplicitTop = 116
    ExplicitWidth = 241
    inherited btnCancel: TButton
      Left = 125
      ExplicitLeft = 125
    end
    inherited btnOk: TButton
      Left = 6
      OnClick = btnOkClick
      ExplicitLeft = 6
    end
  end
  object dtReturnDoc: TDBDateTimeEditEh [2]
    Left = 14
    Top = 72
    Width = 169
    Height = 24
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 0
    Visible = True
  end
  object cbReturnDoc: TDBCheckBoxEh [3]
    Left = 14
    Top = 13
    Width = 185
    Height = 17
    BiDiMode = bdRightToLeft
    Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1074#1086#1079#1074#1088#1072#1097#1077#1085#1099
    DynProps = <>
    ParentBiDiMode = False
    TabOrder = 1
  end
  inherited dsLocal: TDataSource
    Left = 200
    Top = 112
  end
  inherited qrAux: TADOQuery
    Left = 144
    Top = 112
  end
end
