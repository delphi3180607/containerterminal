﻿unit CarriagesOnGo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Functions, Vcl.Mask, DBCtrlsEh;

type
  TFormCarriagesOnGo = class(TFormGrid)
    sbClearFilter: TPngSpeedButton;
    plStatus: TPanel;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    procedure btFilterClick(Sender: TObject);
    procedure sbClearFilterClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure N8Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
  private
    //--
  protected
    procedure AssignSelField(colname: string); override;
  public
    procedure Init; override;
    procedure SetFilter(conditions: string);
  end;

var
  FormCarriagesOnGo: TFormCarriagesOnGo;

implementation

{$R *.dfm}

uses filterlite, dmu, editcarriage;

procedure TFormCarriagesOnGo.Init;
begin
  CreateGuid(gu);
  g := GuidToString(gu);
  SetFilter(FormFilterLite.conditions);
  inherited;
  self.formEdit := FormEditCarriage;
  meData.Open;
end;


procedure TFormCarriagesOnGo.AssignSelField(colname: string);
begin

  selfield := '';
  if colname = 'date_income' then selfield := 'date_income';
  if colname = 'car_paper_number' then selfield := 'car_paper_number';
  if colname = 'train_num' then selfield := 'train_num';

end;


procedure TFormCarriagesOnGo.btFilterClick(Sender: TObject);
var fl: TStringList;
begin
  FormFilterLite.ShowModal;
  if FormFilterLite.ModalResult in [mrOk, mrYes] then
  begin
    CreateGuid(gu);
    g := GuidToString(gu);
    SetFilter(FormFilterLite.conditions);
    fl := TStringList.Create;
    fl.AddStrings(FormFilterLite.meNumbers.Lines);
    CheckMissingFilterValues(fl,'pnum');
  end;
end;

procedure TFormCarriagesOnGo.dgDataDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;
  if Column.FieldName = 'pnum' then
  begin
    if meData.FieldByName('urgent_repair_sign').AsInteger>0 then
    begin
        dgData.Canvas.Pen.Color := clRed;
        dgData.Canvas.Pen.Width := 1;
        dgData.Canvas.Brush.Color := clRed;
        dgData.Canvas.Rectangle(Rect.Right-8,Rect.Top+2,Rect.Right-2,Rect.Top+15);
        dgData.Canvas.Brush.Color := clWindow;
    end;
  end;
end;

procedure TFormCarriagesOnGo.meDataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  if Assigned(drvData.SelectCommand.Parameters.FindParam('guid')) then
      drvData.SelectCommand.Parameters.ParamByName('guid').Value := g;
end;

procedure TFormCarriagesOnGo.N10Click(Sender: TObject);
begin
  ClearCells;
end;

procedure TFormCarriagesOnGo.N8Click(Sender: TObject);
begin

    if fQYN('Действительно удалить вагон из всех предварительных погрузок ?') then
    begin
      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('delete from docloadjoint where id in (');
      qrAux.SQL.Add('	select l.joint_id from docloadjoint j, docload l, documents d');
      qrAux.SQL.Add('	where j.id = l.joint_id and l.id = d.id and isnull(d.isconfirmed,0)=0');
      qrAux.SQL.Add('	and j.carriage_id = :carriage_id ');
      qrAux.SQL.Add(')');
      qrAux.Parameters.ParamByName('carriage_id').Value := meData.FieldByName('id').AsInteger;
      qrAux.ExecSQL;
      drvData.GetrecCommand.Parameters.ParamByName('current_id').Value := meData.FieldByName('id').AsInteger;
      meData.RefreshRecord;
    end;

end;

procedure TFormCarriagesOnGo.sbClearFilterClick(Sender: TObject);
begin
  SetFilter('');
end;

procedure TFormCarriagesOnGo.SetFilter(conditions: string);
begin

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := conditions;
    dm.spCreateFilter.ExecProc;

    meData.Close;
    meData.Open;

    if conditions = '' then
      plStatus.Caption := ' Фильтр снят.'
    else
    begin
      plStatus.Caption := ' Установлен фильтр по списку номеров.';
    end;

end;


end.
