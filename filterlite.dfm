﻿inherited FormFilterLite: TFormFilterLite
  Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1085#1086#1084#1077#1088#1072#1084
  ClientHeight = 520
  ClientWidth = 506
  Font.Height = -11
  ExplicitWidth = 512
  ExplicitHeight = 548
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 8
    Top = 10
    Width = 192
    Height = 13
    Caption = #1053#1086#1084#1077#1088#1072' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074' ('#1087#1083#1072#1090#1092#1086#1088#1084')'
  end
  object cbClear: TSpeedButton [1]
    Left = 206
    Top = 4
    Width = 81
    Height = 26
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
  end
  inherited plBottom: TPanel
    Top = 482
    Width = 506
    Height = 38
    TabOrder = 1
    ExplicitTop = 482
    ExplicitWidth = 506
    ExplicitHeight = 38
    inherited btnCancel: TButton
      Left = 390
      Height = 32
      Font.Height = -11
      ParentFont = False
      ExplicitLeft = 390
      ExplicitHeight = 32
    end
    inherited btnOk: TButton
      Left = 271
      Height = 32
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      Font.Height = -11
      OnClick = btnOkClick
      ExplicitLeft = 271
      ExplicitHeight = 32
    end
    object btClear: TButton
      AlignWithMargins = True
      Left = 152
      Top = 3
      Width = 113
      Height = 32
      Align = alRight
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ModalResult = 6
      ParentFont = False
      TabOrder = 2
      OnClick = btClearClick
    end
  end
  object meNumbers: TMemo [3]
    Left = 8
    Top = 33
    Width = 489
    Height = 439
    ScrollBars = ssVertical
    TabOrder = 0
  end
  inherited dsLocal: TDataSource
    Left = 112
    Top = 128
  end
  inherited qrAux: TADOQuery
    Left = 168
    Top = 128
  end
end
