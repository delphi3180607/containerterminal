﻿inherited FormEditDocRestriction: TFormEditDocRestriction
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1079#1072#1087#1088#1077#1090#1072
  ClientHeight = 402
  ExplicitWidth = 543
  ExplicitHeight = 430
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 361
    TabOrder = 6
  end
  object cbIsActive: TDBCheckBoxEh [1]
    Left = 16
    Top = 16
    Width = 97
    Height = 17
    Caption = #1040#1082#1090#1091#1072#1083#1100#1085#1086'?'
    DataField = 'isactive'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 0
  end
  object dtStart: TDBDateTimeEditEh [2]
    Left = 16
    Top = 126
    Width = 201
    Height = 24
    ControlLabel.Width = 86
    ControlLabel.Height = 16
    ControlLabel.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
    ControlLabel.Visible = True
    DataField = 'datestart'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 2
    Visible = True
  end
  object edBase: TDBEditEh [3]
    Left = 16
    Top = 184
    Width = 505
    Height = 24
    ControlLabel.Width = 74
    ControlLabel.Height = 16
    ControlLabel.Caption = #1054#1089#1085#1086#1074#1072#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'base'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object edNote: TDBEditEh [4]
    Left = 16
    Top = 240
    Width = 505
    Height = 24
    ControlLabel.Width = 82
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 4
    Visible = True
  end
  object luType: TDBSQLLookUp [5]
    Left = 16
    Top = 68
    Width = 505
    Height = 24
    ControlLabel.Width = 84
    ControlLabel.Height = 16
    ControlLabel.Caption = #1058#1080#1087' '#1079#1072#1087#1088#1077#1090#1072
    ControlLabel.Visible = True
    DataField = 'type_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    SqlSet = ssRestTypes
  end
  object dtEnd: TDBDateTimeEditEh [6]
    Left = 16
    Top = 310
    Width = 201
    Height = 24
    ControlLabel.Width = 101
    ControlLabel.Height = 16
    ControlLabel.Caption = #1044#1072#1090#1072' '#1079#1072#1082#1088#1099#1090#1080#1103
    ControlLabel.Visible = True
    DataField = 'dateend'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 5
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 312
    Top = 296
  end
  inherited qrAux: TADOQuery
    Left = 248
    Top = 296
  end
  object ssRestTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from restrictkinds order by code')
    DownSql.Strings = (
      'select * from restrictkinds order by code')
    InitSql.Strings = (
      'select * from restrictkinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 240
    Top = 64
  end
end
