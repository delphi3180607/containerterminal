﻿unit selectdoctemplate;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.ComCtrls, Data.DB,
  Data.Win.ADODB, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFormSelectDocTemplate = class(TFormEdit)
    FileList: TListView;
  private
    { Private declarations }
  public
    procedure Init;
  end;

var
  FormSelectDocTemplate: TFormSelectDocTemplate;

implementation

{$R *.dfm}

procedure TFormSelectDocTemplate.Init;
var SR: TSearchRec; li:TListItem;
begin

  FileList.Items.BeginUpdate;
  try
      FileList.Items.Clear;

      FindFirst(ExtractFilePath(Application.ExeName) +'/templates/*.xls*', faAnyFile, SR);
      try
          repeat
              li := FileList.Items.Add;
              li.Caption := SR.Name;

              if ((SR.Attr and faDirectory) <> 0)  then li.ImageIndex := 1
              else li.ImageIndex := 0;

          until (FindNext(SR) <> 0);
      finally
          FindClose(SR);
      end;
  finally
      FileList.Items.EndUpdate;
  end;

end;

end.
