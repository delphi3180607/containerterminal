﻿inherited FormEditDocFlow: TFormEditDocFlow
  Caption = #1043#1088#1091#1079' '#1087#1086' '#1046#1044
  ClientHeight = 584
  ClientWidth = 697
  ExplicitWidth = 703
  ExplicitHeight = 612
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 18
    Top = 43
    Width = 94
    Height = 16
    Caption = #1053#1086#1084#1077#1088' '#1087#1086#1077#1079#1076#1072
  end
  object Label2: TLabel [1]
    Left = 18
    Top = 106
    Width = 71
    Height = 16
    Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
  end
  object Label3: TLabel [2]
    Left = 18
    Top = 174
    Width = 38
    Height = 16
    Caption = #1042#1072#1075#1086#1085
  end
  object sbContainer: TSpeedButton [3]
    Left = 572
    Top = 127
    Width = 52
    Height = 24
    Caption = '...'
    OnClick = sbContainerClick
  end
  object sbCarriage: TSpeedButton [4]
    Left = 572
    Top = 195
    Width = 52
    Height = 24
    Caption = '...'
    OnClick = sbCarriageClick
  end
  object Label4: TLabel [5]
    Left = 18
    Top = 239
    Width = 91
    Height = 16
    Caption = #1053#1086#1084#1077#1088' '#1082#1083#1077#1097#1072
  end
  object Label5: TLabel [6]
    Left = 18
    Top = 315
    Width = 126
    Height = 16
    Caption = #1042#1077#1089' ('#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100')'
  end
  object Label6: TLabel [7]
    Left = 192
    Top = 315
    Width = 105
    Height = 16
    Caption = #1042#1077#1089' ('#1076#1086#1082#1091#1084#1077#1085#1090')'
  end
  object Label7: TLabel [8]
    Left = 354
    Top = 315
    Width = 75
    Height = 16
    Caption = #1042#1077#1089' ('#1092#1072#1082#1090')'
  end
  object Label8: TLabel [9]
    Left = 18
    Top = 390
    Width = 116
    Height = 16
    Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
  end
  object SpeedButton3: TSpeedButton [10]
    Left = 572
    Top = 411
    Width = 52
    Height = 24
    Caption = '...'
    OnClick = SpeedButton3Click
  end
  object Label9: TLabel [11]
    Left = 18
    Top = 470
    Width = 118
    Height = 16
    Caption = #1050#1083#1080#1077#1085#1090' ('#1076#1086#1075#1086#1074#1086#1088')'
  end
  object SpeedButton4: TSpeedButton [12]
    Left = 572
    Top = 491
    Width = 52
    Height = 24
    Caption = '...'
    OnClick = SpeedButton4Click
  end
  object sbTrain: TSpeedButton [13]
    Left = 572
    Top = 65
    Width = 52
    Height = 24
    Caption = '...'
    OnClick = sbTrainClick
  end
  inherited plBottom: TPanel
    Top = 543
    Width = 697
    TabOrder = 8
    ExplicitTop = 543
    ExplicitWidth = 697
    inherited btnCancel: TButton
      Left = 581
      ExplicitLeft = 581
    end
    inherited btnOk: TButton
      Left = 462
      ExplicitLeft = 462
    end
  end
  object luContainer: TDBLookupComboboxEh
    Left = 18
    Top = 128
    Width = 548
    Height = 24
    DynProps = <>
    DataField = 'container_id'
    DataSource = dsLocal
    EditButtons = <>
    KeyField = 'id'
    ListField = 'cnum'
    ListSource = FormContainers.dsData
    TabOrder = 1
    Visible = True
  end
  object edSealNumber: TDBEditEh
    Left = 18
    Top = 261
    Width = 233
    Height = 24
    DataField = 'seal_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object nuWeightSender: TDBNumberEditEh
    Left = 18
    Top = 337
    Width = 126
    Height = 24
    DataField = 'weight_sender'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object nuWeightDocument: TDBNumberEditEh
    Left = 192
    Top = 337
    Width = 121
    Height = 24
    DataField = 'weight_doc'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 4
    Visible = True
  end
  object nuWeightFact: TDBNumberEditEh
    Left = 354
    Top = 337
    Width = 121
    Height = 24
    DataField = 'weight_fact'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 5
    Visible = True
  end
  object luConsignee: TDBLookupComboboxEh
    Left = 18
    Top = 412
    Width = 548
    Height = 24
    DynProps = <>
    DataField = 'consignee_id'
    DataSource = dsLocal
    EditButtons = <>
    KeyField = 'id'
    ListField = 'code'
    ListSource = FormCounteragents.dsData
    TabOrder = 6
    Visible = True
  end
  object luContract: TDBLookupComboboxEh
    Left = 18
    Top = 492
    Width = 548
    Height = 24
    DynProps = <>
    DataField = 'contract_id'
    DataSource = dsLocal
    EditButtons = <>
    KeyField = 'id'
    ListField = 'contract_code'
    ListSource = FormContracts.dsData
    TabOrder = 7
    Visible = True
  end
  object luTrain: TDBLookupComboboxEh
    Left = 18
    Top = 65
    Width = 548
    Height = 24
    DynProps = <>
    DataField = 'train_id'
    DataSource = dsLocal
    EditButtons = <>
    KeyField = 'id'
    ListField = 'tnum'
    ListSource = FormTrains.dsData
    TabOrder = 0
    Visible = True
  end
  object stState: TDBEditEh
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 691
    Height = 24
    TabStop = False
    Align = alTop
    Color = 10808562
    DataField = 'state_code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Enabled = False
    TabOrder = 9
    Visible = True
  end
  object luCarriage: TDBLookupComboboxEh
    Left = 18
    Top = 196
    Width = 548
    Height = 24
    DynProps = <>
    DataField = 'carriage_id'
    DataSource = dsLocal
    EditButtons = <>
    KeyField = 'id'
    ListField = 'pnum'
    ListSource = FormCarriages.dsData
    TabOrder = 10
    Visible = True
  end
  object dsLocal: TDataSource
    Left = 480
    Top = 6
  end
end
