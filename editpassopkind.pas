﻿unit EditPassOpKind;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, editcodename, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, Vcl.ExtCtrls, DBGridEh, DBLookupEh,
  DBSQLLookUp;

type
  TFormEditPassOpKind = class(TFormEditCodeName)
    edCodeShort: TDBEditEh;
    Label3: TLabel;
    Label4: TLabel;
    ssDocTypes: TADOLookUpSqlSet;
    leEndDocType: TDBSQLLookUp;
    Label5: TLabel;
    cbSection: TDBComboBoxEh;
    cbDirection: TDBComboBoxEh;
    Label6: TLabel;
    ssDlvTypes: TADOLookUpSqlSet;
    laDlvTypes: TDBSQLLookUp;
    Label16: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditPassOpKind: TFormEditPassOpKind;

implementation

{$R *.dfm}

end.
