﻿unit SPCust;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBGridEh, Vcl.Buttons, Vcl.Mask, DBCtrlsEh,
  DBLookupEh, functions, DBSQLLookUp;

type
  TFormEditSPCusts = class(TFormEdit)
    Label8: TLabel;
    sbForwarder: TSpeedButton;
    Label17: TLabel;
    dtChange: TDBDateTimeEditEh;
    Label1: TLabel;
    nePrice: TDBNumberEditEh;
    luCustomer: TDBSQLLookUp;
    procedure sbForwarderClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditSPCusts: TFormEditSPCusts;

implementation

{$R *.dfm}

uses counteragents;

procedure TFormEditSPCusts.sbForwarderClick(Sender: TObject);
begin
  SFDE(TFormCounteragents,self.luCustomer);
end;

end.
