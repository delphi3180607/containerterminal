﻿unit carriagekinds;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, MemTableDataEh, Data.DB,
  Data.Win.ADODB, MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport;

type
  TFormCarriageKinds = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormCarriageKinds: TFormCarriageKinds;

implementation

{$R *.dfm}

uses editcarriagekinds;

procedure TFormCarriageKinds.Init;
begin
  inherited;
  tablename := 'carriagekinds';
  self.formEdit := FormEditCarriageKind;
end;

end.
