﻿unit editcarriage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  DBGridEh, Vcl.Buttons, DBLookupEh, DBSQLLookUp, DBCtrlsEh, Vcl.Mask, functions,
  Data.DB, Data.Win.ADODB;

type
  TFormEditCarriage = class(TFormEdit)
    Label2: TLabel;
    edCNum: TDBEditEh;
    Label4: TLabel;
    edNote: TDBMemoEh;
    ssCarriageKind: TADOLookUpSqlSet;
    leContainerKind: TDBSQLLookUp;
    Label3: TLabel;
    Label5: TLabel;
    cbTechCondition: TDBLookupComboboxEh;
    cbDefective: TDBCheckBoxEh;
    luModel: TDBSQLLookUp;
    Label1: TLabel;
    ssModels: TADOLookUpSqlSet;
    cbInspection: TDBCheckBoxEh;
    edDateNextRepair: TDBDateTimeEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditCarriage: TFormEditCarriage;

implementation

{$R *.dfm}

uses counteragents;

end.
