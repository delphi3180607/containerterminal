﻿inherited FormCarriagesInspect: TFormCarriagesInspect
  Caption = #1054#1089#1084#1086#1090#1088' '#1074#1072#1075#1086#1085#1086#1074
  PixelsPerInch = 96
  TextHeight = 13
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select i.*, '
      
        '(case when i.isconfirmed = 1 then '#39#43#39' else '#39#39' end) as sta' +
        'te_code,'
      
        '(select person_name from persons p where  p.id =i.person_id) as ' +
        'person_name'
      'from docinspect i where objecttype = :objecttype '
      'order by doc_date')
    GetrecCommand.CommandText.Strings = (
      'select i.*, '
      
        '(case when i.isconfirmed = 1 then '#39#43#39' else '#39#39' end) as sta' +
        'te_code,'
      
        '(select person_name from persons p where  p.id =i.person_id) as ' +
        'person_name'
      'from docinspect i where id = :current_id')
  end
end
