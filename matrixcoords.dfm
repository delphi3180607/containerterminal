﻿inherited FormMatrixCoords: TFormMatrixCoords
  Caption = #1057#1090#1088#1091#1082#1090#1091#1088#1072' '#1084#1072#1090#1088#1080#1094#1099
  ClientHeight = 724
  ClientWidth = 1053
  ExplicitWidth = 1069
  ExplicitHeight = 762
  PixelsPerInch = 96
  TextHeight = 13
  object SplitterVert: TSplitter [0]
    Left = 529
    Top = 0
    Width = 6
    Height = 683
    Color = 15790320
    ParentColor = False
    StyleElements = [seFont, seBorder]
    ExplicitLeft = 331
    ExplicitTop = 8
  end
  inherited plBottom: TPanel
    Top = 683
    Width = 1053
    ExplicitTop = 683
    ExplicitWidth = 1053
    inherited btnOk: TButton
      Left = 818
      ExplicitLeft = 818
    end
    inherited btnCancel: TButton
      Left = 937
      ExplicitLeft = 937
    end
  end
  inherited plAll: TPanel
    Width = 529
    Height = 683
    Align = alLeft
    ExplicitWidth = 529
    ExplicitHeight = 683
    object Splitter2: TSplitter [0]
      Left = 0
      Top = 360
      Width = 529
      Height = 6
      Cursor = crVSplit
      Align = alBottom
      Color = 15790320
      ParentColor = False
      ExplicitTop = 368
    end
    inherited plTop: TPanel
      Width = 529
      ExplicitWidth = 529
      inherited btTool: TPngSpeedButton
        Left = 491
        ExplicitLeft = 712
      end
      inherited plCount: TPanel
        Left = 463
        Width = 25
        Visible = False
        ExplicitLeft = 463
        ExplicitWidth = 25
        inherited edCount: TDBEditEh
          Left = -47
          ControlLabel.ExplicitLeft = -153
          ControlLabel.ExplicitTop = 8
          ControlLabel.ExplicitWidth = 103
          ExplicitLeft = -47
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 529
      Height = 331
      AutoFitColWidths = True
      Font.Height = -13
      Font.Name = 'Calibri'
      TitleParams.Font.Height = -13
      TitleParams.Font.Name = 'Calibri'
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'place_order'
          Footers = <>
          Title.Caption = #1055#1086#1088#1103#1076#1086#1082
          Width = 67
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isactive'
          Footers = <>
          Title.Caption = #1048#1089#1087#1086#1083#1100#1079#1091#1077#1090#1089#1103'?'
          Width = 97
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1059#1095#1072#1089#1090#1086#1082
          Width = 166
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'rows'
          Footers = <>
          Title.Caption = #1064#1090#1072#1073#1077#1083#1077#1081
          Width = 67
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'stacks'
          Footers = <>
          Title.Caption = #1057#1077#1082#1094#1080#1081
          Width = 85
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'levels'
          Footers = <>
          Title.Caption = #1071#1088#1091#1089#1086#1074
          Width = 62
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 366
      Width = 529
      Height = 317
      Align = alBottom
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 3
      object plToolExclude: TPanel
        Left = 0
        Top = 0
        Width = 529
        Height = 28
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Align = alTop
        BevelOuter = bvNone
        Caption = #1048#1089#1082#1083#1102#1095#1077#1085#1080#1103' '#1080#1079' '#1087#1088#1072#1074#1080#1083#1072
        Color = 15790320
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        StyleElements = [seFont, seBorder]
        object sbAddExcl: TPngSpeedButton
          AlignWithMargins = True
          Left = 0
          Top = 0
          Width = 34
          Height = 28
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Action = aAdd
          Align = alLeft
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbAddExclClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000E9C00000E
            9C01079453DD0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000002764944415478DAA5935B48145118C7
            FF733AB3EC8C0EDA4B82B5B5651042498B2CAB66FA12BD78698330A38788F212
            582F05AE2F696042493DD83E943E44411704A5072F154104BB8A2D51ACDD28A8
            DDBC4441E2B2BB33E3CE7AA63389646A60F41DCE8173F8F89DEFF2FF04AC3427
            DFC75082A3C8959D98562318C11DFE768BEFC85247D334212CB98BA0F0675E70
            D7B9F2F3B073B303D97605337A026FBF44F0EA7D14F1F6B11EA868E2BEC67280
            889342C8555255E02D2A522549228CC196349250C40C8020A5691AEB1B0BC8E1
            47C1D7B81F2BB420BF01327A0ABBBC270E979726E64C26A7611249B0B13D39C5
            087E1B8566A60885C02481AAB7034F32C3A7867AA0A36111E0543A3C9FCE790F
            A884523BE1DFA5F9CAB66591335B1AD115BD8ED9548C519E1FB3566A4EBF3A30
            24C77CA3DB38206201DACAFB6BCF57B83C7AD2487180C9A3674411159C7536E1
            4AC48FB811E7582B2B816488367D78FCB9FD69F53D1F075C1678B53F3474F9B6
            E7ADCF61DC039268278699FE957BFDC6E3E89EBAC9014988028566E8169C4D27
            7E507F4547C09C32CB041C92532D6D3E4A2961FB73F7A154F110FCC502F131F6
            70F2310311E9C5DA96CFE64B33EF9F239837E7D9D7E40CF557F10826AC0880D6
            BDFD35AD55AEE2B5D5609D4D1F7C17B23FABBCDBCC6BD0B9D0857637EF825725
            36710D5D30F4CEC10772A239B49503A20B3AB0E386CB5F5D77A4BC6CAD3AE8E6
            3A685CA1C4DDC51505073D1EAEC44C92668C2679E8193C154A485AD354D6170A
            CAE1E191307A67DD7F2A7111025C932F15D6BBF3776097C3892C49414C8B637C
            2A82176F3E22D11AB27E3EBDDA2C2C9FC61A6C422536C081EF98C02406F85BEF
            AAD3681DFF633F013BA960F0622C30B40000000049454E44AE426082}
          ExplicitLeft = 8
        end
        object sdDelExcl: TPngSpeedButton
          AlignWithMargins = True
          Left = 34
          Top = 0
          Width = 34
          Height = 28
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Action = aDelete
          Align = alLeft
          Flat = True
          OnClick = sdDelExclClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000E9C00000E
            9C01079453DD0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000001484944415478DAAD53414B024114FE
            66095996FE417F41C49325EEBAE622B241D84442873A458724FA33051DEA545D
            0AF6B0A162BA048BE62EE8EF08BA7888080FD5343B90AC68896B8F8179CCBCEF
            9BF7BDF78630C6B088917F23208404DBDA11B0720E58D382F7017A0DBC70B73B
            C285085619A51E0603545C778B93DC87C165C0BC2B956A22D6B6358E7B1A23D8
            2384DE140A1692C977743ACA89E7D153C00EEE0E80CD4B4DB3914ABDA1DF5F3E
            76DDF21963D684049EE2C6553A5D452E37E424F24EBB6DBE029F4D5D6F72B038
            AB789EC86E9A048C5E330C1BAAFA854643822C03F9BCF0C359FD4A10D83A603C
            EA7A0BF1F8109204F8BEBCDBEB99B7C0C34FCC9F04065F4E54823109F5BA0445
            01B2D90F38CED24C09A288994C9503E62FA26863B168219188DCC6D983649A35
            C462C120A91CD79D7B940F81ED0BE099BBFE8484853F5354FB06D031E7E18A64
            5A800000000049454E44AE426082}
          ExplicitLeft = 42
        end
        object sdEditExcl: TPngSpeedButton
          AlignWithMargins = True
          Left = 68
          Top = 0
          Width = 34
          Height = 28
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Action = aEdit
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sdEditExclClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
            62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
            08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
            4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
            C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
            892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
            4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
            0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
            C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
            8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
            346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
            5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
            3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
            378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
            C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
            1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
            FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
            F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
            0049454E44AE426082}
          ExplicitLeft = 76
        end
      end
      object dgExclude: TDBGridEh
        Left = 0
        Top = 28
        Width = 529
        Height = 289
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alClient
        AllowedOperations = []
        AutoFitColWidths = True
        Border.Color = clSilver
        BorderStyle = bsNone
        ColumnDefValues.Title.TitleButton = True
        Ctl3D = False
        DataSource = dsExcludes
        DynProps = <>
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
        GridLineParams.BrightColor = 15395041
        GridLineParams.DarkColor = 15395041
        GridLineParams.DataBoundaryColor = 15395041
        GridLineParams.DataHorzColor = 14540253
        GridLineParams.DataVertColor = 14540253
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
        IndicatorParams.HorzLineColor = clSilver
        IndicatorParams.VertLineColor = clSilver
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        OptionsEh = [dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
        ParentCtl3D = False
        ParentFont = False
        ParentShowHint = False
        SelectionDrawParams.SelectionStyle = gsdsClassicEh
        SelectionDrawParams.DrawFocusFrame = False
        SelectionDrawParams.DrawFocusFrameStored = True
        ShowHint = True
        SortLocal = True
        STFilter.Color = 15725813
        STFilter.Font.Charset = DEFAULT_CHARSET
        STFilter.Font.Color = clWindowText
        STFilter.Font.Height = -11
        STFilter.Font.Name = 'Calibri'
        STFilter.Font.Style = []
        STFilter.HorzLineColor = 14540253
        STFilter.Local = True
        STFilter.ParentFont = False
        STFilter.VertLineColor = 14540253
        TabOrder = 1
        TitleParams.Color = clBtnFace
        TitleParams.FillStyle = cfstThemedEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -13
        TitleParams.Font.Name = 'Calibri'
        TitleParams.Font.Style = [fsBold]
        TitleParams.HorzLineColor = 13421772
        TitleParams.MultiTitle = True
        TitleParams.ParentFont = False
        TitleParams.SecondColor = 15987699
        TitleParams.SortMarkerStyle = smstDefaultEh
        TitleParams.VertLineColor = 13421772
        OnMouseDown = dgDataMouseDown
        Columns = <
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'limit_type_code'
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1080#1089#1082#1083#1102#1095#1077#1085#1080#1103
            Width = 85
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'row_start'
            Footers = <>
            Title.Caption = #1064#1090#1072#1073#1077#1083#1100' '#1089
            Width = 77
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'row_end'
            Footers = <>
            Title.Caption = #1064#1090#1072#1073#1077#1083#1100' '#1087#1086
            Width = 82
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'stack_start'
            Footers = <>
            Title.Caption = #1057#1077#1082#1094#1080#1103' '#1089
            Width = 78
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'stack_end'
            Footers = <>
            Title.Caption = #1057#1077#1082#1094#1080#1103' '#1087#1086
            Width = 91
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'levels'
            Footers = <>
            Title.Caption = #1063#1080#1089#1083#1086' '#1103#1088#1091#1089#1086#1074
            Width = 100
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cells'
            Footers = <>
            Title.Caption = #1071#1095#1077#1077#1082
            Width = 86
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object plRight: TPanel [3]
    Left = 535
    Top = 0
    Width = 518
    Height = 683
    Margins.Right = 0
    Align = alClient
    BevelOuter = bvNone
    Caption = 'plRight'
    TabOrder = 2
    object Bevel4: TBevel
      Left = 517
      Top = 25
      Width = 1
      Height = 335
      Align = alRight
      ExplicitLeft = 80
      ExplicitTop = 29
      ExplicitHeight = 585
    end
    object Splitter1: TSplitter
      Left = 0
      Top = 360
      Width = 518
      Height = 6
      Cursor = crVSplit
      Align = alBottom
      Color = 15790320
      ParentColor = False
      ExplicitTop = 359
      ExplicitWidth = 781
    end
    object dgRows: TDBGridEh
      Left = 0
      Top = 25
      Width = 517
      Height = 335
      Align = alClient
      AutoFitColWidths = True
      Border.Color = clSilver
      BorderStyle = bsNone
      ColumnDefValues.Title.TitleButton = True
      Ctl3D = False
      DataSource = dsRows
      DynProps = <>
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Calibri'
      Font.Style = []
      GridLineParams.DataHorzColor = 14540253
      GridLineParams.DataVertColor = 14540253
      GridLineParams.DataVertLines = True
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      IndicatorOptions = []
      Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghDialogFind, dghColumnResize, dghColumnMove]
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      SelectionDrawParams.DrawFocusFrame = True
      SelectionDrawParams.DrawFocusFrameStored = True
      SortLocal = True
      TabOrder = 0
      TitleParams.FillStyle = cfstThemedEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -13
      TitleParams.Font.Name = 'Calibri'
      TitleParams.Font.Style = [fsBold]
      TitleParams.HorzLineColor = clSilver
      TitleParams.MultiTitle = True
      TitleParams.ParentFont = False
      TitleParams.VertLineColor = 14540253
      TitleParams.VertLines = True
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isactive'
          Footers = <>
          Title.Caption = #1048#1089#1087#1086#1083#1100#1079#1091#1077#1090#1089#1103
          Width = 132
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'order_num'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 68
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Width = 288
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object plTool: TPanel
      Left = 0
      Top = 0
      Width = 518
      Height = 25
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Align = alTop
      BevelOuter = bvNone
      Caption = #1064#1090#1072#1073#1077#1083#1080
      Color = 15790320
      ParentBackground = False
      TabOrder = 1
      StyleElements = [seFont, seBorder]
      object btEditRow: TPngSpeedButton
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 34
        Height = 25
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Action = aEdit
        Align = alLeft
        Flat = True
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        OnClick = btEditRowClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
          62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
          08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
          4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
          C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
          892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
          4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
          0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
          C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
          8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
          346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
          5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
          3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
          378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
          C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
          1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
          FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
          F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
          0049454E44AE426082}
        ExplicitLeft = 76
      end
      object btDisableRow: TPngSpeedButton
        AlignWithMargins = True
        Left = 34
        Top = 0
        Width = 34
        Height = 25
        Hint = #1047#1072#1087#1088#1077#1090#1080#1090#1100' '#1088#1103#1076
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Action = aEdit
        Align = alLeft
        Flat = True
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        OnClick = btDisableRowClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000097048597300000B1300000B1301009A9C18000002474944415478DA
          636400829D5671F5EEC716353290011841C4769BB806A67F0CFFC9318411C620
          D71046640EC890FF7F19195B3E73F2B3B13245BD7DF55BF03F23C33F7E018E17
          FF7F33AE387673520550D97F9C06586A66C599FCFDBAD0C088E3BF4A4A18A38E
          BE1603C3BF8F0C776F9E673877FAD1FFCEE61B8C0ACA7CD1FBCF4E5C866140A4
          57B12B03C3E7AD95CD6AAC9C171419BE7FFDCDB0EF2B3BC3DFBFFF19E2D39C19
          8484BE30FCFD7984212BE9D2EFFF4CAC1E739637EC831B202714AFADAECA76AE
          B54790CDC4BA8DE1C5B38F0CB3C21B18989998189ECAAB30BC79FD9161C6E25C
          0621E15F0C7FBFEF6508F13EFDFBE2B9B77A0F3E2FB80136C05223E34664FC6B
          F598947A0641115D86A9BD9B1926766E6248D4E165088BB163E8D9FF84415543
          9AA1A83A90E1DFAF0B0C47F61D61A82EBB7DE3C8E5499A580D98DEB78561E39A
          930CF7EFBE6448D0E661F8FBE71F03AF9703437E4500C3FFFF5F190EED98C750
          5771E7C6A14B5003E4796235D5B5382FB4F40A80BDF0E6D567869488890C3F7F
          FE61B877E70543942A2743708435836E6E38C3BFDF571982DCE6FFBE7CF6B5CE
          BDCF8B6EC10331DAA7CCF9CFEF0FDBABDBD458750C0B81867C6258341B184E4C
          0C0C2191360CBFF71F65F8FFEF3B43C7CEABBF39D8B8DDA72DABDE8F118D36DA
          39514F9E7C591A932CF0CFC5D38F49CFD0142C7EE7D633865347CFFFBA317B3B
          9BBE10EBAAD41373C2B1A603183055CB686762FC1FF1F1D31F0986FF8C7FB9B9
          391E002364DDE9DB53EAD0532C560308016443C832006688E791450D641B0003
          004DA3FD11FEC2FCB00000000049454E44AE426082}
        ExplicitLeft = 33
      end
    end
    object plSpec: TPanel
      Left = 0
      Top = 366
      Width = 518
      Height = 317
      Align = alBottom
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 2
      object plToolSpec: TPanel
        Left = 0
        Top = 0
        Width = 518
        Height = 28
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Align = alTop
        BevelOuter = bvNone
        Caption = #1057#1077#1082#1094#1080#1080
        Color = 15790320
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        StyleElements = [seFont, seBorder]
        object btEditStack: TPngSpeedButton
          AlignWithMargins = True
          Left = 0
          Top = 0
          Width = 34
          Height = 28
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Action = aEdit
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = btEditStackClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
            62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
            08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
            4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
            C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
            892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
            4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
            0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
            C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
            8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
            346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
            5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
            3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
            378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
            C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
            1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
            FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
            F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
            0049454E44AE426082}
          ExplicitLeft = 8
        end
        object btDisableStack: TPngSpeedButton
          AlignWithMargins = True
          Left = 34
          Top = 0
          Width = 34
          Height = 28
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Action = aEdit
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = btDisableStackClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            61000000097048597300000B1300000B1301009A9C18000002474944415478DA
            636400829D5671F5EEC716353290011841C4769BB806A67F0CFFC9318411C620
            D71046640EC890FF7F19195B3E73F2B3B13245BD7DF55BF03F23C33F7E018E17
            FF7F33AE387673520550D97F9C06586A66C599FCFDBAD0C088E3BF4A4A18A38E
            BE1603C3BF8F0C776F9E673877FAD1FFCEE61B8C0ACA7CD1FBCF4E5C866140A4
            57B12B03C3E7AD95CD6AAC9C171419BE7FFDCDB0EF2B3BC3DFBFFF19E2D39C19
            8484BE30FCFD7984212BE9D2EFFF4CAC1E739637EC831B202714AFADAECA76AE
            B54790CDC4BA8DE1C5B38F0CB3C21B18989998189ECAAB30BC79FD9161C6E25C
            0621E15F0C7FBFEF6508F13EFDFBE2B9B77A0F3E2FB80136C05223E34664FC6B
            F598947A0641115D86A9BD9B1926766E6248D4E165088BB163E8D9FF84415543
            9AA1A83A90E1DFAF0B0C47F61D61A82EBB7DE3C8E5499A580D98DEB78561E39A
            930CF7EFBE6448D0E661F8FBE71F03AF9703437E4500C3FFFF5F190EED98C750
            5771E7C6A14B5003E4796235D5B5382FB4F40A80BDF0E6D567869488890C3F7F
            FE61B877E70543942A2743708435836E6E38C3BFDF571982DCE6FFBE7CF6B5CE
            BDCF8B6EC10331DAA7CCF9CFEF0FDBABDBD458750C0B81867C6258341B184E4C
            0C0C2191360CBFF71F65F8FFEF3B43C7CEABBF39D8B8DDA72DABDE8F118D36DA
            39514F9E7C591A932CF0CFC5D38F49CFD0142C7EE7D633865347CFFFBA317B3B
            9BBE10EBAAD41373C2B1A603183055CB686762FC1FF1F1D31F0986FF8C7FB9B9
            391E002364DDE9DB53EAD0532C560308016443C832006688E791450D641B0003
            004DA3FD11FEC2FCB00000000049454E44AE426082}
          ExplicitLeft = 42
        end
      end
      object dgStacks: TDBGridEh
        Left = 0
        Top = 28
        Width = 518
        Height = 289
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alClient
        AllowedOperations = []
        Border.Color = clSilver
        BorderStyle = bsNone
        ColumnDefValues.Title.TitleButton = True
        Ctl3D = False
        DataSource = dsStacks
        DynProps = <>
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
        GridLineParams.BrightColor = 15395041
        GridLineParams.DarkColor = 15395041
        GridLineParams.DataBoundaryColor = 15395041
        GridLineParams.DataHorzColor = 14540253
        GridLineParams.DataVertColor = 14540253
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
        IndicatorParams.HorzLineColor = clSilver
        IndicatorParams.VertLineColor = clSilver
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        OptionsEh = [dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
        ParentCtl3D = False
        ParentFont = False
        ParentShowHint = False
        SelectionDrawParams.SelectionStyle = gsdsClassicEh
        SelectionDrawParams.DrawFocusFrame = False
        SelectionDrawParams.DrawFocusFrameStored = True
        ShowHint = True
        SortLocal = True
        STFilter.Color = 15725813
        STFilter.Font.Charset = DEFAULT_CHARSET
        STFilter.Font.Color = clWindowText
        STFilter.Font.Height = -11
        STFilter.Font.Name = 'Calibri'
        STFilter.Font.Style = []
        STFilter.HorzLineColor = 14540253
        STFilter.Local = True
        STFilter.ParentFont = False
        STFilter.VertLineColor = 14540253
        TabOrder = 1
        TitleParams.Color = clBtnFace
        TitleParams.FillStyle = cfstThemedEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -13
        TitleParams.Font.Name = 'Calibri'
        TitleParams.Font.Style = [fsBold]
        TitleParams.HorzLineColor = 13421772
        TitleParams.MultiTitle = True
        TitleParams.ParentFont = False
        TitleParams.SecondColor = 15987699
        TitleParams.SortMarkerStyle = smstDefaultEh
        TitleParams.VertLineColor = 13421772
        OnMouseDown = dgDataMouseDown
        Columns = <
          item
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isactive'
            Footers = <>
            Title.Caption = #1048#1089#1087#1086#1083#1100#1079#1091#1077#1090#1089#1103
            Width = 116
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'order_num'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088
            Width = 74
          end
          item
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isnarrow'
            Footers = <>
            Title.Caption = #1042' '#1086#1076#1085#1091' '#1096#1080#1088#1080#1085#1091
            Width = 108
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'note'
            Footers = <>
            Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
            Width = 291
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited pmGrid: TPopupMenu
    Left = 560
    Top = 160
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from matrixsectors order by place_order')
    UpdateCommand.CommandText.Strings = (
      'update matrixsectors'
      'set'
      '  code = :code,'
      '  name = :name,'
      '  place_order = :place_order,'
      '  rows = :rows,'
      '  stacks = :stacks,'
      '  levels = :levels,'
      ' isactive = :isactive, '
      ' roworderreverse = :roworderreverse, '
      ' stackorderreverse = :stackorderreverse, '
      ' levelorderreverse = :levelorderreverse'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'place_order'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'rows'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'stacks'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'levels'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isactive'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'roworderreverse'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'stackorderreverse'
        Size = -1
        Value = Null
      end
      item
        Name = 'levelorderreverse'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into matrixsectors'
      
        '  (code, name, place_order, rows, stacks, levels, isactive, rowo' +
        'rderreverse, stackorderreverse, levelorderreverse)'
      'values'
      
        '  (:code, :name, :place_order, :rows, :stacks, :levels, :isactiv' +
        'e, :roworderreverse, :stackorderreverse, :levelorderreverse)')
    InsertCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'place_order'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'rows'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'stacks'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'levels'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isactive'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'roworderreverse'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'stackorderreverse'
        Size = -1
        Value = Null
      end
      item
        Name = 'levelorderreverse'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from matrixsectors where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from matrixsectors where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 104
    Top = 136
  end
  inherited meData: TMemTableEh
    Left = 160
    Top = 136
  end
  inherited dsData: TDataSource
    Left = 208
    Top = 136
  end
  inherited qrAux: TADOQuery
    Left = 616
    Top = 160
  end
  inherited drvForms: TADODataDriverEh
    Left = 756
    Top = 162
  end
  inherited meForms: TMemTableEh
    Left = 804
    Top = 162
  end
  inherited dsForms: TDataSource
    Left = 852
    Top = 162
  end
  inherited al: TActionList
    Left = 504
    Top = 160
  end
  inherited exReport: TEXLReport
    Left = 662
    Top = 160
  end
  object drvRows: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select * from matrixrows where sector_id = :sector_id order by o' +
        'rder_num')
    SelectCommand.Parameters = <
      item
        Name = 'sector_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update matrixrows'
      'set'
      '  order_num = :order_num,'
      '  isactive = :isactive,'
      '  note = :note'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'order_num'
        Size = -1
        Value = Null
      end
      item
        Name = 'isactive'
        Size = -1
        Value = Null
      end
      item
        Name = 'note'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into matrixrows'
      '  (sector_id, order_num, isactive, note)'
      'values'
      '  (:sector_id, :order_num, :isactive, :note)')
    InsertCommand.Parameters = <
      item
        Name = 'sector_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'order_num'
        Size = -1
        Value = Null
      end
      item
        Name = 'isactive'
        Size = -1
        Value = Null
      end
      item
        Name = 'note'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from matrixrows where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from matrixrows where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 488
    Top = 256
  end
  object meRows: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvRows
    AfterInsert = meRowsAfterInsert
    Left = 536
    Top = 256
  end
  object dsRows: TDataSource
    DataSet = meRows
    Left = 584
    Top = 256
  end
  object drvStacks: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select * from matrixstacks where  sector_id = :sector_id order b' +
        'y order_num')
    SelectCommand.Parameters = <
      item
        Name = 'sector_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update matrixstacks'
      'set'
      '  sector_id = :sector_id,'
      '  order_num = :order_num,'
      '  isactive = :isactive,'
      '  isnarrow = :isnarrow, '
      '  note = :note'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'sector_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'order_num'
        Size = -1
        Value = Null
      end
      item
        Name = 'isactive'
        Size = -1
        Value = Null
      end
      item
        Name = 'isnarrow'
        Size = -1
        Value = Null
      end
      item
        Name = 'note'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into matrixstacks'
      '  (sector_id, order_num, isactive, isnarrow, note)'
      'values'
      '  (:sector_id, :order_num, :isactive, :isnarrow, :note)')
    InsertCommand.Parameters = <
      item
        Name = 'sector_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'order_num'
        Size = -1
        Value = Null
      end
      item
        Name = 'isactive'
        Size = -1
        Value = Null
      end
      item
        Name = 'isnarrow'
        Size = -1
        Value = Null
      end
      item
        Name = 'note'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from matrixstacks where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from matrixstacks where  id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 616
    Top = 480
  end
  object meStacks: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvStacks
    Left = 672
    Top = 480
  end
  object dsStacks: TDataSource
    DataSet = meStacks
    Left = 720
    Top = 480
  end
  object drvExcludes: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select me.*,  '
      
        '(case when me.limit_type = 1 then '#39#1071#1088#1091#1089#1099#39' when me.limit_type = 2' +
        ' then '#39#1071#1095#1077#1081#1082#1080#39' else '#39'???'#39' end) as limit_type_code'
      
        'from matrixexcludes me where  sector_id = :sector_id order by ro' +
        'w_start, stack_start')
    SelectCommand.Parameters = <
      item
        Name = 'sector_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update matrixexcludes'
      'set'
      '  sector_id = :sector_id,'
      '  limit_type = :limit_type, '
      '  row_start = :row_start,'
      '  row_end = :row_end,'
      '  stack_start = :stack_start,'
      '  stack_end = :stack_end,'
      '  levels = :levels, '
      '  cells = :cells '
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'sector_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'limit_type'
        Size = -1
        Value = Null
      end
      item
        Name = 'row_start'
        Size = -1
        Value = Null
      end
      item
        Name = 'row_end'
        Size = -1
        Value = Null
      end
      item
        Name = 'stack_start'
        Size = -1
        Value = Null
      end
      item
        Name = 'stack_end'
        Size = -1
        Value = Null
      end
      item
        Name = 'levels'
        Size = -1
        Value = Null
      end
      item
        Name = 'cells'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into matrixexcludes'
      
        '  (sector_id, limit_type, row_start, row_end, stack_start, stack' +
        '_end, levels, cells)'
      'values'
      
        '  (:sector_id, :limit_type, :row_start, :row_end, :stack_start, ' +
        ':stack_end, :levels, :cells)')
    InsertCommand.Parameters = <
      item
        Name = 'sector_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'limit_type'
        Size = -1
        Value = Null
      end
      item
        Name = 'row_start'
        Size = -1
        Value = Null
      end
      item
        Name = 'row_end'
        Size = -1
        Value = Null
      end
      item
        Name = 'stack_start'
        Size = -1
        Value = Null
      end
      item
        Name = 'stack_end'
        Size = -1
        Value = Null
      end
      item
        Name = 'levels'
        Size = -1
        Value = Null
      end
      item
        Name = 'cells'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from matrixexcludes where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select me.*,  '
      
        '(case when me.limit_type = 1 then '#39#1071#1088#1091#1089#1099#39' when me.limit_type = 2' +
        ' then '#39#1071#1095#1077#1081#1082#1080#39' else '#39'???'#39' end) as limit_type_code'
      'from matrixexcludes me '
      'where me.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 120
    Top = 544
  end
  object meExcludes: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvExcludes
    AfterInsert = meExcludesAfterInsert
    Left = 176
    Top = 544
  end
  object dsExcludes: TDataSource
    DataSet = meExcludes
    Left = 224
    Top = 544
  end
end
