﻿unit editcolumn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Vcl.ExtCtrls, Data.DB, Data.Win.ADODB;

type
  TFormEditColumn = class(TFormEdit)
    edFieldName: TDBEditEh;
    edCaption: TDBEditEh;
    edShortCaption: TDBEditEh;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cbGroup: TDBComboBoxEh;
    cbShowInGrid: TDBCheckBoxEh;
    neOrder: TDBNumberEditEh;
    Label5: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditColumn: TFormEditColumn;

implementation

{$R *.dfm}

end.
