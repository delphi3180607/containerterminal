﻿unit editcontractspec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEh, Vcl.StdCtrls, DBCtrlsEh,
  Vcl.Buttons, Vcl.Mask, DBLookupEh, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls, functions;

type
  TFormEditContractSpec = class(TFormEdit)
    luService: TDBLookupComboboxEh;
    sbContainer: TSpeedButton;
    nePrice: TDBNumberEditEh;
    Label2: TLabel;
    Цена: TLabel;
    Label1: TLabel;
    neAmount: TDBNumberEditEh;
    procedure sbContainerClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditContractSpec: TFormEditContractSpec;

implementation

{$R *.dfm}

uses servicekinds;

procedure TFormEditContractSpec.sbContainerClick(Sender: TObject);
begin
  SFD(FormServiceKinds, self.luService);
end;

end.
