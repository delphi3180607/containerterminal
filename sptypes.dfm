﻿inherited FormEditSPTypes: TFormEditSPTypes
  Caption = #1058#1080#1087' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
  ClientHeight = 188
  ClientWidth = 524
  ExplicitWidth = 530
  ExplicitHeight = 216
  PixelsPerInch = 96
  TextHeight = 16
  object Label3: TLabel [0]
    Left = 8
    Top = 68
    Width = 108
    Height = 16
    Caption = #1042#1080#1076' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
  end
  object Label2: TLabel [1]
    Left = 8
    Top = 10
    Width = 132
    Height = 16
    Caption = #1058#1080#1087' '#1080#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1103
  end
  inherited plBottom: TPanel
    Top = 147
    Width = 524
    TabOrder = 2
    ExplicitTop = 202
    ExplicitWidth = 524
    inherited btnCancel: TButton
      Left = 408
      ExplicitLeft = 408
    end
    inherited btnOk: TButton
      Left = 289
      ExplicitLeft = 289
    end
  end
  object leContainerKind: TDBSQLLookUp [3]
    Left = 8
    Top = 90
    Width = 505
    Height = 24
    DataField = 'contkind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
    SqlSet = ssContainerKind
  end
  object cbTypes: TDBComboBoxEh [4]
    Left = 8
    Top = 32
    Width = 505
    Height = 24
    DataField = 'conttype'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      #1055#1086#1088#1086#1078#1085#1080#1081
      #1043#1088#1091#1078#1077#1085#1099#1081)
    KeyItems.Strings = (
      '0'
      '1')
    TabOrder = 0
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 328
  end
  inherited qrAux: TADOQuery
    Left = 400
    Top = 48
  end
  object ssContainerKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from containerkinds order by code')
    DownSql.Strings = (
      'select * from containerkinds order by code')
    InitSql.Strings = (
      'select * from containerkinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 185
    Top = 76
  end
end
