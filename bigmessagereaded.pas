﻿unit BigMessageReaded;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BigMessage, Vcl.StdCtrls, DBCtrlsEh,
  Vcl.Buttons;

type
  TFormBigMessReaded = class(TFormBigMessage)
    cbReaded: TDBCheckBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormBigMessReaded: TFormBigMessReaded;

implementation

{$R *.dfm}

end.
