﻿unit placecarriages;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  Vcl.ExtCtrls, EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions,
  Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Buttons, PngSpeedButton, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids;

type
  TFormPlaceCarriages = class(TFormGrid)
    Splitter1: TSplitter;
    gpRight: TGridPanel;
    sbFilterPlan: TPngSpeedButton;
    pl1: TPanel;
    PngSpeedButton1: TPngSpeedButton;
    Panel1: TPanel;
    PngSpeedButton2: TPngSpeedButton;
    Panel2: TPanel;
    PngSpeedButton3: TPngSpeedButton;
    Panel3: TPanel;
    PngSpeedButton4: TPngSpeedButton;
    Panel4: TPanel;
    PngSpeedButton5: TPngSpeedButton;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    DBGrid4: TDBGrid;
    DBGrid5: TDBGrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPlaceCarriages: TFormPlaceCarriages;

implementation

{$R *.dfm}

end.
