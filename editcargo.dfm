﻿inherited FormEditCargo: TFormEditCargo
  Caption = #1043#1088#1091#1079
  ClientHeight = 440
  ClientWidth = 528
  ExplicitWidth = 534
  ExplicitHeight = 468
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 11
    Top = 5
    Width = 41
    Height = 14
    Caption = #1053#1086#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel [1]
    Left = 8
    Top = 248
    Width = 81
    Height = 14
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label15: TLabel [2]
    Left = 11
    Top = 61
    Width = 62
    Height = 14
    Caption = #1058#1080#1087' '#1075#1088#1091#1079#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbCargoType: TSpeedButton [3]
    Left = 375
    Top = 80
    Width = 31
    Height = 24
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = sbCargoTypeClick
  end
  object Label5: TLabel [4]
    Left = 11
    Top = 128
    Width = 119
    Height = 14
    Caption = #1042#1077#1089' ('#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel [5]
    Left = 185
    Top = 128
    Width = 97
    Height = 14
    Caption = #1042#1077#1089' ('#1076#1086#1082#1091#1084#1077#1085#1090')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel [6]
    Left = 347
    Top = 128
    Width = 69
    Height = 14
    Caption = #1042#1077#1089' ('#1092#1072#1082#1090')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel [7]
    Left = 11
    Top = 185
    Width = 93
    Height = 14
    Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 399
    Width = 528
    TabOrder = 9
    ExplicitTop = 399
    ExplicitWidth = 528
    inherited btnCancel: TButton
      Left = 412
      ExplicitLeft = 412
    end
    inherited btnOk: TButton
      Left = 293
      ExplicitLeft = 293
    end
  end
  object edCNum: TDBEditEh [9]
    Left = 11
    Top = 26
    Width = 305
    Height = 22
    DataField = 'cnum'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edNote: TDBMemoEh [10]
    Left = 8
    Top = 270
    Width = 505
    Height = 115
    AutoSize = False
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    Visible = True
    WantReturns = True
  end
  object cbIsEmpty: TDBCheckBoxEh [11]
    Left = 416
    Top = 84
    Width = 97
    Height = 17
    Caption = #1055#1086#1088#1086#1078#1085#1080#1081
    DataField = 'isempty'
    DataSource = dsLocal
    DynProps = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = cbIsEmptyClick
  end
  object nuWeightSender: TDBNumberEditEh [12]
    Left = 11
    Top = 146
    Width = 126
    Height = 22
    DataField = 'weight_sender'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object nuWeightDocument: TDBNumberEditEh [13]
    Left = 185
    Top = 146
    Width = 121
    Height = 22
    DataField = 'weight_doc'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 4
    Visible = True
  end
  object nuWeightFact: TDBNumberEditEh [14]
    Left = 347
    Top = 146
    Width = 121
    Height = 22
    DataField = 'weight_fact'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
  object edSealNumber: TDBEditEh [15]
    Left = 11
    Top = 207
    Width = 173
    Height = 22
    DataField = 'seal_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    Visible = True
  end
  object luCargoType: TDBSQLLookUp [16]
    Left = 11
    Top = 81
    Width = 359
    Height = 22
    DataField = 'type_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
    SqlSet = ssCargoType
    KeyValue = Null
    RowCount = 0
  end
  object cbSealWaste: TDBComboBoxEh [17]
    Left = 224
    Top = 203
    Width = 58
    Height = 27
    ControlLabel.Width = 113
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1083#1086#1084#1073#1072' '#1075#1086#1076#1085#1072#1103' ?'
    ControlLabel.Visible = True
    DataField = 'issealwaste'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Items.Strings = (
      '+'
      '-'
      '?'
      ' ')
    KeyItems.Strings = (
      '+'
      '-'
      '?'
      ' ')
    LimitTextToListValues = True
    ParentFont = False
    TabOrder = 7
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 448
    Top = 16
  end
  inherited qrAux: TADOQuery
    Left = 392
    Top = 16
  end
  object ssCargoType: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select id, code+'#39' | '#39'+name as fullname from cargotypes '
      'where code like '#39'%@pattern%'#39'  or name like '#39'%@pattern%'#39' '
      'order by code')
    DownSql.Strings = (
      
        'select top 20 id, code+'#39' | '#39'+name as fullname from cargotypes or' +
        'der by code')
    InitSql.Strings = (
      'select id, code+'#39' | '#39'+name as fullname from cargotypes '
      'where id = @id')
    KeyName = 'id'
    DisplayName = 'fullname'
    Connection = dm.connMain
    Left = 136
    Top = 64
  end
end
