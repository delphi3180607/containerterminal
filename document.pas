﻿unit document;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEh, DBGridEhGrouping,
  ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh,
  PngSpeedButton, DBCtrlsEh, Vcl.StdCtrls, Vcl.Mask, DBLookupEh, Vcl.ExtCtrls,
  Vcl.Buttons, Data.DB, Data.Win.ADODB, MemTableDataEh, MemTableEh,
  DataDriverEh, ADODataDriverEh;

type
  TFormEditCargoDoc = class(TFormEdit)
    plTop: TPanel;
    Label8: TLabel;
    SpeedButton3: TSpeedButton;
    Label9: TLabel;
    SpeedButton4: TSpeedButton;
    Label2: TLabel;
    SpeedButton2: TSpeedButton;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Bevel1: TBevel;
    luConsignee: TDBLookupComboboxEh;
    luContract: TDBLookupComboboxEh;
    luForwarder: TDBLookupComboboxEh;
    edDocNumber: TDBEditEh;
    dtDocDate: TDBDateTimeEditEh;
    dtPlanDate: TDBDateTimeEditEh;
    dtFactDate: TDBDateTimeEditEh;
    plTool: TPanel;
    PngSpeedButton4: TPngSpeedButton;
    PngSpeedButton1: TPngSpeedButton;
    PngSpeedButton5: TPngSpeedButton;
    dgData: TDBGridEh;
    stDocType: TDBEditEh;
    dsData: TDataSource;
    sbLinkObject: TPngSpeedButton;
    Label6: TLabel;
    plService: TPanel;
    luService: TDBLookupComboboxEh;
    sbService: TSpeedButton;
    Label7: TLabel;
    drvData: TADODataDriverEh;
    meData: TMemTableEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditCargoDoc: TFormEditCargoDoc;

implementation

{$R *.dfm}

end.
