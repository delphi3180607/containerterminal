﻿inherited FormCarriageModels: TFormCarriageModels
  Caption = #1052#1086#1076#1077#1083#1080' '#1074#1072#1075#1086#1085#1086#1074
  ExplicitWidth = 836
  ExplicitHeight = 549
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'model_code'
          Footers = <>
          Title.Caption = #1052#1086#1076#1077#1083#1100
          Width = 140
        end
        item
          DisplayFormat = '### ##0.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_length'
          Footers = <>
          Title.Caption = #1044#1083#1080#1085#1072
          Width = 163
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from carriagemodels order by model_code')
    UpdateCommand.CommandText.Strings = (
      'update carriagemodels'
      'set'
      '  model_code = :model_code,'
      '  car_length = :car_length'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'model_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'car_length'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into carriagemodels'
      '  (model_code, car_length)'
      'values'
      '  (:model_code, :car_length)')
    InsertCommand.Parameters = <
      item
        Name = 'model_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'car_length'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from carriagemodels where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from carriagemodels where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
end
