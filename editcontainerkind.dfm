﻿inherited FormEditContainerKind: TFormEditContainerKind
  Caption = #1058#1080#1087' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
  ClientHeight = 234
  ExplicitWidth = 543
  ExplicitHeight = 262
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 15
    Top = 14
    Width = 24
    Height = 16
    Caption = #1050#1086#1076
  end
  object Label3: TLabel [1]
    Left = 15
    Top = 69
    Width = 98
    Height = 16
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  object Label1: TLabel [2]
    Left = 15
    Top = 130
    Width = 84
    Height = 16
    Caption = #1056#1072#1079#1084#1077#1088', '#1092#1091#1090
  end
  inherited plBottom: TPanel
    Top = 193
    TabOrder = 4
    ExplicitTop = 229
  end
  object edCode: TDBEditEh [4]
    Left = 15
    Top = 32
    Width = 305
    Height = 24
    DataField = 'code'
    DataSource = FormContainerKinds.dsData
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edName: TDBEditEh [5]
    Left = 15
    Top = 88
    Width = 505
    Height = 24
    DataField = 'name'
    DataSource = FormContainerKinds.dsData
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object neFootSize: TDBNumberEditEh [6]
    Left = 15
    Top = 152
    Width = 121
    Height = 24
    DataField = 'footsize'
    DataSource = FormContainerKinds.dsData
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object cbDefault: TDBCheckBoxEh [7]
    Left = 200
    Top = 152
    Width = 145
    Height = 17
    Caption = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    DataField = 'isdefault'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 3
  end
end
