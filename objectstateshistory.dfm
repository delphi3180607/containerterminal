﻿inherited FormObjectStatesHistory: TFormObjectStatesHistory
  ActiveControl = dgData
  Caption = #1048#1089#1090#1086#1088#1080#1103' '#1086#1087#1077#1088#1072#1094#1080#1081' '#1089' '#1086#1073#1098#1077#1082#1090#1086#1084
  ClientHeight = 608
  ClientWidth = 984
  ExplicitWidth = 1000
  ExplicitHeight = 646
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter3: TSplitter [0]
    Left = 0
    Top = 381
    Width = 984
    Height = 6
    Cursor = crVSplit
    Align = alBottom
    Beveled = True
    ExplicitLeft = 168
    ExplicitTop = 413
    ExplicitWidth = 438
  end
  inherited plBottom: TPanel
    Top = 567
    Width = 984
    ExplicitTop = 567
    ExplicitWidth = 984
    inherited btnOk: TButton
      Left = 749
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ExplicitLeft = 749
    end
    inherited btnCancel: TButton
      Left = 868
      Visible = False
      ExplicitLeft = 868
    end
  end
  inherited plAll: TPanel
    Width = 984
    Height = 381
    ExplicitWidth = 984
    ExplicitHeight = 381
    inherited plTop: TPanel
      Width = 984
      ExplicitWidth = 984
      inherited sbDelete: TPngSpeedButton
        Visible = False
      end
      inherited btFilter: TPngSpeedButton
        Left = 148
        Width = 39
        Visible = False
        ExplicitLeft = 144
        ExplicitWidth = 39
      end
      inherited btExcel: TPngSpeedButton
        Width = 38
        ExplicitLeft = 104
        ExplicitWidth = 38
      end
      inherited sbAdd: TPngSpeedButton
        Visible = False
      end
      inherited sbEdit: TPngSpeedButton
        Visible = False
      end
      inherited Bevel2: TBevel
        Visible = False
      end
      inherited btTool: TPngSpeedButton
        Left = 946
        ExplicitLeft = 881
      end
      inherited plCount: TPanel
        Left = 749
        ExplicitLeft = 749
      end
    end
    inherited dgData: TDBGridEh
      Width = 984
      Height = 352
      AllowedOperations = []
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_factexecution'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'  '#1092#1072#1082#1090#1080#1095#1077#1089#1082#1072#1103
          Width = 98
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_history'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1092#1086#1088#1084#1083#1077#1085#1080#1103
          Width = 94
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 91
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'delivery_code'
          Footers = <>
          HideDuplicates = True
          Title.Caption = #1058#1080#1087' '#1086#1087#1077#1088#1072#1094#1080#1080
          Width = 109
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_code'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 83
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num'
          Footers = <>
          Title.Caption = #1050#1055
          Width = 64
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'forwarder_code'
          Footers = <>
          HideDuplicates = True
          Title.Caption = #1050#1083#1080#1077#1085#1090
          Width = 133
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer_code'
          Footers = <>
          HideDuplicates = True
          Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
          Width = 115
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee_code'
          Footers = <>
          HideDuplicates = True
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 124
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'station_code'
          Footers = <>
          HideDuplicates = True
          Title.Caption = #1057#1090#1072#1085#1094#1080#1103
          Width = 133
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'task_id'
          Footers = <>
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  object Panel3: TPanel [3]
    Left = 0
    Top = 387
    Width = 984
    Height = 180
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object DBGridEh1: TDBGridEh
      Left = 0
      Top = 0
      Width = 984
      Height = 180
      Align = alClient
      BorderStyle = bsNone
      DataSource = dsLinkedObjects
      DynProps = <>
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = []
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
      ParentFont = False
      ReadOnly = True
      SelectionDrawParams.DrawFocusFrame = True
      SelectionDrawParams.DrawFocusFrameStored = True
      TabOrder = 0
      TitleParams.FillStyle = cfstGradientEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -12
      TitleParams.Font.Name = 'Verdana'
      TitleParams.Font.Style = [fsBold]
      TitleParams.ParentFont = False
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_history'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
          Visible = False
          Width = 141
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'object_code'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
          Width = 149
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isempty'
          Footers = <>
          Title.Caption = #1055#1086#1088#1086#1078#1085#1080#1081'?'
          Width = 133
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'object_id'
          Footers = <>
          Title.Caption = 'OBJECTID'
          Width = 102
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select odh.*,'
      
        '(select code from doctypes t where t.id = d.doctype_id)+'#39' '#8470' '#39'+d.' +
        'doc_number as doc_code, '
      
        '(select code from objectstatekinds k where k.id = odh.object_sta' +
        'te_id) as state_code,'
      
        '(select p.train_num from docload l, docloadjoint j, loadplan p w' +
        'here l.joint_id = j.id and j.load_plan_id = p.id and l.id = d.id' +
        ') as train_num,'
      'odh.date_history,'
      
        '(select name from stations c where c.id = t.station_id) as stati' +
        'on_code,'
      
        '(select code from counteragents c where c.id = t.forwarder_id) a' +
        's forwarder_code,'
      
        '(select code from counteragents c where c.id = t.consignee_id) a' +
        's consignee_code,'
      
        '(select code from counteragents c where c.id = t.payer_id) as pa' +
        'yer_code,'
      
        '(select code from deliverytypes tp where tp.id = t.dlv_type_id) ' +
        'as delivery_code '
      'from documents d,  objectstatehistory odh'
      'left outer join tasks t on (t.id = odh.task_id) '
      'where d.id = odh.doc_id and odh.object_id = :object_id'
      'order by odh.date_factexecution')
    SelectCommand.Parameters = <
      item
        Name = 'object_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
  end
  inherited meData: TMemTableEh
    AfterOpen = meDataAfterScroll
    AfterScroll = meDataAfterScroll
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
    inherited aDelete: TAction
      Enabled = False
    end
    inherited aEdit: TAction
      Enabled = False
    end
  end
  object drvLinkedObjects: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select odh.object_id, odh.date_history, '
      
        '(case when vl.cnum like '#39'{%'#39' then '#39#39' else vl.cnum end) as object' +
        '_code, '
      'vl.isempty'
      'from objectstatehistory odh'
      'left outer join v_objects vl on (vl.id = odh.object_id)'
      
        'where  odh.linked_object_id = :object_id and odh.doc_id = :doc_i' +
        'd'
      'order by odh.date_history desc')
    SelectCommand.Parameters = <
      item
        Name = 'object_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      '')
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 192
    Top = 475
  end
  object meLinkedObjects: TMemTableEh
    Params = <>
    DataDriver = drvLinkedObjects
    Left = 240
    Top = 475
  end
  object dsLinkedObjects: TDataSource
    DataSet = meLinkedObjects
    Left = 296
    Top = 475
  end
end
