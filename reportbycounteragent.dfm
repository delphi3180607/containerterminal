﻿inherited FormReportByCounteragent: TFormReportByCounteragent
  ActiveControl = dgData
  Caption = #1054#1090#1095#1077#1090' '#1087#1086' '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090#1091
  ClientHeight = 565
  ClientWidth = 921
  ExplicitWidth = 937
  ExplicitHeight = 603
  PixelsPerInch = 96
  TextHeight = 13
  object SplitterHor: TSplitter [0]
    Left = 0
    Top = 236
    Width = 921
    Height = 4
    Cursor = crVSplit
    Align = alBottom
    Color = clBtnFace
    ParentColor = False
    ExplicitTop = 237
  end
  inherited plBottom: TPanel
    Top = 524
    Width = 921
    ExplicitTop = 524
    ExplicitWidth = 921
    inherited btnOk: TButton
      Left = 686
      ExplicitLeft = 686
    end
    inherited btnCancel: TButton
      Left = 805
      ExplicitLeft = 805
    end
  end
  inherited plAll: TPanel
    Width = 921
    Height = 236
    ExplicitWidth = 921
    ExplicitHeight = 237
    inherited plTop: TPanel
      Width = 921
      Height = 30
      ExplicitWidth = 921
      ExplicitHeight = 30
      inherited sbDelete: TPngSpeedButton
        Left = 673
        Height = 30
        ExplicitLeft = 582
        ExplicitHeight = 41
      end
      inherited btFilter: TPngSpeedButton
        Left = 816
        Height = 30
        ExplicitLeft = 732
        ExplicitHeight = 40
      end
      inherited btExcel: TPngSpeedButton
        Left = 782
        Height = 30
        ExplicitLeft = 686
        ExplicitHeight = 40
      end
      inherited sbAdd: TPngSpeedButton
        Left = 707
        Height = 30
        ExplicitLeft = 419
        ExplicitTop = 3
        ExplicitHeight = 28
      end
      inherited sbEdit: TPngSpeedButton
        Left = 741
        Height = 30
        ExplicitLeft = 645
        ExplicitHeight = 41
      end
      inherited Bevel2: TBevel
        Left = 778
        Height = 30
        ExplicitLeft = 681
        ExplicitHeight = 30
      end
      inherited btTool: TPngSpeedButton
        Left = 883
        Height = 24
        ExplicitLeft = 869
        ExplicitHeight = 35
      end
      inherited plCount: TPanel
        Left = 686
        Height = 30
        TabOrder = 2
        ExplicitLeft = 686
        ExplicitHeight = 30
        inherited edCount: TDBEditEh
          Height = 24
          ExplicitHeight = 24
        end
      end
      object Panel3: TPanel
        Left = 204
        Top = 0
        Width = 343
        Height = 30
        Margins.Left = 0
        Align = alLeft
        TabOrder = 0
        object luCustomer: TDBSQLLookUp
          AlignWithMargins = True
          Left = 69
          Top = 4
          Width = 270
          Height = 22
          Align = alRight
          ControlLabel.Width = 60
          ControlLabel.Height = 13
          ControlLabel.Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
          ControlLabel.Visible = True
          ControlLabelLocation.Position = lpLeftCenterEh
          DynProps = <>
          EditButtons = <
            item
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          HighlightRequired = True
          ParentFont = False
          TabOrder = 0
          Visible = True
          SqlSet = dm.ssCounteragents
          RowCount = 0
        end
      end
      object Panel6: TPanel
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 201
        Height = 30
        Margins.Left = 0
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alLeft
        TabOrder = 1
        object dtStart: TDBDateTimeEditEh
          AlignWithMargins = True
          Left = 91
          Top = 5
          Width = 106
          Height = 21
          Margins.Left = 90
          ControlLabel.Width = 81
          ControlLabel.Height = 13
          ControlLabel.Caption = #1053#1072#1095#1080#1085#1072#1103' '#1089' '#1076#1072#1090#1099
          ControlLabel.Visible = True
          ControlLabelLocation.Position = lpLeftCenterEh
          Align = alBottom
          DynProps = <>
          EditButtons = <>
          Kind = dtkDateEh
          TabOrder = 0
          Visible = True
        end
      end
      object sbRun: TButton
        AlignWithMargins = True
        Left = 550
        Top = 2
        Width = 121
        Height = 26
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        Caption = #1055#1086#1083#1091#1095#1080#1090#1100' '#1086#1090#1095#1077#1090
        TabOrder = 3
        OnClick = sbRunClick
      end
    end
    inherited dgData: TDBGridEh
      Top = 30
      Width = 921
      Height = 206
      Font.Height = -13
      Font.Name = 'Calibri'
      OptionsEh = [dghHighlightFocus, dghAutoSortMarking, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      STFilter.HorzLineColor = 12303291
      TitleParams.Font.Height = -13
      TitleParams.Font.Name = 'Calibri'
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
          Width = 107
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          Title.Caption = #1058#1080#1087
          Width = 55
        end
        item
          CellButtons = <>
          Color = 15000804
          DynProps = <>
          EditButtons = <>
          FieldName = 'dlv_code'
          Footers = <>
          Title.Caption = #1054#1087#1077#1088#1072#1094#1080#1103
          Width = 98
        end
        item
          CellButtons = <>
          Color = 7786324
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 104
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_factexecution'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 133
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
          Width = 111
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isempty'
          Footers = <>
          Title.Caption = #1055#1086#1088'.'
          Title.Hint = #1055#1086#1088#1086#1078#1085#1080#1081' ?'
          Width = 42
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'seal_number'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsItalic]
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
          Width = 86
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'pnum'
          Footers = <>
          Title.Caption = #1042#1072#1075#1086#1085
          Width = 93
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'forwarder_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          Width = 151
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 144
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer_code'
          Footers = <>
          Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
          Width = 127
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Color = 33023
          DynProps = <>
          EditButtons = <>
          FieldName = 'end_point_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Footers = <>
          Title.Caption = #1055#1091#1085#1082#1090' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1103
          Width = 120
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_text'
          Footers = <>
          Title.Caption = #1044#1086#1082#1091#1084#1077#1085#1090
          Width = 166
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'base_doc_text'
          Footers = <>
          Title.Caption = #1053#1072#1095#1072#1083#1100#1085#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
          Width = 167
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  object Panel8: TPanel [3]
    Left = 0
    Top = 240
    Width = 921
    Height = 24
    Align = alBottom
    BevelOuter = bvNone
    Caption = 
      #1048#1089#1090#1086#1088#1080#1103' '#1086#1087#1077#1088#1072#1094#1080#1081' ('#1076#1072#1085#1085#1099#1077' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1099' '#1074' '#1086#1073#1088#1072#1090#1085#1086#1084' '#1093#1088#1086#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1086#1084' '#1087 +
      #1086#1088#1103#1076#1082#1077')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBtnText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object dgHistory: TDBGridEh [4]
    Left = 0
    Top = 264
    Width = 921
    Height = 260
    Align = alBottom
    AllowedOperations = []
    Border.Color = clSilver
    ColumnDefValues.Title.TitleButton = True
    ColumnDefValues.ToolTips = True
    Ctl3D = False
    DataSource = dsHistory
    DynProps = <>
    FixedColor = clCream
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    GridLineParams.BrightColor = clSilver
    GridLineParams.ColorScheme = glcsClassicEh
    GridLineParams.DarkColor = clSilver
    GridLineParams.DataBoundaryColor = clSilver
    GridLineParams.DataHorzColor = clSkyBlue
    GridLineParams.DataVertColor = clSkyBlue
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    IndicatorParams.Color = clBtnFace
    IndicatorParams.HorzLineColor = clSilver
    IndicatorParams.VertLineColor = clSilver
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghHighlightFocus, dghAutoSortMarking, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SearchPanel.Enabled = True
    SearchPanel.PersistentShowing = False
    SelectionDrawParams.SelectionStyle = gsdsClassicEh
    SelectionDrawParams.DrawFocusFrame = False
    SelectionDrawParams.DrawFocusFrameStored = True
    ShowHint = True
    SortLocal = True
    STFilter.Color = clWhite
    STFilter.HorzLineColor = clSilver
    STFilter.InstantApply = True
    STFilter.Local = True
    STFilter.VertLineColor = clSilver
    TabOrder = 3
    TitleParams.Color = clBtnFace
    TitleParams.FillStyle = cfstThemedEh
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -11
    TitleParams.Font.Name = 'Verdana'
    TitleParams.Font.Style = [fsBold]
    TitleParams.HorzLineColor = clSilver
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    TitleParams.SecondColor = clCream
    TitleParams.VertLineColor = clSilver
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'delivery_code'
        Footers = <>
        HideDuplicates = True
        Title.Caption = #1058#1080#1087' '#1086#1087#1077#1088#1072#1094#1080#1080
        Width = 113
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'state_code'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Footers = <>
        Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
        Width = 132
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'date_factexecution'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072' '#1087#1088#1086#1074#1086#1076#1082#1080
        Width = 160
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'doc_code'
        Footers = <>
        Title.Caption = #1044#1086#1082#1091#1084#1077#1085#1090
        Visible = False
        Width = 185
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'forwarder_code'
        Footers = <>
        HideDuplicates = True
        Title.Caption = #1050#1083#1080#1077#1085#1090
        Width = 163
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'payer_code'
        Footers = <>
        HideDuplicates = True
        Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
        Width = 146
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'consignee_code'
        Footers = <>
        HideDuplicates = True
        Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
        Width = 158
      end
      item
        Alignment = taCenter
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'train_num'
        Footers = <>
        Title.Caption = #1050#1055
        Width = 64
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'station_code'
        Footers = <>
        HideDuplicates = True
        Title.Caption = #1057#1090#1072#1085#1094#1080#1103
        Width = 199
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'task_id'
        Footers = <>
        Visible = False
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'declare @contid int, @datestart datetime;'
      ''
      'select @contid = isnull( :contid , 0), @datestart = :datestart;'
      ''
      'select  o.id,  d.id, c.cnum, '
      
        '(select code from containerkinds k where k.id = c.kind_id) as ki' +
        'nd_code,'
      
        '(select code from counteragents c where c.id = o.owner_id) as ow' +
        'ner_code,'
      'isnull(cr.isempty,0) as isempty,'
      
        '(case when cr.seal_number like '#39'{%'#39' then '#39#39' else cr.seal_number ' +
        'end) as seal_number, '
      'isnull(car.pnum, sh.carriage_num) as pnum,'
      
        '(select code from deliverytypes dt where dt.id = t.dlv_type_id) ' +
        'as dlv_code,'
      
        '(select code from counteragents c where c.id = isnull(t.forwarde' +
        'r_id, sh.forwarder_id)) as forwarder_code,'
      
        '(select code from counteragents c where c.id = isnull(t.consigne' +
        'e_id, sh.consignee_id)) as consignee_code,'
      
        '(select code from counteragents c where c.id = isnull(t.payer_id' +
        ', sh.payer_id)) as payer_code,'
      
        '(case when not dr.id is null then dr.name else st.name end) as e' +
        'nd_point_name,'
      
        '(select sk.code from objectstatekinds sk where sk.id = ls.object' +
        '_state_id) as state_code,'
      'ls.date_factexecution,'
      '(select code from doctypes dt where d.doctype_id = dt.id)+'#39' '#39'+'
      
        'isnull(d.doc_number,'#39#39')+'#39' '#1086#1090' '#39'+convert(varchar, d.doc_date, 104)' +
        ' as doc_text,'
      '(select code from doctypes dt where d1.doctype_id = dt.id)+'#39' '#39'+'
      
        'isnull(d1.doc_number,'#39#39')+'#39' '#1086#1090' '#39'+convert(varchar, d1.doc_date, 10' +
        '4) as base_doc_text'
      'from objects o1, objects o'
      'left outer join carriages car on (car.id = o.linked_object_id), '
      'v_lastobjectstates ls'
      'left outer join documents d on (d.id = ls.doc_id),'
      'containers c, cargos cr, tasks t '
      'left outer join documents d1 on (d1.id = t.basedoc_id)'
      'left outer join doccargosheet sh on (sh.task_id = t.id)'
      'left outer join docorderspec s on (s.cargo_id = t.object_id)'
      'left outer join docorder od on (od.id = s.doc_id)'
      'left outer join stations st on (st.id = od.station_id)'
      'left outer join directions dr on (dr.id = od.direction_id)'
      'where c.id = o.id and t.object_id = cr.id '
      'and o1.id = cr.id and o1.linked_object_id = c.id'
      'and o.id = ls.object_id and ls.task_id = t.id '
      'and not exists '
      
        '(select 1 from tasks t1, v_lastobjectstates ls1 where t1.id = ls' +
        '1.task_id and ls1.object_id = o.id and ls1.date_factexecution > ' +
        'ls.date_factexecution)'
      'and ('
      '    isnull(t.forwarder_id, sh.forwarder_id) = @contid'
      '    or'
      '    isnull(t.consignee_id, sh.consignee_id) = @contid'
      '    or'
      '    isnull(t.payer_id, sh.consignee_id) = @contid'
      ')'
      'and ('
      '  @datestart is null'
      '  or'
      '  ls.date_factexecution >= @datestart'
      ')'
      'order by ls.date_factexecution, c.cnum')
    SelectCommand.Parameters = <
      item
        Name = 'contid'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'datestart'
        Size = -1
        Value = Null
      end>
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
      Visible = False
    end
    inherited aDelete: TAction
      Enabled = False
      Visible = False
    end
    inherited aEdit: TAction
      Enabled = False
      Visible = False
    end
  end
  inherited exReport: TEXLReport
    Left = 430
  end
  object drvHistory: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select odh.*,'
      
        '(select code from doctypes t where t.id = d.doctype_id)+'#39' '#8470' '#39'+d.' +
        'doc_number as doc_code, '
      
        '(select code from objectstatekinds k where k.id = odh.object_sta' +
        'te_id) as state_code,'
      
        '(select p.train_num from docload l, docloadjoint j, loadplan p w' +
        'here l.joint_id = j.id and j.load_plan_id = p.id and l.id = d.id' +
        ') as train_num,'
      'odh.date_history,'
      
        '(select name from stations c where c.id = t.station_id) as stati' +
        'on_code,'
      
        '(select code from counteragents c where c.id = t.forwarder_id) a' +
        's forwarder_code,'
      
        '(select code from counteragents c where c.id = t.consignee_id) a' +
        's consignee_code,'
      
        '(select code from counteragents c where c.id = t.payer_id) as pa' +
        'yer_code,'
      
        '(select code from deliverytypes tp where tp.id = t.dlv_type_id) ' +
        'as delivery_code '
      'from documents d,  objectstatehistory odh'
      'left outer join tasks t on (t.id = odh.task_id) '
      'where d.id = odh.doc_id and odh.object_id = :object_id'
      'order by odh.date_factexecution desc')
    SelectCommand.Parameters = <
      item
        Name = 'object_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 328
    Top = 472
  end
  object meHistory: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvHistory
    BeforeOpen = meDataBeforeOpen
    AfterInsert = meDataAfterInsert
    BeforeEdit = meDataBeforeEdit
    AfterEdit = meDataAfterEdit
    BeforePost = meDataBeforePost
    AfterPost = meDataAfterPost
    Left = 376
    Top = 472
  end
  object dsHistory: TDataSource
    DataSet = meHistory
    Left = 432
    Top = 472
  end
end
