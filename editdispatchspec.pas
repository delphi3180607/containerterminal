﻿unit editdispatchspec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls, DBSQLLookUp;

type
  TFormEditDispatchSpec = class(TFormEdit)
    Label1: TLabel;
    edCarriage: TDBEditEh;
    Label2: TLabel;
    edDispatchNumbers: TDBEditEh;
    luMtuKind: TDBSQLLookUp;
    Label3: TLabel;
    luPictureKind: TDBSQLLookUp;
    Label4: TLabel;
    edContainer: TDBEditEh;
    Label5: TLabel;
    dtServeDate: TDBDateTimeEditEh;
    edServeSheet: TDBEditEh;
    Label6: TLabel;
    Label7: TLabel;
    ssPictureKinds: TADOLookUpSqlSet;
    ssMtuKind: TADOLookUpSqlSet;
    procedure luMtuKindKeyValueChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDispatchSpec: TFormEditDispatchSpec;

implementation

{$R *.dfm}

procedure TFormEditDispatchSpec.luMtuKindKeyValueChange(Sender: TObject);
begin
  luPictureKind.KeyValue := ssMtuKind.ListDataSet.FieldByName('picture_id').AsInteger;
end;

end.
