﻿inherited FormEditContainer: TFormEditContainer
  Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
  ClientHeight = 427
  ClientWidth = 526
  Font.Height = -12
  ExplicitWidth = 532
  ExplicitHeight = 455
  PixelsPerInch = 96
  TextHeight = 14
  object Label2: TLabel [0]
    Left = 6
    Top = 6
    Width = 41
    Height = 14
    Caption = #1053#1086#1084#1077#1088
  end
  object Label1: TLabel [1]
    Left = 6
    Top = 116
    Width = 129
    Height = 14
    Caption = #1056#1072#1079#1084#1077#1088' ('#1089#1087#1088#1072#1074#1086#1095#1085#1086')'
  end
  object Label4: TLabel [2]
    Left = 6
    Top = 317
    Width = 81
    Height = 14
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
  end
  object Label3: TLabel [3]
    Left = 6
    Top = 58
    Width = 106
    Height = 14
    Caption = #1042#1080#1076' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
  end
  object Label7: TLabel [4]
    Left = 160
    Top = 116
    Width = 22
    Height = 14
    Caption = #1042#1077#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel [5]
    Left = 272
    Top = 116
    Width = 119
    Height = 14
    Caption = #1043#1088#1091#1079#1086#1087#1086#1076#1100#1077#1084#1085#1086#1089#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label17: TLabel [6]
    Left = 6
    Top = 171
    Width = 124
    Height = 14
    Caption = #1044#1072#1090#1072' '#1080#1079#1075#1086#1090#1086#1074#1083#1077#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel [7]
    Left = 6
    Top = 227
    Width = 92
    Height = 14
    Caption = #1044#1072#1090#1072' '#1086#1089#1074#1080#1076#1077#1090'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel [8]
    Left = 182
    Top = 227
    Width = 158
    Height = 14
    Caption = #1057#1090#1080#1082#1077#1088' ('#1076#1072#1090#1072' '#1089#1083#1077#1076'. '#1086#1089#1074'.)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 386
    Width = 526
    TabOrder = 13
    ExplicitTop = 396
    ExplicitWidth = 541
    inherited btnCancel: TButton
      Left = 410
      ExplicitLeft = 425
    end
    inherited btnOk: TButton
      Left = 291
      ExplicitLeft = 306
    end
  end
  object edCNum: TDBEditEh [10]
    Left = 6
    Top = 24
    Width = 303
    Height = 22
    CharCase = ecUpperCase
    DataField = 'cnum'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    MaxLength = 11
    ParentFont = False
    TabOrder = 0
    Visible = True
    OnKeyPress = edCNumKeyPress
    EditMask = 'cccc9999999;0;?'
  end
  object neFootSize: TDBNumberEditEh [11]
    Left = 6
    Top = 135
    Width = 121
    Height = 22
    DataField = 'size'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object edNote: TDBMemoEh [12]
    Left = 6
    Top = 339
    Width = 508
    Height = 37
    AutoSize = False
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    Visible = True
    WantReturns = True
  end
  object leContainerKind: TDBSQLLookUp [13]
    Left = 6
    Top = 78
    Width = 505
    Height = 22
    DataField = 'kind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
    SqlSet = ssContainerKind
    RowCount = 0
  end
  object nuWeight: TDBNumberEditEh [14]
    Left = 160
    Top = 135
    Width = 98
    Height = 22
    DataField = 'container_weight'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object nuCarrying: TDBNumberEditEh [15]
    Left = 272
    Top = 135
    Width = 124
    Height = 22
    DataField = 'carrying'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = True
  end
  object dtDateMade: TDBDateTimeEditEh [16]
    Left = 6
    Top = 190
    Width = 153
    Height = 22
    DataField = 'date_made'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
  object dtDateStiker: TDBDateTimeEditEh [17]
    Left = 182
    Top = 246
    Width = 170
    Height = 22
    DataField = 'date_stiker'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 8
    Visible = True
  end
  object cbPicture: TDBCheckBoxEh [18]
    Left = 182
    Top = 190
    Width = 63
    Height = 17
    Caption = #1060#1086#1090#1086
    DataField = 'picture'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 6
  end
  object cbDefective: TDBCheckBoxEh [19]
    Left = 6
    Top = 287
    Width = 136
    Height = 17
    Caption = #1053#1077#1080#1089#1087#1088#1072#1074#1077#1085
    DataField = 'isdefective'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 10
  end
  object dtInspect: TDBDateTimeEditEh [20]
    Left = 6
    Top = 246
    Width = 170
    Height = 22
    DataField = 'date_inspection'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 7
    Visible = True
  end
  object cbNotReady: TDBCheckBoxEh [21]
    Left = 182
    Top = 287
    Width = 201
    Height = 17
    Caption = #1053#1077' '#1075#1086#1090#1086#1074' '#1082' '#1087#1077#1088#1077#1074#1086#1079#1082#1077
    DataField = 'isnotready'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 11
  end
  object cbACEP: TDBCheckBoxEh [22]
    Left = 367
    Top = 248
    Width = 140
    Height = 17
    Caption = 'ACEP'
    DataField = 'acep'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 9
  end
  inherited dsLocal: TDataSource
    Left = 354
    Top = 6
  end
  inherited qrAux: TADOQuery
    Left = 414
    Top = 6
  end
  object ssContainerKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from containerkinds order by code')
    DownSql.Strings = (
      'select * from containerkinds order by code')
    InitSql.Strings = (
      'select * from containerkinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 186
    Top = 50
  end
end
