﻿unit editreport;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, Vcl.StdCtrls, Vcl.DBCtrls, DBCtrlsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, DBSQLLookUp, Vcl.Mask, Vcl.Buttons,
  PngSpeedButton, Vcl.ComCtrls, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls,
  MemTableDataEh, MemTableEh, DataDriverEh, DBXDataDriverEh, ADODataDriverEh, functions;

type
  TFormEditReport = class(TFormEdit)
    pcAll: TPageControl;
    TabSheet1: TTabSheet;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label1: TLabel;
    btAdd: TPngSpeedButton;
    btDelete: TPngSpeedButton;
    sbRowUp: TSpeedButton;
    sbRowDown: TSpeedButton;
    edName: TDBEditEh;
    edTitle: TDBEditEh;
    cbCatalog: TDBSQLLookUp;
    dpFields: TDBGridEh;
    TabSheet4: TTabSheet;
    Panel1: TPanel;
    sbAddTemplate: TPngSpeedButton;
    sbDeleteTemplate: TPngSpeedButton;
    dgTemplates: TDBGridEh;
    TabSheet2: TTabSheet;
    plTop: TPanel;
    sbRefreshParams: TPngSpeedButton;
    dgParameters: TDBGridEh;
    Label5: TLabel;
    sbSQLFull: TSpeedButton;
    meSQL: TDBMemo;
    meTemplates: TMemTableEh;
    dsTemplates: TDataSource;
    meFields: TMemTableEh;
    dsFields: TDataSource;
    foDialog: TFileOpenDialog;
    meParams: TMemTableEh;
    dsParams: TDataSource;
    drvTemplates: TADODataDriverEh;
    drvFields: TADODataDriverEh;
    drvParams: TADODataDriverEh;
    ssCatalogs: TADOLookUpSqlSet;
    sbCatalog: TSpeedButton;
    sbEditParam: TPngSpeedButton;
    sbDeleteParam: TPngSpeedButton;
    sbRefreshFields: TPngSpeedButton;
    Panel2: TPanel;
    edParentIdName: TDBEditEh;
    edChildIdName: TDBEditEh;
    TabSheet3: TTabSheet;
    edAccessRoles: TDBEditEh;
    edGroupFields: TDBEditEh;
    procedure sbCatalogClick(Sender: TObject);
    procedure btAddClick(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure pcAllChange(Sender: TObject);
    procedure sbAddTemplateClick(Sender: TObject);
    procedure sbDeleteTemplateClick(Sender: TObject);
    procedure meFieldsAfterInsert(DataSet: TDataSet);
    procedure sbSQLFullClick(Sender: TObject);
    procedure sbEditParamClick(Sender: TObject);
    procedure meParamsAfterInsert(DataSet: TDataSet);
    procedure sbRefreshParamsClick(Sender: TObject);
    procedure sbDeleteParamClick(Sender: TObject);
    procedure dpFieldsKeyPress(Sender: TObject; var Key: Char);
    procedure dgTemplatesKeyPress(Sender: TObject; var Key: Char);
    procedure dgParametersKeyPress(Sender: TObject; var Key: Char);
    procedure meTemplatesAfterInsert(DataSet: TDataSet);
    procedure sbRowUpClick(Sender: TObject);
    procedure sbRefreshFieldsClick(Sender: TObject);
    procedure sbRowDownClick(Sender: TObject);
    procedure meTemplatesAfterPost(DataSet: TDataSet);
    procedure meFieldsAfterPost(DataSet: TDataSet);
    procedure meParamsAfterPost(DataSet: TDataSet);
    procedure btnOkClick(Sender: TObject);
  private
    procedure ActivateCurrentRecord(ds: TMemTableEh; drv: TADODataDriverEh);
  public
    { Public declarations }
  end;

var
  FormEditReport: TFormEditReport;

implementation

{$R *.dfm}

uses Catalogs, reports, editsql, editreportparam, ReportItem;

procedure TFormEditReport.btAddClick(Sender: TObject);
begin
  meFields.Append;
end;

procedure TFormEditReport.btDeleteClick(Sender: TObject);
begin
  meFields.Delete;
end;

procedure TFormEditReport.btnOkClick(Sender: TObject);
begin
  inherited;
  if (meFields.State in [dsEdit, dsInsert]) then meFields.Post;
  if (meTemplates.State in [dsEdit, dsInsert]) then meTemplates.Post;
  if (meParams.State in [dsEdit, dsInsert]) then meParams.Post;
end;

procedure TFormEditReport.dgParametersKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin

    if dsEdit = meParams.State then
      meParams.Post;

    if dsInsert = meParams.State then
      meParams.Post;

  end;
end;

procedure TFormEditReport.dgTemplatesKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    if dsEdit = meTemplates.State then
      meTemplates.Post;

    if dsInsert = meTemplates.State then
      meTemplates.Post;
  end;

end;

procedure TFormEditReport.dpFieldsKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin

    if dsEdit = meFields.State then
      meFields.Post;

    if dsInsert = meFields.State then
      meFields.Post;

  end;
end;

procedure TFormEditReport.FormShow(Sender: TObject);
begin

  inherited;

  drvFields.SelectCommand.Parameters.ParamByName('report_id').Value := current_id;
  meFields.Close;
  meFields.Open;

  drvTemplates.SelectCommand.Parameters.ParamByName('report_id').Value := current_id;
  meTemplates.Close;
  meTemplates.Open;

  drvParams.SelectCommand.Parameters.ParamByName('report_id').Value := current_id;
  meParams.Close;
  meParams.Open;

end;

procedure TFormEditReport.meFieldsAfterInsert(DataSet: TDataSet);
begin

  if current_id = 0 then
  begin
    FormReports.meData.Post;
    current_id := FormReports.meData.FieldByName('id').AsInteger;
  end;
  meFields.FieldByName('report_id').Value := current_id;

end;

procedure TFormEditReport.meFieldsAfterPost(DataSet: TDataSet);
begin
  ActivateCurrentRecord(meFields, drvFields);
end;

procedure TFormEditReport.meParamsAfterInsert(DataSet: TDataSet);
begin
  if current_id = 0 then
  begin
    FormReports.meData.Post;
    current_id := FormReports.meData.FieldByName('id').AsInteger;
  end;
  meParams.FieldByName('report_id').Value := current_id;
end;

procedure TFormEditReport.meParamsAfterPost(DataSet: TDataSet);
begin
  ActivateCurrentRecord(meParams, drvParams);
end;

procedure TFormEditReport.meTemplatesAfterInsert(DataSet: TDataSet);
begin
  if current_id = 0 then
  begin
    FormReports.meData.Post;
    current_id := FormReports.meData.FieldByName('id').AsInteger;
  end;
  meTemplates.FieldByName('report_id').Value := current_id;

end;

procedure TFormEditReport.meTemplatesAfterPost(DataSet: TDataSet);
begin
  ActivateCurrentRecord(meTemplates, drvTemplates);
end;

procedure TFormEditReport.pcAllChange(Sender: TObject);
begin
  if current_id = 0 then
  begin
    FormReports.meData.Post;
    current_id := FormReports.meData.FieldByName('id').AsInteger;
  end;
end;

procedure TFormEditReport.sbAddTemplateClick(Sender: TObject);
begin
  meTemplates.Append;
end;

procedure TFormEditReport.sbDeleteParamClick(Sender: TObject);
begin
  meParams.Delete;
end;

procedure TFormEditReport.sbDeleteTemplateClick(Sender: TObject);
begin
  meTemplates.Delete;
end;

procedure TFormEditReport.sbEditParamClick(Sender: TObject);
begin
  EE(nil, FormEditReportParam, meParams, 'report_params');
end;

procedure TFormEditReport.sbRefreshFieldsClick(Sender: TObject);
var i: integer;
begin
  if meFields.RecordCount = 0 then
  begin
    if not FormReportItem.meReport.Active then exit;
    for i := 0 to FormReportItem.meReport.FieldCount-1 do
    begin
      meFields.Append;
      meFields.FieldByName('field_name').Value := FormReportItem.meReport.Fields[i].FieldName;
      meFields.Post;
    end;
    meFields.Close;
    meFields.Open;
  end;
end;

procedure TFormEditReport.sbRefreshParamsClick(Sender: TObject);
var i: integer;
begin
  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Text := self.meSQL.Lines.Text;
  meParams.DisableControls;
  for i := 0 to qrAux.Parameters.Count-1 do
  begin
    meParams.First;
    if not meParams.Locate('param_name', qrAux.Parameters[i].Name,[]) then
    begin
      meParams.Append;
      meParams.FieldByName('param_name').Value := qrAux.Parameters[i].Name;
      meParams.Post;
    end;
  end;
  meParams.EnableControls;
  meParams.First;
end;

procedure TFormEditReport.sbRowDownClick(Sender: TObject);
var id: integer;
begin

  id := meFields.FieldByName('id').AsInteger;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update rf set field_order = rownum*10');
  qrAux.SQL.Add('from report_fields rf,');
  qrAux.SQL.Add('(SELECT ROW_NUMBER() OVER (ORDER BY field_order asc) AS rownum, id FROM report_fields where report_id = '+IntToStr(self.current_id)+') s1');
  qrAux.SQL.Add('where s1.id = rf.id and rf.report_id ='+IntToStr(self.current_id));
  qrAux.ExecSQL;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update report_fields set field_order = field_order + 15');
  qrAux.SQL.Add('where id = '+ meFields.FieldByName('id').AsString);
  qrAux.ExecSQL;

  meFields.Close;
  meFields.Open;

  meFields.Locate('id', id, []);


end;

procedure TFormEditReport.sbRowUpClick(Sender: TObject);
var id: integer;
begin

  id := meFields.FieldByName('id').AsInteger;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update rf set field_order = rownum*10');
  qrAux.SQL.Add('from report_fields rf,');
  qrAux.SQL.Add('(SELECT ROW_NUMBER() OVER (ORDER BY field_order asc) AS rownum, id FROM report_fields where report_id = '+IntToStr(self.current_id)+') s1');
  qrAux.SQL.Add('where s1.id = rf.id and rf.report_id ='+IntToStr(self.current_id));
  qrAux.ExecSQL;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('update report_fields set field_order = field_order - 15');
  qrAux.SQL.Add('where id = '+ meFields.FieldByName('rowid').AsString);
  qrAux.ExecSQL;

  meFields.Close;
  meFields.Open;

  meFields.Locate('rowid', id, []);

end;

procedure TFormEditReport.sbCatalogClick(Sender: TObject);
begin
  FormCatalogs.system_section := 'reports';
  SFDLE(FormCatalogs, cbCatalog);
end;

procedure TFormEditReport.sbSQLFullClick(Sender: TObject);
begin
  FormEditSQL.meSQL.Text := self.meSQL.Text;
  FormEditSQL.ShowModal;
  if FormEditSQL.ModalResult = mrOk then
  begin
    self.meSQL.Text := FormEditSQL.meSQL.Text;
  end;
end;


procedure TFormEditReport.ActivateCurrentRecord(ds: TMemTableEh; drv: TADODataDriverEh);
var currentid: integer;
begin

  if ds.FieldByName('id').AsInteger>0 then exit;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select IDENT_CURRENT(''report_fields'')');
  qrAux.Open;
  currentid := qrAux.Fields[0].AsInteger;

  if currentid <> 0 then
  begin
    drv.GetrecCommand.Parameters.ParamByName('current_id').Value := currentid;
    ds.RefreshRecord;
  end else
  begin
    RefreshRecord(ds,currentid);
  end;

end;


end.
