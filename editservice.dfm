﻿inherited FormEditService: TFormEditService
  Caption = #1059#1089#1083#1091#1075#1072
  ClientHeight = 313
  ExplicitWidth = 543
  ExplicitHeight = 341
  PixelsPerInch = 96
  TextHeight = 16
  object Label3: TLabel [0]
    Left = 15
    Top = 69
    Width = 98
    Height = 16
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  object Label2: TLabel [1]
    Left = 15
    Top = 14
    Width = 24
    Height = 16
    Caption = #1050#1086#1076
  end
  object Label1: TLabel [2]
    Left = 15
    Top = 142
    Width = 59
    Height = 16
    Caption = #1050#1086#1076' '#1074' 1C'
  end
  inherited plBottom: TPanel
    Top = 272
    TabOrder = 4
    ExplicitTop = 211
  end
  object edName: TDBEditEh [4]
    Left = 15
    Top = 91
    Width = 505
    Height = 24
    DataField = 'sname'
    DataSource = FormServiceKinds.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edCode: TDBEditEh [5]
    Left = 15
    Top = 32
    Width = 305
    Height = 24
    DataField = 'scode'
    DataSource = FormServiceKinds.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object edCode1C: TDBEditEh [6]
    Left = 15
    Top = 160
    Width = 305
    Height = 24
    DataField = 'code1C'
    DataSource = FormServiceKinds.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object cbExtra: TDBCheckBoxEh [7]
    Left = 15
    Top = 224
    Width = 273
    Height = 17
    Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1072#1103' '#1091#1089#1083#1091#1075#1072' ?'
    DataField = 'isextra'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 3
  end
end
