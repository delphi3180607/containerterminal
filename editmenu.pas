﻿unit editmenu;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask, DBSQLLookUp, Data.Win.ADODB;

type
  TFormEditMenu = class(TFormEdit)
    Label2: TLabel;
    Label1: TLabel;
    edCode: TDBEditEh;
    neOrder: TDBNumberEditEh;
    Label4: TLabel;
    ssOperationKind: TADOLookUpSqlSet;
    leOperationKind: TDBSQLLookUp;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditMenu: TFormEditMenu;

implementation

{$R *.dfm}

end.
