﻿inherited FormEditMtuAndPicture: TFormEditMtuAndPicture
  Caption = #1052#1058#1059' + '#1056#1080#1089#1091#1085#1086#1082
  ClientHeight = 177
  ClientWidth = 663
  ExplicitWidth = 669
  ExplicitHeight = 205
  PixelsPerInch = 96
  TextHeight = 16
  object Label3: TLabel [0]
    Left = 8
    Top = 14
    Width = 25
    Height = 14
    Caption = #1052#1058#1059
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel [1]
    Left = 8
    Top = 75
    Width = 52
    Height = 14
    Caption = #1056#1080#1089#1091#1085#1086#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbMTU: TSpeedButton [2]
    Left = 620
    Top = 34
    Width = 30
    Height = 22
    Caption = '...'
    OnClick = sbMTUClick
  end
  inherited plBottom: TPanel
    Top = 136
    Width = 663
    TabOrder = 2
    ExplicitTop = 136
    ExplicitWidth = 663
    inherited btnCancel: TButton
      Left = 547
      ExplicitLeft = 547
    end
    inherited btnOk: TButton
      Left = 428
      ExplicitLeft = 428
    end
  end
  object luMtuKind: TDBSQLLookUp [4]
    Left = 8
    Top = 34
    Width = 606
    Height = 22
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    MRUList.AutoAdd = False
    ParentFont = False
    TabOrder = 0
    Visible = True
    SqlSet = ssMtuKind
    OnKeyValueChange = luMtuKindKeyValueChange
  end
  object luPictureKind: TDBSQLLookUp [5]
    Left = 8
    Top = 94
    Width = 606
    Height = 22
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    MRUList.AutoAdd = False
    ParentFont = False
    TabOrder = 1
    Visible = True
    SqlSet = ssPictureKinds
  end
  inherited dsLocal: TDataSource
    Left = 320
    Top = 16
  end
  inherited qrAux: TADOQuery
    Left = 264
    Top = 8
  end
  object ssMtuKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select id, rtrim(code)+'#39' =  '#39'+ltrim(rtrim(name)) as codename, pi' +
        'cture_id from mtukinds '
      'where code like '#39'%@pattern%'#39' or name like '#39'%@pattern%'#39
      'order by code')
    DownSql.Strings = (
      
        'select id, rtrim(code)+'#39' =  '#39'+ltrim(rtrim(name)) as codename, pi' +
        'cture_id '
      'from mtukinds order by code'
      '')
    InitSql.Strings = (
      
        'select id, rtrim(code)+'#39' =  '#39'+ltrim(rtrim(name)) as codename, pi' +
        'cture_id '
      'from mtukinds where id = @id')
    KeyName = 'id'
    DisplayName = 'codename'
    Connection = dm.connMain
    Left = 129
    Top = 22
  end
  object ssPictureKinds: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select id, name as codename from picturekinds '
      'where code like '#39'%@pattern%'#39' or name like '#39'%@pattern%'#39
      'order by code')
    DownSql.Strings = (
      'select id, name as codename from picturekinds'
      'order by code'
      '')
    InitSql.Strings = (
      'select id, name as codename from picturekinds where id = @id'
      '')
    KeyName = 'id'
    DisplayName = 'codename'
    Connection = dm.connMain
    Left = 174
    Top = 85
  end
end
