﻿unit editdispatch;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBGridEh, DBCtrlsEh, Vcl.Buttons, Vcl.Mask,
  DBLookupEh, Functions, DBSQLLookUp;

type
  TFormEditDispatch = class(TFormEdit)
    luDispatchKind: TDBLookupComboboxEh;
    Label8: TLabel;
    Label3: TLabel;
    dtDocDate: TDBDateTimeEditEh;
    dtNotifDate: TDBDateTimeEditEh;
    Label5: TLabel;
    edDocNumber: TDBEditEh;
    Label1: TLabel;
    Label2: TLabel;
    edTrainNumber: TDBEditEh;
    Label9: TLabel;
    dtRemoveDate: TDBDateTimeEditEh;
    edRemoveSheet: TDBEditEh;
    Label10: TLabel;
    laStation: TDBSQLLookUp;
    ssStations: TADOLookUpSqlSet;
    Label6: TLabel;
    sbStations: TSpeedButton;
    procedure sbStationsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDispatch: TFormEditDispatch;

implementation

{$R *.dfm}

uses directions, stations;

procedure TFormEditDispatch.sbStationsClick(Sender: TObject);
begin
  SFDE(TFormStations,self.laStation);
end;

end.
