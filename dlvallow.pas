﻿unit dlvallow;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh;

type
  TFormDlvAllow = class(TFormEdit)
    cbDlvAllowed: TDBCheckBoxEh;
    edDvlAllowedNote: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormDlvAllow: TFormDlvAllow;

implementation

{$R *.dfm}

end.
