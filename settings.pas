﻿unit settings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions,
  Vcl.ActnList, Vcl.Mask, DBCtrlsEh;

type
  TFormSettings = class(TFormGrid)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSettings: TFormSettings;

implementation

{$R *.dfm}

end.
