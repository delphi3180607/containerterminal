﻿unit containers_old;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB, functions,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand, EXLReport;

type
  TFormContainersOld = class(TFormGrid)
    Panel1: TPanel;
    dgStates: TDBGridEh;
    Splitter2: TSplitter;
    drvOwner: TADODataDriverEh;
    meOwner: TMemTableEh;
    dsOwner: TDataSource;
    drvState: TADODataDriverEh;
    meStates: TMemTableEh;
    dsStates: TDataSource;
    plObjects: TPanel;
    Panel2: TPanel;
    sbAddObject: TPngSpeedButton;
    sbDeleteObject: TPngSpeedButton;
    sbEditObject: TPngSpeedButton;
    dgObjects: TDBGridEh;
    Splitter1: TSplitter;
    Panel3: TPanel;
    DBGridEh1: TDBGridEh;
    procedure sbDeleteObjectClick(Sender: TObject);
    procedure meDataAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormContainersOld: TFormContainersOld;

implementation

{$R *.dfm}

uses editcontainer, editowner;


procedure TFormContainersOld.Init;
begin
  inherited;
  self.formEdit := FormEditContainer;
  self.meOwner.Open;
  //self.meStates.Open;
  meDataAfterScroll(nil);
end;


procedure TFormContainersOld.meDataAfterScroll(DataSet: TDataSet);
begin

  if self.meData.FieldByName('id').AsString <> '' then
  begin

    meOwner.Filter := 'container_id = '+self.meData.FieldByName('id').AsString;
    meOwner.Filtered := true;

  end else
  begin

    meOwner.Filter := 'container_id = -1';
    meOwner.Filtered := true;

  end;

end;

procedure TFormContainersOld.sbDeleteObjectClick(Sender: TObject);
begin
  ED(self.meOwner);
end;

end.
