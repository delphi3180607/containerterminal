﻿inherited FormExtraServices: TFormExtraServices
  ActiveControl = dgData
  Caption = #1040#1082#1090#1099' '#1080' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1091#1089#1083#1091#1075#1080
  ClientHeight = 566
  ClientWidth = 859
  ExplicitWidth = 875
  ExplicitHeight = 604
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 525
    Width = 859
    ExplicitTop = 525
    ExplicitWidth = 859
    inherited btnOk: TButton
      Left = 624
      ExplicitLeft = 624
    end
    inherited btnCancel: TButton
      Left = 743
      ExplicitLeft = 743
    end
  end
  inherited plAll: TPanel
    Width = 859
    Height = 525
    ExplicitWidth = 859
    ExplicitHeight = 525
    object JvxSplitter1: TSplitter [0]
      Left = 0
      Top = 276
      Width = 859
      Height = 4
      Cursor = crVSplit
      Align = alBottom
    end
    inherited plTop: TPanel
      Width = 859
      Height = 28
      ExplicitWidth = 859
      ExplicitHeight = 28
      inherited sbDelete: TPngSpeedButton
        Height = 28
        ExplicitHeight = 25
      end
      inherited btFilter: TPngSpeedButton
        Height = 28
        Visible = False
      end
      inherited btExcel: TPngSpeedButton
        Height = 28
      end
      inherited sbAdd: TPngSpeedButton
        Height = 28
        ExplicitHeight = 26
      end
      inherited sbEdit: TPngSpeedButton
        Height = 28
        ExplicitHeight = 26
      end
      inherited Bevel2: TBevel
        Height = 28
        ExplicitHeight = 28
      end
      inherited btTool: TPngSpeedButton
        Left = 821
        Height = 22
        ExplicitLeft = 807
        ExplicitHeight = 22
      end
      inherited plCount: TPanel
        Left = 624
        Height = 28
      end
    end
    inherited dgData: TDBGridEh
      Top = 28
      Width = 859
      Height = 248
      Border.Color = clMedGray
      Border.EdgeBorders = [ebTop]
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 77
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 81
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'customer_code'
          Footers = <>
          Title.Caption = #1050#1083#1080#1077#1085#1090
          Width = 104
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'basedoc_kind'
          Footers = <>
          Title.Caption = #1054#1089#1085#1086#1074#1072#1085#1080#1077
          Width = 106
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'basedoc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1086#1089#1085#1086#1074#1072#1085#1080#1103
          Width = 106
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'summa'
          Footers = <>
          Title.Caption = #1057#1091#1084#1084#1072
          Width = 90
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_factexecution'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1080#1089#1087#1086#1083#1085#1077#1085#1080#1103
          Width = 138
        end>
    end
    inherited plHint: TPanel
      TabOrder = 3
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object plSpec: TPanel
      Left = 0
      Top = 280
      Width = 859
      Height = 245
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object dgSpec: TDBGridEh
        Left = 0
        Top = 26
        Width = 859
        Height = 219
        Align = alClient
        DataSource = dsSpec
        DynProps = <>
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        GridLineParams.ColorScheme = glcsClassicEh
        GridLineParams.DataHorzColor = 16773303
        GridLineParams.DataVertColor = 16773303
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
        ParentFont = False
        PopupMenu = pmSpec
        ReadOnly = True
        SelectionDrawParams.DrawFocusFrame = True
        SelectionDrawParams.DrawFocusFrameStored = True
        TabOrder = 0
        TitleParams.FillStyle = cfstGradientEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -11
        TitleParams.Font.Name = 'Verdana'
        TitleParams.Font.Style = [fsBold]
        TitleParams.ParentFont = False
        OnDblClick = sbEditSpecClick
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            Title.Caption = #1054#1073#1098#1077#1082#1090
            Width = 160
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'scode'
            Footers = <>
            Title.Caption = #1059#1089#1083#1091#1075#1072
            Width = 133
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'amount'
            Footers = <>
            Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
            Width = 122
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'price'
            Footers = <>
            Title.Caption = #1062#1077#1085#1072
            Width = 109
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'summa'
            Footers = <>
            Title.Caption = #1057#1091#1084#1084#1072
            Width = 137
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 859
        Height = 26
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object sbDeleteSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 28
          Top = 0
          Width = 32
          Height = 26
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          OnClick = sbDeleteSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002554944415478DA95924D68D36018C79F7C746D92A6B5DD
            86B2B56C7810A141F0E8459C7810BC09A2CECFA907F13211C7BC7A134410069E
            046FC26EE2556D657EEC24F5D04C2F62B77569BA43ED47DA244BF2C6E74D6CB1
            743BEC8524BCE4FDFD9E27FF278CEFFBB09F65CFDD11A3AF5E767B7B6640B0F0
            680AEF7178FA44DD15BE793BE7BBCE19E2B84571F9F5CAA080C21C57A67BDFB6
            15F6F9B301897DE3560EDF9798E808ECD4B69751F4587EFBE64728F80743340A
            54E7B7DB402C4BE15F2C0512EBFA5C8E61D9129790814F26C1DEAC80A5EBF789
            E32C8582878B319F900E0802CBC83290560B48A3811253C17681C22CC2914412
            ACAD0AD855DD435848E5DF39FD4F20F30F62BEE7198C20702C1EF49A0D70FFD4
            C1470187D248320156658B56F6B0FD782AFFDE1A0AD1BD7B2F465CCF604591E3
            5307003C82667A795899C2B5102E7CB0769F4218164ADC8E3035CDC2C63A4A3C
            80CC24348ADF09711D29FD316FFD7F7E4860CE5EC3C098526C3203502E871DA0
            A0B9B6064EDB50C6BE7E52F7149897AFE600615E94303019613F689F4AAC5A0D
            4C5D879D765B3958FCA60E09BA97AE04954338016655A3104D9B89A5D2AC3471
            08BA551DBA9A86124399F8A9AA7D41F7E26C30675E14118E83A9E9606D53D88D
            6368804F43184D737236031DAD0A1D0C947692F9FD4B0D04C6F90B336C84CF47
            24095CA30326C238BEF8E897952030FDD8711CB16B48E3E31C3D63D089D4EBA7
            B31BE542FF139A67CFA12492775A4D8215250C6B206DEDC8D1E03F199165AE07
            0F85583F796A06E1D5B1D5CF03706F55A60F53C989ECE67A61CF31EE77FD05EB
            B7706A5FA737EA0000000049454E44AE426082}
          ExplicitLeft = 76
          ExplicitTop = 4
          ExplicitHeight = 41
        end
        object sbExcelSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 100
          Top = 0
          Width = 44
          Height = 26
          Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          OnClick = sbExcelSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C0000031E4944415478DA8D936B6C0C5114C7FF7776B7BB53CF6E
            29F508A11E4D89E8AA846AB32869D89520A1691B1111AFB6D2342A21AA443C2A
            F1F8209E1104ED1791F824E852DD967835969616F5A6655955DD99E9ECEC5C67
            6737F5D5C9993B7327F7FCCEB9FF7B2EE39C63C1CEFAD5E2406B81C0E00060A3
            8701B191D3C07568BADEDEF5937B58F7AF72EF09978A98B10860D1BE46EFC512
            47AA20B0C11C3081A3CFB8F1CDE1EF09A1FA71173A3FF64A2F9A5BED0D275CBD
            7D0057D5BDEF359B33EC2DDF61121833325352842399350E39A4C139A11F6ADB
            653C68F5A3E3A32ABF6A6B4BB87B6C71AF01701FBC2F5D2E76886F02268AE546
            013A0D615D874A00450D237B820D2D7E0E8DA0B77C9DB8ED6DAFF1EC73E6B3BC
            43F3DFEADC3E7670BCCEC2B4DFF4143702C11FB8E3BB6080AC96FE04D2B0383D
            1F85D9EB696E86A4A8283CF244BBB93BCB12015C59B7B06CB96CEDC0FB402B9A
            9EBC27C1240C4D48841AD620493274D8B16CF67E30533C74AA2A6B9C152B0E3F
            566E54648A118035A44F09962F596A7AADD6A323E0475B7307C24C8699C5C16C
            4A42C1BC43548D19210A26C78C5166AC3C42801DB3C4E8295479E5C949E76C05
            D96BF1E8E73574FD09C3E77B0A5B9C1DC5EEF3246A1C42B44E0B933EF44E1B2E
            208F2AB8591103382B77A98A7ADE32313915A5AE4A5CFF7411BA64C5CBD6E7C8
            759460DAF845241E10D2A202A724C200782A09E0281FB18E529CCA4C73222005
            C0551D65EE5DB8D4761C66251E9FDEB5E375E74B9C2EF94027C3490F20350970
            EF7DA8D4ED996D003ED3BF91916689F898A43424278C465E663EEE7EF1E06BD7
            37A87E091B5C97E934B8D198D39205E4EC68501AAAB2A35B985BD128576F71D8
            FC3D16A3FB7E07BFE16AE31E04955F604CC0003111AB728EF67567CA5060EE56
            AFF2E0A8330AC8DA562FD794CFB405640BFEC7C627524C699DD27432270A9855
            5617ACD93E53ECEEB5B2FF014C1AC6F48C8DB5D2B3330B071880199B3C6FAF1F
            98332418E2FD682E44B4D0638B0789E6BE4B255A04DA1274314EF83375CD8D2F
            2DE772D30C4046516D9116E24B1963D349A6F87F57D1F0D8ED88CEC914EAC626
            01A8F69DCD3DFB17534092F0545E8BC50000000049454E44AE426082}
          ExplicitLeft = 113
          ExplicitTop = 2
          ExplicitHeight = 37
        end
        object sbAddSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 0
          Top = 0
          Width = 28
          Height = 26
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbAddSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002BE4944415478DAA5935D4893511CC61FA74E97390D6B9B
            1F516C2E334C2535295A8A792549908124A120D145A6303023233FA2444A08F5
            C6ECA2B430B01681994E4D29511321B134F3634A7EA382B9ED7DA7BEE7BC6FAF
            1B2E28BDA9FFB9389C8BE777FEFF739EC7451004FC4FB9FC09286BBBAEA13C4D
            2394A6134A5484A7E0285910CFB5E25E579EFACCB423A0AC2D378550BE72BF42
            E31FA28A80CC7D3736F80D2C5AE7D033DE8EC5E5A57942487675FA4BC35F0087
            98D64407C77969154761328F62C1360B8EE7E023F581421688AE512386C68719
            B1B38CE7971B0C4EC083D65CB5D87667A4FA648056198E0FF32D6019169C40E0
            E9E901415CEBFC3A42E561E81E69C3C8A4698E2344F73AAB75C20E2835EA0B7C
            7DF615C78724A377A913D60D060CC3202FB6C8DE5DC9E77CD85833246E6E8855
            E950DBF1088C852B6CC8E9B86307DC7D97331675382E785DB201D3F298383707
            8BD58C9284723B20CB7009DEBEEE203CC1216504D6AD0C1A7BDBC69BF59D5A3B
            A0B0E12A1B1F992C1BB10C61C5BA829BC7EF6DFB657A631AE4325F84F945A3AA
            E9B1ED7D6ECF2E3BE0D69B2BEC89F044D9B0791036BA8682A8FB3B02BCA57284
            2B6250F1B6CAF6F1469F039067C81CD31C0C0DE63D04CC58A6C0726B6016CDA8
            BE50EF146E96C44502ADDF11B06616AF7A1AC7BBF3FB1D23E8EBD30BD6246CF1
            D963A9E89A69C11AE520E55D507AE68913C0530152891BE2354978D85881558B
            B5B0EFF657C7235E7B91A6E628ED542AF606C4A84FA3EB47B3FDC1C4AF854037
            1102DC25AE885327C138D0884FDFBFCC899ED1F5177D9B701A29F3E979D148A4
            2650A5F4D28524627A7512B33F2721F03C82F6A811243F80A68106F40E0F32A2
            BD3344F16F236DD5C5EAA414D1EF95D495F8C7859E8252EEBF7939A657A660EC
            6F87D9CACE8BE2EC2DF1B6613A5799A01121699C334C440C13B58789F2A4AEBF
            6878E730FD4BFD027D1196F03509C5820000000049454E44AE426082}
          ExplicitLeft = 2
          ExplicitTop = 2
          ExplicitHeight = 43
        end
        object sbEditSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 60
          Top = 0
          Width = 26
          Height = 26
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbEditSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D4948445200000014000000140803000000BA57ED
            3F0000000373424954080808DBE14FE00000012F504C5445FFFFFFFEFEFEFDFD
            FDFCFCFCFBFBFBFAFAFAF9F9F9F8F8F8F7F7F7F6F6F6F5F5F5FFF3E5FDF7C7F4
            F4F4F3F3F3FDF6C1FFF2E0FDF6C3F2F2F2F1F1F1F0F0F0EFEFEFF6EEE4F5EBDF
            F1E9E0F2E9DEEAEAEAE9E9E9F3E6D8F5E5D4F9E3B1F2E5BCF3E4AEE3E3E3EEE2
            D4FCE0C3F0E3B9E2E2E2DFDFDFF8E25CF7E25AF7DF62F7E159F5DF5AF6DD6BEE
            D8B0F5DC5DE8D9A7F5DC5EF6D3ADF0D2B3F2D679EFD2ABE4D5ABE5D5B1E5D4AE
            EED680EED47CF1D47AEED378F0D378F9C990EDD077F9C793CCCCCCCBCBCBEDCA
            6CE7CB76C9C9C9E4C954D7C2A4E7BF8AC2C2C2E7BD76C9BDAFD7B975D1B68FD5
            B579DBB084CBA76ED8A457D7A262E2A150D4A16ADD9C50D29E65D89953DD994A
            D09C45DC9748CC9558C49748C78D47BD894BC98440A98842BD7919AD6A258466
            2D88602A5942173C25EF5A000000097048597300000B1200000B1201D2DD7EFC
            0000001C74455874536F6674776172650041646F62652046697265776F726B73
            2043533571B5E336000000D54944415478DA6364C0021871092ACAB340798FEE
            FC83093A1CFFFB1F2CC6E2780F2C0A127439C0F99781819381E38591F43FAEAB
            972182FB5981241B1068BEBCA9A9B60C2AC80C24993965399F69BEFA64B0162A
            08365056F486B4183FCBC1574882323217557E70CBFFD8FE1F2168C27352E98F
            80EC31F5BD084163D12352FF44650FBC7244083AB3FFFFF94C58ECF0873F4882
            311BDDB87E331E78CD802C187F97419EF9E06320C71E2198F8FFD7A7936F408E
            7040B2080E1CF740FCEE78F42F5C8CD9EA0044505586192EF8E7E96D3C818C01
            004FE4561533982BD20000000049454E44AE426082}
          ExplicitHeight = 43
        end
        object Bevel1: TBevel
          AlignWithMargins = True
          Left = 96
          Top = 0
          Width = 1
          Height = 26
          Margins.Left = 10
          Margins.Top = 0
          Margins.Bottom = 0
          Align = alLeft
          ExplicitLeft = 107
          ExplicitTop = 1
          ExplicitHeight = 41
        end
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'select e.*, (select code from counteragents c where c.id = e.cus' +
        'tomer_id) as customer_code'
      'from docextraservices e order by doc_date'
      '')
    SelectCommand.Parameters = <
      item
        Name = 'current_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update docextraservices'
      'set'
      '  doc_date = :doc_date,'
      '  doc_number = :doc_number,'
      '  customer_id = :customer_id,'
      '  basedoc_kind = :basedoc_kind,'
      '  basedoc_number = :basedoc_number,'
      '  basedoc_date = :basedoc_date,'
      '  date_factexecution = :date_factexecution,'
      '  summa = :summa'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'customer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'basedoc_kind'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'basedoc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'basedoc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_factexecution'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'summa'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into docextraservices'
      
        '  (doc_date, doc_number, customer_id,  basedoc_kind, basedoc_num' +
        'ber, '
      '   basedoc_date, date_factexecution, summa)'
      'values'
      
        '  (:doc_date, :doc_number, :customer_id, :basedoc_kind, :basedoc' +
        '_number, '
      '   :basedoc_date, :date_factexecution, :summa)')
    InsertCommand.Parameters = <
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'customer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'basedoc_kind'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'basedoc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'basedoc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_factexecution'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'summa'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from docextraservices where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select e.*, (select code from counteragents c where c.id = e.cus' +
        'tomer_id) as customer_code'
      'from docextraservices e where e.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 200
  end
  inherited meData: TMemTableEh
    Left = 248
  end
  inherited dsData: TDataSource
    Left = 304
  end
  inherited qrAux: TADOQuery
    Left = 360
  end
  inherited al: TActionList
    Top = 88
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select s.*, o.cnum, k.scode from docspecextraservices s, v_objec' +
        'ts o, servicekinds k '
      'where s.object_id = o.id and s.service_id = k.id')
    SelectCommand.Parameters = <>
    UpdateCommand.CommandText.Strings = (
      'update docspecextraservices'
      'set'
      '  doc_id = :doc_id,'
      '  object_id = :object_id,'
      '  service_id = :service_id,'
      '  amount = :amount,'
      '  price = :price,'
      '  summa = :summa'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'object_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'service_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'amount'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'price'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'summa'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into docspecextraservices'
      '  (doc_id, object_id, service_id, amount, price, summa)'
      'values'
      '  (:doc_id, :object_id, :service_id, :amount, :price, :summa)')
    InsertCommand.Parameters = <
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'object_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'service_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'amount'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'price'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'summa'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from docspecextraservices where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select s.*, o.cnum, k.scode from docspecextraservices s, v_objec' +
        'ts o, servicekinds k '
      
        'where s.object_id = o.id and s.service_id = k.id and s.id = :cur' +
        'rent_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 184
    Top = 392
  end
  object meSpec: TMemTableEh
    DetailFields = 'doc_id'
    FetchAllOnOpen = True
    MasterFields = 'id'
    MasterSource = dsData
    Params = <>
    DataDriver = drvSpec
    BeforeEdit = meDataBeforeEdit
    BeforePost = meSpecBeforePost
    AfterPost = meDataAfterPost
    Left = 232
    Top = 392
  end
  object dsSpec: TDataSource
    DataSet = meSpec
    Left = 288
    Top = 392
  end
  object pmSpec: TPopupMenu
    Left = 656
    Top = 384
    object N8: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 45
    end
    object N9: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 46
      OnClick = sbDeleteClick
    end
    object N10: TMenuItem
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
      ShortCut = 113
      OnClick = sbEditClick
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object N12: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1082#1086#1084' '#1087#1086' '#1085#1086#1084#1077#1088#1072#1084
    end
  end
end
