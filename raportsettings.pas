﻿unit raportsettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  Vcl.ExtCtrls, EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions,
  Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Buttons, PngSpeedButton, Vcl.StdCtrls, functions;

type
  TFormRaportSettings = class(TFormGrid)
    plSpec: TPanel;
    Splitter1: TSplitter;
    plLeft: TPanel;
    Splitter2: TSplitter;
    plRight: TPanel;
    Panel1: TPanel;
    sbDeleteOp: TPngSpeedButton;
    sbAddOp: TPngSpeedButton;
    sbEditOp: TPngSpeedButton;
    Bevel1: TBevel;
    Panel2: TPanel;
    PngSpeedButton7: TPngSpeedButton;
    sbAddDet: TPngSpeedButton;
    PngSpeedButton11: TPngSpeedButton;
    Bevel3: TBevel;
    dgOperations: TDBGridEh;
    dgDetails: TDBGridEh;
    drvOperations: TADODataDriverEh;
    dsOperations: TDataSource;
    meOperations: TMemTableEh;
    drvDetails: TADODataDriverEh;
    dsDetails: TDataSource;
    meDetails: TMemTableEh;
    drvStates: TADODataDriverEh;
    meStates: TMemTableEh;
    drvDlvTypes: TADODataDriverEh;
    meDlvTypes: TMemTableEh;
    dsStates: TDataSource;
    dsDlvTypes: TDataSource;
    procedure meOperationsBeforePost(DataSet: TDataSet);
    procedure sbAddOpClick(Sender: TObject);
    procedure dgOperationsKeyPress(Sender: TObject; var Key: Char);
    procedure N5Click(Sender: TObject);
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure meDetailsBeforePost(DataSet: TDataSet);
    procedure sbDeleteOpClick(Sender: TObject);
    procedure sbEditOpClick(Sender: TObject);
    procedure sbAddDetClick(Sender: TObject);
    procedure PngSpeedButton7Click(Sender: TObject);
    procedure PngSpeedButton11Click(Sender: TObject);
    procedure meDetailsAfterPost(DataSet: TDataSet);
    procedure meOperationsAfterPost(DataSet: TDataSet);
  private
    last_id: integer;
    last_state: TDataSetState;
  public
    procedure Init; override;
  end;

var
  FormRaportSettings: TFormRaportSettings;

implementation

{$R *.dfm}

procedure TFormRaportSettings.dgOperationsKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    if meData.State in [dsInsert, dsEdit] then meData.Post
  end;
end;

procedure TFormRaportSettings.Init;
begin
  inherited;
  meDlvTypes.Open;
  meStates.Open;
  meOperations.Open;
  //meDetails.Open;
  Screen.Cursor := crDefault;
end;


procedure TFormRaportSettings.meDataAfterScroll(DataSet: TDataSet);
begin

  inherited;

  drvOperations.SelectCommand.Parameters.ParamByName('head_id').Value := meData.FieldByName('id').AsInteger;
  meOperations.Close;
  meOperations.Open;

  drvDetails.SelectCommand.Parameters.ParamByName('head_id').Value := meData.FieldByName('id').AsInteger;
  meDetails.Close;
  meDetails.Open;

end;

procedure TFormRaportSettings.meDetailsAfterPost(DataSet: TDataSet);
begin

  if last_state = (dsEdit) then
  begin
    last_id := meDetails.FieldByName('id').AsInteger;
  end else
  begin
    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select @@IDENTITY;');
    qrAux.Open;
    last_id := qrAux.Fields[0].AsInteger;
  end;

  drvDetails.GetRecCommand.Parameters.ParamByName('current_id').Value := last_id;
  meDetails.RefreshRecord;

end;

procedure TFormRaportSettings.meDetailsBeforePost(DataSet: TDataSet);
begin

  meDetails.FieldByName('head_id').Value := meData.FieldByName('id').AsInteger;
  last_state := meDetails.State;

end;

procedure TFormRaportSettings.meOperationsAfterPost(DataSet: TDataSet);
begin

  if last_state = (dsEdit) then
  begin
    last_id := meOperations.FieldByName('id').AsInteger;
  end else
  begin
    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select @@IDENTITY;');
    qrAux.Open;
    last_id := qrAux.Fields[0].AsInteger;
  end;

  drvOperations.GetRecCommand.Parameters.ParamByName('current_id').Value := last_id;
  meOperations.RefreshRecord;
end;

procedure TFormRaportSettings.meOperationsBeforePost(DataSet: TDataSet);
begin

  meOperations.FieldByName('head_id').Value := meData.FieldByName('id').AsInteger;
  last_state := meOperations.State;

end;

procedure TFormRaportSettings.N5Click(Sender: TObject);
begin
  meDlvTypes.Close;
  meStates.Close;
  meDlvTypes.Open;
  meStates.Open;
  inherited;
  meOperations.Close;
  meOperations.Open;
end;

procedure TFormRaportSettings.PngSpeedButton11Click(Sender: TObject);
begin
  meDetails.Edit;
end;

procedure TFormRaportSettings.PngSpeedButton7Click(Sender: TObject);
begin
  ED(meDetails);
end;

procedure TFormRaportSettings.sbAddDetClick(Sender: TObject);
begin
  meDetails.Append;
end;

procedure TFormRaportSettings.sbAddOpClick(Sender: TObject);
begin
  meOperations.Append;
end;

procedure TFormRaportSettings.sbDeleteOpClick(Sender: TObject);
begin
  ED(meOperations);
end;

procedure TFormRaportSettings.sbEditOpClick(Sender: TObject);
begin
  meOperations.Edit;
end;

end.
