﻿inherited FormEditOperationKind: TFormEditOperationKind
  Caption = #1054#1087#1077#1088#1072#1094#1080#1103
  ClientHeight = 271
  ClientWidth = 542
  ExplicitWidth = 548
  ExplicitHeight = 299
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 15
    Top = 14
    Width = 24
    Height = 16
    Caption = #1050#1086#1076
  end
  object Label3: TLabel [1]
    Left = 15
    Top = 69
    Width = 98
    Height = 16
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  object Label1: TLabel [2]
    Left = 15
    Top = 138
    Width = 100
    Height = 16
    Caption = #1042#1080#1076' '#1089#1086#1089#1090#1086#1103#1085#1080#1103
  end
  inherited plBottom: TPanel
    Top = 230
    Width = 542
    TabOrder = 2
    inherited btnCancel: TButton
      Left = 426
    end
    inherited btnOk: TButton
      Left = 307
    end
  end
  object edCode: TDBEditEh
    Left = 15
    Top = 32
    Width = 305
    Height = 24
    DataField = 'code'
    DataSource = FormStateKinds.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object edName: TDBEditEh
    Left = 15
    Top = 88
    Width = 505
    Height = 24
    DataField = 'name'
    DataSource = FormStateKinds.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object DBSQLLookUp1: TDBSQLLookUp
    Left = 15
    Top = 160
    Width = 505
    Height = 24
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 3
    Text = 'DBSQLLookUp1'
    Visible = True
  end
end
