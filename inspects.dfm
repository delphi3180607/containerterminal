﻿inherited FormInspects: TFormInspects
  Caption = #1040#1082#1090#1099
  ClientHeight = 541
  ClientWidth = 896
  ExplicitWidth = 912
  ExplicitHeight = 579
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 500
    Width = 896
    ExplicitTop = 500
    ExplicitWidth = 896
    inherited btnOk: TButton
      Left = 661
      ExplicitLeft = 661
    end
    inherited btnCancel: TButton
      Left = 780
      ExplicitLeft = 780
    end
  end
  inherited plAll: TPanel
    Width = 896
    Height = 500
    ExplicitWidth = 896
    ExplicitHeight = 500
    object Splitter1: TSplitter [0]
      Left = 0
      Top = 234
      Width = 896
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 233
    end
    inherited plTop: TPanel
      Width = 896
      ExplicitWidth = 896
      inherited btFilter: TPngSpeedButton
        Visible = False
      end
      inherited btTool: TPngSpeedButton
        Left = 844
        ExplicitLeft = 844
      end
      object sbConfirm: TPngSpeedButton
        AlignWithMargins = True
        Left = 205
        Top = 3
        Width = 33
        Height = 23
        Hint = #1055#1088#1086#1074#1077#1089#1090#1080'/'#1087#1086#1076#1090#1074#1077#1088#1076#1080#1090#1100
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbConfirmClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000015000000150806000000A917A5
          960000000473424954080808087C086488000000097048597300000AEB00000A
          EB01828B0D5A0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000004B34944415478DA8D947D4C535718C6
          9F737B6F6F5B8A0A053A410427886C531451A793213AB20F95B80D37251AFD63
          59C4AFD438CD3E8C719B315B36C2D03822101D32E6E634EA266699188722A04B
          185304B68AE51B29B6505A5A7ADBDEBB730BA2F8B9933C499BF39EDF7DCEFBBE
          E7259224E1292B8E4A4D3581CA4165A1EAA6323FEE0079027475AFDBB8AC7FB0
          71A9D3D3A4117CFD208485869B00353BB17E7C60CA0906E4148DABF93FD07936
          7773CE4DEB91B9E6818BB0BB9BE093062890811C298140AD0843903A0111DA25
          F618DDBB0504D84EB7C4C7415799FACE1EAA337FADB20B4628880A0CE16894E2
          BE100992E4854F7403CC202203572141BFED5CA03222836EF63F085D70ABF7CC
          852B9D1FB2A2E804CB04C8C71F9D3306F078ED50DA92E1D0FE8CE8E03598373E
          FBB89A1BBBE21E5442B079E046C5A5F68DF10E4F1B58A2BDFF36A38184C0E9ED
          86CE9989453139F8AB7D3FEAC42D480ADF8359FAED068E51E6DE75BAE152EBCE
          038D96EFC0B341FE2B3E7A31704B3D503B67213DAE047C800AAC8F475563311A
          C94EA4459774458D5B304D86869AACD5E5973B0DF12E4F0F148CFAAEA707E00C
          44C606D1390E4B234B1112144EF32A8065399CADD98B5BCC5ECC09FF1833C3B6
          BE2743A75E37FFD050DE62808A0DF55FCF43ABED155DE0180D4D85C68F14890B
          03FD5E2C0EF9095326BE48815E5A4809276B0DB8C39F00C7F1D0A912315FFF65
          37F1787D4B2A3A3E3F53D5F6150295E17088163CA3781ED3D8DDA8EADB81416D
          0B78692C6CCE2E2C0C3A84C4E80CDA62A29C08FC58BB013DDC2968793DFD8848
          0D29F0CE949320ADB79B7655DBF67D7ACE940BB512D06301564E3C8AA0A050B4
          DFAE4751D33258551D7875CC56BC1E938D5EA70D014A370AAEA5A355BA4A7FD3
          244943C972FB806D4957409A5B1BB7DCF07C9F7BBA690F342A206A7039D63C77
          0C56C10C9D3614265B29AE36FF8AB7E30F40F2B7AB198575A9E8624CD0CAC0E1
          2691C10ADA6A592F50A8244849D53D797F1E356E40000BB86850AC2F132BE30E
          A3DF6781860FA07955D09271700B2D28AC4F83856B410037BA8E02751919381D
          A9BA6C90F2AA73939561778C97FB3F2066470778EAC64EC1339085B762BF459F
          CF0C2D170287D080C38D8B61E7BBE9331D7286FBFAC426006F4CDA01CE3AA35C
          FECFD61A2F16B4B325EB2A3B0FD261316480161A73F84DC888DD0F53DF35149B
          5221A8AC5033A381F2F252136AA516AF8416A0AD414AF637BF217F79CAEAB4CC
          3FFE1676A1DDF60F6DADA160970788C132748A151854F68227C340326A1440A0
          4A8DD88D818E49651F15AD4D1F79FBFB4F6FCC4B494A595F6937C032D035E278
          90E68AA3EE58F2F03B938FCAFB73C3D78374277ABE38FEFECB657BA4EA11286D
          7AE6C02F9B4B5F9A95FCDA4DEF4134DBCEFB2B2B579421F7DE971C2E5279A934
          BC1289BA1D707644A1F842EE8AA2ADD78F3F34FA2858B9B37869FEC284F4B5DA
          302B7AC4DFD169AF85D363F5575741C94A7A8360D5545AE964688414FC6B345A
          8F9CCFC9FAED33DBB1270D69A47C42D6BD397B93E1D9099313C2F57A702A3B24
          C60E512450487AF4F6BAD0DED5869AA6B2FCBCB2CA6F5C47A586A74DFEE19949
          74333761F6A2E9F337B3DC98390C98109A0D4A76DFB6384C2585A76E9D90CE4B
          758F3AFB1F1643191E2C961CD40000000049454E44AE426082}
        ExplicitLeft = 283
        ExplicitTop = 6
      end
      object sbCancelConfirm: TPngSpeedButton
        AlignWithMargins = True
        Left = 244
        Top = 3
        Width = 33
        Height = 23
        Hint = #1057#1085#1103#1090#1100' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1077
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbCancelConfirmClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000015000000150806000000A917A5
          960000000473424954080808087C086488000000097048597300000AC300000A
          C301342924AB0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E3360000046F4944415478DA85957B4C5B551CC7
          BFF7D1960263A5B6ACC2904D1C2F451E834D7182311297B8C9902D3A4784A844
          B788921883669145A3311A97C11FBA851933356294CDAD818D198D1B6E718B30
          9CC0204EC7FBD195969696BECBBDFE6E61B725433DC92F37E7FE7EBFCFFD9E73
          BEA7654451C47F0DBBDD9E4E0F35C55A8A790AEB8DF8F8DEA534936AB3DDD6C3
          FC1B94609580677B00A3DB7C988C0EC005061CACF195720D0137D2A3E77FA104
          7B10081CB2A063B3CD7315EEC02404D14B952CE2524EC9750AF3FBD02B8A9C6A
          E41EA5E91B14C28A5002EEF6E3FAE743EE6FA2DC8171B08C8A82A32A0E31C9C7
          E4BAF9B14A08821F60FD48882E4512F7CC8F34D94929C7322801B7F8F0E7B901
          C7615E10BCE05835442CE6E252BE90818ED12A493482413714AE1C78D43FC310
          B715EBF89AE394DE254309A8A5F28B7DF307333D413338463A9745A0665D58A1
          7DA41A0C0378176C58ED2DC5BD77EEC198F51C86C44664E86A60C0B63A2A6BBA
          05DD3711FCFE93D1B9335072AB64A0767D58E1EC7095A4017ED8A1F266A0C050
          2FE726ADDD18659A51A83D30AD40523663B3D9F47E983A7BE79A327D413B2D5B
          152AD4DF1D06CE0C2D0205C605D1178BA2848F49312BE707A6DB30C57E892CDD
          7348E4B6BF284133ECE819BC6A6E22951A2A6690901A069A6F54859E02E383D7
          B5808D9A77A18935C8F9DF278F614ED1099E57204E9986ECD87D3725E8136341
          637BBFB905D1BC0EC9E92D72C3F81F35F0AB4D5088B170F9ACC85BF5169234B9
          72FED2F8A7B07317A0566AC90D42C8768FEA3F0063B1991AAEB95BDFE99A6AC5
          2385BF441A1B76A7151D93F5702867B039E669E4AE79168105F228B9ECCCD09B
          B8290E224A211F0102C4DDBDFE2818B365E2D5E1E00F4D9CE18530D06A832728
          90021616CF350C982EA238E525B262A815EDC3B5B032D3502BC29697B82C39A3
          7C2D4169F9057497BB6E012FF71523592845F15DAF84BEAC089F0744D183B691
          3A387813A2B8E557535AC19AE854E4AFAE05D32D8B07AEF41787206E7AB301E5
          D894580DAA85D4BF203A707AF435B895B350B1D20722EE3A852B083C40BEE51D
          1B3A6568D07C10FD3346A8F8C517525196B202F7934D24AB9D9DAE4550E9808A
          8950B1341668452A851A059A7A9847F0B004C5A5F6FD258F17969EFF3BF019CC
          AE31A89696E62370121E8245E8855FE184925952C84410691EA0C8D73F0FEF4C
          E24F473ADE7B52BEFB277F6D3C9C9791F7729FBB09735E6B184C2A7882700C6E
          1B52AB9FF259BA1D6066D3025F9DFFA8B8E129E365195A5252C2BEFEE1CED3D9
          E9395B27164EC1347F25D4249D28CB2C0709A13D06D989475AEC1EF8660D38DB
          D3BA6BEF63878E87F6D816F1CB5D5656A6AC7ABBA839FF9E2D556A8D936EF96F
          B0B8FF8237E808394112CFD39EC7295390A0CC4194988BF18989D98EEE6FF7EE
          2F6FF94E3E38DB0A7F07074E9455176756D425EA9372745A2D78CE4D5BE75EF4
          22B470BA7C305BCDB83ED9D57CB2BBBFB1B5D63818D9BF2234A47A47D91D6915
          28CC4FBDAF96E36336B1607524D609C16F7278A6BF6EBB3075C2D860EC5FA9F7
          1FE859F7552DB79AED0000000049454E44AE426082}
        ExplicitLeft = 283
        ExplicitTop = 6
      end
    end
    inherited dgData: TDBGridEh
      Width = 896
      Height = 205
      OnDrawColumnCell = dgDataDrawColumnCell
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 97
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 97
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 99
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'fact_datetime'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1079#1072#1074#1077#1088#1096#1077#1085#1080#1103
          Width = 197
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'person_name'
          Footers = <>
          Title.Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1081
          Width = 273
        end>
    end
    object plSpec: TPanel
      Left = 0
      Top = 239
      Width = 896
      Height = 261
      Align = alBottom
      TabOrder = 2
      object Panel1: TPanel
        Left = 1
        Top = 1
        Width = 894
        Height = 30
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object sbDeleteSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 33
          Top = 3
          Width = 32
          Height = 24
          Align = alLeft
          Flat = True
          OnClick = sbDeleteSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
            0D0000000473424954080808087C086488000000097048597300000B1200000B
            1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000003804944415478DACD944F6C54451CC7
            BF33F3DEDB7DDBA5348B85A5DD5AFB0FDBAE848558A38D012F280703C40BE1A2
            3151C28518AA41A4468B89D58493A40749058D87CA81C6A01863A307046CB445
            AB586C698A6D714B6DD7EED276DFEEFB3333CE839274578F983893F726BF99DF
            7CF2FBFDE63B43A494B8978DFC67C0AEDADA86CDBB769EAAA88EC5FA4F7EF8D1
            1F63D78F72D7052780498180F271D4C785F47FD0198D94D7D6764637343C79FD
            527FCF8BE9F4EB05C08BAFB48D6CD9B1FD41E3D65F485EBCE05EE83D776AFCC6
            CDFD94518456006D9723C0D8A38D894D1FAC8DC737DC9C9C9A1CFBBEFFB5B69C
            7DA60038D87E78FAA1CDF1F5C18539606A1CCED5217C7D7EE8B781B97C6B8946
            323E3027048DAE2A3D934824766728B586AF5C39E8E6ED6ED7B5F172CE2E4CF9
            7845054C5D3BF8F0C6FAF6A6B0B786252760CD66F0C34C3E75D9124D26416393
            AE9FADACAC8C0C5BB981D1E4F4E3A6613825A5A5B09616FE097CBFBE0ED65C0A
            9A6323A2E3F8C630DB7F9FE0BA630B8CE6F892C96186358D0D0AF171DAE3CF11
            42A11B06CC92927F079E68A8879D4E23E8B9C82E65A1CEA22AAAD3CF1BA5D8C4
            3CA9D295B8C4D8B145C843868241D9011F180EC35ABC85B66260D703F7233B3F
            0FEA79D03C0E8B2054C5E5405CF06628179D11FCC8E8E0A8202DBABF41CD6994
            C0D095A582386C3B85C08EA0A1D4A0E440084C89AA06217EAAE362CD82B2AF32
            FA45A381A7AA4354EB5D44D78C230F68F40E54AA4E547F9BBB85C037033A841A
            35605DC2E3233542967115D5778C9E9E05D9AB4BECDABD4EEB0D9B8C9E18B713
            6AD72F7485A03B8B811D0AC801DACCC564B310315D27F899D0895F25A909A9F5
            BC5A5492DCB3AF2E787A68DE9B383FE7D40454CA77DB5BAA5405C0A3868E72C1
            7B5AA5DC6B06186628F1BE7110574BD7343F3B81DB07130BD2779F8E065EFD74
            3ABF2FE3CA6EB6CC7CA318D8A1D1B5DB2265C92A3BAB714DE05C16EFA43C1CD1
            88EFA442C71D3FA5223CB25A1B5D4D49F9B71927E2D7DC6FEDC5C0F7CA4A5FD8
            B9FD89EE55532398FF3329FB526E3525E486EFEE5FDF94C3C197DF112651B735
            AC0D8F587C8F2D71D6F779C92DAA6167C8DCF1FC91B62FD7F30CAEF5F5F1CF2E
            FF1E550F40EA6E8D2C5740C9D1D7E7EDB19C91633182B48AABD39F7B265F043C
            A442DFB2ADB5FFB1AD2D2D5F9DFCA4676C7AF6597D19E67B988C4157872057CC
            8509A82F71DF3E601701EFF97BF8BF05FE0DBECDBAE86B24A614000000004945
            4E44AE426082}
          ExplicitLeft = 76
          ExplicitTop = 4
          ExplicitHeight = 41
        end
        object sbExcelSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 111
          Top = 0
          Width = 44
          Height = 29
          Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
          Margins.Left = 1
          Margins.Top = 0
          Margins.Right = 1
          Margins.Bottom = 1
          Align = alLeft
          Flat = True
          OnClick = sbExcelSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000B1300000B
            1301009A9C18000001804944415478DAA593BD4E024110C767F6E00E82529A48
            6CF4012824684EC08258D8925098D84BA3D51D7E35C60EE2A13156125FC0D607
            30120F2205095A5A181B1B0B2316CA81B7E31E1F892602B930D56667E73733FF
            9D419F1E2318C3D00170E05504BC71199A44806807404079CBA8ECBA0957F425
            4324D5FE020E80F93EE309E2841CF1AD6D980FCE63BFA64639B00032B29B4715
            535CD1FF802E39271C3B00DCE25C0A3309A681A8DFDE5ED328E70657E0D846C4
            2B07E55B066C516863328010009B139EAB66A09C8243E0C3018E534BCC22F2BA
            3806BB37FC4969CB91C669E97DB806BFCCA7AB6722F3A67326C28C55308BA345
            EC076BEA32205C0B80D4C90FF02AD9F6FCD7C9DDCB48807F7B61C6E6DE9AE87D
            4AA43E171F12660C54478FD6472B09C55A7B30606B55919546C911504C57DD9A
            F4A84AC30EA1C7BE17D54C109161152AD98100BF1E4B7182F55EDFFBAD63F3B1
            37076931076BA22D02E93B6BE5ABCF234574378904978078E16E17282376218D
            636FA3A2C757C601FC004459E3B670DD18170000000049454E44AE426082}
          ExplicitLeft = 113
          ExplicitTop = 2
          ExplicitHeight = 37
        end
        object sbAddSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 1
          Top = 1
          Width = 28
          Height = 26
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = N8Click
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C0864880000027E4944415478DA75934F6813
            5110C6E765379B6C9B5453A2A68A076911F1107B502C8855C845A820251EBC89
            083D14D4A207A18A9E7A1005BD2854144FB607050551C4D47AA8ED6A6D7B6804
            D14BC13F4D9A9AEC66B3D924BBD9B7CE8BCD33ADEDC0B061BE995FDEFB6697C0
            DF0861466083E8ECEC944686878E6CAD7CD9EB48AD9AB1A5FB697B7BC70CD3C8
            4ACFC19CAA7E208400CF9A4AC0711C9724EF817FF60ED17D5190B23F80CADB68
            FE40FFB55DDDA786EA802E55D51449F282AAAA50AD56F9BFFB53634E68EAA240
            2D027ACF13F04F3F049A1C073BB4DB4D1DBF7D940332CBCB8AECF7432E9703DB
            B66B4504396D1F073C56F627A158124EDC073AFD184A9F2740344C300EF73DE0
            8042A1A00882004B4B4B1C804F7BC7D8192F3D390AC1E0262072108B1524DB60
            BEBC0BBF34F305071886A1783C1ED0348D5F01E79DA6F1EB44FEFADA03147DE9
            7B0EE4ED30946713406D01BEF75CBDF11F607171112CCBE21E54D2DFAC9DAF2E
            49A4688078EE19B86F103093007D4F2C3FB13DB68F038AC5A2C2DCC7AB30E739
            C0E7F3D15472CA0ABC7FE4F5E9BF05C715A1D07188BEAB848FF59FBF90E08065
            34D18F269AA6B90A500FDC4EC52C95DC70382CB6452202F6B66259E3001C54D8
            8F743ACD4D5C2F229108C8B20C922485D6053013D79E80794329ADE5CAB0BB19
            0325FD1FA05452C0756B1EB0C6C6E1402000994C060D4E4173731364B3D95BB1
            58EC32CA94034A087011A0EBFAAA133000F326994C42341A85B9B9B9D1DEDEDE
            D3F97CDE6EFC16BA908AAFB20468E6AA358AA2082D2D2DB0B0B000E572796470
            70F0ECE4E464B9AED701FB51FCC44EC056D918EC3AE88F3B3F3F7F331E8F5FC1
            6D541BF57AB70F53868D839952C074D70A7F002AF04A8E29B489580000000049
            454E44AE426082}
          ExplicitLeft = 2
          ExplicitTop = 2
          ExplicitHeight = 43
        end
        object sbEditSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 69
          Top = 1
          Width = 26
          Height = 26
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbEditSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D4948445200000014000000140803000000BA57ED
            3F0000000373424954080808DBE14FE00000012F504C5445FFFFFFFEFEFEFDFD
            FDFCFCFCFBFBFBFAFAFAF9F9F9F8F8F8F7F7F7F6F6F6F5F5F5FFF3E5FDF7C7F4
            F4F4F3F3F3FDF6C1FFF2E0FDF6C3F2F2F2F1F1F1F0F0F0EFEFEFF6EEE4F5EBDF
            F1E9E0F2E9DEEAEAEAE9E9E9F3E6D8F5E5D4F9E3B1F2E5BCF3E4AEE3E3E3EEE2
            D4FCE0C3F0E3B9E2E2E2DFDFDFF8E25CF7E25AF7DF62F7E159F5DF5AF6DD6BEE
            D8B0F5DC5DE8D9A7F5DC5EF6D3ADF0D2B3F2D679EFD2ABE4D5ABE5D5B1E5D4AE
            EED680EED47CF1D47AEED378F0D378F9C990EDD077F9C793CCCCCCCBCBCBEDCA
            6CE7CB76C9C9C9E4C954D7C2A4E7BF8AC2C2C2E7BD76C9BDAFD7B975D1B68FD5
            B579DBB084CBA76ED8A457D7A262E2A150D4A16ADD9C50D29E65D89953DD994A
            D09C45DC9748CC9558C49748C78D47BD894BC98440A98842BD7919AD6A258466
            2D88602A5942173C25EF5A000000097048597300000B1200000B1201D2DD7EFC
            0000001C74455874536F6674776172650041646F62652046697265776F726B73
            2043533571B5E336000000D54944415478DA6364C0021871092ACAB340798FEE
            FC83093A1CFFFB1F2CC6E2780F2C0A127439C0F99781819381E38591F43FAEAB
            972182FB5981241B1068BEBCA9A9B60C2AC80C24993965399F69BEFA64B0162A
            08365056F486B4183FCBC1574882323217557E70CBFFD8FE1F2168C27352E98F
            80EC31F5BD084163D12352FF44650FBC7244083AB3FFFFF94C58ECF0873F4882
            311BDDB87E331E78CD802C187F97419EF9E06320C71E2198F8FFD7A7936F408E
            7040B2080E1CF740FCEE78F42F5C8CD9EA0044505586192EF8E7E96D3C818C01
            004FE4561533982BD20000000049454E44AE426082}
          ExplicitLeft = 60
          ExplicitTop = 0
          ExplicitHeight = 43
        end
        object Bevel1: TBevel
          AlignWithMargins = True
          Left = 106
          Top = 0
          Width = 1
          Height = 30
          Margins.Left = 10
          Margins.Top = 0
          Margins.Bottom = 0
          Align = alLeft
          ExplicitLeft = 107
          ExplicitTop = 1
          ExplicitHeight = 41
        end
      end
      object dgSpec: TDBGridEh
        Left = 1
        Top = 31
        Width = 894
        Height = 229
        Align = alClient
        BorderStyle = bsNone
        DataSource = dsSpec
        DynProps = <>
        Flat = True
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
        PopupMenu = pmSpec
        TabOrder = 1
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -11
        TitleParams.Font.Name = 'Verdana'
        TitleParams.Font.Style = [fsBold]
        TitleParams.ParentFont = False
        Columns = <
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088
            Width = 138
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'techcond_code'
            Footers = <>
            Title.Caption = #1057#1090#1072#1090#1091#1089
            Width = 121
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'note'
            Footers = <>
            Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
            Width = 216
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select i.*, '
      
        '(case when d.isconfirmed = 1 then '#39#43#39' else '#39#39' end) as sta' +
        'te_code,'
      
        '(select person_name from persons p where  p.id =i.person_id) as ' +
        'person_name'
      'from docinspect i where objecttype = :objecttype '
      'order by doc_date')
    SelectCommand.Parameters = <
      item
        Name = 'objecttype'
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update docinspect'
      'set'
      '  doc_date = :doc_date,'
      '  doc_number = :doc_number,'
      '  fact_datetime = :fact_datetime,'
      '  person_id = :person_id'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'fact_datetime'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'person_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into docinspect'
      '  (doc_date, doc_number, fact_datetime, person_id, objecttype )'
      'values'
      
        '  (:doc_date, :doc_number, :fact_datetime, :person_id, :objectty' +
        'pe )')
    InsertCommand.Parameters = <
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'fact_datetime'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'person_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'objecttype'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from docinspect where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select i.*, '
      
        '(case when d.isconfirmed = 1 then '#39#43#39' else '#39#39' end) as sta' +
        'te_code,'
      
        '(select person_name from persons p where  p.id =i.person_id) as ' +
        'person_name'
      'from docinspect i where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
  inherited al: TActionList
    Left = 528
    Top = 200
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select i.*,'
      
        '(select code from techconditions c where c.id = techcond_id) as ' +
        'techcond_code,'
      '(select cnum from v_objects o where o.id = object_id) as cnum'
      'from docinspectspec i'
      '')
    SelectCommand.Parameters = <>
    UpdateCommand.CommandText.Strings = (
      'update docinspectspec'
      'set'
      '  object_id = :object_id,'
      '  techcond_id = :techcond_id,'
      '  note = :note'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'object_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'techcond_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into docinspectspec'
      '  (object_id, techcond_id, note)'
      'values'
      '  (:object_id, :techcond_id, :note)')
    InsertCommand.Parameters = <
      item
        Name = 'object_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'techcond_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from docinspectspec where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select i.*,'
      
        '(select code from techconditions c where c.id = techcond_id) as ' +
        'techcond_code,'
      '(select cnum from v_objects o where o.id = object_id) as cnum'
      'from docinspectspec i where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 88
    Top = 384
  end
  object meSpec: TMemTableEh
    DetailFields = 'parent_id'
    FetchAllOnOpen = True
    MasterFields = 'id'
    MasterSource = dsData
    Params = <>
    DataDriver = drvSpec
    BeforeEdit = meDataBeforeEdit
    BeforePost = meDataBeforePost
    AfterPost = meDataAfterPost
    Left = 144
    Top = 384
  end
  object dsSpec: TDataSource
    DataSet = meSpec
    Left = 200
    Top = 384
  end
  object pmSpec: TPopupMenu
    Left = 272
    Top = 384
    object N8: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 45
      OnClick = N8Click
    end
    object N9: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 46
      OnClick = sbDeleteClick
    end
    object N10: TMenuItem
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
      ShortCut = 113
      OnClick = sbEditClick
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object N12: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1082#1086#1084' '#1087#1086' '#1085#1086#1084#1077#1088#1072#1084
      OnClick = N12Click
    end
  end
end
