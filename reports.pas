﻿unit reports;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB, EhLibMTE,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.StdCtrls, DBXDataDriverEh, EXLReportExcelTLB,
  EXLReportBand, EXLReport, Vcl.Mask, DBCtrlsEh, DBSQLLookUp, functions, EXLReportDictionary;

type
  TFormReports = class(TFormGrid)
    Splitter1: TSplitter;
    plTool: TPanel;
    sbStart: TPngSpeedButton;
    btExport: TPngSpeedButton;
    sbClose: TPngSpeedButton;
    dsReport: TDataSource;
    qrReport: TADOQuery;
    meReport: TMemTableEh;
    sbUnWrap: TPngSpeedButton;
    sbWrap: TPngSpeedButton;
    dpReports: TDBGridEh;
    plMessage: TPanel;
    procedure sbStartClick(Sender: TObject);
    procedure sbCloseClick(Sender: TObject);
    procedure btExportClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure sbUnWrapClick(Sender: TObject);
    procedure sbWrapClick(Sender: TObject);
    procedure dgDataKeyPress(Sender: TObject; var Key: Char);
    procedure dpReportsDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
  private
    procedure MakeGroupStructure(group_fields: string);
  public
    procedure Init; override;
    procedure FillParamsForm;
    function GotParams: boolean;
    procedure GenerateColumns;
    procedure FormatColumns;
    procedure MakeExcelReport;
    function MakeControl(field_name: string; data_type: integer; data_length: integer; decimal_length: integer; dsParam: TDataSet): TControl;
    procedure AssignParameters(dsParams: TDataSet);
  end;

var
  FormReports: TFormReports;

implementation

{$R *.dfm}

uses editreport, ParamsFormUnit, dmu, SelectDocForm, selecttemplate, main;

procedure TFormReports.Init;
begin
  inherited;
  self.formEdit := FormEditReport;
  if not FormMain.isAdmin then
  begin
    al.Actions[0].Enabled := false;
    al.Actions[1].Enabled := false;
    al.Actions[2].Enabled := false;
  end;
end;

procedure TFormReports.sbCloseClick(Sender: TObject);
begin
  qrReport.Close;
  dpReports.Hide;
  btExport.Enabled := false;
  sbClose.Enabled := false;
  sbUnwrap.Hide;
  sbWrap.Hide;
end;

procedure TFormReports.sbStartClick(Sender: TObject);
begin
  qrReport.Close;
  meReport.Close;
  sbUnwrap.Hide;
  sbWrap.Hide;
  dpReports.Columns.Clear;
  meReport.Filter := '';
  dpReports.ClearFilter;
  dpReports.DataGrouping.GroupLevels.Clear;
  dpReports.DataGrouping.Footers.Clear;
  dpReports.DataGrouping.Active := false;
  dpReports.OddRowColor := $00F0F4F4;
  dpReports.GridLineParams.DataHorzColor := $00DEE2E2;
  dpReports.GridLineParams.DataVertColor := $00DEE2E2;
  FillParamsForm();
  if GotParams() then
  begin
    qrReport.SQL.Text := meData.FieldByName('sql_text').AsString;
    AssignParameters(ParamsForm.meLocal);
    try
      GenerateColumns;
      plMessage.Show;
      Application.ProcessMessages;
      Screen.Cursor := crHourGlass;
      qrReport.Open;
      meReport.Close;
      meReport.LoadFromDataSet(qrReport, 0, lmCopy, false);
      FormatColumns;

      meReport.TreeList.Active := false;
      meReport.TreeList.RefParentFieldName := '';
      meReport.TreeList.KeyFieldName := '';

      if (meData.FieldByName('tree_parent_id_name').AsString<>'')
      and (meData.FieldByName('tree_child_id_name').AsString<>'')
      then begin
        meReport.TreeList.RefParentFieldName := meData.FieldByName('tree_parent_id_name').AsString;
        meReport.TreeList.KeyFieldName := meData.FieldByName('tree_child_id_name').AsString;
        meReport.TreeList.Active := true;
      end;

      meReport.Open;

      if (meData.FieldByName('group_fields').AsString<>'')
      then begin
        MakeGroupStructure(meData.FieldByName('group_fields').AsString);
      end;

      dpReports.Show;
      btExport.Enabled := true;
      sbClose.Enabled := true;
    except
      on E : Exception do
      begin
        plMessage.Hide;
        Screen.Cursor := crDefault;
        ShowMessage('Ошибка при выполнении запроса: '+E.Message);
      end;
    end;

    plMessage.Hide;
    Screen.Cursor := crDefault;

  end;
end;


procedure TFormReports.sbUnWrapClick(Sender: TObject);
begin
  dpReports.DataGrouping.GroupLevels[0].ExpandNodes;
end;

procedure TFormReports.sbWrapClick(Sender: TObject);
begin
  dpReports.DataGrouping.GroupLevels[0].CollapseNodes;
end;

function TFormReports.GotParams: boolean;
begin

  result := false;

  if ParamsForm.meLocal.FieldCount<2 then
    result := true
  else begin
    ParamsForm.ShowModal;
    ParamsForm.meLocal.Post;
    if ParamsForm.ModalResult = mrOk then
      result := true;
  end;

end;


procedure TFormReports.GenerateColumns;
var c: TColumnEh;
begin

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select * from report_fields where report_id = :report_id order by field_order');
  qrAux.Parameters.ParamByName('report_id').Value := meData.FieldByName('id').AsInteger;
  qrAux.Open;

  dpReports.Columns.Clear;

  while not qrAux.Eof do
  begin

    c := dpReports.Columns.Add;

    c.Title.Caption := qrAux.FieldByName('field_caption').AsString;
    c.FieldName := qrAux.FieldByName('field_name').AsString;
    c.Width := qrAux.FieldByName('field_width').AsInteger;
    c.Title.TitleButton := true;
    c.TextEditing := true;

    if qrAux.FieldByName('field_align').AsInteger>0 then
      c.Alignment := TAlignment(qrAux.FieldByName('field_align').AsInteger - 1);

    qrAux.Next;

  end;

end;


procedure TFormReports.FormatColumns;
var i: integer; c: TColumnEh;
begin

   for i := 0 to dpReports.Columns.Count-1 do
   begin

    c := dpReports.Columns[i];

    if c.Field.DataType = ftFloat then
    begin
      c.DisplayFormat := '### ### ##0.00';
    end;


   end;

end;


procedure TFormReports.btExportClick(Sender: TObject);
begin
  MakeExcelReport;
end;


procedure TFormReports.dgDataKeyPress(Sender: TObject; var Key: Char);
begin

  if Key = #13 then
  self.sbStartClick(self);

end;

procedure TFormReports.dpReportsDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
var s: string;
begin
  s := meReport.FieldByName(dpReports.Columns[0].FieldName).AsString;
  if pos('ИТОГО',s)>0 then
  if ord(s[1])=8226 then
  begin
    dpReports.Canvas.Font.Style := [fsBold];
    dpReports.DefaultDrawColumnCell(Rect, dataCol, Column, State);
  end;
end;

procedure TFormReports.MakeExcelReport;
var grp1, grp2: string; sl: TStringList; rf: TEXLReportField; i: integer;
begin

  sl := TStringList.Create;
  sl.Delimiter := ',';
  sl.StrictDelimiter := true;
  sl.DelimitedText := meData.FieldByName('group_fields').AsString;

  grp1 := '';
  grp2 := '';

  if sl.Count>0 then grp1 := sl[0];
  if sl.Count>1 then grp2 := sl[1];

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select * from report_templates where report_id = :report_id');
  qrAux.Parameters.ParamByName('report_id').Value := meData.FieldByName('id').AsInteger;
  qrAux.Open;

  if qrAux.RecordCount =0 then
  begin
    ExportExcel(dpReports, meData.FieldByName('report_title').AsString);
    exit;
  end;

  exReport.Dictionary.Clear;
  rf := exReport.Dictionary.Add;
  rf.FieldName := 'print_title';
  rf.ValueAsString := meData.FieldByName('report_title').AsString;


  for i := 1 to ParamsForm.meLocal.FieldCount-1 do
  begin

    rf := exReport.Dictionary.Add;
    rf.FieldName := ParamsForm.meLocal.Fields[i].FieldName;
    rf.ValueAsString := VarToStr(ParamsForm.meLocal.Fields[i].Value);

  end;

  if qrAux.RecordCount > 1 then
  begin
     FormSelectTemplate.dsLocal.DataSet := qrAux;
     FormSelectTemplate.ShowModal;
     if FormSelectTemplate.ModalResult = mrOk then
     begin
      exReport.Template := ExtractFilePath(Application.ExeName) +'/templates/'+qrAux.FieldByName('template_file').AsString;
      exReport.Show('', false, grp1, grp2);
     end;
     exit;
  end;

  if qrAux.RecordCount = 1 then
  begin
     exReport.Template := ExtractFilePath(Application.ExeName) +'/templates/'+qrAux.FieldByName('template_file').AsString;
     exReport.Show('', false, grp1, grp2);
  end;

end;


procedure TFormReports.meDataBeforeOpen(DataSet: TDataSet);
begin
  drvData.SelectCommand.Parameters.ParamByName('role').Value := FormMain.currentRole;
  drvData.SelectCommand.Parameters.ParamByName('isadmin').Value := FormMain.isAdmin;
  inherited;
end;

procedure TFormReports.FillParamsForm;
var pl: TPanel; l: TLabel; i: integer; c: TControl; sumHeight: integer;
begin

  ParamsForm.meLocal.Close;
  ParamsForm.meLocal.FieldDefs.Clear;
  ParamsForm.meLocal.StoreDefs := true;
  ParamsForm.meLocal.FieldDefs.Add('dumb', TFieldType.ftString, 20, false);

  ParamsForm.ClearControls;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select * from report_params where report_id = :report_id order by param_order');
  qrAux.Parameters.ParamByName('report_id').Value := meData.FieldByName('id').AsInteger;
  qrAux.Open;

  sumHeight := 0;

  while not qrAux.Eof do
  begin

    pl := TPanel.Create(ParamsForm);
    pl.Height := 32;
    pl.AlignWithMargins := true;
    pl.Parent := ParamsForm.plWork;
    pl.Top := 200;
    pl.Align := alTop;
    pl.BorderStyle := bsNone;
    pl.BevelKind := bkNone;
    pl.BevelInner := bvNone;
    pl.BevelOuter := bvNone;
    pl.Show;

    sumHeight := sumHeight + 45;

    l := TLabel.Create(ParamsForm);
    l.Caption := qrAux.FieldByName('param_title').AsString;
    l.AutoSize := false;
    l.AlignWithMargins := true;
    l.Constraints.MaxWidth := 300;
    l.Constraints.MinWidth := 300;
    l.Show;
    l.Parent := pl;
    l.Align := alLeft;

    c := MakeControl(
        qrAux.FieldByName('param_name').AsString,
        qrAux.FieldByName('param_type').AsInteger,
        qrAux.FieldByName('param_field_length').AsInteger,
        qrAux.FieldByName('param_decimal_length').AsInteger,
        qrAux
        );

    c.Parent := pl;
    c.AlignWithMargins := true;
    c.Align := alClient;

    if qrAux.FieldByName('param_type').AsInteger = 3 then
      ParamsForm.meLocal.FieldDefs.Add(qrAux.FieldByName('param_name').AsString, TFieldType.ftDateTime, 0, false)
    else
      ParamsForm.meLocal.FieldDefs.Add(qrAux.FieldByName('param_name').AsString, TFieldType.ftVariant, 200, false);

    qrAux.Next;

  end;

  ParamsForm.meLocal.CreateDataSet;
  ParamsForm.meLocal.Open;
  ParamsForm.meLocal.Append;

  ParamsForm.Height := sumHeight + 140;

  ParamsForm.Caption := meData.FieldByName('report_name').AsString;

end;


function TFormReports.MakeControl(field_name: string; data_type: integer; data_length: integer; decimal_length: integer; dsParam: TDataSet): TControl;
var c: TControl; ss: TAdoLookUpSqlSet;
begin
  if data_type = 1 then
  begin
    c := TDBEditEh.Create(ParamsForm);
    TDbEditEh(c).DataSource := ParamsForm.dsLocal;
    TDbEditEh(c).DataField := field_name;
  end;
  if data_type = 2 then
  begin
    c := TDBNumberEditEh.Create(ParamsForm);
    TDBNumberEditEh(c).DataSource := ParamsForm.dsLocal;
    TDBNumberEditEh(c).DataField := field_name;
    TDBNumberEditEh(c).DecimalPlaces := decimal_length;
  end;
  if data_type = 3 then
  begin
    c := TDBDateTimeEditEh.Create(ParamsForm);
    TDBDateTimeEditEh(c).DataSource := ParamsForm.dsLocal;
    TDBDateTimeEditEh(c).DataField := field_name;
  end;
  if data_type = 4 then
  begin
    c := TDBSQLLookUp.Create(ParamsForm);
    TDBSQLLookUp(c).DataSource := ParamsForm.dsLocal;
    TDBSQLLookUp(c).DataField := field_name;
    ss := TAdoLookUpSqlSet.Create(c);

    ss.TmplSql.Text :=
    StringReplace(
       dsParam.FieldByName('param_sql').AsString,
       '<wherefilter>', 'where '+dsParam.FieldByName('param_display').AsString+' like ''%@pattern%''', [rfReplaceAll]);

    ss.TmplSql.Text :=
    StringReplace(
       ss.TmplSql.Text,
       '<andfilter>', 'and '+dsParam.FieldByName('param_display').AsString+' like ''%@pattern%''', [rfReplaceAll]);

    ss.DownSql.Text :=
    StringReplace(
       dsParam.FieldByName('param_sql').AsString,
       '<wherefilter>', '', [rfReplaceAll]);

    ss.DownSql.Text :=
    StringReplace(
       ss.DownSql.Text,
       '<andfilter>', '', [rfReplaceAll]);

    ss.InitSql.Text :=
    StringReplace(
       dsParam.FieldByName('param_sql').AsString,
       '<wherefilter>', 'where '+dsParam.FieldByName('param_key').AsString+' = @id', [rfReplaceAll]);

    ss.InitSql.Text :=
    StringReplace(
       ss.InitSql.Text,
       '<andfilter>', 'and '+dsParam.FieldByName('param_key').AsString+' = @id', [rfReplaceAll]);

    ss.KeyName := dsParam.FieldByName('param_key').AsString;
    ss.DisplayName := dsParam.FieldByName('param_display').AsString;

    ss.Connection := dm.connMain;

    TDBSQLLookUp(c).sqlSetLinked := ss;
    TDBSQLLookUp(c).SqlSet := ss;
    TDBSQLLookUp(c).DataField := field_name;

  end;
  result := c;
end;


procedure TFormReports.AssignParameters(dsParams: TDataSet);
var i: integer; param_name: string; parameter: TParameter;
begin


  for i := 0 to dsParams.FieldCount-1 do
  begin

    param_name := dsParams.Fields[i].FieldName;
    parameter := qrReport.Parameters.FindParam(param_name);

    if (param_name<>'dumb') then
    begin
       if dsParams.FieldByName(param_name).DataType = ftDateTime then
        qrReport.Parameters.ParamByName(param_name).Value := dsParams.FieldByName(param_name).AsDateTime
       else
        qrReport.Parameters.ParamByName(param_name).Value := dsParams.FieldByName(param_name).Value;
    end;
  end;

end;



procedure TFormReports.MakeGroupStructure(group_fields: string);
var sl: TStringList; grp1, grp2: string;

procedure MakeGroupLevel(fieldname: string);
var gl1: TGridDataGroupLevelEh;
begin

    gl1 := dpReports.DataGrouping.GroupLevels.Add;
    gl1.Column := dpReports.FindFieldColumn(fieldname);
    gl1.Color := $00DCEDED;
    gl1.Font.Style := [fsBold];

    dpReports.OddRowColor := clWindow;
    dpReports.EvenRowColor := clWindow;

    dpReports.GridLineParams.DataHorzColor := clSilver;
    dpReports.GridLineParams.DataVertColor := clSilver;

    sbUnwrap.Show;
    sbWrap.Show;

end;

procedure MakeFooter;
var ft1: TGridDataGroupFooterEh; i, j: integer;
begin

    ft1 := dpReports.DataGrouping.Footers.Add;
    ft1.Color := $00DCEDED;
    ft1.Font.Style := [fsBold];

    qrAux.First;

    while not qrAux.Eof do
    begin
      if qrAux.FieldByName('is_total').AsBoolean then
      begin

        i := ft1.ColumnItems.ItemIndexByColumn(dpReports.FindFieldColumn(qrAux.FieldByName('field_name').AsString));

        if meReport.FieldByName(qrAux.FieldByName('field_name').AsString).DataType = ftString then
          ft1.ColumnItems[i].ValueType := gfvCountEh
        else
          ft1.ColumnItems[i].ValueType := gfvSumEh;

      end;
      qrAux.Next;
    end;

    ft1.Visible := true;

end;


begin


  sl := TStringList.Create;
  sl.Delimiter := ',';
  sl.StrictDelimiter := true;
  sl.DelimitedText := meData.FieldByName('group_fields').AsString;

  grp1 := '';
  grp2 := '';

  if sl.Count>0 then grp1 := sl[0];
  if sl.Count>1 then grp2 := sl[1];

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select * from report_fields where report_id = :report_id order by field_order');
  qrAux.Parameters.ParamByName('report_id').Value := meData.FieldByName('id').AsInteger;
  qrAux.Open;

  dpReports.DataGrouping.Active := true;
  dpReports.DataGrouping.GroupPanelVisible := false;

  if grp1<>'' then
  begin
    MakeGroupLevel(grp1);
    MakeFooter;
  end;

  if grp2<>'' then
  begin
    MakeGroupLevel(grp2);
  end;



end;



end.
