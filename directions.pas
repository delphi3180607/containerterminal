﻿unit directions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand,
  EXLReport;

type
  TFormDirections = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormDirections: TFormDirections;

implementation

{$R *.dfm}

uses editcodename;

procedure TFormDirections.Init;
begin
  inherited;
  self.formEdit := FormEditCodeName;
end;

end.
