﻿unit editemailaccount;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, DBCtrlsEh, Data.DB, Data.Win.ADODB;

type
  TFormEditEmailAccount = class(TFormEdit)
    Label1: TLabel;
    edEmailSender: TDBEditEh;
    Label2: TLabel;
    edPassword: TDBEditEh;
    Label3: TLabel;
    edAddress: TDBEditEh;
    cbSSL: TDBCheckBoxEh;
    Label4: TLabel;
    nuPort: TDBNumberEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditEmailAccount: TFormEditEmailAccount;

implementation

{$R *.dfm}

end.
