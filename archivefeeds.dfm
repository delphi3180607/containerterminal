﻿inherited FormArchiveFeeds: TFormArchiveFeeds
  Caption = #1040#1088#1093#1080#1074#1099' '#1087#1086#1076#1072#1095#1080
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    object Splitter1: TSplitter [0]
      Left = 339
      Top = 46
      Height = 424
      Color = 15323838
      ParentColor = False
      StyleElements = [seFont, seBorder]
    end
    inherited plTop: TPanel
      inherited sbDelete: TPngSpeedButton
        Left = 388
        Visible = False
        ExplicitLeft = 264
      end
      inherited btFilter: TPngSpeedButton
        Left = 498
        OnClick = btFilterClick
        ExplicitLeft = 374
      end
      inherited btExcel: TPngSpeedButton
        Left = 466
        ExplicitLeft = 342
      end
      inherited sbAdd: TPngSpeedButton
        Left = 356
        Visible = False
        ExplicitLeft = 232
      end
      inherited sbEdit: TPngSpeedButton
        Left = 420
        Hint = ''
        Action = nil
        Visible = False
        ExplicitLeft = 296
      end
      inherited Bevel2: TBevel
        Left = 462
        ExplicitLeft = 338
      end
      object sbClearFilter: TPngSpeedButton [7]
        AlignWithMargins = True
        Left = 531
        Top = 1
        Width = 34
        Height = 26
        Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1080#1083#1100#1090#1088
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbClearFilterClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000002334944415478DA63FCFFFF3F032580
          91A1FD0A7E13FEA3A8C66A8005903E7E26499541969785285B1F7FFEC36032EF
          3688690931136AC8C9445506691EA021606F01A518FF33349F7C0F56526326C8
          C008147AFAE50F83F9BC3B20694B864A9D130847810CF9FBF7F8E1247506492E
          16B873BBCF400C2833166478F6ED0F83EDBC9B0C0CCCCC60CD98BE0219F2F3C7
          F13D49DA0CE2DC2C60C9C6A3AFC02E4A3714667059709D8181950DAE197BB080
          0CF9F2F9F896140386EBA7CF322C58B78FE1C6DD67C0B08484A68682C4774B5D
          A56573EBE253188CD3B0852BC4109B67878F5FBD729321CEDB92212DD096414A
          949FE1DAFDE70CF3361E6558B8E5388393A9C69D5D27AEA9623520A676EEFA95
          BBCE041C9C55C2A0F6EF13039B800003AF820258EED981030CA7BFB13244B7AF
          640872345C8FD50059AFF2DFD9618E2CE5F1EE0C6F2F5C60D81910C0E0BE6103
          C3CD050B187E7DF8C0E000A40BFB5631ACDF7FE10F5603184DD2FFBFDFDFCFC0
          CFC309E6830CD9ECE0C02005C46E408340E0FCCDC70CA6B16D0C380D78BABD93
          4152841FCC3F909000B6F90DD020904B840D0C188E5DBACB6097D2C380D30BA1
          2EC62CBD85A160CD20007236B2774A365E64D877FAC61F9C81B8E9E0C580355D
          190CAEE69A28727FFFFD6398B1E61043C594750C810E38021114BF36663AFFCF
          5EBCC9E065ADCB90E46FCDA02E2FCE70F9CE538659EB0E3300A38FC1D90C1A8D
          D8B233300CC074B2BFF5ECE397EF45DF78F0821326A72E2FF1DD4A4F69293021
          A5822C02009244E8A4AC1C3E1E0000000049454E44AE426082}
        ExplicitLeft = 457
        ExplicitTop = 4
      end
      inherited plCount: TPanel
        TabOrder = 1
      end
      object Panel6: TPanel
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 222
        Height = 29
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 10
        Margins.Bottom = 0
        Align = alLeft
        TabOrder = 0
        object dtStart: TDBDateTimeEditEh
          AlignWithMargins = True
          Left = 101
          Top = 4
          Width = 117
          Height = 21
          Margins.Left = 100
          ControlLabel.Width = 81
          ControlLabel.Height = 13
          ControlLabel.Caption = #1053#1072#1095#1080#1085#1072#1103' '#1089' '#1076#1072#1090#1099
          ControlLabel.Visible = True
          ControlLabelLocation.Position = lpLeftTextBaselineEh
          Align = alBottom
          DynProps = <>
          EditButtons = <>
          Kind = dtkDateEh
          TabOrder = 0
          Visible = True
        end
      end
      object btReset: TButton
        AlignWithMargins = True
        Left = 232
        Top = 2
        Width = 121
        Height = 25
        Margins.Left = 0
        Margins.Top = 2
        Margins.Bottom = 2
        Align = alLeft
        Caption = #1055#1077#1088#1077#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100
        TabOrder = 2
        OnClick = btResetClick
      end
    end
    inherited dgData: TDBGridEh
      Left = 342
      Top = 46
      Width = 478
      Height = 424
      AllowedOperations = [alopUpdateEh]
      IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      ReadOnly = False
      TitleParams.MultiTitle = True
      OnDblClick = nil
      Columns = <
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isinarchive'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1042' '#1072#1088#1093#1080#1074#1077' ?'
          Width = 57
        end
        item
          Alignment = taLeftJustify
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_income'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1087#1086#1076#1072#1095#1080
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 128
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_paper_number'
          Footers = <>
          Title.Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086#1076#1072#1095#1080
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 80
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num'
          Footers = <>
          Title.Caption = #1050#1055
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 112
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'pnum'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 101
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1042#1080#1076
          Width = 74
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_length'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1044#1083#1080#1085#1072
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1058#1077#1082#1091#1097#1080#1081' '#1089#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
          Width = 109
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_next_repair'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1044#1072#1090#1072' '#1086#1095#1077#1088#1077#1076'. '#1088#1077#1084#1086#1085#1090#1072
          Width = 70
        end
        item
          CellButtons = <>
          Color = 13036283
          DynProps = <>
          EditButtons = <>
          FieldName = 'techcond_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1058#1077#1093'. '#1089#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 74
        end
        item
          CellButtons = <>
          Checkboxes = False
          Color = 13036283
          DisplayFormat = 'dd.mm.yyyy'
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_inspection'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1044#1072#1090#1072' '#1090#1077#1093'. '#1089#1086#1089#1090#1086#1103#1085#1080#1103
          Width = 104
        end
        item
          CellButtons = <>
          Checkboxes = True
          Color = 13036283
          DynProps = <>
          EditButtons = <>
          FieldName = 'isdefective'
          Footers = <>
          ReadOnly = True
          TextEditing = True
          Title.Caption = #1053#1077'- '#1080#1089#1087#1088#1072'- '#1074#1077#1085'?'
          Width = 57
        end
        item
          CellButtons = <>
          Checkboxes = True
          Color = 8454143
          DynProps = <>
          EditButtons = <>
          FieldName = 'need_inspection'
          Footers = <>
          ReadOnly = True
          TextEditing = True
          Title.Caption = #1054#1089#1084#1086#1090#1088
          Width = 60
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'model_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1052#1086#1076#1077#1083#1100
          Width = 142
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'id'
          Footers = <>
        end>
    end
    inherited plHint: TPanel
      Left = 348
      Top = 373
      ExplicitLeft = 348
      ExplicitTop = 373
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object dgDates: TDBGridEh
      Left = 0
      Top = 46
      Width = 339
      Height = 424
      Align = alLeft
      AutoFitColWidths = True
      DataSource = dsDates
      DynProps = <>
      GridLineParams.DataHorzColor = 14540253
      GridLineParams.DataVertColor = 14540253
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
      ReadOnly = True
      TabOrder = 3
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -13
      TitleParams.Font.Name = 'Verdana'
      TitleParams.Font.Style = []
      TitleParams.ParentFont = False
      Columns = <
        item
          CellButtons = <>
          DisplayFormat = 'dd.mm.yyyy'
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_income'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1087#1086#1076#1072#1095#1080
          Width = 108
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnt_linked'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Footers = <>
          Title.Caption = #1042' '#1072#1088#1093#1080#1074#1077
          Width = 79
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnt_free'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Footers = <>
          Title.Caption = #1057#1074#1086#1073#1086#1076#1085#1086
          Width = 74
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object plStatus: TPanel
      Left = 0
      Top = 29
      Width = 820
      Height = 17
      Align = alTop
      BevelOuter = bvLowered
      ParentColor = True
      TabOrder = 4
    end
  end
  inherited pmGrid: TPopupMenu
    Left = 384
    object N8: TMenuItem [3]
      Caption = #1042#1082#1083#1102#1095#1080#1090#1100' '#1074' '#1072#1088#1093#1080#1074' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1077
      OnClick = N8Click
    end
    object N9: TMenuItem [4]
      Caption = #1048#1089#1082#1083#1102#1095#1080#1090#1100' '#1080#1079' '#1072#1088#1093#1080#1074#1072' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1077
      OnClick = N9Click
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'declare @dateincome datetime;'
      ''
      'select @dateincome = :dateincome;'
      ''
      'select car.id, a.id as archive_id, '
      '(case when a.id is null then 0 else 1 end) as isinarchive,'
      'isnull(a.date_income, car.date_income) as date_income,'
      'a.car_paper_number as car_paper_number,'
      'isnull(a.train_num, car.train_num) as train_num,'
      'car.pnum, '
      
        '(select model_code from carriagemodels m where m.id = car.model_' +
        'id) as model_code,'
      
        '(select car_length from carriagemodels m where m.id = car.model_' +
        'id) as car_length,'
      
        '(select code from counteragents cr where cr.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from carriagekinds k where k.id  = car.kind_id) as ' +
        'kind_code'
      'from objects o, carriages car '
      'left outer join '
      
        '(select * from archivefeed a where convert(date, a.date_income) ' +
        '= convert(date, @dateincome)) a'
      'on (car.id = a.carriage_id)'
      'where o.id = car.id'
      
        'and convert(date, isnull(a.date_income, car.date_income)) = conv' +
        'ert(date, @dateincome)'
      'and dbo.filter_objects(:guid, car.pnum) = 1'
      'order by car.pnum ')
    SelectCommand.Parameters = <
      item
        Name = 'dateincome'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'guid'
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      
        'declare @id int, @date_income datetime,  @car_paper_number varch' +
        'ar(50), @train_num varchar(50), @archive_id int;'
      ''
      
        'select @id = :id, @date_income = :date_income, @car_paper_number' +
        ' = :car_paper_number, @train_num = :train_num, @archive_id = :ar' +
        'chive_id;'
      ''
      
        'if (@archive_id is null) and isnull(ltrim(@car_paper_number), '#39#39 +
        ') <> '#39#39
      
        '  insert into archivefeed (carriage_id, date_income, car_paper_n' +
        'umber, train_num)'
      '  values (@id, @date_income, @car_paper_number, @train_num);'
      'else'
      '  update archivefeed'
      '  set'
      '     date_income= @date_income,'
      '    car_paper_number = @car_paper_number, '
      '     train_num = @train_num'
      '  where'
      '   id = @archive_id;'
      ''
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end
      item
        Name = 'date_income'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_paper_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'train_num'
        Size = -1
        Value = Null
      end
      item
        Name = 'archive_id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'declare @dateincome datetime;'
      ''
      'select @dateincome = :dateincome;'
      ''
      'select car.id, a.id as archive_id, '
      '(case when a.id is null then 0 else 1 end) as isinarchive,'
      'isnull(a.date_income, car.date_income) as date_income,'
      'a.car_paper_number as car_paper_number,'
      'isnull(a.train_num, car.train_num) as train_num,'
      'car.pnum, '
      
        '(select model_code from carriagemodels m where m.id = car.model_' +
        'id) as model_code,'
      
        '(select car_length from carriagemodels m where m.id = car.model_' +
        'id) as car_length,'
      
        '(select code from counteragents cr where cr.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from carriagekinds k where k.id  = car.kind_id) as ' +
        'kind_code'
      'from objects o, carriages car '
      'left outer join '
      
        '(select * from archivefeed a where convert(date, a.date_income) ' +
        '= convert(date, @dateincome)) a'
      'on (car.id = a.carriage_id)'
      'where o.id = car.id'
      
        'and convert(date, isnull(a.date_income, car.date_income)) = conv' +
        'ert(date, @dateincome)'
      'and car.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'dateincome'
        Size = -1
        Value = Null
      end
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 400
    Top = 272
  end
  inherited meData: TMemTableEh
    Left = 448
    Top = 272
  end
  inherited dsData: TDataSource
    Left = 504
    Top = 272
  end
  inherited qrAux: TADOQuery
    Left = 440
  end
  inherited drvForms: TADODataDriverEh
    Left = 580
  end
  inherited meForms: TMemTableEh
    Left = 628
  end
  inherited dsForms: TDataSource
    Left = 676
  end
  inherited al: TActionList
    Left = 96
    Top = 187
    inherited aAdd: TAction
      Caption = 'aAdd'
      Enabled = False
    end
    inherited aDelete: TAction
      Caption = 'aDelete'
      Enabled = False
    end
    inherited aEdit: TAction
      Caption = 'aEdit'
      Enabled = False
    end
  end
  inherited exReport: TEXLReport
    Left = 486
  end
  object dsDates: TDataSource
    DataSet = meDates
    Left = 184
    Top = 256
  end
  object drvDates: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select d.dday as date_income,'
      
        'sum((case when isnull(a.id,0) = 0 then 0 else 1 end)) as cnt_lin' +
        'ked,'
      
        'sum((case when isnull(car.id,0) = 0 then 0 else 1 end))-sum((cas' +
        'e when isnull(a.id,0) = 0 then 0 else 1 end)) as cnt_free'
      'from dates d'
      
        'left outer join carriages car  on (d.dday = convert(datetime, co' +
        'nvert(date, car.date_income)))'
      
        'left outer join archivefeed a on (car.id = a.carriage_id and d.d' +
        'day = convert(datetime, convert(date, a.date_income)))'
      'where d.dday  >= :datestart and d.dday<=getdate()'
      'and dbo.filter_objects(:guid, car.pnum) = 1'
      'group by d.dday'
      'order by 1 desc')
    SelectCommand.Parameters = <
      item
        Name = 'datestart'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'guid'
        Size = -1
        Value = Null
      end>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.CommandText.Strings = (
      'select d.dday as date_income,'
      
        'sum((case when isnull(a.id,0) = 0 then 0 else 1 end)) as cnt_lin' +
        'ked,'
      
        'sum((case when isnull(car.id,0) = 0 then 0 else 1 end))-sum((cas' +
        'e when isnull(a.id,0) = 0 then 0 else 1 end)) as cnt_free'
      'from dates d'
      
        'left outer join carriages car on  (d.dday = convert(datetime, co' +
        'nvert(date, car.date_income)))'
      
        'left outer join archivefeed a on (car.id = a.carriage_id and d.d' +
        'day = convert(datetime, convert(date, a.date_income)))'
      'where d.dday  = :dateincome'
      'and dbo.filter_objects(:guid, car.pnum) = 1'
      'group by d.dday'
      'order by 1 desc')
    GetrecCommand.Parameters = <
      item
        Name = 'dateincome'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'guid'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 88
    Top = 256
  end
  object meDates: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvDates
    BeforeOpen = meDatesBeforeOpen
    AfterOpen = meDatesAfterOpen
    AfterScroll = meDatesAfterScroll
    Left = 136
    Top = 256
  end
end
