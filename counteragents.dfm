﻿inherited FormCounteragents: TFormCounteragents
  ActiveControl = dgData
  Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090#1099
  ClientHeight = 709
  ClientWidth = 1023
  ExplicitWidth = 1039
  ExplicitHeight = 747
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 211
    Top = 0
    Width = 5
    Height = 668
    Color = clWhite
    ParentColor = False
    Visible = False
    ExplicitLeft = 217
    ExplicitTop = 43
    ExplicitHeight = 367
  end
  inherited plBottom: TPanel
    Top = 668
    Width = 1023
    ExplicitTop = 668
    ExplicitWidth = 1023
    inherited btnOk: TButton
      Left = 788
      ExplicitLeft = 788
    end
    inherited btnCancel: TButton
      Left = 907
      ExplicitLeft = 907
    end
  end
  inherited plAll: TPanel
    Left = 216
    Width = 807
    Height = 668
    ExplicitLeft = 216
    ExplicitWidth = 807
    ExplicitHeight = 668
    inherited plTop: TPanel
      Width = 807
      ExplicitWidth = 807
      inherited btTool: TPngSpeedButton
        Left = 575
        ExplicitLeft = 553
      end
      inherited plCount: TPanel
        Left = 613
        ExplicitLeft = 613
      end
    end
    inherited dgData: TDBGridEh
      Width = 807
      Height = 639
      IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      SearchPanel.FilterOnTyping = True
      SearchPanel.PersistentShowing = True
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'INN'
          Footers = <>
          Title.Caption = #1048#1053#1053
          Width = 117
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'contract_num'
          Footers = <>
          Title.Caption = #1044#1086#1075#1086#1074#1086#1088
          Width = 119
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1052#1085#1077#1084#1086#1082#1086#1076
          Width = 138
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 237
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'email'
          Footers = <>
          Title.Caption = 'Email'
          Width = 115
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'els_code'
          Footers = <>
          Title.Caption = #1045#1051#1057' '#1082#1086#1076
          Width = 110
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'location'
          Footers = <>
          Title.Caption = #1040#1076#1088#1077#1089
          Width = 143
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'chief'
          Footers = <>
          Title.Caption = #1056#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1100
          Width = 132
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'manager'
          Footers = <>
          Title.Caption = #1050#1086#1085#1090#1072#1082#1090#1085#1086#1077' '#1083#1080#1094#1086
          Width = 146
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  object plLeft: TPanel [3]
    Left = 0
    Top = 0
    Width = 211
    Height = 668
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object Bevel1: TBevel
      Left = 210
      Top = 25
      Width = 1
      Height = 643
      Align = alRight
      ExplicitLeft = 215
      ExplicitHeight = 342
    end
    object plTool: TPanel
      Left = 0
      Top = 0
      Width = 211
      Height = 25
      Align = alTop
      Caption = #1060#1080#1083#1100#1090#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object plFilter: TPanel
      Left = 0
      Top = 25
      Width = 210
      Height = 643
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
    end
  end
  inherited pmGrid: TPopupMenu
    Left = 320
    object N10: TMenuItem [3]
      Caption = #1054#1073#1098#1077#1076#1080#1085#1080#1090#1100' '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090#1072
      Visible = False
      OnClick = N10Click
    end
    object N11: TMenuItem [4]
      Caption = #1057#1075#1088#1091#1087#1087#1080#1088#1086#1074#1072#1090#1100
      OnClick = N11Click
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select *, '
      
        '(select 1 where exists (select 1 from counteragents c2 where c2.' +
        'parent_id = c.id)) as exists_child'
      'from counteragents c '
      'order by c.code')
    UpdateCommand.CommandText.Strings = (
      'update counteragents set '
      'INN= :INN, '
      'code = :code, '
      'name = :name, '
      'location = :location, '
      'chief = :chief, '
      'manager = :manager, '
      'folder_id = :folder_id, '
      'email = :email,'
      'contacts = :contacts, '
      'contract_num = :contract_num, '
      'contract_date = :contract_date,'
      'els_code = :els_code, '
      'station_id = :station_id'
      'where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'INN'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 14
        Value = Null
      end
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'location'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'chief'
        Size = -1
        Value = Null
      end
      item
        Name = 'manager'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'email'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'contacts'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'contract_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'contract_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'els_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'station_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      
        'insert into counteragents (INN, code, name, location, chief, man' +
        'ager, folder_id, email, contacts, contract_num, contract_date, e' +
        'ls_code, station_id)'
      
        'values ( :INN, :code, :name, :location, :chief, :manager, :folde' +
        'r_id, :email, :contacts, :contract_num, :contract_date, :els_cod' +
        'e, :station_id )')
    InsertCommand.Parameters = <
      item
        Name = 'INN'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 14
        Value = Null
      end
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'location'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'chief'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'manager'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'email'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'contacts'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'contract_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'contract_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'els_code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'station_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from counteragents where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select *, '
      
        '(select 1 where exists (select 1 from counteragents c2 where c2.' +
        'parent_id = c.id)) as exists_child'
      ' from counteragents c where c.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 360
  end
  inherited meData: TMemTableEh
    DetailFields = 'folder_id'
    MasterFields = 'id'
    TreeList.Active = True
    TreeList.KeyFieldName = 'id'
    TreeList.RefParentFieldName = 'parent_id'
    Left = 408
  end
  inherited dsData: TDataSource
    Left = 464
  end
  inherited qrAux: TADOQuery
    Left = 280
  end
  inherited al: TActionList
    Left = 232
  end
  inherited exReport: TEXLReport
    Left = 654
  end
end
