﻿inherited FormDispatchInfo: TFormDispatchInfo
  Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103
  ClientHeight = 232
  ClientWidth = 441
  ExplicitWidth = 447
  ExplicitHeight = 260
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 191
    Width = 441
    inherited btnCancel: TButton
      Left = 325
      Visible = False
    end
    inherited btnOk: TButton
      Left = 206
      Caption = #1047#1072#1082#1088#1099#1090#1100
    end
  end
  object edAmount: TDBEditEh [1]
    Left = 212
    Top = 24
    Width = 169
    Height = 31
    ControlLabel.Width = 171
    ControlLabel.Height = 18
    ControlLabel.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1074#1072#1075#1086#1085#1086#1074
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -16
    ControlLabel.Font.Name = 'Verdana'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    ControlLabelLocation.Position = lpLeftCenterEh
    DataField = 'cnt'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    Visible = True
  end
  object edLen1: TDBEditEh [2]
    Left = 212
    Top = 80
    Width = 169
    Height = 31
    ControlLabel.Width = 136
    ControlLabel.Height = 18
    ControlLabel.Caption = #1059#1089#1083#1086#1074#1085#1072#1103' '#1076#1083#1080#1085#1072
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -16
    ControlLabel.Font.Name = 'Verdana'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    ControlLabelLocation.Position = lpLeftCenterEh
    DataField = 'norm_length'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
    Visible = True
  end
  object edLen: TDBEditEh [3]
    Left = 212
    Top = 136
    Width = 169
    Height = 31
    ControlLabel.Width = 113
    ControlLabel.Height = 18
    ControlLabel.Caption = #1054#1073#1097#1072#1103' '#1076#1083#1080#1085#1072
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -16
    ControlLabel.Font.Name = 'Verdana'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    ControlLabelLocation.Position = lpLeftCenterEh
    DataField = 'full_length'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 3
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 136
    Top = 208
  end
  inherited qrAux: TADOQuery
    Left = 72
    Top = 216
  end
end
