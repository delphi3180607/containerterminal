﻿inherited FormClassifications: TFormClassifications
  Caption = #1050#1083#1072#1089#1089#1080#1092#1080#1082#1072#1090#1086#1088#1099
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    object Splitter1: TSplitter [0]
      Left = 393
      Top = 29
      Height = 441
      ExplicitLeft = 421
      ExplicitTop = 35
    end
    inherited dgData: TDBGridEh
      Width = 393
      Align = alLeft
      AutoFitColWidths = True
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'table_name'
          Footers = <>
          Title.Caption = #1058#1072#1073#1083#1080#1094#1072
          Width = 109
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'classification_name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 147
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'ismulty'
          Footers = <>
          Title.Caption = #1052#1091#1083#1100#1090#1080#1074#1099#1073#1086#1088
          Width = 98
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        ExplicitLeft = -18
        ExplicitTop = -36
        ExplicitWidth = 185
        ExplicitHeight = 49
      end
    end
    object dgSpec: TDBGridEh
      Left = 396
      Top = 29
      Width = 424
      Height = 441
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Align = alClient
      Border.Color = 12500154
      Border.ExtendedDraw = True
      ColumnDefValues.Title.TitleButton = True
      ColumnDefValues.ToolTips = True
      Ctl3D = False
      DataSource = dsSpec
      DynProps = <>
      FixedColor = clCream
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      GridLineParams.BrightColor = clSilver
      GridLineParams.ColorScheme = glcsClassicEh
      GridLineParams.DarkColor = clSilver
      GridLineParams.DataBoundaryColor = clSilver
      GridLineParams.DataHorzColor = 14540253
      GridLineParams.DataVertColor = 14540253
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
      IndicatorParams.Color = clBtnFace
      IndicatorParams.FillStyle = cfstThemedEh
      IndicatorParams.HorzLineColor = 14540253
      IndicatorParams.VertLineColor = 14540253
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghHighlightFocus, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      PopupMenu = pmSpec
      ReadOnly = True
      RowDetailPanel.Color = clBtnFace
      SearchPanel.Enabled = True
      SearchPanel.PersistentShowing = False
      SelectionDrawParams.SelectionStyle = gsdsClassicEh
      SelectionDrawParams.DrawFocusFrame = False
      SelectionDrawParams.DrawFocusFrameStored = True
      ShowHint = True
      SortLocal = True
      STFilter.Color = clWhite
      STFilter.HorzLineColor = 14540253
      STFilter.InstantApply = True
      STFilter.Local = True
      STFilter.VertLineColor = 14540253
      STFilter.Visible = True
      TabOrder = 3
      TitleParams.Color = clBtnFace
      TitleParams.FillStyle = cfstThemedEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -11
      TitleParams.Font.Name = 'Verdana'
      TitleParams.Font.Style = [fsBold]
      TitleParams.HorzLineColor = 13421772
      TitleParams.ParentFont = False
      TitleParams.SecondColor = clCream
      TitleParams.VertLineColor = 14540253
      OnActiveGroupingStructChanged = dgDataActiveGroupingStructChanged
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'string_value'
          Footers = <>
          Title.Caption = #1047#1085#1072#1095#1077#1085#1080#1077
          Width = 226
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'exclude_values_ids'
          Footers = <>
          Title.Caption = #1048#1089#1082#1083#1102#1095#1072#1077#1084#1099#1077' ID'
          Width = 161
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'select * from classifications order by table_name,  classificati' +
        'on_name')
    UpdateCommand.CommandText.Strings = (
      'update classifications'
      'set'
      '  table_name = :table_name,'
      '  classification_name = :classification_name,'
      '  ismulty = :ismulty'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'table_name'
        Size = -1
        Value = Null
      end
      item
        Name = 'classification_name'
        Size = -1
        Value = Null
      end
      item
        Name = 'ismulty'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into classifications'
      '  (table_name, classification_name, ismulty)'
      'values'
      '  (:table_name, :classification_name, :ismulty)')
    InsertCommand.Parameters = <
      item
        Name = 'table_name'
        Size = -1
        Value = Null
      end
      item
        Name = 'classification_name'
        Size = -1
        Value = Null
      end
      item
        Name = 'ismulty'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from classifications'
      'where'
      '  id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from classifications where id  = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select * from classificationvalues '
      'where classification_id = :classification_id'
      'order by string_value')
    SelectCommand.Parameters = <
      item
        Name = 'classification_id'
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update classificationvalues'
      'set'
      '  classification_id = :classification_id,'
      '  string_value = :string_value,'
      '  exclude_values_ids = :exclude_values_ids'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'classification_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'string_value'
        Size = -1
        Value = Null
      end
      item
        Name = 'exclude_values_ids'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into classificationvalues'
      '  (classification_id, string_value, exclude_values_ids)'
      'values'
      '  (:classification_id, :string_value, :exclude_values_ids)')
    InsertCommand.Parameters = <
      item
        Name = 'classification_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'string_value'
        Size = -1
        Value = Null
      end
      item
        Name = 'exclude_values_ids'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from classificationvalues where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from classificationvalues where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 352
    Top = 296
  end
  object meSpec: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvSpec
    BeforeOpen = meSpecBeforeOpen
    BeforePost = meSpecBeforePost
    Left = 400
    Top = 296
  end
  object dsSpec: TDataSource
    DataSet = meSpec
    Left = 456
    Top = 296
  end
  object pmSpec: TPopupMenu
    Left = 464
    Top = 96
    object N8: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnClick = N8Click
    end
    object N9: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnClick = N9Click
    end
    object N10: TMenuItem
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
      OnClick = N10Click
    end
  end
end
