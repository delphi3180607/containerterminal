﻿inherited FormBilled: TFormBilled
  Caption = #1042#1099#1089#1090#1072#1074#1083#1077#1085#1085#1099#1077' '#1089#1095#1077#1090#1072
  ClientWidth = 860
  ExplicitWidth = 876
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Width = 860
    ExplicitWidth = 860
    inherited btnOk: TButton
      Left = 625
      ExplicitLeft = 625
    end
    inherited btnCancel: TButton
      Left = 744
      ExplicitLeft = 744
    end
  end
  inherited plAll: TPanel
    Width = 860
    ExplicitWidth = 860
    inherited plTop: TPanel
      Width = 860
      ExplicitWidth = 860
      inherited btFilter: TPngSpeedButton
        Width = 41
        OnClick = btFilterClick
        ExplicitLeft = 150
        ExplicitWidth = 41
      end
      inherited sbAdd: TPngSpeedButton
        Visible = False
      end
      inherited sbEdit: TPngSpeedButton
        Visible = False
      end
      inherited btTool: TPngSpeedButton
        Left = 822
        ExplicitLeft = 808
      end
      object sbClearFilter: TPngSpeedButton [7]
        AlignWithMargins = True
        Left = 184
        Top = 1
        Width = 34
        Height = 26
        Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1080#1083#1100#1090#1088
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbClearFilterClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000002334944415478DA63FCFFFF3F032580
          91A1FD0A7E13FEA3A8C66A8005903E7E26499541969785285B1F7FFEC36032EF
          3688690931136AC8C9445506691EA021606F01A518FF33349F7C0F56526326C8
          C008147AFAE50F83F9BC3B20694B864A9D130847810CF9FBF7F8E1247506492E
          16B873BBCF400C2833166478F6ED0F83EDBC9B0C0CCCCC60CD98BE0219F2F3C7
          F13D49DA0CE2DC2C60C9C6A3AFC02E4A3714667059709D8181950DAE197BB080
          0CF9F2F9F896140386EBA7CF322C58B78FE1C6DD67C0B08484A68682C4774B5D
          A56573EBE253188CD3B0852BC4109B67878F5FBD729321CEDB92212DD096414A
          949FE1DAFDE70CF3361E6558B8E5388393A9C69D5D27AEA9623520A676EEFA95
          BBCE041C9C55C2A0F6EF13039B800003AF820258EED981030CA7BFB13244B7AF
          640872345C8FD50059AFF2DFD9618E2CE5F1EE0C6F2F5C60D81910C0E0BE6103
          C3CD050B187E7DF8C0E000A40BFB5631ACDF7FE10F5603184DD2FFBFDFDFCFC0
          CFC309E6830CD9ECE0C02005C46E408340E0FCCDC70CA6B16D0C380D78BABD93
          4152841FCC3F909000B6F90DD020904B840D0C188E5DBACB6097D2C380D30BA1
          2EC62CBD85A160CD20007236B2774A365E64D877FAC61F9C81B8E9E0C580355D
          190CAEE69A28727FFFFD6398B1E61043C594750C810E38021114BF36663AFFCF
          5EBCC9E065ADCB90E46FCDA02E2FCE70F9CE538659EB0E3300A38FC1D90C1A8D
          D8B233300CC074B2BFF5ECE397EF45DF78F0821326A72E2FF1DD4A4F69293021
          A5822C02009244E8A4AC1C3E1E0000000049454E44AE426082}
        ExplicitLeft = 192
        ExplicitTop = 3
      end
      inherited plCount: TPanel
        Left = 625
        TabOrder = 1
        ExplicitLeft = 625
      end
      object Panel6: TPanel
        AlignWithMargins = True
        Left = 219
        Top = 0
        Width = 217
        Height = 29
        Margins.Left = 0
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object dtStart: TDBDateTimeEditEh
          AlignWithMargins = True
          Left = 125
          Top = 3
          Width = 89
          Height = 21
          Margins.Left = 125
          Margins.Top = 0
          Margins.Bottom = 5
          ControlLabel.Width = 113
          ControlLabel.Height = 13
          ControlLabel.Caption = #1053#1072#1095#1080#1085#1072#1103' '#1089' '#1076#1072#1090#1099' '#1089#1095#1077#1090#1072
          ControlLabel.Visible = True
          ControlLabelLocation.Position = lpLeftCenterEh
          Align = alBottom
          DynProps = <>
          EditButtons = <>
          Kind = dtkDateEh
          TabOrder = 0
          Visible = True
        end
      end
      object btRequery: TButton
        AlignWithMargins = True
        Left = 442
        Top = 1
        Width = 119
        Height = 25
        Margins.Top = 1
        Align = alLeft
        Caption = #1055#1077#1088#1077#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100
        TabOrder = 2
        OnClick = sbRequeryClick
      end
    end
    inherited dgData: TDBGridEh
      Top = 51
      Width = 856
      Height = 417
      AllowedOperations = [alopUpdateEh]
      ReadOnly = False
      TitleParams.MultiTitle = True
      OnDblClick = nil
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'docorder_data'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1044#1072#1090#1072', '#1085#1086#1084#1077#1088' '#1079#1072#1103#1074#1082#1080
          Width = 146
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082' ('#1082#1083#1080#1077#1085#1090')'
          Width = 122
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Footers = <>
          ReadOnly = True
          Title.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
          Width = 102
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1057#1090#1072#1090#1091#1089' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
          Width = 108
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'containerkind_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1058#1080#1087
          Width = 68
        end
        item
          CellButtons = <>
          DisplayFormat = '### ###  '
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_fact'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1042#1077#1089
          Width = 70
        end
        item
          CellButtons = <>
          Color = 13816530
          DynProps = <>
          EditButtons = <>
          FieldName = 'cargotype_code'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Footers = <>
          ReadOnly = True
          Title.Caption = #1058#1080#1087' '#1075#1088#1091#1079#1072
          Width = 156
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'invoice_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1089#1095#1077#1090#1072
          Width = 85
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'invoice_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1089#1095#1077#1090#1072
          Width = 83
        end
        item
          CellButtons = <>
          DisplayFormat = '### ### ##0.00  '
          DynProps = <>
          EditButtons = <>
          FieldName = 'invoice_amount'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          Footers = <>
          Title.Caption = #1057#1091#1084#1084#1072
          Width = 91
        end
        item
          Alignment = taRightJustify
          CellButtons = <>
          DisplayFormat = 'dd.mm.yyyy hh:nn:ss  '
          DynProps = <>
          EditButtons = <>
          FieldName = 'create_datetime'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1044#1072#1090#1072' '#1087#1088#1080#1074#1103#1079#1082#1080
          Width = 156
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'user_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
          Width = 112
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'bill_id'
          Footers = <>
          Visible = False
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object plStatus: TPanel
      Left = 0
      Top = 29
      Width = 860
      Height = 20
      Align = alTop
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 16318464
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      StyleElements = [seClient, seBorder]
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select dos.id, b.id as bill_id, '
      
        'convert(varchar(10),d.doc_date, 104)+'#39' / '#39'+d.doc_number as docor' +
        'der_data, '
      
        'c.cnum, (select code from objectstatekinds osk where osk.id = lo' +
        's.object_state_id) as state_code,'
      
        '(select code from deliverytypes dlv where dlv.id = t.dlv_type_id' +
        ') as deliverytype_code, '
      
        '(select code from containerkinds ck where ck.id = c.kind_id) as ' +
        'containerkind_code, '
      
        '(select code+'#39', '#39'+name from cargotypes crt where crt.id = cr.typ' +
        'e_id) as cargotype_code, '
      'cr.weight_fact, '
      'b.invoice_amount,'
      'b.invoice_date,'
      'b.invoice_number, '
      'b.user_id,'
      
        '(select code from counteragents ct1 where ct1.id = t.payer_id) a' +
        's payer_code,'
      
        'b.create_datetime, (select user_name from users u where u.id = b' +
        '.user_id) as user_code, '
      'c.id as container_id, t.id as task_id'
      'from docorder od'
      'inner join docorderspec dos on (dos.doc_id = od.id)'
      'inner join documents d on (d.id = od.id)'
      'inner join containers c on (dos.container_id = c.id)'
      'inner join v_lastobjectstates los on (los.object_id = c.id)'
      'inner join tasks t on (los.task_id = t.id)'
      'inner join cargos cr on (t.object_id = cr.id)'
      
        'inner join objectstatehistory osh on (osh.task_id = t.id and osh' +
        '.object_id = c.id and osh.doc_id = od.id)'
      
        'left outer join billed b on (b.task_id = t.id and b.container_id' +
        ' = c.id)'
      
        'where t.dlv_type_id in (select id from deliverytypes d where d.g' +
        'lobal_section = '#39'output'#39')'
      'and d.doc_date >= :datestart'
      'and dbo.filter_objects(:guid, c.cnum) = 1'
      'order by d.doc_date desc')
    SelectCommand.Parameters = <
      item
        Name = 'datestart'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'guid'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      
        'declare @container_id int, @task_id int, @invoice_amount int, @i' +
        'nvoice_date datetime, @invoice_number varchar(80), @user_id int;'
      ''
      
        'select @container_id = :container_id, @task_id = :task_id, @invo' +
        'ice_amount = :invoice_amount, @invoice_date = :invoice_date, @in' +
        'voice_number = :invoice_number, @user_id = :user_id;'
      ''
      'insert into billed'
      
        '  (container_id, task_id, invoice_amount, invoice_date, invoice_' +
        'number, user_id)'
      'select '
      
        '  @container_id, @task_id, @invoice_amount, @invoice_date, @invo' +
        'ice_number, @user_id'
      'where '
      
        '   not exists (select 1 from billed b where b.container_id = @co' +
        'ntainer_id and b.task_id = @task_id);'
      ''
      'update billed'
      'set'
      '  invoice_amount = @invoice_amount,'
      '  invoice_date = @invoice_date,'
      '  invoice_number = @invoice_number'
      'where'
      '  id = :bill_id')
    UpdateCommand.Parameters = <
      item
        Name = 'container_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'task_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'invoice_amount'
        Size = -1
        Value = Null
      end
      item
        Name = 'invoice_date'
        Size = -1
        Value = Null
      end
      item
        Name = 'invoice_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'user_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'bill_id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into billed'
      
        '  (container_id, task_id, invoice_amount, invoice_date, invoice_' +
        'number, user_id)'
      'values'
      
        '  (:container_id, :task_id, :invoice_amount, :invoice_date, :inv' +
        'oice_number, :user_id)')
    InsertCommand.Parameters = <
      item
        Name = 'container_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'task_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'invoice_amount'
        Size = -1
        Value = Null
      end
      item
        Name = 'invoice_date'
        Size = -1
        Value = Null
      end
      item
        Name = 'invoice_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'user_id'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      '')
    GetrecCommand.CommandText.Strings = (
      'select dos.id, b.id as bill_id, '
      
        'convert(varchar(10),d.doc_date, 104)+'#39' / '#39'+d.doc_number as docor' +
        'der_data, '
      
        'c.cnum, (select code from objectstatekinds osk where osk.id = lo' +
        's.object_state_id) as state_code,'
      
        '(select code from deliverytypes dlv where dlv.id = t.dlv_type_id' +
        ') as deliverytype_code, '
      
        '(select code from containerkinds ck where ck.id = c.kind_id) as ' +
        'containerkind_code, '
      
        '(select code+'#39', '#39'+name from cargotypes crt where crt.id = cr.typ' +
        'e_id) as cargotype_code, '
      'cr.weight_fact, '
      'b.invoice_amount,'
      'b.invoice_date,'
      'b.invoice_number, '
      'b.user_id,'
      
        '(select code from counteragents ct1 where ct1.id = t.payer_id) a' +
        's payer_code,'
      
        'b.create_datetime, (select user_name from users u where u.id = b' +
        '.user_id) as user_code, '
      'c.id as container_id, t.id as task_id'
      'from docorder od'
      'inner join docorderspec dos on (dos.doc_id = od.id)'
      'inner join documents d on (d.id = od.id)'
      'inner join containers c on (dos.container_id = c.id)'
      'inner join v_lastobjectstates los on (los.object_id = c.id)'
      'inner join tasks t on (los.task_id = t.id)'
      'inner join cargos cr on (t.object_id = cr.id)'
      
        'inner join objectstatehistory osh on (osh.task_id = t.id and osh' +
        '.object_id = c.id and osh.doc_id = od.id)'
      
        'left outer join billed b on (b.task_id = t.id and b.container_id' +
        ' = c.id)'
      'where dos.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited qrAux: TADOQuery
    Parameters = <
      item
        Name = 'bill_id'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'delete from billed where id = :bill_id')
  end
end
