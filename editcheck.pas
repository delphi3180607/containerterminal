﻿unit EditCheck;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Mask, DBCtrlsEh;

type
  TFormEditCheck = class(TFormEdit)
    cbUnloadResult: TDBComboBoxEh;
    meNote: TDBMemo;
    Label1: TLabel;
    dtCheckDateTime: TDBDateTimeEditEh;
    Button1: TButton;
    cbShift: TDBCheckBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditCheck: TFormEditCheck;

implementation

{$R *.dfm}

end.
