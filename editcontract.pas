﻿unit editcontract;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  DBGridEh, Vcl.Buttons, Vcl.Mask, DBCtrlsEh, DBLookupEh, functions, Data.DB,
  Data.Win.ADODB;

type
  TFormEditContract = class(TFormEdit)
    luCustomer: TDBLookupComboboxEh;
    Label3: TLabel;
    sbOwner: TSpeedButton;
    dtBegin: TDBDateTimeEditEh;
    Label1: TLabel;
    dtEnd: TDBDateTimeEditEh;
    Label2: TLabel;
    Label4: TLabel;
    edNumber: TDBEditEh;
    procedure sbOwnerClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditContract: TFormEditContract;

implementation

{$R *.dfm}

uses counteragents;

procedure TFormEditContract.sbOwnerClick(Sender: TObject);
begin
  SFD(FormCounteragents, self.luCustomer);
end;

end.
