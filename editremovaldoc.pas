﻿unit editremovaldoc;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBCtrlsEh, Vcl.StdCtrls, Vcl.Mask, functions,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls, DBGridEh, Vcl.Buttons, DBLookupEh, counteragents,
  DBSQLLookUp, Vcl.ComCtrls;

type
  TFormEditRemovalDoc = class(TFormEdit)
    Label1: TLabel;
    Label3: TLabel;
    edDocNumber: TDBEditEh;
    dtDocDate: TDBDateTimeEditEh;
    Label8: TLabel;
    sbForwarder: TSpeedButton;
    edReturnAddress: TDBEditEh;
    Label15: TLabel;
    dtReturnDateTime: TDBDateTimeEditEh;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    edCarData: TDBEditEh;
    nuMovers: TDBNumberEditEh;
    ssDlvTypes: TADOLookUpSqlSet;
    laDlvTypes: TDBSQLLookUp;
    Label2: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    edAttorney: TDBEditEh;
    cbExtraPlace: TDBCheckBoxEh;
    luDealer: TDBSQLLookUp;
    ssPersons: TADOLookUpSqlSet;
    edNote: TDBEditEh;
    edCarNumber: TDBEditEh;
    Label27: TLabel;
    laDriver: TDBSQLLookUp;
    pcAddresses: TPageControl;
    TabSheet1: TTabSheet;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    dtReceptDateTime: TDBDateTimeEditEh;
    edReceptAdress: TDBEditEh;
    edReceptContact: TDBEditEh;
    dtDlvDateTime: TDBDateTimeEditEh;
    edDlvAddress: TDBEditEh;
    edDlvContact: TDBEditEh;
    edReceptOrgName: TDBEditEh;
    edDeliveryOrgName: TDBEditEh;
    Bevel3: TBevel;
    Label23: TLabel;
    Label22: TLabel;
    dtArrivalFact: TDBDateTimeEditEh;
    dtGoFact: TDBDateTimeEditEh;
    TabSheet2: TTabSheet;
    Label4: TLabel;
    dtExtraDateTime: TDBDateTimeEditEh;
    Label5: TLabel;
    edExtraAddress: TDBEditEh;
    Label19: TLabel;
    edExtraContact: TDBEditEh;
    Label26: TLabel;
    dtExtraArrivalFact: TDBDateTimeEditEh;
    dtExtraGoFact: TDBDateTimeEditEh;
    Label28: TLabel;
    procedure sbForwarderClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure laDriverEditButtons1Click(Sender: TObject; var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditRemovalDoc: TFormEditRemovalDoc;

implementation

{$R *.dfm}

uses persons, adddriver;

procedure TFormEditRemovalDoc.btnOkClick(Sender: TObject);
begin

  if dsLocal.DataSet.FieldByName('dealer_id').Value = null then
  begin
    ShowMessage('Нужно заполнить перевозчика.');
    ModalResult := mrNone;
    exit;
  end;

  if dsLocal.DataSet.FieldByName('driver_id').Value = null then
  begin
    ShowMessage('Нужно заполнить водителя.');
    ModalResult := mrNone;
    exit;
  end;

  if trim(dsLocal.DataSet.FieldByName('car_number').AsString) = '' then
  begin
    ShowMessage('Номер автомобиля - обязательное поле.');
    ModalResult := mrNone;
    exit;
  end;

  inherited;
end;

procedure TFormEditRemovalDoc.laDriverEditButtons1Click(Sender: TObject;
  var Handled: Boolean);
begin
  EA(nil, FormAddDriver, FormPersons.meData, 'persons');
  //laDriver.KeyValue := FormPersons.meData.FieldByName('id').AsInteger;
  Handled := true;
end;

procedure TFormEditRemovalDoc.sbForwarderClick(Sender: TObject);
begin
  SFDE(TFormCounteragents,self.luDealer);
end;

end.
