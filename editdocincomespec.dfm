﻿inherited FormEditInComeSpec: TFormEditInComeSpec
  ClientHeight = 504
  ExplicitHeight = 532
  PixelsPerInch = 96
  TextHeight = 14
  inherited sbCargoType: TSpeedButton
    Left = 529
    Top = 344
    Enabled = False
    ExplicitLeft = 529
    ExplicitTop = 344
  end
  inherited nuWeightCargo: TDBNumberEditEh
    Left = 8
    Top = 395
    ControlLabel.ExplicitLeft = 8
    ControlLabel.ExplicitTop = 378
    ParentFont = False
    ExplicitLeft = 8
    ExplicitTop = 395
  end
  inherited neAmount: TDBNumberEditEh
    DataField = ''
    Visible = False
  end
  object laAddress: TDBSQLLookUp [4]
    Left = 197
    Top = 395
    Width = 374
    Height = 22
    ControlLabel.Width = 120
    ControlLabel.Height = 14
    ControlLabel.Caption = #1040#1076#1088#1077#1089' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
    ControlLabel.Visible = True
    DataField = 'address_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 13
    Visible = True
    SqlSet = ssAddress
    RowCount = 0
  end
  inherited laCargoType: TDBSQLLookUp
    Left = 197
    Top = 345
    ControlLabel.ExplicitLeft = 197
    ControlLabel.ExplicitTop = 328
    Enabled = False
    ExplicitLeft = 197
    ExplicitTop = 345
  end
  inherited cbIsEmpty: TDBCheckBoxEh
    Left = 571
    Top = 346
    ExplicitLeft = 571
    ExplicitTop = 346
  end
  inherited edContainerNum: TDBEditEh
    EditMask = 'cccc9999999;0;?'
  end
  inherited edSealNumber: TDBEditEh
    Top = 345
    ControlLabel.ExplicitTop = 328
    ExplicitTop = 345
  end
  inherited Panel1: TPanel
    Top = 137
    Height = 180
    Visible = True
    ExplicitTop = 137
    ExplicitHeight = 180
    inherited nuWeight: TDBNumberEditEh
      Top = 20
      ControlLabel.ExplicitTop = 3
      DataField = 'container_weight'
      DataSource = dsLocal
      ExplicitTop = 20
    end
    inherited dtInspect: TDBDateTimeEditEh
      Top = 73
      ControlLabel.ExplicitTop = 56
      DataField = 'date_inspection'
      DataSource = dsLocal
      ExplicitTop = 73
    end
    inherited cbPicture: TDBCheckBoxEh
      Left = 159
      Top = 75
      DataField = 'picture'
      DataSource = dsLocal
      ExplicitLeft = 159
      ExplicitTop = 75
    end
    inherited dtDateStiker: TDBDateTimeEditEh
      Top = 73
      ControlLabel.ExplicitTop = 56
      DataField = 'date_stiker'
      DataSource = dsLocal
      ExplicitTop = 73
    end
    inherited dtDateMade: TDBDateTimeEditEh
      Top = 73
      ControlLabel.ExplicitTop = 56
      DataField = 'date_made'
      DataSource = dsLocal
      ExplicitTop = 73
    end
    inherited nuCarrying: TDBNumberEditEh
      Top = 20
      ControlLabel.ExplicitTop = 3
      DataField = 'carrying'
      DataSource = dsLocal
      ExplicitTop = 20
    end
    inherited cbIsDefective: TDBCheckBoxEh
      Left = 8
      Top = 114
      DataField = 'is_defective'
      DataSource = dsLocal
      ExplicitLeft = 8
      ExplicitTop = 114
    end
    inherited edNote: TDBEditEh
      Left = 8
      Top = 157
      ControlLabel.ExplicitLeft = 8
      ControlLabel.ExplicitTop = 140
      DataField = 'defect_note'
      DataSource = dsLocal
      ExplicitLeft = 8
      ExplicitTop = 157
    end
    inherited cbNotReady: TDBCheckBoxEh
      Top = 114
      DataField = 'isnotready'
      DataSource = dsLocal
      ExplicitTop = 114
    end
    inherited cbACEP: TDBCheckBoxEh
      Left = 550
      Top = 75
      DataField = 'acep'
      DataSource = dsLocal
      ExplicitLeft = 550
      ExplicitTop = 75
    end
    object cbShift: TDBCheckBoxEh
      Left = 436
      Top = 114
      Width = 201
      Height = 17
      Caption = #1057#1084#1077#1097#1077#1085#1080#1077' '#1094#1077#1085#1090#1088#1072' '#1090#1103#1078#1077#1089#1090#1080
      DataField = 'weight_shift'
      DataSource = dsLocal
      DynProps = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 10
    end
  end
  inherited edExternalOrderNum: TDBEditEh
    Top = 441
    ControlLabel.ExplicitTop = 424
    Visible = False
    ExplicitTop = 441
  end
  inherited leContainerKind: TDBSQLLookUp
    Left = 180
    ControlLabel.ExplicitLeft = 180
    ExplicitLeft = 180
  end
  inherited plBottom: TPanel
    Top = 463
    ExplicitTop = 463
  end
  inherited cbIsSigned: TDBCheckBoxEh
    Top = 441
    Visible = False
    ExplicitTop = 441
  end
  inherited plState: TPanel
    Left = 8
    Height = 79
    TabOrder = 14
    ExplicitLeft = 8
    ExplicitHeight = 79
    inherited lbWarning: TLabel
      Left = 0
      Top = 49
      Width = 729
      Height = 23
      ExplicitLeft = 0
      ExplicitTop = 49
      ExplicitWidth = 729
      ExplicitHeight = 23
    end
    inherited txRealState: TDBEditEh
      Left = 0
      Top = 18
      ControlLabel.Font.Color = clGray
      ControlLabel.ExplicitTop = 1
      ControlLabel.ExplicitWidth = 134
      ExplicitLeft = 0
      ExplicitTop = 18
    end
    inherited txFormalState: TDBEditEh
      Left = 246
      Top = 18
      ControlLabel.Font.Color = clGray
      ControlLabel.StyleElements = [seClient, seBorder]
      ControlLabel.ExplicitLeft = 246
      ControlLabel.ExplicitTop = 1
      ControlLabel.ExplicitWidth = 139
      ExplicitLeft = 246
      ExplicitTop = 18
    end
  end
  inherited cbSealWaste: TDBComboBoxEh
    Left = 594
    Top = 395
    ControlLabel.ExplicitLeft = 594
    ControlLabel.ExplicitTop = 378
    Visible = False
    ExplicitLeft = 594
    ExplicitTop = 395
  end
  inherited dsLocal: TDataSource
    Left = 47
    Top = 439
  end
  inherited qrAux: TADOQuery
    Left = 92
    Top = 439
  end
  inherited drvContainers: TADODataDriverEh
    Left = 184
    Top = 435
  end
  inherited meContainers: TMemTableEh
    Left = 256
    Top = 435
  end
  inherited drvCargo: TADODataDriverEh
    Left = 352
    Top = 436
  end
  inherited meCargo: TMemTableEh
    Left = 416
    Top = 436
  end
  object ssAddress: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from addresscontainers order by code')
    DownSql.Strings = (
      'select * from addresscontainers order by code')
    InitSql.Strings = (
      'select * from addresscontainers where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 406
    Top = 358
  end
end
