﻿inherited FormDocFlow: TFormDocFlow
  ActiveControl = dgLinkedObjects
  Caption = 'FormDocFlow'
  ClientHeight = 660
  ClientWidth = 933
  Color = 15658734
  ParentFont = True
  StyleElements = [seFont, seBorder]
  ExplicitWidth = 949
  ExplicitHeight = 698
  PixelsPerInch = 96
  TextHeight = 13
  object SplitterVert: TSplitter [0]
    Left = 193
    Top = 0
    Width = 7
    Height = 627
    StyleElements = [seFont, seBorder]
  end
  inherited plBottom: TPanel
    Top = 627
    Width = 933
    Height = 33
    ExplicitTop = 627
    ExplicitWidth = 933
    ExplicitHeight = 33
    inherited btnOk: TButton
      Left = 698
      Height = 27
      ExplicitLeft = 698
      ExplicitHeight = 27
    end
    inherited btnCancel: TButton
      Left = 817
      Height = 27
      ExplicitLeft = 817
      ExplicitHeight = 27
    end
  end
  inherited plAll: TPanel
    Left = 200
    Width = 733
    Height = 627
    Margins.Left = 0
    Color = 15658734
    StyleElements = [seFont, seBorder]
    ExplicitLeft = 200
    ExplicitWidth = 733
    ExplicitHeight = 627
    object SplitterHor: TSplitter [0]
      Left = 0
      Top = 435
      Width = 733
      Height = 7
      Cursor = crVSplit
      Align = alBottom
      StyleElements = [seFont, seBorder]
      ExplicitTop = 434
    end
    inherited plTop: TPanel
      Width = 733
      Height = 26
      ExplicitWidth = 733
      ExplicitHeight = 26
      inherited sbDelete: TPngSpeedButton
        Left = 64
        Width = 32
        Height = 26
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 64
        ExplicitWidth = 32
        ExplicitHeight = 26
      end
      inherited btFilter: TPngSpeedButton
        Left = 165
        Width = 32
        Height = 26
        ParentShowHint = False
        ShowHint = True
        OnClick = btFilterClick
        ExplicitLeft = 165
        ExplicitWidth = 32
        ExplicitHeight = 26
      end
      inherited btExcel: TPngSpeedButton
        Left = 505
        Width = 32
        Height = 26
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 505
        ExplicitWidth = 32
        ExplicitHeight = 26
      end
      inherited sbAdd: TPngSpeedButton
        Left = 32
        Width = 32
        Height = 26
        ExplicitLeft = 32
        ExplicitWidth = 32
        ExplicitHeight = 26
      end
      inherited sbEdit: TPngSpeedButton
        Left = 96
        Width = 32
        Height = 26
        ExplicitLeft = 96
        ExplicitWidth = 32
        ExplicitHeight = 26
      end
      inherited Bevel2: TBevel
        Left = 162
        Top = 2
        Height = 22
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        ExplicitLeft = 152
        ExplicitTop = 2
        ExplicitHeight = 26
      end
      inherited btTool: TPngSpeedButton
        Left = 535
        Width = 34
        Height = 20
        ExplicitLeft = 699
        ExplicitTop = 4
        ExplicitWidth = 34
        ExplicitHeight = 20
      end
      object Bevel1: TBevel [7]
        AlignWithMargins = True
        Left = 263
        Top = 2
        Width = 1
        Height = 22
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        ExplicitLeft = 251
        ExplicitTop = 0
        ExplicitHeight = 38
      end
      object btFlow: TPngSpeedButton [8]
        AlignWithMargins = True
        Left = 367
        Top = 0
        Width = 32
        Height = 26
        Hint = #1057#1086#1079#1076#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = btFlowClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
          6FA8640000013C4944415478DA6364A0103042484606759B585F591D9B6610F7
          F19523B5378F2CDECCF0FF3FBA62462B111193A36FDE9C463140DD36CED72638
          6FA382821C58E0FEC3C70CC7D64E0ABA7E68E10646A81AA051FF991819994FBA
          B8FC6EBC7AD563CBB367BBE006B864CEBA60EF1EA0C7CEC2F483899989E5D3B7
          1FFFEE9E38C4167E6AD61BD6BFBFFEFF6764648498C1C0C0C1CCCC2BCACECE51
          7BE58AF786A74FB7830D70CE9875DED123409F0D6AC067A001778E1F660B3F3D
          136E00D484FF9CCCCCFC22ECEC6CD5972F7B6C02BA02C90BF9402FC8801DF5E0
          C11386E31B26045D3BB008D30BCECEBFEBAF5C71D9F6E2C53E9440D4B08BF696
          D1B26F05719F5C3B587DE3D0D2ADD802D14258D8F0F8DBB7E7506381E268A44A
          3AB005A5033BA017FEFF7B7CF9F0683A20371D4869DAB53002859E5E3F5843B7
          74000005ABF01126EC5CA40000000049454E44AE426082}
        ExplicitTop = -3
      end
      object sbHistory: TPngSpeedButton [9]
        AlignWithMargins = True
        Left = 399
        Top = 0
        Width = 32
        Height = 26
        Hint = #1044#1086#1082#1091#1084#1077#1085#1090#1099', '#1089#1086#1079#1076#1072#1085#1085#1099#1077' '#1085#1072' '#1086#1089#1085#1086#1074#1072#1085#1080#1080
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbHistoryClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
          6FA864000002034944415478DA9D93CF6A135114C6CFB9F70E51B08255A151D4
          2E0575A9B533D4208989BA3091EEC45AE24E28B5AD6B9FC045ABF80035082208
          12BB6A5DD411D24E1A48DF4008A545E8D2A84DE7FEF1DC742A934A0CF42CCFBD
          DF6FBEFB9D33B8924E6BC6390022D8D2540E636CABD9DC2C542AE70D8081FF14
          06D9ACBE984A01771C305A0330A6514AF6657171F39EEFF706AC64B3F27226A3
          4522C12CC0206A2EA5F8BCB0B091F7FDC19E805A2EF7D781560AEC73C801F84B
          4BDB779697077A02CA9EB78142688E288E1D397AFAC7CEEF6D07D1A10C1A8F6B
          B56B1100BB02E804ED254680D18713AB1FDEBEBE6E8C51F14B23C5D96ADF89E4
          A052BB3B4821EDF715398E93315F9C093EBD99F3280B496804630019E377A7DF
          6DB9A95BA73849CD5E0F243DB35EAF77021E3C79FE2D0C5B3FC3DD56D30A09A4
          0C0A0E67462E0D0DBBC2E1DCC6CC1832080950ADAEC500F4C5FBE3536BE5D22B
          977472CF40E460E6FD773795EEE72CEAD19994EA8003EAE6C79EAE964B73C370
          20796FECC5D7BEFE810B4A862D9B995511C72E5D27A0F0682A20800DB10380C8
          78D729743A98AC944B2FBD433B182D3E5BFF383F7BF5701910A2303E1D70CE13
          5286BFDAB68D36760A78F6C69521D71382331A02B0F618C31082A0F6CF86E1FE
          5FD9AEC8C1EDC952C3BB994B72A4C11AEA308CF660BDFB8AC62B3331DF387E32
          794E938AE422BE897F0070670F2F29A2F0610000000049454E44AE426082}
      end
      object sbRestrictions: TPngSpeedButton [10]
        AlignWithMargins = True
        Left = 436
        Top = 0
        Width = 32
        Height = 26
        Hint = #1054#1075#1088#1072#1085#1080#1095#1077#1085#1080#1103' '#1080' '#1076#1086#1087'.'#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        Layout = blGlyphRight
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbRestrictionsClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
          6FA8640000023C4944415478DA63644003A2ECEC3C31B2B27E96A2A27192DCDC
          5620B1E75FBF1E3BFEF2E5A2254F9F6E7AFDF3E71764F58CC88C3C25A5884C5D
          DD65EC52520C3CA2A20C6C9C9C60B95FDFBF337C79F386E1E7D3A70C532E5F0E
          9D72EFDE5A140340C4447DFD764F7DFD72410D8D3F1CDCDCBF98FEFF67FBFFFF
          3F13589E91F1DF3F46C65F3FBF7DE37877FD3AD3960B179A0A2F5D6A801B900F
          B439C7C66699B09EDE270E66669EFF7FFF32FD076A42712AD0304666E67F3FFE
          FDFBF6F6C2059EBEA34783A7DFBBB79E11E4E7C31E1E9F44CCCDFF70717232FD
          FFF78F0968E53FA0C94CC806FC6760F8C700328489E9DFF71F3F18DE1C3FCE64
          B563070F63A18A4A549E9BDB125175F56F8CBF7F738114A26B46360424F79F95
          F5DBEB9B37B926ECDC19CDB8CAD27297A3ABAB0B97A0E01F90ED4C6C6C5835C3
          C0BF5FBFFE81BCF2EDED5B96FD7BF6EC623CECE2F249CFD99987858DED1F031B
          1B034742021303070750E53F549D4C4073814EFFB160C13F865FBF18FEFCFAC5
          7471EFDE4F941B00F4C20EA017DC48F102133020BFBE7FCFB26FF7EE1D8C854A
          4A51791E1EE407222C1A452D2CFE71029D4E74349E38C164B37D3B2F3821E528
          2905E7DBD8AC0626A42FC084C44554423A762C64FADDBBEBE079A15F57B7C1C7
          C0A04E484BEB1F3B17D70F9C49F9DA35A6CDC0A45C74F9322229C340A6925260
          818ECE5A361919061E11110636506CC032D3DBB70CBF809969E2952B21D38036
          63E44618106663E30266677F2B31B158E4EC7CECD5ABC54B9F3CD9F4E6E7CFAF
          C8EA011D6B38AA01C465630000000049454E44AE426082}
      end
      object sbConfirm: TPngSpeedButton [11]
        AlignWithMargins = True
        Left = 266
        Top = 0
        Width = 32
        Height = 26
        Hint = #1055#1088#1086#1074#1077#1089#1090#1080'/'#1087#1086#1076#1090#1074#1077#1088#1076#1080#1090#1100
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbConfirmClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000E9C00000E
          9C01079453DD0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000003124944415478DA75937B48D35114C7
          CFBDBFC71EB9B6D27C30D3D4C8B2122ACB586F7B4A0E4D4DA312B428C28CFA23
          7B605A1682690F281B56D4A42CA566456821058668F54751AC2890A8D65A6669
          399BDBBCFBEDFEFA6DA342C9FBDFE19CCFF7DE73CFF92218FDF0902C2B07C4C9
          E1A9E380140FFDAF088D883578DBB466FACD610FC9484C9EA79B3C5EC6C8E0C9
          F337035FEB5F7672D1E3559E1A739A5437F05F01B431D6945BB1317365C84C70
          096E98A40C231830585C5F7996E1E185FD3D18F2AA6BA1ED67E148010D14849E
          4F2858B5BE2C29CBA9C4321E10C61ED18B7D490E31D44B05CA612C5476DD9377
          54375E832BBD4552CAEE17E0F2133B324BF5BA7511B39D7246A914A4721F8910
          608418F078090D6215F0DED58345EA76BF1BB4CB0DA5573A88D1BC38F0827513
          5B2E5F3D911AC28E158628C18C0461CCF86F17A807545C10BC735869B9A901B2
          572FA259DA05ECF2ACBCFB70DBBAD627200B6F48EB3E9C9AA3D12A42FD377B44
          C10F7B451A8007AD507EF3062C5892400B63D680CDF51D97B436F6DBB2EF4420
          48565465D417EEDD1EB58238A987EF213F6192220C5C5E320C5EB4340176C5EA
          A177A81FE4982746EB23BE69F399533E81E31B1A7617E747A61087D7CDEF6BAB
          83F8B828289B920BCFED5D70D4F40FEE23BF4014BDA0C47272F1D303FEF6B60B
          B5FE16B4A68CEEC32B33351315E1F4ADC306D5CD261C33271A2CE62F3037290E
          F6C4A5FB61248A94C52C7C76F64045FB5D6C4B6F52073E51AF6DB974FD646A30
          A7166498C71F5D363872AB11EB164E87DD71FABFB07F3D25816ED28F77EEAFB0
          10C3AB98C0187726B6E714672CD487CE72CB302767104B9D5E178C6195D83705
          51043F8C30C24045F2B0CFCCD7195B6C83A59D517F16490DAB834FCFA85C9B7F
          283EDDADC40A56AAC56E69A4BE248B382AF54E05D1239CFED02A7F5C74BD06DA
          060E4AA9C1E15E5836CEB0C9B86B474A700210690AD18A090480815E62E7557C
          103CFC6E86735BABEAA0B56FCB68661ACB1525367B2C3F7E85E5CC4AD6CD9F2A
          998981CED75D0356E3B34E3E52AD22675FE97D2B3C9AC0483B1F0324293C7596
          8C66E7DFF8E545E8FF6F74AA0000000049454E44AE426082}
      end
      object sbCancelConfirm: TPngSpeedButton [12]
        AlignWithMargins = True
        Left = 298
        Top = 0
        Width = 32
        Height = 26
        Hint = #1057#1085#1103#1090#1100' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1077
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbCancelConfirmClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000002214944415478DAA593414854411880BF79BBAE91A5E11A
          49B16EA9A965872048EA50770FA61B5114614851D02524E8D221F350874ED2A5
          8317C17318044114041156161DC4758B2D4D8AA4D5B0A0F6BD7D3B7FF3467775
          334F0EBC37EFBD99EF9BF9E7FDBF1211D6D3D42A815219738F165E75E9E89C23
          52B3B640A9EFB436D570E42098EF764CEBE2A55FBC45521F7F85452A570B0AF0
          D136487F45E6162C24019CCF235515B06B1B7AF41DF90FD3990D225B97052BE1
          851C5CBD86F4F723E9F4221C8FE3F4F591BF711D2A147A6C1C3F3D93D9682405
          8170E90CF2E91BEAF61D686C04CF23DFDD6D77111A1A824804999CC4BF700EEA
          6B71EF3F6593882A0AE4E269184B21B118CEE0A00502896D65658879F6130974
          2A856A6B21FBE019950581369D3A7F123102BBE5BABAC5550D689B81BDAE2E73
          80297B2681C07DF8FC1F41CF09E4551231024CCCA1E1E1A220583DD7D1813621
          04E3CEA13DB88F464B057427909713CBF05208C13F524108AE8BDBDE8E4E2609
          1DDECB9FC7AFD95210F8A673CE1E43BF9F2174F71EAAB9D902B9CE4EBBE5C8C8
          08949723E3E3B8A78EA39A76907DF26659E099EC53BBE35175A0157E64716EDE
          C2EFEDB50726BE8F6A682032308077E5325485C925A7C87DC9CC568BD4161329
          6B244E432CAAF69BD527A6D0B3F3365E7B26C155BD19A7258637398DBF04AF4A
          E5DF411DECDC1E0DEFAB376021858D449B39A2F1529F4BE0FF16D3CF358A69A9
          9F377074E57CB5DE72FE0B42C953F0B2C476DB0000000049454E44AE426082}
      end
      object Bevel5: TBevel [13]
        AlignWithMargins = True
        Left = 470
        Top = 2
        Width = 1
        Height = 22
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        ExplicitLeft = 476
        ExplicitHeight = 43
      end
      object sbReport: TPngSpeedButton [14]
        AlignWithMargins = True
        Left = 473
        Top = 0
        Width = 32
        Height = 26
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        OnClick = sbReportClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000002B44944415478DA8D935F4853511CC7BF6773BA810DA5B1
          E67AA837A9B4873996218B2437B13F0FBE0CFA3308F7626EB03DE8430A8B8D28
          E7439A3421C8F6201A810FEACB40A82C17924E8CA93DD834219C9256DA74CE7B
          77EFEDDC5B6A7F46F4BBDC7B7FE7F7BBDFCFF99D737F870882805DB3785F5D57
          A9F3AECA08CED121A1571A0201C765F2BE7E16BA22810A17FE30F22BE0FCDDD7
          4C8FAB4C21A384DDB0F85EDD64D0175D47E2437AFBFDDC5CE1CB071776B2022E
          B58DA57A5D65AAF817B92805CBF16032024C4714985D15107E9BC0F3D1F927CF
          EE9CBD921570B16D6CBBCF55A65C1001047B80C3F92C0EA8E474293CAEB54F66
          867D66C55F00BFDFAF1A5756ADF73A8DB91FBF890042C53C850848A658303C8F
          8AA30AD8EE4D30FA85EE825028B4BD07A06207F53B269455F93D4E2356920AF0
          74091CCD31AC8014CB81A500833E0797DBC751F82EB8C9308CA7BFBFBF9BF87C
          3E4B5151D1B0C16080FF058BD00D03DD3405C4BA3274769E4252B412D13F7E88
          C07E7F1237CF6430383888E9E969AB0888D5D6D6966AB55A048341FC8F399D4E
          C4E371747676CE8B809D9696965C3131353585E2E2E27F8A67666660329924BF
          B1B151205EAF57A0002930343404A3D188B5B5B5AC628D46834824029BCD268D
          EBEBEB419A9B9B05BBDD2E0546464650595989E5E5E5AC009D4E8770388C9A9A
          9A7D4053539360B55AA5402C1643757535969696B202F47A3D060606505E5E2E
          8DDD6E3788C7E3112C168B14585C5CDC2B3F9D4E636B6B0BC964121B1B1BD22D
          FEF29292126912D1A816A4A1A181339BCD32B55A8D68348A63274EE2A0469BB5
          82D54F2BB8EDBF85402080442281D6D6561087C3F19036C56996654B55663782
          7506A418487D20F600C789678203471B4957A084F3D124463BEC623531FAC9D3
          DFCE425DD71B2E472E93613FF4D3FDF124B4BD33199E7FEC3C25DFCD7F073274
          5465D0BF6E470000000049454E44AE426082}
      end
      object Bevel6: TBevel [15]
        AlignWithMargins = True
        Left = 364
        Top = 2
        Width = 1
        Height = 22
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        ExplicitLeft = 311
        ExplicitTop = 4
        ExplicitHeight = 43
      end
      object sbImport2: TPngSpeedButton [16]
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 32
        Height = 26
        Hint = #1048#1084#1087#1086#1088#1090' '#1080#1079' Excel'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC00000016744558744372656174696F6E2054696D650031312F31
          342F3138D1C2EBDE0000001C74455874536F6674776172650041646F62652046
          697265776F726B732043533571B5E336000001F14944415478DA63FCFFFF3F03
          08B44E9C07611000D5F9498CC87C46900120CD400962F483D4A218C2D83A61CE
          FFBCB4588689B396106580B5A529C3B1E3A718AAF29319510CE0E16463F8F3F7
          1FC38F5F7F18BEFFFCCDF003886FDD7B08D6C4C9C1CEA02227C120C4C7CDF0FC
          CD0786C5AB36611A0002CC4C8C0C9FBE7C6378FDFE33C3DB0F9F199EBD7CC3C0
          CBCDC560A9AFC6C0CFC3C1C09AC5C6F07BDA2F86AE698B100634744DFBCFC2C6
          CE909F1683D500733D550639096106D13C31860FDF3E30FC9EF30B6C190B2B2B
          C480DA8EC9FFCBF2D2B086819B9D3983A18E1A58F3FBE91F1818438082EC5049
          2620E6001A50D1D4FBBFBA24071C067FFFFD878403D0FF8C0CFF18B8D859E19A
          A3CFE9322C35BA8C6201633C920120C0C6C2C4F0EBF71FA0F3BF30488BF2C335
          0BAF97C31A23EF363C62602CAE69FF2F2822060E03640398814E9410E667600D
          6263E00AE7C56AC0B7039F19180BAA5AFE37D794E20D039021424972601B199E
          81DC0EC5CC402ABBA4EE7F475335380CFE0153252865FF04A685B71FBF82D381
          AABC248394A820D81006490686DF53D1620166000870B0B100C9FF0C1FBF7C67
          B872FB31663A001AF27BDD2F06A0AB19A6743732C2F3427A7EE5FFDE8E469C06
          28CB4B31480AF3815D79F8D82906477B6B445E80E54673275FA272E3C97D9B31
          7323250000B644EEB7D61588CC0000000049454E44AE426082}
      end
      object sbSearch: TPngSpeedButton [17]
        AlignWithMargins = True
        Left = 229
        Top = 0
        Width = 32
        Height = 26
        Hint = #1055#1086#1080#1089#1082' '#1087#1086' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1091
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbSearchClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000003354944415478DA4D936D48535118C7FFE7DEBD38CD340B
          CBCA8A4A282242D372A2D9BB963315CA02CBA2202C2B2C50E895BE544A945064
          7D8A20C892A23E68B5B685BA19581619AE5CA4F46E6AE6A8DCAEDBDDB93D77B3
          D585739E7BCEBDE7F7FCCFF3C2144581FAAC39D1BAC3305E5F22302CA665040D
          16FCA0CE0A4D0A87CC798F7B48B1B19FC395F6CB265FF0F35FC0FAD36DF6EBFB
          17CF1704164B3B2242DBC127F48B82C1DF7EDCE870E3DBC751CFEBAEEE38C765
          D3681860AA7932507F202DCE390051602CE8999C22A07A961578FD32962745C1
          DAE3457BF720FA3EFABC6F5DAE0941C0B1EA8B717D5FBF0C89E0282C29C3ADAB
          17E097033874B21601CEE12380E40B605952049C830A64825A3ABFE1B1BDA79E
          EDDA571993386BCE4951D454386CF7B1A3FC30AED5D520253D133A7D24728A4A
          08AB8144C039B11CD106117AAD061EC987ADB5CF6576BCE652A2CBD9F93A61EA
          CC711DED6DC85E9387278E16A41A33D1E1B0E168F525444F8827151CA3A4C247
          8A388DACD97A149FEF9058AA71D9A984698947569936A1C566C6A4C989E8FFDC
          838CE5396835DFC3DC790B505A56053F57E00F70B22A00489DAEC1E65A021497
          EE1E164431D6FD6308DCE745F2D26C345B1AA1D5EBE0F578F1D33D8CDBD69774
          9800142F99AC1AB70553046C5115500C24537E81BEF7C3277C7DEFC28A75C5B8
          79AD0EB9A602F4F50FC2D27407E7AE3484B24109F3D3A4E66DEE448400DBCB0E
          2AA60D85B03C7A08F7F77EACCDDF425769446E5E01DAEC2D6879D4849B0F9E82
          ABDE49BA6AC9607E3C907FEAA9C43696EC528CD9ABD1F9BC1DEFBA9DD8BCAD0C
          36F35D24A765E04DD74BB45A9A607EF681D289E0C100C5422DCC450902561F73
          482C3D6BA54BA3D56144169366C447B35F29A5E86D38030DA5CAEBF1C0C734C8
          3A630D57241FABCE1B395AACA8B24BE14ACC3ADCEAADAF5C1271D6A9434ABC40
          52D52AA4C1107C57C600EA783520A3D648672A9AFF018C879A47EA8F2C31D474
          4530854E70A6062BD44F2A80D12A08A26E1348465D36E3697BAC9E302075AFAD
          F74175E6A411BF12457B02FE931B63D084AF60D00AA056E1069DF06BE14EF397
          3020ADDC5A2EFB9522C65832F98EFCD78A63F2FFB667C848548D2F040AC51FEC
          3AACAC5E27D6D50000000049454E44AE426082}
      end
      object Bevel7: TBevel [18]
        AlignWithMargins = True
        Left = 433
        Top = 2
        Width = 1
        Height = 22
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        ExplicitLeft = 542
        ExplicitTop = 4
        ExplicitHeight = 29
      end
      object sbClearFilter: TPngSpeedButton [19]
        AlignWithMargins = True
        Left = 197
        Top = 0
        Width = 32
        Height = 26
        Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1080#1083#1100#1090#1088
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbClearFilterClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000AEB00000A
          EB01828B0D5A0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000000B94944415478DA63FCFFFF3F032580
          D161CA0B8A4C607CF0E18740C2920FEF17C40890A411A88701A8479011E48587
          1F7F2A0005EE136B084CB33C3FFB07465818000D31004A9C276408B266B01790
          031168880350C17E5C86A06BC630006A480250E17C7443A09A0D819A2FA00422
          B668749CFAF23F230E2F8054EFCF1667C46B8013D080291B0319B4761D4311BF
          E666C590E3BF9E611F310600158135C00C81B18172C41B00D308023083E86B00
          455EA02810298E46520000704CAE7A8505FCA90000000049454E44AE426082}
      end
      object Bevel3: TBevel [20]
        AlignWithMargins = True
        Left = 539
        Top = 2
        Width = 1
        Height = 22
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        ExplicitLeft = 596
        ExplicitTop = -1
      end
      object sbClock: TPngSpeedButton [21]
        AlignWithMargins = True
        Left = 542
        Top = 0
        Width = 32
        Height = 26
        Hint = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1081
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbClockClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000000097048597300000B1300000B1301009A9C180000020E4944415478DA
          6364A031601CB560E85820EB3A574A53934F01C4BE7EFDD383C7BB939F51CD82
          9E4567DD0585F9E6DC7BF943869995E9BFA102F7ADBB0F5EE796C49AEEA6D882
          D0CACD96C6FA6A074EDFFDC0C6C1CDC9C0C6CEC6C0C5CECA6021CBFC69FDDE4B
          2EEB3AFC4E536441FBD24B7BCFDDFFECC42F2ACAC0C884502EC6C7C6C0F7E3F5
          9CF2789354B22DD0735BC49D9A697D79CFC5578A62D22228726CCC4C0CE6E2BF
          B64EAC3B1476F66CFA37327DF09FB16BD9B5D3F73E3118FF6566439111E16163
          10F8FD7A76799C491A45415434F9B49F8898D086A7DF18197FFDF90B713D0B33
          8386F0DF9F878EDDB45EDDE17396220B4060C6D6BBF97FFFB174BEFFF98F9D09
          C817E7667CF3F9F3E7A882305DCA5311188436B049FFD515335513D0FAFEF1F3
          F72B2FFFDE7FFAFDE96B86ED793F29B220B1EF640E270B532E3B072BFF9F3FFF
          1819FE333072B031FD16E365FBC2CDC6F0E1FD872F972EDEFFB06055B3DB5192
          2D289F7B65F983777F2278F8381998989850E478D85918AC35458011CDCCC0FA
          F7F387A3179E6494C518AF24DA028DC0856AA68E8637383839713A80979395C1
          555F9C415A8099E1FBE7B70FFDAB5638BCD850F880280B32261C6B7FF195B982
          839D8D011F3056126610E3676390E5FBC5B074EBF5C8B90DEE2B88B2A067EB35
          F9B72FFE443210009CECFF3FB133317EFFF8F1CB335EBEBFA72AA36DDF131D07
          D402A3160CBC05002213B81906054A2A0000000049454E44AE426082}
      end
      object sbBarrel: TPngSpeedButton [22]
        AlignWithMargins = True
        Left = 330
        Top = 0
        Width = 32
        Height = 26
        Hint = #1040#1085#1085#1091#1083#1080#1088#1086#1074#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbBarrelClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C08648800000009704859730000005E000000
          5E01F8D90FF90000001974455874536F667477617265007777772E696E6B7363
          6170652E6F72679BEE3C1A0000018D4944415478DA636480004E1909DEBBEE8E
          4A3F1988003BF7DF637FF2E2B32A90F9951126C8AF22B7EBBFA696363106305E
          BB76EDE3DD47AE6036548C5B5655F502B7960E0731067CBF71FDCBC39B370C81
          CC1F3003985595652F589BC9F11363C0E1930FBEDDBDF7541FC8FC09F7829589
          CCFAA31BA20340ECBF7FFF33744D3BFD61CDB6DBDF17F4BB49EA6A88A218601D
          B074C3B1334F0291BD003700A4D93A68E5F3B71FD945FEFEFDCB3AB3D5E4BFAB
          9D0223D1063C79FE99C12E64F58BBFFFB9059919BFBDDFB6D04FE2EBF7BF7FE2
          0A76BC4908D1E028CD3413C06B0088FDFBCF3F8665EBAE7F3F77EDF547662646
          862D7BEEFF67E79290E4E5FCF0E4D8FA0819A20DB870E3ED072686FF8C9B6106
          707C7A726C43187E039EBDFCC26013B8EAE55F066E012686AF402FF84B7CFB41
          82174081681BBCF2F9EB0FECC2C0406423391061D1D837FBCC87555B6E7F9BD7
          E32A454C34B20113D2796042E22526219D38F3F8E38DDB8F8C81CC5F30033815
          B4B4CE71A8AAF31063C0D71BD7BE3DBE79D30094AAA99699C8CECE00EECDFE11
          A5E0CDF20000000049454E44AE426082}
      end
      object sbPass: TPngSpeedButton [23]
        AlignWithMargins = True
        Left = 128
        Top = 0
        Width = 32
        Height = 26
        Hint = #1054#1092#1086#1088#1084#1080#1090#1100' '#1084#1072#1090#1077#1088#1080#1072#1083#1100#1085#1099#1081' '#1087#1088#1086#1087#1091#1089#1082
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        Visible = False
        OnClick = sbPassClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000180000001808040000004A7EF5
          730000000467414D410000B18F0BFC610500000002624B47440000AA8D233200
          0000097048597300000EC400000EC401952B0E1B0000000774494D4507E40304
          06271C438A8C2B000000E74944415478DA63642011300E4E0D090C050CE20C47
          194A181E10A3218921952191E115830F431D8331C347C21AAE31F8312C64D060
          C865B062B8C8309BB086F70C4A0C37800AA5188E3094325CC6ABFA0FC31C9006
          418674063BA0836C197C1916304C6758CFB0914199A10FE8A7DB408786316430
          7C63A86660666862E067980CD220CC90C6F088C19F410DC8FACEA0CEB0126896
          17C31786430C2C0C31407BEF0083C48F613950849F411E628339C307063D0607
          0605A00DF89DB40FA201021218F4190A89F1F408D4F00F18A4DF192281E9E83D
          8D6C78CD2002E7D530B412D6402218841A0054D6411794A43C2C000000257445
          5874646174653A63726561746500323032302D30332D30345430363A33393A32
          382B30303A3030CF2377D00000002574455874646174653A6D6F646966790032
          3032302D30332D30345430363A33393A32382B30303A3030BE7ECF6C00000019
          74455874536F667477617265007777772E696E6B73636170652E6F72679BEE3C
          1A0000000049454E44AE426082}
      end
      inherited plCount: TPanel
        Left = 572
        Width = 161
        Height = 26
        ExplicitLeft = 572
        ExplicitWidth = 161
        ExplicitHeight = 26
        inherited edCount: TDBEditEh
          Left = 119
          Width = 39
          Height = 20
          ControlLabel.ExplicitLeft = 13
          ControlLabel.ExplicitTop = 6
          ExplicitLeft = 119
          ExplicitWidth = 39
          ExplicitHeight = 20
        end
      end
    end
    inherited dgData: TDBGridEh
      Top = 63
      Width = 733
      Height = 372
      AllowedOperations = [alopUpdateEh]
      EvenRowColor = clWhite
      FixedColor = 15987699
      Font.Height = -12
      IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
      IndicatorParams.Color = 15987699
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      PopupMenu = pmDocFlow
      SearchPanel.Enabled = False
      SearchPanel.FilterEnabled = False
      SearchPanel.PersistentShowing = True
      SearchPanel.ShortCut = 24646
      STFilter.Color = 15725813
      TitleParams.SecondColor = 15987699
      OnCellClick = dgDataCellClick
    end
    inherited plHint: TPanel
      TabOrder = 5
    end
    object plStatus: TPanel
      Left = 0
      Top = 26
      Width = 733
      Height = 17
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 3618615
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 2
      StyleElements = [seFont, seBorder]
    end
    object plLinkedObjects: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 445
      Width = 727
      Height = 179
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object dgLinkedObjects: TDBGridEh
        Left = 0
        Top = 0
        Width = 727
        Height = 179
        Align = alClient
        Border.Color = clSilver
        Border.ExtendedDraw = True
        BorderStyle = bsNone
        Ctl3D = False
        DataSource = dsLinkedObjects
        DynProps = <>
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        GridLineParams.ColorScheme = glcsClassicEh
        GridLineParams.DataBoundaryColor = clPurple
        GridLineParams.DataHorzColor = 15395041
        GridLineParams.DataVertColor = 15395041
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorParams.FillStyle = cfstThemedEh
        IndicatorParams.HorzLineColor = 15263453
        IndicatorParams.VertLineColor = 15263453
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = pmLinkedObjects
        ReadOnly = True
        SelectionDrawParams.DrawFocusFrame = True
        SelectionDrawParams.DrawFocusFrameStored = True
        STFilter.Color = clSilver
        STFilter.HorzLineColor = clSilver
        STFilter.VertLineColor = clSilver
        TabOrder = 0
        TitleParams.FillStyle = cfstThemedEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -11
        TitleParams.Font.Name = 'Arial'
        TitleParams.Font.Style = [fsBold]
        TitleParams.HorzLineColor = 15263453
        TitleParams.ParentFont = False
        TitleParams.VertLineColor = 15263453
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'rus_object_type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1086#1073#1098#1077#1082#1090#1072
            Width = 114
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088
            Width = 102
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'kind_code'
            Footers = <>
            Title.Caption = #1042#1080#1076
            Width = 105
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'owner_code'
            Footers = <>
            Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
            Width = 135
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'state_code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = [fsItalic]
            Footers = <>
            Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 134
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'dlvtype_code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7697781
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1086#1087#1077#1088#1072#1094#1080#1080
            Width = 125
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'task_id'
            Footers = <>
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'o2s_id'
            Footers = <>
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'osk_id'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object plSearch: TPanel
      Left = 0
      Top = 43
      Width = 733
      Height = 20
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 4
      Visible = False
      object Label1: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 176
        Height = 14
        Align = alLeft
        Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072' '#1080#1083#1080' '#1087#1083#1072#1090#1092#1086#1088#1084#1099
        ExplicitHeight = 13
      end
      object edSearch: TEdit
        Left = 182
        Top = 0
        Width = 551
        Height = 20
        Align = alClient
        Color = 13828095
        TabOrder = 0
        StyleElements = [seFont, seBorder]
        OnKeyPress = edSearchKeyPress
        ExplicitHeight = 21
      end
    end
  end
  object plLeft: TPanel [3]
    Left = 0
    Top = 0
    Width = 193
    Height = 627
    Margins.Right = 0
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'plLeft'
    TabOrder = 2
    object Bevel4: TBevel
      Left = 192
      Top = 25
      Width = 1
      Height = 602
      Align = alRight
      ExplicitLeft = 215
      ExplicitHeight = 342
    end
    object dgFolders: TDBGridEh
      Left = 0
      Top = 25
      Width = 192
      Height = 602
      Align = alClient
      AutoFitColWidths = True
      Border.Color = 14211288
      BorderStyle = bsNone
      ColumnDefValues.Title.TitleButton = True
      Ctl3D = False
      DataSource = dsFolders
      DynProps = <>
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      GridLineParams.DataVertLines = False
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      IndicatorOptions = []
      Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghDialogFind, dghColumnResize, dghColumnMove]
      ParentCtl3D = False
      ParentFont = False
      PopupMenu = pmFolders
      ReadOnly = True
      SelectionDrawParams.DrawFocusFrame = True
      SelectionDrawParams.DrawFocusFrameStored = True
      SortLocal = True
      TabOrder = 0
      TitleParams.FillStyle = cfstThemedEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -13
      TitleParams.Font.Name = 'Tahoma'
      TitleParams.Font.Style = [fsBold]
      TitleParams.HorzLineColor = clSilver
      TitleParams.ParentFont = False
      TitleParams.VertLines = False
      OnDblClick = MenuItem11Click
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'folder_name'
          Footers = <>
          TextEditing = False
          Title.Caption = #1050#1072#1090#1072#1083#1086#1075
          Width = 214
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object plTool: TPanel
      Left = 0
      Top = 0
      Width = 193
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      Color = 15921906
      ParentBackground = False
      TabOrder = 1
      StyleElements = [seFont, seBorder]
      object sbWrap: TSpeedButton
        AlignWithMargins = True
        Left = 92
        Top = 0
        Width = 101
        Height = 24
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 1
        Align = alClient
        Caption = #1057#1074#1077#1088#1085#1091#1090#1100' '#1087#1072#1087#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 16278023
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsUnderline]
        ParentFont = False
        StyleElements = [seClient, seBorder]
        OnClick = sbWrapClick
        ExplicitLeft = 120
        ExplicitTop = 8
        ExplicitWidth = 23
        ExplicitHeight = 22
      end
      object cbAll: TCheckBox
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 86
        Height = 19
        Align = alLeft
        Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1074#1089#1077
        TabOrder = 0
        OnClick = cbAllClick
      end
    end
  end
  inherited pmGrid: TPopupMenu
    Left = 328
    Top = 128
    inherited N7: TMenuItem
      OnClick = btFilterClick
    end
  end
  inherited drvData: TADODataDriverEh
    Left = 184
    Top = 200
  end
  inherited meData: TMemTableEh
    AfterOpen = meDataAfterScroll
    BeforeDelete = meDataBeforeDelete
    BeforeRefresh = meDataBeforeRefresh
    Left = 232
    Top = 200
  end
  inherited dsData: TDataSource
    Left = 288
    Top = 200
  end
  inherited qrAux: TADOQuery
    Top = 200
  end
  inherited drvForms: TADODataDriverEh
    Left = 532
    Top = 194
  end
  inherited meForms: TMemTableEh
    Left = 580
    Top = 194
  end
  inherited dsForms: TDataSource
    Left = 628
    Top = 194
  end
  object pmDocFlow: TPopupMenu [12]
    Left = 264
    Top = 128
    object MenuItem1: TMenuItem
      Action = aAdd
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    end
    object MenuItem2: TMenuItem
      Action = aDelete
      Caption = #1059#1076#1072#1083#1080#1090#1100
    end
    object MenuItem3: TMenuItem
      Action = aEdit
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object MenuItem4: TMenuItem
      Caption = #1055#1088#1086#1074#1077#1089#1090#1080
      ShortCut = 16473
      OnClick = sbConfirmClick
    end
    object N8: TMenuItem
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      ShortCut = 16452
      OnClick = btFlowClick
    end
    object N116: TMenuItem
      Tag = 99
      Caption = '-'
      Visible = False
    end
    object N115: TMenuItem
      Tag = 99
      Caption = #1056#1072#1079#1088#1077#1096#1080#1090#1100' '#1086#1087#1077#1088#1072#1094#1080#1102' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
      Visible = False
    end
    object N12: TMenuItem
      Caption = '-'
    end
    object N11: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ShortCut = 116
      OnClick = N11Click
    end
    object N10: TMenuItem
      Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100' '#1074' '#1076#1088#1091#1075#1091#1102' '#1087#1072#1087#1082#1091
      ShortCut = 16461
      OnClick = N10Click
    end
    object MenuItem5: TMenuItem
      Caption = '-'
    end
    object MenuItem6: TMenuItem
      Caption = #1060#1080#1083#1100#1090#1088
      ShortCut = 16454
      OnClick = btFilterClick
    end
    object N18: TMenuItem
      Caption = #1055#1086#1080#1089#1082' '#1087#1086' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1091'/'#1074#1072#1075#1086#1085#1091
      ShortCut = 16455
      OnClick = sbSearchClick
    end
    object MenuItem7: TMenuItem
      Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
      ShortCut = 16453
      OnClick = btExcelClick
    end
    object N19: TMenuItem
      Caption = #1055#1077#1095#1072#1090#1100
      ShortCut = 16464
      OnClick = sbReportClick
    end
  end
  inherited al: TActionList
    Left = 416
    Top = 200
  end
  object drvFolders: TADODataDriverEh [14]
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select * from folders where folder_section = :folder_section ord' +
        'er by folder_name')
    SelectCommand.Parameters = <
      item
        Name = 'folder_section'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update folders set '
      'folder_name = :folder_name,'
      'parent_id = :parent_id'
      'where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'folder_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'parent_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into folders (folder_name, folder_section, parent_id)'
      'values ( :folder_name, :folder_section, :parent_id )')
    InsertCommand.Parameters = <
      item
        Name = 'folder_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'folder_section'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'parent_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from folders where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from folders where id = :id')
    GetrecCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 16
    Top = 176
  end
  object meFolders: TMemTableEh [15]
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvFolders
    TreeList.Active = True
    TreeList.KeyFieldName = 'id'
    TreeList.RefParentFieldName = 'parent_id'
    BeforeInsert = meFoldersBeforeInsert
    BeforeEdit = meFoldersBeforeEdit
    BeforePost = meFoldersBeforePost
    AfterScroll = meFoldersAfterScroll
    Left = 64
    Top = 176
  end
  object dsFolders: TDataSource [16]
    DataSet = meFolders
    Left = 120
    Top = 176
  end
  object pmFolders: TPopupMenu [17]
    Left = 72
    Top = 264
    object MenuItem9: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 45
      OnClick = MenuItem9Click
    end
    object MenuItem10: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 46
      OnClick = MenuItem10Click
    end
    object MenuItem11: TMenuItem
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
      ShortCut = 113
      OnClick = MenuItem11Click
    end
    object MenuItem12: TMenuItem
      Caption = '-'
    end
    object MenuItem13: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ShortCut = 116
      OnClick = MenuItem13Click
    end
    object N13: TMenuItem
      Caption = '-'
    end
    object N14: TMenuItem
      Caption = #1056#1072#1089#1082#1088#1099#1090#1100' '#1074#1089#1077
      OnClick = N14Click
    end
  end
  object drvLinkedObjects: TADODataDriverEh [18]
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select distinct ds.doc_id, o.id, o.linked_object_id, o.object_ty' +
        'pe, o.rus_object_type, o.cnum, o.kind_code, ds.task_id, ds.id as' +
        ' o2s_id, ls.id as osk_id,'
      
        '(select code from counteragents c where c.id = o.owner_id) as ow' +
        'ner_code, '
      
        '(select code from objectstatekinds k where k.id = ls.object_stat' +
        'e_id) as state_code,'
      
        '(select code from deliverytypes dt, tasks t where t.dlv_type_id ' +
        '= dt.id and t.id = ds.task_id) as dlvtype_code'
      'from v_objects o, objects2docspec ds, v_lastobjectstates ls'
      
        'where o.id = ds.object_id  and ls.task_id = ds.task_id and ls.ob' +
        'ject_id = o.id and ds.doc_id = :doc_id;')
    SelectCommand.Parameters = <
      item
        Name = 'doc_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      '')
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 337
    Top = 530
  end
  object meLinkedObjects: TMemTableEh [19]
    Params = <>
    DataDriver = drvLinkedObjects
    TreeList.Active = True
    TreeList.KeyFieldName = 'id'
    TreeList.RefParentFieldName = 'linked_object_id'
    TreeList.DefaultNodeExpanded = True
    BeforeOpen = meLinkedObjectsBeforeOpen
    Left = 449
    Top = 530
  end
  object dsLinkedObjects: TDataSource [20]
    DataSet = meLinkedObjects
    Left = 537
    Top = 530
  end
  object pmLinkedObjects: TPopupMenu [21]
    Left = 248
    Top = 528
    object MenuItem25: TMenuItem
      Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
      ShortCut = 16453
      OnClick = MenuItem25Click
    end
    object N15Collapse: TMenuItem
      Caption = #1057#1074#1077#1088#1085#1091#1090#1100' '#1074#1089#1077
      OnClick = N15CollapseClick
    end
    object N155: TMenuItem
      Caption = '-'
    end
    object N166: TMenuItem
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1076#1074#1080#1078#1077#1085#1080#1077
      OnClick = N166Click
    end
  end
  inherited exReport: TEXLReport
    DataSet = qrReport
    Left = 462
    Top = 200
  end
  object qrReport: TADOQuery
    Connection = dm.connMain
    Parameters = <>
    Left = 696
    Top = 192
  end
  object drvPass: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select *, isnull(p.car_data,'#39#39')+'#39' '#39'+isnull(p.car_number,'#39#39') as c' +
        'ar_full,'
      
        '(select person_name from persons pr where pr.id = p.driver_id) a' +
        's driver_name'
      'from passes p order by pass_date')
    SelectCommand.Parameters = <>
    UpdateCommand.CommandText.Strings = (
      'update passes'
      'set'
      '  pass_date = :pass_date,'
      '  pass_number = :pass_number,'
      '  car_data = :car_data,'
      '  car_number = :car_number,'
      '  driver_id = :driver_id,'
      '  attorney = :attorney,'
      '  user_id = :user_id,'
      '  date_created = :date_created'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'pass_date'
        Size = -1
        Value = Null
      end
      item
        Name = 'pass_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_data'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'driver_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'attorney'
        Size = -1
        Value = Null
      end
      item
        Name = 'user_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'date_created'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into passes'
      
        '  (pass_date, pass_number, car_data, car_number, driver_id, atto' +
        'rney, user_id, date_created)'
      'values'
      
        '  (:pass_date, :pass_number, :car_data, :car_number, :driver_id,' +
        ' :attorney, :user_id, :date_created)')
    InsertCommand.Parameters = <
      item
        Name = 'pass_date'
        Size = -1
        Value = Null
      end
      item
        Name = 'pass_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_data'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'driver_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'attorney'
        Size = -1
        Value = Null
      end
      item
        Name = 'user_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'date_created'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from passes where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select *, isnull(p.car_data,'#39#39')+'#39' '#39'+isnull(p.car_number,'#39#39') as c' +
        'ar_full,'
      
        '(select person_name from persons pr where pr.id = p.driver_id) a' +
        's driver_name'
      'from passes p where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 400
    Top = 296
  end
  object mePass: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvPass
    BeforeOpen = meDataBeforeOpen
    AfterOpen = meDataAfterOpen
    AfterInsert = meDataAfterInsert
    BeforeEdit = meDataBeforeEdit
    AfterEdit = meDataAfterEdit
    BeforePost = meDataBeforePost
    AfterPost = meDataAfterPost
    AfterScroll = meDataAfterScroll
    Left = 448
    Top = 296
  end
  object IL: TPngImageList
    PngImages = <
      item
        Background = clNone
        Name = 'stop'
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E33600000016744558744372656174696F6E2054
          696D650030342F32342F32302D0665C60000007C4944415478DA63FCFFFF3F03
          258011C300464607205900C4FE686A3702F10486FFFF0FE0368091710290CC27
          60E944A02105980610A719C31088011067EF27D1FB8E20EFC00CD880C5CF84C0
          46A00101300320FE203646181919A0EA198791011407A2030345D10871160509
          09E1370A9232C2109077C8CC4C6400009B6E6DE147AE976D0000000049454E44
          AE426082}
      end>
    Left = 654
    Top = 307
    Bitmap = {}
  end
end
