﻿inherited FormUserColumns: TFormUserColumns
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1082#1086#1083#1086#1085#1082#1080
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    inherited btnOk: TButton
      Caption = #1047#1072#1082#1088#1099#1090#1100
    end
  end
  inherited plAll: TPanel
    inherited plTop: TPanel
      inherited sbDelete: TPngSpeedButton
        Visible = False
      end
      inherited sbAdd: TPngSpeedButton
        Visible = False
      end
      inherited sbEdit: TPngSpeedButton
        Visible = False
      end
      inherited Bevel2: TBevel
        Visible = False
      end
      inherited btTool: TPngSpeedButton
        Visible = False
      end
    end
    inherited dgData: TDBGridEh
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1055#1086#1088#1103#1076#1086#1082
          Width = 69
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1048#1084#1103' '#1087#1086#1083#1103
          Width = 84
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1047#1072#1075#1086#1083#1086#1074#1086#1082
          Width = 240
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1064#1080#1088#1080#1085#1072
          Width = 70
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1042#1080#1076#1080#1084#1086#1089#1090#1100
          Width = 83
        end>
    end
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
    inherited aDelete: TAction
      Enabled = False
    end
    inherited aEdit: TAction
      Enabled = False
    end
  end
end
