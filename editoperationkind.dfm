﻿inherited FormEditOperationKind: TFormEditOperationKind
  Caption = #1054#1087#1077#1088#1072#1094#1080#1103
  ClientHeight = 314
  ClientWidth = 527
  ExplicitWidth = 533
  ExplicitHeight = 342
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 13
    Top = 14
    Width = 24
    Height = 16
    Caption = #1050#1086#1076
  end
  object Label3: TLabel [1]
    Left = 13
    Top = 65
    Width = 98
    Height = 16
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  object Label4: TLabel [2]
    Left = 13
    Top = 130
    Width = 139
    Height = 16
    Caption = #1055#1088#1080#1084#1077#1085#1080#1084#1086' '#1082' '#1091#1089#1083#1091#1075#1077
  end
  object Label1: TLabel [3]
    Left = 13
    Top = 200
    Width = 126
    Height = 16
    Caption = #1055#1086#1088#1086#1078#1076#1072#1077#1090' '#1091#1089#1083#1091#1075#1091
  end
  inherited plBottom: TPanel
    Top = 273
    Width = 527
    TabOrder = 2
    ExplicitTop = 273
    ExplicitWidth = 527
    inherited btnCancel: TButton
      Left = 411
      ExplicitLeft = 411
    end
    inherited btnOk: TButton
      Left = 292
      ExplicitLeft = 292
    end
  end
  object edCode: TDBEditEh [5]
    Left = 13
    Top = 32
    Width = 305
    Height = 24
    DataField = 'code'
    DataSource = FormOperationKinds.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object edName: TDBEditEh [6]
    Left = 13
    Top = 84
    Width = 505
    Height = 24
    DataField = 'name'
    DataSource = FormOperationKinds.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 376
    Top = 24
  end
  inherited qrAux: TADOQuery
    Left = 432
    Top = 24
  end
end
