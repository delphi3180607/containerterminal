﻿unit psvzhrep;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  EhLibVCL,
  GridsEh, PivotGridsEh, PivotGridToolsEh, Vcl.ExtCtrls, Vcl.ComCtrls,
  MemTableDataEh, Data.Win.ADODB, DataDriverEh, ADODataDriverEh, MemTableEh,
  Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, Vcl.Buttons, PngSpeedButton, EhLibMTE,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, DBAxisGridsEh,
  DBGridEh, System.Generics.Collections;

type
  TFormPsVzhRep = class(TForm)
    plAll: TPanel;
    plTop: TPanel;
    PageControl1: TPageControl;
    TabSheetPivot: TTabSheet;
    TabSheetData: TTabSheet;
    dsDetails: TDataSource;
    meData: TMemTableEh;
    drvData: TADODataDriverEh;
    edStart: TDBDateTimeEditEh;
    edEnd: TDBDateTimeEditEh;
    sbBuild: TPngSpeedButton;
    DBGridEh1: TDBGridEh;
    dgPivot: TDBGridEh;
    mePivot: TMemTableEh;
    dsPivot: TDataSource;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    procedure sbBuildClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    rowFields: TStringList;
    colFields: TStringList;
    procedure MakePivotDataSet;
  end;

var
  FormPsVzhRep: TFormPsVzhRep;

implementation

{$R *.dfm}

procedure TFormPsVzhRep.sbBuildClick(Sender: TObject);
begin
  meData.Close;
  meData.Open;

  rowFields.Add('has_sea_path');
  rowFields.Add('paid_sea_freight');
  rowFields.Add('code');
  colFields.Add('yearop');
  colFields.Add('monthop');

  MakePivotDataSet;
end;

procedure TFormPsVzhRep.FormCreate(Sender: TObject);
begin
  rowFields := TStringList.Create;
  colFields := TStringList.Create;
end;

procedure TFormPsVzhRep.MakePivotDataSet;
var i, colnum, level, max_level: integer; nn: string;
  watch_values: TList<Variant>;
  watch_fields: TList<string>;
  colrow_signs: TList<string>;
  pivot_columns: TStringList;
  pivot_column_levels: TList<integer>;
  indicator: real;
  col: TColumnEh;
  mustReInit: boolean;

  procedure ReInitWatchValues;
  var i: integer;
  begin

    watch_values.clear;

    for i := colFields.Count-1 downto 0 do
    begin
      watch_values.Add(meData.FieldByName(colFields[i]).Value);
    end;

    for i := rowFields.Count-1 downto 0 do
    begin
      watch_values.Add(meData.FieldByName(rowFields[i]).Value);
    end;

  end;


begin

  watch_fields := TList<string>.Create;
  watch_values := TList<Variant>.Create;
  colrow_signs := TList<string>.Create;
  pivot_columns := TStringList.Create;
  pivot_column_levels := TList<integer>.Create;

  mePivot.Close;
  mePivot.DestroyTable;
  mePivot.FieldDefs.Clear;

  mePivot.FieldDefs.Add('ID',ftInteger,0);
  mePivot.FieldDefs.Add('PARENT_ID',ftInteger,0);
  mePivot.FieldDefs.Add('INDICATOR',ftString,250);

  for i := colFields.Count-1 downto 0 do
  begin
    watch_fields.Add(colFields[i]);
    colrow_signs.Add('c');
  end;

  for i := rowFields.Count-1 downto 0 do
  begin
    watch_fields.Add(rowFields[i]);
    colrow_signs.Add('r');
  end;

  meData.First;

  ReInitWatchValues;

  for i := 0 to watch_fields.Count-1 do
  begin
    Memo1.Lines.Add(VarToStr(watch_fields[i]));
  end;
  Memo1.Lines.Add('---');
  for i := 0 to watch_values.Count-1 do
  begin
    Memo1.Lines.Add(VarToStr(watch_values[i]));
  end;


  level := 0;
  max_level := watch_fields.Count-1;
  colnum := 0;

  while true do
  begin

     mustReinit := false;
     meData.Next;
     if meData.Eof then break;

    // отслеживание изменений значений полей для level
    // Сверка полей снизу вверх

    for level := 0 to max_level do
    begin

      if colrow_signs[level] = 'c' then
      begin
          if pivot_columns.IndexOf(watch_values[level])<0 then
          begin
            nn := 'C'+IntToStr(colnum);
            pivot_columns.Add(watch_values[level]);
            pivot_column_levels.Add(level);
            mePivot.FieldDefs.Add(nn,ftVariant,0);
            colnum := colnum+1;
          end;
      end;

      if (meData.FieldByName(watch_fields[level]).Value <> watch_values[level])
      then begin
        mustReInit := true;
      end;

    end;

    if mustReinit then ReInitWatchValues;

  end;

  mePivot.CreateDataSet;
  mePivot.Open;

  meData.First;
  ReInitWatchValues;

  mePivot.Append;
  mePivot.Post;

  level := 0;
  max_level := watch_fields.Count-1;
  indicator := 0;

  while true do
  begin

    mustReinit := false;
    meData.Next;
    indicator := indicator+1;
    if meData.Eof then break;

    // отслеживание изменений значений полей для level
    // Сверка полей снизу вверх

    for level := 0 to max_level do
    begin

      if (meData.FieldByName(watch_fields[level]).Value <> watch_values[level])
      then begin

        if colrow_signs[level] = 'c' then
        begin

          // Вычисляем колонку
          colnum := pivot_columns.IndexOf(watch_values[level]);
          nn := 'C'+IntToStr(colnum);

          //-- записываем значения
          mePivot.Edit;
          mePivot.FieldByName(nn).Value := indicator;
          mePivot.Post;

        end else
        begin

          // -- пишем боковину, добавляем строки
          mePivot.Edit;
          mePivot.FieldByName('INDICATOR').Value := VarToStr(watch_values[level]);
          mePivot.Post;

          mePivot.Append;
          mePivot.Post;

        end;

        indicator := 0;
        mustReinit := true;

      end;

    end;

    if mustReinit then ReInitWatchValues;

  end;

  dgPivot.Columns.Clear;

  col := dgPivot.Columns.Add;
  col.FieldName := 'INDICATOR';
  col.Title.Caption := 'Показатель';

  for i:= 0 to pivot_columns.Count-1 do
  begin

      nn := 'C'+IntToStr(i);
      col := dgPivot.Columns.Add;
      col.FieldName := nn;
      col.Title.Caption := pivot_columns[i];

  end;

end;

end.
