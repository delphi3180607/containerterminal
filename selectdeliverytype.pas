﻿unit selectdeliverytype;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBGridEh, Vcl.Mask, DBCtrlsEh, DBLookupEh,
  DBSQLLookUp;

type
  TFormSelectDeliveryType = class(TFormEdit)
    Label16: TLabel;
    ssDlvTypes: TADOLookUpSqlSet;
    laDlvTypes: TDBSQLLookUp;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSelectDeliveryType: TFormSelectDeliveryType;

implementation

{$R *.dfm}

end.
