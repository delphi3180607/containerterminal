﻿unit deliverytypes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Vcl.Mask, DBCtrlsEh;

type
  TFormDeliveryTypes = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormDeliveryTypes: TFormDeliveryTypes;

implementation

{$R *.dfm}

uses editdeliverytype;

procedure TFormDeliveryTypes.Init;
begin
  inherited;
  tablename := 'deliverytypes';
  self.formEdit := FormEditDeliveryType;
  meData.Filter := '';
  meData.Filtered := false;
end;

end.
