﻿inherited FormChildDocs: TFormChildDocs
  Caption = #1057#1074#1103#1079#1072#1085#1085#1099#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited plTop: TPanel
      inherited sbDelete: TPngSpeedButton
        Visible = False
      end
      inherited sbAdd: TPngSpeedButton
        Visible = False
      end
      inherited sbEdit: TPngSpeedButton
        Visible = False
      end
      inherited Bevel2: TBevel
        Visible = False
      end
    end
    inherited dgData: TDBGridEh
      Columns = <
        item
          CellButtons = <>
          Checkboxes = False
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_type_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 257
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 149
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 123
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isconfirmed'
          Footers = <>
          Title.Caption = #1055#1088#1086#1074#1077#1076#1077#1085' ?'
          Width = 167
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from f_GetLinkedDocuments( :DocId);')
    SelectCommand.Parameters = <
      item
        Name = 'DocId'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
    inherited aDelete: TAction
      Enabled = False
    end
    inherited aEdit: TAction
      Enabled = False
    end
  end
end
