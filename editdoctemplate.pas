﻿unit EditDocTemplate;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, SynEdit, SynMemo, Vcl.DBCtrls;

type
  TFormEditDocTemplate = class(TFormEdit)
    Label7: TLabel;
    edTitle: TDBEditEh;
    edTemplate: TDBEditEh;
    Label4: TLabel;
    Label1: TLabel;
    meSQL: TDBMemo;
    edGroupField: TDBEditEh;
    Label2: TLabel;
    procedure edTemplateEditButtons0Click(Sender: TObject;
      var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDocTemplate: TFormEditDocTemplate;

implementation

{$R *.dfm}

uses selectdoctemplate;

procedure TFormEditDocTemplate.edTemplateEditButtons0Click(Sender: TObject;
  var Handled: Boolean);
begin
  FormSelectDocTemplate.Init;
  FormSelectDocTemplate.ShowModal;

  if FormSelectDocTemplate.ModalResult = mrOk then
  begin
     edTemplate.Text := FormSelectDocTemplate.FileList.Selected.Caption;
  end;

end;

end.
