﻿unit tasksincome;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, tasks, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand,
  EXLReport;

type
  TFormTasksIncome = class(TFormTasks)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormTasksIncome: TFormTasksIncome;

implementation

{$R *.dfm}

procedure TFormTasksIncome.Init;
begin
  inherited;
  self.system_filter := 'income';
  self.system_filter_name := 'грузодоставка';
end;


end.
