﻿inherited FormEditPerson: TFormEditPerson
  Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086
  ClientHeight = 350
  ClientWidth = 534
  ExplicitWidth = 540
  ExplicitHeight = 378
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 16
    Top = 14
    Width = 24
    Height = 16
    Caption = #1042#1080#1076
  end
  object Label2: TLabel [1]
    Left = 16
    Top = 70
    Width = 26
    Height = 16
    Caption = #1048#1084#1103
  end
  object Label18: TLabel [2]
    Left = 16
    Top = 179
    Width = 121
    Height = 14
    Caption = #1052#1072#1088#1082#1072' '#1072#1074#1090#1086#1084#1086#1073#1080#1083#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label27: TLabel [3]
    Left = 186
    Top = 178
    Width = 121
    Height = 14
    Caption = #1053#1086#1084#1077#1088' '#1072#1074#1090#1086#1084#1086#1073#1080#1083#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label25: TLabel [4]
    Left = 16
    Top = 241
    Width = 92
    Height = 14
    Caption = #1044#1086#1074#1077#1088#1077#1085#1085#1086#1089#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 309
    Width = 534
    TabOrder = 6
    ExplicitTop = 244
    ExplicitWidth = 536
    inherited btnCancel: TButton
      Left = 418
      ExplicitLeft = 420
    end
    inherited btnOk: TButton
      Left = 299
      ExplicitLeft = 301
    end
  end
  object cbKind: TDBComboBoxEh [6]
    Left = 16
    Top = 32
    Width = 305
    Height = 24
    DataField = 'person_kind'
    DataSource = FormPersons.dsData
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Items.Strings = (
      #1044#1080#1089#1087#1077#1090#1095#1077#1088
      #1042#1086#1076#1080#1090#1077#1083#1100)
    KeyItems.Strings = (
      '0'
      '1')
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edFieldName: TDBEditEh [7]
    Left = 16
    Top = 88
    Width = 505
    Height = 24
    DataField = 'person_name'
    DataSource = FormPersons.dsData
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object cbIsBlocked: TDBCheckBoxEh [8]
    Left = 16
    Top = 140
    Width = 168
    Height = 17
    Caption = #1047#1072#1073#1083#1086#1082#1080#1088#1086#1074#1072#1085'?'
    DataField = 'isblocked'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 2
  end
  object edCarData: TDBEditEh [9]
    Left = 16
    Top = 197
    Width = 161
    Height = 22
    DataField = 'car_data'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object edCarNumber: TDBEditEh [10]
    Left = 186
    Top = 197
    Width = 174
    Height = 22
    DataField = 'car_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 4
    Visible = True
  end
  object edAttorney: TDBEditEh [11]
    Left = 16
    Top = 261
    Width = 505
    Height = 22
    DataField = 'attorney'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
end
