﻿unit login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TFormLogin = class(TForm)
    BtnOk: TBitBtn;
    BtnCancel: TBitBtn;
    lePassword: TLabeledEdit;
    leLogin: TComboBox;
    Label1: TLabel;
    procedure Panel2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure plTopMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    procedure CreateParams(var Params: TCreateParams); override;
  public
    { Public declarations }
  end;

var
  FormLogin: TFormLogin;

implementation

{$R *.dfm}


procedure TFormLogin.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.ExStyle := Params.ExStyle OR WS_EX_APPWINDOW;
end;

procedure TFormLogin.Panel2MouseDown(
Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer
);
begin
  ReleaseCapture;
  Perform(WM_SysCommand,$F012,0);
end;

procedure TFormLogin.plTopMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  ReleaseCapture;
  Perform(WM_SysCommand,$F012,0);
end;

end.
