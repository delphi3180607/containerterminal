﻿inherited FormExtraInfo: TFormExtraInfo
  Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1072#1103' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103
  ClientHeight = 575
  ClientWidth = 928
  ExplicitWidth = 934
  ExplicitHeight = 603
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 534
    Width = 928
    ExplicitTop = 534
    ExplicitWidth = 928
    inherited btnCancel: TButton
      Left = 812
      Visible = False
      ExplicitLeft = 812
    end
    inherited btnOk: TButton
      Left = 693
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ExplicitLeft = 693
    end
  end
  object plLeft: TPanel [1]
    AlignWithMargins = True
    Left = 2
    Top = 3
    Width = 102
    Height = 528
    Margins.Left = 2
    Margins.Right = 0
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    object sbInv: TPngSpeedButton
      Left = 0
      Top = 96
      Width = 102
      Height = 48
      Align = alTop
      GroupIndex = 1
      Caption = #1057#1095#1077#1090#1072
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = sbInvClick
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 143
    end
    object sbNotes: TPngSpeedButton
      Left = 0
      Top = 48
      Width = 102
      Height = 48
      Align = alTop
      GroupIndex = 1
      Caption = #1055#1080#1089#1100#1084#1072
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = sbNotesClick
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 143
    end
    object sbRestr: TPngSpeedButton
      Left = 0
      Top = 0
      Width = 102
      Height = 48
      Align = alTop
      GroupIndex = 1
      Down = True
      Caption = #1047#1072#1087#1088#1077#1090#1099
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = sbRestrClick
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 143
    end
  end
  object plAll: TPanel [2]
    AlignWithMargins = True
    Left = 104
    Top = 3
    Width = 821
    Height = 528
    Margins.Left = 0
    Align = alClient
    BevelOuter = bvNone
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 2
  end
  inherited dsLocal: TDataSource
    Left = 344
    Top = 512
  end
  inherited qrAux: TADOQuery
    Left = 408
    Top = 504
  end
end
