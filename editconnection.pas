﻿unit editconnection;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEditConnection = class(TFormEdit)
    edServer: TDBEditEh;
    edLogin: TDBEditEh;
    edPassword: TDBEditEh;
    edDataBase: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditConnection: TFormEditConnection;

implementation

{$R *.dfm}

end.
