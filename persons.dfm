﻿inherited FormPersons: TFormPersons
  Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1077' '#1083#1080#1094#1072
  ClientWidth = 1063
  ExplicitWidth = 1079
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Width = 1063
    inherited btnOk: TButton
      Left = 828
    end
    inherited btnCancel: TButton
      Left = 947
    end
  end
  inherited plAll: TPanel
    Width = 1063
    inherited plTop: TPanel
      Width = 1063
      inherited btTool: TPngSpeedButton
        Left = 1025
      end
      inherited plCount: TPanel
        Left = 828
      end
    end
    inherited dgData: TDBGridEh
      Width = 1059
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'person_name'
          Footers = <>
          Title.Caption = #1048#1084#1103
          Width = 261
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'person_kind_text'
          Footers = <>
          Title.Caption = #1042#1080#1076
          Width = 118
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isblocked'
          Footers = <>
          Title.Caption = #1047#1072#1073#1083#1086#1082#1080#1088#1086#1074#1072#1085
          Width = 109
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_data'
          Footers = <>
          Title.Caption = #1052#1072#1088#1082#1072' '#1072#1074#1090#1086#1084#1086#1073#1080#1083#1103
          Width = 103
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 122
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'attorney'
          Footers = <>
          Title.Caption = #1044#1086#1074#1077#1088#1077#1085#1085#1086#1089#1090#1100
          Width = 261
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'select id, person_name, person_kind, isblocked, car_data, car_nu' +
        'mber, attorney,'
      
        '(case when person_kind = 0 then '#39#1044#1080#1089#1087#1077#1090#1095#1077#1088#39' when person_kind = 1' +
        ' then '#39#1042#1086#1076#1080#1090#1077#1083#1100#39' end) as person_kind_text'
      'from persons order by person_name')
    UpdateCommand.CommandText.Strings = (
      'update persons set '
      'person_name = :person_name, '
      'person_kind = :person_kind,'
      'isblocked = :isblocked,'
      'car_data = :car_data, '
      'car_number = :car_number,'
      'attorney = :attorney'
      'where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'person_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'person_kind'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isblocked'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'car_data'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'attorney'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      
        'insert into persons (person_name, person_kind, car_data, car_num' +
        'ber, attorney)'
      
        'values (  :person_name, :person_kind, :car_data, :car_number, :a' +
        'ttorney )')
    InsertCommand.Parameters = <
      item
        Name = 'person_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'person_kind'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'car_data'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'attorney'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from persons where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select id, person_name, person_kind, isblocked, car_data, car_nu' +
        'mber, attorney,'
      
        '(case when person_kind = 0 then '#39#1044#1080#1089#1087#1077#1090#1095#1077#1088#39' when person_kind = 1' +
        ' then '#39#1042#1086#1076#1080#1090#1077#1083#1100#39' end) as person_kind_text'
      'from persons where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
  end
end
