﻿inherited FormSelectDocForm: TFormSelectDocForm
  Caption = #1042#1099#1073#1086#1088' '#1096#1072#1073#1083#1086#1085#1072' '#1087#1077#1095#1072#1090#1080
  ClientHeight = 206
  ClientWidth = 447
  ExplicitWidth = 453
  ExplicitHeight = 234
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 165
    Width = 447
    ExplicitTop = 165
    ExplicitWidth = 447
    inherited btnCancel: TButton
      Left = 331
      ExplicitLeft = 331
    end
    inherited btnOk: TButton
      Left = 212
      Caption = #1055#1077#1095#1072#1090#1072#1090#1100
      ExplicitLeft = 212
    end
  end
  object DBGridEh1: TDBGridEh [1]
    Left = 0
    Top = 0
    Width = 447
    Height = 165
    Align = alClient
    AllowedOperations = []
    AutoFitColWidths = True
    DataSource = dsTemplates
    DynProps = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    IndicatorParams.Color = clSilver
    IndicatorParams.FillStyle = cfstGradientEh
    IndicatorParams.HorzLineColor = clSilver
    IndicatorParams.VertLineColor = clSilver
    Options = [dgEditing, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -13
    TitleParams.Font.Name = 'Verdana'
    TitleParams.Font.Style = [fsBold]
    TitleParams.ParentFont = False
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'title'
        Footers = <>
        Title.Caption = #1064#1072#1073#1083#1086#1085
        Width = 383
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qrTemplates: TADOQuery
    Connection = dm.connMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'doctype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from doctemplates '
      'where doctype_id = :doctype_id'
      'order by title')
    Left = 160
    Top = 64
  end
  object dsTemplates: TDataSource
    DataSet = qrTemplates
    Left = 232
    Top = 64
  end
end
