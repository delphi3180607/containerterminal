﻿unit archivefeeds;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  Vcl.ExtCtrls, EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions,
  Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus,
  Vcl.StdCtrls, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Buttons,
  PngSpeedButton, Vcl.Mask, DBCtrlsEh, Functions;

type
  TFormArchiveFeeds = class(TFormGrid)
    Splitter1: TSplitter;
    dgDates: TDBGridEh;
    N8: TMenuItem;
    N9: TMenuItem;
    Panel6: TPanel;
    dtStart: TDBDateTimeEditEh;
    dsDates: TDataSource;
    drvDates: TADODataDriverEh;
    meDates: TMemTableEh;
    sbClearFilter: TPngSpeedButton;
    plStatus: TPanel;
    btReset: TButton;
    procedure meDatesBeforeOpen(DataSet: TDataSet);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure meDatesAfterScroll(DataSet: TDataSet);
    procedure btFilterClick(Sender: TObject);
    procedure sbClearFilterClick(Sender: TObject);
    procedure meDatesAfterOpen(DataSet: TDataSet);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure btResetClick(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
  private
    //--
  public
    procedure Init; override;
    procedure SetFilter(conditions: string);
    procedure AssignSelField(colname: string); override;
  end;

var
  FormArchiveFeeds: TFormArchiveFeeds;

implementation

{$R *.dfm}

uses filterlite, dmu;

procedure TFormArchiveFeeds.btFilterClick(Sender: TObject);
var fl: TStringList;
begin
  FormFilterLite.ShowModal;
  if FormFilterLite.ModalResult in [mrOk, mrYes] then
  begin
    CreateGuid(gu);
    g := GuidToString(gu);
    SetFilter(FormFilterLite.conditions);
    fl := TStringList.Create;
    fl.AddStrings(FormFilterLite.meNumbers.Lines);
    CheckMissingFilterValues(fl,'pnum');
  end;
end;

procedure TFormArchiveFeeds.btResetClick(Sender: TObject);
begin
  meDates.Close;
  meData.Close;
  meDates.Open;
  meData.Open;
end;

procedure TFormArchiveFeeds.dgDataDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

  if Column.FieldName = 'isinarchive' then
  begin

    if meData.FieldByName('isinarchive').AsInteger = 1 then
    begin
      dgData.Canvas.Brush.Color := $00D6F1C0;
    end;

  end;

  inherited;

end;

procedure TFormArchiveFeeds.Init;
begin
  meDates.Close;
  meData.Close;
  if dtStart.Value = null then dtStart.Value := IncMonth(now(),-3);
  meDates.Open;
  meData.Open;
end;

procedure TFormArchiveFeeds.meDataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  drvData.SelectCommand.Parameters.ParamByName('dateincome').Value := meDates.FieldByName('date_income').Value;
  if Assigned(drvData.SelectCommand.Parameters.FindParam('guid')) then drvData.SelectCommand.Parameters.ParamByName('guid').Value := g;
end;

procedure TFormArchiveFeeds.meDatesAfterOpen(DataSet: TDataSet);
begin
  inherited;
  drvData.GetRecCommand.Parameters.ParamByName('dateincome').Value := meDates.FieldByName('date_income').Value;
end;

procedure TFormArchiveFeeds.meDatesAfterScroll(DataSet: TDataSet);
begin
  meData.Close;
  meData.Open;
  drvData.GetRecCommand.Parameters.ParamByName('dateincome').Value := meDates.FieldByName('date_income').Value;
end;

procedure TFormArchiveFeeds.meDatesBeforeOpen(DataSet: TDataSet);
begin
  drvDates.SelectCommand.Parameters.ParamByName('datestart').Value := dtStart.Value;
  if Assigned(drvDates.SelectCommand.Parameters.FindParam('guid')) then drvDates.SelectCommand.Parameters.ParamByName('guid').Value := g;
end;


procedure TFormArchiveFeeds.N5Click(Sender: TObject);
begin
  drvDates.GetRecCommand.Parameters.ParamByName('dateincome').Value := meDates.FieldByName('date_income').Value;
  meDates.RefreshRecord;
  inherited;
end;

procedure TFormArchiveFeeds.N8Click(Sender: TObject);
begin
  if fQYN('Действительно хотите включить отмеченные записи в архив подачи?') then
  begin


    CreateGUID(gu);
    g:= GuidToString(gu);

    FillSelectListAll(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('insert into archivefeed (carriage_id, date_income, car_paper_number, train_num)');
    qrAux.SQL.Add('select id, date_income, car_paper_number, train_num from carriages car where id in ');
    qrAux.SQL.Add('(select id from selectlist where guid = '''+g+''')');
    qrAux.SQL.Add('and not exists (select 1 from archivefeed a where a.carriage_id = car.id and a.date_income = car.date_income)');

    qrAux.ExecSQL;

    meData.Close;
    meData.Open;

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    drvDates.GetRecCommand.Parameters.ParamByName('dateincome').Value := meDates.FieldByName('date_income').Value;
    meDates.RefreshRecord;

  end;
end;

procedure TFormArchiveFeeds.N9Click(Sender: TObject);
begin

  if fQYN('Действительно хотите исключить отмеченные записи из архива подачи?') then
  begin


    CreateGUID(gu);
    g:= GuidToString(gu);

    FillSelectListAll(g, 'archive_id');

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from archivefeed where id in ');
    qrAux.SQL.Add('(select id from selectlist where guid = '''+g+''')');

    qrAux.ExecSQL;

    meData.Close;
    meData.Open;

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    drvDates.GetRecCommand.Parameters.ParamByName('dateincome').Value := meDates.FieldByName('date_income').Value;
    meDates.RefreshRecord;

  end;

end;

procedure TFormArchiveFeeds.sbClearFilterClick(Sender: TObject);
begin
  SetFilter('');
end;

procedure TFormArchiveFeeds.SetFilter(conditions: string);
begin

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := conditions;
    dm.spCreateFilter.ExecProc;

    meDates.Close;
    meDates.Open;

    meData.Close;
    meData.Open;

    if conditions = '' then
      plStatus.Caption := ' Фильтр снят.'
    else
    begin
      plStatus.Caption := ' Установлен фильтр по списку номеров.';
    end;

end;


procedure TFormArchiveFeeds.AssignSelField(colname: string);
begin
  selfield := '';
  if colname = 'date_income' then selfield := 'date_income';
  if colname = 'car_paper_number' then selfield := 'car_paper_number';
  if colname = 'train_num' then selfield := 'train_num';
end;


end.
