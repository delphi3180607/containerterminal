﻿unit editcarriagekinds;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBCtrlsEh, Vcl.StdCtrls, Vcl.Mask,
  Vcl.ExtCtrls, Data.DB, Data.Win.ADODB;

type
  TFormEditCarriageKind = class(TFormEdit)
    Label2: TLabel;
    Label3: TLabel;
    edCode: TDBEditEh;
    edName: TDBEditEh;
    Label1: TLabel;
    neFootSize: TDBNumberEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditCarriageKind: TFormEditCarriageKind;

implementation

{$R *.dfm}

end.
