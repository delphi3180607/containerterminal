﻿inherited FormSelectObjectType: TFormSelectObjectType
  ActiveControl = dbData
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1090#1080#1087' '#1086#1073#1098#1077#1082#1090#1072
  ClientHeight = 300
  ClientWidth = 417
  ExplicitWidth = 423
  ExplicitHeight = 328
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 259
    Width = 417
    inherited btnCancel: TButton
      Left = 301
    end
    inherited btnOk: TButton
      Left = 182
    end
  end
  object dbData: TDBGridEh [1]
    Left = 0
    Top = 0
    Width = 417
    Height = 259
    Align = alClient
    AutoFitColWidths = True
    DataSource = dsData
    DynProps = <>
    IndicatorOptions = []
    Options = [dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    Columns = <
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'Name'
        Footers = <>
        TextEditing = False
        Title.Caption = #1044#1086#1089#1090#1091#1087#1085#1099#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
        Width = 459
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited qrAux: TADOQuery
    SQL.Strings = (
      'select * from objecttypes order by id')
  end
  object dsData: TDataSource
    DataSet = qrAux
    Left = 176
    Top = 144
  end
end
