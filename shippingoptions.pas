﻿unit ShippingOptions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton;

type
  TFormShippingOptions = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormShippingOptions: TFormShippingOptions;

implementation

{$R *.dfm}

uses editcodename;

procedure TFormShippingOptions.Init;
begin
  inherited;
  self.formEdit := FormEditCodeName;
end;


end.
