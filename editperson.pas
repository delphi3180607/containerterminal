﻿unit editperson;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  DBCtrlsEh, Vcl.Mask, Data.DB, Data.Win.ADODB;

type
  TFormEditPerson = class(TFormEdit)
    Label1: TLabel;
    cbKind: TDBComboBoxEh;
    Label2: TLabel;
    edFieldName: TDBEditEh;
    cbIsBlocked: TDBCheckBoxEh;
    Label18: TLabel;
    edCarData: TDBEditEh;
    edCarNumber: TDBEditEh;
    Label27: TLabel;
    edAttorney: TDBEditEh;
    Label25: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditPerson: TFormEditPerson;

implementation

{$R *.dfm}

end.
