﻿inherited FormEditCodeName: TFormEditCodeName
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
  ClientHeight = 174
  ClientWidth = 655
  ExplicitWidth = 661
  ExplicitHeight = 202
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 8
    Top = 8
    Width = 24
    Height = 16
    Caption = #1050#1086#1076
  end
  object Label2: TLabel [1]
    Left = 8
    Top = 64
    Width = 98
    Height = 16
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  inherited plBottom: TPanel
    Top = 133
    Width = 655
    TabOrder = 2
    ExplicitTop = 133
    ExplicitWidth = 352
    inherited btnCancel: TButton
      Left = 539
      ExplicitLeft = 236
    end
    inherited btnOk: TButton
      Left = 420
      ExplicitLeft = 117
    end
  end
  object edCode: TDBEditEh [3]
    Left = 8
    Top = 30
    Width = 241
    Height = 24
    DataField = 'code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edName: TDBEditEh [4]
    Left = 8
    Top = 86
    Width = 625
    Height = 24
    DataField = 'name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 120
    Top = 24
  end
  inherited qrAux: TADOQuery
    Left = 120
    Top = 88
  end
end
