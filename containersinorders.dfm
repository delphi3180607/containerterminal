﻿inherited FormContainersInOrders: TFormContainersInOrders
  Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088#1099' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1077#1085#1085#1086' '#1074' '#1079#1072#1074#1103#1082#1072#1093
  ClientWidth = 917
  ExplicitWidth = 933
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Width = 917
    ExplicitWidth = 917
    inherited btnOk: TButton
      Left = 682
      Default = True
      Font.Style = []
      ExplicitLeft = 682
    end
    inherited btnCancel: TButton
      Left = 801
      ExplicitLeft = 801
    end
  end
  inherited plAll: TPanel
    Width = 917
    ExplicitWidth = 917
    inherited plTop: TPanel
      Width = 917
      ExplicitWidth = 917
      inherited btTool: TPngSpeedButton
        Left = 879
        ExplicitLeft = 865
      end
      inherited plCount: TPanel
        Left = 682
        ExplicitLeft = 682
      end
    end
    inherited dgData: TDBGridEh
      Width = 917
      AllowedOperations = []
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 70
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 67
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1082#1086#1085#1090'.'
          Width = 57
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          Title.Caption = #1042#1083#1072#1076#1077#1083#1077#1094
          Width = 118
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'amount'
          Footers = <>
          Title.Caption = #1050#1086#1083'-'#1074#1086
          Width = 67
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'rest_amount'
          Footers = <>
          Title.Caption = #1054#1089#1090#1072#1090#1086#1082
          Width = 67
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'station_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1085#1094#1080#1103' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1103
          Width = 152
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 152
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077' '#1082' '#1079#1072#1103#1074#1082#1077
          Width = 545
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'select  s.id+1-1 as id, o.*,  d.doc_number, d.doc_date,  s.conta' +
        'iner_kind_id, s.container_owner_id,'
      
        '(select code from counteragents cr where cr.id = s.container_own' +
        'er_id) as owner_code,'
      
        '(select code from containerkinds k where k.id  = s.container_kin' +
        'd_id) as kind_code,'
      
        '(select name from stations st where st.id = o.station_id) as sta' +
        'tion_code,'
      
        '(select code from counteragents c where c.id = o.consignee_id) a' +
        's consignee_code,'
      
        '(select code from counteragents c1 where c1.id = o.payer_id) as ' +
        'payer_code,'
      'o.note as task_note,'
      
        'isnull(s.amount,0) as amount, isnull(s.amount,0)-isnull(s.used_a' +
        'mount,0) as rest_amount'
      'from docorder o, docorderspec s, documents d'
      'where s.doc_id = o.id and d.id = o.id'
      'and isnull(s.container_id,0)=0'
      '--and isnull(s.amount,0)-isnull(s.used_amount,0)>0'
      'and d.isconfirmed = 1'
      'and isnull(d.isabolished,0) = 0'
      'order by d.doc_date')
  end
end
