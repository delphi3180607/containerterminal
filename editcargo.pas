﻿unit editcargo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask, DBSQLLookUp, DBGridEh,
  Vcl.Buttons, DBLookupEh, Cargotypes, Functions;

type
  TFormEditCargo = class(TFormEdit)
    edCNum: TDBEditEh;
    Label2: TLabel;
    Label4: TLabel;
    edNote: TDBMemoEh;
    Label15: TLabel;
    sbCargoType: TSpeedButton;
    cbIsEmpty: TDBCheckBoxEh;
    Label5: TLabel;
    nuWeightSender: TDBNumberEditEh;
    Label6: TLabel;
    nuWeightDocument: TDBNumberEditEh;
    nuWeightFact: TDBNumberEditEh;
    Label7: TLabel;
    edSealNumber: TDBEditEh;
    Label1: TLabel;
    ssCargoType: TADOLookUpSqlSet;
    luCargoType: TDBSQLLookUp;
    cbSealWaste: TDBComboBoxEh;
    procedure cbIsEmptyClick(Sender: TObject);
    procedure sbCargoTypeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditCargo: TFormEditCargo;

implementation

{$R *.dfm}

procedure TFormEditCargo.cbIsEmptyClick(Sender: TObject);
begin
  if cbIsEmpty.Checked then self.luCargoType.Value := null;
  self.luCargoType.Enabled := not cbIsEmpty.Checked;
  self.sbCargoType.Enabled := not cbIsEmpty.Checked;
  self.luCargoType.Refresh;
end;

procedure TFormEditCargo.sbCargoTypeClick(Sender: TObject);
begin
  SFDE(TFormCargotypes,self.luCargoType);
end;

end.
