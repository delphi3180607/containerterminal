﻿inherited FormSQLQuery: TFormSQLQuery
  BorderStyle = bsSizeable
  Caption = 'SQL '#1079#1072#1087#1088#1086#1089
  ClientHeight = 481
  ClientWidth = 860
  ExplicitWidth = 876
  ExplicitHeight = 519
  PixelsPerInch = 96
  TextHeight = 16
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 273
    Width = 860
    Height = 3
    Cursor = crVSplit
    Align = alTop
    ExplicitLeft = 8
    ExplicitTop = 236
    ExplicitWidth = 870
  end
  inherited plBottom: TPanel
    Top = 440
    Width = 860
    inherited btnCancel: TButton
      Left = 744
    end
    inherited btnOk: TButton
      Left = 625
      Caption = #1047#1072#1082#1088#1099#1090#1100
    end
  end
  object se: TSynMemo [2]
    Left = 0
    Top = 32
    Width = 860
    Height = 241
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    TabOrder = 1
    Gutter.Font.Charset = DEFAULT_CHARSET
    Gutter.Font.Color = clWindowText
    Gutter.Font.Height = -11
    Gutter.Font.Name = 'Courier New'
    Gutter.Font.Style = []
    FontSmoothing = fsmNone
  end
  object dg: TDBGridEh [3]
    Left = 0
    Top = 276
    Width = 860
    Height = 164
    Align = alClient
    DataSource = dsLocal
    DynProps = <>
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 2
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object plTool: TPanel [4]
    Left = 0
    Top = 0
    Width = 860
    Height = 32
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 3
    ExplicitLeft = -3
    ExplicitTop = -20
    object btExec: TButton
      AlignWithMargins = True
      Left = 134
      Top = 4
      Width = 124
      Height = 24
      Align = alLeft
      Caption = #1044#1077#1081#1089#1090#1074#1080#1077
      TabOrder = 0
      OnClick = btExecClick
    end
    object btSelect: TButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 124
      Height = 24
      Align = alLeft
      Caption = #1042#1099#1073#1086#1088#1082#1072
      TabOrder = 1
      OnClick = btSelectClick
    end
  end
  inherited dsLocal: TDataSource
    DataSet = qrAux
    Left = 288
    Top = 448
  end
  inherited qrAux: TADOQuery
    Left = 344
    Top = 448
  end
end
