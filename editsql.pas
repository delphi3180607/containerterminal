﻿unit editsql;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, Vcl.ExtCtrls, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, Vcl.Buttons, PngSpeedButton, SynEditMiscClasses, SynEditSearch,
  SynEditHighlighter, SynHighlighterSQL, SynEdit, SynMemo, Data.DB,
  Data.Win.ADODB, Vcl.StdCtrls;

type
  TFormEditSql = class(TFormEdit)
    meSQL: TSynMemo;
    synSQL: TSynSQLSyn;
    synSearch: TSynEditSearch;
    dsTest: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditSql: TFormEditSql;

implementation

{$R *.dfm}

end.
