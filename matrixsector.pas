﻿unit matrixsector;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, Vcl.ExtCtrls, Vcl.Menus, Functions;

type TFakeDBGridEh = class(TDBGridEh);

type
  TFormMatrixSector = class(TForm)
    dgView: TDBGridEh;
    drvSector: TADODataDriverEh;
    meSector: TMemTableEh;
    dsSector: TDataSource;
    plProps: TPanel;
    edDlvType: TDBEditEh;
    edNotReady: TDBEditEh;
    edDefect: TDBEditEh;
    edShift: TDBEditEh;
    edWeightFact: TDBEditEh;
    edContainerWeight: TDBEditEh;
    edCarrying: TDBEditEh;
    edDateStriker: TDBEditEh;
    edDateMade: TDBEditEh;
    edOwner: TDBEditEh;
    drvContInfo: TADODataDriverEh;
    meContInfo: TMemTableEh;
    dsContInfo: TDataSource;
    edInPlaceStart: TDBEditEh;
    edDlvStart: TDBEditEh;
    spPlaceContainer: TADOStoredProc;
    pmGrid: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    edEmptyLoaded: TDBEditEh;
    edKind: TDBEditEh;
    edState: TDBEditEh;
    edNextState: TDBEditEh;
    edFormalState: TDBEditEh;
    N4: TMenuItem;
    N5: TMenuItem;
    edCargoType: TDBEditEh;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    spMatrixMark: TADOStoredProc;
    N11: TMenuItem;
    edCarData: TDBEditEh;
    procedure dgViewDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure N1Click(Sender: TObject);
    procedure meSectorAfterScroll(DataSet: TDataSet);
    procedure dgViewCellMouseClick(Grid: TCustomGridEh; Cell: TGridCoord;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer;
      var Processed: Boolean);
    procedure dgViewColumns0UpdateData(Sender: TObject; var Text: string;
      var Value: Variant; var UseText, Handled: Boolean);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure dgViewDblClick(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure dgViewAdvDrawDataCell(Sender: TCustomDBGridEh; Cell,
      AreaCell: TGridCoord; Column: TColumnEh; const ARect: TRect;
      var Params: TColCellParamsEh; var Processed: Boolean);
    procedure dgViewColEnter(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure dgViewKeyPress(Sender: TObject; var Key: Char);
  private
    isInited: boolean;

    moving_cnum: string;
    start_moving: boolean;
    hide_quest: boolean;

    nscale: integer;

    procedure BuildStructure(sectorid: integer);
    procedure ReplaceCellValue;
    procedure DisplayContInfo;
    procedure RefreshSector;

    procedure MarkForSelected(operation: integer; color: integer);
    procedure ClearMarkForSelected;

    procedure MarkOne(operation: integer; cnum: string; color: integer);

  public

    sectorid_stored: integer;
    sectorid_stored_name: string;
    rowOrder_stored: TSortOrderEh;

    function PlaceContainer(operation, containerid: integer; row_num, level_num, stack_num, cell_num: string): boolean;
    procedure AddContainer(operationtype: integer);
    function AddContainerById(containerid: integer): boolean;
    procedure DeleteContainer;
    function DeleteContainerById(containerid: integer): boolean;
    procedure Init(sectorid: integer; sector_name: string; isRowOrderReverse: boolean);
    procedure RestorePosition;
    procedure DecreaseScale;
    procedure IncreaseScale;
  end;

var
  FormMatrixSector: TFormMatrixSector;

implementation

{$R *.dfm}

uses SelectContainer, matrix;

function TFormMatrixSector.PlaceContainer(operation, containerid: integer; row_num, level_num, stack_num, cell_num: string): boolean;
begin

  result := true;

  spPlaceContainer.Parameters.ParamByName('@operation').Value := operation;
  spPlaceContainer.Parameters.ParamByName('@ContainerId').Value := containerid;
  spPlaceContainer.Parameters.ParamByName('@SectorId').Value := sectorid_stored;
  spPlaceContainer.Parameters.ParamByName('@RowNum').Value := StrToInt(row_num);
  spPlaceContainer.Parameters.ParamByName('@StackNum').Value := StrToInt(stack_num);
  spPlaceContainer.Parameters.ParamByName('@LevelNum').Value := StrToInt(level_num);
  spPlaceContainer.Parameters.ParamByName('@CellNum').Value := StrToInt(cell_num);

  try

    spPlaceContainer.ExecProc;

    drvSector.GetrecCommand.Parameters.ParamByName('sectorid').Value := sectorid_stored;
    drvSector.GetrecCommand.Parameters.ParamByName('row_num').Value := StrToInt(row_num);
    drvSector.GetrecCommand.Parameters.ParamByName('level_num').Value := StrToInt(level_num);
    meSector.RefreshRecord;

    DisplayContInfo;

  except
    on E: Exception do
    begin
      Screen.Cursor := crDefault;
      ShowMessage(E.Message);
      result := false;
    end;
  end;

end;


procedure TFormMatrixSector.AddContainer(operationtype: integer);
var fn, c1, c2: string; containerid: integer; b: TBookMark;
begin

  fn := dgView.Columns[dgView.Col-1].FieldName;

  if (fn<>'row_num') and (fn<>'level_num') and (copy(fn,1,1)<>'w') then
  begin

    c2:= copy(fn,2);
    c1 := copy(c2,1, pos('_',c2)-1);
    c2 := copy(c2,pos('_',c2)+1);

    if Pos('~-', meSector.FieldByName(fn).AsString) >0 then
    begin
      ShowMessage('На закрытые области ставить нельзя.');
      exit;
    end;

    if Length(meSector.FieldByName(fn).AsString)>20 then
    begin
      ShowMessage('Ячейка занята.');
      exit;
    end;

    if not start_moving then
    begin
      FormSelectContainer.lbExplain.Caption :=
        'Участок: '+sectorid_stored_name+
        ', секция: '+c1+
        ', ряд: '+meSector.FieldByName('row_num').AsString+
        ', ячейка: '+c2;
      FormSelectContainer.lbMark.Caption := '';
      FormSelectContainer.edContainerNum.Text := '';
      FormSelectContainer.ShowModal;
    end else
    begin
      FormSelectContainer.qrAux.Close;
      FormSelectContainer.edContainerNum.Text := moving_cnum;
      FormSelectContainer.t1.OnTimer(nil);
    end;

    if (start_moving) or (FormSelectContainer.ModalResult = mrOk) then
    begin

      if (not hide_quest) and (start_moving) then
      begin

        if not fQYN('Переместить контейнер '+moving_cnum+' в выбранную ячейку?') then
        begin

          hide_quest := false;
          start_moving := false;
          N5.Enabled := true;
          N11.Enabled := false;

          if moving_cnum <> '' then
          begin
            FormMatrix.FindContainer(trim(moving_cnum));
            drvSector.GetrecCommand.Parameters.ParamByName('sectorid').Value := sectorid_stored;
            drvSector.GetrecCommand.Parameters.ParamByName('row_num').Value := StrToInt(meSector.FieldByName('row_num').AsString);
            drvSector.GetrecCommand.Parameters.ParamByName('level_num').Value := StrToInt(meSector.FieldByName('level_num').AsString);
            meSector.RefreshRecord;
          end;
          moving_cnum := '';
          Screen.Cursor := crDefault;
          exit;
        end;

      end;

      containerid := FormSelectContainer.qrAux.FieldByName('id').AsInteger;

      if PlaceContainer(
      operationtype,
      containerid,
      meSector.FieldByName('row_num').AsString, meSector.FieldByName('level_num').AsString, c1, c2
      ) then
      begin

          if start_moving then
          begin
            RestorePosition;
          end;

          start_moving := false;
          N5.Enabled := true;
          N11.Enabled := false;
          moving_cnum := '';
          Screen.Cursor := crDefault;

      end else
      begin

          if start_moving then
          begin
            Screen.Cursor := crDrag;
          end;

      end;


    end;

    dgView.EditorMode := false;

  end;

  hide_quest := false;

end;


procedure TFormMatrixSector.RestorePosition;
var r, r1, c, OldRow, OldCol: integer;
begin
  r := dgView.Row;
  c := dgView.Col;
  OldRow := TFakeDBGridEh(dgView).TopRow;
  OldCol := TFakeDBGridEh(dgView).LeftCol;
  r1 := meSector.RecNo;
  RefreshSector;
  dgView.Row := r;
  dgView.Col := c;
  TFakeDBGridEh(dgView).TopRow := OldRow;
  TFakeDBGridEh(dgView).LeftCol := OldCol;
  meSector.RecNo := r1;
  DisplayContInfo;
end;

function TFormMatrixSector.AddContainerById(containerid: integer): boolean;
var fn, c1, c2: string;
begin

  result := true;

  fn := dgView.Columns[dgView.Col-1].FieldName;

  if (fn<>'row_num') and (fn<>'level_num') and (copy(fn,1,1)<>'w') then
  begin

    if Pos('~-', meSector.FieldByName(fn).AsString) >0 then
    begin
      ShowMessage('На закрытые области ставить нельзя.');
      result := false;
      exit;
    end;

    if Length(meSector.FieldByName(fn).AsString)>20 then
    begin
      ShowMessage('Ячейка занята.');
      result := false;
      exit;
    end;

    c2:= copy(fn,2);
    c1 := copy(c2,1, pos('_',c2)-1);
    c2 := copy(c2,pos('_',c2)+1);

    result := PlaceContainer(
    0,
    containerid,
    meSector.FieldByName('row_num').AsString, meSector.FieldByName('level_num').AsString, c1, c2
    );

  end else
  begin

    ShowMessage('Необходимо установить курсор на ячейку хранения.');
    result := false;

  end;

end;


procedure TFormMatrixSector.DeleteContainer;
var fn, c1, c2: string;
begin

  fn := dgView.Columns[dgView.Col-1].FieldName;

  if (fn<>'row_num') and (fn<>'level_num') and (copy(fn,1,1)<>'w') then
  begin

    if fQYN('Удалить контейнер из матрицы?') then
    begin

      c2:= copy(fn,2);
      c1 := copy(c2,1, pos('_',c2)-1);
      c2 := copy(c2,pos('_',c2)+1);

      PlaceContainer(
      1,
      0,
      meSector.FieldByName('row_num').AsString, meSector.FieldByName('level_num').AsString, c1, c2
      );


    end;

  end;

end;


function TFormMatrixSector.DeleteContainerById(containerid: integer): boolean;
var fn, c1, c2: string;
begin

    result := 
    PlaceContainer(
    1,
    containerid,
    '0', '0', '0', '0'
    );

end;

procedure TFormMatrixSector.dgViewAdvDrawDataCell(Sender: TCustomDBGridEh; Cell,
  AreaCell: TGridCoord; Column: TColumnEh; const ARect: TRect;
  var Params: TColCellParamsEh; var Processed: Boolean);

var Rect: TRect;
begin

  Rect := ARect;

  if (Params.Row = dgView.Row) then
  begin

    {if (Column.Index = dgView.Col-1)then
    begin
      dgView.Canvas.Brush.Style := bsSolid;
      dgView.Canvas.Brush.Color := clWhite;
      dgView.Canvas.Pen.Color := clBlue;
      dgView.Canvas.Pen.Width := 4;
      dgView.Canvas.Font.Style := Column.Font.Style;
      dgView.Canvas.Rectangle(Rect.Left+2, Rect.Top+2, Rect.Right, Rect.Bottom);
    end;}

    dgView.Canvas.Brush.Style := bsSolid;
    dgView.Canvas.Font.Style := Column.Font.Style;
    dgView.Canvas.Pen.Color := clBlue;
    dgView.Canvas.Pen.Width := 1;
    dgView.Canvas.MoveTo(Rect.Left, Rect.Bottom);
    dgView.Canvas.LineTo(Rect.Right, Rect.Bottom);

  end;

  if (Column.Index = dgView.Col-1) then
  begin

    dgView.Canvas.Brush.Style := bsSolid;
    dgView.Canvas.Font.Style := Column.Font.Style;
    dgView.Canvas.Pen.Color := clBlue;
    dgView.Canvas.Pen.Width := 1;
    dgView.Canvas.MoveTo(Rect.Right, Rect.Top);
    dgView.Canvas.LineTo(Rect.Right, Rect.Bottom);

  end;

  dgView.Canvas.Pen.Width := 1;

end;

procedure TFormMatrixSector.dgViewCellMouseClick(Grid: TCustomGridEh;
  Cell: TGridCoord; Button: TMouseButton; Shift: TShiftState; X, Y: Integer;
  var Processed: Boolean);
begin
  ReplaceCellValue;
end;

procedure TFormMatrixSector.DisplayContInfo;
var fn, text: string; maininfo: array of string;
begin

  fn := dgView.Columns[dgView.Col-1].FieldName;

  if copy(fn,1,1)='s' then
  begin
    text := meSector.FieldByName(fn).AsString;
    text := trim(copy(text, 1, pos('~', text)-1));

    maininfo := Split(text, '|');

    meContInfo.Close;

    if Length(maininfo)>1 then
    begin
      drvContInfo.SelectCommand.Parameters.ParamByName('cnum').Value := maininfo[1];
      meContInfo.Open;
    end;

  end else
  begin
    meContInfo.Close;
  end;

end;


procedure TFormMatrixSector.dgViewColEnter(Sender: TObject);
begin
  DisplayContInfo;
end;

procedure TFormMatrixSector.dgViewColumns0UpdateData(Sender: TObject;
  var Text: string; var Value: Variant; var UseText, Handled: Boolean);
begin
  ReplaceCellValue;
end;

procedure TFormMatrixSector.dgViewDblClick(Sender: TObject);
begin

  if start_moving then
  begin
    hide_quest := false;
    AddContainer(2);
  end else
  begin
    AddContainer(0);
  end;

end;

procedure TFormMatrixSector.dgViewDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
var i: integer; fn, text, mark, defect, notready: string;
textRect1, textRect, cellRect: TRect; pnum, s: string; orderspecid: integer;
var marks, maininfo: array of string;
begin

  fn := Column.FieldName;
  textRect := System.Classes.rect(Rect.Left+2, Rect.Top+2, Rect.Right-1, Rect.Bottom-1);
  dgView.Canvas.Font.Style := Column.Font.Style;

  if copy(fn,1,1)='w'  then
  begin

    dgView.Canvas.Brush.Color := $00888888;
    dgView.Canvas.Brush.Style := bsSolid;
    dgView.Canvas.Pen.Color := clGray;
    //dgView.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    dgView.Canvas.Rectangle(textRect.Left-1, textRect.Top, textRect.Right+1, textRect.Bottom);
    dgView.Canvas.Brush.Style := bsClear;
    exit;

  end;


  if fn='row_num'  then
  begin

    i:= meSector.RecNo-1;

    pnum := meSector.FieldByName('row_num').AsString;

    if (i < meSector.RecordsView.Count) and (i >= 0 ) then
    begin

      if (i=0) or // мы на верхней ячейке
      (meSector.RecordsView.Rec[i-1].DataValues['row_num', TDataValueVersionEh.dvvCurValueEh] <> pnum) then
      begin
        dgView.DefaultDrawColumnCell(Rect, DataCol, Column, State);
      end;

    end;

    exit;

  end;

  if fn='level_num' then
  begin
    dgView.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    exit;
  end;

  if copy(fn,1,1)='s' then
  begin
    // особое отображение
    text := meSector.FieldByName(fn).AsString;
    mark := copy(text, pos('~', text)+1);
    marks := Split(mark, '|');
    text := copy(text, 1, pos('~', text)-1);
    maininfo := Split(text, '|');

    dgView.Canvas.Brush.Style := bsSolid;
    dgView.Canvas.Pen.Style := psClear;
    dgView.Canvas.Brush.Color := clWhite;

    defect := '';
    notready := '';

      textRect := System.Classes.rect(Rect.Left+18, Rect.Top+2, Rect.Right-1, Rect.Bottom-1);

      if marks[0]='-' then
      begin
        dgView.Canvas.Brush.Color := $00EEEEEE;
        dgView.Canvas.Rectangle(textRect.Left-1, textRect.Top-1, textRect.Right+1, textRect.Bottom+1);
      end;

      if Length(maininfo)>1 then
      begin

        defect := marks[5];
        notready := marks[6];

        if marks[1]='0' then
        begin
          dgView.Canvas.Font.Style := [fsItalic];
        end else
        begin
          dgView.Canvas.Font.Style := [fsBold];
        end;

        // Есть заявка на отправку
        if (marks[4]='2') then
        begin
          dgView.Canvas.Brush.Style := bsSolid;
          dgView.Canvas.Brush.Color := $00C1F0FF;
        end;

        if (marks[3]='>') then
        begin
          dgView.Canvas.Brush.Style := bsSolid;
          dgView.Canvas.Brush.Color := clAqua;
        end;

        if (marks[5]='Е') then
        begin
          dgView.Canvas.Brush.Style := bsSolid;
          dgView.Canvas.Brush.Color := clBlue;
          dgView.Canvas.Font.Color := clWhite;
        end;

        if (marks[2]='П') or (marks[2]='^') or (marks[3]='<') then
        begin
          dgView.Canvas.Brush.Style := bsSolid;
          dgView.Canvas.Brush.Color := clYellow;
        end;

        if (marks[4]='1') then
        begin
          dgView.Canvas.Brush.Style := bsSolid;
          dgView.Canvas.Brush.Color := clLime;
        end;

        if (marks[2]='>') or (marks[2]='<') then
        begin
          dgView.Canvas.Brush.Style := bsSolid;
          dgView.Canvas.Brush.Color := clRed;
          dgView.Canvas.Font.Color := clWhite;
        end;

        if maininfo[1] = moving_cnum then
        begin
          dgView.Canvas.Brush.Style := bsSolid;
          dgView.Canvas.Brush.Color := clSkyBlue;
          dgView.Canvas.Font.Color := clBlack;
          dgView.Canvas.Pen.Style := psDot;
          dgView.Canvas.Pen.Color := clBlue;
          dgView.Canvas.Pen.Width := 1;
          dgView.Canvas.Rectangle(textRect.Left, textRect.Top, textRect.Right, textRect.Bottom+1);
        end else
        begin

          if marks[10]='' then
          begin
            dgView.Canvas.Rectangle(textRect.Left, textRect.Top, textRect.Right, textRect.Bottom+1);
          end else
          begin

            dgView.Canvas.Brush.Style := bsClear;
            dgView.Canvas.Pen.Style := psSolid;
            dgView.Canvas.Pen.Width := 3;

            if (marks[2]='>') or (marks[2]='<') then
            begin
              dgView.Canvas.Brush.Style := bsSolid;
              dgView.Canvas.Brush.Color := clRed;
              dgView.Canvas.Font.Color := clWhite;
              dgView.Canvas.Font.Style := [];
            end else
            begin
              dgView.Canvas.Font.Color := clBlack;
            end;

            if marks[10]='1' then
              dgView.Canvas.Pen.Color := clBlue
            else
              dgView.Canvas.Pen.Color := clMaroon;

            dgView.Canvas.Rectangle(textRect.Left-2, textRect.Top-2, textRect.Right+1, textRect.Bottom);

          end;

        end;

        s := maininfo[1]+'-'+maininfo[2];

        dgView.Canvas.TextRect(textRect, s);

        if maininfo[0]<>'' then
        begin
          textRect1 := System.Classes.rect(Rect.Left+10, Rect.Top+2, Rect.Left+17, Rect.Bottom-1);
          dgView.Canvas.Brush.Color := clBlack;
          dgView.Canvas.Rectangle(textRect1.Left-1, textRect1.Top-1, textRect1.Right+1, textRect1.Bottom+1);
          dgView.Canvas.Font.Color := clWhite;
          dgView.Canvas.TextRect(textRect1, maininfo[0]);
          dgView.Canvas.Font.Color := clBlack;
        end;

        dgView.Canvas.Brush.Style := bsClear;
        dgView.Canvas.Pen.Style := psSolid;
        dgView.Canvas.Pen.Width := 1;

        if defect = '1' then
        begin
          dgView.Canvas.Brush.Color := clRed;
          dgView.Canvas.Brush.Style := bsSolid;
          dgView.Canvas.Pen.Color := clRed;
          dgView.Canvas.Pen.Style := psSolid;
          dgView.Canvas.Rectangle(Rect.Left+1, Rect.Top+2, Rect.Left+7, Rect.Bottom-1);
        end;

        if notready = '1' then
        begin
          dgView.Canvas.Brush.Color := $000080FF;
          dgView.Canvas.Brush.Style := bsSolid;
          dgView.Canvas.Pen.Color := $000080FF;
          dgView.Canvas.Pen.Style := psSolid;
          dgView.Canvas.Rectangle(Rect.Left+1, Rect.Top+2, Rect.Left+7, Rect.Bottom-1);
        end;

     end else
     begin
          dgView.Canvas.Pen.Color := $00E2E2E2;
          dgView.Canvas.Pen.Style := psSolid;
          dgView.Canvas.Rectangle(Rect.Left+2, Rect.Top+2, Rect.Right-2, Rect.Bottom-2);
     end;

     dgView.Canvas.Brush.Style := bsClear;
     dgView.Canvas.Pen.Style := psClear;
     dgView.Canvas.Rectangle(Rect.Left+8, Rect.Top+2, Rect.Left+19, Rect.Bottom-1);
     dgView.Canvas.Pen.Style := psSolid;

  end else
  begin
      text := meSector.FieldByName(fn).AsString;
      dgView.Canvas.TextRect(textRect, text);
  end;

end;

procedure TFormMatrixSector.dgViewKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #27 then
  begin
    start_moving := false;
    N5.Enabled := true;
    N11.Enabled := false;
    hide_quest := false;
    if moving_cnum <> '' then
    begin
      FormMatrix.FindContainer(trim(moving_cnum));
      drvSector.GetrecCommand.Parameters.ParamByName('sectorid').Value := sectorid_stored;
      drvSector.GetrecCommand.Parameters.ParamByName('row_num').Value := StrToInt(meSector.FieldByName('row_num').AsString);
      drvSector.GetrecCommand.Parameters.ParamByName('level_num').Value := StrToInt(meSector.FieldByName('level_num').AsString);
      meSector.RefreshRecord;
    end;
    moving_cnum := '';
    Screen.Cursor := crDefault;
  end;
end;

procedure TFormMatrixSector.Init(sectorid: integer; sector_name: string; isRowOrderReverse: boolean);
begin

  nscale := 20;

  hide_quest := false;
  start_moving := false;
  N5.Enabled := true;
  N11.Enabled := false;
  moving_cnum := '';
  Screen.Cursor := crDefault;

  if sectorid>=0 then
  begin
    sectorid_stored := sectorid;
    sectorid_stored_name := sector_name;
    if isRowOrderReverse then
      rowOrder_stored := soDescEh
    else
     rowOrder_stored := soAscEh;
  end;

  if not self.isInited then
  begin
    meSector.Close;
    drvSector.SelectCommand.Parameters.ParamByName('sectorid').Value := sectorid_stored;
    meSector.Open;
    BuildStructure(sectorid_stored);
    isInited := true;
  end;

  hide_quest := false;

end;


procedure TFormMatrixSector.meSectorAfterScroll(DataSet: TDataSet);
begin
  DisplayContInfo;
end;

procedure TFormMatrixSector.N10Click(Sender: TObject);
begin
  ClearMarkForSelected;
end;

procedure TFormMatrixSector.N11Click(Sender: TObject);
begin
 hide_quest := true;
 AddContainer(2);
 hide_quest := false;
end;

procedure TFormMatrixSector.N1Click(Sender: TObject);
begin
  RestorePosition;
end;

procedure TFormMatrixSector.RefreshSector;
begin
  Screen.Cursor := crHourGlass;
  meSector.Close;
  drvSector.SelectCommand.Parameters.ParamByName('sectorid').Value := sectorid_stored;
  meSector.Open;
  BuildStructure(sectorid_stored);
  Screen.Cursor := crDefault;
end;


procedure TFormMatrixSector.N2Click(Sender: TObject);
begin
  AddContainer(0);
end;

procedure TFormMatrixSector.N3Click(Sender: TObject);
begin
  DeleteContainer;
end;

procedure TFormMatrixSector.N5Click(Sender: TObject);
var fn, text: string; maininfo: array of string;
begin

  moving_cnum := '';
  start_moving := false;
  N5.Enabled := true;
  N11.Enabled := false;

  fn := dgView.Columns[dgView.Col-1].FieldName;

  if copy(fn,1,1)='s' then
  begin

    text := meSector.FieldByName(fn).AsString;
    text := trim(copy(text, 1, pos('~', text)-1));

    maininfo := Split(text, '|');

    if Length(maininfo)>1 then
    begin
      moving_cnum := maininfo[1];
      start_moving := true;
      N5.Enabled := false;
      N11.Enabled := true;
      Screen.Cursor := crDrag;

      drvSector.GetrecCommand.Parameters.ParamByName('sectorid').Value := sectorid_stored;
      drvSector.GetrecCommand.Parameters.ParamByName('row_num').Value := StrToInt(meSector.FieldByName('row_num').AsString);
      drvSector.GetrecCommand.Parameters.ParamByName('level_num').Value := StrToInt(meSector.FieldByName('level_num').AsString);
      meSector.RefreshRecord;

    end;

  end;

end;

procedure TFormMatrixSector.N8Click(Sender: TObject);
begin
  MarkForSelected(1, 1)
end;

procedure TFormMatrixSector.N9Click(Sender: TObject);
begin
  MarkForSelected(1, 2);
end;

procedure TFormMatrixSector.BuildStructure(sectorid: integer);
var i: integer; fn, c1, c2: string; dg: TGridDataGroupLevelEh;

function lscale(value: integer): integer;
begin
  result := value - trunc(value*(20-self.nscale)/20);
end;

begin

  dgView.Columns.Clear;
  dgView.DataGrouping.GroupLevels.Clear;

  for i := 0 to dgView.Columns.Count-1 do
  begin

    fn := dgView.Columns[i].FieldName;

    if fn='row_num'  then
    begin
      dgView.Columns[i].MaxWidth := lscale(22);
      dgView.Columns[i].Width := lscale(22);
      dgView.Columns[i].Title.Caption := 'Ряд';
      dgView.Columns[i].HideDuplicates := true;
      dgView.Columns[i].Alignment := taCenter;
      dg := dgView.DataGrouping.GroupLevels.Add;
      dg.SortOrder := rowOrder_stored;
      dg.Column := dgView.Columns[i];
      dg.ExpandNodes;
      dg.RowHeight := lscale(18);
      dgView.Columns[i].Font.Style := [fsBold];
      dgView.Columns[i].Font.Size := lscale(12);
    end;

    if fn='level_num'  then
    begin
      dgView.Columns[i].Width := lscale(18);
      dgView.Columns[i].MaxWidth := lscale(18);
      dgView.Columns[i].Title.Caption := 'Ур.';
      dgView.Columns[i].AutoFitColWidth := false;
      dgView.Columns[i].Alignment := taCenter;
      dgView.Columns[i].Font.Style := [fsBold];
      dgView.Columns[i].Color := $00DDDDDD;
    end;

    if copy(fn,1,1)='s'  then
    begin
      dgView.Columns[i].Width := lscale(95);
      c2:= copy(fn,2);
      c1 := copy(c2,1, pos('_',c2)-1);
      c2 := copy(c2,pos('_',c2)+1);
      dgView.Columns[i].Title.Caption := 'Секция '+c1+'|'+c2;
      dgView.Columns[i].OnUpdateData := dgViewColumns0UpdateData;
    end;

    if copy(fn,1,1)='w'  then
    begin
      dgView.Columns[i].Width := 2;
      dgView.Columns[i].MaxWidth := 2;
      dgView.Columns[i].MinWidth := 2;
      dgView.Columns[i].Title.Caption := ' ';
      dgView.Columns[i].Title.Color := clBlack;
    end;

  end;

  self.Font.Size := lscale(7);

  dgView.FrozenCols := 2;

end;

procedure TFormMatrixSector.ReplaceCellValue;
var fn, text: string;
begin

  if dgView.EditorMode then
  begin

      fn := dgView.Columns[dgView.Col-1].FieldName;

      if copy(fn,1,1)='s' then
      begin
        text := meSector.FieldByName(fn).AsString;
        text := copy(text, 1, pos('~', text)-1);
        dgView.InplaceEditor.Text := text;
      end;

  end;

end;


procedure TFormMatrixSector.MarkForSelected(operation: integer; color: integer);

  procedure MarkCellsInRow;
  var i: integer; fn, text: string; maininfo: array of string;
  begin

      for i := dgView.Selection.Rect.LeftCol to dgView.Selection.Rect.RightCol do
      begin

          fn := dgView.Columns[i].FieldName;

          if copy(fn,1,1)='s' then
          begin

            text := meSector.FieldByName(fn).AsString;
            text := trim(copy(text, 1, pos('~', text)-1));

            maininfo := Split(text, '|');

            if Length(maininfo)>1 then
            begin
              MarkOne(operation, maininfo[1], color);
            end;

          end;

      end;

  end;


  procedure MarkCell;
  var fn, text: string; maininfo: array of string;
  begin

    fn := dgView.Columns[dgView.Col-1].FieldName;

    if copy(fn,1,1)='s' then
    begin

      text := meSector.FieldByName(fn).AsString;
      text := trim(copy(text, 1, pos('~', text)-1));

      maininfo := Split(text, '|');

      if Length(maininfo)>1 then
      begin
        MarkOne(operation, maininfo[1], color);
      end;

    end;


  end;

begin

  if dgView.Selection.SelectionType = (gstRectangle) then
  begin

    meSector.DisableControls;
    meSector.Bookmark := dgView.Selection.Rect.TopRow;
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;

    while true do
    begin

      MarkCellsInRow;

      if meSector.CompareBookmarks(meSector.Bookmark , dgView.Selection.Rect.BottomRow) = 0
      then break;

      meSector.Next;

    end;

    meSector.EnableControls;
    RestorePosition;

  end else
  begin
    MarkCell;
    drvSector.GetrecCommand.Parameters.ParamByName('sectorid').Value := sectorid_stored;
    drvSector.GetrecCommand.Parameters.ParamByName('row_num').Value := StrToInt(meSector.FieldByName('row_num').AsString);
    drvSector.GetrecCommand.Parameters.ParamByName('level_num').Value := StrToInt(meSector.FieldByName('level_num').AsString);
    meSector.RefreshRecord;
  end;

  Screen.Cursor := crDefault;

end;

procedure TFormMatrixSector.ClearMarkForSelected;
begin
 MarkForSelected(2, -1);
end;

procedure TFormMatrixSector.MarkOne(operation: integer; cnum: string; color: integer);
var i, j: integer;
begin

  spMatrixMark.Parameters.ParamByName('@operation').Value := operation;
  spMatrixMark.Parameters.ParamByName('@cnum').Value := cnum;
  spMatrixMark.Parameters.ParamByName('@mark').Value := color;
  spMatrixMark.ExecProc;

end;

procedure TFormMatrixSector.DecreaseScale;
begin
  nscale := nscale-1;
  RestorePosition;
end;

procedure TFormMatrixSector.IncreaseScale;
begin
  nscale := nscale+1;
  RestorePosition;
end;

end.
