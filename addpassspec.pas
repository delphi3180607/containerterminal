﻿unit AddPassSpec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, Vcl.ComCtrls, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls,
  MemTableDataEh, Vcl.Menus, MemTableEh;

type
  TFormAddPassSpec = class(TFormEdit)
    pcLayout: TPageControl;
    TabSheet1: TTabSheet;
    meNumbers: TMemo;
    btSeek: TButton;
    TabSheet2: TTabSheet;
    Label1: TLabel;
    dgData: TDBGridEh;
    meData: TMemTableEh;
    pmMemo: TPopupMenu;
    N1: TMenuItem;
    procedure btSeekClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure dgDataSelectionChanged(Sender: TObject);
  private
    g: string;
    passid: integer;
    opkindid: integer;
    spareChild: integer;
    spareParent: integer;
  public
    procedure ReInit(nOpkindid: integer; nSpareChild: integer; nSpareParent: integer; sContNumber: string);
  end;

var
  FormAddPassSpec: TFormAddPassSpec;

implementation

{$R *.dfm}

uses dmu;

procedure TFormAddPassSpec.ReInit(nOpkindid: integer; nSpareChild: integer; nSpareParent: integer; sContNumber: string);
begin

  meData.Close;
  meData.Fields.Clear;
  qrAux.Close;
  pcLayout.Pages[0].TabVisible := true;
  pcLayout.Pages[1].TabVisible := true;
  pcLayout.ActivePageIndex := 0;
  opkindid := nOpkindid;
  spareChild := nSpareChild;
  spareParent := nSpareParent;

  if (nSpareChild > 0) or (nSpareParent > 0) then
  begin

    meNumbers.Lines.Clear;
    meNumbers.Lines.Add(sContNumber);
    btSeekClick(nil);
    pcLayout.ActivePageIndex := 1;
    pcLayout.Pages[0].TabVisible := false;

  end;

end;


procedure TFormAddPassSpec.btnOkClick(Sender: TObject);
begin

  // сбрасываем таблицу с фильтром
  dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
  dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
  dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := '';
  dm.spCreateFilter.ExecProc;

  qrAux.Close;
  g := '';

  if dgData.SelectedRows.Count = 0 then
  begin
      meData.Edit;
      meData.FieldByName('checkcolumn').Value := 1;
      meData.Post;
      exit;
  end;

  inherited;

end;

procedure TFormAddPassSpec.btSeekClick(Sender: TObject);
var gu: TGUID; i: integer; conditions: string;
begin

    qrAux.Close;
    conditions := '';

    if (meNumbers.Lines.Count = 0)
    or (trim(meNumbers.Lines.Text) = '')
    then
    begin
      ShowMessage('Укажите, хотя бы один номер контейнера.');
      exit;
    end;

    for i := 0 to meNumbers.Lines.Count-1 do
    begin
     conditions := conditions +';object_num='+trim(meNumbers.Lines[i]);
    end;


    CreateGuid(gu);
    g := GuidToString(gu);

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := conditions;
    dm.spCreateFilter.ExecProc;

    qrAux.Parameters.ParamByName('guid').Value := g;
    qrAux.Parameters.ParamByName('opkindid').Value := opkindid;
    Screen.Cursor := crHourGlass;
    qrAux.Open;
    meData.Close;
    meData.LoadFromDataSet(qrAux,0,lmCopy,false);
    meData.Open;

    pcLayout.ActivePageIndex := 1;
    Screen.Cursor := crDefault;

end;

procedure TFormAddPassSpec.dgDataSelectionChanged(Sender: TObject);
begin

    if dgData.SelectedRows.CurrentRowSelected then
    begin
      meData.Edit;
      meData.FieldByName('checkcolumn').Value := 1;
      meData.Post;
    end else
    begin
      meData.Edit;
      meData.FieldByName('checkcolumn').Value := 0;
      meData.Post;
    end;

end;

end.
