﻿unit externaldatalinks;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand,
  EXLReport;

type
  TFormExternalDatalinks = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormExternalDatalinks: TFormExternalDatalinks;

implementation

{$R *.dfm}

uses editexternaldatalink;

procedure TFormExternalDatalinks.Init;
begin
  inherited;
  self.formEdit := FormEditExternalDataLink;

end;


end.
