﻿object FormGrid: TFormGrid
  Left = 0
  Top = 0
  Caption = 'FormGrid'
  ClientHeight = 511
  ClientWidth = 820
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object plBottom: TPanel
    Left = 0
    Top = 470
    Width = 820
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    object btnOk: TButton
      AlignWithMargins = True
      Left = 585
      Top = 3
      Width = 113
      Height = 35
      Align = alRight
      Caption = #1047#1072#1087#1080#1089#1072#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
    end
    object btnCancel: TButton
      AlignWithMargins = True
      Left = 704
      Top = 3
      Width = 113
      Height = 35
      Align = alRight
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object plAll: TPanel
    Left = 0
    Top = 0
    Width = 820
    Height = 470
    Align = alClient
    BevelOuter = bvNone
    Caption = 'plAll'
    ParentBackground = False
    TabOrder = 1
    object plTop: TPanel
      Left = 0
      Top = 0
      Width = 820
      Height = 29
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Align = alTop
      BevelOuter = bvNone
      Color = 15921906
      ParentBackground = False
      TabOrder = 0
      StyleElements = [seFont, seBorder]
      object sbDelete: TPngSpeedButton
        AlignWithMargins = True
        Left = 34
        Top = 0
        Width = 34
        Height = 29
        Hint = #1059#1076#1072#1083#1080#1090#1100
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Action = aDelete
        Align = alLeft
        Flat = True
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000E9C00000E
          9C01079453DD0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000001484944415478DAAD53414B024114FE
          66095996FE417F41C49325EEBAE622B241D84442873A458724FA33051DEA545D
          0AF6B0A162BA048BE62EE8EF08BA7888080FD5343B90AC68896B8F8179CCBCEF
          9BF7BDF78630C6B088917F23208404DBDA11B0720E58D382F7017A0DBC70B73B
          C285085619A51E0603545C778B93DC87C165C0BC2B956A22D6B6358E7B1A23D8
          2384DE140A1692C977743ACA89E7D153C00EEE0E80CD4B4DB3914ABDA1DF5F3E
          76DDF21963D684049EE2C6553A5D452E37E424F24EBB6DBE029F4D5D6F72B038
          AB789EC86E9A048C5E330C1BAAFA854643822C03F9BCF0C359FD4A10D83A603C
          EA7A0BF1F8109204F8BEBCDBEB99B7C0C34FCC9F04065F4E54823109F5BA0445
          01B2D90F38CED24C09A288994C9503E62FA26863B168219188DCC6D983649A35
          C462C120A91CD79D7B940F81ED0BE099BBFE8484853F5354FB06D031E7E18A64
          5A800000000049454E44AE426082}
        ExplicitTop = -2
      end
      object btFilter: TPngSpeedButton
        AlignWithMargins = True
        Left = 143
        Top = 0
        Width = 34
        Height = 29
        Hint = #1054#1090#1073#1086#1088
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000AEB00000A
          EB01828B0D5A0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000000A34944415478DA63FCFFFF3F032580
          D161CA0B8A4C607CF0E18740C2920FEF17C40890A411A88701A8479011E48587
          1F7F2A0005EE136B084CB33C3FFB07465818000D31004A9C276408B266B01790
          031168880350C17E5C86A06BC630006A480250E17C7443A09A0D819A2FA00422
          B668841952E1CA03E677ECFE8255334E0340C071EACBFF3057806CDF9F2DCE88
          351A470DC06D00B221641B00330444936D0021000075459B7A13DFF16F000000
          0049454E44AE426082}
        ExplicitLeft = 150
        ExplicitHeight = 27
      end
      object btExcel: TPngSpeedButton
        AlignWithMargins = True
        Left = 109
        Top = 0
        Width = 34
        Height = 29
        Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        OnClick = btExcelClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000010000000100803000000282D0F
          530000000373424954080808DBE14FE000000072504C544590FFD08CFDCB7FE5
          B772CFA524DAE56DCCA191BDA923D4E059C29468B7AC52BE8E4DBB8A1AB7C119
          B3BD46AC7E236A4A2169481D60431A5B3F1A583D165239005156005258124A31
          124B310F462C00403F003D3F003C3A053320042E19002B2C012C1700240F0023
          1000210F0B0B0BFFFFFFCD5AC7F30000002674524E53FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A77A81BC
          00000009704859730000004E0000004E01B1CD1F330000001C74455874536F66
          74776172650041646F62652046697265776F726B732043533571B5E336000000
          8F4944415478DA8D8F4D0B82501045E7BE34D0BEC8C8089116CDFFFF472D4224
          925244AA85F95E77F90A17CD62863970B83388E4BBA02DFC7D092D066F9FE4D0
          ABF3851DF4C519F46CE1604562E8C989C90A82BCB482E308B8D15CDF09360D95
          2DF4E224482A82B47E0BF623CAD94A98960459D58B39403B11133F08664F5E34
          FF27B661ECA22558758C4D30FD79FF03D1143A18699A33D30000000049454E44
          AE426082}
        ExplicitLeft = 116
        ExplicitHeight = 27
      end
      object sbAdd: TPngSpeedButton
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 34
        Height = 29
        Hint = #1044#1086#1073#1072#1074#1080#1090#1100
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Action = aAdd
        Align = alLeft
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000E9C00000E
          9C01079453DD0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000002764944415478DAA5935B48145118C7
          FF733AB3EC8C0EDA4B82B5B5651042498B2CAB66FA12BD78698330A38788F212
          582F05AE2F696042493DD83E943E44411704A5072F154104BB8A2D51ACDD28A8
          DDBC4441E2B2BB33E3CE7AA63389646A60F41DCE8173F8F89DEFF2FF04AC3427
          DFC75082A3C8959D98562318C11DFE768BEFC85247D334212CB98BA0F0675E70
          D7B9F2F3B073B303D97605337A026FBF44F0EA7D14F1F6B11EA868E2BEC67280
          889342C8555255E02D2A522549228CC196349250C40C8020A5691AEB1B0BC8E1
          47C1D7B81F2BB420BF01327A0ABBBC270E979726E64C26A7611249B0B13D39C5
          087E1B8566A60885C02481AAB7034F32C3A7867AA0A36111E0543A3C9FCE790F
          A884523BE1DFA5F9CAB66591335B1AD115BD8ED9548C519E1FB3566A4EBF3A30
          24C77CA3DB38206201DACAFB6BCF57B83C7AD2487180C9A3674411159C7536E1
          4AC48FB811E7582B2B816488367D78FCB9FD69F53D1F075C1678B53F3474F9B6
          E7ADCF61DC039268278699FE957BFDC6E3E89EBAC9014988028566E8169C4D27
          7E507F4547C09C32CB041C92532D6D3E4A2961FB73F7A154F110FCC502F131F6
          70F2310311E9C5DA96CFE64B33EF9F239837E7D9D7E40CF557F10826AC0880D6
          BDFD35AD55AEE2B5D5609D4D1F7C17B23FABBCDBCC6BD0B9D0857637EF825725
          36710D5D30F4CEC10772A239B49503A20B3AB0E386CB5F5D77A4BC6CAD3AE8E6
          3A685CA1C4DDC51505073D1EAEC44C92668C2679E8193C154A485AD354D6170A
          CAE1E191307A67DD7F2A7111025C932F15D6BBF3776097C3892C49414C8B637C
          2A82176F3E22D11AB27E3EBDDA2C2C9FC61A6C422536C081EF98C02406F85BEF
          AAD3681DFF633F013BA960F0622C30B40000000049454E44AE426082}
        ExplicitHeight = 27
      end
      object sbEdit: TPngSpeedButton
        AlignWithMargins = True
        Left = 68
        Top = 0
        Width = 34
        Height = 29
        Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Action = aEdit
        Align = alLeft
        Flat = True
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
          62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
          08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
          4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
          C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
          892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
          4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
          0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
          C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
          8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
          346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
          5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
          3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
          378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
          C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
          1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
          FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
          F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
          0049454E44AE426082}
        ExplicitHeight = 27
      end
      object Bevel2: TBevel
        AlignWithMargins = True
        Left = 105
        Top = 0
        Width = 1
        Height = 29
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 107
        ExplicitTop = 1
        ExplicitHeight = 41
      end
      object btTool: TPngSpeedButton
        AlignWithMargins = True
        Left = 782
        Top = 3
        Width = 35
        Height = 23
        Hint = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1082#1086#1083#1086#1085#1086#1082
        Align = alRight
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Visible = False
        OnClick = btToolClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000002034944415478DA8D915D68526118C7FFC73C608B561195
          A3981074B18E4414AE9BADF0CA046F465117DD44C101C3035D268144D46E2D21
          C5E1F42218B21578D5551F585E0CA9083157B66A75820497AB7463EC7CF5BC3B
          C6A8B91D1F7878E13DEFEFF77C1CCE300C58452693B9AAEBBA486F0FD1095114
          F9BFDF382B413A9D66F06DBFDFDF43816C368B6030C8752D48A552159FCF37C0
          F33C4AA512AAD52A42A150F7826432A90402017BBD5EC70B0A49924E46A35183
          8D42A95A0A1289844A1D6CA9D56A28168BCF09B2B95CAE21411090CBE53617C4
          E3F1D3043CF27ABD5CABD5024B56D9E170C066B3219FCFCF74163CB8125B5AD1
          87C77F086E8FC76367F32B8A02F656D3B4D52C140A4B24BBBE5E3029C6D07B40
          82A6A0D96C624EB80C5996512E97D9DC5A7BF60F94639148E4CEBF822CC13BFB
          24EC130076FF791A0B8D058C350696097087C3E18FFF37BB2698B814C3AEFD12
          9C6EE0570DD01560DB1E60360F45AE14F96BD3273AEDC9144C5C8C6107B5EDA4
          CABF095E6C00FC5660FB5EE0FD53407E338548E55C67C1FD0BD476BF84BE76E5
          C579C0DED3869F11FCEA216E54CF6EF4A73863FC8C812323047F075AF366E55E
          A759F90BC1376737844DC13DBF8163E7819F32A0A9263CF318F8FA3A875B9F46
          60119C71F7D44BEC3E781CFD83B4380D78F7842A133C3A6709AF2D71F4E85BA8
          2B87A12E834E82BF7505B3F8035B6C1B8064E1E95A0000000049454E44AE4260
          82}
        ExplicitLeft = 781
      end
      object plCount: TPanel
        Left = 585
        Top = 0
        Width = 194
        Height = 29
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object edCount: TDBEditEh
          AlignWithMargins = True
          Left = 122
          Top = 3
          Width = 69
          Height = 23
          Align = alRight
          Alignment = taCenter
          BorderStyle = bsNone
          ControlLabel.Width = 103
          ControlLabel.Height = 13
          ControlLabel.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1079#1072#1087#1080#1089#1077#1081
          ControlLabel.Visible = True
          ControlLabelLocation.Position = lpLeftCenterEh
          DynProps = <>
          EditButtons = <>
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = True
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          Visible = True
        end
      end
    end
    object dgData: TDBGridEh
      Left = 0
      Top = 29
      Width = 820
      Height = 441
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Align = alClient
      Border.Color = 12500154
      Border.ExtendedDraw = True
      BorderStyle = bsNone
      ColumnDefValues.Title.TitleButton = True
      ColumnDefValues.ToolTips = True
      Ctl3D = False
      DataSource = dsData
      DynProps = <>
      FixedColor = clCream
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Calibri'
      Font.Style = []
      GridLineParams.BrightColor = clSilver
      GridLineParams.ColorScheme = glcsClassicEh
      GridLineParams.DarkColor = clSilver
      GridLineParams.DataBoundaryColor = clSilver
      GridLineParams.DataHorzColor = 14540253
      GridLineParams.DataVertColor = 14540253
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
      IndicatorParams.Color = clBtnFace
      IndicatorParams.FillStyle = cfstThemedEh
      IndicatorParams.HorzLineColor = 14540253
      IndicatorParams.VertLineColor = 14540253
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghHighlightFocus, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      PopupMenu = pmGrid
      ReadOnly = True
      RowDetailPanel.Color = clBtnFace
      SearchPanel.Enabled = True
      SearchPanel.PersistentShowing = False
      SelectionDrawParams.SelectionStyle = gsdsClassicEh
      SelectionDrawParams.DrawFocusFrame = False
      SelectionDrawParams.DrawFocusFrameStored = True
      ShowHint = True
      SortLocal = True
      STFilter.Color = clWhite
      STFilter.HorzLineColor = 14540253
      STFilter.InstantApply = True
      STFilter.Local = True
      STFilter.VertLineColor = 14540253
      STFilter.Visible = True
      TabOrder = 1
      TitleParams.Color = clBtnFace
      TitleParams.FillStyle = cfstSolidEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -11
      TitleParams.Font.Name = 'Arial'
      TitleParams.Font.Style = [fsBold]
      TitleParams.HorzLineColor = 13421772
      TitleParams.ParentFont = False
      TitleParams.SecondColor = clCream
      TitleParams.VertLineColor = 14540253
      OnActiveGroupingStructChanged = dgDataActiveGroupingStructChanged
      OnApplyFilter = dgDataApplyFilter
      OnColumnMoved = dgDataColumnMoved
      OnColWidthsChanged = dgDataColWidthsChanged
      OnDblClick = sbEditClick
      OnDrawColumnCell = dgDataDrawColumnCell
      OnKeyPress = dgDataKeyPress
      OnMouseDown = dgDataMouseDown
      OnMouseMove = dgDataMouseMove
      OnMouseUp = dgDataMouseUp
      OnSelectionChanged = dgDataSelectionChanged
      OnTitleBtnClick = dgDataTitleBtnClick
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object plHint: TPanel
      Left = 160
      Top = 400
      Width = 193
      Height = 57
      Alignment = taLeftJustify
      Color = clInfoBk
      Ctl3D = False
      ParentBackground = False
      ParentCtl3D = False
      TabOrder = 2
      Visible = False
      StyleElements = [seFont, seBorder]
      object lbText: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 4
        Width = 185
        Height = 49
        Align = alClient
        Caption = 'lbText'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 1400426
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        WordWrap = True
        StyleElements = [seClient, seBorder]
        ExplicitWidth = 34
        ExplicitHeight = 13
      end
    end
  end
  object pmGrid: TPopupMenu
    Left = 312
    Top = 184
    object N1: TMenuItem
      Action = aAdd
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    end
    object N2: TMenuItem
      Action = aDelete
      Caption = #1059#1076#1072#1083#1080#1090#1100
    end
    object N3: TMenuItem
      Action = aEdit
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object N7: TMenuItem
      Caption = #1060#1080#1083#1100#1090#1088
      ShortCut = 16454
    end
    object Excel1: TMenuItem
      Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
      ShortCut = 16453
      OnClick = btExcelClick
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ShortCut = 116
      OnClick = N5Click
    end
  end
  object drvData: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.Parameters = <>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 88
    Top = 184
  end
  object meData: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvData
    BeforeOpen = meDataBeforeOpen
    AfterOpen = meDataAfterOpen
    AfterInsert = meDataAfterInsert
    BeforeEdit = meDataBeforeEdit
    AfterEdit = meDataAfterEdit
    BeforePost = meDataBeforePost
    AfterPost = meDataAfterPost
    AfterScroll = meDataAfterScroll
    Left = 136
    Top = 184
  end
  object dsData: TDataSource
    DataSet = meData
    Left = 192
    Top = 184
  end
  object qrAux: TADOQuery
    Connection = dm.connMain
    Parameters = <>
    Left = 368
    Top = 184
  end
  object drvForms: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select * from forms where id = :id')
    SelectCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update forms'
      'set'
      '  code = :code,'
      '  select_sql = :select_sql,'
      '  update_sql = :update_sql,'
      '  insert_sql = :insert_sql,'
      '  delete_sql = :delete_sql,'
      '  operation_sql = :operation_sql,'
      '  caption = :caption,'
      '  doc_kind = :doc_kind,'
      '  form_type = :form_type'
      'where'
      '  id = :id'
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 80
        Value = Null
      end
      item
        Name = 'select_sql'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'update_sql'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'insert_sql'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'delete_sql'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'operation_sql'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'caption'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'doc_kind'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'form_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into forms'
      
        '  (code, select_sql, update_sql, insert_sql, delete_sql, operati' +
        'on_sql, '
      '   caption, doc_kind, form_type)'
      'values'
      
        '  (:code, :select_sql, :update_sql, :insert_sql, :delete_sql, :o' +
        'peration_sql, '
      '   :caption, :doc_kind, :form_type)')
    InsertCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 80
        Value = Null
      end
      item
        Name = 'select_sql'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'update_sql'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'insert_sql'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'delete_sql'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'operation_sql'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'caption'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'doc_kind'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'form_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from forms'
      'where'
      '  id = :id'
      '')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from forms where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 508
    Top = 186
  end
  object meForms: TMemTableEh
    Params = <>
    DataDriver = drvForms
    Left = 556
    Top = 186
  end
  object dsForms: TDataSource
    DataSet = meForms
    Left = 604
    Top = 186
  end
  object al: TActionList
    Left = 256
    Top = 184
    object aAdd: TAction
      ShortCut = 45
      OnExecute = sbAddClick
    end
    object aDelete: TAction
      OnExecute = sbDeleteClick
    end
    object aEdit: TAction
      OnExecute = sbEditClick
    end
  end
  object exReport: TEXLReport
    About = 'EMS Advanced Excel Report(tm) Component Suite for Delphi(R)'
    PageBreakAfterGroupFooter = False
    Dictionary = <>
    _Version = '1.9.0.2'
    Left = 414
    Top = 184
  end
end
