﻿inherited FormDocTypeSelect: TFormDocTypeSelect
  ActiveControl = dbData
  BorderStyle = bsToolWindow
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1090#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 351
  ClientWidth = 587
  OnActivate = FormActivate
  ExplicitWidth = 593
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 310
    Width = 587
    ExplicitTop = 310
    ExplicitWidth = 635
    inherited btnCancel: TButton
      Left = 471
      ExplicitLeft = 519
    end
    inherited btnOk: TButton
      Left = 352
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
      ExplicitLeft = 400
    end
  end
  object dbData: TDBGridEh [1]
    Left = 0
    Top = 0
    Width = 587
    Height = 310
    Align = alClient
    AutoFitColWidths = True
    DataSource = dsLocal
    DynProps = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    GridLineParams.DataHorzLines = False
    GridLineParams.DataVertLines = False
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    IndicatorOptions = []
    Options = [dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    OnDrawColumnCell = dbDataDrawColumnCell
    Columns = <
      item
        AutoFitColWidth = False
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'start_state_code'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        Footers = <>
        Visible = False
        Width = 256
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'doctype_code'
        Footers = <>
        TextEditing = False
        Title.Caption = #1044#1086#1089#1090#1091#1087#1085#1099#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
        Width = 269
      end
      item
        CellButtons = <>
        Checkboxes = True
        DynProps = <>
        EditButtons = <>
        FieldName = 'pref_sign'
        Footers = <>
        Width = 102
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited dsLocal: TDataSource
    Left = 176
    Top = 56
  end
  inherited qrAux: TADOQuery
    Left = 248
    Top = 56
  end
  object dsAux: TDataSource
    DataSet = qrAux
    Left = 104
    Top = 56
  end
end
