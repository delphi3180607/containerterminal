﻿unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.ComCtrls, Vcl.Buttons, IniFiles, DBCtrlsEh,
  PngSpeedButton, System.Actions, Vcl.ActnList, Data.DB, Data.Win.ADODB, docflow, grid, StdCtrls, Functions,
  DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, Vcl.ExtCtrls, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, MemTableDataEh, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.TabNotBk, PropStorageEh,
  IdCoder, IdCoder3to4, IdCoderBinHex4, IdBaseComponent;

type
  TFormMain = class(TForm)
    mm: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N25: TMenuItem;
    N28: TMenuItem;
    pmPc: TPopupMenu;
    N29: TMenuItem;
    btClose: TPngSpeedButton;
    N33: TMenuItem;
    N10: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N15: TMenuItem;
    N20: TMenuItem;
    N8: TMenuItem;
    N14: TMenuItem;
    N31: TMenuItem;
    N32: TMenuItem;
    N35: TMenuItem;
    N36: TMenuItem;
    N9: TMenuItem;
    N26: TMenuItem;
    N39: TMenuItem;
    N40: TMenuItem;
    N44: TMenuItem;
    N45: TMenuItem;
    N23: TMenuItem;
    N47: TMenuItem;
    N48: TMenuItem;
    N49: TMenuItem;
    N51: TMenuItem;
    N52: TMenuItem;
    N54: TMenuItem;
    N53: TMenuItem;
    N55: TMenuItem;
    N56: TMenuItem;
    N57: TMenuItem;
    N58: TMenuItem;
    N59: TMenuItem;
    N60: TMenuItem;
    N7: TMenuItem;
    N43: TMenuItem;
    N46: TMenuItem;
    N24: TMenuItem;
    N27: TMenuItem;
    N37: TMenuItem;
    N41: TMenuItem;
    N50: TMenuItem;
    N62: TMenuItem;
    N64: TMenuItem;
    N65: TMenuItem;
    N67: TMenuItem;
    N70: TMenuItem;
    N4: TMenuItem;
    N71: TMenuItem;
    N72: TMenuItem;
    N11: TMenuItem;
    N73: TMenuItem;
    N74: TMenuItem;
    N76: TMenuItem;
    N77: TMenuItem;
    N78: TMenuItem;
    N34: TMenuItem;
    N79: TMenuItem;
    N61: TMenuItem;
    N30: TMenuItem;
    N63: TMenuItem;
    N66: TMenuItem;
    N68: TMenuItem;
    N69: TMenuItem;
    N75: TMenuItem;
    N80: TMenuItem;
    N81: TMenuItem;
    N82: TMenuItem;
    N83: TMenuItem;
    N84: TMenuItem;
    N85: TMenuItem;
    N38: TMenuItem;
    N42: TMenuItem;
    N86: TMenuItem;
    N87: TMenuItem;
    N88: TMenuItem;
    N89: TMenuItem;
    N90: TMenuItem;
    N91: TMenuItem;
    N92: TMenuItem;
    N93: TMenuItem;
    N94: TMenuItem;
    N96: TMenuItem;
    N97: TMenuItem;
    N98: TMenuItem;
    N99: TMenuItem;
    N100: TMenuItem;
    N101: TMenuItem;
    N102: TMenuItem;
    drvData: TADODataDriverEh;
    meData: TMemTableEh;
    dsData: TDataSource;
    al: TActionList;
    pmGrid: TPopupMenu;
    MenuItem5: TMenuItem;
    Excel1: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    N105: TMenuItem;
    N106: TMenuItem;
    N95: TMenuItem;
    N103: TMenuItem;
    pc: TPageControl;
    N104: TMenuItem;
    N107: TMenuItem;
    N108: TMenuItem;
    N109: TMenuItem;
    N110: TMenuItem;
    N111: TMenuItem;
    N112: TMenuItem;
    N113: TMenuItem;
    N114: TMenuItem;
    N115: TMenuItem;
    N116: TMenuItem;
    procedure N29Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure N19Click(Sender: TObject);
    procedure N24Click(Sender: TObject);
    procedure N25Click(Sender: TObject);
    procedure N33Click(Sender: TObject);
    procedure N31Click(Sender: TObject);
    procedure N32Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure N34Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N35Click(Sender: TObject);
    procedure N36Click(Sender: TObject);
    procedure N26Click(Sender: TObject);
    procedure N39Click(Sender: TObject);
    procedure N48Click(Sender: TObject);
    procedure N49Click(Sender: TObject);
    procedure N42Click(Sender: TObject);
    procedure N45Click(Sender: TObject);
    procedure N37Click(Sender: TObject);
    procedure N50Click(Sender: TObject);
    procedure N52Click(Sender: TObject);
    procedure N51Click(Sender: TObject);
    procedure N53Click(Sender: TObject);
    procedure N54Click(Sender: TObject);
    procedure N55Click(Sender: TObject);
    procedure N44Click(Sender: TObject);
    procedure pcChange(Sender: TObject);
    procedure N46Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N72Click(Sender: TObject);
    procedure N66Click(Sender: TObject);
    procedure N73Click(Sender: TObject);
    procedure N64Click(Sender: TObject);
    procedure N62Click(Sender: TObject);
    procedure N60Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N74Click(Sender: TObject);
    procedure N77Click(Sender: TObject);
    procedure N78Click(Sender: TObject);
    procedure N65Click(Sender: TObject);
    procedure N30Click(Sender: TObject);
    procedure N63Click(Sender: TObject);
    procedure N68Click(Sender: TObject);
    procedure N75Click(Sender: TObject);
    procedure N80Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure N81Click(Sender: TObject);
    procedure N84Click(Sender: TObject);
    procedure N82Click(Sender: TObject);
    procedure N85Click(Sender: TObject);
    procedure N38Click(Sender: TObject);
    procedure N56Click(Sender: TObject);
    procedure N87Click(Sender: TObject);
    procedure N88Click(Sender: TObject);
    procedure N89Click(Sender: TObject);
    procedure sbMainDblClick(Sender: TObject);
    procedure N28Click(Sender: TObject);
    procedure N90Click(Sender: TObject);
    procedure N92Click(Sender: TObject);
    procedure N93Click(Sender: TObject);
    procedure N97Click(Sender: TObject);
    procedure N98Click(Sender: TObject);
    procedure N86Click(Sender: TObject);
    procedure N105Click(Sender: TObject);
    procedure N106Click(Sender: TObject);
    procedure N95Click(Sender: TObject);
    procedure N103Click(Sender: TObject);
    procedure N107Click(Sender: TObject);
    procedure N108Click(Sender: TObject);
    procedure N109Click(Sender: TObject);
    procedure N111Click(Sender: TObject);
    procedure N113Click(Sender: TObject);
    procedure N115Click(Sender: TObject);
  private
    { Private declarations }
  public
    currentUserId: integer;
    currentUser: string;
    currentRole: string;

    readonly_sections : string;
    confirmrestrict_sections :string;
    unavalable_sections :string;
    allowdelete_sections :string;
    allow_edit_price :boolean;
    restrict_edit_dicts :boolean;
    restrict_edit_objects :boolean;

    isAdmin: boolean;
    isSupervisor: boolean;
    registeredForms: TStringList;
    registeredFormsList: TList;
    registeredFormsIds: TStringList;
    formIdCounter: integer;
    Scale: integer;

    procedure AddForm(f: TForm);
    procedure AddFormGrid(f: TFormGrid);
    procedure AddFormGrid1(var f: TFormGrid; c: TComponentClass);
    procedure AddDocFlowForm(settings_code: string);
    function CheckUser(login: string; password: string; weak: boolean = true): boolean;
    function GetDocFlowForm(setting_code: string): TFormDocFlow;
    function Init: boolean;
    procedure InitObjectType(var c: TDBComboBoxEh);
    procedure InitLinkType(var c: TDBComboBoxEh);
    procedure SaveConfig(dbname, hostname, user, password: string);
    function Login: boolean;

  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

uses dmu, login, users, DocFlowRules, counteragents, carriagekinds,
  containerkinds, stations, servicekinds, statekinds, operationkinds, persons,
  Invoices, containers, carriages, trains, Contracts, emailaccounts, cargosheet,
  cargotypes, deliverytypes, doctypes, cargos, cargodocs, addresscarriages,
  addresscontainers, loadplan, objectmove, removaldocs,
  settings, docroutes, objectstatekinds, restrictions, tasksincome, unloaddocs,
  orders, directions, load, dispatch, dispatchkinds, reports, AlertQue, report1,
  ObjectsInProcess, StorePrices, CustomerOperations, docemptyincome, ExtraServices,
  ServicePrices, techconditions, rotations, ContainersList, CarriagesList,
  externaldatalinks, inspects, carriagesinspect, containersinspect, tariftypes,
  mtukinds, picturekinds, sqlquery, carriagemodels, ContainersOnGo,
  CarriagesOnGo, docfeed, unloadplan, unloadfronts, placecarriages, paths,
  ReportByCounteragent, About, SetListCarriages, loadschess, filterloads,
  DispatchMonitor, raportsettings, psvzhrep, EnterpriseMonitor, roles,
  StartChecks, ShippingOptions, billed, ShippingOptionsRules, archivefeeds,
  PassOperations, Passes, Classifications, matrix, matrixcoords, MatrixAssigns;

procedure TFormMain.N103Click(Sender: TObject);
begin
  self.AddFormGrid(FormArchiveFeeds);
end;

procedure TFormMain.N105Click(Sender: TObject);
begin
  self.AddFormGrid(FormShippingOptions);
end;

procedure TFormMain.N106Click(Sender: TObject);
begin

  if pos('billed',FormMain.unavalable_sections)>0 then
  begin
    ShowMessage('У вас нет доступа в данный раздел.');
    exit;
  end;

  self.AddFormGrid(FormBilled);
end;

procedure TFormMain.N107Click(Sender: TObject);
begin
  self.AddFormGrid(FormPassOperations);
end;

procedure TFormMain.N108Click(Sender: TObject);
begin

  if pos('passes',FormMain.unavalable_sections)>0 then
  begin
    ShowMessage('У вас нет доступа в данный раздел.');
    exit;
  end;

  self.AddFormGrid1(TFormGrid(FormPasses0), TFormPasses);

end;

procedure TFormMain.N109Click(Sender: TObject);
begin
  self.AddFormGrid1(TFormGrid(FormClassifications), TFormClassifications);
end;

procedure TFormMain.N111Click(Sender: TObject);
begin

  if pos('matrix',FormMain.unavalable_sections)>0 then
  begin
    ShowMessage('У вас нет доступа в данный раздел.');
    exit;
  end;

  self.AddFormGrid(FormMatrix);
end;

procedure TFormMain.N113Click(Sender: TObject);
begin
  if pos('matrix',FormMain.unavalable_sections)>0 then
  begin
    ShowMessage('У вас нет доступа в данный раздел.');
    exit;
  end;

  self.AddFormGrid(FormMatrixCoords);
end;

procedure TFormMain.N115Click(Sender: TObject);
begin
  if pos('matrix',FormMain.unavalable_sections)>0 then
  begin
    ShowMessage('У вас нет доступа в данный раздел.');
    exit;
  end;

  self.AddFormGrid(FormMatrixAssigns);
end;

procedure TFormMain.N11Click(Sender: TObject);
begin
  self.AddFormGrid(FormServicePrices);
end;

procedure TFormMain.N12Click(Sender: TObject);
begin
  self.AddFormGrid(FormTrains);
end;

procedure TFormMain.N13Click(Sender: TObject);
begin
  self.AddFormGrid(FormContracts);
end;

procedure TFormMain.N16Click(Sender: TObject);
begin
  self.AddFormGrid1(TFormGrid(FormCounteragents), TFormCounteragents);
end;

procedure TFormMain.N17Click(Sender: TObject);
begin
  self.AddFormGrid(FormCarriageKinds);
end;

procedure TFormMain.N18Click(Sender: TObject);
begin
  self.AddFormGrid(FormContainerKinds);
end;

procedure TFormMain.N19Click(Sender: TObject);
begin
  self.AddFormGrid1(TFormGrid(FormStations), TFormStations);
end;

procedure TFormMain.N20Click(Sender: TObject);
begin
  self.AddFormGrid(FormEmailAccounts);
end;

procedure TFormMain.N21Click(Sender: TObject);
begin
  self.AddFormGrid(FormServiceKinds);
end;

procedure TFormMain.N22Click(Sender: TObject);
begin
  self.AddFormGrid(FormUsers);
end;

procedure TFormMain.N24Click(Sender: TObject);
begin
  self.AddFormGrid(FormDispatchKinds);
end;

procedure TFormMain.N25Click(Sender: TObject);
begin
  self.AddFormGrid(FormDocRoutes);
end;

procedure TFormMain.N26Click(Sender: TObject);
begin
  self.AddFormGrid(FormDocTypes);
end;

procedure TFormMain.N28Click(Sender: TObject);
begin
FormAbout.ShowModal;
end;

procedure TFormMain.N29Click(Sender: TObject);
var t: TTabSheet; f: TWinControl;
begin

 t := pc.ActivePage;
 if t.Name = 'TabStart' then exit;

 t.Hide;
 f := TWinControl(t.Controls[0]);
 f.Hide;
 f.Parent := nil;
 t.Free;

 if pc.PageCount = 0 then
 begin
   btClose.Hide;
   pc.Hide;
 end;

end;

procedure TFormMain.N30Click(Sender: TObject);
begin
   AddFormGrid(FormTarifTypes);
end;

procedure TFormMain.N31Click(Sender: TObject);
begin
   AddFormGrid(FormContainers);
end;

procedure TFormMain.N32Click(Sender: TObject);
begin
  self.AddFormGrid(FormCarriages);
end;

procedure TFormMain.N33Click(Sender: TObject);
begin
  self.AddFormGrid(FormPersons);
end;

procedure TFormMain.N34Click(Sender: TObject);
begin
  self.AddFormGrid(FormExternalDataLinks);
end;

procedure TFormMain.N35Click(Sender: TObject);
begin
  self.AddFormGrid(FormCargoTypes);
end;

procedure TFormMain.N36Click(Sender: TObject);
begin
  self.AddFormGrid(FormDeliveryTypes);
end;

procedure TFormMain.N37Click(Sender: TObject);
begin
  self.AddFormGrid(FormAlertQue);
end;

procedure TFormMain.N38Click(Sender: TObject);
begin
  self.AddFormGrid(FormDocFeed);
end;

procedure TFormMain.N39Click(Sender: TObject);
begin
  self.AddFormGrid(FormCargos);
end;

procedure TFormMain.N3Click(Sender: TObject);
begin
  self.AddFormGrid(FormReports);
end;

procedure TFormMain.N42Click(Sender: TObject);
begin
  self.AddFormGrid(FormUnloadFronts);
end;

procedure TFormMain.N44Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormDocDispatch), TFormDocDispatch);
end;

procedure TFormMain.N45Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormDocOrders), TFormDocOrders);
end;

procedure TFormMain.N46Click(Sender: TObject);
begin
  self.AddFormGrid(FormDirections);
end;

procedure TFormMain.N48Click(Sender: TObject);
begin
  self.AddFormGrid(FormAddressCarriages);
end;

procedure TFormMain.N49Click(Sender: TObject);
begin
  self.AddFormGrid(FormAddressContainers);
end;

procedure TFormMain.N4Click(Sender: TObject);
begin
  self.AddFormGrid(FormInvoices);
end;

procedure TFormMain.N50Click(Sender: TObject);
begin
  self.AddFormGrid(FormObjectMove);
end;

procedure TFormMain.N51Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormRemovalDocs), TFormRemovalDocs);
end;

procedure TFormMain.N52Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormLoads), TFormLoads);
end;

procedure TFormMain.N53Click(Sender: TObject);
begin
  AddFormGrid(FormRestrictions);
end;

procedure TFormMain.N54Click(Sender: TObject);
begin
  AddFormGrid(FormSettings);
end;

procedure TFormMain.N55Click(Sender: TObject);
begin
  AddFormGrid(FormObjectStateKinds);
end;

procedure TFormMain.N56Click(Sender: TObject);
begin
  AddFormGrid(FormUnLoadPlan);
end;

procedure TFormMain.N60Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormExtraServices), TFormExtraServices);
end;

procedure TFormMain.N62Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormCarriagesInspect), TFormCarriagesInspect);
end;

procedure TFormMain.N63Click(Sender: TObject);
begin
  AddFormGrid(FormLoadPlan);
end;

procedure TFormMain.N64Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormDocEmptyIncome), TFormDocEmptyIncome);
end;

procedure TFormMain.N65Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormContainersInspects), TFormContainersInspects);
end;

procedure TFormMain.N66Click(Sender: TObject);
begin
  AddFormGrid(FormMtuKinds);
end;

procedure TFormMain.N68Click(Sender: TObject);
begin
  AddFormGrid(FormPictureKinds);
end;

procedure TFormMain.N72Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormStorePrices), TFormStorePrices);
end;

procedure TFormMain.N73Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormCustomerOperations), TFormCustomerOperations);
end;

procedure TFormMain.N74Click(Sender: TObject);
begin
  AddFormGrid(FormTechConditions);
end;

procedure TFormMain.N75Click(Sender: TObject);
begin
  FormSQLQuery.ShowModal;
end;

procedure TFormMain.N77Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormContainersList), TFormContainersList);
end;

procedure TFormMain.N78Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormCarriagesList), TFormCarriagesList);
end;

procedure TFormMain.N80Click(Sender: TObject);
begin
  AddFormGrid(FormStartChecks);
end;

procedure TFormMain.N81Click(Sender: TObject);
begin
  AddFormGrid(FormCarriageModels);
end;

procedure TFormMain.N82Click(Sender: TObject);
begin
  AddFormGrid(FormContainersOnGo);
end;

procedure TFormMain.N84Click(Sender: TObject);
begin
  FormContainersOnGo.drvData.SelectCommand.Parameters.ParamByName('sections').Value := 'output';
  AddFormGrid(FormContainersOnGo);
end;

procedure TFormMain.N85Click(Sender: TObject);
begin
  AddFormGrid(FormCarriagesOnGo);
end;

procedure TFormMain.N86Click(Sender: TObject);
begin
  AddFormGrid(FormRoles);
end;

procedure TFormMain.N87Click(Sender: TObject);
begin
  AddFormGrid(FormPlaceCarriages);
end;

procedure TFormMain.N88Click(Sender: TObject);
begin
  AddFormGrid(FormPaths);
end;

procedure TFormMain.N89Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormReportByCounteragent), TFormReportByCounteragent);
end;

procedure TFormMain.N8Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormCargoSheet), TFormCargoSheet);
end;

procedure TFormMain.N90Click(Sender: TObject);
begin

  if pos('outcome_dispatch',FormMain.unavalable_sections)>0 then
  begin
    ShowMessage('У вас нет доступа в данный раздел.');
    exit;
  end;

  AddFormGrid1(TFormGrid(FormLoadsChess), TFormLoadsChess);
  FormLoadsChess.Caption := 'Дислокация';
  FormLoadsChess.plBottom.Height := 41;
  FormLoadsChess.btnOk.Hide;
end;

procedure TFormMain.N92Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormDispatchMonitor), TFormDispatchMonitor);
end;

procedure TFormMain.N93Click(Sender: TObject);
begin
  AddFormGrid1(TFormGrid(FormRaportSettings), TFormRaportSettings);
end;

procedure TFormMain.N95Click(Sender: TObject);
begin
  self.AddFormGrid(FormShippingOptionsRules);
end;

procedure TFormMain.N97Click(Sender: TObject);
begin
 FormPsVZhRep.ShowModal;
end;

procedure TFormMain.N98Click(Sender: TObject);
begin
  self.AddForm(FormEnterpriseMonitor);
end;

procedure TFormMain.pcChange(Sender: TObject);
var f: TFormGrid; t: TTabSheet;
begin
  t := pc.ActivePage;
  if t.Name = 'TabStart' then exit;
  f := TFormGrid(pc.ActivePage.Controls[0]);
  f.PartialInit;
end;

procedure TFormMain.sbMainDblClick(Sender: TObject);
begin
  FormSetListCarriages.ShowModal;
end;

procedure TFormMain.AddDocFlowForm(settings_code: string);
var f: TFormDocFlow;
begin
  pc.Show;

  f := GetDocFlowForm(settings_code);
  if Assigned(f) then
  begin
    self.AddForm(f);
    f.Init;
  end;
end;


function TFormMain.GetDocFlowForm(setting_code: string): TFormDocFlow;
var f: TFormDocFlow; i: integer;
begin

  i := registeredForms.IndexOf(setting_code);

  if i>-1 then
  begin
    f:= TFormDocFlow(registeredFormsList[i]);
    result := f;
    exit;
  end;

  dm.qrAux.Close;
  dm.qrAux.SQL.Clear;
  dm.qrAux.SQL.Add('select * from forms where code = '''+trim(setting_code)+'''');
  dm.qrAux.Open;

  f := TFormDocFlow.Create(Application);

  if dm.qrAux.RecordCount>0 then
  begin

    registeredForms.Add(dm.qrAux.FieldByName('code').AsString);
    registeredFormsList.Add(f);
    registeredFormsIds.Add(dm.qrAux.FieldByName('id').AsString);

    f.Caption := dm.qrAux.FieldByName('caption').AsString;

    f.formId := dm.qrAux.FieldByName('Id').AsInteger;

  end else
  begin

    dm.qrAux.Close;
    dm.qrAux.SQL.Clear;
    dm.qrAux.SQL.Add('insert into forms (code, caption) values ('''+setting_code+''', ''Новая форма'')');
    dm.qrAux.ExecSQL;

    dm.qrAux.Close;
    dm.qrAux.SQL.Clear;
    dm.qrAux.SQL.Add('select IDENT_CURRENT(''forms'')');
    dm.qrAux.Open;

    f.Caption := 'Новая форма';

    f.formId := dm.qrAux.Fields[0].AsInteger;

    registeredForms.Add(setting_code);
    registeredFormsList.Add(f);
    registeredFormsIds.Add(dm.qrAux.Fields[0].AsString)

  end;

  result := f;

end;


procedure TFormMain.AddForm(f: TForm);
var t: TTabSheet;
begin

   pc.Show;

  // сначала ищем эту форму

  if Assigned(f.Parent) then
  begin
    t := TTabSheet(f.Parent);
    pc.ActivePage := t;
    t.Caption := f.Caption;
    exit;
  end;

  t := TTabSheet.Create(pc);
  t.AlignWithMargins := false;
  t.Caption := f.Caption;
  t.PageControl := pc;
  t.PopupMenu := pmPc;
  t.Show;
  f.Parent := t;
  f.BorderStyle := bsNone;
  f.Align := alClient;
  f.Show;
  btClose.Show;
end;


procedure TFormMain.AddFormGrid(f: TFormGrid);
var t: TTabSheet; i: integer;
begin

   pc.Show;

  // сначала ищем эту форму

  if Assigned(f) then
  if Assigned(f.Parent) then
  begin
    t := TTabSheet(f.Parent);
    pc.ActivePage := t;
    exit;
  end;

  t := TTabSheet.Create(pc);
  t.AlignWithMargins := false;
  t.Caption := f.Caption;
  t.PageControl := pc;
  t.PopupMenu := pmPc;
  t.Show;

  if not Assigned(f) then
  exit;

  f.Parent := t;
  f.BorderStyle := bsNone;
  f.Align := alClient;
  f.Show;
  f.BorderStyle := bsNone;
  f.Align := alClient;
  f.Init;

  btClose.Show;

end;


procedure TFormMain.AddFormGrid1(var f: TFormGrid; c: TComponentClass);
var t: TTabSheet; i: integer;
begin

   pc.Show;

  // сначала ищем эту форму

  if Assigned(f) then
  if Assigned(f.Parent) then
  begin
    t := TTabSheet(f.Parent);
    pc.ActivePage := t;
    exit;
  end;

  if not Assigned(f) then
  Application.CreateForm(c,f);

  t := TTabSheet.Create(pc);
  t.AlignWithMargins := false;
  t.Caption := f.Caption;
  t.PageControl := pc;
  t.PopupMenu := pmPc;
  t.Show;

  f.Parent := t;
  f.BorderStyle := bsNone;
  f.Align := alClient;
  f.Show;
  f.Init;
  btClose.Show;
end;


function TFormMain.CheckUser(login: string; password: string; weak: boolean = true): boolean;
var rolename: string;
begin
  result := false;
  dm.qrCommon.Close;
  dm.qrCommon.SQL.Clear;
  dm.qrCommon.SQL.Add('select u.password, u.user_name, u.user_login, u.isAdmin, u.id, u.isSuperVisor, ');
  dm.qrCommon.SQL.Add('r.readonly_sections, r.confirmrestrict_sections, r.unavalable_sections, r.allowdelete_sections, ');
  dm.qrCommon.SQL.Add('r.allow_edit_price, r.restrict_edit_dicts, r.restrict_edit_objects, r.name as user_role');
  dm.qrCommon.SQL.Add('from users u left outer join roles r on (r.id = u.user_role)');
  dm.qrCommon.SQL.Add('where UPPER(user_login)='''+UpperCase(login)+'''');
  dm.qrCommon.Open;
  if dm.qrCommon.RecordCount>0 then
  begin
    if (dm.qrCommon.Fields[0].AsString = password) then
    begin

      if not dm.qrCommon.Fields[3].AsBoolean then
      begin
        self.N5.Visible := false;
      end;

      self.Caption := 'Управление контейнерным терминалом ('+dm.qrCommon.Fields[1].AsString +')';

      self.currentUser := dm.qrCommon.Fields[1].AsString;
      self.isAdmin := dm.qrCommon.Fields[3].AsBoolean;
      self.currentUserId := dm.qrCommon.Fields[4].AsInteger;
      self.isSuperVisor := dm.qrCommon.Fields[5].AsBoolean;

      self.readonly_sections := dm.qrCommon.Fields[6].AsString;
      self.confirmrestrict_sections := dm.qrCommon.Fields[7].AsString;
      self.unavalable_sections := dm.qrCommon.Fields[8].AsString;

      self.allowdelete_sections := dm.qrCommon.Fields[9].AsString;

      self.allow_edit_price := dm.qrCommon.Fields[10].AsBoolean;
      self.restrict_edit_dicts := dm.qrCommon.Fields[11].AsBoolean;
      self.restrict_edit_objects := dm.qrCommon.Fields[12].AsBoolean;
      self.currentRole := dm.qrCommon.Fields[13].AsString;

      result := true;
    end;
  end;
end;


procedure TFormMain.FormCreate(Sender: TObject);
begin
  Scale := 100;
end;


procedure TFormMain.SaveConfig(dbname, hostname, user, password: string);
var AppDir: string; infprog: TIniFile; HashString: string;
begin

 HashString := '0192837465pqowieuryt';

 SetCurrentDir(ExtractFileDir(Application.ExeName));
 AppDir := ExtractFileDir(Application.ExeName);

 infprog := TIniFile.Create(AppDir+'/config.ini');

 infprog.WriteString('main','dbname', XorEncode(dbname, HashString));
 infprog.WriteString('main','host',XorEncode(hostname, HashString));
 infprog.WriteString('main','user',XorEncode(user, HashString));
 infprog.WriteString('main','password',XorEncode(password, HashString));

end;




function TFormMain.Init: boolean;
var AppDir, dbname, hostname, user, password: string; infprog: TIniFile;
var HashString: string;
begin

 result := true;

 HashString := '0192837465pqowieuryt';

 registeredForms := TStringList.Create;
 registeredFormsList := TList.Create;
 registeredFormsIds := TStringList.Create;

 formIdCounter := 0;

  // 1. читаем конфигурационный файл
 SetCurrentDir(ExtractFileDir(Application.ExeName));
 AppDir := ExtractFileDir(Application.ExeName);

 infprog := TIniFile.Create(AppDir+'/config.ini');
 dbname := XorDecode(infprog.ReadString('main','dbname',''), HashString);

 if dbname = '' then
 begin
   ShowMessage('Неверный или отсутствует конфигурационный файл config.ini.');
   result := false;
   exit;
 end;

 hostname := XorDecode(infprog.ReadString('main','host',''), HashString);
 user := XorDecode(infprog.ReadString('main','user',''), HashString);
 password := XorDecode(infprog.ReadString('main','password',''), HashString);

 dm.connMain.Close;
 dm.connMain.ConnectionString := 'Provider=SQLOLEDB.1;Password='+password+';Persist Security Info=True;User ID='+user+';Initial Catalog='+dbname+';Data Source='+hostname+';Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Use Encryption for Data=False;Tag with column collation when possible=False;Connect Timeout=1800';
 //dm.connMain.CommandTimeout := 1800;
 //dm.connMain.ConnectionTimeOut :=1800;

 try
   dm.connMain.Open;
 except
   ShowMessage('Не могу подключиться к базе данных. ');
   result := false;
   exit;
 end;

  //dm.connMain.CommandTimeout := 1800;
  //dm.connMain.ConnectionTimeOut :=1800;

end;


procedure TFormMain.InitObjectType(var c: TDBComboBoxEh);
begin

  c.Items.Clear;
  c.Items.Add('Контейнерные поезда');
  c.Items.Add('Грузы в вагонах');
  c.Items.Add('Грузы в автомобилях');
  c.Items.Add('Контейнеры');
  c.Items.Add('Вагоны');

  c.KeyItems.Clear;
  c.KeyItems.Add('trains');
  c.KeyItems.Add('cargosoncarriages');
  c.KeyItems.Add('cargosoncars');
  c.KeyItems.Add('containers');
  c.KeyItems.Add('carriages');

end;


procedure TFormMain.InitLinkType(var c: TDBComboBoxEh);
begin

  c.Items.Clear;
  c.Items.Add('Контейнерные поезда');
  c.Items.Add('Грузы в вагонах');
  c.Items.Add('Грузы в автомобилях');
  c.Items.Add('Контейнеры');
  c.Items.Add('Вагоны');

  c.KeyItems.Clear;
  c.KeyItems.Add('trains');
  c.KeyItems.Add('cargosoncarriages');
  c.KeyItems.Add('cargosoncars');
  c.KeyItems.Add('containers');
  c.KeyItems.Add('carriages');

end;


function TFormMain.Login: boolean;
var c: TLabel;
begin
   dm.qrAux.Close;
   dm.qrAux.SQL.Clear;
   dm.qrAux.SQL.Add('select user_login from users where isnull(isblocked,0) = 0 order by 1');
   dm.qrAux.Open;

   FormLogin.leLogin.Items.Clear;

   while not dm.qrAux.Eof do
   begin
    FormLogin.leLogin.Items.Add(dm.qrAux.Fields[0].AsString);
    dm.qrAux.Next;
   end;

   while true do
    begin
      FormLogin.ShowModal;
      if FormLogin.ModalResult = mrOk then
      begin
        if (FormLogin.leLogin.Text = 'Администратор') and  (FormLogin.lePassword.Text = 'root147896325')
        then begin
          self.currentUser := 'root';
          self.isAdmin := true;
          result := true;
          break;
        end else
        begin
          if (not CheckUser(FormLogin.leLogin.Text, FormLogin.lePassword.Text, true)) then
          begin
            ShowMessage('Неверная пара логин/пароль');
            result := false;
          end else begin
            result := true;
            break;
          end;
        end;
      end;
      if (FormLogin.ModalResult <> mrOk) then
      begin
        result := false;
        break;
      end;
    end;
end;



end.
