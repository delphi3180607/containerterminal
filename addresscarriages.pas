﻿unit addresscarriages;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport;

type
  TFormAddressCarriages = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormAddressCarriages: TFormAddressCarriages;

implementation

{$R *.dfm}

uses editcodename;

procedure TFormAddressCarriages.Init;
begin
  inherited;
  tablename := 'addresscarriages';
  self.formEdit := FormEditCodeName;
end;


end.
