﻿inherited FormEditSPCusts: TFormEditSPCusts
  Caption = #1050#1083#1080#1077#1085#1090
  ClientHeight = 231
  ClientWidth = 455
  ExplicitWidth = 461
  ExplicitHeight = 259
  PixelsPerInch = 96
  TextHeight = 16
  object Label8: TLabel [0]
    Left = 16
    Top = 64
    Width = 47
    Height = 16
    Caption = #1050#1083#1080#1077#1085#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbForwarder: TSpeedButton [1]
    Left = 407
    Top = 85
    Width = 37
    Height = 24
    Caption = '...'
    OnClick = sbForwarderClick
  end
  object Label17: TLabel [2]
    Left = 16
    Top = 8
    Width = 110
    Height = 16
    Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel [3]
    Left = 16
    Top = 125
    Width = 34
    Height = 16
    Caption = #1062#1077#1085#1072
  end
  inherited plBottom: TPanel
    Top = 190
    Width = 455
    TabOrder = 3
    ExplicitTop = 190
    ExplicitWidth = 455
    inherited btnCancel: TButton
      Left = 339
      ExplicitLeft = 339
    end
    inherited btnOk: TButton
      Left = 220
      ExplicitLeft = 220
    end
  end
  object dtChange: TDBDateTimeEditEh [5]
    Left = 16
    Top = 30
    Width = 121
    Height = 24
    DataField = 'date_change'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object nePrice: TDBNumberEditEh [6]
    Left = 16
    Top = 147
    Width = 121
    Height = 24
    DataField = 'price_default'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object luCustomer: TDBSQLLookUp [7]
    Left = 16
    Top = 86
    Width = 387
    Height = 23
    DataField = 'customer_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 1
    Visible = True
    SqlSet = dm.ssCounteragents
  end
  inherited dsLocal: TDataSource
    Left = 294
    Top = 8
  end
  inherited qrAux: TADOQuery
    Left = 360
    Top = 16
  end
end
