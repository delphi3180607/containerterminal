﻿unit loadplan;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, docflow, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, grid, EXLReportExcelTLB,
  EXLReportBand, EXLReport;

type
  TFormLoadPlan = class(TFormGrid)
    btClear: TButton;
    procedure FormCreate(Sender: TObject);
    procedure meDataBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    system_section: string;
    procedure Init; override;
  end;

var
  FormLoadPlan: TFormLoadPlan;

implementation

{$R *.dfm}

uses editloadplan;

procedure TFormLoadPlan.FormCreate(Sender: TObject);
begin
  inherited;
  system_section := 'outcome_loads';
end;

procedure TFormLoadPlan.Init;
begin
 self.drvData.SelectCommand.Parameters.ParamByName('system_section').Value := system_section;
 inherited;
 self.formEdit := FormEditLoadPlan;
end;



procedure TFormLoadPlan.meDataBeforePost(DataSet: TDataSet);
begin
  inherited;
  meData.FieldByName('system_section').Value := system_section;
end;

end.
