﻿inherited FormEditReportParam: TFormEditReportParam
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088' '#1086#1090#1095#1077#1090#1072
  ClientHeight = 516
  ClientWidth = 542
  ExplicitWidth = 548
  ExplicitHeight = 544
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 12
    Top = 175
    Width = 78
    Height = 16
    Caption = 'SQL '#1079#1072#1087#1088#1086#1089
  end
  object edTitle: TDBEditEh [1]
    Left = 10
    Top = 81
    Width = 519
    Height = 24
    ControlLabel.Width = 148
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1088#1080#1075#1083#1072#1096#1077#1085#1080#1077' '#1082' '#1074#1074#1086#1076#1091
    ControlLabel.Visible = True
    DataField = 'param_title'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edDisplayName: TDBEditEh [2]
    Left = 154
    Top = 377
    Width = 231
    Height = 24
    ControlLabel.Width = 180
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1086#1083#1077' '#1076#1083#1103' '#1087#1086#1082#1072#1079#1072' '#1074' '#1089#1087#1080#1089#1082#1077
    ControlLabel.Visible = True
    DataField = 'param_display'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 9
    Visible = True
  end
  object edLen: TDBNumberEditEh [3]
    Left = 194
    Top = 135
    Width = 121
    Height = 24
    ControlLabel.Width = 42
    ControlLabel.Height = 16
    ControlLabel.Caption = #1044#1083#1080#1085#1072
    ControlLabel.Visible = True
    DataField = 'param_field_length'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    TabOrder = 5
    Visible = True
  end
  object edDefaultExpression: TDBEditEh [4]
    Left = 10
    Top = 438
    Width = 519
    Height = 24
    ControlLabel.Width = 214
    ControlLabel.Height = 13
    ControlLabel.Caption = #1047#1085#1072#1095#1077#1085#1080#1077' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102' (SQL '#1074#1099#1088#1072#1078#1077#1085#1080#1077')'
    ControlLabel.Visible = True
    DataField = 'param_default_expression'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 10
    Visible = True
  end
  object meSQL: TDBMemo [5]
    Left = 10
    Top = 194
    Width = 519
    Height = 151
    Anchors = [akLeft, akTop, akRight]
    Ctl3D = True
    DataField = 'param_sql'
    DataSource = dsLocal
    ParentCtl3D = False
    TabOrder = 7
  end
  object edKeyName: TDBEditEh [6]
    Left = 10
    Top = 377
    Width = 127
    Height = 24
    ControlLabel.Width = 104
    ControlLabel.Height = 16
    ControlLabel.Caption = #1050#1083#1102#1095#1077#1074#1086#1077' '#1087#1086#1083#1077
    ControlLabel.Visible = True
    DataField = 'param_key'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 8
    Visible = True
  end
  object cbType: TDBComboBoxEh [7]
    Left = 10
    Top = 135
    Width = 169
    Height = 24
    ControlLabel.Width = 25
    ControlLabel.Height = 16
    ControlLabel.Caption = #1058#1080#1087
    ControlLabel.Visible = True
    DataField = 'param_type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      #1057#1090#1088#1086#1082#1072
      #1063#1080#1089#1083#1086
      #1044#1072#1090#1072
      #1057#1087#1080#1089#1086#1082)
    KeyItems.Strings = (
      '1'
      '2'
      '3'
      '4')
    TabOrder = 4
    Visible = True
  end
  object neOrder: TDBNumberEditEh [8]
    Left = 194
    Top = 25
    Width = 121
    Height = 24
    ControlLabel.Width = 44
    ControlLabel.Height = 13
    ControlLabel.Caption = #1055#1086#1088#1103#1076#1086#1082
    ControlLabel.Visible = True
    DataField = 'param_order'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object cbObligatory: TDBCheckBoxEh [9]
    Left = 342
    Top = 25
    Width = 137
    Height = 17
    Caption = #1054#1073#1103#1079#1072#1090#1077#1083#1100#1085#1099#1081
    DataField = 'isobligatory'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 3
  end
  object edName: TDBEditEh [10]
    Left = 10
    Top = 25
    Width = 169
    Height = 24
    ControlLabel.Width = 76
    ControlLabel.Height = 13
    ControlLabel.Caption = #1048#1084#1103' '#1087#1072#1088#1072#1084#1077#1090#1088#1072
    ControlLabel.Visible = True
    DataField = 'param_name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Enabled = False
    ReadOnly = True
    TabOrder = 0
    Visible = True
  end
  inherited plBottom: TPanel
    Top = 475
    Width = 542
    TabOrder = 11
    ExplicitTop = 475
    ExplicitWidth = 542
    inherited btnCancel: TButton
      Left = 426
      ExplicitLeft = 426
    end
    inherited btnOk: TButton
      Left = 307
      ExplicitLeft = 307
    end
  end
  object edDecimalLen: TDBNumberEditEh [12]
    Left = 328
    Top = 135
    Width = 121
    Height = 24
    ControlLabel.Width = 133
    ControlLabel.Height = 16
    ControlLabel.Caption = #1044#1077#1089#1103#1090#1080#1095#1085#1099#1093' '#1079#1085#1072#1082#1086#1074
    ControlLabel.Visible = True
    DataField = 'param_decimal_length'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    TabOrder = 6
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 104
    Top = 472
  end
  inherited qrAux: TADOQuery
    Left = 48
    Top = 472
  end
end
