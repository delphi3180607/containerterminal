﻿inherited FormEditDocType: TFormEditDocType
  Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 410
  ClientWidth = 529
  ExplicitWidth = 535
  ExplicitHeight = 438
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 10
    Top = 9
    Width = 24
    Height = 16
    Caption = #1050#1086#1076
  end
  object Label3: TLabel [1]
    Left = 10
    Top = 61
    Width = 98
    Height = 16
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  object Label1: TLabel [2]
    Left = 10
    Top = 120
    Width = 159
    Height = 16
    Caption = #1055#1088#1086#1094#1077#1076#1091#1088#1072' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1103
  end
  object Label4: TLabel [3]
    Left = 10
    Top = 181
    Width = 126
    Height = 16
    Caption = #1057#1080#1089#1090#1077#1084#1085#1099#1081' '#1088#1072#1079#1076#1077#1083
  end
  object Label5: TLabel [4]
    Left = 10
    Top = 243
    Width = 131
    Height = 16
    Caption = #1043#1083#1086#1073#1072#1083#1100#1085#1099#1081' '#1088#1072#1079#1076#1077#1083
  end
  object Label16: TLabel [5]
    Left = 10
    Top = 308
    Width = 166
    Height = 16
    Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080'/'#1054#1087#1077#1088#1072#1094#1080#1103
  end
  inherited plBottom: TPanel
    Top = 369
    Width = 529
    TabOrder = 7
    ExplicitTop = 369
    ExplicitWidth = 529
    inherited btnCancel: TButton
      Left = 413
      ExplicitLeft = 413
    end
    inherited btnOk: TButton
      Left = 294
      ExplicitLeft = 294
    end
  end
  object edCode: TDBEditEh [7]
    Left = 10
    Top = 27
    Width = 305
    Height = 24
    DataField = 'code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edName: TDBEditEh [8]
    Left = 10
    Top = 83
    Width = 505
    Height = 24
    DataField = 'name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object edProcedure: TDBEditEh [9]
    Left = 10
    Top = 142
    Width = 505
    Height = 24
    DataField = 'confirm_procedure'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object cbSection: TDBComboBoxEh [10]
    Left = 10
    Top = 203
    Width = 505
    Height = 24
    DataField = 'system_section'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Items.Strings = (
      'income_sheet'
      'income_unload'
      'income_self_removal'
      'income_car_removal'
      'income_extra_service'
      'income_empty'
      'outcome_apps'
      'outcome_empty'
      'outcome_load'
      'outcome_dispatch'
      '')
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object edGlobalSection: TDBEditEh [11]
    Left = 10
    Top = 265
    Width = 505
    Height = 24
    DataField = 'global_section'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = True
  end
  object laDlvTypes: TDBSQLLookUp [12]
    Left = 10
    Top = 330
    Width = 255
    Height = 22
    DataField = 'dlv_type_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 5
    Visible = True
    SqlSet = ssDlvTypes
  end
  object cbAvoidUpdate: TDBCheckBoxEh [13]
    Left = 281
    Top = 333
    Width = 234
    Height = 17
    Caption = #1048#1079#1073#1077#1075#1072#1090#1100' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103' '#1086#1073#1098#1077#1082#1090#1072
    DataField = 'avoid_object_update'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 6
  end
  inherited dsLocal: TDataSource
    Left = 339
    Top = 19
  end
  inherited qrAux: TADOQuery
    Left = 395
    Top = 20
  end
  object ssDlvTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from deliverytypes order by code')
    DownSql.Strings = (
      'select * from deliverytypes order by code')
    InitSql.Strings = (
      'select * from deliverytypes where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 114
    Top = 324
  end
end
