﻿unit unloadfronts;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls;

type
  TFormUnloadFronts = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormUnloadFronts: TFormUnloadFronts;

implementation

{$R *.dfm}

uses editcodename;


procedure TFormUnloadFronts.Init;
begin
  self.formEdit := FormEditCodeName;
  inherited;
end;

end.
