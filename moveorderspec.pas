﻿unit MoveOrderSpec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Data.DB, Data.Win.ADODB, Vcl.StdCtrls, Vcl.ExtCtrls, MemTableDataEh,
  MemTableEh, orders, functions;

type
  TFormMoveOrderSpec = class(TFormEdit)
    dgData: TDBGridEh;
    meData: TMemTableEh;
    dsData: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    orderform: TFormDocOrders;
    orderid: integer;
    procedure FillSelectListAll(g: string; defaultid:string = 'id');
  end;

var
  FormMoveOrderSpec: TFormMoveOrderSpec;

implementation

{$R *.dfm}

uses dmu;

procedure TFormMoveOrderSpec.btnCancelClick(Sender: TObject);
begin
  self.Close;
end;

procedure TFormMoveOrderSpec.btnOkClick(Sender: TObject);
var gu: TGuid; g: string;
begin

  if orderform.meData.FieldByName('id').AsInteger  = self.orderid then
  begin
    ShowMessage('Выберите ДРУГУЮ заявку, а не ту же самую.');
    exit;
  end;


  if fQYN('Вы действительно хотите переместить выбранные записи в указанную заявку?') then
  begin

    CreateGUID(gu);
    g := GuidToString(gu);

    dgData.Selection.SelectAll;

    FillSelectListAll(g);

    dm.spMoveToOrder.Parameters.ParamByName('@selectguid').Value := g;
    dm.spMoveToOrder.Parameters.ParamByName('@docid').Value := orderform.meData.FieldByName('id').AsInteger;
    dm.spMoveToOrder.ExecProc;

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    orderform.meSpec.Close;
    orderform.meSpec.Open;

    self.Hide;
    ShowMessage('Готово!');

  end;

end;


procedure TFormMoveOrderSpec.FillSelectListAll(g: string; defaultid:string = 'id');
var i: integer; e: integer;
begin

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    if self.dgData.Selection.SelectionType = (gstAll) then
    begin

      self.meData.First;
      while not self.meData.Eof do
      begin

        dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := self.meData.FieldByName(defaultid).AsInteger;
        dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
        dm.spAdd2SelectList.ExecProc;

        self.meData.Next;

      end;

    end;

end;


end.
