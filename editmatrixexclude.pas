﻿unit EditMatrixExclude;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEditMatrixExclude = class(TFormEdit)
    edRowStart: TDBNumberEditEh;
    edStackStart: TDBNumberEditEh;
    edLevels: TDBNumberEditEh;
    edStackEnd: TDBNumberEditEh;
    edRowEnd: TDBNumberEditEh;
    edCells: TDBNumberEditEh;
    cbLimitType: TDBComboBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditMatrixExclude: TFormEditMatrixExclude;

implementation

{$R *.dfm}

end.
