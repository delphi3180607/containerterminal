﻿unit SPTypes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBCtrlsEh, DBSQLLookUp, Vcl.Mask, Vcl.DBCtrls;

type
  TFormEditSPTypes = class(TFormEdit)
    leContainerKind: TDBSQLLookUp;
    Label3: TLabel;
    ssContainerKind: TADOLookUpSqlSet;
    Label2: TLabel;
    cbTypes: TDBComboBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditSPTypes: TFormEditSPTypes;

implementation

uses StorePrices;

{$R *.dfm}

end.
