﻿inherited FormCargoDocs: TFormCargoDocs
  ActiveControl = dgData
  Caption = 'FormCargoDocs'
  ClientHeight = 611
  ClientWidth = 828
  ExplicitWidth = 844
  ExplicitHeight = 649
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 370
    Width = 828
    Height = 6
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 371
  end
  inherited plBottom: TPanel
    Top = 570
    Width = 828
    ExplicitTop = 570
    ExplicitWidth = 828
    inherited btnOk: TButton
      Left = 593
      ExplicitLeft = 593
    end
    inherited btnCancel: TButton
      Left = 712
      ExplicitLeft = 712
    end
  end
  inherited plAll: TPanel
    Width = 828
    Height = 370
    ExplicitWidth = 828
    ExplicitHeight = 370
    inherited plTop: TPanel
      Width = 828
      ExplicitWidth = 828
      inherited btTool: TPngSpeedButton
        Left = 776
        ExplicitLeft = 776
      end
      object Bevel1: TBevel
        AlignWithMargins = True
        Left = 205
        Top = 0
        Width = 1
        Height = 43
        Margins.Top = 0
        Margins.Right = 10
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 251
        ExplicitHeight = 38
      end
      object btFlow: TPngSpeedButton
        AlignWithMargins = True
        Left = 219
        Top = 3
        Width = 33
        Height = 37
        Hint = #1048#1079#1084#1077#1085#1080#1090#1100' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000014000000140803000000BA57ED
          3F0000000373424954080808DBE14FE00000008D504C5445333333EDEDED0E6E
          FF94949493BEFF7DB1FF5555555B9DFFB3D1FFBFBFBF4B93FF727272B6B6B6FF
          FFFFE1E1E1373737DFECFF72AAFF89B8FFA5C9FFD7E7FF277DFF666666F0F6FF
          CCCCCC41414184B5FF66A3FF808080F9F9F99DC4FF999999ADCEFF1A76FF4D4D
          4D6969695A5A5A7F7F7FE6E6E6F5F9FF6AA6FF7BB0FFE7F1FF60A0FF5197FFA7
          CAFFDCEAFFC1A5875E0000002F74524E53FFFFFFFFFFFFFFFFFFFFFFFFFF00FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          79886839000000097048597300000B1200000B1201D2DD7EFC0000001C744558
          74536F6674776172650041646F62652046697265776F726B732043533571B5E3
          3600000016744558744372656174696F6E2054696D650031312F32342F313706
          DD8C9F000000C44944415478DA65D05B0B82401005E0335A9AB11B956844B787
          D0A2FFFF637A91A2D08AA0404DBB6A1A58B99EA7E17BD89D39C481D1C1479154
          8F00E28813DBF9226BBFCE19C6F2748B5FE4E152A3C572FE6F40F368916EB828
          67EC5013723E25835D811CD4C5239F024C36AC828C23201179DB6346781130DB
          D970ACBD80A1646ABB5A0947E7BBAE9C8437CD1B7799F891295D2A7BA6F4BDE8
          0A1A074FE1CC569D7A47232A994A525EDD6CF56F81BDCE4AEEC44A7A2DAC1F7D
          4ACE1AD4FC46818AEA016F9A6546B8FD251BD30000000049454E44AE426082}
        ExplicitLeft = 288
        ExplicitHeight = 29
      end
      object sbHistory: TPngSpeedButton
        AlignWithMargins = True
        Left = 258
        Top = 3
        Width = 46
        Height = 37
        Hint = #1048#1089#1090#1086#1088#1080#1103' '#1089#1086#1089#1090#1086#1103#1085#1080#1081
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        PngImage.Data = {
          89504E470D0A1A0A0000000D494844520000001800000014080600000097B5FD
          830000000473424954080808087C086488000000097048597300000B1300000B
          1301009A9C18000003F34944415478DAA5956D6C536514C79FF3F4DE0DA4DDB2
          76CB08B260A22E83304112133FF84622C60F1AF9A08042547053E9CBEDEEAD33
          5BF7D68EB1CD8CD1AEDD5D6301F9408C614B4CF8608CE23EA88BFA69892FDB18
          21464780896C0DC8B6BEDDE7785AB725CEEA8A9CA469FB3CCF39E7B9FFF33BE7
          02CBD354555D9B32583B03F6637F5FE00C2D613E7E90CF19455177533427438C
          20B00A60B04B706CD683C1D1BB4AE0746A5500D88580A3B2098E05028185CCBA
          A228E588FC28254BA2916AD1757DE68E12B85CAE2204A98536EF35D2BC7160A0
          F7D7DCE7D447E9CB4761CE4D4F5F8E0E0D0D19AB250097ABEE00227B8D73DE15
          0A1D1F5E4D82B6B6363E3B7BF310FDDC03C0DBC967246702475DDD0E2EA00319
          9E4FC6E7FBA3D1682A8FFA2CDBE1C30D25921CF7A1806244C9ABEB3D57B30934
          4D2B4DA5F00841210388A65028F4DB9D045E690E875ACD397652E811ABB52800
          4E45F530211A18E34FF7F707BEBF9BE04BAA38DCDAF31CF11C0A7C222BD1EB5A
          534542BE67D09CF8E3A219122AD132FBFF6EAF552E14AE3DB9605A77D972F397
          5A92791EACFAF83308D0451245F65E3C3D29137602D8C7D7AF5D39918B8A5C56
          5F5F6F89C7D3CD84ED7D37D654B49FDFF4EC2BF4203B391AAF824D9FD8490DD4
          CD004ECF94559D7869680F2B2FDF58039CBD480DE55F49C54A3948E2FD24C341
          8AF15E381CFCDC7672C282096C21581E119C1FCC4A74C8D3B4E952F1E6B317AC
          DB9229267B63CEAA111A0DD6CC6800448B1072E31215CB72107520F028200C5B
          ADC521BFCF97B20D4CEE2F48DD7EA77A7674A27A6EB2A6B7B7778E6E50F72E65
          6910063C7966FB1B57E4A4C94F53C6421FEF8C6BF355B7DBBD5D20EFA4FF5F32
          960ECAB26CC95207B8C6C49937180C5E2B0D8FEF20793A48DA2F62D27C5819FB
          70178D964FC8E72958BCA98F0956B4C46F5964729B21442730FCAA242D052FB9
          2B930E457D99CE3472CEAE33849670F8F837E6F72F941618E208202BE0289A7E
          776E9D76B9B487118D0E00D37022713BBCDC68345F1E5A9C2F5FDB4A8A839947
          2ED527F6191C6B1933F5C4EC559F7A3C9E75535353F1A12D76B09597BFC504EC
          1612B6C6DEDEF2ADC3E1B0712EB553590A0D436A1E1838369D2DD2CAA2D1E4DC
          2B04D6A0097BF4BEBECFCAFA7F321BDCD40C281E20B61B90C10683F356136383
          374AAB4E65A058BFBEE24DA2F005F2F785C381EFFE1630171A76BBDDCCA5022F
          6DDF2F716C249D7F269D2B8509A2B43D96968D965BB55B6715457B0C51B4D2DA
          2015FA03BFDF2FFE81D97FF14DB23D48B2518FC0782231D79D699CBF1AAA7E03
          40BA937176CB4815B64522DDB17FE538AF0E55B4E7384395AE778A0ABF918AFA
          F8E2DCFA6135DFBC12648CDE11858C49AD84E2981E0A7CC4F27C65FE0929FCCA
          2B22AF3AD00000000049454E44AE426082}
        ExplicitLeft = 381
        ExplicitTop = 2
        ExplicitHeight = 41
      end
      object Bevel3: TBevel
        AlignWithMargins = True
        Left = 310
        Top = 0
        Width = 1
        Height = 43
        Margins.Top = 0
        Margins.Right = 10
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 341
        ExplicitTop = 3
      end
    end
    inherited dgData: TDBGridEh
      Width = 828
      Height = 327
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1086#1087#1077#1088#1072#1094#1080#1080
          Width = 123
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 100
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 77
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 78
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          Width = 129
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 142
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
          Width = 161
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1057#1091#1084#1084#1072
          Width = 113
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1087#1083#1072#1085
          Width = 83
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1092#1072#1082#1090
          Width = 81
        end>
    end
  end
  object DBGridEh1: TDBGridEh [3]
    Left = 0
    Top = 376
    Width = 828
    Height = 194
    Align = alBottom
    AutoFitColWidths = True
    BorderStyle = bsNone
    DynProps = <>
    TabOrder = 2
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -13
    TitleParams.Font.Name = 'Verdana'
    TitleParams.Font.Style = [fsBold]
    TitleParams.ParentFont = False
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        Footers = <>
        Title.Caption = #1057#1086#1089#1090#1072#1074
        Width = 299
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        Footers = <>
        Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        Width = 110
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        Footers = <>
        Title.Caption = #1062#1077#1085#1072
        Width = 94
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        Footers = <>
        Title.Caption = #1057#1091#1084#1084#1072
        Width = 102
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
end
