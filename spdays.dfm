﻿inherited FormEditSPDays: TFormEditSPDays
  Caption = #1044#1085#1080' '#1093#1088#1072#1085#1077#1085#1080#1103
  ClientHeight = 203
  ClientWidth = 270
  ExplicitWidth = 276
  ExplicitHeight = 231
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 8
    Top = 76
    Width = 34
    Height = 16
    Caption = #1062#1077#1085#1072
  end
  object Label2: TLabel [1]
    Left = 8
    Top = 13
    Width = 94
    Height = 16
    Caption = #1044#1085#1080' '#1093#1088#1072#1085#1077#1085#1080#1103
  end
  inherited plBottom: TPanel
    Top = 162
    Width = 270
    TabOrder = 2
    inherited btnCancel: TButton
      Left = 154
    end
    inherited btnOk: TButton
      Left = 35
    end
  end
  object nePrice: TDBNumberEditEh [3]
    Left = 8
    Top = 98
    Width = 121
    Height = 24
    DataField = 'price'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object neDays: TDBNumberEditEh [4]
    Left = 8
    Top = 35
    Width = 121
    Height = 24
    DataField = 'days'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 232
    Top = 112
  end
  inherited qrAux: TADOQuery
    Left = 184
    Top = 112
  end
end
