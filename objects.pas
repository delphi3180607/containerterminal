﻿unit objects;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  Vcl.ExtCtrls, MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Vcl.Mask, DBCtrlsEh;

type
  TFormObjects = class(TFormGrid)
    plObjects: TPanel;
    Panel2: TPanel;
    sbAddObject: TPngSpeedButton;
    sbDeleteObject: TPngSpeedButton;
    sbEditObject: TPngSpeedButton;
    dgObjects: TDBGridEh;
    Panel1: TPanel;
    dgStates: TDBGridEh;
    Panel3: TPanel;
    DBGridEh1: TDBGridEh;
    Splitter2: TSplitter;
    Splitter1: TSplitter;
    Splitter3: TSplitter;
    drvState: TADODataDriverEh;
    meStates: TMemTableEh;
    dsStates: TDataSource;
    drvOwner: TADODataDriverEh;
    meOwner: TMemTableEh;
    dsOwner: TDataSource;
    drvLinkedObjects: TADODataDriverEh;
    meLinkedObjects: TMemTableEh;
    dsLinkedObjects: TDataSource;
    pmOwners: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    sbClearFilter: TPngSpeedButton;
    plStatus: TPanel;
    procedure sbAddObjectClick(Sender: TObject);
    procedure sbDeleteObjectClick(Sender: TObject);
    procedure sbEditObjectClick(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure meDataAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure meOwnerAfterPost(DataSet: TDataSet);
    procedure meStatesAfterScroll(DataSet: TDataSet);
    procedure btFilterClick(Sender: TObject);
    procedure sbClearFilterClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure dgDataApplyFilter(Sender: TObject);
  private
    var g: string;
    gu: TGuid;
  public
    procedure Init; override;
    procedure PartialInit; override;
    procedure SetFilter(conditions: string);
  end;

var
  FormObjects: TFormObjects;

implementation

{$R *.dfm}

uses docgenerate, functions, editowner, filterlite, dmu;


procedure TFormObjects.btFilterClick(Sender: TObject);
begin
  FormFilterLite.ShowModal;
  if FormFilterLite.ModalResult in [mrOk, mrYes] then
  begin
    SetFilter('');
    CreateGuid(gu);
    g := GuidToString(gu);
    SetFilter(FormFilterLite.conditions);
  end;
end;



procedure TFormObjects.SetFilter(conditions: string);
begin

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := conditions;
    dm.spCreateFilter.ExecProc;

    meData.Close;
    meData.Open;

    if conditions = '' then
    begin
      plStatus.Caption := ' Фильтр снят.';
      g := '';
    end else
    begin
      plStatus.Caption := ' Установлен фильтр по списку номеров.';
    end;

end;


procedure TFormObjects.dgDataApplyFilter(Sender: TObject);
begin
  inherited;
  dgData.DefaultApplyFilter;
  meDataAfterScroll(meData);
end;

procedure TFormObjects.FormActivate(Sender: TObject);
begin
  inherited;
  meData.Close;
  meData.Open;
end;

procedure TFormObjects.Init;
begin
  inherited;
  tablename := 'objects';
  self.meOwner.Open;
  self.meStates.Open;
  self.meLinkedObjects.Open;
end;

procedure TFormObjects.PartialInit;
begin
  inherited;
  //FullRefresh(meData);
end;


procedure TFormObjects.meDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  meOwner.Close;
  drvOwner.SelectCommand.Parameters.ParamByName('object_id').Value := meData.FieldByName('id').AsInteger;
  meOwner.Open;
  meStates.Close;
  drvState.SelectCommand.Parameters.ParamByName('object_id').Value := meData.FieldByName('id').AsInteger;
  meStates.Open;
  meLinkedObjects.Close;
  drvLinkedObjects.SelectCommand.Parameters.ParamByName('object_id').Value := meStates.FieldByName('object_id').AsInteger;
  drvLinkedObjects.SelectCommand.Parameters.ParamByName('doc_id').Value := meStates.FieldByName('doc_id').AsInteger;
  meLinkedObjects.Open;
end;

procedure TFormObjects.meDataAfterScroll(DataSet: TDataSet);
begin
  meStates.Close;
  drvState.SelectCommand.Parameters.ParamByName('object_id').Value := meData.FieldByName('id').AsInteger;
  meStates.Open;
  meOwner.Close;
  drvOwner.SelectCommand.Parameters.ParamByName('object_id').Value := meData.FieldByName('id').AsInteger;
  meOwner.Open;
end;

procedure TFormObjects.meDataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  if Assigned(drvData.SelectCommand.Parameters.FindParam('guid')) then
      drvData.SelectCommand.Parameters.ParamByName('guid').Value := g;
end;

procedure TFormObjects.MenuItem8Click(Sender: TObject);
begin
  meOwner.Close;
  meOwner.Open;
end;

procedure TFormObjects.meOwnerAfterPost(DataSet: TDataSet);
begin
  inherited;
  drvData.GetrecCommand.Parameters.ParamByName('current_id').Value := meData.FieldByName('id').AsInteger;
  meData.RefreshRecord;
end;

procedure TFormObjects.meStatesAfterScroll(DataSet: TDataSet);
begin
  meLinkedObjects.Close;
  drvLinkedObjects.SelectCommand.Parameters.ParamByName('object_id').Value := meStates.FieldByName('object_id').AsInteger;
  drvLinkedObjects.SelectCommand.Parameters.ParamByName('doc_id').Value := meStates.FieldByName('doc_id').AsInteger;
  meLinkedObjects.Open;
end;

procedure TFormObjects.sbAddObjectClick(Sender: TObject);
begin
  EA(self, FormEditOwner, self.meOwner, 'objectownerhistory', 'object_id', self.meData.FieldByName('id').AsInteger);
end;

procedure TFormObjects.sbClearFilterClick(Sender: TObject);
begin
  SetFilter('');
end;

procedure TFormObjects.sbDeleteObjectClick(Sender: TObject);
begin
  ED(self.meOwner);
end;

procedure TFormObjects.sbEditObjectClick(Sender: TObject);
begin
  EE(self, FormEditOwner, self.meOwner, 'objectownerhistory');
end;

end.
