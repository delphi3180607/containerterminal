﻿unit seek;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, Data.DB, Data.Win.ADODB, functions;

type
  TFormSeek = class(TForm)
    edSeek: TEdit;
    dgSeek: TDBGridEh;
    qrAux: TADOQuery;
    dsAux: TDataSource;
    procedure edSeekChange(Sender: TObject);
    procedure dgSeekKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edSeekKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure dgSeekDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure InitList(text: string);
    function ProcessOdds: boolean;
  end;

var
  FormSeek: TFormSeek;

implementation

{$R *.dfm}

uses dmu, counteragents;


procedure TFormSeek.dgSeekDblClick(Sender: TObject);
begin
  self.ModalResult := mrOk;
end;

procedure TFormSeek.dgSeekKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then self.ModalResult := mrOk;
  if Key = 27 then self.ModalResult := mrCancel;
end;

procedure TFormSeek.edSeekChange(Sender: TObject);
begin
  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select * from counteragents');
  qrAux.SQL.Add('where code like ''%'+trim(self.edSeek.Text)+'%''');
  qrAux.SQL.Add('or name like ''%'+trim(self.edSeek.Text)+'%''');
  qrAux.Open;

end;

procedure TFormSeek.edSeekKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
begin
  if Key = 13 then self.ModalResult := mrOk;
  if Key = 27 then self.ModalResult := mrCancel;
  if Key = VK_DOWN then dgSeek.SetFocus;
end;

function TFormSeek.ProcessOdds: boolean;
begin

  result := false;

  if self.edSeek.Text = '' then
  begin
    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select 0 as id');
    qrAux.Open;
    result := true;
    exit;
  end;

  if qrAux.RecordCount=1 then
    result := true;

  if qrAux.RecordCount=0 then
  if fQYN('Такой контрагент не найден. Добавить контрагента?') then
  begin

    FormCounteragents.meData.Append;
    FormCounteragents.meData.FieldByName('code').Value := self.edSeek.Text;
    FormCounteragents.meData.FieldByName('name').Value := self.edSeek.Text;
    FormCounteragents.meData.Post;
    FormCounteragents.sbEditClick(nil);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select * from counteragents');
    qrAux.SQL.Add('where code = '''+FormCounteragents.meData.FieldByName('code').AsString+'''');
    qrAux.Open;
    result := true;

  end;

end;



procedure TFormSeek.InitList(text: string);
begin
  self.edSeek.Text := text;
  edSeekChange(self);
end;


end.
