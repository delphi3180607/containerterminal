﻿inherited FormEditCarriage: TFormEditCarriage
  Caption = #1042#1072#1075#1086#1085
  ClientHeight = 355
  ClientWidth = 515
  Font.Height = -12
  ExplicitWidth = 521
  ExplicitHeight = 383
  PixelsPerInch = 96
  TextHeight = 14
  object Label2: TLabel [0]
    Left = 5
    Top = 6
    Width = 41
    Height = 14
    Caption = #1053#1086#1084#1077#1088
  end
  object Label4: TLabel [1]
    Left = 5
    Top = 200
    Width = 81
    Height = 14
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
  end
  object Label3: TLabel [2]
    Left = 5
    Top = 51
    Width = 72
    Height = 14
    Caption = #1058#1080#1087' '#1074#1072#1075#1086#1085#1072
  end
  object Label5: TLabel [3]
    Left = 5
    Top = 102
    Width = 92
    Height = 14
    Caption = #1058#1077#1093'.'#1089#1086#1089#1090#1086#1103#1085#1080#1077
  end
  object Label1: TLabel [4]
    Left = 261
    Top = 51
    Width = 48
    Height = 14
    Caption = #1052#1086#1076#1077#1083#1100
  end
  inherited plBottom: TPanel
    Top = 314
    Width = 515
    TabOrder = 7
    ExplicitTop = 279
    ExplicitWidth = 515
    inherited btnCancel: TButton
      Left = 399
      ExplicitLeft = 399
    end
    inherited btnOk: TButton
      Left = 280
      ExplicitLeft = 280
    end
  end
  object edCNum: TDBEditEh [6]
    Left = 5
    Top = 24
    Width = 174
    Height = 22
    DataField = 'pnum'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    MaxLength = 8
    ParentFont = False
    TabOrder = 0
    Visible = True
    EditMask = '99999999;0;?'
  end
  object edNote: TDBMemoEh [7]
    Left = 5
    Top = 218
    Width = 498
    Height = 90
    ScrollBars = ssVertical
    AutoSize = False
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    Visible = True
    WantReturns = True
  end
  object leContainerKind: TDBSQLLookUp [8]
    Left = 5
    Top = 71
    Width = 247
    Height = 22
    DataField = 'kind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
    SqlSet = ssCarriageKind
    RowCount = 0
  end
  object cbTechCondition: TDBLookupComboboxEh [9]
    Left = 5
    Top = 122
    Width = 247
    Height = 22
    DynProps = <>
    DataField = 'techcond_id'
    DataSource = dsLocal
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    KeyField = 'id'
    ListField = 'code'
    ListSource = FormTechConditions.dsData
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object cbDefective: TDBCheckBoxEh [10]
    Left = 5
    Top = 164
    Width = 136
    Height = 17
    Caption = #1053#1077#1080#1089#1087#1088#1072#1074#1077#1085
    DataField = 'isdefective'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 4
  end
  object luModel: TDBSQLLookUp [11]
    Left = 261
    Top = 71
    Width = 242
    Height = 22
    DataField = 'model_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    Visible = True
    SqlSet = ssModels
    RowCount = 0
  end
  object cbInspection: TDBCheckBoxEh [12]
    Left = 143
    Top = 164
    Width = 157
    Height = 17
    Caption = #1058#1088#1077#1073#1091#1077#1090#1089#1103' '#1086#1089#1084#1086#1090#1088
    DataField = 'need_inspection'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 5
  end
  object edDateNextRepair: TDBDateTimeEditEh [13]
    Left = 261
    Top = 122
    Width = 156
    Height = 22
    ControlLabel.Width = 147
    ControlLabel.Height = 14
    ControlLabel.Caption = #1044#1072#1090#1072' '#1086#1095#1077#1088#1077#1076'. '#1088#1077#1084#1086#1085#1090#1072
    ControlLabel.Visible = True
    DataField = 'date_next_repair'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 278
    Top = 253
  end
  inherited qrAux: TADOQuery
    Left = 230
    Top = 253
  end
  object ssCarriageKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from carriagekinds order by code')
    DownSql.Strings = (
      'select * from carriagekinds order by code')
    InitSql.Strings = (
      'select * from carriagekinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 182
    Top = 52
  end
  object ssModels: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from carriagemodels order by model_code')
    DownSql.Strings = (
      'select * from carriagemodels order by model_code')
    InitSql.Strings = (
      'select * from carriagemodels where id = @id')
    KeyName = 'id'
    DisplayName = 'model_code'
    Connection = dm.connMain
    Left = 438
    Top = 52
  end
end
