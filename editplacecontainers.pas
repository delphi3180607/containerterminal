﻿unit EditPlaceContainers;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBCtrlsEh, Vcl.StdCtrls, Vcl.Mask,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.DBCGrids, MemTableDataEh, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.DBCtrls, DBSQLLookUp, Functions, grid;

type
  TFormEditPlaceContainers = class(TFormEdit)
    plCarriage: TPanel;
    edCarModel: TDBEditEh;
    edCarType: TDBEditEh;
    edCarNum: TDBEditEh;
    edOwner: TDBEditEh;
    plTop: TPanel;
    sbAdd: TPngSpeedButton;
    sbDelete: TPngSpeedButton;
    dgSpec: TDBCtrlGrid;
    plContainer: TPanel;
    dsSpec: TDataSource;
    meSpec: TMemTableEh;
    drvSpec: TADODataDriverEh;
    plUpDown: TPanel;
    plFields1: TPanel;
    edContainerNum: TDBEdit;
    plFields2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    Label3: TLabel;
    edOwnerCode: TDBEdit;
    Panel2: TPanel;
    Label4: TLabel;
    edKindCode: TDBEdit;
    sbUp: TPngSpeedButton;
    sbDown: TPngSpeedButton;
    edTaskId: TDBEdit;
    Panel3: TPanel;
    cbEmpty: TDBCheckBox;
    Panel4: TPanel;
    Label5: TLabel;
    edStation: TDBEdit;
    Panel5: TPanel;
    Label6: TLabel;
    edOrder: TDBEdit;
    plRight: TPanel;
    plSelectOwner: TPanel;
    plSelectKind: TPanel;
    plSelectCont: TPanel;
    plOrder: TPanel;
    edNote: TDBEdit;
    procedure sbAddClick(Sender: TObject);
    procedure meSpecAfterInsert(DataSet: TDataSet);
    procedure plSelectContMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure plSelectContMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnOkClick(Sender: TObject);
    procedure plSelectContClick(Sender: TObject);
    procedure meSpecAfterPost(DataSet: TDataSet);
    procedure sbDeleteClick(Sender: TObject);
    procedure plSelectKindClick(Sender: TObject);
    procedure plSelectOwnerClick(Sender: TObject);
    procedure cbEmptyMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure sbUpClick(Sender: TObject);
    procedure sbDownClick(Sender: TObject);
    procedure meSpecAfterOpen(DataSet: TDataSet);
    procedure dgSpecPaintPanel(DBCtrlGrid: TDBCtrlGrid; Index: Integer);
    procedure plOrderClick(Sender: TObject);
  private
    fGridByNumber: TForm;
  public
    joint_id: integer;
  end;

var
  FormEditPlaceContainers: TFormEditPlaceContainers;

implementation

{$R *.dfm}

uses containers, containerkinds, counteragents, ContainersOnGo,
  ContainersInOrders;

procedure TFormEditPlaceContainers.btnOkClick(Sender: TObject);
begin
  if meSpec.State = (dsInsert) then meSpec.Post;
  if meSpec.State = (dsEdit) then meSpec.Post;
end;

procedure TFormEditPlaceContainers.cbEmptyMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if meSpec.State = (dsInsert) then meSpec.Post;
  if meSpec.State = (dsEdit) then meSpec.Post;
  drvSpec.GetrecCommand.Parameters.ParamByName('current_id').Value := current_id;
  meSpec.RefreshRecord;
  meSpec.Locate('id', current_id, []);
end;

procedure TFormEditPlaceContainers.dgSpecPaintPanel(DBCtrlGrid: TDBCtrlGrid;
  Index: Integer);
begin
  if Index = DBCtrlGrid.PanelIndex then
  begin
    DBCtrlGrid.Canvas.Brush.Color:=$00E3E3DB;
    DBCtrlGrid.Canvas.FillRect(rect(0,0, DBCtrlGrid.PanelWidth, DBCtrlGrid.PanelHeight));
  end;
end;

procedure TFormEditPlaceContainers.meSpecAfterInsert(DataSet: TDataSet);
begin
  if meSpec.RecordCount>3 then
  begin
    meSpec.Cancel;
    exit;
  end;
  meSpec.FieldByName('joint_id').Value := self.joint_id;
  meSpec.FieldByName('isempty').Value := 0;
end;

procedure TFormEditPlaceContainers.meSpecAfterOpen(DataSet: TDataSet);
begin

  if meSpec.RecordCount=0 then
  begin
    meSpec.Append;
    meSpec.Post;
    meSpec.Close;
    meSpec.Open;
    current_id := meSpec.FieldByName('id').AsInteger;
  end;

end;

procedure TFormEditPlaceContainers.meSpecAfterPost(DataSet: TDataSet);
begin
  current_id := meSpec.FieldByName('id').AsInteger;
  if current_id<=0 then
  begin

     qrAux.Close;
     qrAux.SQL.Clear;
     qrAux.SQL.Add('select IDENT_CURRENT(''docload'')');
     qrAux.Open;
     current_id := qrAux.Fields[0].AsInteger;

  end;
end;

procedure TFormEditPlaceContainers.plOrderClick(Sender: TObject);
var pv: TPairValues; pp: TPairparam; fGrid: TForm;
begin

  fGrid := nil;

  if not cbEmpty.Checked then
  begin
    ShowMessage('Выбор из заявок возможен только для Порожних контейнеров.');
    exit;
  end;

  if meSpec.State = (dsInsert) then meSpec.Post;
  if meSpec.State = (dsEdit) then meSpec.Post;

  pp.paramname := '';

  pv := SFDV(FormContainersInOrders, 0, 'id', pp, fGrid);

  if pv.keyvalue>0 then
  begin

      if TFormGrid(fGrid).meData.FieldByName('station_id').AsInteger = 0 then
      begin
        ShowMessage('Не указана станция назначения для выбраной заявки.');
        exit;
      end;

      if meSpec.RecordCount>0 then meSpec.Edit
      else meSpec.Append;

      meSpec.FieldByName('orderspec_id').Value := pv.keyvalue;
      meSpec.FieldByName('isempty').Value := 1;
      meSpec.FieldByName('kind_id').Value := TFormGrid(fGrid).meData.FieldByName('container_kind_id').Value;
      meSpec.FieldByName('owner_id').Value := TFormGrid(fGrid).meData.FieldByName('container_owner_id').Value;
      meSpec.FieldByName('task_id').Value := null;
      meSpec.FieldByName('station_id').Value := TFormGrid(fGrid).meData.FieldByName('station_id').Value;
      meSpec.FieldByName('note').Value := TFormGrid(fGrid).meData.FieldByName('task_note').Value;

      try
        meSpec.Post;
      except
        on E : Exception do
        begin
          if meSpec.State = (dsEdit) then meSpec.Cancel;
          ShowMessage(E.Message);
          exit;
        end;
      end;

      drvSpec.GetrecCommand.Parameters.ParamByName('current_id').Value := current_id;
      meSpec.RefreshRecord;
      meSpec.Locate('id', current_id, []);

  end;

end;

procedure TFormEditPlaceContainers.plSelectContClick(Sender: TObject);
var pv: TPairValues; pp: TPairParam;
begin

  if meSpec.State = (dsInsert) then meSpec.Post;
  if meSpec.State = (dsEdit) then meSpec.Post;

  pp.paramname := 'sections';
  pp.paramvalue := ',output,';

  pv := SFDV(FormContainersOnGo, 0, 'cnum', pp, fGridByNumber);
  if pv.keyvalue>0 then
  begin

      if TFormGrid(fGridByNumber).meData.FieldByName('station_id').AsInteger = 0 then
      begin
        ShowMessage('Не указана станция назначения для выбраного контейнера.');
        exit;
      end;

      if meSpec.FieldByName('isempty').AsBoolean then
      if not TFormGrid(fGridByNumber).meData.FieldByName('isempty').AsBoolean then
      begin
        ShowMessage('Выбранный контейнер не отмечен как порожний, при установленной "галке" Порожний.');
        exit;
      end;

      if meSpec.RecordCount>0 then meSpec.Edit
      else meSpec.Append;

      meSpec.FieldByName('container_id').Value := pv.keyvalue;
      meSpec.FieldByName('orderspec_id').Value := TFormGrid(fGridByNumber).meData.FieldByName('orderspec_id').Value;
      meSpec.FieldByName('isempty').Value := TFormGrid(fGridByNumber).meData.FieldByName('isempty').Value;
      meSpec.FieldByName('kind_id').Value := TFormGrid(fGridByNumber).meData.FieldByName('kind_id').Value;
      meSpec.FieldByName('owner_id').Value := TFormGrid(fGridByNumber).meData.FieldByName('owner_id').Value;
      meSpec.FieldByName('task_id').Value := TFormGrid(fGridByNumber).meData.FieldByName('task_id').Value;
      meSpec.FieldByName('station_id').Value := TFormGrid(fGridByNumber).meData.FieldByName('station_id').Value;
      meSpec.FieldByName('note').Value := TFormGrid(fGridByNumber).meData.FieldByName('task_note').Value;

      try
        meSpec.Post;
      except
        on E : Exception do
        begin
          if meSpec.State = (dsEdit) then meSpec.Cancel;
          ShowMessage(E.Message);
          exit;
        end;
      end;

      drvSpec.GetrecCommand.Parameters.ParamByName('current_id').Value := current_id;
      meSpec.RefreshRecord;

      TFormGrid(fGridByNumber).meData.Refresh;

  end;

end;

procedure TFormEditPlaceContainers.plSelectContMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  TPanel(Sender).BevelOuter := bvLowered;
  TPanel(Sender).BorderStyle := bsSingle;
end;

procedure TFormEditPlaceContainers.plSelectContMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  TPanel(Sender).BevelOuter := bvRaised;
  TPanel(Sender).BorderStyle := bsNone;
end;

procedure TFormEditPlaceContainers.plSelectKindClick(Sender: TObject);
var pv: TPairValues; pp: TPairParam; fGrid: TForm;
begin

  fGrid := nil;

  if meSpec.FieldByName('container_id').AsInteger = 0 then
  begin

    pp.paramname := '';
    pv := SFDV(FormContainerKinds, 0, 'code', pp, fGrid);

    if pv.keyvalue>0 then
    begin
      if meSpec.State = (dsInsert) then meSpec.Post;
      if meSpec.State = (dsEdit) then meSpec.Post;
      if meSpec.RecordCount>0 then meSpec.Edit
      else meSpec.Append;
      meSpec.FieldByName('kind_id').Value := pv.keyvalue;
      meSpec.Post;
      drvSpec.GetrecCommand.Parameters.ParamByName('current_id').Value := current_id;
      meSpec.RefreshRecord;
    end;


  end else
  begin
    ShowMessage('Нельзя указать тип, если выбран контейнер из справочника');
  end;



end;

procedure TFormEditPlaceContainers.plSelectOwnerClick(Sender: TObject);
var pv: TPairValues; pp: TPairParam; fGrid: TForm;
begin

  fGrid := nil;

  if meSpec.FieldByName('container_id').AsInteger = 0 then
  begin

    pp.paramname := '';
    pv := SFDV(FormCounteragents, 0, 'code', pp, fGrid);

    if pv.keyvalue>0 then
    begin
      if meSpec.State = (dsInsert) then meSpec.Post;
      if meSpec.State = (dsEdit) then meSpec.Post;
      if meSpec.RecordCount>0 then meSpec.Edit
      else meSpec.Append;
      meSpec.FieldByName('owner_id').Value := pv.keyvalue;
      meSpec.Post;
      drvSpec.GetrecCommand.Parameters.ParamByName('current_id').Value := current_id;
      meSpec.RefreshRecord;
    end;

  end else
  begin
    ShowMessage('Нельзя указать собственника, если выбран контейнер из справочника');
  end;
end;

procedure TFormEditPlaceContainers.sbAddClick(Sender: TObject);
begin
  if meSpec.State = (dsInsert) then meSpec.Post;
  if meSpec.State = (dsEdit) then meSpec.Post;
  meSpec.Append;
  if meSpec.State = (dsInsert) then meSpec.Post;
  if meSpec.State = (dsEdit) then meSpec.Post;
  meSpec.DisableControls;
  meSpec.Close;
  meSpec.Open;
  meSpec.Last;
  meSpec.EnableControls;
end;

procedure TFormEditPlaceContainers.sbDeleteClick(Sender: TObject);
begin
  meSpec.Delete;
  //if meSpec.RecordCount>0 then meSpec.Edit
  //else meSpec.Append;
end;

procedure TFormEditPlaceContainers.sbDownClick(Sender: TObject);
begin
  if meSpec.RecordCount = 0 then exit;
  if meSpec.FieldByName('order_num').AsInteger>3 then exit;
  meSpec.Edit;
  //meSpec.FieldByName('temp_move_sign').Value := 1;
  meSpec.FieldByName('order_num').Value := meSpec.FieldByName('order_num').AsInteger+1;
  meSpec.Post;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormEditPlaceContainers.sbUpClick(Sender: TObject);
begin
  if meSpec.RecordCount = 0 then exit;
  if meSpec.FieldByName('order_num').AsInteger<2 then exit;
  meSpec.Edit;
  //meSpec.FieldByName('temp_move_sign').Value := -1;
  meSpec.FieldByName('order_num').Value := meSpec.FieldByName('order_num').AsInteger-1;
  meSpec.Post;
  meSpec.Close;
  meSpec.Open;
end;

end.
