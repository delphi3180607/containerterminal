﻿inherited FormAddPassSpec: TFormAddPassSpec
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074' '#1074' '#1084#1072#1090#1077#1088#1080#1072#1083#1100#1085#1099#1081' '#1087#1088#1086#1087#1091#1089#1082
  ClientHeight = 498
  ClientWidth = 1072
  ExplicitWidth = 1078
  ExplicitHeight = 526
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 457
    Width = 1072
    ExplicitTop = 457
    ExplicitWidth = 1072
    inherited btnCancel: TButton
      Left = 956
      ExplicitLeft = 956
    end
    inherited btnOk: TButton
      Left = 837
      OnClick = btnOkClick
      ExplicitLeft = 837
    end
  end
  object pcLayout: TPageControl [1]
    Left = 0
    Top = 0
    Width = 1072
    Height = 457
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = #1055#1086#1080#1089#1082' '#1087#1086' '#1085#1086#1084#1077#1088#1091' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
      object meNumbers: TMemo
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 1058
        Height = 38
        Hint = #1053#1072#1087#1080#1096#1080#1090#1077' '#1085#1086#1084#1077#1088#1072'  ('#1080#1083#1080' '#1095#1072#1089#1090#1080#1100' '#1085#1086#1084#1077#1088#1072') '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074' '#1074' '#1089#1090#1086#1083#1073#1080#1082
        Align = alTop
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object btSeek: TButton
        AlignWithMargins = True
        Left = 3
        Top = 47
        Width = 1058
        Height = 33
        Align = alTop
        Caption = #1053#1072#1081#1090#1080' '#1082#1086#1085#1090#1077#1081#1085#1077#1088
        TabOrder = 1
        OnClick = btSeekClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1055#1088#1080#1074#1103#1079#1082#1072' '#1082' '#1086#1087#1077#1088#1072#1094#1080#1103#1084
      ImageIndex = 1
      object Label1: TLabel
        Left = 0
        Top = 410
        Width = 1064
        Height = 16
        Align = alBottom
        Caption = 
          #1042#1085#1080#1084#1072#1085#1080#1077'! '#1047#1072#1087#1080#1089#1080' '#1073#1077#1079' '#1086#1090#1084#1077#1090#1086#1082' '#1087#1088#1080' '#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1080' '#1087#1088#1080#1074#1103#1079#1082#1080' '#1082' '#1087#1088#1086#1087#1091#1089#1082 +
          #1091' '#1091#1095#1080#1090#1099#1074#1072#1090#1100#1089#1103' '#1085#1077' '#1073#1091#1076#1091#1090'!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 5727656
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        StyleElements = [seClient, seBorder]
        ExplicitWidth = 656
      end
      object dgData: TDBGridEh
        Left = 0
        Top = 0
        Width = 1064
        Height = 410
        Align = alClient
        Border.Color = clSilver
        ColumnDefValues.Title.TitleButton = True
        ColumnDefValues.ToolTips = True
        Ctl3D = False
        DataSource = dsLocal
        DynProps = <>
        FixedColor = clCream
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        GridLineParams.BrightColor = clSilver
        GridLineParams.ColorScheme = glcsClassicEh
        GridLineParams.DarkColor = clSilver
        GridLineParams.DataBoundaryColor = clSilver
        GridLineParams.DataHorzColor = 15395041
        GridLineParams.DataVertColor = 15395041
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
        IndicatorParams.Color = clBtnFace
        IndicatorParams.HorzLineColor = clSilver
        IndicatorParams.VertLineColor = clSilver
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        OptionsEh = [dghHighlightFocus, dghAutoSortMarking, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
        ParentCtl3D = False
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        SearchPanel.Enabled = True
        SearchPanel.PersistentShowing = False
        SelectionDrawParams.SelectionStyle = gsdsClassicEh
        SelectionDrawParams.DrawFocusFrame = False
        SelectionDrawParams.DrawFocusFrameStored = True
        ShowHint = True
        SortLocal = True
        STFilter.Color = clWhite
        STFilter.HorzLineColor = clSilver
        STFilter.InstantApply = True
        STFilter.Local = True
        STFilter.VertLineColor = clSilver
        STFilter.Visible = True
        TabOrder = 0
        TitleParams.Color = clBtnFace
        TitleParams.FillStyle = cfstSolidEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -11
        TitleParams.Font.Name = 'Verdana'
        TitleParams.Font.Style = [fsBold]
        TitleParams.HorzLineColor = clSilver
        TitleParams.MultiTitle = True
        TitleParams.ParentFont = False
        TitleParams.SecondColor = clCream
        TitleParams.VertLineColor = clSilver
        OnSelectionChanged = dgDataSelectionChanged
        Columns = <
          item
            AutoFitColWidth = False
            CellButtons = <>
            Color = 14811105
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
            Width = 126
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'real_state_code'
            Footers = <>
            Title.Caption = #1053#1072#1093#1086#1078#1076#1077#1085#1080#1077
            Width = 124
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'containerkind_code'
            Footers = <>
            Title.Caption = #1058#1080#1087
            Width = 49
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'weight_fact'
            Footers = <>
            Title.Caption = #1042#1077#1089
            Width = 60
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            Color = 16769230
            DynProps = <>
            EditButtons = <>
            FieldName = 'state_code'
            Footers = <>
            Title.Caption = #1057#1090#1072#1090#1091#1089' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
            Width = 119
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'date_factexecution'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1089#1090#1072#1090#1091#1089#1072
            Width = 100
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'deliverytype_code'
            Footers = <>
            Title.Caption = #1058#1077#1082#1091#1097#1072#1103' '#1086#1087#1077#1088#1072#1094#1080#1103
            Width = 135
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'payer_code'
            Footers = <>
            Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082' ('#1082#1083#1080#1077#1085#1090')'
            Width = 122
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'pok_code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            Footers = <>
            Title.Caption = #1050#1086#1076' '#1052#1055
            Width = 223
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'checkcolumn'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited dsLocal: TDataSource
    DataSet = meData
    Left = 240
    Top = 224
  end
  inherited qrAux: TADOQuery
    Parameters = <
      item
        Name = 'opkindid'
        Size = -1
        Value = Null
      end
      item
        Name = 'guid'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select '
      'pok.id as pok_id,'
      'pok.code as pok_code,'
      
        '(select code from deliverytypes dt where id = pos.dlv_type_id) a' +
        's plan_dlv_code,'
      'c.id as container_id,'
      't.id as task_id,'
      'c.cnum, '
      
        '(select code from containerkinds ck where ck.id = c.kind_id) as ' +
        'containerkind_code, '
      
        '(select code+'#39', '#39'+name from cargotypes crt where crt.id = cr.typ' +
        'e_id) as cargotype_code, '
      'cr.weight_fact, '
      
        '(case when pos.seal_number = '#39'@'#39' then '#39#39' else cr.seal_number end' +
        ') as seal_number, '
      'cr.isempty, '
      
        '(select code from objectstatekinds osk where osk.id = los.object' +
        '_state_id) as state_code,'
      
        '(select code from objectstatekinds osk where osk.id = lrs.object' +
        '_state_id) as real_state_code,'
      'los.date_factexecution,'
      
        '(select code from deliverytypes dlv where dlv.id = t.dlv_type_id' +
        ') as deliverytype_code, '
      
        '(select code from counteragents ct1 where ct1.id = t.payer_id) a' +
        's payer_code,'
      '0 as checkcolumn'
      'from '
      
        'documents d, v_lastobjectrealstates lrs, v_lastobjectstates los,' +
        ' '
      'passopsettings pos, passoperationkinds pok, '
      'tasks t, cargos cr, containers c'
      'where d.id = los.doc_id'
      'and los.object_id = c.id and lrs.object_id = c.id'
      'and los.task_id = t.id and t.object_id = cr.id '
      
        'and (pos.start_state_id is null or pos.start_state_id = los.obje' +
        'ct_state_id)'
      
        'and (pos.real_state_id is null or pos.real_state_id = los.object' +
        '_state_id)'
      'and pos.dlv_type_id = t.dlv_type_id'
      'and pok.id = pos.kind_id'
      'and pok.id = :opkindid'
      'and dbo.filter_objects(:guid, c.cnum) = 1'
      '')
    Left = 184
    Top = 224
  end
  object meData: TMemTableEh
    Params = <>
    Left = 124
    Top = 222
  end
  object pmMemo: TPopupMenu
    Left = 396
    Top = 227
    object N1: TMenuItem
      Caption = #1053#1072#1081#1090#1080' '#1079#1072#1103#1074#1082#1080
      ShortCut = 16397
      OnClick = btSeekClick
    end
  end
end
