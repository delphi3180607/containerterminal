﻿inherited FormEditEmailAccount: TFormEditEmailAccount
  Caption = #1059#1095#1077#1090#1085#1072#1103' '#1079#1072#1087#1080#1089#1100' '#1101#1083#1077#1082#1090#1088#1086#1085#1085#1086#1081' '#1087#1086#1095#1090#1099' SMTP'
  ExplicitWidth = 543
  ExplicitHeight = 375
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 16
    Top = 172
    Width = 295
    Height = 16
    Caption = #1069#1083#1077#1082#1090#1088#1086#1085#1085#1072#1103' '#1087#1086#1095#1090#1072' '#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1103' ('#1072#1082#1082#1072#1091#1085#1090')'
  end
  object Label2: TLabel [1]
    Left = 16
    Top = 236
    Width = 48
    Height = 16
    Caption = #1055#1072#1088#1086#1083#1100
  end
  object Label3: TLabel [2]
    Left = 16
    Top = 8
    Width = 176
    Height = 16
    Caption = #1040#1076#1088#1077#1089' '#1087#1086#1095#1090#1086#1074#1086#1075#1086' '#1089#1077#1088#1074#1077#1088#1072
  end
  object Label4: TLabel [3]
    Left = 16
    Top = 105
    Width = 86
    Height = 16
    Caption = #1053#1086#1084#1077#1088' '#1087#1086#1088#1090#1072
  end
  object edEmailSender: TDBEditEh [5]
    Left = 16
    Top = 194
    Width = 457
    Height = 24
    DataField = 'email_sender'
    DataSource = FormEmailAccounts.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edPassword: TDBEditEh [6]
    Left = 16
    Top = 258
    Width = 457
    Height = 24
    DataField = 'password'
    DataSource = FormEmailAccounts.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object edAddress: TDBEditEh [7]
    Left = 16
    Top = 30
    Width = 457
    Height = 24
    DataField = 'server_name'
    DataSource = FormEmailAccounts.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object cbSSL: TDBCheckBoxEh [8]
    Left = 24
    Top = 72
    Width = 201
    Height = 17
    Caption = #1047#1072#1097#1080#1090#1072' '#1089#1086#1077#1076#1080#1085#1077#1085#1080#1103' SSL'
    DataField = 'ssl_check'
    DataSource = FormEmailAccounts.dsData
    DynProps = <>
    TabOrder = 4
  end
  object nuPort: TDBNumberEditEh [9]
    Left = 16
    Top = 127
    Width = 121
    Height = 24
    DataField = 'port_number'
    DataSource = FormEmailAccounts.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 5
    Visible = True
  end
end
