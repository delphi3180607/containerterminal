﻿inherited FormCargoSheet: TFormCargoSheet
  ActiveControl = dgData
  Caption = #1046#1044' '#1085#1072#1082#1083#1072#1076#1085#1099#1077
  ClientHeight = 627
  ClientWidth = 1234
  ExplicitWidth = 1250
  ExplicitHeight = 665
  PixelsPerInch = 96
  TextHeight = 13
  inherited SplitterVert: TSplitter
    Height = 592
    ExplicitHeight = 592
  end
  inherited plBottom: TPanel
    Top = 592
    Width = 1234
    Height = 35
    ExplicitTop = 592
    ExplicitWidth = 1234
    ExplicitHeight = 35
    inherited btnOk: TButton
      Left = 999
      Height = 29
      ExplicitLeft = 999
      ExplicitHeight = 29
    end
    inherited btnCancel: TButton
      Left = 1118
      Height = 29
      ExplicitLeft = 1118
      ExplicitHeight = 29
    end
  end
  inherited plAll: TPanel
    Width = 1034
    Height = 592
    ExplicitWidth = 1034
    ExplicitHeight = 592
    inherited SplitterHor: TSplitter
      Top = 467
      Width = 1034
      ExplicitTop = 476
      ExplicitWidth = 1039
    end
    inherited plTop: TPanel
      Width = 1034
      ExplicitWidth = 1034
      inherited sbDelete: TPngSpeedButton
        Left = 108
        ExplicitLeft = 114
      end
      inherited btFilter: TPngSpeedButton
        Left = 319
        ExplicitLeft = 296
        ExplicitHeight = 24
      end
      inherited btExcel: TPngSpeedButton
        Left = 767
        ExplicitLeft = 680
        ExplicitTop = 4
      end
      inherited sbAdd: TPngSpeedButton
        Left = 76
        Flat = True
        ExplicitLeft = 82
      end
      inherited sbEdit: TPngSpeedButton
        Left = 140
        ExplicitLeft = 146
      end
      inherited Bevel2: TBevel
        Left = 315
        Width = 2
        ExplicitLeft = 298
        ExplicitWidth = 2
        ExplicitHeight = 22
      end
      inherited btTool: TPngSpeedButton
        Left = 993
        Width = 38
        ExplicitLeft = 683
        ExplicitWidth = 38
      end
      inherited Bevel1: TBevel
        Left = 729
        Width = 3
        Margins.Right = 3
        ExplicitLeft = 681
        ExplicitTop = 2
        ExplicitWidth = 3
        ExplicitHeight = 22
      end
      inherited btFlow: TPngSpeedButton
        Left = 523
        ExplicitLeft = 500
      end
      inherited sbHistory: TPngSpeedButton
        Left = 555
        ExplicitLeft = 597
      end
      inherited sbRestrictions: TPngSpeedButton
        Left = 657
        Visible = False
        ExplicitLeft = 602
        ExplicitHeight = 24
      end
      inherited sbConfirm: TPngSpeedButton
        Left = 421
        ExplicitLeft = 398
        ExplicitHeight = 24
      end
      inherited sbCancelConfirm: TPngSpeedButton
        Left = 453
        ExplicitLeft = 430
        ExplicitHeight = 24
      end
      inherited Bevel5: TBevel
        Left = 589
        Width = 2
        ExplicitLeft = 598
        ExplicitWidth = 2
        ExplicitHeight = 22
      end
      inherited sbReport: TPngSpeedButton
        Left = 735
        ExplicitLeft = 547
        ExplicitTop = 6
      end
      inherited Bevel6: TBevel
        Left = 519
        Width = 2
        ExplicitLeft = 500
        ExplicitTop = 2
        ExplicitWidth = 2
        ExplicitHeight = 22
      end
      inherited sbImport2: TPngSpeedButton
        Flat = False
        OnClick = sbImport2Click
        ExplicitLeft = 2
      end
      inherited sbSearch: TPngSpeedButton
        Left = 383
        ExplicitLeft = 360
        ExplicitHeight = 24
      end
      inherited Bevel7: TBevel
        Left = 417
        Width = 2
        ExplicitLeft = 422
        ExplicitTop = 2
        ExplicitWidth = 2
        ExplicitHeight = 22
      end
      object sbRegisterDate: TPngSpeedButton [19]
        AlignWithMargins = True
        Left = 248
        Top = 0
        Width = 32
        Height = 24
        Hint = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090#1099' '#1088#1072#1079#1075#1088#1091#1079#1082#1080' (Alt+X)'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        OnClick = sbRegisterDateClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000000D84944415478DA63FCFFFF3F033298
          65C2882A8006D2CEFC6744E6336231E0369052C1A1FF0ED00055420680A8E340
          6C81A6F92C109B000D60C06B00D4903540EA2050F16428DF144401710150EC20
          4E03800A5981541F481CA83007CD507B20550CC47540B90BB80C680652BA401C
          0F54F4118BCB0C80D47990F780F227510C004A4602A974A08403BE5800AA3307
          52A5403C01A8F608D800A0A0235000E4E45AA0E035060200A8DE06484D04E222
          9801FF191881A10F720D8934C40053A6FF09477F302CB062674838F69378DA9A
          63D400EA1940413402007A90137A3B5756CD0000000049454E44AE426082}
        ExplicitLeft = 222
        ExplicitHeight = 26
      end
      inherited sbClearFilter: TPngSpeedButton
        Left = 351
        ExplicitLeft = 328
        ExplicitHeight = 24
      end
      inherited Bevel3: TBevel
        Left = 691
        Width = 2
        ExplicitLeft = 690
        ExplicitTop = 2
        ExplicitWidth = 2
      end
      inherited sbClock: TPngSpeedButton
        Left = 695
        ExplicitLeft = 589
      end
      inherited sbBarrel: TPngSpeedButton
        Left = 485
        Enabled = False
        Visible = False
        ExplicitLeft = 573
        ExplicitTop = 3
      end
      object sbExtraInfo: TPngSpeedButton [24]
        AlignWithMargins = True
        Left = 625
        Top = 0
        Width = 32
        Height = 24
        Hint = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1086' (Ctrl+Q)'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        Layout = blGlyphRight
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbExtraInfoClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000002C54944415478DA95926B4893511CC6
          9FF3EE7D77D5DDD42DD36CB699A45BE9487199664950884894E2BA10F4C16C41
          DFCB2F1116441715419D0876A1082AF253D3E88317046FB9215260691F72CC24
          2F78D972DBBBB733F38388213D873FE77078FEBF73CEF3BE2459A5548878A152
          A489CB60C4AC44A00A03080B00A12526585790AE2374260C0429610881A0951F
          3F5143F6CA1577F40F1F8762CBED674830AC66044464D468604530D3EE398127
          11DAA50541749F6739E1DDD749787B7B57E3CF959C27C618E5F7B48676BFBFF0
          4046808F40E697C10C31B4AC8079E96F8C22008E6790C311C4D1738314D2EF9B
          85AFB7BF6FFC664D2189DFA57FB5E7ECEDAC8B823AB56BB82764CE9E25BADD2B
          C284CF2F5A9835F1FBF9932177609C7C235E7084A34F09A344710473DEB1A056
          6DAC25D0291E489C53D6BBBED86352471B5FDEF482E84AC7852F9DABCCE26B47
          642C708F7FA472939F9145B0820821F0B04AD223AAE937F2FC488A9B70F18A56
          D238683EADCBB455D8DFA2A0B11D49C51E4C0D0431FFDC811654A33D698EA62A
          C5DF18195A6B481F72A26A25FB0305C438C5F543965255926DDF8D26D8ECDD58
          4EEC8646224342BF036DCBD7D1A2F7D2F4363E4714C14890E57982CA80B9731D
          C0358C584AD53A9BA8EA2AF24E4DC2BA5406D7AF6E5C36A4E099FF3E6A353FE8
          E19B01621CA2007BC0DCB57183414B71A2C9A6B8D5810BC645B89832F4CC0C22
          2D711ED35C2E86B90020E0DF0069DD8025D768B2F5B54EC0BCA6C4A86686BA94
          4088A5F605DA2CC2666D0B284835D9DE378F024A6AE66909D15F316A27D8AA6D
          014729C0D5EC01142C76D2564093AC6EE060BEC194EF6A71FF07E0E95F001BA7
          7819767A0C453483EEFACF40ECCE005040C648032EAD667612B93AA6FA4A456B
          95416FCC6EFCD801A958B263BFC010D816C2384C32BB48825C25CE4BB61619A5
          39D7E6424B720624BC6D729B14A623994D4181ACECD31F764F1024680B5C8D00
          00000049454E44AE426082}
        ExplicitLeft = 570
        ExplicitHeight = 26
      end
      object sbChange: TPngSpeedButton [25]
        AlignWithMargins = True
        Left = 210
        Top = 0
        Width = 32
        Height = 24
        Hint = #1048#1079#1084#1077#1085#1080#1090#1100' '#1090#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbChangeClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000014000000140803000000BA57ED
          3F0000000373424954080808DBE14FE00000003C504C54456C6C6CC4C4C49494
          940060A78E8E8EF4F4F4B3B3B37C7C7CA5A5A5E2E2E20066CCD9D9D90063BA83
          8383999999E3E3E3727272ADADADFFFFFFCCCCCC468EB0F50000001474524E53
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFF169F5EE0000000970485973
          00000AEB00000AEB01828B0D5A00000015744558744372656174696F6E205469
          6D6500322F31372F3038209CAA580000001C74455874536F6674776172650041
          646F62652046697265776F726B732043533571B5E336000000A74944415478DA
          8DD0EB0E83200C05E003881DD369DCFBBFA406035591B179D98CC9367E40F892
          4369C51DE725BE23834EE866487340ABC94EC84A1ECB1D2D0D26A4B87239971B
          F2148B3E9D452F32DAE3B3376D85AE46945BDC051A2793DE749966655EC83A8C
          B8B081230FAD465A51402E98C715119CA0881487E068D45EA843619B639BAC3B
          89CA3FEFD2D5DBE7AF6953D1AFF6D1A6181ABFD87B20A185BEB5F51FA3FB39F9
          07B562487DDFD76D0F0000000049454E44AE426082}
        ExplicitLeft = 184
        ExplicitHeight = 26
      end
      object sbEarlyRegister: TPngSpeedButton [26]
        AlignWithMargins = True
        Left = 281
        Top = 0
        Width = 32
        Height = 24
        Hint = #1047#1072#1088#1077#1075#1080#1089#1090#1088#1080#1088#1086#1074#1072#1090#1100' '#1087#1088#1077#1076#1087#1086#1076#1072#1075#1072#1077#1084#1091#1102' '#1076#1072#1090#1091' '#1087#1086#1076#1072#1095#1080
        Margins.Left = 1
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        OnClick = sbEarlyRegisterClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C0000027F4944415478DAA5937B48145114C6BF418A50CA325731
          FCA3A28D48B20C335F95695AD143250B04A960155AA9B0D6151FBDA044A10429
          2C22D3655D455DA31496CC58535B283024D0C62D6D1329B3A76B21E4CEDCB9DD
          9959934082F00E87EF9C33CCEFDC33F75C8E528AF92C4E06589EBA2F30C92154
          0A9524094402445989EC53884C678D808894E5A5DED22C6D940230F74C7C3914
          B538F07F2A5F6A1A9EBE7A6CDD220550DBF58D1E89F687DFBD5C406E89888044
          0041507DCF34F33DC0B4A0E4A60A3B5162798DCAEC304E0154777EA599B14B19
          400F8468D8C7A26A54F4426400339141F88F982AEE42818947D589701570FBF1
          679AB52D607607545295FD076527644689F26E4AD784BCEA7EDC3919A102AADA
          C7E9F18440B4BDF88ECCF8A07FF6DEF26C0C096101C8AF1980292F520554DAC6
          A82E5183F69713381C138407FD2D70BCEB4676B41ECE4F3CEC431D48D2A6206D
          4306EA7B3E2076ED1294D4F168306E5501156DEF694E7230ECFD6EA44769944A
          B92D3A9CDE6E80C3D58DA35B7428B61970EDE00DD4748E224EEB8FF3161ECD45
          312AA0FCFE28D5A784A067D08D03917F033A9C0F1595E39B197771EBD10812D7
          2FC339CB00AC45712AE0B275849EDABB02CF8726B12742A3546DE833237EF50E
          F82EF0C593613B76AE49525AB86E736177F87294599D309DF5B670B1C945CFEC
          0B45EFDB1F48DE180865BC39EFA8B2478E394E4D54B40E63FF660DAE34F2A833
          7A5B28A97F43F35357A28F01C62745F5D4585E9067891D9D87253C2CF0081216
          FA10A44606A3AC9987C9E00514989DB4307D9577EE67669FFEB90B73ADD2C657
          92D918EBA3000CB5830E51209B58253F4124908D8812AB2E83C43901EE9FBF5A
          EDE5BBD2B8F95EE7DFEFF786F03D64652C0000000049454E44AE426082}
        ExplicitLeft = 257
        ExplicitHeight = 26
      end
      object sbFilterPlan: TPngSpeedButton [27]
        AlignWithMargins = True
        Left = 38
        Top = 0
        Width = 32
        Height = 24
        Hint = #1054#1090#1073#1086#1088' '#1087#1086' '#1050#1055
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        AllowAllUp = True
        GroupIndex = 1
        ParentShowHint = False
        ShowHint = True
        OnClick = sbFilterPlanClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C08648800000009704859730000004E000000
          4E01B1CD1F330000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000001FA4944415478DA636440020D0C0D4C
          B7CDDFF22C3D39F91303918011C6C8B5CD155510FEB74E41EC9FD2F5278C6B6A
          B64DCB27DA8042CB642105498E1D0196BF4D999919186E3D657ABFE71C4B64DB
          FE293B091A90669CC6A529C7BA3FC8EAB71948330CAC3BCA72FACA833FD6B3CE
          CEFA8DD7801A97BCDC28879F93F8B8FEA3487CFECEF86FF5619609F7DFFE9FC0
          C1CCA4C3CAC4F178D2B19E2B1806E4D995AA1ACB7FDDE968F04791891155F2E6
          63A637DFFF30328AF1FF17FEFC8DE1F3E5074C979FBC6049EF3936096E10584B
          A955AEB298F0FF690E7A7F6D2585FF72E272EEBD678C1FF65F646EA9DF3DAD17
          231640A0CE25375349F26F9EA3DE1F0DE4F0006B7EC1F4F9D065E69E9AED539B
          B046230C94DBE7E4C6B9FC9EC4CF8D08937BCF993EED3DC73CA571EFD46A9CE9
          0006DA7DB3E7C73AFE4E80F11FBC64FAF4418CFBEBED5BFF2E7F9CC9EF094C6C
          FFF01AD0E19B332FC6F157224CF3EE6BEC17420AB80C844499B816F57C9B5AB9
          A8BF00AF01558E7961C1363F177FFFC5F863CF4596FE861D531AEAC28A52CD9D
          58AA24E598F8372FF9915FBF6CC2629C0680C45A3C73B6FEF9CF700AA4196EB0
          7BB691A201FB6C7E616691F3FB7F78B66F9F7C0D97013841927A19AFA6CDEF15
          BF7EFE57B97189C568F1A59EAF24190073614348C1C4EF5FFF6B756E9FE8428E
          016050E19117F8FECBFF7B006B30C9507591C2870000000049454E44AE426082}
        ExplicitLeft = 43
        ExplicitHeight = 26
      end
      object Bevel8: TBevel [28]
        AlignWithMargins = True
        Left = 72
        Top = 2
        Width = 2
        Height = 22
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        ExplicitLeft = 89
        ExplicitTop = -1
      end
      object Bevel9: TBevel [29]
        AlignWithMargins = True
        Left = 244
        Top = 2
        Width = 2
        Height = 22
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        ExplicitLeft = 188
        ExplicitTop = -1
      end
      object Bevel11: TBevel [30]
        AlignWithMargins = True
        Left = 206
        Top = 2
        Width = 2
        Height = 22
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        ExplicitLeft = 187
        ExplicitTop = -1
      end
      object Bevel12: TBevel [31]
        AlignWithMargins = True
        Left = 34
        Top = 2
        Width = 2
        Height = 22
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        ExplicitLeft = 89
        ExplicitTop = -1
      end
      inherited sbPass: TPngSpeedButton
        Left = 172
        Visible = True
        ExplicitLeft = 164
      end
      object pgPlug: TPngSpeedButton [33]
        AlignWithMargins = True
        Left = 593
        Top = 0
        Width = 32
        Height = 24
        Hint = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1086' (Ctrl+Q)'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        Layout = blGlyphRight
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbExtraInfoClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D494844520000000D000000120806000000806B54
          320000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B7320435336E8BCB28C000000DF4944415478DA6364C8BDC900075F
          5F7D64E016E363F8F3C3818185E32090B607D20780E29F80E2FC30658C03A4E9
          CBCBF70C3CE202607104F80F14FF00141744688ADA920297E61299C8C029C8C5
          F0F6560650EA2F503D3383B0DA0C86EFEFBF317C7B938F6CD37F061201C8A6AB
          0CC2AA5A44EB787BFB1ACC4FA4D8C6387C35BDBE760C18475AC8B18E0140A9E5
          DB9B6B0CA25A56A829E2D393170C7C32E2181A3E3D79091497805B85A2E9F3B3
          B70CBC5242189A3E3F7B071417C6AEE9F5B5E340EB2D303441C4ADE09AFEFFC7
          0C03C6BC5B70C1FF93D41831E4C9D10400B67C7D35BFA10E270000000049454E
          44AE426082}
        ExplicitLeft = 570
        ExplicitHeight = 26
      end
      inherited plCount: TPanel
        Left = 829
        ExplicitLeft = 829
        inherited edCount: TDBEditEh
          ControlLabel.ExplicitLeft = 16
          ControlLabel.ExplicitTop = -16
          ControlLabel.ExplicitWidth = 103
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 1034
      Height = 379
      DataGrouping.Active = True
      DataGrouping.DefaultStateExpanded = True
      DataGrouping.GroupLevels = <
        item
          ColumnName = 'Column_1_train_num'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end>
      FixedColor = clWhite
      FrozenCols = 1
      OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDblClickOptimizeColWidth, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      ReadOnly = False
      TitleParams.MultiTitle = True
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 51
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num'
          Footers = <>
          Title.Caption = #1050#1055
          Width = 61
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'prefix'
          Footers = <>
          MaxWidth = 60
          Title.Caption = #1055#1088#1077#1092#1080#1082#1089
          Visible = False
          Width = 60
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          CellDataIsLink = True
          Color = clSilver
          DynProps = <>
          EditButtons = <>
          FieldName = 'restr_sign'
          Footers = <>
          MaxWidth = 40
          MinWidth = 40
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1054#1075#1088'.'
          Width = 40
          OnCellDataLinkClick = dgDataColumns5CellDataLinkClick
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          CellDataIsLink = True
          Color = clSilver
          DynProps = <>
          EditButtons = <>
          FieldName = 'letter_sign'
          Footers = <>
          MaxWidth = 43
          MinWidth = 43
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1055#1088#1080#1084'.'
          Width = 43
          OnCellDataLinkClick = dgDataColumns6CellDataLinkClick
        end
        item
          CellButtons = <>
          CellDataIsLink = True
          Checkboxes = True
          Color = clYellow
          DynProps = <>
          EditButtons = <>
          FieldName = 'paidfull'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1054#1087#1083'. '#1087#1086#1083#1085'.'
          Width = 44
          OnCellDataLinkClick = dgDataColumns7CellDataLinkClick
        end
        item
          CellButtons = <>
          CellDataIsLink = True
          Checkboxes = True
          Color = clYellow
          DynProps = <>
          EditButtons = <>
          FieldName = 'dlvallowed'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1056#1072#1079#1088'. '#1074#1099#1076'.'
          Width = 39
          OnCellDataLinkClick = dgDataColumns8CellDataLinkClick
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          CellDataIsLink = True
          Color = 16623272
          DynProps = <>
          EditButtons = <>
          FieldName = 'pass_number'
          Footers = <>
          Title.Caption = #1055#1088#1086' '#1087#1091#1089#1082
          Width = 46
          OnCellDataLinkClick = dgDataColumns1CellDataLinkClick
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = 'dd.mm.yyyy'
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 65
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'datesent'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1090#1087#1088#1072#1074#1082#1080
          Width = 66
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_modified'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
          Visible = False
          Width = 79
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'income_plan'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1055#1083#1072#1085' '#1087#1086#1076#1072#1095#1080
          Width = 136
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'unloadfront'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = [fsItalic]
          Footers = <>
          Title.Caption = #1060#1088#1086#1085#1090
          Width = 45
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = False
          DisplayFormat = '###%'
          DynProps = <>
          EditButtons = <>
          FieldName = 'usedproc'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1057#1086#1079#1076'. '#1076#1086#1082'.'
          Visible = False
          Width = 46
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'emptyreturn_sign'
          Footers = <>
          MaxWidth = 40
          MinWidth = 40
          ReadOnly = True
          Title.Caption = #1042#1086#1079#1074#1088'. '#1076#1086#1082'.'
          Width = 40
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          CellDataIsLink = True
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'guard_sign'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1054#1093#1088'.'
          Width = 31
          OnCellDataLinkClick = dgDataColumns15CellDataLinkClick
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          Color = clAqua
          DynProps = <>
          EditButtons = <>
          FieldName = 'isplugged'
          Footers = <>
          Title.Caption = #1055#1086#1076#1082#1083'.'
          Width = 48
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_sender'
          Footers = <>
          Title.Caption = #1042#1077#1089', '#1086#1090#1087#1088'., '#1090#1086#1085#1085
          Width = 47
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Color = 2866676
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_doc'
          Footers = <>
          Title.Caption = #1042#1077#1089' '#1046#1044#1053', '#1082#1075'.'
          Width = 47
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_fact'
          Footers = <>
          Title.Caption = #1042#1077#1089' '#1092#1072#1082#1090', '#1082#1075'.'
          Width = 45
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'seal_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
          Width = 82
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'carriage_num'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1053#1086#1084#1077#1088' '#1074#1072#1075#1086#1085#1072
          Width = 81
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'container_num'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
          Width = 81
        end
        item
          CellButtons = <>
          Color = 7138631
          DynProps = <>
          EditButtons = <>
          FieldName = 'container_state'
          Footers = <>
          Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 67
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'isempty'
          Footers = <>
          Title.Caption = #1055#1086#1088#1086#1078'- '#1085#1080#1081'?'
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'container_kind_text'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1058#1080#1087
          Width = 47
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Color = 2866676
          DynProps = <>
          EditButtons = <>
          FieldName = 'container_owner'
          Footers = <>
          Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
          Width = 96
          OnUpdateData = dgDataColumnUpdateData
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Color = 2866676
          DynProps = <>
          EditButtons = <>
          FieldName = 'forwarder'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          Width = 113
          OnUpdateData = dgDataColumnUpdateData
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Color = 2866676
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 113
          OnUpdateData = dgDataColumnUpdateData
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Color = 2866676
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer'
          Footers = <>
          Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
          Width = 106
          OnUpdateData = dgDataColumnUpdateData
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Color = 2866676
          DynProps = <>
          EditButtons = <>
          FieldName = 'dlvtype'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
          Width = 83
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'notifysign'
          Footers = <>
          Title.Caption = #1059#1074'.'
          Width = 32
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'alert_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1091#1074#1077#1076#1086#1084#1083'.'
          Width = 68
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'last_alert_date'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1055#1086#1089#1083'. '#1076#1072#1090#1072' '#1091#1074#1077#1076'.'
          Width = 65
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = '### ### ##0.00'
          DynProps = <>
          EditButtons = <>
          FieldName = 'services_summa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = []
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1057#1091#1084#1084#1072' '#1089#1095#1077#1090
          Width = 79
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'user_created'
          Footers = <>
          Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
          Width = 95
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_created'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1089#1086#1079#1076'.'
          Width = 56
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'id'
          Footers = <>
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doctype'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1058#1080#1087' '#1076#1086#1082'.'
          Width = 55
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'task_id'
          Footers = <>
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          Color = clWhite
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 61
        end>
    end
    inherited plHint: TPanel
      Top = 373
      ExplicitTop = 373
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    inherited plStatus: TPanel
      Width = 1034
      ExplicitWidth = 1034
    end
    inherited plLinkedObjects: TPanel
      Top = 477
      Width = 1028
      Height = 112
      ExplicitTop = 477
      ExplicitWidth = 1028
      ExplicitHeight = 112
      inherited dgLinkedObjects: TDBGridEh
        Width = 1028
        Height = 112
        GridLineParams.ColorScheme = glcsDefaultEh
        TitleParams.MultiTitle = True
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'rus_object_type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1086#1073#1098#1077#1082#1090#1072
            Width = 114
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088
            Width = 102
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'kind_code'
            Footers = <>
            Title.Caption = #1042#1080#1076
            Width = 105
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'owner_code'
            Footers = <>
            Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
            Width = 135
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'state_code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = [fsItalic]
            Footers = <>
            Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 134
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'dlvtype_code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7697781
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1086#1087#1077#1088#1072#1094#1080#1080
            Width = 125
          end>
      end
    end
    inherited plSearch: TPanel
      Width = 1034
      ExplicitWidth = 1034
      inherited Label1: TLabel
        Height = 14
      end
      inherited edSearch: TEdit
        Width = 852
        ExplicitWidth = 852
      end
    end
    object plNotes: TPanel
      Left = 0
      Top = 442
      Width = 1034
      Height = 25
      Align = alBottom
      Constraints.MinHeight = 25
      TabOrder = 6
      object Label2: TLabel
        AlignWithMargins = True
        Left = 494
        Top = 4
        Width = 42
        Height = 17
        Margins.Left = 10
        Align = alLeft
        Caption = #1054#1087#1083#1072#1090#1072':'
        ExplicitHeight = 13
      end
      object edNote: TDBEdit
        AlignWithMargins = True
        Left = 1
        Top = 1
        Width = 480
        Height = 23
        Margins.Left = 0
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alLeft
        BorderStyle = bsNone
        Color = 15593971
        DataField = 'cargo_description'
        DataSource = dsData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        StyleElements = [seBorder]
      end
      object edPaidSumm: TDBEdit
        AlignWithMargins = True
        Left = 540
        Top = 3
        Width = 109
        Height = 19
        Margins.Left = 1
        Margins.Top = 2
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        DataField = 'paidsumm'
        DataSource = dsData
        ReadOnly = True
        TabOrder = 1
        ExplicitHeight = 21
      end
      object cbPaidFull: TDBCheckBox
        AlignWithMargins = True
        Left = 659
        Top = 4
        Width = 60
        Height = 17
        Margins.Left = 10
        Align = alLeft
        Caption = #1055#1086#1083#1085#1072#1103
        DataField = 'paidfull'
        DataSource = dsData
        ReadOnly = True
        TabOrder = 2
      end
      object cbDlvAllowed: TDBCheckBox
        AlignWithMargins = True
        Left = 732
        Top = 4
        Width = 221
        Height = 17
        Margins.Left = 10
        Align = alLeft
        Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1072' '#1074#1099#1076#1072#1095#1072' '#1073#1077#1079' '#1087#1086#1083#1085#1086#1081' '#1086#1087#1083#1072#1090#1099
        DataField = 'dlvallowed'
        DataSource = dsData
        ReadOnly = True
        TabOrder = 3
        OnMouseDown = dgDataMouseUp
        OnMouseUp = dgDataMouseUp
      end
      object edDlvallowedDate: TDBEdit
        AlignWithMargins = True
        Left = 957
        Top = 5
        Width = 109
        Height = 19
        Margins.Left = 1
        Margins.Top = 4
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        BevelInner = bvSpace
        BevelOuter = bvNone
        BorderStyle = bsNone
        Color = clBtnFace
        DataField = 'dlvallowed_date'
        DataSource = dsData
        ReadOnly = True
        TabOrder = 4
      end
    end
  end
  inherited plLeft: TPanel
    Height = 592
    ExplicitHeight = 592
    inherited Bevel4: TBevel
      Height = 567
      ExplicitHeight = 448
    end
    inherited dgFolders: TDBGridEh
      Height = 567
    end
    inherited plTool: TPanel
      inherited sbWrap: TSpeedButton
        Width = 100
        Margins.Right = 1
        ExplicitWidth = 98
      end
    end
  end
  inherited pmGrid: TPopupMenu
    Left = 344
    Top = 200
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'declare @userid int, @trainnum varchar(100), @doctypeid int, @da' +
        'tebegin datetime, @dateend datetime, @isconfirmed int;'
      ''
      
        'select @doctypeid = min(id) from doctypes where system_section =' +
        ' '#39'income_sheet'#39';'
      ''
      'select @userid = :userid, @trainnum = :trainnum;'
      'select @trainnum = rtrim(ltrim(@trainnum));'
      ''
      
        'select @datebegin = datebegin, @dateend  = dateend, @isconfirmed' +
        '  = isconfirmed '
      'from dbo.f_GetFilterParams(@userid,@doctypeid);'
      ''
      'select '
      
        'd.id, d.doc_number,  d.doc_date, d.folder_id, d.doctype_id, s.pr' +
        'efix, d.inwait, d.datestopinwait, '
      
        '(select code from doctypes t2 where t2.id = d.doctype_id) as doc' +
        'type,'
      
        '(case when isnull(d.isconfirmed,0) = 1 then '#39' +'#39' else '#39#39' end) as' +
        ' state_code,'
      's.datesent, s.unload_doc_id,'
      
        '(case when ps1.cnt>1 then '#39'**'#39' else ps1.pass_number end) as pass' +
        '_number,'
      'dbo.f_GetDocUsedProcs(d.id, '#39'-4-18-'#39') as usedproc,'
      'dbo.get_restr_state(d.id) as restr_sign,'
      
        '(select 1 where not exists (select 1 from doccargorestrictions r' +
        ' where r.doc_id = d.id and r.dateend is null)) as restr_closed,'
      
        'isnull((select '#39#1045#1089#1090#1100#39' where exists (select 1 from doccargoletter' +
        's r where r.doc_id = d.id)),'#39'    '#39') as letter_sign,'
      
        '(select sum(summa) from doccargoservices sr where sr.doc_id = d.' +
        'id) as services_summa,'
      's.train_num,'
      's.container_num,'
      
        '(select sk1.code from containers c1, v_lastobjectstates los1, ob' +
        'jectstatekinds sk1 where c1.id = los1.object_id and los1.task_id' +
        ' = s.task_id and sk1.id = los1.object_state_id and c1.cnum = s.c' +
        'ontainer_num) container_state,'
      's.container_kind_id, s.container_foot, '
      
        'isnull((select code from containerkinds ck where ck.id = s.conta' +
        'iner_kind_id), s.container_foot) as container_kind_text, '
      
        '(select code from counteragents c where c.id = s.container_owner' +
        '_id) as container_owner,'
      'incplan.income_plan, '
      'incplan.unloadfront,'
      's.carriage_num,'
      's.carriage_type,'
      
        '(select code from counteragents c where c.id = s.carriage_owner_' +
        'id) as carriage_owner,'
      
        '(select code from cargotypes t where t.id = s.cargotype_id) as c' +
        'argotype,'
      's.seal_number,'
      's.weight_sender,'
      's.weight_doc,'
      's.weight_fact,'
      
        '(select code from counteragents c where c.id = s.forwarder_id) a' +
        's forwarder,'
      
        '(select code from counteragents c where c.id = s.consignee_id) a' +
        's consignee,'
      
        '(select code from counteragents c where c.id = s.payer_id) as pa' +
        'yer,'
      
        '(select code from deliverytypes t where t.id = s.dlv_type_id) as' +
        ' dlvtype,'
      's.dlv_term,'
      's.task_id, '
      's.forwarder_id,'
      's.consignee_id,'
      's.payer_id,'
      's.dlv_type_id,'
      's.container_owner_id,'
      's.carriage_owner_id,'
      's.cargotype_id,'
      's.isempty,'
      's.guard_sign,'
      's.docreturn_sign,'
      's.emptyreturn_sign,'
      's.emptyreturn_date,'
      's.cargo_description,'
      's.need_pay, '
      's.alert_date,'
      's.last_alert_date,'
      'd.date_created,'
      'd.date_modified,'
      'd.isconfirmed,'
      'd.date_confirm, '
      
        '(select user_name from users u where u.id = d.user_created) as u' +
        'ser_created,'
      'd.isdeleted,'
      'd.isabolished,'
      'notifysign, '
      'paidsumm, '
      'isnull(paidfull,0) as paidfull, '
      'paiddate_register, '
      'isnull(dlvallowed,0) as dlvallowed,'
      'dlvallowed_date, '
      'dlvallowed_note, '
      'paydocs, '
      'plugstart, '
      'plugend,'
      '(case when plugstart is null then 0 else 1 end) as isplugged'
      'from documents d,  doccargosheet s'
      'left outer join ('
      
        '     select po.task_id, osh.doc_id, min(p.pass_number) as pass_n' +
        'umber, count(*) as cnt '
      '     from passes p, passoperations po, objectstatehistory osh '
      
        '     where p.id = po.parent_id and po.task_id = osh.task_id and ' +
        'po.container_id = osh.object_id'
      '     and isnull(p.isabolished,0) = 0  '
      '     group by po.task_id, osh.doc_id'
      ') ps1 on (ps1.task_id = s.task_id and s.id = ps1.doc_id)'
      'left outer join '
      '('
      
        '  select f.id, isnull(l.note,'#39'???'#39')+'#39' , '#39'+convert(varchar, plan_' +
        'date, 104) as income_plan, plan_date, '
      
        '  (select code from unloadfronts uf where uf.id = f.unload_front' +
        '_id) as unloadfront'
      '  from loadplan l, docfeed f where l.id = f.load_plan_id '
      ') as incplan'
      'on (incplan.id = s.unload_doc_id)'
      'where d.id = s.id '
      'and (1 = :showall or d.folder_id = :folder_id)'
      'and'
      '  ('
      
        '    not exists (select 1 from dbo.f_GetFilterList(@userid, @doct' +
        'ypeid))'
      
        '    or exists (select 1 from dbo.f_GetFilterList(@userid, @docty' +
        'peid) fl where charindex(fl.v, s.container_num)>0) '
      
        '    or exists (select 1 from dbo.f_GetFilterList(@userid, @docty' +
        'peid) fl where charindex(fl.v, s.carriage_num)>0) '
      '  )'
      'and ('
      
        '  ((@datebegin is null or d.doc_date >=@datebegin) and (@dateend' +
        ' is null or  d.doc_date <@dateend+1))'
      
        '  or ((@datebegin is null or incplan.plan_date >=@datebegin) and' +
        ' (@dateend is null or  incplan.plan_date <@dateend+1))'
      
        '  or (isnull(@trainnum,'#39#39')<>'#39#39' and rtrim(ltrim(s.train_num)) = r' +
        'trim(ltrim(@trainnum)))'
      ')'
      'and  ( '
      
        '  exists (select 1 from dbo.f_GetFilterList(@userid, @doctypeid)' +
        ') '
      '  or rtrim(ltrim(s.train_num)) = ltrim(@trainnum)'
      '  or isnull(@trainnum,'#39#39')='#39#39' '
      ')'
      
        'and (isnull(@isconfirmed,0)=0 or d.isconfirmed = isnull(@isconfi' +
        'rmed,0))'
      'order by s.train_num, s.id')
    SelectCommand.Parameters = <
      item
        Name = 'userid'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'trainnum'
        DataType = ftString
        Size = 3
        Value = '0'
      end
      item
        Name = 'showall'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'folder_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update doccargosheet'
      'set'
      '  train_num = :train_num,'
      '  datesent = :datesent,'
      '  container_num = :container_num,'
      '  container_foot = :container_foot,'
      '  container_owner_id = :container_owner_id,'
      '  carriage_num = :carriage_num,'
      '  carriage_type = :carriage_type,'
      '  carriage_owner_id = :carriage_owner_id,'
      '  cargotype_id = :cargotype_id,'
      '  cargo_description = :cargo_description,'
      '  seal_number = :seal_number,'
      '  weight_sender = :weight_sender,'
      '  weight_doc = :weight_doc,'
      '  weight_fact = :weight_fact,'
      '  forwarder_id = :forwarder_id,'
      '  consignee_id = :consignee_id,'
      '  payer_id = :payer_id,'
      '  dlv_type_id = :dlv_type_id,'
      '  dlv_term = :dlv_term,'
      '  guard_sign = :guard_sign,'
      '  docreturn_sign = :docreturn_sign,'
      '  emptyreturn_sign = :emptyreturn_sign,'
      '  need_pay = :need_pay,'
      '  isempty = :isempty,'
      '  alert_date = :alert_date,'
      '  emptyreturn_date = :emptyreturn_date,'
      '  paidsumm = :paidsumm,'
      '  paidfull = :paidfull, '
      '  dlvallowed = :dlvallowed,'
      '  dlvallowed_note = :dlvallowed_note, '
      '  paydocs = :paydocs, '
      '  container_kind_id = :container_kind_id,'
      '  plugstart = :plugstart, '
      '  plugend = :plugend'
      'where'
      '  id = :id;'
      ''
      
        'update  documents set  date_modified = getdate(), doc_number = :' +
        'doc_number, doc_date = :doc_date where id = :id;'
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'train_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'datesent'
        Size = -1
        Value = Null
      end
      item
        Name = 'container_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_foot'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'carriage_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'carriage_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'carriage_owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cargotype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cargo_description'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2000
        Value = Null
      end
      item
        Name = 'seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'weight_sender'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_doc'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_fact'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'forwarder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'consignee_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'payer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dlv_term'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'guard_sign'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'docreturn_sign'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'emptyreturn_sign'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'need_pay'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'isempty'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'alert_date'
        Size = -1
        Value = Null
      end
      item
        Name = 'emptyreturn_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'paidsumm'
        Size = -1
        Value = Null
      end
      item
        Name = 'paidfull'
        Size = -1
        Value = Null
      end
      item
        Name = 'dlvallowed'
        Size = -1
        Value = Null
      end
      item
        Name = 'dlvallowed_note'
        Size = -1
        Value = Null
      end
      item
        Name = 'paydocs'
        Size = -1
        Value = Null
      end
      item
        Name = 'container_kind_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'plugstart'
        Size = -1
        Value = Null
      end
      item
        Name = 'plugend'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'declare @newid int, @doctypeid int;'
      ''
      
        'select @doctypeid = id from doctypes where system_section='#39'incom' +
        'e_sheet'#39';'
      ''
      
        'insert into documents (doctype_id, folder_id, doc_number,  doc_d' +
        'ate) values (@doctypeid, :folder_id, :doc_number,  :doc_date);'
      ''
      'select @newid = IDENT_CURRENT('#39'documents'#39');'
      ''
      ''
      'insert into doccargosheet'
      
        '  (id, train_num, container_num, container_foot, container_owner' +
        '_id, '
      
        '   carriage_num, carriage_type, carriage_owner_id, cargotype_id,' +
        ' cargo_description, '
      
        '   seal_number, weight_sender, weight_doc, weight_fact, forwarde' +
        'r_id, consignee_id, '
      
        '   payer_id, dlv_type_id, dlv_term, guard_sign, docreturn_sign, ' +
        'emptyreturn_sign, '
      
        '   need_pay, isempty, emptyreturn_date, container_kind_id, plugs' +
        'tart, plugend )'
      'values'
      
        '  (@newid, :train_num, :container_num, :container_foot, :contain' +
        'er_owner_id, '
      
        '   :carriage_num, :carriage_type, :carriage_owner_id, :cargotype' +
        '_id, :cargo_description, '
      
        '   :seal_number, :weight_sender, :weight_doc, :weight_fact, :for' +
        'warder_id, '
      
        '   :consignee_id, :payer_id, :dlv_type_id, :dlv_term, :guard_sig' +
        'n, :docreturn_sign, '
      
        '   :emptyreturn_sign, :need_pay, :isempty, :emptyreturn_date, :c' +
        'ontainer_kind_id, :plugstart, :plugend );'
      ''
      '')
    InsertCommand.Parameters = <
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'train_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_foot'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'carriage_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'carriage_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'carriage_owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cargotype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cargo_description'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2000
        Value = Null
      end
      item
        Name = 'seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'weight_sender'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_doc'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_fact'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'forwarder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'consignee_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'payer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dlv_term'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'guard_sign'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'docreturn_sign'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'emptyreturn_sign'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'need_pay'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'isempty'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'emptyreturn_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'container_kind_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'plugstart'
        Size = -1
        Value = Null
      end
      item
        Name = 'plugend'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      
        'delete from documents where  id = :id and isnull(isconfirmed,0)=' +
        '0 '
      
        'and not exists (select 1 from docfeed f, doccargosheet s where s' +
        '.id = documents.id and f.id = s.unload_doc_id)')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select '
      
        'd.id, d.doc_number,  d.doc_date, d.folder_id, d.doctype_id, s.pr' +
        'efix, d.inwait, d.datestopinwait,  '
      
        '(select code from doctypes t2 where t2.id = d.doctype_id) as doc' +
        'type,'
      
        '(case when isnull(d.isconfirmed,0) = 1 then '#39' +'#39' else '#39#39' end) as' +
        ' state_code,'
      's.datesent, s.unload_doc_id,'
      
        '(case when ps1.cnt>1 then '#39'**'#39' else ps1.pass_number end) as pass' +
        '_number,'
      'dbo.f_GetDocUsedProcs(d.id, '#39'-4-18-'#39') as usedproc,'
      'dbo.get_restr_state(d.id) as restr_sign,'
      
        '(select 1 where not exists (select 1 from doccargorestrictions r' +
        ' where r.doc_id = d.id and r.dateend is null)) as restr_closed,'
      
        'isnull((select '#39#1045#1089#1090#1100#39' where exists (select 1 from doccargoletter' +
        's r where r.doc_id = d.id)),'#39'    '#39') as letter_sign,'
      
        '(select sum(summa) from doccargoservices sr where sr.doc_id = d.' +
        'id) as services_summa,'
      's.train_num,'
      's.container_num,'
      
        '(select sk1.code from containers c1, v_lastobjectstates los1, ob' +
        'jectstatekinds sk1 where c1.id = los1.object_id and los1.task_id' +
        ' = s.task_id and sk1.id = los1.object_state_id and c1.cnum = s.c' +
        'ontainer_num) container_state,'
      's.container_kind_id, s.container_foot, '
      
        'isnull((select code from containerkinds ck where ck.id = s.conta' +
        'iner_kind_id), s.container_foot) as container_kind_text, '
      
        '(select code from counteragents c where c.id = s.container_owner' +
        '_id) as container_owner,'
      'incplan.income_plan, '
      'incplan.unloadfront,'
      's.carriage_num,'
      's.carriage_type,'
      
        '(select code from counteragents c where c.id = s.carriage_owner_' +
        'id) as carriage_owner,'
      
        '(select code from cargotypes t where t.id = s.cargotype_id) as c' +
        'argotype,'
      's.seal_number,'
      's.weight_sender,'
      's.weight_doc,'
      's.weight_fact,'
      
        '(select code from counteragents c where c.id = s.forwarder_id) a' +
        's forwarder,'
      
        '(select code from counteragents c where c.id = s.consignee_id) a' +
        's consignee,'
      
        '(select code from counteragents c where c.id = s.payer_id) as pa' +
        'yer,'
      
        '(select code from deliverytypes t where t.id = s.dlv_type_id) as' +
        ' dlvtype,'
      's.dlv_term,'
      's.task_id, '
      's.forwarder_id,'
      's.consignee_id,'
      's.payer_id,'
      's.dlv_type_id,'
      's.container_owner_id,'
      's.carriage_owner_id,'
      's.cargotype_id,'
      's.isempty,'
      's.guard_sign,'
      's.docreturn_sign,'
      's.emptyreturn_sign,'
      's.emptyreturn_date,'
      's.cargo_description,'
      's.need_pay, '
      's.alert_date,'
      's.last_alert_date,'
      'd.date_created,'
      'd.date_modified,'
      'd.isconfirmed,'
      'd.date_confirm, '
      
        '(select user_name from users u where u.id = d.user_created) as u' +
        'ser_created,'
      'd.isdeleted,'
      'd.isabolished,'
      'notifysign, '
      'paidsumm, '
      'isnull(paidfull,0) as paidfull, '
      'paiddate_register, '
      'isnull(dlvallowed,0) as dlvallowed,'
      'dlvallowed_date, '
      'dlvallowed_note, '
      'paydocs, '
      'plugstart, '
      'plugend,'
      '(case when plugstart is null then 0 else 1 end) as isplugged '
      'from documents d,  doccargosheet s'
      'left outer join ('
      
        '     select po.task_id, osh.doc_id, min(p.pass_number) as pass_n' +
        'umber, count(*) as cnt '
      '     from passes p, passoperations po, objectstatehistory osh '
      
        '     where p.id = po.parent_id and po.task_id = osh.task_id and ' +
        'po.container_id = osh.object_id'
      '     and isnull(p.isabolished,0) = 0  '
      '     group by po.task_id, osh.doc_id'
      ') ps1 on (ps1.task_id = s.task_id and s.id = ps1.doc_id)'
      'left outer join '
      '('
      
        '  select f.id, isnull(l.note,'#39'???'#39')+'#39' , '#39'+convert(varchar, plan_' +
        'date, 104) as income_plan, plan_date, '
      
        '  (select code from unloadfronts uf where uf.id = f.unload_front' +
        '_id) as unloadfront'
      '  from loadplan l, docfeed f where l.id = f.load_plan_id '
      ') as incplan'
      'on (incplan.id = s.unload_doc_id)'
      'where d.id = s.id and d.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 224
    Top = 280
  end
  inherited meData: TMemTableEh
    AfterOpen = meDataAfterOpen
    Left = 272
    Top = 280
  end
  inherited dsData: TDataSource
    Left = 320
    Top = 280
  end
  inherited qrAux: TADOQuery
    Left = 288
  end
  inherited meForms: TMemTableEh
    Left = 588
  end
  inherited dsForms: TDataSource
    Left = 636
  end
  inherited pmDocFlow: TPopupMenu
    Left = 224
    Top = 200
    object N25: TMenuItem [3]
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1075#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1103' '#1074' '#1087#1083#1072#1090#1077#1083#1100#1097#1080#1082#1072
      OnClick = N25Click
    end
    object N23: TMenuItem [4]
      Caption = '-'
    end
    object N27: TMenuItem [5]
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1091#1074#1077#1076#1086#1084#1083#1077#1085#1080#1103' '#1087#1086' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1084
      OnClick = N27Click
    end
    object N24: TMenuItem [6]
      Caption = #1055#1088#1086#1089#1090#1072#1074#1080#1090#1100' '#1076#1072#1090#1091' '#1091#1074#1077#1076#1086#1084#1083#1077#1085#1080#1103' '#1087#1086' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1084
      OnClick = N24Click
    end
    object N26: TMenuItem [7]
      Caption = #1055#1088#1086#1089#1090#1072#1074#1080#1090#1100' '#1086#1090#1084#1077#1090#1082#1091' '#1086' '#1074#1086#1079#1074#1088#1072#1090#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074' '#1087#1086' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1084
      OnClick = N26Click
    end
    object N17: TMenuItem [8]
      Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      ShortCut = 16465
      OnClick = sbExtraInfoClick
    end
    object N21: TMenuItem [11]
      Caption = #1055#1086#1076#1072#1095#1072'/'#1074#1099#1075#1088#1091#1079#1082#1072
      ShortCut = 32856
      OnClick = sbRegisterDateClick
    end
    object N20: TMenuItem [16]
      Caption = #1057#1074#1077#1088#1085#1091#1090#1100' '#1074#1089#1077
      ShortCut = 16471
      OnClick = N20Click
    end
    object N22: TMenuItem [20]
      Caption = #1054#1090#1073#1086#1088' '#1087#1086' '#1087#1088#1080#1084#1077#1095#1072#1085#1080#1102
      ShortCut = 32838
      OnClick = N22Click
    end
    object N15: TMenuItem
      Caption = '-'
      Visible = False
    end
    object N16: TMenuItem
      Caption = #1052#1072#1089#1089#1086#1074#1086#1077' '#1079#1072#1087#1086#1083#1085#1077#1085#1080#1077' '#1076#1072#1085#1085#1099#1093
      Visible = False
      OnClick = N16Click
    end
  end
  inherited al: TActionList
    Left = 408
  end
  inherited drvLinkedObjects: TADODataDriverEh
    Top = 482
  end
  inherited meLinkedObjects: TMemTableEh
    Top = 482
  end
  inherited dsLinkedObjects: TDataSource
    Top = 482
  end
  inherited pmLinkedObjects: TPopupMenu
    Top = 480
  end
  inherited qrReport: TADOQuery
    Left = 704
  end
  object meMass: TMemTableEh [24]
    Params = <>
    Left = 550
    Top = 304
  end
  inherited mePass: TMemTableEh
    BeforeOpen = nil
    AfterOpen = nil
    AfterInsert = nil
    BeforeEdit = nil
    AfterEdit = nil
    BeforePost = nil
    AfterPost = nil
    AfterScroll = nil
  end
  inherited IL: TPngImageList
    Bitmap = {}
  end
end
