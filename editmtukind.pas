﻿unit EditMtuKind;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, editcodename, DBSQLLookUp, Data.DB,
  Data.Win.ADODB, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, Vcl.ExtCtrls;

type
  TFormEditMtuKind = class(TFormEditCodeName)
    ssPictureKinds: TADOLookUpSqlSet;
    luPictureKind: TDBSQLLookUp;
    Label4: TLabel;
    Label3: TLabel;
    edLoadCode: TDBEditEh;
    edAmount: TDBEditEh;
    Label5: TLabel;
    edNote: TDBEditEh;
    Label6: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditMtuKind: TFormEditMtuKind;

implementation

{$R *.dfm}

end.
