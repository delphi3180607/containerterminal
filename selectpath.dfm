﻿inherited FormSelectPath: TFormSelectPath
  Caption = #1042#1099#1073#1088#1072#1090#1100' '#1087#1091#1090#1100
  ClientHeight = 292
  ClientWidth = 454
  ExplicitWidth = 460
  ExplicitHeight = 320
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 251
    Width = 454
    inherited btnCancel: TButton
      Left = 338
    end
    inherited btnOk: TButton
      Left = 219
      Caption = #1042#1099#1073#1088#1072#1090#1100
    end
  end
  object dgData: TDBGridEh [1]
    Left = 0
    Top = 0
    Width = 454
    Height = 251
    Align = alClient
    DataSource = dsLocal
    DynProps = <>
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    IndicatorOptions = [gioShowRowIndicatorEh]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
    TabOrder = 1
    Columns = <
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'code'
        Footers = <>
        Title.Caption = #1055#1091#1090#1100
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -13
        Title.Font.Name = 'Verdana'
        Title.Font.Style = [fsBold]
        Width = 350
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited dsLocal: TDataSource
    DataSet = qrAux
    Left = 152
    Top = 120
  end
  inherited qrAux: TADOQuery
    SQL.Strings = (
      'select * from paths order by code')
    Left = 96
    Top = 120
  end
end
