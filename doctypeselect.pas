﻿unit doctypeselect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Data.DB, Data.Win.ADODB;

type
  TFormDocTypeSelect = class(TFormEdit)
    dbData: TDBGridEh;
    dsAux: TDataSource;
    procedure FormActivate(Sender: TObject);
    procedure dbDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormDocTypeSelect: TFormDocTypeSelect;

implementation

{$R *.dfm}

procedure TFormDocTypeSelect.dbDataDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

  inherited;

  if dsLocal.DataSet.FieldByName('pref_sign').AsBoolean then
    if Column.FieldName = 'pref_sign' then
    begin
      dbData.Canvas.Brush.Color := clGreen;
      dbData.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;

end;

procedure TFormDocTypeSelect.FormActivate(Sender: TObject);
begin
  dbData.SetFocus;
end;

end.
