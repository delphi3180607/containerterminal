﻿inherited FormEditContainerInDoc: TFormEditContainerInDoc
  Caption = #1044#1072#1085#1085#1099#1077' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
  ClientHeight = 463
  ClientWidth = 631
  ExplicitWidth = 637
  ExplicitHeight = 491
  PixelsPerInch = 96
  TextHeight = 16
  object Shape1: TShape [0]
    Left = 7
    Top = 23
    Width = 616
    Height = 170
    Brush.Style = bsClear
    Pen.Color = clGray
    Pen.Style = psDot
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 3
    Width = 79
    Height = 16
    Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel [2]
    Left = 16
    Top = 37
    Width = 42
    Height = 16
    Caption = #1053#1086#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel [3]
    Left = 175
    Top = 37
    Width = 108
    Height = 16
    Caption = #1042#1080#1076' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel [4]
    Left = 351
    Top = 37
    Width = 24
    Height = 16
    Caption = #1042#1077#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel [5]
    Left = 455
    Top = 37
    Width = 124
    Height = 16
    Caption = #1043#1088#1091#1079#1086#1087#1086#1076#1100#1077#1084#1085#1086#1089#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label17: TLabel [6]
    Left = 16
    Top = 88
    Width = 130
    Height = 16
    Caption = #1044#1072#1090#1072' '#1080#1079#1075#1086#1090#1086#1074#1083#1077#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel [7]
    Left = 175
    Top = 88
    Width = 98
    Height = 16
    Caption = #1044#1072#1090#1072' '#1086#1089#1074#1080#1076#1077#1090'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel [8]
    Left = 351
    Top = 88
    Width = 172
    Height = 16
    Caption = #1057#1090#1080#1082#1077#1088' ('#1076#1072#1090#1072' '#1089#1083#1077#1076'. '#1086#1089#1074'.)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel [9]
    Left = 271
    Top = 145
    Width = 159
    Height = 13
    Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1080#1081' ID '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 422
    Width = 631
    inherited btnCancel: TButton
      Left = 515
    end
    inherited btnOk: TButton
      Left = 396
    end
  end
  object edContNum: TDBEditEh [11]
    Left = 16
    Top = 55
    Width = 151
    Height = 24
    DataField = 'container_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    MaxLength = 11
    TabOrder = 1
    Visible = True
    EditMask = 'cccc9999999;0;?'
  end
  object leContainerKind: TDBSQLLookUp [12]
    Left = 175
    Top = 55
    Width = 170
    Height = 24
    DataField = 'containerkind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
    SqlSet = ssContainerKind
  end
  object nuWeight: TDBNumberEditEh [13]
    Left = 351
    Top = 55
    Width = 98
    Height = 24
    DataField = 'container_weight'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object nuCarrying: TDBNumberEditEh [14]
    Left = 455
    Top = 55
    Width = 124
    Height = 24
    DataField = 'carrying'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = True
  end
  object dtDateMade: TDBDateTimeEditEh [15]
    Left = 16
    Top = 107
    Width = 153
    Height = 24
    DataField = 'date_made'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
  object dtDateStiker: TDBDateTimeEditEh [16]
    Left = 351
    Top = 107
    Width = 170
    Height = 24
    DataField = 'date_stiker'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 6
    Visible = True
  end
  object cbPicture: TDBCheckBoxEh [17]
    Left = 537
    Top = 107
    Width = 63
    Height = 17
    Caption = #1060#1086#1090#1086
    DataField = 'picture'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 7
  end
  object dtDateInspect: TDBComboBoxEh [18]
    Left = 175
    Top = 107
    Width = 170
    Height = 24
    DataField = 'date_inspection'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 8
    Visible = True
  end
  object edContainerId: TDBEditEh [19]
    Left = 271
    Top = 159
    Width = 329
    Height = 21
    DataField = 'container_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    Visible = True
  end
  object ssContainerKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from containerkinds order by code')
    DownSql.Strings = (
      'select * from containerkinds order by code')
    InitSql.Strings = (
      'select * from containerkinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 248
    Top = 41
  end
end
