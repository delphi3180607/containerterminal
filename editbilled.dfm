﻿inherited FormEditBilled: TFormEditBilled
  Caption = #1055#1088#1080#1074#1103#1079#1082#1072' '#1089#1095#1077#1090#1072
  ClientHeight = 443
  ClientWidth = 916
  PopupMenu = pmMemo
  ExplicitWidth = 922
  ExplicitHeight = 471
  PixelsPerInch = 96
  TextHeight = 16
  object sbDistrib: TSpeedButton [0]
    Left = 667
    Top = 32
    Width = 118
    Height = 26
    Caption = #1056#1072#1089#1087#1088#1077#1076#1077#1083#1080#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = sbDistribClick
  end
  inherited plBottom: TPanel
    Top = 402
    Width = 916
    TabOrder = 4
    ExplicitTop = 402
    ExplicitWidth = 916
    inherited btnCancel: TButton
      Left = 800
      ExplicitLeft = 800
    end
    inherited btnOk: TButton
      Left = 681
      OnClick = btnOkClick
      ExplicitLeft = 681
    end
  end
  object edDocNumber: TDBEditEh [2]
    Left = 220
    Top = 32
    Width = 161
    Height = 26
    ControlLabel.Width = 105
    ControlLabel.Height = 18
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1089#1095#1077#1090#1072
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -16
    ControlLabel.Font.Name = 'Verdana'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 0
    Visible = True
  end
  object edDocDate: TDBDateTimeEditEh [3]
    Left = 396
    Top = 32
    Width = 121
    Height = 26
    ControlLabel.Width = 91
    ControlLabel.Height = 18
    ControlLabel.Caption = #1044#1072#1090#1072' '#1089#1095#1077#1090#1072
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -16
    ControlLabel.Font.Name = 'Verdana'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
    Kind = dtkDateEh
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 1
    Visible = True
  end
  object nuDocSumma: TDBNumberEditEh [4]
    Left = 532
    Top = 32
    Width = 129
    Height = 26
    ControlLabel.Width = 52
    ControlLabel.Height = 18
    ControlLabel.Caption = #1057#1091#1084#1084#1072
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -16
    ControlLabel.Font.Name = 'Verdana'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 2
    Visible = True
    OnExit = nuDocSummaExit
  end
  object pcLayout: TPageControl [5]
    Left = 0
    Top = 77
    Width = 916
    Height = 325
    ActivePage = TabSheet2
    Align = alBottom
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = #1055#1086#1080#1089#1082' '#1087#1086' '#1085#1086#1084#1077#1088#1072#1084' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074
      object meNumbers: TMemo
        AlignWithMargins = True
        Left = 3
        Top = 42
        Width = 902
        Height = 249
        Align = alClient
        TabOrder = 0
        OnChange = meNumbersChange
      end
      object btSeek: TButton
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 902
        Height = 33
        Align = alTop
        Caption = #1053#1072#1081#1090#1080' '#1079#1072#1103#1074#1082#1080
        TabOrder = 1
        OnClick = btSeekClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1055#1088#1080#1074#1103#1079#1082#1072' '#1082' '#1086#1087#1077#1088#1072#1094#1080#1103#1084
      ImageIndex = 1
      object Label1: TLabel
        Left = 0
        Top = 278
        Width = 908
        Height = 16
        Align = alBottom
        Caption = 
          #1042#1085#1080#1084#1072#1085#1080#1077'! '#1047#1072#1087#1080#1089#1080' '#1073#1077#1079' '#1086#1090#1084#1077#1090#1086#1082' '#1087#1088#1080' '#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1080' '#1087#1088#1080#1074#1103#1079#1082#1080' '#1089#1095#1077#1090#1072' '#1091#1095#1080 +
          #1090#1099#1074#1072#1090#1100#1089#1103' '#1085#1077' '#1073#1091#1076#1091#1090'!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 5727656
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        StyleElements = [seClient, seBorder]
        ExplicitWidth = 618
      end
      object plTool: TPanel
        Left = 0
        Top = 0
        Width = 908
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object btCheckAll: TButton
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 170
          Height = 27
          Align = alLeft
          Caption = #1055#1086#1089#1090#1072#1074#1080#1090#1100' '#1074#1089#1077' '#1086#1090#1084#1077#1090#1082#1080
          TabOrder = 0
          TabStop = False
          OnClick = btCheckAllClick
        end
        object btUnCheckAll: TButton
          AlignWithMargins = True
          Left = 179
          Top = 3
          Width = 170
          Height = 27
          Align = alLeft
          Caption = #1057#1073#1088#1086#1089#1080#1090#1100' '#1074#1089#1077' '#1086#1090#1084#1077#1090#1082#1080
          TabOrder = 1
          TabStop = False
          OnClick = btUnCheckAllClick
        end
      end
      object dgData: TDBGridEh
        Left = 0
        Top = 33
        Width = 908
        Height = 245
        Align = alClient
        Border.Color = clSilver
        ColumnDefValues.Title.TitleButton = True
        ColumnDefValues.ToolTips = True
        Ctl3D = False
        DataSource = dsLocal
        DynProps = <>
        FixedColor = clCream
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        GridLineParams.BrightColor = clSilver
        GridLineParams.ColorScheme = glcsClassicEh
        GridLineParams.DarkColor = clSilver
        GridLineParams.DataBoundaryColor = clSilver
        GridLineParams.DataHorzColor = 15395041
        GridLineParams.DataVertColor = 15395041
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
        IndicatorParams.Color = clBtnFace
        IndicatorParams.HorzLineColor = clSilver
        IndicatorParams.VertLineColor = clSilver
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        OptionsEh = [dghHighlightFocus, dghAutoSortMarking, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
        ParentCtl3D = False
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        SearchPanel.Enabled = True
        SearchPanel.PersistentShowing = False
        SelectionDrawParams.SelectionStyle = gsdsClassicEh
        SelectionDrawParams.DrawFocusFrame = False
        SelectionDrawParams.DrawFocusFrameStored = True
        ShowHint = True
        SortLocal = True
        STFilter.Color = clWhite
        STFilter.HorzLineColor = clSilver
        STFilter.InstantApply = True
        STFilter.Local = True
        STFilter.VertLineColor = clSilver
        STFilter.Visible = True
        TabOrder = 1
        TitleParams.Color = clBtnFace
        TitleParams.FillStyle = cfstSolidEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -11
        TitleParams.Font.Name = 'Verdana'
        TitleParams.Font.Style = [fsBold]
        TitleParams.HorzLineColor = clSilver
        TitleParams.MultiTitle = True
        TitleParams.ParentFont = False
        TitleParams.SecondColor = clCream
        TitleParams.VertLineColor = clSilver
        OnSelectedRowsItemChanged = dgDataSelectedRowsItemChanged
        Columns = <
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'docorder_data'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072', '#1085#1086#1084#1077#1088' '#1079#1072#1103#1074#1082#1080
            Width = 120
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
            Width = 86
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'state_code'
            Footers = <>
            Title.Caption = #1057#1090#1072#1090#1091#1089' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
            Width = 97
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'containerkind_code'
            Footers = <>
            Title.Caption = #1058#1080#1087
            Width = 49
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'weight_fact'
            Footers = <>
            Title.Caption = #1042#1077#1089
            Width = 60
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cargotype_code'
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1075#1088#1091#1079#1072
            Width = 153
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'payer_code'
            Footers = <>
            Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082' ('#1082#1083#1080#1077#1085#1090')'
            Width = 122
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DisplayFormat = '### ### ##0.00'
            DynProps = <>
            EditButtons = <>
            FieldName = 'summa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            Footers = <>
            Title.Caption = #1057#1091#1084#1084#1072
            Width = 81
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            Color = 8580619
            DisplayFormat = '### ### ##0.00'
            DynProps = <>
            EditButtons = <>
            FieldName = 'linked_summa'
            Footers = <>
            Title.Caption = #1057#1074#1103#1079#1072#1085#1085#1072#1103' '#1089#1091#1084#1084#1072
            Width = 76
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited dsLocal: TDataSource
    DataSet = meData
    Left = 136
    Top = 336
  end
  inherited qrAux: TADOQuery
    Parameters = <
      item
        Name = 'guid'
        DataType = ftWideString
        Size = 1
        Value = ' '
      end>
    Prepared = True
    SQL.Strings = (
      'select '
      'c.id as container_id,'
      't.id as task_id,'
      
        'convert(varchar(10),d.doc_date, 104)+'#39' / '#39'+d.doc_number as docor' +
        'der_data, '
      
        'c.cnum, (select code from objectstatekinds osk where osk.id = lo' +
        's.object_state_id) as state_code,'
      
        '(select code from deliverytypes dlv where dlv.id = t.dlv_type_id' +
        ') as deliverytype_code, '
      
        '(select code from containerkinds ck where ck.id = c.kind_id) as ' +
        'containerkind_code, '
      
        '(select code+'#39', '#39'+name from cargotypes crt where crt.id = cr.typ' +
        'e_id) as cargotype_code, '
      'cr.weight_fact, '
      
        '(select code from counteragents ct1 where ct1.id = t.payer_id) a' +
        's payer_code,'
      '10000001.00 as summa,'
      
        '(select sum(invoice_amount) from billed b where b.container_id =' +
        ' c.id and b.task_id = t.id) as linked_summa'
      
        'from docorder od, documents d, docorderspec dos, v_lastobjectsta' +
        'tes los, '
      'tasks t, objectstatehistory osh, cargos cr, containers c'
      'where d.id = od.id and dos.doc_id = od.id'
      'and los.task_id = t.id and los.object_id = c.id'
      'and t.object_id = cr.id '
      'and osh.task_id = t.id and osh.object_id = c.id '
      'and osh.doc_id = od.id and dos.container_id = c.id'
      
        'and t.dlv_type_id in (select id from deliverytypes d where d.glo' +
        'bal_section = '#39'output'#39')'
      'and dbo.filter_objects(:guid, c.cnum) = 1')
    Left = 216
    Top = 336
  end
  object meData: TMemTableEh
    Params = <>
    Left = 292
    Top = 334
  end
  object pmMemo: TPopupMenu
    Left = 396
    Top = 227
    object N1: TMenuItem
      Caption = #1053#1072#1081#1090#1080' '#1079#1072#1103#1074#1082#1080
      ShortCut = 16397
      OnClick = btSeekClick
    end
  end
end
