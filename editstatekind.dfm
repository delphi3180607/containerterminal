﻿inherited FormEditStateKind: TFormEditStateKind
  Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
  ClientHeight = 340
  ExplicitHeight = 368
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 15
    Top = 7
    Width = 24
    Height = 16
    Caption = #1050#1086#1076
  end
  object Label3: TLabel [1]
    Left = 15
    Top = 62
    Width = 98
    Height = 16
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  inherited plBottom: TPanel
    Top = 299
    TabOrder = 8
    ExplicitTop = 299
  end
  object edCode: TDBEditEh [3]
    Left = 15
    Top = 25
    Width = 305
    Height = 24
    DataField = 'code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object edName: TDBEditEh [4]
    Left = 15
    Top = 81
    Width = 505
    Height = 24
    DataField = 'name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object cbInplaceSign: TDBCheckBoxEh [5]
    Left = 15
    Top = 128
    Width = 298
    Height = 17
    Caption = #1055#1088#1080#1079#1085#1072#1082' '#1085#1072' '#1090#1077#1088#1084#1080#1085#1072#1083#1077
    DataField = 'inplacesign'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 3
  end
  object cbInWork: TDBCheckBoxEh [6]
    Left = 15
    Top = 160
    Width = 298
    Height = 17
    Caption = #1055#1088#1080#1079#1085#1072#1082' '#1074' '#1088#1072#1073#1086#1090#1077
    DataField = 'inworksign'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 4
  end
  object cbIsFormal: TDBCheckBoxEh [7]
    Left = 15
    Top = 192
    Width = 298
    Height = 17
    Caption = #1055#1088#1080#1079#1085#1072#1082' '#1092#1086#1088#1084#1072#1083#1100#1085#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
    DataField = 'isformalsign'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 5
  end
  object cbAllowRepeat: TDBCheckBoxEh [8]
    Left = 15
    Top = 224
    Width = 298
    Height = 17
    Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086' '#1087#1086#1074#1090#1086#1088#1085#1086#1077' '#1086#1092#1086#1088#1084#1083#1077#1085#1080#1077
    DataField = 'allow_repeat'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 6
  end
  object cbStop: TDBCheckBoxEh [9]
    Left = 15
    Top = 257
    Width = 298
    Height = 17
    Caption = #1055#1088#1080#1079#1085#1072#1082' '#1057#1058#1054#1055
    DataField = 'stop_sign'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 7
  end
  object edMark: TDBEditEh [10]
    Left = 335
    Top = 25
    Width = 185
    Height = 24
    ControlLabel.Width = 132
    ControlLabel.Height = 16
    ControlLabel.Caption = #1054#1090#1084#1077#1090#1082#1072' '#1087#1088#1086#1094#1077#1089#1089#1072' '
    ControlLabel.Visible = True
    DataField = 'mark'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  inherited dsLocal: TDataSource
    Top = 33
  end
end
