﻿unit editmtuandpicture;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  DBSQLLookUp, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls, Vcl.Buttons, Functions;

type
  TFormEditMtuAndPicture = class(TFormEdit)
    ssMtuKind: TADOLookUpSqlSet;
    luMtuKind: TDBSQLLookUp;
    Label3: TLabel;
    Label4: TLabel;
    luPictureKind: TDBSQLLookUp;
    ssPictureKinds: TADOLookUpSqlSet;
    sbMTU: TSpeedButton;
    procedure luMtuKindKeyValueChange(Sender: TObject);
    procedure sbMTUClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditMtuAndPicture: TFormEditMtuAndPicture;

implementation

{$R *.dfm}

uses mtukinds;

procedure TFormEditMtuAndPicture.luMtuKindKeyValueChange(Sender: TObject);
begin
  luPictureKind.KeyValue := ssMtuKind.ListDataSet.FieldByName('picture_id').AsInteger;
end;

procedure TFormEditMtuAndPicture.sbMTUClick(Sender: TObject);
begin
  if SFDE(TFormMtuKinds,self.luMtuKind, true) then
  luPictureKind.KeyValue := FormMtuKinds.meData.FieldByName('picture_id').AsInteger;
end;

end.
