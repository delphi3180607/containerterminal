﻿unit selectobjects;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Buttons, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.StdCtrls, Vcl.ExtCtrls, PngSpeedButton, Vcl.Mask,
  DBCtrlsEh;

type
  TFormSelectObjects = class(TFormEdit)
    plLeft: TPanel;
    plCaption: TPanel;
    dgList: TDBGridEh;
    plMiddle: TPanel;
    sbAdd: TSpeedButton;
    sbDelete: TSpeedButton;
    plRight: TPanel;
    plCaption1: TPanel;
    drvData: TADODataDriverEh;
    meData: TMemTableEh;
    dsData: TDataSource;
    meSelected: TMemTableEh;
    dsSelected: TDataSource;
    DBGridEh1: TDBGridEh;
    nuLen: TDBNumberEditEh;
    procedure sbAddClick(Sender: TObject);
    procedure sbDeleteClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure SetFilter(objecttype: string);
  end;

var
  FormSelectObjects: TFormSelectObjects;

implementation

{$R *.dfm}


procedure TFormSelectObjects.sbAddClick(Sender: TObject);
var id: integer;
begin
  if not meSelected.Active then
  begin
    meSelected.LoadFromDataSet(meData,1,lmCopy,false);
    nuLen.Value := nuLen.Value + meData.FieldByName('car_length').AsFloat;
    exit;
  end;
  id := meData.FieldByName('id').AsInteger;
  meSelected.First;
  if not meSelected.Locate('id', id, []) then
  begin
    meSelected.LoadFromDataSet(meData,1,lmAppend,false);
    nuLen.Value := nuLen.Value + meData.FieldByName('car_length').AsFloat;
  end;
end;

procedure TFormSelectObjects.sbDeleteClick(Sender: TObject);
begin
  meSelected.Delete;
  nuLen.Value := nuLen.Value - meData.FieldByName('car_length').AsFloat;
end;

procedure TFormSelectObjects.SetFilter(objecttype: string);
begin
  meData.Close;
  drvData.SelectCommand.Parameters.ParamByName('objecttype').Value := objecttype;
  meData.Open;
  meSelected.Open;
end;


end.
