﻿inherited FormEditObjectLink: TFormEditObjectLink
  Caption = #1054#1073#1100#1077#1082#1090#1099' '#1086#1087#1077#1088#1072#1094#1080#1080
  ClientHeight = 497
  ClientWidth = 524
  OnCreate = FormCreate
  ExplicitWidth = 530
  ExplicitHeight = 525
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 8
    Top = 76
    Width = 134
    Height = 16
    Caption = #1062#1077#1083#1077#1074#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
  end
  object Label5: TLabel [1]
    Left = 8
    Top = 324
    Width = 106
    Height = 16
    Caption = #1064#1072#1073#1083#1086#1085' '#1087#1080#1089#1100#1084#1072
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 10
    Width = 156
    Height = 16
    Caption = #1042#1080#1076' '#1086#1073#1100#1077#1082#1090#1072' '#1076#1083#1103' '#1089#1074#1103#1079#1080
  end
  object Label3: TLabel [3]
    Left = 8
    Top = 146
    Width = 69
    Height = 16
    Caption = #1058#1080#1087' '#1089#1074#1103#1079#1080
  end
  object sbClear: TSpeedButton [4]
    Left = 485
    Top = 168
    Width = 23
    Height = 24
    Caption = 'X'
    OnClick = sbClearClick
  end
  object Label4: TLabel [5]
    Left = 8
    Top = 214
    Width = 102
    Height = 16
    Caption = #1058#1080#1087' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
  end
  inherited plBottom: TPanel
    Top = 456
    Width = 524
    TabOrder = 6
    ExplicitTop = 456
    ExplicitWidth = 524
    inherited btnCancel: TButton
      Left = 408
      ExplicitLeft = 408
    end
    inherited btnOk: TButton
      Left = 289
      ExplicitLeft = 289
    end
  end
  object leStateKind: TDBSQLLookUp [7]
    Left = 8
    Top = 98
    Width = 505
    Height = 24
    DataField = 'objectstate_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    SqlSet = ssStateKind
  end
  object cbSendMail: TDBCheckBoxEh [8]
    Left = 8
    Top = 286
    Width = 304
    Height = 17
    Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100' '#1086#1087#1086#1074#1077#1097#1077#1085#1080#1077' '#1075#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1102
    DataField = 'send_mail_consignee'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 4
  end
  object meMailTemplate: TDBMemoEh [9]
    Left = 8
    Top = 346
    Width = 505
    Height = 89
    AutoSize = False
    DataField = 'mail_template'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 5
    Visible = True
  end
  object cbObjectType: TDBComboBoxEh [10]
    Left = 8
    Top = 32
    Width = 505
    Height = 24
    DataField = 'object_type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      #1043#1088#1091#1079
      #1050#1086#1085#1090#1077#1081#1085#1077#1088
      #1042#1072#1075#1086#1085)
    KeyItems.Strings = (
      'cargos'
      'containers'
      'carriages')
    TabOrder = 0
    Visible = True
  end
  object cbRelation: TDBComboBoxEh [11]
    Left = 8
    Top = 168
    Width = 473
    Height = 24
    DataField = 'relation'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      #1057#1086#1077#1076#1080#1085#1077#1085#1080#1077
      #1056#1072#1079#1100#1077#1076#1080#1085#1077#1085#1080#1077
      #1057#1086#1093#1088#1072#1085#1077#1085#1080#1077)
    KeyItems.Strings = (
      '1'
      '2'
      '3')
    TabOrder = 2
    Visible = True
  end
  object cbUpdateType: TDBComboBoxEh [12]
    Left = 8
    Top = 236
    Width = 505
    Height = 24
    DataField = 'update_type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      #1048#1079#1084#1077#1085#1077#1085#1080#1077
      #1041#1083#1086#1082#1080#1088#1086#1074#1072#1085#1080#1077)
    KeyItems.Strings = (
      '1'
      '2')
    TabOrder = 3
    Visible = True
  end
  object ssStateKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from statekinds order by code')
    DownSql.Strings = (
      'select * from statekinds order by code')
    InitSql.Strings = (
      'select * from statekinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 186
    Top = 79
  end
end
