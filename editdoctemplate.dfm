﻿inherited FormEditDocTemplate: TFormEditDocTemplate
  Caption = #1064#1072#1073#1083#1086#1085' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 531
  ClientWidth = 862
  ExplicitWidth = 868
  ExplicitHeight = 559
  PixelsPerInch = 96
  TextHeight = 16
  object Label7: TLabel [0]
    Left = 7
    Top = 7
    Width = 165
    Height = 16
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1086#1090#1095#1077#1090#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel [1]
    Left = 7
    Top = 62
    Width = 59
    Height = 16
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = #1064#1072#1073#1083#1086#1085
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel [2]
    Left = 8
    Top = 125
    Width = 54
    Height = 16
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = #1047#1072#1087#1088#1086#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel [3]
    Left = 454
    Top = 63
    Width = 164
    Height = 16
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = #1043#1088#1091#1087#1087#1080#1088#1086#1074#1086#1095#1085#1086#1077' '#1087#1086#1083#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 490
    Width = 862
    TabOrder = 4
    ExplicitTop = 445
    ExplicitWidth = 689
    inherited btnCancel: TButton
      Left = 746
      ExplicitLeft = 573
    end
    inherited btnOk: TButton
      Left = 627
      ExplicitLeft = 454
    end
  end
  object edTitle: TDBEditEh [5]
    Left = 7
    Top = 27
    Width = 436
    Height = 24
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Ctl3D = True
    DataField = 'title'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    ParentCtl3D = False
    TabOrder = 0
    Visible = True
  end
  object edTemplate: TDBEditEh [6]
    Left = 8
    Top = 83
    Width = 435
    Height = 24
    DataField = 'template'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
        Style = ebsEllipsisEh
        OnClick = edTemplateEditButtons0Click
      end>
    TabOrder = 1
    Visible = True
  end
  object meSQL: TDBMemo [7]
    Left = 8
    Top = 146
    Width = 841
    Height = 338
    DataField = 'sql_text'
    DataSource = dsLocal
    TabOrder = 3
  end
  object edGroupField: TDBEditEh [8]
    Left = 454
    Top = 83
    Width = 227
    Height = 24
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Ctl3D = True
    DataField = 'group_field'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    ParentCtl3D = False
    TabOrder = 2
    Visible = True
  end
end
