﻿inherited FormEditOrderNumber: TFormEditOrderNumber
  Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1086
  ClientHeight = 195
  ClientWidth = 290
  ExplicitWidth = 296
  ExplicitHeight = 223
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 79
    Top = 106
    Width = 108
    Height = 16
    Caption = #1055#1083#1086#1084#1073#1072' '#1075#1086#1076#1085#1072#1103'?'
  end
  inherited plBottom: TPanel
    Top = 154
    Width = 290
    TabOrder = 3
    ExplicitTop = 143
    ExplicitWidth = 227
    inherited btnCancel: TButton
      Left = 174
      ExplicitLeft = 111
    end
    inherited btnOk: TButton
      Left = 55
      ExplicitLeft = -8
    end
  end
  object edNumber: TDBEditEh [2]
    Left = 11
    Top = 26
    Width = 198
    Height = 24
    ControlLabel.Width = 94
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1079#1072#1103#1074#1082#1080
    ControlLabel.Visible = True
    DataField = 'external_order_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object cbIsSigned: TDBCheckBoxEh [3]
    Left = 11
    Top = 59
    Width = 193
    Height = 17
    Caption = #1055#1086#1076#1087#1080#1089#1072#1085#1072' ?'
    DataField = 'external_order_signed'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 1
  end
  object cbSealWaste: TDBComboBoxEh [4]
    Left = 11
    Top = 103
    Width = 58
    Height = 24
    DataField = 'issealwaste'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      '+'
      '-'
      '?'
      ' ')
    KeyItems.Strings = (
      '+'
      '-'
      '?'
      ' ')
    LimitTextToListValues = True
    TabOrder = 2
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 96
    Top = 168
  end
  inherited qrAux: TADOQuery
    Left = 32
    Top = 168
  end
end
