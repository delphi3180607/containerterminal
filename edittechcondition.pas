﻿unit edittechcondition;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, editcodename, DBCtrlsEh, Data.DB,
  Data.Win.ADODB, Vcl.StdCtrls, Vcl.Mask, Vcl.ExtCtrls;

type
  TFormEditTechCondition = class(TFormEditCodeName)
    cbDefective: TDBCheckBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditTechCondition: TFormEditTechCondition;

implementation

{$R *.dfm}

end.
