﻿inherited FormSetPrefix: TFormSetPrefix
  Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1077' '#1087#1088#1077#1092#1080#1082#1089' '#1076#1083#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
  ClientHeight = 321
  ClientWidth = 539
  ExplicitWidth = 545
  ExplicitHeight = 349
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 30
    Top = 25
    Width = 267
    Height = 16
    Alignment = taRightJustify
    Caption = #1055#1088#1077#1092#1080#1082#1089' ('#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102': '#1076#1077#1085#1100'+'#1084#1077#1089#1103#1094')'
  end
  object Label2: TLabel [1]
    Left = 33
    Top = 73
    Width = 264
    Height = 16
    Alignment = taRightJustify
    Caption = #1053#1072#1095#1072#1083#1100#1085#1099#1081' '#1085#1086#1084#1077#1088' ('#1087#1091#1089#1090#1086' - '#1072#1074#1090#1086#1084#1072#1090#1086#1084')'
  end
  object Label3: TLabel [2]
    Left = 233
    Top = 121
    Width = 64
    Height = 16
    Alignment = taRightJustify
    Caption = #1053#1086#1084#1077#1088' '#1050#1055
  end
  object Label4: TLabel [3]
    Left = 13
    Top = 174
    Width = 284
    Height = 16
    Alignment = taRightJustify
    Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080' '#1076#1083#1103' '#1075#1088#1091#1078#1077#1085#1099#1093' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074
  end
  object Label5: TLabel [4]
    Left = 12
    Top = 227
    Width = 285
    Height = 16
    Alignment = taRightJustify
    Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080' '#1076#1083#1103' '#1087#1086#1088#1086#1078#1085#1080#1093' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074
  end
  inherited plBottom: TPanel
    Top = 280
    Width = 539
    TabOrder = 5
    ExplicitTop = 280
    ExplicitWidth = 539
    inherited btnCancel: TButton
      Left = 423
      ExplicitLeft = 423
    end
    inherited btnOk: TButton
      Left = 304
      OnClick = btnOkClick
      ExplicitLeft = 304
    end
  end
  object edPrefix: TDBEditEh [6]
    Left = 310
    Top = 21
    Width = 209
    Height = 24
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object neStartNumber: TDBNumberEditEh [7]
    Left = 310
    Top = 69
    Width = 209
    Height = 24
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edKP: TDBEditEh [8]
    Left = 312
    Top = 118
    Width = 207
    Height = 24
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    TabOrder = 2
    Visible = True
  end
  object laDlvTypeLoad: TDBSQLLookUp [9]
    Left = 312
    Top = 172
    Width = 207
    Height = 22
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    HighlightRequired = True
    ParentFont = False
    TabOrder = 3
    Visible = True
    SqlSet = ssDlvTypes
    RowCount = 0
  end
  object laDlvTypeEmpty: TDBSQLLookUp [10]
    Left = 312
    Top = 225
    Width = 207
    Height = 22
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    HighlightRequired = True
    ParentFont = False
    TabOrder = 4
    Visible = True
    SqlSet = ssDlvTypes
    RowCount = 0
  end
  inherited dsLocal: TDataSource
    Left = 32
    Top = 280
  end
  inherited qrAux: TADOQuery
    Left = 80
    Top = 280
  end
  object ssDlvTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from deliverytypes d where exists '
      '('
      'select 1 from docroutes r, doctypes t '
      
        'where r.end_doctype_id = t.id and t.system_section = '#39'income_she' +
        'et'#39
      'and r.dlv_type_id = d.id'
      ')')
    DownSql.Strings = (
      'select * from deliverytypes d where exists '
      '('
      'select 1 from docroutes r, doctypes t '
      
        'where r.end_doctype_id = t.id and t.system_section = '#39'income_she' +
        'et'#39
      'and r.dlv_type_id = d.id'
      ')'
      '')
    InitSql.Strings = (
      'select * from deliverytypes where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 386
    Top = 181
  end
end
