﻿inherited FormDocEmptyIncome: TFormDocEmptyIncome
  ActiveControl = dgData
  Caption = #1055#1088#1080#1077#1084'/'#1074#1099#1076#1072#1095#1072' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074
  PixelsPerInch = 96
  TextHeight = 13
  inherited SplitterVert: TSplitter
    Height = 625
    ExplicitHeight = 625
  end
  inherited plBottom: TPanel
    Top = 625
    Height = 35
    ExplicitTop = 625
    ExplicitHeight = 35
    inherited btnOk: TButton
      Height = 29
      ExplicitHeight = 29
    end
    inherited btnCancel: TButton
      Height = 29
      ExplicitHeight = 29
    end
  end
  inherited plAll: TPanel
    Height = 625
    Caption = ''
    ExplicitHeight = 625
    inherited SplitterHor: TSplitter
      Left = 545
      Top = 63
      Width = 3
      Height = 314
      Cursor = crHSplit
      Align = alRight
      ExplicitLeft = 555
      ExplicitTop = 63
      ExplicitWidth = 3
      ExplicitHeight = 164
    end
    object Splitter1: TSplitter [1]
      Left = 0
      Top = 377
      Width = 733
      Height = 7
      Cursor = crVSplit
      Align = alBottom
      Color = 15132390
      ParentColor = False
      StyleElements = [seFont, seBorder]
      ExplicitTop = 376
    end
    inherited plTop: TPanel
      inherited sbDelete: TPngSpeedButton
        Left = 96
        ExplicitLeft = 138
        ExplicitTop = -3
        ExplicitHeight = 23
      end
      inherited btFilter: TPngSpeedButton
        ExplicitLeft = 159
      end
      inherited btExcel: TPngSpeedButton
        ExplicitLeft = 574
      end
      inherited sbAdd: TPngSpeedButton
        ExplicitLeft = 43
      end
      inherited sbEdit: TPngSpeedButton
        Left = 64
        ExplicitLeft = 64
      end
      inherited Bevel2: TBevel
        ExplicitLeft = 154
      end
      inherited btTool: TPngSpeedButton
        ExplicitLeft = 507
      end
      inherited Bevel1: TBevel
        ExplicitLeft = 279
      end
      inherited btFlow: TPngSpeedButton
        ExplicitLeft = 385
      end
      inherited sbHistory: TPngSpeedButton
        ExplicitLeft = 424
      end
      inherited sbRestrictions: TPngSpeedButton
        ExplicitLeft = 479
      end
      inherited sbConfirm: TPngSpeedButton
        ExplicitLeft = 293
      end
      inherited sbCancelConfirm: TPngSpeedButton
        ExplicitLeft = 332
      end
      inherited Bevel5: TBevel
        ExplicitLeft = 525
      end
      inherited sbReport: TPngSpeedButton
        ExplicitLeft = 532
      end
      inherited Bevel6: TBevel
        ExplicitLeft = 371
      end
      inherited sbImport2: TPngSpeedButton
        Enabled = False
        Visible = False
        ExplicitHeight = 24
      end
      inherited sbSearch: TPngSpeedButton
        ExplicitLeft = 236
      end
      inherited Bevel7: TBevel
        ExplicitLeft = 475
      end
      inherited sbClearFilter: TPngSpeedButton
        ExplicitLeft = 200
      end
      inherited Bevel3: TBevel
        ExplicitLeft = 577
      end
      inherited sbClock: TPngSpeedButton
        ExplicitLeft = 581
      end
      inherited sbBarrel: TPngSpeedButton
        ExplicitLeft = 320
      end
      inherited plCount: TPanel
        inherited edCount: TDBEditEh
          ControlLabel.ExplicitLeft = 16
          ControlLabel.ExplicitTop = -16
          ControlLabel.ExplicitWidth = 103
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 545
      Height = 314
      Font.Height = -13
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 61
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doctype_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 96
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 79
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 102
        end
        item
          CellButtons = <>
          DisplayFormat = '##%'
          DynProps = <>
          EditButtons = <>
          FieldName = 'usedproc'
          Footers = <>
          Title.Caption = '% '#1086#1090#1088#1072#1073
          Width = 55
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'delivery_code'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1086#1087#1077#1088#1072#1094#1080#1080
          Width = 114
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'driver_name'
          Footers = <>
          Title.Caption = #1042#1086#1076#1080#1090#1077#1083#1100
          Width = 160
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'attorney'
          Footers = <>
          Title.Caption = #1044#1086#1074#1077#1088#1077#1085#1085#1086#1089#1090#1100
          Width = 193
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_data'
          Footers = <>
          Title.Caption = #1040#1074#1090#1086#1084#1086#1073#1080#1083#1100
          Width = 115
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'inspector_code'
          Footers = <>
          Title.Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1081
          Width = 135
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_created'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'.'#1089#1086#1079'.'
          Width = 83
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Visible = False
          Width = 96
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dealer_code'
          Footers = <>
          Title.Caption = #1055#1077#1088#1077#1074#1086#1079#1095#1080#1082
          Visible = False
          Width = 152
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'user_created'
          Footers = <>
          Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
          Width = 140
        end>
    end
    inherited plHint: TPanel
      TabOrder = 6
    end
    inherited plLinkedObjects: TPanel
      Left = 551
      Top = 66
      Width = 179
      Height = 308
      Align = alRight
      ExplicitLeft = 551
      ExplicitTop = 66
      ExplicitWidth = 179
      ExplicitHeight = 308
      inherited dgLinkedObjects: TDBGridEh
        Width = 179
        Height = 308
        Border.EdgeBorders = [ebTop]
      end
    end
    object plSpec: TPanel
      Left = 0
      Top = 384
      Width = 733
      Height = 241
      Align = alBottom
      Color = 15921906
      ParentBackground = False
      TabOrder = 5
      object plToolSpec: TPanel
        Left = 1
        Top = 1
        Width = 731
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        StyleElements = [seFont, seBorder]
        object sbDeleteSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 60
          Top = 0
          Width = 30
          Height = 24
          Hint = #1059#1076#1072#1083#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          OnClick = sbDeleteSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000E9C00000E
            9C01079453DD0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000001484944415478DAAD53414B024114FE
            66095996FE417F41C49325EEBAE622B241D84442873A458724FA33051DEA545D
            0AF6B0A162BA048BE62EE8EF08BA7888080FD5343B90AC68896B8F8179CCBCEF
            9BF7BDF78630C6B088917F23208404DBDA11B0720E58D382F7017A0DBC70B73B
            C285085619A51E0603545C778B93DC87C165C0BC2B956A22D6B6358E7B1A23D8
            2384DE140A1692C977743ACA89E7D153C00EEE0E80CD4B4DB3914ABDA1DF5F3E
            76DDF21963D684049EE2C6553A5D452E37E424F24EBB6DBE029F4D5D6F72B038
            AB789EC86E9A048C5E330C1BAAFA854643822C03F9BCF0C359FD4A10D83A603C
            EA7A0BF1F8109204F8BEBCDBEB99B7C0C34FCC9F04065F4E54823109F5BA0445
            01B2D90F38CED24C09A288994C9503E62FA26863B168219188DCC6D983649A35
            C462C120A91CD79D7B940F81ED0BE099BBFE8484853F5354FB06D031E7E18A64
            5A800000000049454E44AE426082}
          ExplicitLeft = 62
        end
        object sbEditSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 30
          Top = 0
          Width = 30
          Height = 24
          Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbEditSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
            62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
            08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
            4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
            C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
            892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
            4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
            0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
            C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
            8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
            346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
            5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
            3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
            378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
            C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
            1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
            FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
            F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
            0049454E44AE426082}
          ExplicitLeft = 32
        end
        object sbAddSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 0
          Top = 0
          Width = 30
          Height = 24
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbAddSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000E9C00000E
            9C01079453DD0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000002764944415478DAA5935B48145118C7
            FF733AB3EC8C0EDA4B82B5B5651042498B2CAB66FA12BD78698330A38788F212
            582F05AE2F696042493DD83E943E44411704A5072F154104BB8A2D51ACDD28A8
            DDBC4441E2B2BB33E3CE7AA63389646A60F41DCE8173F8F89DEFF2FF04AC3427
            DFC75082A3C8959D98562318C11DFE768BEFC85247D334212CB98BA0F0675E70
            D7B9F2F3B073B303D97605337A026FBF44F0EA7D14F1F6B11EA868E2BEC67280
            889342C8555255E02D2A522549228CC196349250C40C8020A5691AEB1B0BC8E1
            47C1D7B81F2BB420BF01327A0ABBBC270E979726E64C26A7611249B0B13D39C5
            087E1B8566A60885C02481AAB7034F32C3A7867AA0A36111E0543A3C9FCE790F
            A884523BE1DFA5F9CAB66591335B1AD115BD8ED9548C519E1FB3566A4EBF3A30
            24C77CA3DB38206201DACAFB6BCF57B83C7AD2487180C9A3674411159C7536E1
            4AC48FB811E7582B2B816488367D78FCB9FD69F53D1F075C1678B53F3474F9B6
            E7ADCF61DC039268278699FE957BFDC6E3E89EBAC9014988028566E8169C4D27
            7E507F4547C09C32CB041C92532D6D3E4A2961FB73F7A154F110FCC502F131F6
            70F2310311E9C5DA96CFE64B33EF9F239837E7D9D7E40CF557F10826AC0880D6
            BDFD35AD55AEE2B5D5609D4D1F7C17B23FABBCDBCC6BD0B9D0857637EF825725
            36710D5D30F4CEC10772A239B49503A20B3AB0E386CB5F5D77A4BC6CAD3AE8E6
            3A685CA1C4DDC51505073D1EAEC44C92668C2679E8193C154A485AD354D6170A
            CAE1E191307A67DD7F2A7111025C932F15D6BBF3776097C3892C49414C8B637C
            2A82176F3E22D11AB27E3EBDDA2C2C9FC61A6C422536C081EF98C02406F85BEF
            AAD3681DFF633F013BA960F0622C30B40000000049454E44AE426082}
          ExplicitLeft = 1
          ExplicitHeight = 22
        end
        object sbExcelSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 90
          Top = 0
          Width = 30
          Height = 24
          Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          ParentShowHint = False
          ShowHint = True
          OnClick = sbExcelSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000B1200000B
            1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000002CC4944415478DA6D934B48545118C7
            FF67346A141B7332F35136A6398E8E332A6A48505A8E2F68D16634451745602F
            93162911F65A0411F82C082A3433CB686764D3C3458B462B17968A1486A8E942
            D42847C77BCEE9DC7BC7C16C2EDC73CFEBFBDDFFF73FDF2158F7D83BAFED5E9C
            9DCF5876BB5297D88A894ACCB4C8DDB503350F3BE1E321F6BB17AA9618B75026
            25AC4892516252B05B5A81442918631063700DCA3ED7B63FF20928BA5DCDA9D8
            4819555E3548F4054086C86BD1C161F321BA2D2E0E0E06C619E5728BA1E91F5A
            52D8728E5331A09ECD2A481E4B2A48F41342A31112B819C7F61F41ABB35B09B6
            46ED8163C439490A5AAAF8BF81E2EB01AA0A284C61BB10B23108259979E81AEC
            052780252216AF46FA26497EB30CA0D06D0A54729AFE358B2C8319A333E3189F
            9B5152326E8B865E1B84E2341B1E0F38E0B7C11FD61DF1022014E4359F5500E6
            F0585465DB71D3D18EDABC72D8EF5DC4E2F2920248100AF45A9D00E4A2E3530F
            888620D5605201B6A6337C55FED5A213B044C6E1D69B0E747F79EF353521CCA0
            28389A9E8FF6FE97C24A08057178F76D6092E4369E564C9437DA8C99A8CE29C1
            E13BE731FB67C17B2289DB63B0354027003601E8111E70240BC5AF473F4E9143
            8DA714133584E07EE925CCFE5EC0E0CFEFA87FDBA102849189E12AA02CBD006D
            FD2F201FA7252A1E8E61E71439D820009C629FC182AC986434F53E455B791D2A
            5AAF60627E46012445C62254004A3D002880383886FAA6484EC349EE2D1E4FCE
            1AA2518ED1E55E56E6CD1131D00706A33CA310AD7D721D70A4EC348A3AF83041
            B2EB2BD700642FE87F9568167F931554EC2DC4035148B2072991F1E819764E90
            0302A0160FF5E6BC5A50F23D90FB067DF89C4E1BE4E29C2BF2A968840FFCEBF4
            5800C9BA71BC9230C942FD4822A3CC24C243A8585E5B89F0F72B1EADEB7AE2F3
            32AD9FB05E2EDEC556581AD54856892349E46B9608A919BBFEFC992FC05FFC64
            D8FDA3D5A84C0000000049454E44AE426082}
          ExplicitLeft = 93
          ExplicitHeight = 23
        end
        object sbCancelSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 184
          Top = 0
          Width = 30
          Height = 24
          Hint = #1057#1085#1103#1090#1100' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1077
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = sbCancelSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002214944415478DAA593414854411880BF79BBAE91A5E11A
            49B16EA9A965872048EA50770FA61B5114614851D02524E8D221F350874ED2A5
            8317C17318044114041156161DC4758B2D4D8AA4D5B0A0F6BD7D3B7FF3467775
            334F0EBC37EFBD99EF9BF9E7FDBF1211D6D3D42A815219738F165E75E9E89C23
            52B3B640A9EFB436D570E42098EF764CEBE2A55FBC45521F7F85452A570B0AF0
            D136487F45E6162C24019CCF235515B06B1B7AF41DF90FD3990D225B97052BE1
            851C5CBD86F4F723E9F4221C8FE3F4F591BF711D2A147A6C1C3F3D93D9682405
            8170E90CF2E91BEAF61D686C04CF23DFDD6D77111A1A824804999CC4BF700EEA
            6B71EF3F6593882A0AE4E269184B21B118CEE0A00502896D65658879F6130974
            2A856A6B21FBE019950581369D3A7F123102BBE5BABAC5550D689B81BDAE2E73
            80297B2681C07DF8FC1F41CF09E4551231024CCCA1E1E1A220583DD7D1813621
            04E3CEA13DB88F464B057427909713CBF05208C13F524108AE8BDBDE8E4E2609
            1DDECB9FC7AFD95210F8A673CE1E43BF9F2174F71EAAB9D902B9CE4EBBE5C8C8
            08949723E3E3B8A78EA39A76907DF26659E099EC53BBE35175A0157E64716EDE
            C2EFEDB50726BE8F6A682032308077E5325485C925A7C87DC9CC568BD4161329
            6B244E432CAAF69BD527A6D0B3F3365E7B26C155BD19A7258637398DBF04AF4A
            E5DF411DECDC1E0DEFAB376021858D449B39A2F1529F4BE0FF16D3CF358A69A9
            9F377074E57CB5DE72FE0B42C953F0B2C476DB0000000049454E44AE426082}
          ExplicitLeft = 214
          ExplicitTop = 2
        end
        object sbFilter: TPngSpeedButton
          AlignWithMargins = True
          Left = 120
          Top = 0
          Width = 32
          Height = 24
          Hint = #1054#1090#1073#1086#1088
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          AllowAllUp = True
          GroupIndex = 1
          ParentShowHint = False
          ShowHint = True
          OnClick = sbFilterClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000AEB00000A
            EB01828B0D5A0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000000A34944415478DA63FCFFFF3F032580
            D161CA0B8A4C607CF0E18740C2920FEF17C40890A411A88701A8479011E48587
            1F7F2A0005EE136B084CB33C3FFB07465818000D31004A9C276408B266B01790
            031168880350C17E5C86A06BC630006A480250E17C7443A09A0D819A2FA00422
            B668841952E1CA03E677ECFE8255334E0340C071EACBFF3057806CDF9F2DCE88
            351A470DC06D00B221641B00330444936D0021000075459B7A13DFF16F000000
            0049454E44AE426082}
          ExplicitLeft = 225
          ExplicitTop = -2
          ExplicitHeight = 28
        end
        object sbResetFilter: TPngSpeedButton
          AlignWithMargins = True
          Left = 152
          Top = 0
          Width = 32
          Height = 24
          Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1080#1083#1100#1090#1088
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          ParentShowHint = False
          ShowHint = True
          OnClick = sbResetFilterClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000AEB00000A
            EB01828B0D5A0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000000B94944415478DA63FCFFFF3F032580
            D161CA0B8A4C607CF0E18740C2920FEF17C40890A411A88701A8479011E48587
            1F7F2A0005EE136B084CB33C3FFB07465818000D31004A9C276408B266B01790
            031168880350C17E5C86A06BC630006A480250E17C7443A09A0D819A2FA00422
            B668749CFAF23F230E2F8054EFCF1667C46B8013D080291B0319B4761D4311BF
            E666C590E3BF9E611F310600158135C00C81B18172C41B00D308023083E86B00
            455EA02810298E46520000704CAE7A8505FCA90000000049454E44AE426082}
          ExplicitLeft = 190
        end
      end
      object dgSpec: TDBGridEh
        Left = 1
        Top = 25
        Width = 731
        Height = 215
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        Border.Color = clSilver
        BorderStyle = bsNone
        ColumnDefValues.Title.TitleButton = True
        Ctl3D = False
        DataSource = dsSpec
        DynProps = <>
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
        GridLineParams.DataHorzColor = 14540253
        GridLineParams.DataVertColor = 14540253
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorParams.HorzLineColor = 14540253
        IndicatorParams.VertLineColor = 14540253
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghDialogFind, dghColumnResize, dghColumnMove]
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = pmSpec
        SelectionDrawParams.DrawFocusFrame = True
        SelectionDrawParams.DrawFocusFrameStored = True
        SortLocal = True
        STFilter.Color = 15463413
        STFilter.Font.Charset = DEFAULT_CHARSET
        STFilter.Font.Color = clWindowText
        STFilter.Font.Height = -11
        STFilter.Font.Name = 'Verdana'
        STFilter.Font.Style = []
        STFilter.HorzLineColor = 14540253
        STFilter.InstantApply = True
        STFilter.ParentFont = False
        STFilter.VertLineColor = 14540253
        STFilter.Visible = True
        TabOrder = 1
        TitleParams.FillStyle = cfstGradientEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -13
        TitleParams.Font.Name = 'Calibri'
        TitleParams.Font.Style = [fsBold]
        TitleParams.HorzLineColor = 14540253
        TitleParams.MultiTitle = True
        TitleParams.ParentFont = False
        TitleParams.VertLineColor = 14540253
        OnDblClick = sbEditSpecClick
        OnTitleBtnClick = dgSpecTitleBtnClick
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            ReadOnly = True
            Title.Caption = #8470' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
            Width = 110
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'kind_code'
            Footers = <>
            ReadOnly = True
            Title.Caption = #1058#1080#1087
            Width = 61
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'owner_code'
            Footers = <>
            ReadOnly = True
            Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
            Width = 115
          end
          item
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'is_defective'
            Footers = <>
            ReadOnly = True
            Title.Caption = #1044#1077#1092#1077#1082#1090#1085#1099#1081
            Width = 82
          end
          item
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isnotready'
            Footers = <>
            ReadOnly = True
            Title.Caption = #1053#1077' '#1075#1086#1090#1086#1074
            Width = 58
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'weight_shift'
            Footers = <>
            Title.Caption = #1057#1084#1077#1097'. '#1094'.'#1090'.'
            Width = 60
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'seal_number'
            Footers = <>
            ReadOnly = True
            Title.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
            Width = 127
          end
          item
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isempty'
            Footers = <>
            ReadOnly = True
            Title.Caption = #1055#1086#1088#1086#1078#1085#1080#1081
            Width = 77
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'weight_container'
            Footers = <>
            Title.Caption = #1042#1077#1089' '#1085#1077#1090#1090#1086
            Width = 108
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'weight_cargo'
            Footers = <>
            Title.Caption = #1042#1077#1089' '#1073#1088#1091#1090#1090#1086
            Width = 111
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'state_code'
            Footers = <>
            ReadOnly = True
            Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 150
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited plLeft: TPanel
    Height = 625
    ExplicitHeight = 625
    inherited Bevel4: TBevel
      Height = 600
      ExplicitHeight = 600
    end
    inherited dgFolders: TDBGridEh
      Height = 600
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'declare @userid int, @doctypeid int, @datebegin datetime, @datee' +
        'nd datetime, @isconfirmed int;'
      ''
      
        'select @doctypeid = min(id) from doctypes where system_section =' +
        ' '#39'income_empty'#39';'
      'select @userid = :userid;'
      ''
      
        'select @datebegin = datebegin, @dateend  = dateend, @isconfirmed' +
        '  = isconfirmed '
      'from dbo.f_GetFilterParams(@userid,@doctypeid);'
      ''
      
        'select d.*, h.*, (case when d.isconfirmed = 1 then '#39#39' else '#39#39' en' +
        'd) as state_code,'
      'dbo.f_GetDocUsedProcs(d.id, null) as usedproc,'
      
        '(select code from doctypes t where t.id = d.doctype_id) as docty' +
        'pe_code,'
      
        '(select code from counteragents c where c.id = h.dealer_id) as d' +
        'ealer_code,'
      
        '(select person_name from persons p where p.id = h.inspector_id) ' +
        'as inspector_code,'
      
        '(select code from deliverytypes t where t.id = h.dlv_type_id) as' +
        ' delivery_code, '
      
        '(select user_name from users u where u.id = d.user_created) as u' +
        'ser_created'
      'from docincome h, documents d '
      'where d.id = h.id and (1 = :showall or d.folder_id = :folder_id)'
      'and ('
      
        '    not exists (select 1 from dbo.f_GetFilterList(@userid, @doct' +
        'ypeid) fl1)'
      
        '    or exists (select 1 from dbo.f_GetFilterList(@userid, @docty' +
        'peid) fl, docincomespec s where  s.doc_id = d.id and charindex(f' +
        'l.v, s.container_num)>0) '
      ')'
      'and (@datebegin is null or  d.doc_date >=@datebegin)'
      'and (@dateend is null or  d.doc_date < @dateend+1)'
      
        'and (isnull(@isconfirmed,0)=0 or d.isconfirmed = isnull(@isconfi' +
        'rmed,0))'
      'order by d.doc_date desc')
    SelectCommand.Parameters = <
      item
        Name = 'userid'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'showall'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'declare @id int;'
      'select @id = :id;'
      ''
      'update documents set doctype_id = :doctype_id where id = @id;'
      ''
      ''
      'update docincome'
      'set'
      '  dealer_id = :dealer_id,'
      '  driver_name = :driver_name,'
      '  car_data = :car_data,'
      '  attorney = :attorney,'
      '  note = :note,'
      '  date_income = :date_income,'
      '  dlv_type_id = :dlv_type_id,'
      '  inspector_id = :inspector_id'
      'where'
      '  id = @id')
    UpdateCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doctype_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'dealer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'driver_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'car_data'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'attorney'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'date_income'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'inspector_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'begin'
      ''
      '  declare @newid int;'
      ''
      
        '  insert into documents (doctype_id, folder_id) values (:doctype' +
        '_id, :folder_id);'
      ''
      '  select @newid = IDENT_CURRENT('#39'documents'#39');'
      ''
      'insert into docincome'
      
        '  (id, dealer_id, driver_name, car_data, attorney, note, date_in' +
        'come, dlv_type_id, inspector_id)'
      'values'
      
        '  (@newid, :dealer_id, :driver_name, :car_data, :attorney, :note' +
        ', :date_income, :dlv_type_id, :inspector_id);'
      ''
      'end;')
    InsertCommand.Parameters = <
      item
        Name = 'doctype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dealer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'driver_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'car_data'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'attorney'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'date_income'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'inspector_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      
        'delete from documents where  id = :id and isnull(isconfirmed,0) ' +
        '<> 1')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select d.*, h.*, (case when d.isconfirmed = 1 then '#39#39' else '#39#39' en' +
        'd) as state_code,'
      'dbo.f_GetDocUsedProcs(d.id, null) as usedproc,'
      
        '(select code from doctypes t where t.id = d.doctype_id) as docty' +
        'pe_code,'
      
        '(select code from counteragents c where c.id = h.dealer_id) as d' +
        'ealer_code,'
      
        '(select person_name from persons p where p.id = h.inspector_id) ' +
        'as inspector_code,'
      
        '(select code from deliverytypes t where t.id = h.dlv_type_id) as' +
        ' delivery_code, '
      
        '(select user_name from users u where u.id = d.user_created) as u' +
        'ser_created'
      'from docincome h, documents d '
      'where d.id = h.id and d.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited meData: TMemTableEh
    AfterOpen = meDataAfterOpen
  end
  inherited drvForms: TADODataDriverEh
    Left = 540
    Top = 202
  end
  inherited meForms: TMemTableEh
    Left = 588
    Top = 202
  end
  inherited dsForms: TDataSource
    Left = 636
    Top = 202
  end
  inherited drvFolders: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'select * from folders where folder_section = :folder_section ord' +
        'er by folder_name desc')
  end
  inherited drvLinkedObjects: TADODataDriverEh
    Left = 881
    Top = 210
  end
  inherited meLinkedObjects: TMemTableEh
    Left = 785
    Top = 146
  end
  inherited dsLinkedObjects: TDataSource
    Left = 873
    Top = 146
  end
  inherited pmLinkedObjects: TPopupMenu
    Left = 792
    Top = 208
  end
  inherited qrReport: TADOQuery
    Left = 688
  end
  inherited IL: TPngImageList
    Bitmap = {}
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'declare @guid varchar(42);'
      ''
      'select @guid = :guid;'
      ''
      'select s.*,v.*, 0 as issealwaste,'
      
        '(select code from counteragents c1 where c1.id = s.container_own' +
        'er_id) as owner_code,'
      
        '(select code from containerkinds k where k.id = v.kind_id) as ki' +
        'nd_code,'
      
        '(select sk1.code from v_lastobjectstates los1, objectstatekinds ' +
        'sk1 where los1.object_id = v.id and los1.task_id = o2s.task_id a' +
        'nd sk1.id = los1.object_state_id) as state_code,'
      
        '(select code from cargotypes t where t.id = s.cargotype_id) as c' +
        'argotype_code'
      'from docincomespec s  '
      'left outer join containers v on (s.container_id = v.id)'
      
        'left outer join objects2docspec o2s on (o2s.doc_id = s.doc_id an' +
        'd o2s.object_id = v.id)'
      'where s.doc_id = :doc_id '
      'and '
      '('
      '  not exists (select 1 from dbo.f_GetFilterList(0, @guid) fl1)'
      
        '  or exists (select 1 from dbo.f_GetFilterList(0, @guid) fl wher' +
        'e charindex(fl.v, v.cnum)>0) '
      ')'
      '')
    SelectCommand.Parameters = <
      item
        Name = 'guid'
        Size = -1
        Value = Null
      end
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update docincomespec'
      'set'
      '  container_id = :container_id,'
      '  container_num = :container_num,'
      '  container_kind_id = :container_kind_id,'
      '  container_owner_id = :container_owner_id,'
      '  is_defective = :is_defective,'
      '  defect_note = :defect_note,'
      '  container_weight = :container_weight,'
      '  carrying = :carrying,'
      '  date_made = :date_made,'
      '  date_inspection = :date_inspection,'
      '  date_stiker = :date_stiker,'
      '  picture = :picture,'
      '  cargo_id = :cargo_id,'
      '  seal_number = :seal_number,'
      '  cargotype_id = :cargotype_id,'
      '  isempty = :isempty,'
      '  weight_cargo = :weight_cargo,'
      '  weight_container = :weight_container,'
      '  external_order_num = :external_order_num,'
      '  note = :note,'
      '  acep = :acep, '
      '  isnotready = :isnotready,'
      ' address_id = :address_id,'
      ' weight_shift = :weight_shift '
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'container_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'is_defective'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'defect_note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'container_weight'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'carrying'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'date_made'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_inspection'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_stiker'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'picture'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'cargo_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'cargotype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'weight_cargo'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_container'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'external_order_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'acep'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'isnotready'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'address_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'weight_shift'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into docincomespec'
      
        '  (doc_id, container_id, container_num, container_kind_id, conta' +
        'iner_owner_id, '
      
        '   is_defective, defect_note, container_weight, carrying, date_m' +
        'ade, date_inspection, '
      
        '   date_stiker, picture, cargo_id, seal_number, cargotype_id, is' +
        'empty, '
      
        '   weight_cargo, weight_container, external_order_num, note, ace' +
        'p, isnotready, address_id, weight_shift )'
      'values'
      
        '  (:doc_id, :container_id, :container_num, :container_kind_id, :' +
        'container_owner_id, '
      
        '   :is_defective, :defect_note, :container_weight, :carrying, :d' +
        'ate_made, '
      
        '   :date_inspection, :date_stiker, :picture, :cargo_id, :seal_nu' +
        'mber, :cargotype_id, '
      
        '   :isempty, :weight_cargo, :weight_container, :external_order_n' +
        'um, :note, :acep, :isnotready, :address_id, :weight_shift  )')
    InsertCommand.Parameters = <
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'is_defective'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'defect_note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'container_weight'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'carrying'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'date_made'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_inspection'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_stiker'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'picture'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'cargo_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'cargotype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'weight_cargo'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_container'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'external_order_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'acep'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'isnotready'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'address_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'weight_shift'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from docincomespec where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select s.*,v.*, 0 as issealwaste,'
      
        '(select code from counteragents c1 where c1.id = s.container_own' +
        'er_id) as owner_code,'
      
        '(select code from containerkinds k where k.id = v.kind_id) as ki' +
        'nd_code,'
      
        '(select sk1.code from v_lastobjectstates los1, objectstatekinds ' +
        'sk1 where los1.object_id = v.id and los1.task_id = o2s.task_id a' +
        'nd sk1.id = los1.object_state_id) as state_code,'
      
        '(select code from cargotypes t where t.id = s.cargotype_id) as c' +
        'argotype_code'
      'from docincomespec s  '
      'left outer join containers v on (s.container_id = v.id)'
      
        'left outer join objects2docspec o2s on (o2s.doc_id = s.doc_id an' +
        'd o2s.object_id = v.id)'
      'where s.id = :current_id ')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 320
    Top = 456
  end
  object meSpec: TMemTableEh
    Params = <>
    DataDriver = drvSpec
    BeforeOpen = meSpecBeforeOpen
    BeforeInsert = meSpecBeforeInsert
    AfterInsert = meSpecAfterInsert
    BeforeEdit = meSpecBeforeEdit
    Left = 368
    Top = 456
  end
  object dsSpec: TDataSource
    DataSet = meSpec
    Left = 424
    Top = 456
  end
  object pmSpec: TPopupMenu
    Left = 484
    Top = 454
    object N15: TMenuItem
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1076#1074#1080#1078#1077#1085#1080#1077' '#1087#1086' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1091
      OnClick = N15Click
    end
  end
end
