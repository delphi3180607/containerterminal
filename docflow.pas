﻿unit docflow;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Vcl.ComCtrls, filter, Vcl.ToolWin, Vcl.Mask,
  DBCtrlsEh, System.ImageList, Vcl.ImgList, PngImageList;

type
  TFormDocFlow = class(TFormGrid)
    Bevel1: TBevel;
    btFlow: TPngSpeedButton;
    sbHistory: TPngSpeedButton;
    pmDocFlow: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    N12: TMenuItem;
    N11: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    sbRestrictions: TPngSpeedButton;
    sbConfirm: TPngSpeedButton;
    sbCancelConfirm: TPngSpeedButton;
    plLeft: TPanel;
    Bevel4: TBevel;
    dgFolders: TDBGridEh;
    plTool: TPanel;
    cbAll: TCheckBox;
    drvFolders: TADODataDriverEh;
    meFolders: TMemTableEh;
    dsFolders: TDataSource;
    pmFolders: TPopupMenu;
    MenuItem9: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    Bevel5: TBevel;
    sbReport: TPngSpeedButton;
    drvLinkedObjects: TADODataDriverEh;
    meLinkedObjects: TMemTableEh;
    dsLinkedObjects: TDataSource;
    Bevel6: TBevel;
    N13: TMenuItem;
    N14: TMenuItem;
    pmLinkedObjects: TPopupMenu;
    MenuItem25: TMenuItem;
    N15Collapse: TMenuItem;
    sbImport2: TPngSpeedButton;
    qrReport: TADOQuery;
    plStatus: TPanel;
    sbSearch: TPngSpeedButton;
    Bevel7: TBevel;
    plLinkedObjects: TPanel;
    dgLinkedObjects: TDBGridEh;
    sbClearFilter: TPngSpeedButton;
    plSearch: TPanel;
    Label1: TLabel;
    edSearch: TEdit;
    N18: TMenuItem;
    N19: TMenuItem;
    Bevel3: TBevel;
    sbClock: TPngSpeedButton;
    N155: TMenuItem;
    N166: TMenuItem;
    N115: TMenuItem;
    N116: TMenuItem;
    sbWrap: TSpeedButton;
    SplitterVert: TSplitter;
    SplitterHor: TSplitter;
    sbBarrel: TPngSpeedButton;
    sbPass: TPngSpeedButton;
    drvPass: TADODataDriverEh;
    mePass: TMemTableEh;
    IL: TPngImageList;
    procedure btFlowClick(Sender: TObject);
    procedure sbHistoryClick(Sender: TObject);
    procedure sbConfirmClick(Sender: TObject);
    procedure sbCancelConfirmClick(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
    procedure MenuItem10Click(Sender: TObject);
    procedure MenuItem11Click(Sender: TObject);
    procedure meDataBeforePost(DataSet: TDataSet);
    procedure cbAllClick(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure meDataBeforeRefresh(DataSet: TDataSet);
    procedure N11Click(Sender: TObject);
    procedure MenuItem13Click(Sender: TObject);
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure meFoldersAfterScroll(DataSet: TDataSet);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure meLinkedObjectsBeforeOpen(DataSet: TDataSet);
    procedure N14Click(Sender: TObject);
    procedure MenuItem25Click(Sender: TObject);
    procedure N15CollapseClick(Sender: TObject);
    procedure btFilterClick(Sender: TObject);
    procedure sbReportClick(Sender: TObject);
    procedure meDataBeforeEdit(DataSet: TDataSet);
    procedure meFoldersBeforePost(DataSet: TDataSet);
    procedure meFoldersBeforeInsert(DataSet: TDataSet);
    procedure meFoldersBeforeEdit(DataSet: TDataSet);
    procedure sbClearFilterClick(Sender: TObject);
    procedure sbSearchClick(Sender: TObject);
    procedure sbRestrictionsClick(Sender: TObject);
    procedure sbClockClick(Sender: TObject);
    procedure meDataAfterPost(DataSet: TDataSet);
    procedure edSearchKeyPress(Sender: TObject; var Key: Char);
    procedure N166Click(Sender: TObject);
    procedure dgDataCellClick(Column: TColumnEh);
    procedure dgDataApplyFilter(Sender: TObject);
    procedure meDataBeforeDelete(DataSet: TDataSet);
    procedure sbDeleteClick(Sender: TObject);
    procedure sbWrapClick(Sender: TObject);
    procedure sbBarrelClick(Sender: TObject);
    procedure sbAddClick(Sender: TObject);
    procedure meDataAfterInsert(DataSet: TDataSet);
    procedure sbPassClick(Sender: TObject);
  private
    current_folder_id: integer;
  protected

    special_current_mode: string;

    allowcancel: boolean;
    function CheckBeforeDelete(id: integer; guid: string): boolean; override;

    procedure SetReadOnly; override;

  public

    system_section: string;
    global_section: string;
    doctype_ids: string;
    default_doctype_id: integer;
    active_doctype_id: integer;
    ownFormFilter: TFormFilter;
    barbell_support: boolean;
    save_folder_position: boolean;

    oldid: integer;
    printid: string;

    isfiltering : boolean;

    procedure FillSelectList(g: string);
    procedure FillSelectListEx(g: string; dg: TDBGridEh; idname: string);
    procedure SetFilter(conditions: string); virtual;
    procedure Init; override;
  end;

var
  FormDocFlow: TFormDocFlow;

implementation

{$R *.dfm}

uses stateshistory, operationedit, tasksrestrictcontrol, dmu, functions, transformdocuments,
  doctypeselect, editcatalog, selectfolder, childdocs, dateexecution,
  SelectDocForm, main, AddSelect, ExtraInfo, Clock, ObjectStatesHistory,
  EditPass, Passes;

procedure TFormDocFlow.btFilterClick(Sender: TObject);
begin
  ownFormFilter.ShowModal;
  if (ownFormFilter.ModalResult = mrOk) or (ownFormFilter.ModalResult = mrYes) then
  begin
    SetFilter(ownFormFilter.conditions);
  end;
end;

procedure TFormDocFlow.SetFilter(conditions: string);
var rus_cond: string;
begin

    Screen.Cursor := crHourGlass;

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := doctype_ids;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := conditions;
    dm.spCreateFilter.ExecProc;

    if meFolders.Active then
    begin
      meData.Close;
      meData.Open;
    end;

    if conditions = '' then
      plStatus.Caption := ' Фильтр снят.'
    else
    begin
      rus_cond := StringReplace(conditions,';date_begin',', дата начала',[]);
      rus_cond := StringReplace(rus_cond,';date_end',', дата окончания',[]);
      rus_cond := StringReplace(rus_cond,';object_num',', №',[rfReplaceAll]);
      rus_cond := StringReplace(rus_cond,';isconfirmed',', Только_проведенные',[rfReplaceAll]);
      plStatus.Caption := ' Установлен фильтр:'+rus_cond;
    end;

    Screen.Cursor := crDefault;

end;


procedure TFormDocFlow.btFlowClick(Sender: TObject);
var gg: TGuid; newsystem_section: string;
begin

  if (self.meData.FieldByName('isconfirmed').AsInteger = 0) then
  begin
    ShowMessage('Документ не проведен.');
    exit;
  end;

  if meData.FieldByName('inwait').AsBoolean then
  begin
    ShowMessage('Заявка на согласовании.');
    exit;
  end;

    CreateGUID(gg);
    g := GuidToString(gg);

    FillSelectList(g);

    FormDocTypeSelect.dsLocal.DataSet := nil;
    dm.spGetPurposeDocumentTypes.Close;
    dm.spGetPurposeDocumentTypes.Parameters.ParamByName('@selectguid').Value := g;

    try
      dm.spGetPurposeDocumentTypes.Open;
    except
      on E: Exception
      do begin
        ShowMessage(E.Message);
        exit;
      end;
    end;

    if dm.spGetPurposeDocumentTypes.RecordCount = 0 then
    begin
      ShowMessage('Окончание маршрута документооборота, либо нет разрешенных маршрутов.');
      exit;
    end;

    FormDocTypeSelect.dsLocal.DataSet := dm.spGetPurposeDocumentTypes;
    FormDocTypeSelect.ShowModal;

    if FormDocTypeSelect.ModalResult <> mrOk then
    begin
      dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
      dm.spClearSelectList.ExecProc;
      exit;
    end;

    FormTransformDocuments.dsData.DataSet := nil;
    dm.spPrepareTransformDocuments.Close;
    dm.spPrepareTransformDocuments.Parameters.ParamByName('@selectguid').Value := g;
    dm.spPrepareTransformDocuments.Parameters.ParamByName('@end_doctype_id').Value := dm.spGetPurposeDocumentTypes.FieldByName('end_doctype_id').AsInteger;
    dm.spPrepareTransformDocuments.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
    dm.spPrepareTransformDocuments.ExecProc;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from temp_filter where doctypes = ''-1'' and userid = '+IntToStr(FormMain.currentUserId));
    qrAux.ExecSQL;

    FormTransformDocuments.g := g;
    FormTransformDocuments.qrData.Close;
    FormTransformDocuments.qrData.Parameters.ParamByName('selectguid').Value := g;
    FormTransformDocuments.qrData.Parameters.ParamByName('userid').Value := FormMain.currentUserId;
    FormTransformDocuments.qrData.Parameters.ParamByName('userid1').Value := FormMain.currentUserId;
    FormTransformDocuments.qrData.Open;

    if FormTransformDocuments.qrData.RecordCount = 0 then
    begin
      ShowMessage('Нет объектов, доступных для включения в новый документ.'+#10+'Вероятно, уже создан документ на основании данного документа.');
      exit;
    end;

    if (FormTransformDocuments.qrData.RecordCount > 1) or (meLinkedObjects.RecordCount>2) then
    begin
      FormTransformDocuments.dsData.DataSet := FormTransformDocuments.qrData;
      FormTransformDocuments.ShowModal;
    end else
    begin
      FormTransformDocuments.ModalResult := mrOk;
    end;

    if FormTransformDocuments.ModalResult = mrOk then
    begin

      try
        dm.spCheckNonConfirmedChildDocs.Close;
        dm.spCheckNonConfirmedChildDocs.Parameters.ParamByName('@selectguid').Value := g;
        dm.spCheckNonConfirmedChildDocs.ExecProc;
      except
        On E:Exception do
        begin
          ShowMessage(E.Message);
          exit;
        end;
      end;


      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('select system_section from doctypes where id = '+dm.spGetPurposeDocumentTypes.FieldByName('end_doctype_id').AsString);
      qrAux.Open;

      newsystem_section := qrAux.Fields[0].AsString;

      FormSelectFolder.meFolders.Close;
      FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('folder_section').Value := newsystem_section;
      FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('startid').Value := 0;
      FormSelectFolder.meFolders.Open;
      FormSelectFolder.meFolders.SortOrder := 'folder_name desc';
      FormSelectFolder.ShowModal;

      if FormSelectFolder.ModalResult = mrOk then
      begin

        dm.spTransformDocuments.Parameters.ParamByName('@routeid').Value := dm.spGetPurposeDocumentTypes.FieldByName('rid').AsInteger;;
        dm.spTransformDocuments.Parameters.ParamByName('@newdoctypeid').Value := dm.spGetPurposeDocumentTypes.FieldByName('end_doctype_id').AsInteger;
        dm.spTransformDocuments.Parameters.ParamByName('@folderid').Value := FormSelectFolder.meFolders.FieldByName('id').AsInteger;
        dm.spTransformDocuments.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
        dm.spTransformDocuments.ExecProc;

        FullRefresh(meData);

      end else
        ShowMessage('Отменено.');

    end else
    begin
      ShowMessage('Создание документов не выполнено.');
    end;

    self.dgData.Selection.Clear;

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    meDataAfterScroll(meData);
    self.meLinkedObjects.Close;
    self.meLinkedObjects.Open;

end;

procedure TFormDocFlow.Init;
var doctypes: string; i: integer;
begin

  tablename := 'documents';
  printid := 'id';

  dm.qrAux.Close;
  dm.qrAux.SQL.Clear;
  dm.qrAux.SQL.Add('select id from doctypes where system_section = '''+self.system_section+'''');
  dm.qrAux.Open;

  doctypes := '';

  while not dm.qrAux.Eof do
  begin
    doctypes := ','+dm.qrAux.Fields[0].AsString+','+doctypes;
    dm.qrAux.Next;
  end;

  self.doctype_ids := doctypes;
  self.default_doctype_id := dm.qrAux.Fields[0].AsInteger;
  self.active_doctype_id := dm.qrAux.Fields[0].AsInteger;

  if not Assigned(ownFormFilter) then
  begin

    Application.CreateForm(TComponentClass(FormFilter.ClassType),ownFormFilter);

    ownFormFilter.dtBeginDate.Value := now()-2;
    ownFormFilter.dtEndDate.Value := now();
    ownFormFilter.ComposeConditions;

    SetFilter(ownFormFilter.conditions);

  end;

  drvFolders.SelectCommand.Parameters.ParamByName('folder_section').Value := self.system_section;

  if Assigned(drvData.SelectCommand.Parameters.FindParam('userid')) then
    drvData.SelectCommand.Parameters.ParamByName('userid').Value := FormMain.currentUserId;

  meFolders.Open;
  meFolders.AfterScroll := meFoldersAfterScroll;
  meFoldersAfterScroll(meFolders);

  inherited;

  meLinkedObjects.Open;

  allow_edit := false;
  allowcancel := false;

  if not FormMain.isAdmin then
  begin
    for i := 0 to pmDocFlow.Items.Count-1 do
    begin
      if pmDocFlow.Items[i].Tag = 99 then
      pmDocFlow.Items[i].Visible := false;
    end;
  end;

  save_folder_position := false;
  isfiltering := false;

end;

procedure TFormDocFlow.SetReadOnly;
begin

  if pos(self.system_section,FormMain.readonly_sections)>0 then
  begin
    meData.ReadOnly := true;
    exit;
  end;

  if (meData.FieldByName('isconfirmed').Value = 1) and (not allow_edit) then
    meData.ReadOnly := true
  else
    meData.ReadOnly := false;

  if meData.FieldByName('isdeleted').AsBoolean then
    meData.ReadOnly := true;

  if pos(self.system_section,FormMain.readonly_sections)>0 then
    meData.ReadOnly := true;

end;


function TFormDocFlow.CheckBeforeDelete(id: integer; guid: string): boolean;
var mess: string;
begin

  result := inherited CheckBeforeDelete(id, guid);
  if not result then exit;

  if id<>0 then
  begin

    drvData.GetRecCommand.Parameters.ParamByName('current_id').Value := meData.FieldByName('id').AsInteger;
    meData.RefreshRecord;
    if meData.FieldByName('isconfirmed').AsInteger = 1 then
    begin
      mess := 'Удаление проведенного документа невозможно.';
      result := false;
      ShowMessage(mess);
      exit;
    end;

  end else
  begin

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select 1 where exists ');
    qrAux.SQL.Add('(select 1 from documents d, selectlist sl where sl.id = d.id');
    qrAux.SQL.Add('and sl.guid = '''+guid+'''');
    qrAux.SQL.Add('and isnull(d.isconfirmed,0) = 1)');
    mess := 'В выборке существуют проведенные документы. Удаление невозможно.';

    qrAux.Open;
    if qrAux.Fields[0].AsInteger = 1 then
    begin
      result := false;
      ShowMessage(mess);
      exit;
    end;

  end;

end;


procedure TFormDocFlow.cbAllClick(Sender: TObject);
begin
  if isfiltering then exit;
  Screen.Cursor := crHourGlass;
  meData.Close;
  meData.Open;
  meLinkedObjects.Close;
  meLinkedObjects.Open;
  Screen.Cursor := crDefault;
end;

procedure TFormDocFlow.dgDataApplyFilter(Sender: TObject);
begin
  dgData.DefaultApplyFilter;
  meDataAfterScroll(meData);
end;

procedure TFormDocFlow.dgDataCellClick(Column: TColumnEh);
begin
  meDataAfterScroll(meData);
  self.oldid := meData.FieldByName('id').AsInteger;
end;

procedure TFormDocFlow.dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
var grf: TGraphic;
begin

   if meData.FieldByName('isdeleted').AsBoolean then
   begin
    dgData.Canvas.Font.Color := clRed;
   end;


   if meData.FieldByName('isabolished').AsBoolean then
   begin
    dgData.Canvas.Font.Color := clGray;
   end;

  inherited;

  if Column.FieldName = 'state_code' then
   if meData.FieldByName('isconfirmed').Value = 1 then
   begin

     if meData.FieldByName('isabolished').AsBoolean then
      dgData.Canvas.Brush.Color := $000E2F43
     else
      dgData.Canvas.Brush.Color := $000CA570;

      dgData.Canvas.Font.Color := clGreen;

      dgData.Canvas.Pen.Style := psClear;
      dgData.Canvas.Rectangle(Rect.Left+5,Rect.Top+2,Rect.Left+24,Rect.Top+14);
      dgData.Canvas.Brush.Color := clWindow;
      dgData.Canvas.Pen.Style := psSolid;

     if meData.FieldDefs.IndexOf('isstop') > -1 then
     if meData.FieldByName('isstop').AsInteger > 0 then
     begin
      IL.Draw(dgData.Canvas, Rect.Left+16, Rect.Top, 0, true);
     end;

     if meData.FieldDefs.IndexOf('inwait') > -1 then
     if meData.FieldByName('inwait').AsBoolean then
     begin
      IL.Draw(dgData.Canvas, Rect.Left+16, Rect.Top, 1, true);
     end;

   end;

end;

procedure TFormDocFlow.edSearchKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    isfiltering := true;
    cbAll.Checked := true;
    self.SetFilter(';object_num='+edSearch.Text+';');
    self.oldid := -1;
    meFolders.TreeList.FullExpand;
    self.meDataAfterScroll(meData);
    plSearch.Hide;
    isfiltering := false;
    Key := #0;
  end;
  if Key = #27 then
  begin
    plSearch.Hide;
    dgData.SetFocus;
    Key := #0;
  end;
end;

procedure TFormDocFlow.FillSelectList(g: string);
var i: integer; e: integer;
begin

    e := 0;

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    if self.dgData.Selection.SelectionType = (gstAll) then
    begin

      self.meData.First;
      while not self.meData.Eof do
      begin

        if (self.meData.FieldByName('isconfirmed').AsInteger = 0) then
        begin
          e:=1;
        end else
        begin
          dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := self.meData.FieldByName('id').AsInteger;
          dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
          dm.spAdd2SelectList.ExecProc;
        end;

        self.meData.Next;
      end;
      //meData.Close;
      //meData.Open;

    end else
    if self.dgData.Selection.SelectionType = (gstRecordBookmarks) then
    begin

      for i := 0 to self.dgData.Selection.Rows.Count-1 do
      begin
        self.meData.Bookmark := self.dgData.Selection.Rows[i];

        if (self.meData.FieldByName('isconfirmed').AsInteger = 0) then
        begin
          e:=1;
        end else
        begin
          dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := self.meData.FieldByName('id').AsInteger;
          dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
          dm.spAdd2SelectList.ExecProc;
        end;

      end;
      //meData.Close;
      //meData.Open;

    end else
    begin

      if drvData.GetrecCommand.Parameters.FindParam('current_id') <> nil then
      begin

        if (self.meData.FieldByName('isconfirmed').AsInteger = 0) then
        begin
          e:=1;
        end else
        begin
          dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := self.meData.FieldByName('id').AsInteger;
          dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
          dm.spAdd2SelectList.ExecProc;
        end;

      end else
      begin
        FullRefresh(meData);
      end;
    end;

    if (e=1) then ShowMessage('Не проведенные документы учитываться не будут.');

end;


procedure TFormDocFlow.FillSelectListEx(g: string; dg: TDBGridEh; idname: string);
var i: integer; ds: TMemTableEh; drv: TDataDriverEh;
begin

    ds := TMemTableEh(dg.DataSource.DataSet);
    drv :=  ds.DataDriver;

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    if dg.Selection.SelectionType = (gstAll) then
    begin

      ds.First;
      while not ds.Eof do
      begin

        dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := ds.FieldByName(idname).AsInteger;
        dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
        dm.spAdd2SelectList.ExecProc;

        ds.Next;

      end;

    end else
    if dg.Selection.SelectionType = (gstRecordBookmarks) then
    begin

      for i := 0 to dg.Selection.Rows.Count-1 do
      begin

        ds.Bookmark := dg.Selection.Rows[i];

        dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := ds.FieldByName(idname).AsInteger;
        dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
        dm.spAdd2SelectList.ExecProc;

      end;

    end else
    begin

        dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := ds.FieldByName(idname).AsInteger;
        dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
        dm.spAdd2SelectList.ExecProc;

    end;

end;

procedure TFormDocFlow.meDataAfterInsert(DataSet: TDataSet);
begin
  inherited;
  meDataAfterScroll(meData);
end;

procedure TFormDocFlow.meDataAfterPost(DataSet: TDataSet);
begin
  inherited;

  if special_current_mode <> '' then
  begin
    old_current_mode := special_current_mode;
  end;


  dm.spRegisterDataChange.Parameters.ParamByName('@tablename').Value := 'documents';
  dm.spRegisterDataChange.Parameters.ParamByName('@tableid').Value := meData.FieldByName('id').AsInteger;
  dm.spRegisterDataChange.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
  dm.spRegisterDataChange.Parameters.ParamByName('@EventType').Value := old_current_mode;
  dm.spRegisterDataChange.ExecProc;

  old_current_mode := '';

end;

procedure TFormDocFlow.meDataAfterScroll(DataSet: TDataSet);
var folderid: integer;
begin

  inherited;

  drvLinkedObjects.SelectCommand.Parameters.ParamByName('doc_id').Value := DataSet.FieldByName('id').AsInteger;
  meLinkedObjects.Close;
  meLinkedObjects.Open;

  if DataSet.FieldByName('id').AsInteger = self.oldid then
  begin
    exit;
  end;

  folderid := DataSet.FieldByName('folder_id').AsInteger;

  if dgData.Selection.Rows.Count < 2 then
  begin
     //self.sbConfirm.Enabled := not(meData.FieldByName('isconfirmed').AsInteger = 1);
     //self.sbCancelConfirm.Enabled := (meData.FieldByName('isconfirmed').AsInteger = 1);
  end else
  begin
     //self.sbConfirm.Enabled := true;
     //self.sbCancelConfirm.Enabled := true;
  end;

  if cbAll.Checked then
    if not save_folder_position then
    meFolders.Locate('id',folderid,[]);


  SetReadOnly;

  self.oldid := DataSet.FieldByName('id').AsInteger;


end;

procedure TFormDocFlow.meDataBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  if meData.FieldByName('isconfirmed').AsInteger = 1 then
  begin
    Raise Exception.Create('Удаление проведенного документа запрещено.');
  end;
end;

procedure TFormDocFlow.meDataBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  SetReadOnly;
end;

procedure TFormDocFlow.meDataBeforeOpen(DataSet: TDataSet);
begin
  drvData.SelectCommand.Parameters.ParamByName('folder_id').Value := meFolders.FieldByName('id').AsInteger;
  if self.cbAll.Checked then
    drvData.SelectCommand.Parameters.ParamByName('showall').Value := 1
  else
    drvData.SelectCommand.Parameters.ParamByName('showall').Value := 0;
end;

procedure TFormDocFlow.meDataBeforePost(DataSet: TDataSet);
begin

  self.meData.FieldByName('folder_id').Value := self.meFolders.FieldByName('id').AsInteger;
  self.meData.FieldByName('doctype_id').Value := self.active_doctype_id;

  inherited;

end;

procedure TFormDocFlow.meDataBeforeRefresh(DataSet: TDataSet);
begin
  dm.qrAux.Close;
  dm.qrAux.SQL.Clear;
  dm.qrAux.SQL.Add('update documents set folder_id='+self.meData.FieldByName('folder_id').AsString+' where id='+IntToStr(current_id));
  dm.qrAux.ExecSQL;
end;

procedure TFormDocFlow.meFoldersAfterScroll(DataSet: TDataSet);
begin
  if cbAll.Checked then exit;
  if self.current_folder_id <> meFolders.FieldByName('id').AsInteger then
  begin

    Screen.Cursor := crHourGlass;

    try
      meData.Close;
      meData.Open;
      self.current_folder_id := meFolders.FieldByName('id').AsInteger;
      meDataAfterScroll(meData);
    finally
      Screen.Cursor := crDefault;
    end;

  end;
  self.current_folder_id := meFolders.FieldByName('id').AsInteger;
end;

procedure TFormDocFlow.meFoldersBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  TFormEditCatalog(self.current_edit_form).system_section := self.system_section;
end;

procedure TFormDocFlow.meFoldersBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  TFormEditCatalog(self.current_edit_form).system_section := self.system_section;
end;

procedure TFormDocFlow.meFoldersBeforePost(DataSet: TDataSet);
begin
  inherited;
  meFolders.FieldByName('folder_section').Value := self.system_section;
end;

procedure TFormDocFlow.meLinkedObjectsBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  drvLinkedObjects.SelectCommand.Parameters.ParamByName('doc_id').Value := meData.FieldByName('id').AsInteger;
end;

procedure TFormDocFlow.MenuItem10Click(Sender: TObject);
begin

  dm.qrAux.Close;
  dm.qrAux.SQL.Clear;
  dm.qrAux.SQL.Add('select 1 where exists (select 1 from documents where folder_id = '''+self.meFolders.FieldByName('id').AsString+''')');
  dm.qrAux.Open;

  if dm.qrAux.RecordCount>0 then
  begin
    ShowMessage('Не могу удалить непустой каталог.');
    exit;
  end;

  if fQYN('Удалить запись ?') then
  begin
     self.meFolders.Delete;
  end;

end;

procedure TFormDocFlow.MenuItem11Click(Sender: TObject);
begin
  EE(self, FormEditCatalog,meFolders,'folders');
end;

procedure TFormDocFlow.MenuItem13Click(Sender: TObject);
begin
  meFolders.Close;
  meFolders.Open;
end;

procedure TFormDocFlow.MenuItem25Click(Sender: TObject);
begin
  if fQYN('Экспортировать список в Excel ?') then
    ExportExcel(self.dgLinkedObjects, self.Caption);
end;

procedure TFormDocFlow.MenuItem9Click(Sender: TObject);
begin
  EA(self, FormEditCatalog,meFolders,'folders');
end;

procedure TFormDocFlow.N10Click(Sender: TObject);
var folderid: integer; gg: TGuid; g: string;
begin

  FormSelectFolder.meFolders.Close;
  FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('folder_section').Value := self.system_section;
  FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('startid').Value := 0;
  FormSelectFolder.meFolders.Open;
  FormSelectFolder.ShowModal;

  if FormSelectFolder.ModalResult = mrOk then
  begin

    folderid := FormSelectFolder.meFolders.FieldByName('id').AsInteger;

    CreateGUID(gg);
    g := GuidToString(gg);

    FillSelectListAll(g);

    Screen.Cursor := crHourGlass;

    try

      dm.spMoveToFolder.Parameters.ParamByName('@selectguid').Value := g;
      dm.spMoveToFolder.Parameters.ParamByName('@folderid').Value := folderid;
      dm.spMoveToFolder.ExecProc;

      self.dgData.Selection.Clear;

      meData.Close;
      meData.Open;

    finally
      Screen.Cursor := crDefault;
    end;

  end;

end;

procedure TFormDocFlow.N11Click(Sender: TObject);
begin
  FullRefresh(meData);
  meLinkedObjects.Close;
  meLinkedObjects.Open;
end;

procedure TFormDocFlow.N14Click(Sender: TObject);
begin
  inherited;
  meFolders.TreeList.FullExpand;
end;

procedure TFormDocFlow.N15CollapseClick(Sender: TObject);
begin
  self.meLinkedObjects.TreeList.FullCollapse;
end;

procedure TFormDocFlow.N166Click(Sender: TObject);
begin
  FormObjectStatesHistory.meLinkedObjects.Close;
  FormObjectStatesHistory.meData.Close;
  FormObjectStatesHistory.drvData.SelectCommand.Parameters.ParamByName('object_id').Value := meLinkedObjects.FieldByName('id').AsInteger;
  FormObjectStatesHistory.meData.Open;
  FormObjectStatesHistory.ShowModal;
  Screen.Cursor := crDefault;
end;

procedure TFormDocFlow.sbAddClick(Sender: TObject);
begin

  if pos(self.system_section,FormMain.readonly_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  inherited;

end;

procedure TFormDocFlow.sbBarrelClick(Sender: TObject);
var gg: TGuid;
begin

  if pos(self.system_section,FormMain.readonly_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  RefreshRecord(meData);

  if (self.meData.FieldByName('isabolished').AsBoolean) then
  begin
    ShowMessage('Документ уже аннуллирован.');
    exit;
  end;

  if (self.meData.FieldByName('isconfirmed').AsInteger = 0) then
  begin
    ShowMessage('Документ не проведен.');
    exit;
  end;

  ShowBigMessage('Вы нажали на кнопку аннулирования документа. После аннулирования документа документ нельзя будет восстановить.');

  if fQYN('Вы собираетесь аннулировать документ. Действие нельзя будет отменить. Вы уверены?') then
  begin


    dm.spAbolishDoc.Parameters.ParamByName('@docid').Value := meData.FieldByName('id').AsInteger;
    dm.spAbolishDoc.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
    dm.spAbolishDoc.ExecProc;

    RefreshRecord(meData);

  end;

end;

procedure TFormDocFlow.sbCancelConfirmClick(Sender: TObject);
var i: integer; id: integer;
begin

  if pos(self.system_section,FormMain.confirmrestrict_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  RefreshRecord(meData);

  if (self.meData.FieldByName('isabolished').AsBoolean) then
  begin
    ShowMessage('Документ аннуллирован.');
    exit;
  end;


  if (self.meData.FieldByName('isconfirmed').AsInteger = 0) then
  begin
    ShowMessage('Документ не проведен.');
    exit;
  end;

  if not (FormMain.isAdmin or allowcancel)then
  begin
    if meData.FieldByName('date_confirm').AsDateTime < (now()-12/24) then
    begin
      ShowMessage('Распроведение документа старше 12 часов(смена) запрещено. Обратитесь к администратору.');
      exit;
    end;
  end;

  if fQYN('Действительно снять проведение с документа(ов)?') then
  begin

    try

      if self.dgData.Selection.SelectionType = (gstAll) then
      begin

        self.meData.First;
        while not self.meData.Eof do
        begin
          dm.spCancelConfirmDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
          dm.spCancelConfirmDoc.Parameters.ParamByName('@CargoId').Value := null;
          dm.spCancelConfirmDoc.Parameters.ParamByName('@UserId').Value := FormMain.currentUserId;
          dm.spCancelConfirmDoc.ExecProc;
          self.meData.Next;
        end;

      end else
      if self.dgData.Selection.SelectionType = (gstRecordBookmarks) then
      begin

        for i := 0 to self.dgData.Selection.Rows.Count-1 do
        begin
          self.meData.Bookmark := self.dgData.Selection.Rows[i];
          dm.spCancelConfirmDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
          dm.spCancelConfirmDoc.Parameters.ParamByName('@CargoId').Value := null;
          dm.spCancelConfirmDoc.Parameters.ParamByName('@UserId').Value := FormMain.currentUserId;
          dm.spCancelConfirmDoc.ExecProc;
        end;

      end else
      begin

        dm.spCancelConfirmDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
        dm.spCancelConfirmDoc.Parameters.ParamByName('@CargoId').Value := null;
        dm.spCancelConfirmDoc.Parameters.ParamByName('@UserId').Value := FormMain.currentUserId;
        dm.spCancelConfirmDoc.ExecProc;

    end;
    finally

      id := meData.FieldByName('id').AsInteger;

      if
      (self.dgData.Selection.SelectionType = (gstAll)) or
      (self.dgData.Selection.SelectionType = (gstRecordBookmarks)) then
      begin
          FullRefresh(meData);
      end else
      begin

          if drvData.GetrecCommand.Parameters.FindParam('current_id') <> nil then
          begin
             RefreshRecord(meData,self.meData.FieldByName('id').AsInteger);
          end else
          begin
            FullRefresh(meData);
          end;

      end;

    end;

    self.dgData.Selection.Clear;
    meDataAfterScroll(meData);

    meLinkedObjects.Close;
    meLinkedObjects.Open;

  end;

end;

procedure TFormDocFlow.sbClearFilterClick(Sender: TObject);
var conditions: string;
begin

  isfiltering := true;

  ownFormFilter.dtBeginDate.Value := now()-10;
  ownFormFilter.dtEndDate.Value := now()+1;

  conditions := ';date_begin='+ownFormFilter.dtBeginDate.Text;
  conditions := conditions +';date_end='+ownFormFilter.dtEndDate.Text;
  cbAll.Checked := false;
  SetFilter(conditions);

  dgData.ClearFilter;
  dgData.ApplyFilter;

  isfiltering := false;

end;

procedure TFormDocFlow.sbClockClick(Sender: TObject);
begin
  FormClock.table_name := 'documents';
  FormClock.table_id := meData.FieldByName('id').AsInteger;
  FormClock.Init;
  FormClock.ShowModal;
end;

procedure TFormDocFlow.sbConfirmClick(Sender: TObject);
var i: integer; id: integer;
begin

  if pos(self.system_section,FormMain.confirmrestrict_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  RefreshRecord(meData);

  if (self.meData.FieldByName('isabolished').AsBoolean) then
  begin
    ShowMessage('Документ аннуллирован.');
    exit;
  end;

  if (self.meData.FieldByName('isconfirmed').AsInteger = 1) then
  begin
    ShowMessage('Документ уже проведен.');
    exit;
  end;

  FormDateExecution.edUserName.Text := FormMain.currentUser;
  FormDateExecution.dtConfirm.Value := now();
  FormDateExecution.dtFactExecution.Value := now();
  FormDateExecution.ShowModal;

  if FormDateExecution.ModalResult = mrOk then
  begin

    try

      if self.dgData.Selection.SelectionType = (gstAll) then
      begin

        self.meData.First;
        while not self.meData.Eof do
        begin
          dm.spConfirmDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
          dm.spConfirmDoc.Parameters.ParamByName('@SpecId').Value := null;
          dm.spConfirmDoc.Parameters.ParamByName('@DateFactExecution').Value := FormDateExecution.dtFactExecution.Value;
          dm.spConfirmDoc.Parameters.ParamByName('@UserId').Value := FormMain.currentUserId;
          dm.spConfirmDoc.ExecProc;
          self.meData.Next;
        end;

      end else
      if self.dgData.Selection.SelectionType = (gstRecordBookmarks) then
      begin

        for i := 0 to self.dgData.Selection.Rows.Count-1 do
        begin
          self.meData.Bookmark := self.dgData.Selection.Rows[i];
          dm.spConfirmDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
          dm.spConfirmDoc.Parameters.ParamByName('@SpecId').Value := null;
          dm.spConfirmDoc.Parameters.ParamByName('@DateFactExecution').Value := FormDateExecution.dtFactExecution.Value;
          dm.spConfirmDoc.Parameters.ParamByName('@UserId').Value := FormMain.currentUserId;
          dm.spConfirmDoc.ExecProc;
        end;

      end else
      begin

        dm.spConfirmDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
        dm.spConfirmDoc.Parameters.ParamByName('@SpecId').Value := null;
        dm.spConfirmDoc.Parameters.ParamByName('@DateFactExecution').Value := FormDateExecution.dtFactExecution.Value;
        dm.spConfirmDoc.Parameters.ParamByName('@UserId').Value := FormMain.currentUserId;
        dm.spConfirmDoc.ExecProc;

      end;
    finally

      id := meData.FieldByName('id').AsInteger;

      if
      (self.dgData.Selection.SelectionType = (gstAll)) or
      (self.dgData.Selection.SelectionType = (gstRecordBookmarks)) then
      begin
        FullRefresh(meData);
      end else
      begin

          if drvData.GetrecCommand.Parameters.FindParam('current_id') <> nil then
          begin
             RefreshRecord(meData,self.meData.FieldByName('id').AsInteger);
          end else
          begin
            FullRefresh(meData);
          end;

      end;

    end;

    self.dgData.Selection.Clear;
    meDataAfterScroll(meData);
    self.meLinkedObjects.Close;
    self.meLinkedObjects.Open;

  end;

end;

procedure TFormDocFlow.sbDeleteClick(Sender: TObject);
var id, i: integer; g: string; guid: TGuid;
var cdgData: TDBGridEh; cmeData: TMemTableEh; n: TFormEditListNode; cDataSet: TDataSet;
begin

  if (pos(self.system_section,FormMain.readonly_sections)>0)
  and (pos(self.system_section,FormMain.allowdelete_sections)=0)
  then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  if meData.State = dsEdit then exit;

  If Assigned(Screen.ActiveControl)
  And (Screen.ActiveControl Is TDBGridEh)
  then begin
    cdgData := TDbGridEh(Screen.ActiveControl);
    n:= FindEditNodeByGrid(cdgData);
    if Assigned(n) then
    begin
      cmeData := n.meData;
      cDataSet := n.childDataSet;
    end else
    begin
      cmeData := meData;
      cDataSet := nil;
    end;
  end else
  begin
    cmeData := meData;
    cDataSet := nil;
  end;

  if Assigned(cDataSet) then
    if not self.CheckChildBeforeDelete(cDataSet.FieldByName('id').AsString) then exit;

  if fQYN('Удалить запись(и) ?') then
  begin

     id := cmeData.FieldByName('id').AsInteger;

     if cdgData.Selection.SelectionType in [gstRecordBookmarks, gstAll] then
     begin

      CreateGUID(guid);
      g := GuidToString(guid);
      FillSelectListAll(g);

      if not CheckBeforeDelete(0,g) then exit;

      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('delete from '+self.tablename+' where isnull(isconfirmed,0) = 0 and id in (select id from selectlist where rtrim(guid) = '''+trim(g)+''')');
      qrAux.ExecSQL;

      dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
      dm.spClearSelectList.ExecProc;

      FullRefresh(meData);

     end else
     begin

       if not CheckBeforeDelete(id,'') then exit;
       cmeData.Delete;

     end;

     meData.ReadOnly := false;

  end;
end;

procedure TFormDocFlow.sbHistoryClick(Sender: TObject);
begin
  FormChildDocs.drvData.SelectCommand.Parameters.ParamByName('DocId').Value := meData.FieldByName('id').AsInteger;
  FormChildDocs.meData.Close;
  FormChildDocs.meData.Open;
  FormChildDocs.ShowModal;
end;

procedure TFormDocFlow.sbPassClick(Sender: TObject);
begin

 if meData.FieldByName('isconfirmed').AsInteger <> 1 then
 begin
   ShowMessage('Нельзя создавать пропуск, если документ не проведен.');
   exit;
 end;

  if meData.FieldByName('inwait').AsBoolean then
  begin
    ShowMessage('Заявка на согласовании.');
    exit;
  end;

 FormPasses.Init;
 FormPasses.CreatePassByDoc(meData.FieldByName('id').AsInteger,0,0);
 RefreshRecord(meData);

end;

procedure TFormDocFlow.sbReportClick(Sender: TObject);
var gg: TGuid; g: Variant; groupfield: string; dt: TFieldType; doctype: integer;
begin

  if self.dgData.Selection.SelectionType in [gstAll, gstRecordBookmarks] then
  begin
    doctype := self.default_doctype_id;
  end else
  begin
    doctype := meData.FieldByName('doctype_id').AsInteger;
  end;

  FormSelectDocForm.qrTemplates.Close;
  FormSelectDocForm.qrTemplates.ParamCheck := true;
  FormSelectDocForm.qrTemplates.Parameters.ParamByName('doctype_id').Value := doctype;
  FormSelectDocForm.qrTemplates.Open;

  if FormSelectDocForm.qrTemplates.RecordCount = 0 then
  begin
    ShowMessage('Печать данного вида документов не предусмотрена.');
    exit;
  end;

  FormSelectDocForm.ShowModal;
  if FormSelectDocForm.ModalResult = mrOk then
  begin

    CreateGUID(gg);
    g := GuidToString(gg);
    FillSelectListAll(g, printid);

    qrReport.Close;
    qrReport.SQL.Clear;
    qrReport.Parameters.Clear;
    qrReport.SQL.Text := FormSelectDocForm.qrTemplates.FieldByName('sql_text').AsString;
    if Assigned(qrReport.Parameters.FindParam('userid')) then
      qrReport.Parameters.ParamByName('userid').Value := FormMain.currentUserId;
    if Assigned(qrReport.Parameters.FindParam('docid')) then
      qrReport.Parameters.ParamByName('docid').Value := meData.FieldByName('id').AsInteger;

    if self.dgData.Selection.SelectionType in [gstAll, gstRecordBookmarks] then
    begin
      if Assigned(qrReport.Parameters.FindParam('guid')) then
      begin
        qrReport.Parameters.ParamByName('guid').DataType := TFieldType(1);
        qrReport.Parameters.ParamByName('guid').Value := g;
      end;
    end else
    begin
      if Assigned(qrReport.Parameters.FindParam('guid')) then
      begin
        qrReport.Parameters.ParamByName('guid').DataType := TFieldType(1);
        qrReport.Parameters.ParamByName('guid').Value := '-';
      end;
    end;

    qrReport.Open;
    exReport.Template := ExtractFilePath(Application.ExeName) +'/templates/'+FormSelectDocForm.qrTemplates.FieldByName('template').AsString;
    groupfield := FormSelectDocForm.qrTemplates.FieldByName('group_field').AsString;
    exReport.Show('', false, groupfield, '');

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

  end;
end;

procedure TFormDocFlow.sbRestrictionsClick(Sender: TObject);
begin
  FormExtraInfo.docid := self.meData.FieldByName('id').AsInteger;
  FormExtraInfo.Init;
  FormExtraInfo.ShowModal;
end;

procedure TFormDocFlow.sbSearchClick(Sender: TObject);
begin
  if not plSearch.Visible then
  begin
    plSearch.Show;
    edSearch.SetFocus;
  end
  else plSearch.Hide;
end;

procedure TFormDocFlow.sbWrapClick(Sender: TObject);
begin
  meFolders.TreeList.FullCollapse;
end;

end.



