﻿unit docletters;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls;

type
  TFormDocLetters = class(TFormGrid)
    procedure meDataAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormDocLetters: TFormDocLetters;

implementation

{$R *.dfm}

uses editdocletter;


procedure TFormDocLetters.Init;
begin
  self.tablename := 'doccargoletters';
  meData.Close;
  drvData.SelectCommand.Parameters.ParamByName('doc_id').Value := self.parent_id;
  inherited;
  if (not Assigned(FormEditDocLetter)) or (FormEditDocLetter = nil) then
  Application.CreateForm(TFormDocLetters, FormDocLetters);
  self.formEdit := FormEditDocLetter;
end;


procedure TFormDocLetters.meDataAfterInsert(DataSet: TDataSet);
begin
  inherited;
  meData.FieldByName('doc_id').Value := self.parent_id;
  meData.FieldByName('dateregister').Value := now();
end;

end.
