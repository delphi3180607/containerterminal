﻿unit addresscontainers;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport;

type
  TFormAddressContainers = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var FormAddressContainers: TFormAddressContainers;

implementation

{$R *.dfm}

uses editcodename;


procedure TFormAddressContainers.Init;
begin
  inherited;
  tablename := 'addresscontainers';
  self.formEdit := FormEditCodeName;
end;


end.


uses editcodename;