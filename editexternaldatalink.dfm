﻿inherited FormEditExternalDataLink: TFormEditExternalDataLink
  Caption = #1057#1079#1103#1079#1100' '#1076#1072#1085#1085#1099#1093
  ClientHeight = 265
  ExplicitWidth = 543
  ExplicitHeight = 293
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 13
    Top = 10
    Width = 118
    Height = 16
    Caption = #1058#1080#1087' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072
  end
  object Label2: TLabel [1]
    Left = 13
    Top = 72
    Width = 271
    Height = 16
    Caption = #1058#1077#1082#1089#1090#1086#1074#1086#1077' '#1076#1072#1085#1085#1086#1077' '#1080#1079' '#1074#1085#1077#1096#1085#1077#1081' '#1089#1080#1089#1090#1077#1084#1099
  end
  object Label3: TLabel [2]
    Left = 13
    Top = 136
    Width = 263
    Height = 16
    Caption = #1047#1085#1072#1095#1077#1085#1080#1077' '#1074#1086' '#1074#1085#1091#1090#1088#1077#1085#1085#1077#1084' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1077
  end
  inherited plBottom: TPanel
    Top = 224
    TabOrder = 3
  end
  object cbDictType: TDBComboBoxEh [4]
    Left = 13
    Top = 32
    Width = 508
    Height = 24
    DataField = 'data_type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090#1099)
    KeyItems.Strings = (
      'counteragents')
    TabOrder = 0
    Visible = True
  end
  object edExtText: TDBEditEh [5]
    Left = 13
    Top = 94
    Width = 508
    Height = 24
    DataField = 'text_caption'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edDictValue: TDBEditEh [6]
    Left = 13
    Top = 158
    Width = 508
    Height = 24
    DataField = 'data_caption'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 136
    Top = 216
  end
  inherited qrAux: TADOQuery
    Left = 200
    Top = 216
  end
end
