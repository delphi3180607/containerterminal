﻿inherited FormEditExtraService: TFormEditExtraService
  Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1072#1103' '#1091#1089#1083#1091#1075#1072
  ClientHeight = 324
  ClientWidth = 501
  ExplicitWidth = 507
  ExplicitHeight = 352
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 8
    Top = 8
    Width = 119
    Height = 16
    Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object Label3: TLabel [1]
    Left = 167
    Top = 8
    Width = 110
    Height = 16
    Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object sbCustomer: TSpeedButton [2]
    Left = 362
    Top = 85
    Width = 52
    Height = 24
    Caption = '...'
    OnClick = sbCustomerClick
  end
  object Label9: TLabel [3]
    Left = 8
    Top = 66
    Width = 47
    Height = 16
    Caption = #1050#1083#1080#1077#1085#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel [4]
    Left = 8
    Top = 144
    Width = 102
    Height = 16
    Caption = #1058#1080#1087' '#1086#1089#1085#1086#1074#1072#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel [5]
    Left = 184
    Top = 141
    Width = 42
    Height = 16
    Caption = #1053#1086#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel [6]
    Left = 341
    Top = 141
    Width = 33
    Height = 16
    Caption = #1044#1072#1090#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TLabel [7]
    Left = 6
    Top = 213
    Width = 119
    Height = 16
    Caption = #1044#1072#1090#1072' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 283
    Width = 501
    TabOrder = 7
    ExplicitTop = 283
    ExplicitWidth = 501
    inherited btnCancel: TButton
      Left = 385
      ExplicitLeft = 385
    end
    inherited btnOk: TButton
      Left = 266
      ExplicitLeft = 266
    end
  end
  object edDocNumber: TDBEditEh [9]
    Left = 8
    Top = 30
    Width = 147
    Height = 24
    DataField = 'doc_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object dtDocDate: TDBDateTimeEditEh [10]
    Left = 167
    Top = 30
    Width = 121
    Height = 24
    DataField = 'doc_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object luCustomer: TDBLookupComboboxEh [11]
    Left = 8
    Top = 85
    Width = 348
    Height = 24
    DynProps = <>
    DataField = 'customer_id'
    DataSource = dsLocal
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    KeyField = 'id'
    ListField = 'code'
    ListSource = dm.dsCounteragents
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object cbBaseDoc: TDBComboBoxEh [12]
    Left = 8
    Top = 163
    Width = 170
    Height = 24
    DataField = 'basedoc_kind'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object cbBaseNumber: TDBEditEh [13]
    Left = 184
    Top = 163
    Width = 153
    Height = 24
    DataField = 'basedoc_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = True
  end
  object cbBaseDate: TDBDateTimeEditEh [14]
    Left = 343
    Top = 163
    Width = 146
    Height = 24
    DataField = 'basedoc_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
  object dtIncome: TDBDateTimeEditEh [15]
    Left = 8
    Top = 235
    Width = 146
    Height = 24
    DataField = 'date_factexecution'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 6
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 272
    Top = 224
  end
  inherited qrAux: TADOQuery
    Left = 208
    Top = 224
  end
end
