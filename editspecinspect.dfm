﻿inherited FormEditSpecInspect: TFormEditSpecInspect
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103
  ClientHeight = 197
  ClientWidth = 524
  ExplicitWidth = 530
  ExplicitHeight = 225
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 17
    Top = 16
    Width = 164
    Height = 16
    Caption = #1058#1077#1093#1085#1080#1095#1077#1089#1082#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel [1]
    Left = 18
    Top = 80
    Width = 82
    Height = 16
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 156
    Width = 524
    inherited btnCancel: TButton
      Left = 408
    end
    inherited btnOk: TButton
      Left = 289
    end
  end
  object cbTechCond: TDBLookupComboboxEh [3]
    Left = 17
    Top = 38
    Width = 489
    Height = 24
    DynProps = <>
    DataField = 'techcond_id'
    DataSource = dsLocal
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    KeyField = 'id'
    ListField = 'name'
    ListSource = FormTechConditions.dsData
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object edNote: TDBEditEh [4]
    Left = 18
    Top = 102
    Width = 488
    Height = 24
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 40
    Top = 144
  end
  inherited qrAux: TADOQuery
    Left = 96
    Top = 144
  end
end
