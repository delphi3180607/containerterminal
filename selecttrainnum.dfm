﻿inherited FormSelectTrainNum: TFormSelectTrainNum
  Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1050#1055
  ClientHeight = 143
  ClientWidth = 387
  ExplicitWidth = 393
  ExplicitHeight = 171
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 24
    Top = 26
    Width = 92
    Height = 16
    Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1050#1055
  end
  inherited plBottom: TPanel
    Top = 102
    Width = 387
    TabOrder = 1
    ExplicitTop = 102
    ExplicitWidth = 387
    inherited btnCancel: TButton
      Left = 271
      ExplicitLeft = 271
    end
    inherited btnOk: TButton
      Left = 152
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      ExplicitLeft = 152
    end
  end
  object cbTrains: TComboBox [2]
    Left = 24
    Top = 48
    Width = 329
    Height = 24
    TabOrder = 0
  end
  inherited dsLocal: TDataSource
    Left = 224
    Top = 8
  end
  inherited qrAux: TADOQuery
    Parameters = <
      item
        Name = 'folder_id'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select train_num, max_date from'
      '('
      'select sh.train_num, max(d.doc_date) as max_date '
      'from doccargosheet sh, documents d'
      'where sh.id = d.id'
      'group by sh.train_num'
      ') s1'
      'order by 2 desc')
    Left = 168
    Top = 8
  end
end
