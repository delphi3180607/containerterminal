﻿unit ReportByCounteragent;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, DBSQLLookUp, EXLReportExcelTLB,
  EXLReportBand, EXLReport, System.Actions, Vcl.ActnList, MemTableEh,
  DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton;

type
  TFormReportByCounteragent = class(TFormGrid)
    Panel3: TPanel;
    luCustomer: TDBSQLLookUp;
    Panel6: TPanel;
    dtStart: TDBDateTimeEditEh;
    drvHistory: TADODataDriverEh;
    meHistory: TMemTableEh;
    dsHistory: TDataSource;
    SplitterHor: TSplitter;
    Panel8: TPanel;
    dgHistory: TDBGridEh;
    sbRun: TButton;
    procedure sbRunClick(Sender: TObject);
    procedure meDataAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormReportByCounteragent: TFormReportByCounteragent;

implementation

{$R *.dfm}

procedure TFormReportByCounteragent.Init;
begin
  //--
  meData.Close;
  if dtStart.Value = null then dtStart.Value := IncMonth(now(),-3);
end;


procedure TFormReportByCounteragent.meDataAfterScroll(DataSet: TDataSet);
begin

  meHistory.Close;
  drvHistory.SelectCommand.Parameters.ParamByName('object_id').Value := meData.FieldByName('id').AsInteger;
  meHistory.Open;
  Screen.Cursor := crDefault;

end;

procedure TFormReportByCounteragent.sbRunClick(Sender: TObject);
begin
  meData.Close;
  drvData.SelectCommand.Parameters.ParamByName('datestart').Value := dtStart.Value;
  drvData.SelectCommand.Parameters.ParamByName('contid').Value := luCustomer.KeyValue;
  meData.Open;
end;

end.
