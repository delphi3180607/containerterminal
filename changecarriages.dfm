﻿inherited FormChangeCarriages: TFormChangeCarriages
  Caption = #1047#1072#1084#1077#1085#1072' '#1074#1072#1075#1086#1085#1086#1074
  ClientHeight = 279
  ClientWidth = 403
  Font.Height = -12
  ExplicitWidth = 409
  ExplicitHeight = 307
  PixelsPerInch = 96
  TextHeight = 14
  object laCarriageNew: TDBSQLLookUp [0]
    Left = 24
    Top = 196
    Width = 361
    Height = 22
    ControlLabel.Width = 65
    ControlLabel.Height = 13
    ControlLabel.Caption = #1053#1086#1074#1099#1081' '#1074#1072#1075#1086#1085
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 3
    Visible = True
    SqlSet = ssCarriages
    KeyValue = -1
  end
  object cbNew: TCheckBox [1]
    Left = 24
    Top = 132
    Width = 201
    Height = 17
    Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1085#1072' '#1085#1086#1074#1099#1081' '#1074#1072#1075#1086#1085
    TabOrder = 2
    OnClick = cbNewClick
  end
  object laCarriage2: TDBSQLLookUp [2]
    Left = 24
    Top = 84
    Width = 361
    Height = 22
    ControlLabel.Width = 127
    ControlLabel.Height = 13
    ControlLabel.Caption = #1042#1072#1075#1086#1085' 2 '#1080#1079' '#1101#1090#1086#1081' '#1087#1086#1075#1088#1091#1079#1082#1080
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 1
    Visible = True
    SqlSet = ssCarFromLoad
    KeyValue = -1
  end
  object laCarriage1: TDBSQLLookUp [3]
    Left = 24
    Top = 28
    Width = 361
    Height = 22
    ControlLabel.Width = 127
    ControlLabel.Height = 13
    ControlLabel.Caption = #1042#1072#1075#1086#1085' 1 '#1080#1079' '#1101#1090#1086#1081' '#1087#1086#1075#1088#1091#1079#1082#1080
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <
      item
      end>
    ReadOnly = True
    TabOrder = 0
    Visible = True
    SqlSet = ssCarriages
    KeyValue = -1
  end
  inherited plBottom: TPanel
    Top = 238
    Width = 403
    TabOrder = 4
    ExplicitTop = 238
    ExplicitWidth = 403
    inherited btnCancel: TButton
      Left = 287
      ExplicitLeft = 287
    end
    inherited btnOk: TButton
      Left = 168
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
      ExplicitLeft = 168
    end
  end
  inherited dsLocal: TDataSource
    Left = 80
    Top = 232
  end
  inherited qrAux: TADOQuery
    Left = 24
    Top = 232
  end
  object ssCarriages: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select id, pnum from carriages c where pnum like '#39'%@pattern%'#39
      'order by pnum')
    DownSql.Strings = (
      'select top 10 id, pnum from carriages c order by pnum')
    InitSql.Strings = (
      'select id, pnum from carriages c where id = @id')
    KeyName = 'id'
    DisplayName = 'pnum'
    Connection = dm.connMain
    Left = 144
    Top = 232
  end
  object ssCarFromLoad: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select cr.id, cr.pnum from docloadjoint k, carriages cr'
      'where cr.id = k.carriage_id and k.load_plan_id = :load_plan_id'
      'and cr.pnum like '#39'%@pattern%'#39
      'order by cr.pnum')
    DownSql.Strings = (
      'select cr.id, cr.pnum from docloadjoint k, carriages cr'
      'where cr.id = k.carriage_id and k.load_plan_id = :load_plan_id'
      'order by cr.pnum')
    InitSql.Strings = (
      'select cr.id, cr.pnum from docloadjoint k, carriages cr'
      'where cr.id = k.carriage_id and k.load_plan_id = :load_plan_id'
      'and cr.id = @id')
    KeyName = 'id'
    DisplayName = 'pnum'
    Connection = dm.connMain
    Left = 272
    Top = 128
  end
end
