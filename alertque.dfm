﻿inherited FormAlertQue: TFormAlertQue
  ActiveControl = dgData
  Caption = #1046#1091#1088#1085#1072#1083' '#1091#1074#1077#1076#1086#1084#1083#1077#1085#1080#1081
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited plTop: TPanel
      inherited sbDelete: TPngSpeedButton
        Left = 272
        Width = 29
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 57
        ExplicitWidth = 29
      end
      inherited btFilter: TPngSpeedButton
        Left = 344
        Width = 36
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 139
        ExplicitWidth = 36
        ExplicitHeight = 29
      end
      inherited btExcel: TPngSpeedButton
        Left = 308
        Width = 36
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 103
        ExplicitWidth = 36
        ExplicitHeight = 29
      end
      inherited sbAdd: TPngSpeedButton
        Left = 204
        ExplicitLeft = 204
      end
      inherited sbEdit: TPngSpeedButton
        Left = 238
        ExplicitLeft = 31
      end
      inherited Bevel2: TBevel
        Left = 304
        ExplicitLeft = 89
      end
      object Bevel1: TBevel [7]
        AlignWithMargins = True
        Left = 383
        Top = 0
        Width = 1
        Height = 29
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 250
        ExplicitTop = -3
        ExplicitHeight = 43
      end
      object sbSend: TPngSpeedButton [8]
        AlignWithMargins = True
        Left = 387
        Top = 0
        Width = 36
        Height = 26
        Hint = #1054#1090#1087#1088#1072#1074#1080#1090#1100' '#1089#1086#1086#1073#1097#1077#1085#1080#1103
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbSendClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C08648800000009704859730000004E000000
          4E01B1CD1F330000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000001964944415478DA8DD2CD2E03511407
          F0FFBD9DA9EA470C094D3524120B9178036B89B0E009C482155B361EC306096F
          616B2F62457C242AE2236949196A5A35F71E67A64AA633A5673373E7DCF39B33
          67AE2022B40B955B5928B9CE64EE7177A9DD1ED116C8AF8E29A2A3EBAA9DEA31
          1333FDA59DFDCE8191C584AEA70E393551A8DA30A5AC595AE6ACE7BD978E0035
          B8B2C59765EFDE03BC48C6CCB3FCD3EE78085007D3EB5A93194AAC0D2DE0213E
          EA01C9B470B39BB727A1371970853A9E25BA7200B7A593ED61E03CED77D09D06
          721B17C1BC14D0D93877703947A86BF888A33A030C099DE7A6BBC437E08506E8
          8691F26763D3A10594E2289F0266B68ECCFC5DE379B7C1C50677D09C41136846
          F103745F63ED7B6D577E5264C54103B26588AD8017AF2EA8C0DD286A004280FA
          B9D812E1731005E87205E4B890760C7862209300623CB43E9E91F807D0CF5CFC
          FED14C4316257EAA4C82EE0D220140BFBC832AB5E0DF2A1A813575117F8A0A03
          51C551808F2418E951BF80B61DD05B15511105F84892CF4E46437C1E4D11BD46
          17FF05F8484AE30BEB54D924958CE7840000000049454E44AE426082}
        ExplicitLeft = 189
        ExplicitHeight = 29
      end
      object Panel6: TPanel
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 201
        Height = 29
        Margins.Left = 0
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alLeft
        TabOrder = 1
        object dtStart: TDBDateTimeEditEh
          AlignWithMargins = True
          Left = 91
          Top = 4
          Width = 106
          Height = 21
          Margins.Left = 90
          ControlLabel.Width = 81
          ControlLabel.Height = 13
          ControlLabel.Caption = #1053#1072#1095#1080#1085#1072#1103' '#1089' '#1076#1072#1090#1099
          ControlLabel.Visible = True
          ControlLabelLocation.Position = lpLeftCenterEh
          Align = alBottom
          DynProps = <>
          EditButtons = <>
          Kind = dtkDateEh
          TabOrder = 0
          Visible = True
        end
      end
    end
    inherited dgData: TDBGridEh
      Left = 129
      Width = 691
      AllowedOperations = [alopUpdateEh, alopDeleteEh]
      DrawMemoText = True
      IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghFitRowHeightToText, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghAutoFitRowHeight]
      ReadOnly = False
      RowHeight = 2
      RowLines = 1
      TitleParams.MultiTitle = True
      OnDrawColumnCell = nil
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_created'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
          Width = 129
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cname'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
          Width = 106
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'email'
          Footers = <>
          Width = 145
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'issent'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1054#1090#1087#1088#1072#1074#1083#1077#1085#1086'?'
          Width = 98
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_sent'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1086#1090#1087#1088#1072#1074#1082#1080
          Width = 112
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'message'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Verdana'
          Font.Style = []
          Footers = <>
          Title.Caption = #1057#1086#1086#1073#1097#1077#1085#1080#1077
          Width = 510
          WordWrap = True
        end>
    end
    inherited plHint: TPanel
      TabOrder = 3
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object plSwitch: TPanel
      Left = 0
      Top = 29
      Width = 129
      Height = 441
      Align = alLeft
      BevelOuter = bvNone
      Color = 15131359
      ParentBackground = False
      TabOrder = 2
      StyleElements = [seFont, seBorder]
      object sbCurrent: TSpeedButton
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 123
        Height = 25
        Align = alTop
        GroupIndex = 1
        Down = True
        Caption = #1058#1077#1082#1091#1097#1080#1077
        OnClick = sbCurrentClick
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 107
      end
      object sbRecent: TSpeedButton
        AlignWithMargins = True
        Left = 3
        Top = 34
        Width = 123
        Height = 25
        Align = alTop
        GroupIndex = 1
        Caption = #1054#1090#1087#1088#1072#1074#1083#1077#1085#1085#1099#1077
        OnClick = sbRecentClick
        ExplicitLeft = -6
        ExplicitTop = 25
        ExplicitWidth = 107
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select a.*,'
      
        '(select code from counteragents c where c.id = a.counteragent_id' +
        ') as cname'
      
        'from alertque a where issent  = :issentcurrent and date_created ' +
        '>= :start_date'
      'order by date_created desc'
      '')
    SelectCommand.Parameters = <
      item
        Name = 'issentcurrent'
        Size = -1
        Value = Null
      end
      item
        Name = 'start_date'
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update alertque'
      'set'
      '  message = :message,'
      '  email = :email,'
      '  issent = :issent'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'message'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'email'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'issent'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from alertque where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select a.*,'
      
        '(select code from counteragents c where c.id = a.counteragent_id' +
        ') as cname'
      'from alertque a where id = :current_id'
      'order by date_created desc'
      ''
      '')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
      Visible = False
    end
    inherited aEdit: TAction
      Enabled = False
      Visible = False
    end
  end
  object IdEMAIL: TIdSMTP
    IOHandler = IdSSLHandler
    SASLMechanisms = <>
    UseTLS = utUseExplicitTLS
    Left = 464
    Top = 248
  end
  object IdMessage: TIdMessage
    AttachmentEncoding = 'UUE'
    BccList = <>
    CCList = <>
    Encoding = meDefault
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 464
    Top = 320
  end
  object IdSSLHandler: TIdSSLIOHandlerSocketOpenSSL
    Destination = ':25'
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Method = sslvTLSv1_2
    SSLOptions.SSLVersions = [sslvTLSv1_2]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 320
    Top = 328
  end
end
