﻿inherited FormEditDocSpecService: TFormEditDocSpecService
  Caption = #1059#1089#1083#1091#1075#1072
  ClientHeight = 265
  ClientWidth = 419
  OnShow = nePriceChange
  ExplicitWidth = 425
  ExplicitHeight = 293
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 8
    Top = 82
    Width = 47
    Height = 16
    Caption = #1059#1089#1083#1091#1075#1072
  end
  object sbService: TSpeedButton [1]
    Left = 352
    Top = 104
    Width = 52
    Height = 24
    Caption = '...'
    OnClick = sbServiceClick
  end
  object Цена: TLabel [2]
    Left = 8
    Top = 152
    Width = 34
    Height = 16
    Caption = #1062#1077#1085#1072
  end
  object Label1: TLabel [3]
    Left = 144
    Top = 152
    Width = 79
    Height = 16
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
  end
  object Label3: TLabel [4]
    Left = 283
    Top = 152
    Width = 43
    Height = 16
    Caption = #1057#1091#1084#1084#1072
  end
  object Label4: TLabel [5]
    Left = 8
    Top = 10
    Width = 49
    Height = 16
    Caption = #1054#1073#1098#1077#1082#1090
  end
  object sbObject: TSpeedButton [6]
    Left = 352
    Top = 32
    Width = 52
    Height = 24
    Caption = '...'
    OnClick = sbObjectClick
  end
  inherited plBottom: TPanel
    Top = 224
    Width = 419
    TabOrder = 5
    ExplicitTop = 241
    ExplicitWidth = 445
    inherited btnCancel: TButton
      Left = 303
      ExplicitLeft = 329
    end
    inherited btnOk: TButton
      Left = 184
      ExplicitLeft = 210
    end
  end
  object luService: TDBLookupComboboxEh [8]
    Left = 8
    Top = 104
    Width = 338
    Height = 24
    DynProps = <>
    DataField = 'service_id'
    DataSource = FormExtraServices.dsSpec
    EditButtons = <>
    KeyField = 'id'
    ListField = 'scode'
    ListSource = dm.dsServices
    TabOrder = 1
    Visible = True
  end
  object nePrice: TDBNumberEditEh [9]
    Left = 8
    Top = 174
    Width = 121
    Height = 24
    DataField = 'price'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
    OnChange = nePriceChange
  end
  object neAmount: TDBNumberEditEh [10]
    Left = 144
    Top = 174
    Width = 121
    Height = 24
    DataField = 'amount'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
    OnChange = nePriceChange
  end
  object neSumma: TDBNumberEditEh [11]
    Left = 283
    Top = 174
    Width = 121
    Height = 24
    DataField = 'summa'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    ReadOnly = True
    TabOrder = 4
    Visible = True
  end
  object luObject: TDBSQLLookUp [12]
    Left = 8
    Top = 32
    Width = 338
    Height = 24
    DataField = 'object_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 0
    Visible = True
    SqlSet = ssObject
  end
  inherited dsLocal: TDataSource
    Left = 88
    Top = 224
  end
  inherited qrAux: TADOQuery
    Left = 176
    Top = 224
  end
  object ssObject: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select * from containers where cnum like '#39'@pattern%'#39' order by cn' +
        'um')
    DownSql.Strings = (
      'select * from containers order by cnum')
    InitSql.Strings = (
      'select * from containers where id = @id')
    KeyName = 'id'
    DisplayName = 'cnum'
    Connection = dm.connMain
    Left = 182
    Top = 40
  end
end
