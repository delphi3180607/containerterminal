﻿inherited FormSelectPassOpKind: TFormSelectPassOpKind
  Caption = #1042#1099#1073#1086#1088' '#1074#1080#1076#1072' '#1086#1087#1077#1088#1072#1094#1080#1080' '#1076#1083#1103' '#1084#1072#1090#1077#1088#1080#1072#1083#1100#1085#1086#1075#1086' '#1087#1088#1086#1087#1091#1089#1082#1072
  ClientHeight = 527
  ClientWidth = 867
  ExplicitWidth = 883
  ExplicitHeight = 565
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 486
    Width = 867
    Visible = True
    ExplicitTop = 486
    ExplicitWidth = 867
    inherited btnOk: TButton
      Left = 632
      Caption = #1055#1088#1086#1076#1086#1083#1078#1080#1090#1100
      ExplicitLeft = 632
    end
    inherited btnCancel: TButton
      Left = 751
      ExplicitLeft = 751
    end
  end
  inherited plAll: TPanel
    Width = 867
    Height = 486
    ExplicitWidth = 867
    ExplicitHeight = 486
    inherited plTop: TPanel
      Width = 867
      ExplicitWidth = 867
      inherited sbDelete: TPngSpeedButton
        Visible = False
      end
      inherited sbAdd: TPngSpeedButton
        Visible = False
      end
      inherited sbEdit: TPngSpeedButton
        Visible = False
      end
      inherited Bevel2: TBevel
        Visible = False
      end
      inherited btTool: TPngSpeedButton
        Left = 829
        ExplicitLeft = 699
      end
      inherited plCount: TPanel
        Left = 632
        ExplicitLeft = 632
      end
    end
    inherited dgData: TDBGridEh
      Width = 863
      Height = 453
      AutoFitColWidths = True
      Font.Height = -13
      TitleParams.Font.Height = -13
      TitleParams.MultiTitle = True
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1086#1087#1077#1088#1072#1094#1080#1080
          Width = 572
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'spare_child'
          Footers = <>
          Title.Caption = #1055#1072#1088#1085#1072#1103' '#1074#1090#1086#1088#1080#1095#1085#1072#1103
          Width = 100
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'spare_parent'
          Footers = <>
          Title.Caption = #1055#1072#1088#1085#1072#1103' '#1087#1077#1088#1074#1080#1095#1085#1072#1103
          Width = 121
        end>
    end
    inherited plHint: TPanel
      Top = 328
      ExplicitTop = 328
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'declare @passid int, @containerid int;'
      'select @passid = :passid, @containerid = :containerid;'
      ' '
      'select ok.*,  '
      
        '(select code from doctypes where id = ok.end_doctype_id) as end_' +
        'doctype_code,'
      
        '(select count(*) from passopsettings os where os.kind_id = ok.id' +
        ') as spec_amount,'
      
        'isnull((select 1 from passoperations po, passoperationkinds pok,' +
        ' passopsettings pos, tasks t'
      
        '    where po.parent_id = @passid and po.passoperation_id = pok.i' +
        'd and pos.kind_id = pok.id'
      '    and t.id = po.task_id and pos.dlv_type_id = t.dlv_type_id'
      '    and pos.spare_kind_id = ok.id'
      '),0) as spare_child,'
      'isnull((select 1 where '
      '   exists ('
      
        '    select 1 from passoperations po1, passoperationkinds ok1, pa' +
        'ssopsettings pos1, tasks t1, passopsettings pos '
      '    where po1.parent_id = @passid'
      
        '    and po1.passoperation_id = ok1.id and pos1.kind_id = ok1.id ' +
        'and t1.id = po1.task_id and pos1.dlv_type_id = t1.dlv_type_id'
      
        '    and pos.kind_id = ok.id and pos.spare_kind_id = ok1.id and p' +
        'os.dlv_type_id = t1.dlv_type_id'
      '  )'
      '),0) as spare_parent'
      'from passoperationkinds ok '
      'where ('
      '  not exists ('
      
        '    select 1 from passoperations po where po.parent_id = @passid' +
        ' '
      '  )'
      '  or not exists ('
      '    select 1 from passoperations po, passoperationkinds pok'
      
        '    where po.parent_id = @passid and po.passoperation_id = pok.i' +
        'd'
      
        '    and pok.direction = ok.direction and isnull(po.higher_id,0) ' +
        '=0 '
      '  )'
      '  or exists ('
      
        '    select 1 from passoperations po, passoperationkinds pok, pas' +
        'sopsettings pos'
      
        '    where po.parent_id = @passid and po.passoperation_id = pok.i' +
        'd and pos.kind_id = pok.id'
      
        '    and po.container_id = @containerid and pos.spare_kind_id = o' +
        'k.id'
      '  )'
      '  or exists ('
      
        '    select 1 from passoperations po1, passoperationkinds ok1, pa' +
        'ssopsettings pos1, tasks t1, passopsettings pos '
      
        '    where po1.parent_id = @passid and po1.container_id = @contai' +
        'nerid'
      
        '    and po1.passoperation_id = ok1.id and pos1.kind_id = ok1.id ' +
        'and t1.id = po1.task_id and pos1.dlv_type_id = t1.dlv_type_id'
      
        '    and pos.kind_id = ok.id and pos.spare_kind_id = ok1.id and p' +
        'os.dlv_type_id = t1.dlv_type_id'
      '  )'
      ')'
      'order by code')
    SelectCommand.Parameters = <
      item
        Name = 'passid'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'containerid'
        Size = -1
        Value = Null
      end>
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
    inherited aDelete: TAction
      Enabled = False
    end
    inherited aEdit: TAction
      Enabled = False
    end
  end
end
