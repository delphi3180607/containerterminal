﻿unit PassOperations;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Mask, DBCtrlsEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton;

type
  TFormPassOperations = class(TFormGrid)
    plSpec: TPanel;
    plToolSpec: TPanel;
    sbAddSpec: TPngSpeedButton;
    sbDeleteSpec: TPngSpeedButton;
    sbEditSpec: TPngSpeedButton;
    Bevel8: TBevel;
    sbExcelSpec: TPngSpeedButton;
    dgSpec: TDBGridEh;
    Splitter1: TSplitter;
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    procedure sbAddSpecClick(Sender: TObject);
    procedure sbDeleteSpecClick(Sender: TObject);
    procedure sbEditSpecClick(Sender: TObject);
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure meDataAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormPassOperations: TFormPassOperations;

implementation

{$R *.dfm}

uses editcodename, functions, EditPassOpSettings, EditPassOpKind;

procedure TFormPassOperations.Init;
begin
  inherited;
  self.formEdit := FormEditPassOpKind;
  self.meSpec.Open;

end;

procedure TFormPassOperations.meDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  meDataAfterScroll(DataSet);
end;

procedure TFormPassOperations.meDataAfterScroll(DataSet: TDataSet);
begin
  inherited;
  drvSpec.SelectCommand.Parameters.ParamByName('kind_id').Value := self.meData.FieldByName('id').AsInteger;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormPassOperations.sbAddSpecClick(Sender: TObject);
begin
  EA(self, FormEditPassOpSettings, self.meSpec, 'passopsettings', 'kind_id', self.meData.FieldByName('id').AsInteger);
end;

procedure TFormPassOperations.sbDeleteSpecClick(Sender: TObject);
begin
  ED(meSpec);
end;

procedure TFormPassOperations.sbEditSpecClick(Sender: TObject);
begin
  EE(self, FormEditPassOpSettings, self.meSpec, 'passopsettings');
end;

end.
