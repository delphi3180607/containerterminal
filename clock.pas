﻿unit Clock;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh;

type
  TFormClock = class(TFormGrid)
  private
    { Private declarations }
  public
    table_name: string;
    table_id: integer;
    procedure Init; override;
  end;

var
  FormClock: TFormClock;

implementation

{$R *.dfm}

procedure TFormClock.Init;
begin
  meData.Close;
  drvData.SelectCommand.Parameters.ParamByName('table_name').Value := table_name;
  drvData.SelectCommand.Parameters.ParamByName('table_id').Value := table_id;
  inherited;
end;


end.
