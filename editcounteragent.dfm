﻿inherited FormEditCounteragent: TFormEditCounteragent
  Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
  ClientHeight = 454
  ClientWidth = 757
  Font.Height = -11
  OnShow = FormShow
  ExplicitWidth = 763
  ExplicitHeight = 482
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 413
    Width = 757
    ExplicitTop = 413
    ExplicitWidth = 757
    inherited btnCancel: TButton
      Left = 641
      ExplicitLeft = 641
    end
    inherited btnOk: TButton
      Left = 522
      OnClick = btnOkClick
      ExplicitLeft = 522
    end
  end
  object pc: TPageControl [1]
    Left = 0
    Top = 0
    Width = 757
    Height = 413
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = #1054#1089#1085#1086#1074#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      object Label2: TLabel
        Left = 12
        Top = 7
        Width = 24
        Height = 13
        Caption = #1048#1053#1053
      end
      object Label3: TLabel
        Left = 313
        Top = 47
        Width = 85
        Height = 13
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      end
      object Label1: TLabel
        Left = 12
        Top = 190
        Width = 36
        Height = 13
        Caption = #1040#1076#1088#1077#1089
      end
      object Label4: TLabel
        Left = 12
        Top = 237
        Width = 83
        Height = 13
        Caption = #1056#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1100
      end
      object Label5: TLabel
        Left = 12
        Top = 283
        Width = 100
        Height = 13
        Caption = #1050#1086#1085#1090#1072#1082#1090#1085#1086#1077' '#1083#1080#1094#1086
      end
      object Label6: TLabel
        Left = 12
        Top = 47
        Width = 23
        Height = 13
        Caption = #1050#1086#1076
      end
      object Label7: TLabel
        Left = 12
        Top = 95
        Width = 31
        Height = 13
        Caption = 'Email'
      end
      object Label9: TLabel
        Left = 12
        Top = 328
        Width = 60
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085#1099
      end
      object Label10: TLabel
        Left = 513
        Top = 190
        Width = 84
        Height = 13
        Caption = #1044#1086#1075#1086#1074#1086#1088': '#1076#1072#1090#1072
      end
      object Label11: TLabel
        Left = 628
        Top = 190
        Width = 92
        Height = 13
        Caption = #1044#1086#1075#1086#1074#1086#1088': '#1085#1086#1084#1077#1088
      end
      object Label8: TLabel
        Left = 313
        Top = 6
        Width = 51
        Height = 13
        Caption = #1050#1086#1076' '#1045#1051#1057
      end
      object edINN: TDBEditEh
        Left = 12
        Top = 22
        Width = 292
        Height = 21
        DataField = 'INN'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        HighlightRequired = True
        ParentFont = False
        TabOrder = 0
        Visible = True
      end
      object edName: TDBEditEh
        Left = 313
        Top = 63
        Width = 430
        Height = 21
        DataField = 'name'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        HighlightRequired = True
        ParentFont = False
        TabOrder = 1
        Visible = True
      end
      object edLocation: TDBEditEh
        Left = 12
        Top = 208
        Width = 492
        Height = 21
        DataField = 'location'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Visible = True
      end
      object edChief: TDBEditEh
        Left = 12
        Top = 255
        Width = 492
        Height = 21
        DataField = 'chief'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        Visible = True
      end
      object edManager: TDBEditEh
        Left = 12
        Top = 301
        Width = 492
        Height = 21
        DataField = 'manager'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        Visible = True
      end
      object edCode: TDBEditEh
        Left = 12
        Top = 63
        Width = 292
        Height = 21
        DataField = 'code'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        HighlightRequired = True
        ParentFont = False
        TabOrder = 5
        Visible = True
      end
      object edEmail: TDBEditEh
        Left = 12
        Top = 111
        Width = 731
        Height = 75
        AutoSize = False
        DataField = 'email'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        Visible = True
        WordWrap = True
      end
      object edContacts: TDBEditEh
        Left = 12
        Top = 346
        Width = 492
        Height = 21
        DataField = 'contacts'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
        Visible = True
      end
      object edContractNumber: TDBEditEh
        Left = 628
        Top = 208
        Width = 103
        Height = 21
        DataField = 'contract_num'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
        Visible = True
      end
      object dtContractDate: TDBDateTimeEditEh
        Left = 513
        Top = 208
        Width = 108
        Height = 21
        DataField = 'contract_date'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Kind = dtkDateEh
        ParentFont = False
        TabOrder = 9
        Visible = True
      end
      object edELC: TDBEditEh
        Left = 313
        Top = 22
        Width = 172
        Height = 21
        DataField = 'els_code'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 10
        Visible = True
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1050#1083#1072#1089#1089#1080#1092#1080#1082#1072#1090#1086#1088#1099
      ImageIndex = 1
    end
  end
  inherited dsLocal: TDataSource
    Left = 101
    Top = 396
  end
  inherited qrAux: TADOQuery
    Left = 29
    Top = 396
  end
end
