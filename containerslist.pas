﻿unit ContainersList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand,
  EXLReport;

type
  TFormContainersList = class(TFormGrid)
    sbImport2: TPngSpeedButton;
    sbClearFilter: TPngSpeedButton;
    plStatus: TPanel;
    procedure sbImport2Click(Sender: TObject);
    procedure btFilterClick(Sender: TObject);
    procedure sbClearFilterClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    var g: string;
    gu: TGuid;
    procedure Init; override;
    procedure ImportSheet(data:Variant; DimX:integer; DimY:integer);
    procedure SetFilter(conditions: string);
  end;

var
  FormContainersList: TFormContainersList;

implementation

{$R *.dfm}

uses editcontainer, Import, filterlite, dmu;

procedure TFormContainersList.Init;
begin
  inherited;
  self.formEdit := FormEditContainer;
  self.tablename := 'objects';
end;


procedure TFormContainersList.meDataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  if Assigned(drvData.SelectCommand.Parameters.FindParam('guid')) then
      drvData.SelectCommand.Parameters.ParamByName('guid').Value := g;
end;

procedure TFormContainersList.sbClearFilterClick(Sender: TObject);
begin
  SetFilter('');
end;

procedure TFormContainersList.sbImport2Click(Sender: TObject);
begin
  FormImport.ShowModal;
  if FormImport.ModalResult = mrOk then
  begin
    Screen.Cursor:=crHourGlass;
    Application.ProcessMessages;
    try
      ImportSheet(FormImport.FData, FormImport.DimX, FormImport.DimY);
    finally
      meData.Close;
      meData.Open;
      Screen.Cursor:=crDEFAULT;
    end;
  end;
end;



procedure TFormContainersList.btFilterClick(Sender: TObject);
begin
  FormFilterLite.ShowModal;
  if FormFilterLite.ModalResult in [mrOk, mrYes] then
  begin
    CreateGuid(gu);
    g := GuidToString(gu);
    SetFilter(FormFilterLite.conditions);
  end;
end;


procedure TFormContainersList.SetFilter(conditions: string);
begin

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := conditions;
    dm.spCreateFilter.ExecProc;

    meData.Close;
    meData.Open;

    if conditions = '' then
      plStatus.Caption := ' Фильтр снят.'
    else
    begin
      plStatus.Caption := ' Установлен фильтр по списку номеров.';
    end;

end;


procedure TFormContainersList.ImportSheet(data:Variant; DimX:integer; DimY:integer);
var i: integer; error: integer;
var conttype, contnumber, contowner, weight, datesent, carowner, carnumber, seal, descr: string;
begin

  ShowMessage('Не реализовано - нет формата');

  exit;

  for i := 4 to DimY do
  begin
    //--
  end;

end;

end.
