﻿inherited FormAlertPopupWindow: TFormAlertPopupWindow
  Caption = #1055#1088#1086#1095#1090#1080#1090#1077', '#1101#1090#1086' '#1080#1085#1090#1077#1088#1077#1089#1085#1086'!'
  ClientHeight = 606
  ExplicitHeight = 644
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 565
    ExplicitTop = 565
    inherited btnOk: TButton
      Caption = #1055#1088#1086#1076#1086#1083#1078#1080#1090#1100
    end
    inherited btnCancel: TButton
      Visible = False
    end
  end
  inherited plAll: TPanel
    Height = 565
    ExplicitHeight = 565
    inherited plTop: TPanel
      Visible = False
    end
    inherited dgData: TDBGridEh
      Height = 536
      AutoFitColWidths = True
      Font.Height = -21
      Options = [dgEditing, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghHighlightFocus, dghClearSelection, dghFitRowHeightToText, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      RowHeight = 2
      RowLines = 1
      RowSizingAllowed = True
      STFilter.Visible = False
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1057#1086#1086#1073#1097#1077#1085#1080#1077
          Width = 685
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1055#1086#1076#1088#1086#1073#1085#1077#1077
          Width = 98
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
    inherited aDelete: TAction
      Enabled = False
    end
    inherited aEdit: TAction
      Enabled = False
    end
  end
end
