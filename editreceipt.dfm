﻿inherited FormEditReceipt: TFormEditReceipt
  Caption = #1050#1074#1080#1090#1072#1085#1094#1080#1103
  ClientHeight = 271
  ClientWidth = 273
  ExplicitWidth = 279
  ExplicitHeight = 299
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 28
    Top = 107
    Width = 114
    Height = 14
    Caption = #1087#1091#1089#1090#1086' - '#1085#1077' '#1084#1077#1085#1103#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    StyleElements = [seClient, seBorder]
  end
  object Label2: TLabel [1]
    Left = 28
    Top = 190
    Width = 114
    Height = 14
    Caption = #1087#1091#1089#1090#1086' - '#1085#1077' '#1084#1077#1085#1103#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    StyleElements = [seClient, seBorder]
  end
  inherited plBottom: TPanel
    Top = 230
    Width = 273
    TabOrder = 3
    inherited btnCancel: TButton
      Left = 157
    end
    inherited btnOk: TButton
      Left = 38
    end
  end
  object cbCheck: TCheckBox [3]
    Left = 27
    Top = 16
    Width = 230
    Height = 17
    Caption = #1050#1074#1080#1090#1072#1085#1094#1080#1103' '#1075#1086#1090#1086#1074#1072
    TabOrder = 0
  end
  object edDocNumber: TDBEditEh [4]
    Left = 27
    Top = 80
    Width = 147
    Height = 22
    ControlLabel.Width = 119
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object neSumma: TDBNumberEditEh [5]
    Left = 27
    Top = 160
    Width = 147
    Height = 24
    ControlLabel.Width = 43
    ControlLabel.Height = 16
    ControlLabel.Caption = #1057#1091#1084#1084#1072
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 248
    Top = 8
  end
  inherited qrAux: TADOQuery
    Left = 200
    Top = 8
  end
end
