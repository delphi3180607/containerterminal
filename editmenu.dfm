﻿inherited FormEditMenu: TFormEditMenu
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1087#1091#1085#1082#1090#1072' '#1084#1077#1085#1102
  ClientHeight = 241
  ClientWidth = 343
  ExplicitWidth = 349
  ExplicitHeight = 269
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 15
    Top = 14
    Width = 98
    Height = 16
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  object Label1: TLabel [1]
    Left = 15
    Top = 133
    Width = 57
    Height = 16
    Caption = #1055#1086#1088#1103#1076#1086#1082
  end
  object Label4: TLabel [2]
    Left = 15
    Top = 70
    Width = 95
    Height = 16
    Caption = #1058#1080#1087' '#1086#1087#1077#1088#1072#1094#1080#1080
  end
  inherited plBottom: TPanel
    Top = 200
    Width = 343
    ExplicitTop = 200
    ExplicitWidth = 343
    inherited btnCancel: TButton
      Left = 227
      ExplicitLeft = 227
    end
    inherited btnOk: TButton
      Left = 108
      ExplicitLeft = 108
    end
  end
  object edCode: TDBEditEh [4]
    Left = 15
    Top = 32
    Width = 305
    Height = 24
    DataField = 'code'
    DataSource = FormCarriageKinds.dsData
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object neOrder: TDBNumberEditEh [5]
    Left = 15
    Top = 155
    Width = 121
    Height = 24
    DataSource = FormCarriageKinds.dsData
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object leOperationKind: TDBSQLLookUp [6]
    Left = 15
    Top = 92
    Width = 305
    Height = 24
    DataField = 'objectstate_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 3
    Visible = True
    SqlSet = ssOperationKind
  end
  inherited dsLocal: TDataSource
    Left = 208
    Top = 144
  end
  object ssOperationKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from statekinds order by code')
    DownSql.Strings = (
      'select * from statekinds order by code')
    InitSql.Strings = (
      'select * from statekinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 193
    Top = 73
  end
end
