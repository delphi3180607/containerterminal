﻿unit InvoiceEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  DBGridEh, Vcl.Buttons, Vcl.Mask, DBCtrlsEh, DBLookupEh, Data.DB,
  Data.Win.ADODB;

type
  TFormEditInvoice = class(TFormEdit)
    luCustomer: TDBLookupComboboxEh;
    Label3: TLabel;
    sbOwner: TSpeedButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditInvoice: TFormEditInvoice;

implementation

{$R *.dfm}

end.
