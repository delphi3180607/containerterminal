﻿inherited FormImportLoad: TFormImportLoad
  Caption = #1048#1084#1087#1086#1088#1090' '#1092#1072#1081#1083#1072' '#1076#1080#1089#1083#1086#1082#1072#1094#1080#1080
  ClientHeight = 570
  ClientWidth = 908
  Font.Height = -11
  ExplicitWidth = 914
  ExplicitHeight = 598
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 529
    Width = 908
    ExplicitTop = 529
    ExplicitWidth = 908
    inherited btnCancel: TButton
      Left = 792
      ExplicitLeft = 792
    end
    inherited btnOk: TButton
      Left = 528
      Width = 258
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090#1099' '#1087#1086#1075#1088#1091#1079#1082#1080
      Enabled = False
      ExplicitLeft = 528
      ExplicitWidth = 258
    end
  end
  object pc: TPageControl [1]
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 902
    Height = 523
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = #1058#1072#1073#1083#1080#1094#1072
      object grid: TStringGrid
        AlignWithMargins = True
        Left = 3
        Top = 44
        Width = 888
        Height = 448
        Align = alClient
        ColCount = 10
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        ExplicitTop = 36
        ExplicitHeight = 456
      end
      object plTop: TPanel
        Left = 0
        Top = 0
        Width = 894
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 144
        ExplicitTop = 96
        ExplicitWidth = 185
        object btLink: TButton
          AlignWithMargins = True
          Left = 177
          Top = 3
          Width = 168
          Height = 35
          Align = alLeft
          Caption = #1057#1074#1103#1079#1072#1090#1100
          Constraints.MaxWidth = 894
          TabOrder = 0
          OnClick = btLinkClick
          ExplicitLeft = 18
          ExplicitTop = 5
          ExplicitHeight = 33
        end
        object btPaste: TButton
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 168
          Height = 35
          Align = alLeft
          Caption = #1042#1089#1090#1072#1074#1080#1090#1100
          Constraints.MaxWidth = 894
          TabOrder = 1
          OnClick = btPasteClick
          ExplicitLeft = 1
          ExplicitTop = 1
          ExplicitHeight = 39
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1057#1074#1103#1079#1099#1074#1072#1085#1080#1077
      ImageIndex = 1
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 894
        Height = 495
        Align = alClient
        DataSource = dsData
        DynProps = <>
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorOptions = [gioShowRowIndicatorEh]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
        TabOrder = 0
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -11
        TitleParams.Font.Name = 'Verdana'
        TitleParams.Font.Style = [fsBold]
        TitleParams.ParentFont = False
        Columns = <
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'car_err'
            Footers = <>
            MaxWidth = 25
            MinWidth = 25
            Title.Alignment = taCenter
            Title.Caption = '!'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Verdana'
            Title.Font.Style = []
            Width = 25
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'car_num'
            Footers = <>
            MaxWidth = 140
            MinWidth = 140
            Title.Caption = #1042#1072#1075#1086#1085
            Width = 140
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'cont1_err'
            Footers = <>
            MaxWidth = 25
            MinWidth = 25
            Title.Alignment = taCenter
            Title.Caption = '!'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Verdana'
            Title.Font.Style = []
            Width = 25
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'cont1_num'
            Footers = <>
            MaxWidth = 140
            MinWidth = 140
            Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088' 1'
            Width = 140
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'cont2_err'
            Footers = <>
            MaxWidth = 25
            MinWidth = 25
            Title.Alignment = taCenter
            Title.Caption = '!'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Verdana'
            Title.Font.Style = []
            Width = 25
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'cont2_num'
            Footers = <>
            MaxWidth = 140
            MinWidth = 140
            Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088' 2'
            Width = 140
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'cont3_err'
            Footers = <>
            MaxWidth = 25
            MinWidth = 25
            Title.Alignment = taCenter
            Title.Caption = '!'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Verdana'
            Title.Font.Style = []
            Width = 25
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'cont3_num'
            Footers = <>
            MaxWidth = 140
            MinWidth = 140
            Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088' 3'
            Width = 140
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'cont4_err'
            Footers = <>
            MaxWidth = 25
            MinWidth = 25
            Title.Alignment = taCenter
            Title.Caption = '!'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Verdana'
            Title.Font.Style = []
            Width = 25
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'cont4_num'
            Footers = <>
            MaxWidth = 140
            MinWidth = 140
            Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088' 4'
            Width = 140
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited dsLocal: TDataSource
    Left = 376
    Top = 320
  end
  inherited qrAux: TADOQuery
    Left = 312
    Top = 320
  end
  object drvData: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select * from ##temp_car_links order by car_num')
    SelectCommand.Parameters = <>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 312
    Top = 208
  end
  object meData: TMemTableEh
    Params = <>
    DataDriver = drvData
    Left = 360
    Top = 208
  end
  object dsData: TDataSource
    DataSet = meData
    Left = 416
    Top = 208
  end
end
