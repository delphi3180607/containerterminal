﻿object FormPsVzhRep: TFormPsVzhRep
  Left = 0
  Top = 0
  Caption = #1054#1090#1095#1077#1090' '#1055#1057#1042#1046
  ClientHeight = 626
  ClientWidth = 866
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object plAll: TPanel
    Left = 0
    Top = 0
    Width = 866
    Height = 626
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object plTop: TPanel
      Left = 0
      Top = 0
      Width = 866
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object sbBuild: TPngSpeedButton
        AlignWithMargins = True
        Left = 346
        Top = 6
        Width = 119
        Height = 27
        Margins.Left = 8
        Margins.Top = 6
        Margins.Bottom = 8
        Align = alLeft
        Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1089#1074#1086#1076
        StyleElements = [seFont, seClient]
        OnClick = sbBuildClick
        ExplicitTop = 3
      end
      object edStart: TDBDateTimeEditEh
        AlignWithMargins = True
        Left = 60
        Top = 10
        Width = 121
        Height = 21
        Margins.Left = 60
        Margins.Top = 10
        Margins.Bottom = 10
        ControlLabel.Width = 46
        ControlLabel.Height = 13
        ControlLabel.Caption = #1055#1077#1088#1080#1086#1076' '#1089
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 10
        ControlLabelLocation.Position = lpLeftTextBaselineEh
        Align = alLeft
        DynProps = <>
        EditButtons = <>
        Kind = dtkDateEh
        TabOrder = 0
        Visible = True
      end
      object edEnd: TDBDateTimeEditEh
        AlignWithMargins = True
        Left = 214
        Top = 10
        Width = 121
        Height = 21
        Margins.Left = 30
        Margins.Top = 10
        Margins.Bottom = 10
        ControlLabel.Width = 12
        ControlLabel.Height = 13
        ControlLabel.Caption = #1087#1086
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 10
        ControlLabelLocation.Position = lpLeftTextBaselineEh
        Align = alLeft
        DynProps = <>
        EditButtons = <>
        Kind = dtkDateEh
        TabOrder = 1
        Visible = True
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 41
      Width = 866
      Height = 585
      ActivePage = TabSheetPivot
      Align = alClient
      TabOrder = 1
      object TabSheetPivot: TTabSheet
        Caption = #1057#1074#1086#1076
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dgPivot: TDBGridEh
          Left = 0
          Top = 0
          Width = 858
          Height = 557
          Align = alClient
          DataSource = dsPivot
          DynProps = <>
          GridLineParams.VertEmptySpaceStyle = dessNonEh
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
          TabOrder = 0
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
      object TabSheetData: TTabSheet
        Caption = #1044#1072#1085#1085#1099#1077
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGridEh1: TDBGridEh
          Left = 0
          Top = 0
          Width = 858
          Height = 557
          Align = alClient
          DataSource = dsDetails
          DynProps = <>
          IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
          STFilter.InstantApply = True
          STFilter.Local = True
          STFilter.Visible = True
          TabOrder = 0
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'TabSheet1'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 858
          Height = 557
          Align = alClient
          TabOrder = 0
        end
      end
    end
  end
  object dsDetails: TDataSource
    DataSet = meData
    Left = 280
    Top = 225
  end
  object meData: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvData
    Left = 216
    Top = 225
  end
  object drvData: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select '
      
        '(case when isnull(od.direction_id,0)<>0 then '#39#1055#1057#1046#1042#1057#39' else '#39#1054#1041#1067#1063#1053 +
        #1067#1049#39' end) as has_sea_path,   '
      
        '(case when charindex('#39#1088#1072#1093#1090#39',od.extra_services)>0 then '#39#1052#1054#1056#1057#1050#1054#1049' '#1060 +
        #1056#1040#1061#1058#39' else '#39#39' end) as paid_sea_freight,'
      'upper(ct.code) as code, '
      'year(osh.date_factexecution) as yearop,'
      'month(osh.date_factexecution) as monthop,'
      '--doc.doc_date, doc.doc_number,'
      '--c.cnum,'
      '1 as dumb'
      
        'from documents doc, docorder od, objects2docspec ods, tasks t, o' +
        'bjectstatehistory osh,'
      'deliverytypes d, containers c, cargos cr, counteragents ct'
      'where osh.task_id = t.id and t.dlv_type_id = d.id'
      'and osh.linked_object_id = c.id'
      'and osh.object_id = cr.id'
      'and doc.id = od.id'
      'and od.id = ods.doc_id'
      'and ods.task_id = t.id'
      'and ods.object_id = c.id'
      'and osh.object_state_id = 12 '
      'and d.code = '#39#1054#1090#1087#1088#1072#1074#1082#1072#39
      'and isnull(cr.isempty,0)=0  '
      'and ct.id = t.payer_id'
      'order by 1,2,3,4,5'
      '')
    SelectCommand.Parameters = <>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 160
    Top = 225
  end
  object mePivot: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    Left = 360
    Top = 225
  end
  object dsPivot: TDataSource
    DataSet = mePivot
    Left = 424
    Top = 225
  end
end
