﻿inherited FormPasses: TFormPasses
  Caption = #1052#1072#1090#1077#1088#1080#1072#1083#1100#1085#1099#1077' '#1087#1088#1086#1087#1091#1089#1082#1080
  ClientHeight = 638
  ClientWidth = 1098
  ExplicitWidth = 1114
  ExplicitHeight = 676
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 597
    Width = 1098
    ExplicitTop = 597
    ExplicitWidth = 1098
    inherited btnOk: TButton
      Left = 863
      ExplicitLeft = 863
    end
    inherited btnCancel: TButton
      Left = 982
      ExplicitLeft = 982
    end
  end
  inherited plAll: TPanel
    Width = 1098
    Height = 597
    ExplicitWidth = 1098
    ExplicitHeight = 597
    object Splitter1: TSplitter [0]
      Left = 0
      Top = 217
      Width = 1098
      Height = 7
      Cursor = crVSplit
      Align = alBottom
      Color = 15132390
      ParentColor = False
      StyleElements = [seFont, seBorder]
      ExplicitTop = 218
    end
    inherited plTop: TPanel
      Width = 1098
      Caption = 'F5 - '#1054#1041#1053#1054#1042#1048#1058#1068' '#1057#1055#1048#1057#1054#1050
      ExplicitWidth = 1098
      inherited sbDelete: TPngSpeedButton
        ExplicitTop = -3
      end
      inherited btFilter: TPngSpeedButton
        Left = 239
        OnClick = btFilterClick
        ExplicitLeft = 398
      end
      inherited btExcel: TPngSpeedButton
        Left = 205
        ExplicitLeft = 398
      end
      inherited btTool: TPngSpeedButton
        Left = 1060
        ExplicitLeft = 868
      end
      object sbReport: TPngSpeedButton [7]
        AlignWithMargins = True
        Left = 309
        Top = 0
        Width = 32
        Height = 27
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        OnClick = sbReportClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000002B44944415478DA8D935F4853511CC7BF6773BA810DA5B1
          E67AA837A9B4873996218B2437B13F0FBE0CFA3308F7626EB03DE8430A8B8D28
          E7439A3421C8F6201A810FEACB40A82C17924E8CA93DD834219C9256DA74CE7B
          77EFEDDC5B6A7F46F4BBDC7B7FE7F7BBDFCFF99D737F870882805DB3785F5D57
          A9F3AECA08CED121A1571A0201C765F2BE7E16BA22810A17FE30F22BE0FCDDD7
          4C8FAB4C21A384DDB0F85EDD64D0175D47E2437AFBFDDC5CE1CB071776B2022E
          B58DA57A5D65AAF817B92805CBF16032024C4714985D15107E9BC0F3D1F927CF
          EE9CBD921570B16D6CBBCF55A65C1001047B80C3F92C0EA8E474293CAEB54F66
          867D66C55F00BFDFAF1A5756ADF73A8DB91FBF890042C53C850848A658303C8F
          8AA30AD8EE4D30FA85EE825028B4BD07A06207F53B269455F93D4E2356920AF0
          74091CCD31AC8014CB81A500833E0797DBC751F82EB8C9308CA7BFBFBF9BF87C
          3E4B5151D1B0C16080FF058BD00D03DD3405C4BA3274769E4252B412D13F7E88
          C07E7F1237CF6430383888E9E969AB0888D5D6D6966AB55A048341FC8F399D4E
          C4E371747676CE8B809D9696965C3131353585E2E2E27F8A67666660329924BF
          B1B151205EAF57A0002930343404A3D188B5B5B5AC628D46834824029BCD268D
          EBEBEB419A9B9B05BBDD2E0546464650595989E5E5E5AC009D4E8770388C9A9A
          9A7D4053539360B55AA5402C1643757535969696B202F47A3D060606505E5E2E
          8DDD6E3788C7E3112C168B14585C5CDC2B3F9D4E636B6B0BC964121B1B1BD22D
          FEF29292126912D1A816A4A1A181339BCD32B55A8D68348A63274EE2A0469BB5
          82D54F2BB8EDBF85402080442281D6D6561087C3F19036C56996654B55663782
          7506A418487D20F600C789678203471B4957A084F3D124463BEC623531FAC9D3
          DFCE425DD71B2E472E93613FF4D3FDF124B4BD33199E7FEC3C25DFCD7F073274
          5465D0BF6E470000000049454E44AE426082}
        ExplicitLeft = 238
      end
      object btFlow: TPngSpeedButton [8]
        AlignWithMargins = True
        Left = 109
        Top = 0
        Width = 32
        Height = 27
        Hint = #1057#1086#1079#1076#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = btFlowClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
          6FA8640000013C4944415478DA6364A0103042484606759B585F591D9B6610F7
          F19523B5378F2CDECCF0FF3FBA62462B111193A36FDE9C463140DD36CED72638
          6FA382821C58E0FEC3C70CC7D64E0ABA7E68E10646A81AA051FF991819994FBA
          B8FC6EBC7AD563CBB367BBE006B864CEBA60EF1EA0C7CEC2F483899989E5D3B7
          1FFFEE9E38C4167E6AD61BD6BFBFFEFF6764648498C1C0C0C1CCCC2BCACECE51
          7BE58AF786A74FB7830D70CE9875DED123409F0D6AC067A001778E1F660B3F3D
          136E00D484FF9CCCCCFC22ECEC6CD5972F7B6C02BA02C90BF9402FC8801DF5E0
          C11386E31B26045D3BB008D30BCECEBFEBAF5C71D9F6E2C53E9440D4B08BF696
          D1B26F05719F5C3B587DE3D0D2ADD802D14258D8F0F8DBB7E7506381E268A44A
          3AB005A5033BA017FEFF7B7CF9F0683A20371D4869DAB53002859E5E3F5843B7
          74000005ABF01126EC5CA40000000049454E44AE426082}
        ExplicitLeft = 366
        ExplicitHeight = 23
      end
      object sbBarrel: TPngSpeedButton [9]
        AlignWithMargins = True
        Left = 141
        Top = 0
        Width = 32
        Height = 27
        Hint = #1040#1085#1085#1091#1083#1080#1088#1086#1074#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbBarrelClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C08648800000009704859730000005E000000
          5E01F8D90FF90000001974455874536F667477617265007777772E696E6B7363
          6170652E6F72679BEE3C1A0000018D4944415478DA636480004E1909DEBBEE8E
          4A3F1988003BF7DF637FF2E2B32A90F9951126C8AF22B7EBBFA696363106305E
          BB76EDE3DD47AE6036548C5B5655F502B7960E0731067CBF71FDCBC39B370C81
          CC1F3003985595652F589BC9F11363C0E1930FBEDDBDF7541FC8FC09F7829589
          CCFAA31BA20340ECBF7FFF33744D3BFD61CDB6DBDF17F4BB49EA6A88A218601D
          B074C3B1334F0291BD003700A4D93A68E5F3B71FD945FEFEFDCB3AB3D5E4BFAB
          9D0223D1063C79FE99C12E64F58BBFFFB9059919BFBDDFB6D04FE2EBF7BF7FE2
          0A76BC4908D1E028CD3413C06B0088FDFBCF3F8665EBAE7F3F77EDF547662646
          862D7BEEFF67E79290E4E5FCF0E4D8FA0819A20DB870E3ED072686FF8C9B6106
          707C7A726C43187E039EBDFCC26013B8EAE55F066E012686AF402FF84B7CFB41
          82174081681BBCF2F9EB0FECC2C0406423391061D1D837FBCC87555B6E7F9BD7
          E32A454C34B20113D2796042E22526219D38F3F8E38DDB8F8C81CC5F30033815
          B4B4CE71A8AAF31063C0D71BD7BE3DBE79D30094AAA99699C8CECE00EECDFE11
          A5E0CDF20000000049454E44AE426082}
        ExplicitLeft = 126
      end
      object sbClearFilter: TPngSpeedButton [10]
        AlignWithMargins = True
        Left = 274
        Top = 1
        Width = 34
        Height = 26
        Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1080#1083#1100#1090#1088
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbClearFilterClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000AEB00000A
          EB01828B0D5A0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000000B94944415478DA63FCFFFF3F032580
          D161CA0B8A4C607CF0E18740C2920FEF17C40890A411A88701A8479011E48587
          1F7F2A0005EE136B084CB33C3FFB07465818000D31004A9C276408B266B01790
          031168880350C17E5C86A06BC630006A480250E17C7443A09A0D819A2FA00422
          B668749CFAF23F230E2F8054EFCF1667C46B8013D080291B0319B4761D4311BF
          E666C590E3BF9E611F310600158135C00C81B18172C41B00D308023083E86B00
          455EA02810298E46520000704CAE7A8505FCA90000000049454E44AE426082}
        ExplicitLeft = 255
      end
      object sbClock: TPngSpeedButton [11]
        AlignWithMargins = True
        Left = 173
        Top = 0
        Width = 32
        Height = 27
        Hint = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1081
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbClockClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000000097048597300000B1300000B1301009A9C180000020E4944415478DA
          6364A031601CB560E85820EB3A574A53934F01C4BE7EFDD383C7BB939F51CD82
          9E4567DD0585F9E6DC7BF943869995E9BFA102F7ADBB0F5EE796C49AEEA6D882
          D0CACD96C6FA6A074EDFFDC0C6C1CDC9C0C6CEC6C0C5CECA6021CBFC69FDDE4B
          2EEB3AFC4E536441FBD24B7BCFDDFFECC42F2ACAC0C884502EC6C7C6C0F7E3F5
          9CF2789354B22DD0735BC49D9A697D79CFC5578A62D22228726CCC4C0CE6E2BF
          B64EAC3B1476F66CFA37327DF09FB16BD9B5D3F73E3118FF6566439111E16163
          10F8FD7A76799C491A45415434F9B49F8898D086A7DF18197FFDF90B713D0B33
          8386F0DF9F878EDDB45EDDE17396220B4060C6D6BBF97FFFB174BEFFF98F9D09
          C817E7667CF3F9F3E7A882305DCA5311188436B049FFD515335513D0FAFEF1F3
          F72B2FFFDE7FFAFDE96B86ED793F29B220B1EF640E270B532E3B072BFF9F3FFF
          1819FE333072B031FD16E365FBC2CDC6F0E1FD872F972EDEFFB06055B3DB5192
          2D289F7B65F983777F2278F8381998989850E478D85918AC35458011CDCCC0FA
          F7F387A3179E6494C518AF24DA028DC0856AA68E8637383839713A80979395C1
          555F9C415A8099E1FBE7B70FFDAB5638BCD850F880280B32261C6B7FF195B982
          839D8D011F3056126610E3676390E5FBC5B074EBF5C8B90DEE2B88B2A067EB35
          F9B72FFE443210009CECFF3FB133317EFFF8F1CB335EBEBFA72AA36DDF131D07
          D402A3160CBC05002213B81906054A2A0000000049454E44AE426082}
        ExplicitLeft = 160
        ExplicitTop = -3
      end
      inherited plCount: TPanel
        Left = 863
        ExplicitLeft = 863
      end
      object Panel6: TPanel
        AlignWithMargins = True
        Left = 341
        Top = 0
        Width = 217
        Height = 29
        Margins.Left = 0
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object dtStart: TDBDateTimeEditEh
          AlignWithMargins = True
          Left = 125
          Top = 3
          Width = 89
          Height = 21
          Margins.Left = 125
          Margins.Top = 0
          Margins.Bottom = 5
          ControlLabel.Width = 113
          ControlLabel.Height = 13
          ControlLabel.Caption = #1053#1072#1095#1080#1085#1072#1103' '#1089' '#1076#1072#1090#1099' '#1089#1095#1077#1090#1072
          ControlLabel.Visible = True
          ControlLabelLocation.Position = lpLeftCenterEh
          Align = alBottom
          DynProps = <>
          EditButtons = <>
          Kind = dtkDateEh
          TabOrder = 0
          Visible = True
        end
      end
      object btRequery: TButton
        AlignWithMargins = True
        Left = 564
        Top = 1
        Width = 119
        Height = 25
        Margins.Top = 1
        Align = alLeft
        Caption = #1055#1077#1088#1077#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100
        TabOrder = 2
        OnClick = btRequeryClick
      end
    end
    inherited dgData: TDBGridEh
      Top = 49
      Width = 1098
      Height = 168
      Font.Height = -13
      Font.Name = 'Calibri'
      STFilter.Color = 15725813
      TitleParams.Font.Height = -13
      TitleParams.Font.Name = 'Calibri'
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'id'
          Footers = <>
          Width = 47
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isprinted'
          Footers = <>
          Title.Caption = #1055#1077#1095#1072#1090#1100
          Width = 62
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isabolished'
          Footers = <>
          Title.Caption = #1054#1090#1084#1077#1085#1077#1085
          Width = 69
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'pass_type_text'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1087#1088#1086#1087#1091#1089#1082#1072
          Width = 173
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'pass_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 111
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'pass_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 105
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer_code'
          Footers = <>
          Title.Caption = #1050#1083#1080#1077#1085#1090
          Width = 115
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dealer_code'
          Footers = <>
          Title.Caption = #1055#1077#1088#1077#1074#1086#1079#1095#1080#1082
          Width = 115
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'driver_name'
          Footers = <>
          Title.Caption = #1055#1088#1077#1076#1089#1090#1072#1074#1080#1090#1077#1083#1100
          Width = 125
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_full'
          Footers = <>
          Title.Caption = #1040#1074#1090#1086#1084#1086#1073#1080#1083#1100
          Width = 139
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'onedaypass_text'
          Footers = <>
          Title.Caption = #1056#1072#1079#1086#1074#1099#1081' '#1087#1088#1086#1087#1091#1089#1082
          Width = 259
        end>
    end
    object plSpec: TPanel
      Left = 0
      Top = 224
      Width = 1098
      Height = 373
      Align = alBottom
      Color = 15132390
      ParentBackground = False
      TabOrder = 3
      object pcSpec: TPageControl
        Left = 1
        Top = 1
        Width = 1096
        Height = 371
        ActivePage = TabSheet1
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = #1055#1088#1080#1077#1084'/'#1074#1099#1076#1072#1095#1072' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074
          object dgSpec: TDBGridEh
            Left = 0
            Top = 27
            Width = 1088
            Height = 313
            Align = alClient
            AllowedOperations = []
            Border.Color = clSilver
            ColumnDefValues.Title.TitleButton = True
            Ctl3D = False
            DataSource = dsSpec
            DynProps = <>
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Calibri'
            Font.Style = []
            GridLineParams.BrightColor = 15395041
            GridLineParams.DarkColor = 15395041
            GridLineParams.DataBoundaryColor = 15395041
            GridLineParams.DataHorzColor = 14540253
            GridLineParams.DataVertColor = 14540253
            GridLineParams.VertEmptySpaceStyle = dessNonEh
            IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
            IndicatorParams.HorzLineColor = 14540253
            IndicatorParams.VertLineColor = 14540253
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            SelectionDrawParams.SelectionStyle = gsdsClassicEh
            SelectionDrawParams.DrawFocusFrame = False
            SelectionDrawParams.DrawFocusFrameStored = True
            ShowHint = True
            SortLocal = True
            STFilter.Color = 15725813
            STFilter.Font.Charset = DEFAULT_CHARSET
            STFilter.Font.Color = clWindowText
            STFilter.Font.Height = -11
            STFilter.Font.Name = 'Verdana'
            STFilter.Font.Style = []
            STFilter.HorzLineColor = 14540253
            STFilter.Local = True
            STFilter.ParentFont = False
            STFilter.VertLineColor = 14540253
            STFilter.Visible = True
            TabOrder = 0
            TitleParams.Color = clBtnFace
            TitleParams.FillStyle = cfstThemedEh
            TitleParams.Font.Charset = DEFAULT_CHARSET
            TitleParams.Font.Color = clWindowText
            TitleParams.Font.Height = -13
            TitleParams.Font.Name = 'Calibri'
            TitleParams.Font.Style = [fsBold]
            TitleParams.HorzLineColor = 13421772
            TitleParams.MultiTitle = True
            TitleParams.ParentFont = False
            TitleParams.SecondColor = 15987699
            TitleParams.SortMarkerStyle = smstDefaultEh
            TitleParams.VertLineColor = 13421772
            Columns = <
              item
                CellButtons = <>
                DynProps = <>
                EditButtons = <>
                FieldName = 'op_kind_code'
                Footers = <>
                Title.Caption = #1054#1087#1077#1088#1072#1094#1080#1103
                Width = 125
              end
              item
                CellButtons = <>
                DynProps = <>
                EditButtons = <>
                FieldName = 'cnum'
                Footers = <>
                Title.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
                Width = 129
              end
              item
                CellButtons = <>
                DynProps = <>
                EditButtons = <>
                FieldName = 'seal_number'
                Footers = <>
                Title.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
                Width = 135
              end
              item
                CellButtons = <>
                DynProps = <>
                EditButtons = <>
                FieldName = 'cont_kind_code'
                Footers = <>
                Title.Caption = #1058#1080#1087' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
                Width = 109
              end
              item
                CellButtons = <>
                Checkboxes = True
                DynProps = <>
                EditButtons = <>
                FieldName = 'isempty'
                Footers = <>
                Title.Caption = #1055#1086#1088#1086#1078#1085#1080#1081'?'
                Width = 101
              end
              item
                CellButtons = <>
                DynProps = <>
                EditButtons = <>
                FieldName = 'doctext'
                Footers = <>
                Title.Caption = #1040#1082#1090' '#1087#1088#1080#1077#1084#1082#1080'/'#1074#1099#1076#1072#1095#1080
                Width = 172
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
          object plToolSpec: TPanel
            Left = 0
            Top = 0
            Width = 1088
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            Color = 15921906
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 28637
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            TabOrder = 1
            StyleElements = [seBorder]
            object sbAddSpec: TPngSpeedButton
              AlignWithMargins = True
              Left = 0
              Top = 0
              Width = 33
              Height = 27
              Hint = #1044#1086#1073#1072#1074#1080#1090#1100
              Margins.Left = 0
              Margins.Top = 0
              Margins.Right = 0
              Margins.Bottom = 0
              Align = alLeft
              Layout = blGlyphTop
              ParentShowHint = False
              ShowHint = True
              OnClick = sbAddSpecClick
              PngImage.Data = {
                89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
                61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
                6FA864000002B74944415478DA95536D485351187ECFB9F76E7326AE284DB7E1
                A696A625E43228B21267BF84C26819FD88304DD0B20F427FA53FB3122A23C8A1
                A112A97FB410131209A6865A7FC48FB2E676754E86A10B6D5F77F7DCCE068229
                86BDE7CFE19C97E73CCF799F07C186DA7B50AB3B909B752556AFB91C11A5D479
                573C7697CDF17AB26FB4D9353167DFD88FD6366C14C7E5969A9E1B4E1B8AB5AA
                DDB047150D724E063E21003FDD6E70B89760D8326CB63CEB2AF77B7CC25F0072
                A5823399CB470D71A919295AAD87E5582C4920134401640C17EA0A0485209972
                F0CA293B3FDE52566758030903E4555D32E7E7198BD2F409ABA2242909489845
                0CD144A9C1B1320F4149C418106111F68CF1D61DEF7BFBCD1F6ADBAE8701429A
                8B6AEFCC9CD89FE641182B10C52474C95939CE561F07CBFC10F8837E82018314
                5AA2E81BFCF15569BEFB38D135E5B0A39C5B0535172F9CBB9FA456FB0451A400
                40681B96313238167F143E3947202006282CA2E7803986F1CD2C3815CD4D6FAA
                86CC3D0F51E1939BD3E7F38CC92A6524A1BA8165584C2412D67E24D6009F5D5F
                2880001861088AC1103859F57BD9E6F6CE8177354D27D1D5C6AA4041F6291663
                44F4D13AD8B733090748E8450C72CAC24F5F97A8241996C1F7652B9971DB0820
                CCB674BEB575DC7B91FCFF0C24CA204019745006D594414E4541B5C974B63A59
                ADD9DE1F60C667753915ADE6F6CAC1C6EE4728964EE1DA83DB740AE91EC46C67
                0AC467B14E285F553CD52F4CF37CD807C6CAC297F9678CC5E97ADDB67CD0D3DB
                DFD057DB56BAC98999712919A99A901339FA0F122B50EA1C9582110A869D38CF
                2B276DFC586B595D96DFBBCE896B59309698EA0F1B334B125431340BAA7016FC
                340B8BBFDC30BBBC08231F471A2CF55D373665617DC5A724E812B30F99766963
                F2232223B4DEDFDEB9A55957F7CCC07887F31BBF751AB7A8D0BDF4AF863F8BB1
                762158E300DF0000000049454E44AE426082}
            end
            object sbDeleteSpec: TPngSpeedButton
              AlignWithMargins = True
              Left = 33
              Top = 0
              Width = 33
              Height = 27
              Hint = #1059#1076#1072#1083#1080#1090#1100
              Margins.Left = 0
              Margins.Top = 0
              Margins.Right = 0
              Margins.Bottom = 0
              Align = alLeft
              Flat = True
              OnClick = sbDeleteSpecClick
              PngImage.Data = {
                89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
                61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
                6FA864000002824944415478DA6364C002E42495C43322AB767EFCF4960BC417
                E417F9357D799BCBC367775FA0AB654463FF670482EAF4FEA36B762D48BC79FF
                E22D908486A29E5AB05BC2FCD69945D6FF8100A6166E40B8676ABDBE9659F18F
                5FDF5FB3303173F170F30BBF7DFFF21A03232337D8E8FFFFBF0B0B8A6B7CF9FA
                F1ED9F7F7FBF71B0718A5EBC76B27BE5F639CD2003183B4BE67DF70CF1666364
                02A9FDCFF0F7EFDF7FCCCC2C4C40C740F583C4FE00C598C162FFFF31306C5FB3
                F557794F1227D880DC98BAE3CA0A1A664CCC8C7F98189999601EFA0704202613
                10401C0C14FBFFF7DFBFBFFF59EE3EB8796AF292464BB0158C8C4CCC75D9131F
                05C7864AFEFBFFEF1F0BD076A02B18985998C19AFEFE01B2999919FE005DC1C4
                C8C4B476F1EAE74D53F3E5FEFFFFF7171E880D79531E86C486C91EDD7DF48F85
                9305CB897D2718ECBDECC17207B71D64008A3100C5FE58BB5AB3AC59BCEA71C3
                A41C79E458606CCC9BFA28302A58FAE0F683FFACDDAC998EEC3AC2E0E2EF0296
                DCB3710F838D9B0DC3D15D47FFD97BDA33AD5FB6F669FDA46C3970ACA118101D
                227D78C7A17F56AE564C40C50C4E7E4E60C97D9BF631000D6538B6FBD83F5B0F
                3BA6F54BD70C46038081F820282A44F6C0F603A0806202062613721800C5FE01
                C5FE39783AB0AC5BB60614880AA806E44E7E141A1F217D78E7E13FE68E660C27
                F79F62B2F3B2034B1EDA76880128F60F28C660EB6ECBB266D18AE7F593726590
                0D60A84CEF3DE91B1C602A2822008E1B609433B0B042D2C19FDFA034C1044EFD
                EFDFBC67D8B466C3B18E5925362899495642513CC62F6B2D3B0787E2FF7FFFFF
                00D30B139086388F09947CFFFD03D22C3F7EFCB8BF78E3D4C0A72F1FBC46CF8D
                D8722836F01F99030047C14B22634225630000000049454E44AE426082}
            end
            object Bevel8: TBevel
              AlignWithMargins = True
              Left = 134
              Top = 2
              Width = 1
              Height = 23
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Align = alLeft
              ExplicitLeft = 502
              ExplicitHeight = 43
            end
            object sbExcelSpec: TPngSpeedButton
              AlignWithMargins = True
              Left = 99
              Top = 0
              Width = 33
              Height = 27
              Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
              Margins.Left = 0
              Margins.Top = 0
              Margins.Right = 0
              Margins.Bottom = 0
              Align = alLeft
              Flat = True
              ParentShowHint = False
              ShowHint = True
              PngImage.Data = {
                89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
                610000000473424954080808087C086488000000097048597300000B1200000B
                1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
                65776F726B732043533571B5E336000002CC4944415478DA6D934B48545118C7
                FF67346A141B7332F35136A6398E8E332A6A48505A8E2F68D16634451745602F
                93162911F65A0411F82C082A3433CB686764D3C3458B462B17968A1486A8E942
                D42847C77BCEE9DC7BC7C16C2EDC73CFEBFBDDFFF73FDF2158F7D83BAFED5E9C
                9DCF5876BB5297D88A894ACCB4C8DDB503350F3BE1E321F6BB17AA9618B75026
                25AC4892516252B05B5A81442918631063700DCA3ED7B63FF20928BA5DCDA9D8
                4819555E3548F4054086C86BD1C161F321BA2D2E0E0E06C619E5728BA1E91F5A
                52D8728E5331A09ECD2A481E4B2A48F41342A31112B819C7F61F41ABB35B09B6
                46ED8163C439490A5AAAF8BF81E2EB01AA0A284C61BB10B23108259979E81AEC
                052780252216AF46FA26497EB30CA0D06D0A54729AFE358B2C8319A333E3189F
                9B5152326E8B865E1B84E2341B1E0F38E0B7C11FD61DF1022014E4359F5500E6
                F0585465DB71D3D18EDABC72D8EF5DC4E2F2920248100AF45A9D00E4A2E3530F
                888620D5605201B6A6337C55FED5A213B044C6E1D69B0E747F79EF353521CCA0
                28389A9E8FF6FE97C24A08057178F76D6092E4369E564C9437DA8C99A8CE29C1
                E13BE731FB67C17B2289DB63B0354027003601E8111E70240BC5AF473F4E9143
                8DA714133584E07EE925CCFE5EC0E0CFEFA87FDBA102849189E12AA02CBD006D
                FD2F201FA7252A1E8E61E71439D820009C629FC182AC986434F53E455B791D2A
                5AAF60627E46012445C62254004A3D002880383886FAA6484EC349EE2D1E4FCE
                1AA2518ED1E55E56E6CD1131D00706A33CA310AD7D721D70A4EC348A3AF83041
                B2EB2BD700642FE87F9568167F931554EC2DC4035148B2072991F1E819764E90
                0302A0160FF5E6BC5A50F23D90FB067DF89C4E1BE4E29C2BF2A968840FFCEBF4
                5800C9BA71BC9230C942FD4822A3CC24C243A8585E5B89F0F72B1EADEB7AE2F3
                32AD9FB05E2EDEC556581AD54856892349E46B9608A919BBFEFC992FC05FFC64
                D8FDA3D5A84C0000000049454E44AE426082}
            end
            object sbEditSpec: TPngSpeedButton
              AlignWithMargins = True
              Left = 66
              Top = 0
              Width = 33
              Height = 27
              Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
              Margins.Left = 0
              Margins.Top = 0
              Margins.Right = 0
              Margins.Bottom = 0
              Align = alLeft
              Flat = True
              Layout = blGlyphTop
              ParentShowHint = False
              ShowHint = True
              OnClick = sbEditSpecClick
              PngImage.Data = {
                89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
                610000001974455874536F6674776172650041646F626520496D616765526561
                647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
                62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
                08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
                4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
                C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
                892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
                4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
                0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
                C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
                8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
                346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
                5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
                3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
                378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
                C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
                1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
                FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
                F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
                0049454E44AE426082}
            end
          end
        end
        object Прочее: TTabSheet
          Caption = #1055#1088#1086#1095#1077#1077
          ImageIndex = 1
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 1088
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            Color = 15921906
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 28637
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            TabOrder = 0
            StyleElements = [seBorder]
            object sbAddSpec1: TPngSpeedButton
              AlignWithMargins = True
              Left = 0
              Top = 0
              Width = 32
              Height = 27
              Hint = #1044#1086#1073#1072#1074#1080#1090#1100
              Margins.Left = 0
              Margins.Top = 0
              Margins.Right = 0
              Margins.Bottom = 0
              Align = alLeft
              Layout = blGlyphTop
              ParentShowHint = False
              ShowHint = True
              OnClick = sbAddSpec1Click
              PngImage.Data = {
                89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
                610000000473424954080808087C086488000000097048597300000B1200000B
                1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
                65776F726B732043533571B5E336000002114944415478DA8D93CB4F135114C6
                BF33337D49A48D89AE5CF917D89DD4477C048B6B11790C2DE84277FE256ABA22
                6A5163BAD0A87B25C698311A6A03F5454B35A12ED45625406941B0A5E3D7B960
                54BA989B99B933F79CFB3BE77CE78EA0CDE83A3B9610D1C21041EB1635675FA4
                CE5FFADF57DA0122FDD7ADF8B9E83F6BA9D4533CBF337AC41D603069C547BAF1
                3EF703B5B506BC9D01E45FBD81753BE60E70C81CB786E3DD7899FE8CCA4A1DFE
                5D1DF89A2FC0BA39E40E703876CB32874F6032F3054B2B0DF8082813F02C39B0
                1DA004D3C3D0345E148C33341DA67914E9A9122AAB048476A094FF406F4D4594
                ADB8F24422FDD728588FF36943944D945366BA8CCACF06F6EC0D62FFBEE05F1B
                D5B87B2F0D890C50F1D128DE51B06A751DA23313DEAD4C6ADC6CD3D1F019E8EC
                F0FE69A918F4E15A21334BC0E00D2B367212934C7771797DB304C106776EF0A1
                E93A0CBF078647834E9BE89B657A75CC178A9083434947F1ECC7252CD6EA58AB
                37D160D4DD213F96175661530F5F28E00034BE3B3AD1DEB46D9467E60830C713
                9AA6858546380E2AFDDEBE08326F790EEA367CC100BEE78B2AF256194A8672DB
                361EBB70DFEA3DD385A99979D47ED94E17BEE5E63071F594BB7370BC05E85300
                9E23D546A63B71A5C725E0E203667000D3B30B4E09FE9D019472453CBE1C750F
                384DC0EB62952534E1F11AD4E0131EB9073C4C50B0704B50B08DA2C4CD12B0ED
                77FE0D6936C4A0656B515C0000000049454E44AE426082}
              ExplicitLeft = 1
              ExplicitTop = 1
              ExplicitHeight = 25
            end
            object sbDeleteSpec1: TPngSpeedButton
              AlignWithMargins = True
              Left = 32
              Top = 0
              Width = 35
              Height = 27
              Hint = #1059#1076#1072#1083#1080#1090#1100
              Margins.Left = 0
              Margins.Top = 0
              Margins.Right = 0
              Margins.Bottom = 0
              Align = alLeft
              Flat = True
              OnClick = sbDeleteSpecClick
              PngImage.Data = {
                89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
                610000001974455874536F6674776172650041646F626520496D616765526561
                647971C9653C000002554944415478DA95924D68D36018C79F7C746D92A6B5DD
                86B2B56C7810A141F0E8459C7810BC09A2CECFA907F13211C7BC7A134410069E
                046FC26EE2556D657EEC24F5D04C2F62B77569BA43ED47DA244BF2C6E74D6CB1
                743BEC8524BCE4FDFD9E27FF278CEFFBB09F65CFDD11A3AF5E767B7B6640B0F0
                680AEF7178FA44DD15BE793BE7BBCE19E2B84571F9F5CAA080C21C57A67BDFB6
                15F6F9B301897DE3560EDF9798E808ECD4B69751F4587EFBE64728F80743340A
                54E7B7DB402C4BE15F2C0512EBFA5C8E61D9129790814F26C1DEAC80A5EBF789
                E32C8582878B319F900E0802CBC83290560B48A3811253C17681C22CC2914412
                ACAD0AD855DD435848E5DF39FD4F20F30F62BEE7198C20702C1EF49A0D70FFD4
                C1470187D248320156658B56F6B0FD782AFFDE1A0AD1BD7B2F465CCF604591E3
                5307003C82667A795899C2B5102E7CB0769F4218164ADC8E3035CDC2C63A4A3C
                80CC24348ADF09711D29FD316FFD7F7E4860CE5EC3C098526C3203502E871DA0
                A0B9B6064EDB50C6BE7E52F7149897AFE600615E94303019613F689F4AAC5A0D
                4C5D879D765B3958FCA60E09BA97AE04954338016655A3104D9B89A5D2AC3471
                08BA551DBA9A86124399F8A9AA7D41F7E26C30675E14118E83A9E9606D53D88D
                6368804F43184D737236031DAD0A1D0C947692F9FD4B0D04C6F90B336C84CF47
                24095CA30326C238BEF8E897952030FDD8711CB16B48E3E31C3D63D089D4EBA7
                B31BE542FF139A67CFA12492775A4D8215250C6B206DEDC8D1E03F199165AE07
                0F85583F796A06E1D5B1D5CF03706F55A60F53C989ECE67A61CF31EE77FD05EB
                B7706A5FA737EA0000000049454E44AE426082}
              ExplicitLeft = 33
              ExplicitTop = 1
              ExplicitHeight = 25
            end
            object Bevel1: TBevel
              AlignWithMargins = True
              Left = 132
              Top = 2
              Width = 1
              Height = 23
              Margins.Left = 2
              Margins.Top = 2
              Margins.Right = 2
              Margins.Bottom = 2
              Align = alLeft
              ExplicitLeft = 502
              ExplicitHeight = 43
            end
            object sbExcel1: TPngSpeedButton
              AlignWithMargins = True
              Left = 98
              Top = 0
              Width = 32
              Height = 25
              Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
              Margins.Left = 0
              Margins.Top = 0
              Margins.Right = 0
              Margins.Bottom = 2
              Align = alLeft
              Flat = True
              ParentShowHint = False
              ShowHint = True
              PngImage.Data = {
                89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
                610000001974455874536F6674776172650041646F626520496D616765526561
                647971C9653C0000031E4944415478DA8D936B6C0C5114C7FF7776B7BB53CF6E
                29F508A11E4D89E8AA846AB32869D89520A1691B1111AFB6D2342A21AA443C2A
                F1F8209E1104ED1791F824E852DD967835969616F5A6655955DD99E9ECEC5C67
                6737F5D5C9993B7327F7FCCEB9FF7B2EE39C63C1CEFAD5E2406B81C0E00060A3
                8701B191D3C07568BADEDEF5937B58F7AF72EF09978A98B10860D1BE46EFC512
                47AA20B0C11C3081A3CFB8F1CDE1EF09A1FA71173A3FF64A2F9A5BED0D275CBD
                7D0057D5BDEF359B33EC2DDF61121833325352842399350E39A4C139A11F6ADB
                653C68F5A3E3A32ABF6A6B4BB87B6C71AF01701FBC2F5D2E76886F02268AE546
                013A0D615D874A00450D237B820D2D7E0E8DA0B77C9DB8ED6DAFF1EC73E6B3BC
                43F3DFEADC3E7670BCCEC2B4DFF4143702C11FB8E3BB6080AC96FE04D2B0383D
                1F85D9EB696E86A4A8283CF244BBB93BCB12015C59B7B06CB96CEDC0FB402B9A
                9EBC27C1240C4D48841AD620493274D8B16CF67E30533C74AA2A6B9C152B0E3F
                566E54648A118035A44F09962F596A7AADD6A323E0475B7307C24C8699C5C16C
                4A42C1BC43548D19210A26C78C5166AC3C42801DB3C4E8295479E5C949E76C05
                D96BF1E8E73574FD09C3E77B0A5B9C1DC5EEF3246A1C42B44E0B933EF44E1B2E
                208F2AB8591103382B77A98A7ADE32313915A5AE4A5CFF7411BA64C5CBD6E7C8
                759460DAF845241E10D2A202A724C200782A09E0281FB18E529CCA4C73222005
                C0551D65EE5DB8D4761C66251E9FDEB5E375E74B9C2EF94027C3490F20350970
                EF7DA8D4ED996D003ED3BF91916689F898A43424278C465E663EEE7EF1E06BD7
                37A87E091B5C97E934B8D198D39205E4EC68501AAAB2A35B985BD128576F71D8
                FC3D16A3FB7E07BFE16AE31E04955F604CC0003111AB728EF67567CA5060EE56
                AFF2E0A8330AC8DA562FD794CFB405640BFEC7C627524C699DD27432270A9855
                5617ACD93E53ECEEB5B2FF014C1AC6F48C8DB5D2B3330B071880199B3C6FAF1F
                98332418E2FD682E44B4D0638B0789E6BE4B255A04DA1274314EF83375CD8D2F
                2DE772D30C4046516D9116E24B1963D349A6F87F57D1F0D8ED88CEC914EAC626
                01A8F69DCD3DFB17534092F0545E8BC50000000049454E44AE426082}
              ExplicitLeft = 95
              ExplicitTop = 2
            end
            object sbEditSpec1: TPngSpeedButton
              AlignWithMargins = True
              Left = 67
              Top = 0
              Width = 31
              Height = 27
              Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
              Margins.Left = 0
              Margins.Top = 0
              Margins.Right = 0
              Margins.Bottom = 0
              Align = alLeft
              Flat = True
              Layout = blGlyphTop
              ParentShowHint = False
              ShowHint = True
              OnClick = sbEditSpec1Click
              PngImage.Data = {
                89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
                610000001974455874536F6674776172650041646F626520496D616765526561
                647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
                62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
                08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
                4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
                C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
                892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
                4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
                0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
                C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
                8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
                346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
                5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
                3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
                378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
                C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
                1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
                FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
                F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
                0049454E44AE426082}
              ExplicitLeft = 75
            end
          end
          object dgSpec1: TDBGridEh
            Left = 0
            Top = 27
            Width = 1088
            Height = 313
            Align = alClient
            AllowedOperations = []
            Border.Color = 11645361
            ColumnDefValues.Title.TitleButton = True
            Ctl3D = False
            DataSource = dsSpec
            DynProps = <>
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Calibri'
            Font.Style = []
            GridLineParams.BrightColor = 15395041
            GridLineParams.DarkColor = 15395041
            GridLineParams.DataBoundaryColor = 15395041
            GridLineParams.DataHorzColor = clSilver
            GridLineParams.DataVertColor = clSilver
            GridLineParams.VertEmptySpaceStyle = dessNonEh
            IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
            IndicatorParams.HorzLineColor = clSilver
            IndicatorParams.VertLineColor = clSilver
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            SelectionDrawParams.SelectionStyle = gsdsClassicEh
            SelectionDrawParams.DrawFocusFrame = False
            SelectionDrawParams.DrawFocusFrameStored = True
            ShowHint = True
            SortLocal = True
            STFilter.Color = 15725813
            STFilter.Font.Charset = DEFAULT_CHARSET
            STFilter.Font.Color = clWindowText
            STFilter.Font.Height = -11
            STFilter.Font.Name = 'Verdana'
            STFilter.Font.Style = []
            STFilter.HorzLineColor = clSilver
            STFilter.Local = True
            STFilter.ParentFont = False
            STFilter.VertLineColor = clSilver
            STFilter.Visible = True
            TabOrder = 1
            TitleParams.Color = clBtnFace
            TitleParams.FillStyle = cfstThemedEh
            TitleParams.Font.Charset = DEFAULT_CHARSET
            TitleParams.Font.Color = clWindowText
            TitleParams.Font.Height = -13
            TitleParams.Font.Name = 'Calibri'
            TitleParams.Font.Style = [fsBold]
            TitleParams.HorzLineColor = 11645361
            TitleParams.MultiTitle = True
            TitleParams.ParentFont = False
            TitleParams.SecondColor = 15987699
            TitleParams.SortMarkerStyle = smstDefaultEh
            TitleParams.VertLineColor = 13421772
            Columns = <
              item
                CellButtons = <>
                DynProps = <>
                EditButtons = <>
                FieldName = 'passoptype_text'
                Footers = <>
                Title.Caption = #1054#1087#1077#1088#1072#1094#1080#1103
                Width = 164
              end
              item
                CellButtons = <>
                DynProps = <>
                EditButtons = <>
                FieldName = 'note'
                Footers = <>
                Title.Caption = #1042#1080#1076' '#1075#1088#1091#1079#1072', '#1087#1088#1080#1084#1077#1095#1072#1085#1080#1077
                Width = 837
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
        end
      end
    end
    object plStatus: TPanel
      Left = 0
      Top = 29
      Width = 1098
      Height = 20
      Align = alTop
      BevelOuter = bvNone
      Color = 15921906
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 16318464
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 4
      StyleElements = [seClient, seBorder]
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'declare @guid varchar(50), @id int;'
      ''
      'select @guid = :guid, @id = :id;'
      ''
      'select *, '
      
        '(case when pass_type = 0 then '#39#1055#1088#1080#1077#1084'/'#1074#1099#1076#1072#1095#1072' '#1050#1058#1050#39' when pass_type=' +
        '1 then '#39#1055#1088#1086#1095#1077#1077#39' end) as pass_type_text,'
      
        '(select id from doctypes dt where dt.code = '#39'pass'#39'+convert(varch' +
        'ar(1),pass_type)) as doctype_id,'
      
        '(select code from counteragents ct where ct.id = p.payer_id) as ' +
        'payer_code,'
      
        '(select code from counteragents ct where ct.id = p.dealer_id) as' +
        ' dealer_code,'
      'isnull(p.car_data,'#39#39')+'#39' '#39'+isnull(p.car_number,'#39#39') as car_full,'
      
        '(select person_name from persons pr where pr.id = p.driver_id) a' +
        's driver_name,'
      
        'convert(varchar(10),onedaypass_term_start,104)+'#39', '#39'+(case when o' +
        'nedaypass_paytype = 0 then '#39#1053#1072#1083#1080#1095#1085#1099#1081#39' when onedaypass_paytype = ' +
        '1 then '#39#1041#1077#1079#1085#1072#1083#1080#1095#1085#1099#1081#39' when onedaypass_paytype = 2 then '#39#1044#1077#1087#1086#1079#1080#1090#39' ' +
        'else '#39'???'#39'end) as onedaypass_text'
      'from passes p '
      'where '
      '('
      '    p.pass_date >= :datestart'
      '    and ('
      
        '      not exists (select 1 from dbo.f_GetFilterListExact(0, @gui' +
        'd) fl1)'
      '      or exists ('
      
        '             select 1 from passoperations po where (po.container' +
        '_num in (select v from dbo.f_GetFilterListExact(0, @guid)) or po' +
        '.container_digits in (select v from dbo.f_GetFilterListExact(0, ' +
        '@guid))  )'
      '             and po.parent_id = p.id'
      '    ) or exists ('
      
        '             select 1 from passoperations po, containers c where' +
        ' c.id = po.container_id'
      
        '             and c.cnum in (select v from dbo.f_GetFilterListExa' +
        'ct(0, @guid))'
      '             and po.parent_id = p.id'
      '   )'
      ') or @id>0)'
      'and (isnull(@id,0) = 0 or p.id = @id)'
      'order by pass_date desc')
    SelectCommand.Parameters = <
      item
        Name = 'guid'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'datestart'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update passes'
      'set'
      '  isabolished = :isabolished, '
      '  isprinted = :isprinted, '
      '  pass_date = :pass_date,'
      '  pass_number = :pass_number,'
      '  car_data = :car_data,'
      '  car_number = :car_number,'
      '  payer_id = :payer_id,'
      '  dealer_id = :dealer_id,'
      '  driver_id = :driver_id,'
      '  attorney = :attorney,'
      '  onedaypass_term_start = :onedaypass_term_start,'
      '  onedaypass_term_end = :onedaypass_term_end,'
      '  onedaypass_paytype = :onedaypass_paytype,'
      '  user_id = :user_id,'
      '  date_created = :date_created'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'isabolished'
        Size = -1
        Value = Null
      end
      item
        Name = 'isprinted'
        Size = -1
        Value = Null
      end
      item
        Name = 'pass_date'
        Size = -1
        Value = Null
      end
      item
        Name = 'pass_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_data'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'payer_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'dealer_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'driver_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'attorney'
        Size = -1
        Value = Null
      end
      item
        Name = 'onedaypass_term_start'
        Size = -1
        Value = Null
      end
      item
        Name = 'onedaypass_term_end'
        Size = -1
        Value = Null
      end
      item
        Name = 'onedaypass_paytype'
        Size = -1
        Value = Null
      end
      item
        Name = 'user_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'date_created'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into passes'
      
        '  (pass_type, pass_date, pass_number, car_data, car_number, paye' +
        'r_id, dealer_id, driver_id, attorney, onedaypass_term_start, one' +
        'daypass_term_end, onedaypass_paytype, user_id, date_created)'
      'values'
      
        '  (:pass_type, :pass_date, :pass_number, :car_data, :car_number,' +
        ' :payer_id, :dealer_id, :driver_id, :attorney, :onedaypass_term_' +
        'start, :onedaypass_term_end, :onedaypass_paytype, :user_id, :dat' +
        'e_created)')
    InsertCommand.Parameters = <
      item
        Name = 'pass_type'
        Size = -1
        Value = Null
      end
      item
        Name = 'pass_date'
        Size = -1
        Value = Null
      end
      item
        Name = 'pass_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_data'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'payer_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'dealer_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'driver_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'attorney'
        Size = -1
        Value = Null
      end
      item
        Name = 'onedaypass_term_start'
        Size = -1
        Value = Null
      end
      item
        Name = 'onedaypass_term_end'
        Size = -1
        Value = Null
      end
      item
        Name = 'onedaypass_paytype'
        Size = -1
        Value = Null
      end
      item
        Name = 'user_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'date_created'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from passes where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select *, '
      
        '(case when pass_type = 0 then '#39#1055#1088#1080#1077#1084'/'#1074#1099#1076#1072#1095#1072' '#1050#1058#1050#39' when pass_type=' +
        '1 then '#39#1055#1088#1086#1095#1077#1077#39' end) as pass_type_text,'
      
        '(select id from doctypes dt where dt.code = '#39'pass'#39'+convert(varch' +
        'ar(1),pass_type)) as doctype_id,'
      
        '(select code from counteragents ct where ct.id = p.payer_id) as ' +
        'payer_code,'
      
        '(select code from counteragents ct where ct.id = p.dealer_id) as' +
        ' dealer_code,'
      'isnull(p.car_data,'#39#39')+'#39' '#39'+isnull(p.car_number,'#39#39') as car_full,'
      
        '(select person_name from persons pr where pr.id = p.driver_id) a' +
        's driver_name,'
      
        'convert(varchar(10),onedaypass_term_start,104)+'#39', '#39'+(case when o' +
        'nedaypass_paytype = 0 then '#39#1053#1072#1083#1080#1095#1085#1099#1081#39' when onedaypass_paytype = ' +
        '1 then '#39#1041#1077#1079#1085#1072#1083#1080#1095#1085#1099#1081#39' when onedaypass_paytype = 2 then '#39#1044#1077#1087#1086#1079#1080#1090#39' ' +
        'else '#39'???'#39'end) as onedaypass_text'
      'from passes p where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited exReport: TEXLReport
    DataSet = qrReport
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'declare @parent_id int;'
      ''
      'select @parent_id = :parent_id;'
      ''
      'select s1.*, isnull(doctext0,doctext1) as doctext from '
      '('
      
        'select o.*,  0 as isparent, 0 as ischild, isnull(c.cnum, o.conta' +
        'iner_num) as cnum,'
      
        '(case when passoperation_type = 0 then '#39#1055#1088#1080#1077#1084#39' when passoperatio' +
        'n_type = 1 then '#39#1042#1099#1076#1072#1095#1072#39' else '#39'???'#39' end) as passoptype_text,'
      
        '(select code from containerkinds ck where ck.id = isnull(c.kind_' +
        'id,o.container_kind_id)) as cont_kind_code,'
      'ok.direction, ok.code as op_kind_code, ok.end_doctype_id,'
      
        '(select d.doc_number+'#39' '#1086#1090' '#39'+format(d.doc_date, '#39'dd.MM.yyyy'#39') fro' +
        'm documents d, objects2docspec ods where ods.doc_id = d.id and o' +
        'ds.object_id = o.container_id and ods.task_id = o.task_id and d.' +
        'doctype_id = ok.end_doctype_id) as doctext0,'
      
        '(select d.doc_number+'#39' '#1086#1090' '#39'+format(d.doc_date, '#39'dd.MM.yyyy'#39') fro' +
        'm documents d where id = o.childdoc_id) as doctext1,'
      
        '(case when isnull(state,0) = 0 then '#39#39' when isnull(state,0) = 1 ' +
        'then '#39#1047#1072#1074#1077#1088#1096#1077#1085#1086#39' when isnull(state,0) = 2 then '#39#1055#1088#1080#1089#1090#1072#1085#1086#1074#1083#1077#1085#1086#39' w' +
        'hen isnull(state,0) = 3 then '#39#1040#1085#1085#1091#1083#1080#1088#1086#1074#1072#1085#1086#39' end) as state_code'
      'from passoperations o'
      
        'left outer join passoperationkinds ok on (ok.id = o.passoperatio' +
        'n_id)'
      'left outer join containers c on (c.id =o.container_id)  '
      'where o.parent_id = @parent_id'
      ') s1 order by higher_id, direction')
    SelectCommand.Parameters = <
      item
        Name = 'parent_id'
        DataType = ftInteger
        Size = -1
        Value = -1
      end>
    UpdateCommand.CommandText.Strings = (
      'update passoperations'
      'set'
      '  parent_id = :parent_id,'
      '  passoperation_type = :passoperation_type,'
      '  passoperation_id = :passoperation_id,'
      '  note = :note,'
      '  container_id = :container_id,'
      '  seal_number = :seal_number,'
      '  isempty = :isempty,'
      '  task_id = :task_id,'
      '  state = :state,'
      '  container_num = :container_num, '
      '  container_kind_id = :container_kind_id, '
      '  container_owner_id = :container_owner_id'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'parent_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'passoperation_type'
        Size = -1
        Value = Null
      end
      item
        Name = 'passoperation_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'note'
        Size = -1
        Value = Null
      end
      item
        Name = 'container_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'seal_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'isempty'
        Size = -1
        Value = Null
      end
      item
        Name = 'task_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'state'
        Size = -1
        Value = Null
      end
      item
        Name = 'container_num'
        Size = -1
        Value = Null
      end
      item
        Name = 'container_kind_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'container_owner_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      
        'declare @parent_id int, @isparent int, @ischild int, @container_' +
        'id int, @higher_id int;'
      ''
      
        'select @parent_id = :parent_id, @isparent = :isparent, @ischild ' +
        '= :ischild, @container_id = :container_id;'
      ''
      'if @ischild =1'
      
        'select @higher_id  = id from passoperations po where po.parent_i' +
        'd = @parent_id and container_id = @container_id;'
      ''
      'insert into passoperations'
      
        '  (parent_id, passoperation_type, passoperation_id, note, contai' +
        'ner_id, seal_number, isempty, task_id, state, container_num, con' +
        'tainer_kind_id, container_owner_id, higher_id)'
      'values'
      
        '  (@parent_id, :passoperation_type, :passoperation_id, :note, @c' +
        'ontainer_id, :seal_number, :isempty, :task_id, :state, :containe' +
        'r_num, :container_kind_id, :container_owner_id, @higher_id)'
      '')
    InsertCommand.Parameters = <
      item
        Name = 'parent_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'isparent'
        Size = -1
        Value = Null
      end
      item
        Name = 'ischild'
        Size = -1
        Value = Null
      end
      item
        Name = 'container_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'passoperation_type'
        Size = -1
        Value = Null
      end
      item
        Name = 'passoperation_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'note'
        Size = -1
        Value = Null
      end
      item
        Name = 'seal_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'isempty'
        Size = -1
        Value = Null
      end
      item
        Name = 'task_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'state'
        Size = -1
        Value = Null
      end
      item
        Name = 'container_num'
        Size = -1
        Value = Null
      end
      item
        Name = 'container_kind_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'container_owner_id'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from passoperations where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'declare @parent_id int;'
      ''
      'select @parent_id = :parent_id;'
      ''
      'select s1.*, isnull(doctext0,doctext1) as doctext from '
      '('
      
        'select o.*,  0 as isparent, 0 as ischild, isnull(c.cnum, o.conta' +
        'iner_num) as cnum,'
      
        '(case when passoperation_type = 0 then '#39#1055#1088#1080#1077#1084#39' when passoperatio' +
        'n_type = 1 then '#39#1042#1099#1076#1072#1095#1072#39' else '#39'???'#39' end) as passoptype_text,'
      
        '(select code from containerkinds ck where ck.id = isnull(c.kind_' +
        'id,o.container_kind_id)) as cont_kind_code,'
      'ok.direction, ok.code as op_kind_code, ok.end_doctype_id,'
      
        '(select d.doc_number+'#39' '#1086#1090' '#39'+format(d.doc_date, '#39'dd.MM.yyyy'#39') fro' +
        'm documents d, objects2docspec ods where ods.doc_id = d.id and o' +
        'ds.object_id = o.container_id and ods.task_id = o.task_id and d.' +
        'doctype_id = ok.end_doctype_id) as doctext0,'
      
        '(select d.doc_number+'#39' '#1086#1090' '#39'+format(d.doc_date, '#39'dd.MM.yyyy'#39') fro' +
        'm documents d where id = o.childdoc_id) as doctext1,'
      
        '(case when isnull(state,0) = 0 then '#39#39' when isnull(state,0) = 1 ' +
        'then '#39#1047#1072#1074#1077#1088#1096#1077#1085#1086#39' when isnull(state,0) = 2 then '#39#1055#1088#1080#1089#1090#1072#1085#1086#1074#1083#1077#1085#1086#39' w' +
        'hen isnull(state,0) = 3 then '#39#1040#1085#1085#1091#1083#1080#1088#1086#1074#1072#1085#1086#39' end) as state_code'
      'from passoperations o'
      
        'left outer join passoperationkinds ok on (ok.id = o.passoperatio' +
        'n_id)'
      'left outer join containers c on (c.id =o.container_id)  '
      'where o.id = :current_id'
      ') s1 order by higher_id, direction')
    GetrecCommand.Parameters = <
      item
        Name = 'parent_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 240
    Top = 376
  end
  object meSpec: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvSpec
    TreeList.Active = True
    TreeList.KeyFieldName = 'id'
    TreeList.RefParentFieldName = 'higher_id'
    TreeList.DefaultNodeExpanded = True
    BeforeOpen = meSpecBeforeOpen
    BeforePost = meSpecBeforePost
    Left = 288
    Top = 376
  end
  object dsSpec: TDataSource
    DataSet = meSpec
    Left = 344
    Top = 376
  end
  object qrReport: TADOQuery
    Connection = dm.connMain
    Parameters = <>
    Left = 696
    Top = 192
  end
end
