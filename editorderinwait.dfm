﻿inherited FormEditOrderInWait: TFormEditOrderInWait
  Caption = #1057#1086#1075#1083#1072#1089#1086#1074#1072#1085#1080#1077
  ClientHeight = 131
  ClientWidth = 470
  ExplicitWidth = 476
  ExplicitHeight = 159
  PixelsPerInch = 96
  TextHeight = 16
  object Label5: TLabel [0]
    Left = 16
    Top = 52
    Width = 132
    Height = 16
    Caption = #1044#1072#1090#1072' '#1089#1086#1075#1083#1072#1089#1086#1074#1072#1085#1080#1103
  end
  inherited plBottom: TPanel
    Top = 90
    Width = 470
    TabOrder = 2
    inherited btnCancel: TButton
      Left = 354
    end
    inherited btnOk: TButton
      Left = 235
    end
  end
  object cbInWait: TDBCheckBoxEh [2]
    Left = 16
    Top = 16
    Width = 308
    Height = 17
    Caption = #1053#1072' '#1089#1086#1075#1083#1072#1089#1086#1074#1072#1085#1080#1080
    DataField = 'inwait'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 0
    OnClick = cbInWaitClick
  end
  object dtInWaitStop: TDBDateTimeEditEh [3]
    Left = 154
    Top = 50
    Width = 179
    Height = 22
    DataField = 'datestopinwait'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 1
    Visible = True
    OnChange = dtInWaitStopChange
  end
  inherited dsLocal: TDataSource
    Left = 80
    Top = 88
  end
  inherited qrAux: TADOQuery
    Left = 24
    Top = 88
  end
end
