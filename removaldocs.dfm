﻿inherited FormRemovalDocs: TFormRemovalDocs
  ActiveControl = dgData
  Caption = #1044#1086#1089#1090#1072#1074#1082#1072' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074
  ClientWidth = 901
  ExplicitWidth = 917
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Width = 901
    ExplicitWidth = 901
    inherited btnOk: TButton
      Left = 666
      ExplicitLeft = 666
    end
    inherited btnCancel: TButton
      Left = 785
      ExplicitLeft = 785
    end
  end
  inherited plAll: TPanel
    Width = 701
    ExplicitWidth = 701
    inherited SplitterHor: TSplitter
      Top = 450
      Width = 701
      ExplicitTop = 396
      ExplicitWidth = 706
    end
    inherited plTop: TPanel
      Width = 701
      ExplicitWidth = 701
      inherited sbDelete: TPngSpeedButton
        ExplicitLeft = 71
      end
      inherited btFilter: TPngSpeedButton
        Left = 166
        ExplicitLeft = 180
        ExplicitTop = -2
      end
      inherited btExcel: TPngSpeedButton
        Left = 543
        ExplicitLeft = 645
        ExplicitTop = -3
        ExplicitHeight = 25
      end
      inherited sbAdd: TPngSpeedButton
        Visible = False
        ExplicitLeft = 40
      end
      inherited sbEdit: TPngSpeedButton
        ExplicitLeft = 106
      end
      inherited Bevel2: TBevel
        Left = 131
        Margins.Left = 3
        ExplicitLeft = 141
        ExplicitTop = -1
        ExplicitHeight = 22
      end
      inherited btTool: TPngSpeedButton
        Left = 503
        ExplicitLeft = 666
      end
      inherited Bevel1: TBevel
        Left = 264
        ExplicitLeft = 297
      end
      inherited btFlow: TPngSpeedButton
        Left = 368
        ExplicitLeft = 403
      end
      inherited sbHistory: TPngSpeedButton
        Left = 400
        ExplicitLeft = 442
      end
      inherited sbRestrictions: TPngSpeedButton
        Left = 469
        ExplicitLeft = 548
      end
      inherited sbConfirm: TPngSpeedButton
        Left = 267
        ExplicitLeft = 311
      end
      inherited sbCancelConfirm: TPngSpeedButton
        Left = 299
        ExplicitLeft = 350
      end
      inherited Bevel5: TBevel
        Left = 503
        ExplicitLeft = 594
      end
      inherited sbReport: TPngSpeedButton
        Left = 506
        ExplicitLeft = 601
      end
      inherited Bevel6: TBevel
        Left = 365
        ExplicitLeft = 389
      end
      inherited sbImport2: TPngSpeedButton
        AlignWithMargins = False
        Visible = False
      end
      inherited sbSearch: TPngSpeedButton
        Left = 230
        ExplicitLeft = 196
      end
      inherited Bevel7: TBevel
        Left = 434
        ExplicitLeft = 578
      end
      inherited sbClearFilter: TPngSpeedButton
        Left = 198
        ExplicitLeft = 217
        ExplicitTop = -2
      end
      inherited Bevel3: TBevel
        Left = 540
        ExplicitLeft = 585
      end
      inherited sbClock: TPngSpeedButton
        Left = 575
        ExplicitLeft = 629
      end
      inherited sbBarrel: TPngSpeedButton
        Left = 331
        ExplicitLeft = 348
      end
      object sbCheck: TPngSpeedButton [23]
        AlignWithMargins = True
        Left = 437
        Top = 0
        Width = 32
        Height = 24
        Hint = #1055#1088#1086#1074#1077#1088#1082#1072'/'#1074#1099#1075#1088#1091#1079#1082#1072
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        Layout = blGlyphRight
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = N15Click
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C0864880000001974455874536F6674776172
          65007777772E696E6B73636170652E6F72679BEE3C1A0000023C4944415478DA
          63FCFFFF3F03258091140318656AD8809431109FF8FFA4E53F49060035F36E5E
          14F7C8C95A4960DEF25357726BB6E9030DF9073700A840425B4D2CFDEAAD57D3
          8112AFD035AF9D1BF5C0CE524908A4FA1F90A86EDB7662EED273366003800A64
          964D0FBFA5A420C479EBCEEBEF7139CB55FF3FED7C0AD5CC37A73FE88EB999A2
          E87F46460606262686C5275F30FC07D2F3AAE6D6303248574B77D4B8DD909215
          E0F9F7F73F032B0B13C3AB979FBF15D66C36656064FED459E771DAC84C49E21F
          50030B0B33C3B66BEF191E7CFDCFA0C6F6FBFF84C2CA4246559BBEE91F59FF65
          F8DAAB31C84BF133B0B2B1317070B231B0313331802CE4E2E76160626561F8C7
          C4CC70F6D93786E73F1819C4BE7FFEBFBCBD63CAFB876B5A187955AB2C553465
          0E5F7AF696D9544F9EC1485B8A8197978B818D838D819D8395811968E0D3CFBF
          199E006D65E5E264607FF5EAFFA609BD933F3D59DF0AF4FE2B7018B04BA5F82B
          E8E9AF79CFCAC2C2C3C7C32026C6C72028C8C3C0CAC9C1F09B850548733270F2
          7032FCBEFFF4FF8E697D13BEBFD8DC01D28C128D5C32A9017AF656AB3FF2F0B0
          70F2723370F1703170F282347231700169E6272FFE6F9EDC3DE1F3934D70CD18
          E98057213DD031C863F57B3E41664E3E2EB866FEF7EFFEAFEAEA98F8FEE1BA36
          A0FAD77853A2B05A76507C7EF4EA7BAC7C4C2043D4597EFF9F51593BE9E59DD5
          2D40B56F884ACA327A8521858DA92B8161C0D859503DF9D9CD15CDD834E34DCA
          8C02AEBA0CBFDF68337CBBB00797669233134D0C00005658FCA41106A6E80000
          000049454E44AE426082}
        ExplicitLeft = 405
        ExplicitHeight = 26
      end
      inherited sbPass: TPngSpeedButton
        Left = 134
        Visible = True
        ExplicitLeft = 134
      end
      inherited plCount: TPanel
        Left = 540
        ExplicitLeft = 540
        inherited edCount: TDBEditEh
          ControlLabel.ExplicitLeft = 0
          ControlLabel.ExplicitTop = -16
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 701
      Height = 387
      ReadOnly = False
      RowHeight = 34
      STFilter.RowHeight = 24
      TitleParams.MultiTitle = True
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 48
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'delivery_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
          Width = 132
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          Color = 16623272
          DynProps = <>
          EditButtons = <>
          FieldName = 'pass_number'
          Footers = <>
          Title.Caption = #1055#1088#1086#1087#1091#1089#1082
          Width = 54
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'restr_sign'
          Footers = <>
          Title.Caption = #1054#1075#1088'.'
          Width = 47
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'letter_sign'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084'.'
          Width = 47
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          TextEditing = False
          Title.Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082'.'
          Width = 74
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          TextEditing = False
          Title.Caption = #1044#1072#1090#1072' '#1076#1086#1082'.'
          Width = 97
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'docexists'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1057#1086#1079'. '#1076#1086#1082'?'
          Width = 53
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'recept_datetime'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103': '#1087#1088#1080#1077#1084' '#1075#1088#1091#1079#1072
          Width = 105
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'recept_org_name'
          Footers = <>
          Title.Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090': '#1087#1088#1080#1077#1084' '#1075#1088#1091#1079#1072
          Width = 157
          WordWrap = True
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'recept_address'
          Footers = <>
          TextEditing = False
          Title.Caption = #1040#1076#1088#1077#1089': '#1087#1088#1080#1077#1084' '#1075#1088#1091#1079#1072
          Width = 138
          WordWrap = True
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'delivery_datetime'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103': '#1089#1076#1072#1095#1072' '#1075#1088#1091#1079#1072
          Width = 104
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'delivery_org_name'
          Footers = <>
          Title.Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090': '#1089#1076#1072#1095#1072' '#1075#1088#1091#1079#1072
          Width = 156
          WordWrap = True
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'delivery_address'
          Footers = <>
          TextEditing = False
          Title.Caption = #1040#1076#1088#1077#1089': '#1089#1076#1072#1095#1072' '#1075#1088#1091#1079#1072
          Width = 157
          WordWrap = True
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'extra_place_sign'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1047#1072#1077#1079#1076' '#1085#1072' '#1076#1086#1087'. '#1084#1077#1089#1090#1086
          Width = 66
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_full_data'
          Footers = <>
          Title.Caption = #1040#1074#1090#1086#1084#1086#1073#1080#1083#1100', '#1074#1086#1076#1080#1090#1077#1083#1100
          Width = 114
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dealer_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1055#1077#1088#1077#1074#1086#1079#1095#1080#1082
          Width = 118
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'basedoc_number'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1044#1086#1082#1091#1084#1077#1085#1090'- '#1086#1089#1085#1086#1074#1072#1085#1080#1077
          Width = 101
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'arrival_datetime_fact'
          Footers = <>
          Title.Caption = #1055#1088#1080#1073#1099#1090#1080#1077' '#1092#1072#1082#1090
          Width = 88
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'go_datetime_fact'
          Footers = <>
          Title.Caption = #1059#1073#1099#1090#1080#1077' '#1092#1072#1082#1090
          Width = 81
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doctype_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1058#1080#1087' '#1076#1086#1082'.'
          Width = 74
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_created'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'.'#1089#1086#1079'.'
          Width = 83
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'user_created'
          Footers = <>
          Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
          Width = 140
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    inherited plStatus: TPanel
      Width = 701
      ExplicitWidth = 701
    end
    inherited plLinkedObjects: TPanel
      Top = 460
      Width = 695
      Height = 164
      ExplicitTop = 460
      ExplicitWidth = 695
      ExplicitHeight = 164
      inherited dgLinkedObjects: TDBGridEh
        Top = 28
        Width = 695
        Height = 136
        TitleParams.MultiTitle = True
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'base_doc_text'
            Footers = <>
            Title.Caption = #1046#1044#1053
            Width = 96
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'object_type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1086#1073#1098#1077#1082#1090#1072
            Width = 82
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088
            Width = 91
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'kind_code'
            Footers = <>
            Title.Caption = #1042#1080#1076
            Width = 73
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'forwarder_code'
            Footers = <>
            Title.Caption = #1043#1088#1091#1079#1086'- '#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
            Width = 99
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'consignee_code'
            Footers = <>
            Title.Caption = #1043#1088#1091#1079#1086'- '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
            Width = 90
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'owner_code'
            Footers = <>
            Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
            Width = 99
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'state_code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = [fsItalic]
            Footers = <>
            Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 94
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'dlvtype_code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7697781
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
            Width = 126
          end>
      end
      object plSpec: TPanel
        Left = 0
        Top = 0
        Width = 695
        Height = 28
        Align = alTop
        BevelOuter = bvNone
        Color = 15921906
        ParentBackground = False
        TabOrder = 1
        StyleElements = [seFont, seBorder]
        object sbAddSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 0
          Top = 0
          Width = 32
          Height = 28
          Hint = #1044#1086#1073#1072#1074#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
            6FA864000002B74944415478DA95536D485351187ECFB9F76E7326AE284DB7E1
            A696A625E43228B21267BF84C26819FD88304DD0B20F427FA53FB3122A23C8A1
            A112A97FB410131209A6865A7FC48FB2E676754E86A10B6D5F77F7DCCE068229
            86BDE7CFE19C97E73CCF799F07C186DA7B50AB3B909B752556AFB91C11A5D479
            573C7697CDF17AB26FB4D9353167DFD88FD6366C14C7E5969A9E1B4E1B8AB5AA
            DDB047150D724E063E21003FDD6E70B89760D8326CB63CEB2AF77B7CC25F0072
            A5823399CB470D71A919295AAD87E5582C4920134401640C17EA0A0485209972
            F0CA293B3FDE52566758030903E4555D32E7E7198BD2F409ABA2242909489845
            0CD144A9C1B1320F4149C418106111F68CF1D61DEF7BFBCD1F6ADBAE8701429A
            8B6AEFCC9CD89FE641182B10C52474C95939CE561F07CBFC10F8837E82018314
            5AA2E81BFCF15569BEFB38D135E5B0A39C5B0535172F9CBB9FA456FB0451A400
            40681B96313238167F143E3947202006282CA2E7803986F1CD2C3815CD4D6FAA
            86CC3D0F51E1939BD3E7F38CC92A6524A1BA8165584C2412D67E24D6009F5D5F
            2880001861088AC1103859F57BD9E6F6CE8177354D27D1D5C6AA4041F6291663
            44F4D13AD8B733090748E8450C72CAC24F5F97A8241996C1F7652B9971DB0820
            CCB674BEB575DC7B91FCFF0C24CA204019745006D594414E4541B5C974B63A59
            ADD9DE1F60C667753915ADE6F6CAC1C6EE4728964EE1DA83DB740AE91EC46C67
            0AC467B14E285F553CD52F4CF37CD807C6CAC297F9678CC5E97ADDB67CD0D3DB
            DFD057DB56BAC98999712919A99A901339FA0F122B50EA1C9582110A869D38CF
            2B276DFC586B595D96DFBBCE896B59309698EA0F1B334B125431340BAA7016FC
            340B8BBFDC30BBBC08231F471A2CF55D373665617DC5A724E812B30F99766963
            F2232223B4DEDFDEB9A55957F7CCC07887F31BBF751AB7A8D0BDF4AF863F8BB1
            762158E300DF0000000049454E44AE426082}
          ExplicitLeft = 1
          ExplicitTop = 1
          ExplicitHeight = 26
        end
        object sbDeleteSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 32
          Top = 0
          Width = 32
          Height = 28
          Hint = #1059#1076#1072#1083#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
            6FA864000001654944415478DA6364A0103052D5004731310B2B1919E9D673E7
            D662535CA5AC1C78F2EBD7977B5FBC38866180A38484F98E8C8CE39FFEFC6198
            B06D5B00D0904DC89ACB15143C4B5252B6F2FDFECDE0356386EDDE972F8FA218
            50252717589098B8965354F4DBB7A74FB9A6ECDA15D87CF6EC46905CB5B6B66F
            6E40C0462E09892F3F5FBEE499B86D5B680BD495285EA8D0D4F42A0808D8C22B
            23F3E3EBB3671C9D1B3678FEFDF4E96F657CFC2E1E09891F9F9F3EE598BC7327
            8AEB3002B15A4BCB372F2464A380A4E4BFB78F1E3131B0B030084B4BFFFBF8F0
            21D3642457E18D857C4949E7B29494DD0242423F181819193EBE78C1D1BB7DBB
            67EFC58B3B898AC602A801FCE41880E205A0B31958591984A4A4FE7C7AF48885
            A017C0811818B885575A1A6B207E0106E2245C81088AC642603472901B8D0413
            92BCBC674962E256BEFFFF193CA74FB7D9F7EAD531D293B29151D0F1274F9EED
            7FF5EA04DE582005506C000043E9DA11FA5170A10000000049454E44AE426082}
          ExplicitLeft = 33
          ExplicitTop = 1
          ExplicitHeight = 26
        end
        object sbEditSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 64
          Top = 0
          Width = 32
          Height = 28
          Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
            62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
            08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
            4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
            C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
            892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
            4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
            0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
            C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
            8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
            346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
            5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
            3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
            378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
            C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
            1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
            FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
            F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
            0049454E44AE426082}
          ExplicitLeft = 65
          ExplicitTop = 1
          ExplicitHeight = 26
        end
        object Bevel8: TBevel
          AlignWithMargins = True
          Left = 98
          Top = 2
          Width = 1
          Height = 24
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alLeft
          ExplicitLeft = 502
          ExplicitHeight = 43
        end
        object sbCancelConfirmSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 101
          Top = 0
          Width = 32
          Height = 28
          Hint = #1057#1085#1103#1090#1100' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1077
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = sbCancelConfirmSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002214944415478DAA593414854411880BF79BBAE91A5E11A
            49B16EA9A965872048EA50770FA61B5114614851D02524E8D221F350874ED2A5
            8317C17318044114041156161DC4758B2D4D8AA4D5B0A0F6BD7D3B7FF3467775
            334F0EBC37EFBD99EF9BF9E7FDBF1211D6D3D42A815219738F165E75E9E89C23
            52B3B640A9EFB436D570E42098EF764CEBE2A55FBC45521F7F85452A570B0AF0
            D136487F45E6162C24019CCF235515B06B1B7AF41DF90FD3990D225B97052BE1
            851C5CBD86F4F723E9F4221C8FE3F4F591BF711D2A147A6C1C3F3D93D9682405
            8170E90CF2E91BEAF61D686C04CF23DFDD6D77111A1A824804999CC4BF700EEA
            6B71EF3F6593882A0AE4E269184B21B118CEE0A00502896D65658879F6130974
            2A856A6B21FBE019950581369D3A7F123102BBE5BABAC5550D689B81BDAE2E73
            80297B2681C07DF8FC1F41CF09E4551231024CCCA1E1E1A220583DD7D1813621
            04E3CEA13DB88F464B057427909713CBF05208C13F524108AE8BDBDE8E4E2609
            1DDECB9FC7AFD95210F8A673CE1E43BF9F2174F71EAAB9D902B9CE4EBBE5C8C8
            08949723E3E3B8A78EA39A76907DF26659E099EC53BBE35175A0157E64716EDE
            C2EFEDB50726BE8F6A682032308077E5325485C925A7C87DC9CC568BD4161329
            6B244E432CAAF69BD527A6D0B3F3365E7B26C155BD19A7258637398DBF04AF4A
            E5DF411DECDC1E0DEFAB376021858D449B39A2F1529F4BE0FF16D3CF358A69A9
            9F377074E57CB5DE72FE0B42C953F0B2C476DB0000000049454E44AE426082}
        end
      end
    end
    inherited plSearch: TPanel
      Width = 701
      ExplicitWidth = 701
      inherited Label1: TLabel
        Height = 14
      end
      inherited edSearch: TEdit
        Width = 519
        ExplicitWidth = 519
      end
    end
  end
  inherited plLeft: TPanel
    inherited plTool: TPanel
      Color = clBtnFace
      StyleElements = [seFont, seClient, seBorder]
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'declare @userid int, @doctypeid int, @datebegin datetime, @datee' +
        'nd datetime, @isconfirmed int;'
      ''
      
        'select @doctypeid = min(id) from doctypes where system_section =' +
        ' '#39'income_removal'#39';'
      'select @userid = :userid;'
      ''
      
        'select @datebegin = datebegin, @dateend  = dateend, @isconfirmed' +
        '  = isconfirmed '
      'from dbo.f_GetFilterParams(@userid,@doctypeid);'
      ''
      
        'select d.*, h.*, (case when d.isconfirmed = 1 then '#39#39' else '#39#39' en' +
        'd) as state_code,'
      
        '(select code from doctypes t where t.id = d.doctype_id) as docty' +
        'pe_code,'
      
        '(case when ps1.cnt>1 then '#39'**'#39' else ps1.pass_number end) as pass' +
        '_number,'
      't.code as delivery_code,'
      
        '(select count(*) where exists (select 1 from f_GetLinkedDocument' +
        's(d.id))) as docexists,'
      
        '(select code from counteragents c where c.id = h.dealer_id) as d' +
        'ealer_code,'
      
        'isnull(car_number,'#39'???'#39')+'#39', '#39'+isnull((select person_name from pe' +
        'rsons p where p.id = h.driver_id),'#39'???'#39') as car_full_data,'
      
        '(select '#39#1045#1089#1090#1100#39' where exists (select 1 from doccargorestrictions ' +
        'r, objects2docspec o2s where r.task_id = o2s.task_id and o2s.doc' +
        '_id = d.id and r.dateend is null)) as restr_sign,'
      
        '(select '#39#1045#1089#1090#1100#39' where exists (select 1 from doccargoletters r, ob' +
        'jects2docspec o2s where r.task_id = o2s.task_id and o2s.doc_id =' +
        ' d.id)) as letter_sign, '
      
        '(select user_name from users u where u.id = d.user_created) as u' +
        'ser_created'
      'from docremoval h, documents d'
      'left outer join ('
      
        '     select osh.doc_id, min(p.pass_number) as pass_number, count' +
        '(*) as cnt '
      '     from passes p, passoperations po, objectstatehistory osh '
      
        '     where p.id = po.parent_id and po.task_id = osh.task_id and ' +
        'po.container_id = osh.object_id'
      '     and isnull(p.isabolished,0) = 0  '
      '     group by osh.doc_id'
      ') ps1 on (d.id = ps1.doc_id)'
      ', deliverytypes t'
      'where d.id = h.id and t.id = h.dlv_type_id'
      'and (1 = :showall or d.folder_id = :folder_id)'
      'and ('
      
        '    not exists (select 1 from dbo.f_GetFilterList(@userid, @doct' +
        'ypeid) fl1)'
      
        '    or exists (select 1 from dbo.f_GetFilterList(@userid, @docty' +
        'peid) fl, objects2docspec s, containers c where  s.doc_id = d.id' +
        ' and c.id = s.object_id and charindex(fl.v, c.cnum)>0) '
      ')'
      'and (@datebegin is null or  d.doc_date >=@datebegin)'
      'and (@dateend is null or  d.doc_date < @dateend+1)'
      
        'and (isnull(@isconfirmed,0)=0 or d.isconfirmed = isnull(@isconfi' +
        'rmed,0))'
      'order by d.doc_date'
      '')
    SelectCommand.Parameters = <
      item
        Name = 'userid'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'showall'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'folder_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      
        'update documents set doctype_id = :doctype_id, doc_date = :doc_d' +
        'ate where id = :id;'
      ''
      'update docremoval'
      'set'
      '  dlv_type_id = :dlv_type_id,'
      '  dealer_id = :dealer_id,'
      '  attorney = :attorney,'
      '  driver_name = :driver_name,'
      '  driver_id = :driver_id,'
      '  plan_date = :plan_date,'
      '  fact_date = :fact_date,'
      '  car_data = :car_data,'
      '  car_number = :car_number,'
      '  note = :note,'
      '  recept_org_name = :recept_org_name,'
      '  recept_datetime = :recept_datetime,'
      '  recept_address = :recept_address,'
      '  recept_contact = :recept_contact,'
      '  delivery_org_name = :delivery_org_name,'
      '  delivery_datetime = :delivery_datetime,'
      '  delivery_address = :delivery_address,'
      '  delivery_contact = :delivery_contact,'
      '  return_datetime = :return_datetime,'
      '  return_address = :return_address,'
      '  return_contact = :return_contact,'
      '  movers_amount = :movers_amount,'
      '  arrival_datetime_fact =:arrival_datetime_fact, '
      '  go_datetime_fact =:go_datetime_fact,'
      '  inspector_id = :inspector_id, '
      '  extra_place_sign = :extra_place_sign,'
      '  extra_datetime = :extra_datetime,'
      '  extra_address = :extra_address,'
      '  extra_contact = :extra_contact,'
      '  extra_go_datetime_fact = :extra_go_datetime_fact,'
      '  extra_arrival_datetime_fact = :extra_arrival_datetime_fact'
      'where'
      '  id = :id'
      ''
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'doctype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dealer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'attorney'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'driver_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'driver_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'plan_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'fact_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'car_data'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'car_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'note'
        Size = -1
        Value = Null
      end
      item
        Name = 'recept_org_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'recept_datetime'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'recept_address'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'recept_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'delivery_org_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'delivery_datetime'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'delivery_address'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'delivery_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'return_datetime'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'return_address'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'return_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'movers_amount'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'arrival_datetime_fact'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'go_datetime_fact'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'inspector_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'extra_place_sign'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'extra_datetime'
        Size = -1
        Value = Null
      end
      item
        Name = 'extra_address'
        Size = -1
        Value = Null
      end
      item
        Name = 'extra_contact'
        Size = -1
        Value = Null
      end
      item
        Name = 'extra_go_datetime_fact'
        Size = -1
        Value = Null
      end
      item
        Name = 'extra_arrival_datetime_fact'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      
        'delete from documents where  id = :id and isnull(isconfirmed,0)<' +
        '>1')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select d.*, h.*, (case when d.isconfirmed = 1 then '#39#39' else '#39#39' en' +
        'd) as state_code,'
      
        '(select code from doctypes t where t.id = d.doctype_id) as docty' +
        'pe_code,'
      
        '(case when ps1.cnt>1 then '#39'**'#39' else ps1.pass_number end) as pass' +
        '_number,'
      't.code as delivery_code,'
      
        '(select count(*) where exists (select 1 from f_GetLinkedDocument' +
        's(d.id))) as docexists,'
      
        '(select code from counteragents c where c.id = h.dealer_id) as d' +
        'ealer_code,'
      
        'isnull(car_number,'#39'???'#39')+'#39', '#39'+isnull((select person_name from pe' +
        'rsons p where p.id = h.driver_id),'#39'???'#39') as car_full_data,'
      
        '(select '#39#1045#1089#1090#1100#39' where exists (select 1 from doccargorestrictions ' +
        'r, objects2docspec o2s where r.task_id = o2s.task_id and o2s.doc' +
        '_id = d.id and r.dateend is null)) as restr_sign,'
      
        '(select '#39#1045#1089#1090#1100#39' where exists (select 1 from doccargoletters r, ob' +
        'jects2docspec o2s where r.task_id = o2s.task_id and o2s.doc_id =' +
        ' d.id)) as letter_sign, '
      
        '(select user_name from users u where u.id = d.user_created) as u' +
        'ser_created'
      'from docremoval h, documents d'
      'left outer join ('
      
        '     select osh.doc_id, min(p.pass_number) as pass_number, count' +
        '(*) as cnt '
      '     from passes p, passoperations po, objectstatehistory osh '
      
        '     where p.id = po.parent_id and po.task_id = osh.task_id and ' +
        'po.container_id = osh.object_id'
      '     and isnull(p.isabolished,0) = 0  '
      '     group by osh.doc_id'
      ') ps1 on (d.id = ps1.doc_id)'
      ', deliverytypes t'
      'where d.id = h.id and t.id = h.dlv_type_id'
      'and d.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited pmDocFlow: TPopupMenu
    object N15: TMenuItem [3]
      Caption = #1055#1088#1086#1089#1090#1072#1074#1080#1090#1100' '#1092#1072#1082#1090#1080#1095#1077#1089#1082#1086#1077' '#1074#1088#1077#1084#1103
      ShortCut = 16452
      OnClick = N15Click
    end
    object N16: TMenuItem [4]
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1076#1086#1087'.'#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080
      ShortCut = 16465
      OnClick = sbRestrictionsClick
    end
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
  end
  inherited drvLinkedObjects: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'select distinct ds.doc_id, o.id, o.linked_object_id, o.object_ty' +
        'pe, o.cnum, o.kind_code, '
      
        '(select code from counteragents c where c.id = o.owner_id) as ow' +
        'ner_code, '
      
        '(select code from objectstatekinds k where k.id = ls.object_stat' +
        'e_id) as state_code,'
      
        '(select code from deliverytypes dt where t.dlv_type_id = dt.id) ' +
        'as dlvtype_code, '
      
        '(select code from counteragents c where c.id = t.forwarder_id) a' +
        's forwarder_code,'
      
        '(select code from counteragents c where c.id = t.consignee_id) a' +
        's consignee_code,'
      'o.current_task_id, t.dlv_type_id, '
      
        '(select d1.doc_number+'#39' '#1086#1090' '#39'+convert(varchar, d1.doc_date, 104) ' +
        'from documents d1, doccargosheet sh where d1.id = sh.id and sh.t' +
        'ask_id = t.id) as base_doc_text '
      
        'from v_objects o, objects2docspec ds, v_lastobjectstates ls, tas' +
        'ks t '
      'where o.id = ds.object_id  and ls.task_id = ds.task_id '
      
        'and ls.object_id = o.id and t.id = ds.task_id and ds.doc_id = :d' +
        'oc_id;'
      ''
      '')
    Left = 361
  end
  inherited IL: TPngImageList
    Bitmap = {}
  end
end
