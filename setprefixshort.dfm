﻿inherited FormSetPrefixShort: TFormSetPrefixShort
  Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1077' '#1087#1088#1077#1092#1080#1082#1089' '#1076#1083#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
  ClientHeight = 201
  ClientWidth = 593
  ExplicitWidth = 599
  ExplicitHeight = 229
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 86
    Top = 25
    Width = 267
    Height = 16
    Alignment = taRightJustify
    Caption = #1055#1088#1077#1092#1080#1082#1089' ('#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102': '#1076#1077#1085#1100'+'#1084#1077#1089#1103#1094')'
  end
  object Label2: TLabel [1]
    Left = 89
    Top = 73
    Width = 264
    Height = 16
    Alignment = taRightJustify
    Caption = #1053#1072#1095#1072#1083#1100#1085#1099#1081' '#1085#1086#1084#1077#1088' ('#1087#1091#1089#1090#1086' - '#1072#1074#1090#1086#1084#1072#1090#1086#1084')'
  end
  object Label3: TLabel [2]
    Left = 289
    Top = 121
    Width = 64
    Height = 16
    Alignment = taRightJustify
    Caption = #1053#1086#1084#1077#1088' '#1050#1055
  end
  inherited plBottom: TPanel
    Top = 160
    Width = 593
    TabOrder = 3
    ExplicitTop = 160
    ExplicitWidth = 593
    inherited btnCancel: TButton
      Left = 477
      ExplicitLeft = 477
    end
    inherited btnOk: TButton
      Left = 358
      OnClick = btnOkClick
      ExplicitLeft = 358
    end
  end
  object edPrefix: TDBEditEh [4]
    Left = 366
    Top = 21
    Width = 209
    Height = 24
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object neStartNumber: TDBNumberEditEh [5]
    Left = 366
    Top = 69
    Width = 209
    Height = 24
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edKP: TDBEditEh [6]
    Left = 368
    Top = 118
    Width = 207
    Height = 24
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    TabOrder = 2
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 32
    Top = 152
  end
  inherited qrAux: TADOQuery
    Left = 80
    Top = 152
  end
end
