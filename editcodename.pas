﻿unit editcodename;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEditCodeName = class(TFormEdit)
    Label1: TLabel;
    Label2: TLabel;
    edCode: TDBEditEh;
    edName: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditCodeName: TFormEditCodeName;

implementation

{$R *.dfm}

end.
