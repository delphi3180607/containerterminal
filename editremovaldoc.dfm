﻿inherited FormEditRemovalDoc: TFormEditRemovalDoc
  Caption = #1042#1099#1074#1086#1079
  ClientHeight = 662
  ClientWidth = 932
  Font.Height = -12
  ExplicitLeft = -32
  ExplicitWidth = 938
  ExplicitHeight = 690
  PixelsPerInch = 96
  TextHeight = 14
  object Bevel3: TBevel [0]
    Left = 11
    Top = 514
    Width = 907
    Height = 53
    Shape = bsFrame
  end
  object Label1: TLabel [1]
    Left = 7
    Top = 6
    Width = 102
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel [2]
    Left = 176
    Top = 6
    Width = 93
    Height = 13
    Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel [3]
    Left = 7
    Top = 51
    Width = 70
    Height = 13
    Caption = #1055#1077#1088#1077#1074#1086#1079#1095#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbForwarder: TSpeedButton [4]
    Left = 464
    Top = 68
    Width = 27
    Height = 24
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = sbForwarderClick
  end
  object Label15: TLabel [5]
    Left = 222
    Top = 519
    Width = 36
    Height = 13
    Caption = #1040#1076#1088#1077#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label16: TLabel [6]
    Left = 17
    Top = 519
    Width = 70
    Height = 13
    Caption = #1044#1072#1090#1072', '#1074#1088#1077#1084#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label17: TLabel [7]
    Left = 11
    Top = 497
    Width = 134
    Height = 13
    Caption = #1042#1086#1079#1074#1088#1072#1090' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
  end
  object Label18: TLabel [8]
    Left = 506
    Top = 51
    Width = 109
    Height = 13
    Caption = #1052#1072#1088#1082#1072' '#1072#1074#1090#1086#1084#1086#1073#1080#1083#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel [9]
    Left = 345
    Top = 6
    Width = 143
    Height = 13
    Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080'/'#1054#1087#1077#1088#1072#1094#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label24: TLabel [10]
    Left = 506
    Top = 95
    Width = 56
    Height = 13
    Caption = #1042#1086#1076#1080#1090#1077#1083#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label25: TLabel [11]
    Left = 8
    Top = 95
    Width = 82
    Height = 13
    Caption = #1044#1086#1074#1077#1088#1077#1085#1085#1086#1089#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label27: TLabel [12]
    Left = 718
    Top = 50
    Width = 108
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1072#1074#1090#1086#1084#1086#1073#1080#1083#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 623
    Width = 932
    Height = 39
    TabOrder = 14
    ExplicitTop = 623
    ExplicitWidth = 932
    ExplicitHeight = 39
    inherited btnCancel: TButton
      Left = 816
      Height = 33
      ExplicitLeft = 816
      ExplicitHeight = 33
    end
    inherited btnOk: TButton
      Left = 697
      Height = 33
      OnClick = btnOkClick
      ExplicitLeft = 697
      ExplicitHeight = 33
    end
  end
  object edDocNumber: TDBEditEh [14]
    Left = 7
    Top = 24
    Width = 147
    Height = 21
    DataField = 'doc_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object dtDocDate: TDBDateTimeEditEh [15]
    Left = 176
    Top = 24
    Width = 121
    Height = 21
    DataField = 'doc_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object edReturnAddress: TDBEditEh [16]
    Left = 222
    Top = 539
    Width = 689
    Height = 21
    DataField = 'return_address'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 11
    Visible = True
  end
  object dtReturnDateTime: TDBDateTimeEditEh [17]
    Left = 17
    Top = 539
    Width = 199
    Height = 21
    DataField = 'return_datetime'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateTimeEh
    ParentFont = False
    TabOrder = 10
    Visible = True
  end
  object edCarData: TDBEditEh [18]
    Left = 506
    Top = 69
    Width = 201
    Height = 21
    DataField = 'car_data'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
  object nuMovers: TDBNumberEditEh [19]
    Left = 20
    Top = 591
    Width = 121
    Height = 21
    ControlLabel.Width = 99
    ControlLabel.Height = 13
    ControlLabel.Caption = #1063#1080#1089#1083#1086' '#1075#1088#1091#1079#1095#1080#1082#1086#1074
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -11
    ControlLabel.Font.Name = 'Verdana'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'movers_amount'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    Visible = True
  end
  object laDlvTypes: TDBSQLLookUp [20]
    Left = 345
    Top = 24
    Width = 245
    Height = 21
    DataField = 'dlv_type_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 2
    Visible = True
    SqlSet = ssDlvTypes
    RowCount = 0
  end
  object edAttorney: TDBEditEh [21]
    Left = 8
    Top = 114
    Width = 454
    Height = 21
    DataField = 'attorney'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = True
  end
  object cbExtraPlace: TDBCheckBoxEh [22]
    Left = 20
    Top = 469
    Width = 155
    Height = 17
    Caption = #1047#1072#1077#1079#1076' '#1085#1072' '#1076#1086#1087'.'#1084#1077#1089#1090#1086
    DataField = 'extra_place_sign'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 9
  end
  object luDealer: TDBSQLLookUp [23]
    Left = 8
    Top = 69
    Width = 454
    Height = 22
    AutoSize = False
    Color = 14810109
    DataField = 'dealer_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 3
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object edNote: TDBEditEh [24]
    Left = 222
    Top = 591
    Width = 689
    Height = 21
    ControlLabel.Width = 71
    ControlLabel.Height = 13
    ControlLabel.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -11
    ControlLabel.Font.Name = 'Verdana'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 13
    Visible = True
  end
  object edCarNumber: TDBEditEh [25]
    Left = 718
    Top = 69
    Width = 174
    Height = 21
    Color = 14810109
    DataField = 'car_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 6
    Visible = True
  end
  object laDriver: TDBSQLLookUp [26]
    Left = 506
    Top = 114
    Width = 416
    Height = 22
    AutoSize = False
    Color = 14810109
    DataField = 'driver_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end
      item
        Style = ebsPlusEh
        OnClick = laDriverEditButtons1Click
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 7
    Visible = True
    SqlSet = ssPersons
    RowCount = 20
  end
  object pcAddresses: TPageControl [27]
    Left = 7
    Top = 144
    Width = 914
    Height = 316
    ActivePage = TabSheet1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    object TabSheet1: TTabSheet
      Caption = #1040#1076#1088#1077#1089#1072
      object Label6: TLabel
        Left = 3
        Top = 7
        Width = 83
        Height = 13
        Caption = #1055#1088#1080#1077#1084' '#1075#1088#1091#1079#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 457
        Top = 7
        Width = 80
        Height = 13
        Caption = #1057#1076#1072#1095#1072' '#1075#1088#1091#1079#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 9
        Top = 80
        Width = 70
        Height = 13
        Caption = #1044#1072#1090#1072', '#1074#1088#1077#1084#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label10: TLabel
        Left = 9
        Top = 131
        Width = 36
        Height = 13
        Caption = #1040#1076#1088#1077#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 9
        Top = 183
        Width = 46
        Height = 13
        Caption = #1050#1086#1085#1090#1072#1082#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 464
        Top = 80
        Width = 70
        Height = 13
        Caption = #1044#1072#1090#1072', '#1074#1088#1077#1084#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label13: TLabel
        Left = 464
        Top = 131
        Width = 36
        Height = 13
        Caption = #1040#1076#1088#1077#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label14: TLabel
        Left = 464
        Top = 183
        Width = 46
        Height = 13
        Caption = #1050#1086#1085#1090#1072#1082#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label20: TLabel
        Left = 9
        Top = 30
        Width = 65
        Height = 13
        Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label21: TLabel
        Left = 464
        Top = 30
        Width = 65
        Height = 13
        Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label23: TLabel
        Left = 9
        Top = 240
        Width = 98
        Height = 13
        Caption = #1055#1088#1080#1073#1099#1090#1080#1077' - '#1092#1072#1082#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        StyleElements = [seClient, seBorder]
      end
      object Label22: TLabel
        Left = 190
        Top = 240
        Width = 84
        Height = 13
        Caption = #1059#1073#1099#1090#1080#1077' - '#1092#1072#1082#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        StyleElements = [seClient, seBorder]
      end
      object dtReceptDateTime: TDBDateTimeEditEh
        Left = 9
        Top = 100
        Width = 206
        Height = 21
        DataField = 'recept_datetime'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Kind = dtkDateTimeEh
        ParentFont = False
        TabOrder = 0
        Visible = True
      end
      object edReceptAdress: TDBEditEh
        Left = 9
        Top = 151
        Width = 432
        Height = 21
        DataField = 'recept_address'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        Visible = True
      end
      object edReceptContact: TDBEditEh
        Left = 9
        Top = 203
        Width = 432
        Height = 21
        DataField = 'recept_contact'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Visible = True
      end
      object dtDlvDateTime: TDBDateTimeEditEh
        Left = 464
        Top = 100
        Width = 176
        Height = 21
        DataField = 'delivery_datetime'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Kind = dtkDateTimeEh
        ParentFont = False
        TabOrder = 3
        Visible = True
      end
      object edDlvAddress: TDBEditEh
        Left = 464
        Top = 151
        Width = 432
        Height = 21
        DataField = 'delivery_address'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        Visible = True
      end
      object edDlvContact: TDBEditEh
        Left = 464
        Top = 203
        Width = 432
        Height = 21
        DataField = 'delivery_contact'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        Visible = True
      end
      object edReceptOrgName: TDBEditEh
        Left = 9
        Top = 50
        Width = 434
        Height = 21
        DataField = 'recept_org_name'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        Visible = True
      end
      object edDeliveryOrgName: TDBEditEh
        Left = 464
        Top = 50
        Width = 432
        Height = 21
        DataField = 'delivery_org_name'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
        Visible = True
      end
      object dtArrivalFact: TDBDateTimeEditEh
        Left = 9
        Top = 259
        Width = 163
        Height = 21
        DataField = 'arrival_datetime_fact'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Kind = dtkDateTimeEh
        ParentFont = False
        TabOrder = 8
        Visible = True
      end
      object dtGoFact: TDBDateTimeEditEh
        Left = 190
        Top = 259
        Width = 166
        Height = 21
        DataField = 'go_datetime_fact'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Kind = dtkDateTimeEh
        ParentFont = False
        TabOrder = 9
        Visible = True
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1081' '#1072#1076#1088#1077#1089
      ImageIndex = 1
      object Label4: TLabel
        Left = 25
        Top = 32
        Width = 70
        Height = 13
        Caption = #1044#1072#1090#1072', '#1074#1088#1077#1084#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 25
        Top = 83
        Width = 36
        Height = 13
        Caption = #1040#1076#1088#1077#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label19: TLabel
        Left = 25
        Top = 135
        Width = 46
        Height = 13
        Caption = #1050#1086#1085#1090#1072#1082#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label26: TLabel
        Left = 25
        Top = 196
        Width = 98
        Height = 13
        Caption = #1055#1088#1080#1073#1099#1090#1080#1077' - '#1092#1072#1082#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        StyleElements = [seClient, seBorder]
      end
      object Label28: TLabel
        Left = 206
        Top = 196
        Width = 84
        Height = 13
        Caption = #1059#1073#1099#1090#1080#1077' - '#1092#1072#1082#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        StyleElements = [seClient, seBorder]
      end
      object dtExtraDateTime: TDBDateTimeEditEh
        Left = 25
        Top = 52
        Width = 204
        Height = 21
        DataField = 'extra_datetime'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Kind = dtkDateTimeEh
        ParentFont = False
        TabOrder = 0
        Visible = True
      end
      object edExtraAddress: TDBEditEh
        Left = 25
        Top = 103
        Width = 526
        Height = 21
        DataField = 'extra_address'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        Visible = True
      end
      object edExtraContact: TDBEditEh
        Left = 25
        Top = 155
        Width = 526
        Height = 21
        DataField = 'extra_contact'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Visible = True
      end
      object dtExtraArrivalFact: TDBDateTimeEditEh
        Left = 25
        Top = 215
        Width = 162
        Height = 21
        DataField = 'extra_go_datetime_fact'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Kind = dtkDateTimeEh
        ParentFont = False
        TabOrder = 3
        Visible = True
      end
      object dtExtraGoFact: TDBDateTimeEditEh
        Left = 206
        Top = 215
        Width = 166
        Height = 21
        DataField = 'extra_arrival_datetime_fact'
        DataSource = dsLocal
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Kind = dtkDateTimeEh
        ParentFont = False
        TabOrder = 4
        Visible = True
      end
    end
  end
  inherited dsLocal: TDataSource
    Left = 48
    Top = 614
  end
  inherited qrAux: TADOQuery
    Left = 112
    Top = 614
  end
  object ssDlvTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from deliverytypes '
      'where id in ('
      'select dlv_type_id from docroutes where end_doctype_id in'
      
        '(select id from doctypes where system_section = '#39'income_removal'#39 +
        ')'
      ') order by code')
    DownSql.Strings = (
      'select * from deliverytypes '
      'where id in ('
      'select dlv_type_id from docroutes where end_doctype_id in'
      
        '(select id from doctypes where system_section = '#39'income_removal'#39 +
        ')'
      ') order by code'
      '')
    InitSql.Strings = (
      'select * from deliverytypes where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 449
    Top = 14
  end
  object ssPersons: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select * from persons where isnull(isblocked,0) = 0 and person_k' +
        'ind = 1'
      'and person_name like '#39'%@pattern%'#39
      'order by person_name')
    DownSql.Strings = (
      
        'select * from persons where isnull(isblocked,0) = 0  and person_' +
        'kind = 1'
      'order by person_name')
    InitSql.Strings = (
      'select * from persons where id = @id')
    KeyName = 'id'
    DisplayName = 'person_name'
    Connection = dm.connMain
    Left = 611
    Top = 104
  end
end
