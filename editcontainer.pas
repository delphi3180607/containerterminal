﻿unit editcontainer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  DBCtrlsEh, Vcl.Mask, DBSQLLookUp, DBGridEh, Vcl.Buttons, DBLookupEh, functions,
  Data.DB, Data.Win.ADODB;

type
  TFormEditContainer = class(TFormEdit)
    Label2: TLabel;
    Label1: TLabel;
    edCNum: TDBEditEh;
    neFootSize: TDBNumberEditEh;
    Label4: TLabel;
    edNote: TDBMemoEh;
    ssContainerKind: TADOLookUpSqlSet;
    leContainerKind: TDBSQLLookUp;
    Label3: TLabel;
    Label7: TLabel;
    nuWeight: TDBNumberEditEh;
    Label8: TLabel;
    nuCarrying: TDBNumberEditEh;
    Label17: TLabel;
    dtDateMade: TDBDateTimeEditEh;
    Label9: TLabel;
    Label10: TLabel;
    dtDateStiker: TDBDateTimeEditEh;
    cbPicture: TDBCheckBoxEh;
    cbDefective: TDBCheckBoxEh;
    dtInspect: TDBDateTimeEditEh;
    cbNotReady: TDBCheckBoxEh;
    cbACEP: TDBCheckBoxEh;
    procedure edCNumKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditContainer: TFormEditContainer;

implementation

{$R *.dfm}

uses counteragents;

procedure TFormEditContainer.edCNumKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key)<32 then exit;
  if not (Key in [#8,'0'..'9','A'..'Z','a'..'z']) then Key := #0;
end;

end.
