﻿inherited FormEditPlaceContainers: TFormEditPlaceContainers
  BorderStyle = bsSizeable
  Caption = #1056#1072#1079#1084#1077#1097#1077#1085#1080#1077' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074' '#1085#1072' '#1074#1072#1075#1086#1085#1077
  ClientHeight = 674
  ClientWidth = 908
  Scaled = False
  ExplicitWidth = 924
  ExplicitHeight = 712
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 639
    Width = 908
    Height = 35
    ExplicitTop = 639
    ExplicitWidth = 908
    ExplicitHeight = 35
    inherited btnCancel: TButton
      Left = 792
      Height = 29
      Visible = False
      ExplicitLeft = 792
      ExplicitHeight = 29
    end
    inherited btnOk: TButton
      Left = 673
      Height = 29
      Caption = #1047#1072#1082#1088#1099#1090#1100
      OnClick = btnOkClick
      ExplicitLeft = 673
      ExplicitHeight = 29
    end
  end
  object plCarriage: TPanel [1]
    AlignWithMargins = True
    Left = 3
    Top = 31
    Width = 182
    Height = 605
    Align = alLeft
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clSilver
    ParentBackground = False
    TabOrder = 1
    StyleElements = [seFont, seBorder]
    object edCarModel: TDBEditEh
      AlignWithMargins = True
      Left = 1
      Top = 54
      Width = 176
      Height = 22
      Margins.Left = 1
      Margins.Top = 2
      Margins.Right = 1
      Margins.Bottom = 2
      Align = alTop
      BorderStyle = bsNone
      Ctl3D = False
      DataField = 'model_code'
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      EmptyDataInfo.Text = #1052#1086#1076#1077#1083#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Visible = True
    end
    object edCarType: TDBEditEh
      AlignWithMargins = True
      Left = 1
      Top = 28
      Width = 176
      Height = 22
      Margins.Left = 1
      Margins.Top = 2
      Margins.Right = 1
      Margins.Bottom = 2
      Align = alTop
      BorderStyle = bsNone
      Ctl3D = False
      DataField = 'kind_code'
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      EmptyDataInfo.Text = #1058#1080#1087' '#1074#1072#1075#1086#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      Visible = True
    end
    object edCarNum: TDBEditEh
      AlignWithMargins = True
      Left = 1
      Top = 2
      Width = 176
      Height = 22
      Margins.Left = 1
      Margins.Top = 2
      Margins.Right = 1
      Margins.Bottom = 2
      Align = alTop
      BorderStyle = bsNone
      Ctl3D = False
      DataField = 'pnum'
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      EmptyDataInfo.Text = #1053#1086#1084#1077#1088' '#1074#1072#1075#1086#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      Visible = True
    end
    object edOwner: TDBEditEh
      AlignWithMargins = True
      Left = 1
      Top = 80
      Width = 176
      Height = 22
      Margins.Left = 1
      Margins.Top = 2
      Margins.Right = 1
      Margins.Bottom = 2
      Align = alTop
      BorderStyle = bsNone
      Ctl3D = False
      DataField = 'owner_code'
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      EmptyDataInfo.Text = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082' ('#1072#1088#1077#1085#1076#1072#1090#1086#1088')'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      Visible = True
    end
  end
  object plTop: TPanel [2]
    Left = 0
    Top = 0
    Width = 908
    Height = 28
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object sbAdd: TPngSpeedButton
      AlignWithMargins = True
      Left = 1
      Top = 0
      Width = 31
      Height = 26
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1077#1081#1085#1077#1088
      Margins.Left = 1
      Margins.Top = 0
      Margins.Right = 1
      Margins.Bottom = 2
      Align = alLeft
      Flat = True
      Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      OnClick = sbAddClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
        610000001974455874536F6674776172650041646F626520496D616765526561
        647971C9653C000002BE4944415478DAA5935D4893511CC61FA74E97390D6B9B
        1F516C2E334C2535295A8A792549908124A120D145A6303023233FA2444A08F5
        C6ECA2B430B01681994E4D29511321B134F3634A7EA382B9ED7DA7BEE7BC6FAF
        1B2E28BDA9FFB9389C8BE777FEFF739EC7451004FC4FB9FC09286BBBAEA13C4D
        2394A6134A5484A7E0285910CFB5E25E579EFACCB423A0AC2D378550BE72BF42
        E31FA28A80CC7D3736F80D2C5AE7D033DE8EC5E5A57942487675FA4BC35F0087
        98D64407C77969154761328F62C1360B8EE7E023F581421688AE512386C68719
        B1B38CE7971B0C4EC083D65CB5D87667A4FA648056198E0FF32D6019169C40E0
        E9E901415CEBFC3A42E561E81E69C3C8A4698E2344F73AAB75C20E2835EA0B7C
        7DF615C78724A377A913D60D060CC3202FB6C8DE5DC9E77CD85833246E6E8855
        E950DBF1088C852B6CC8E9B86307DC7D97331675382E785DB201D3F298383707
        8BD58C9284723B20CB7009DEBEEE203CC1216504D6AD0C1A7BDBC69BF59D5A3B
        A0B0E12A1B1F992C1BB10C61C5BA829BC7EF6DFB657A631AE4325F84F945A3AA
        E9B1ED7D6ECF2E3BE0D69B2BEC89F044D9B0791036BA8682A8FB3B02BCA57284
        2B6250F1B6CAF6F1469F039067C81CD31C0C0DE63D04CC58A6C0726B6016CDA8
        BE50EF146E96C44502ADDF11B06616AF7A1AC7BBF3FB1D23E8EBD30BD6246CF1
        D963A9E89A69C11AE520E55D507AE68913C0530152891BE2354978D85881558B
        B5B0EFF657C7235E7B91A6E628ED542AF606C4A84FA3EB47B3FDC1C4AF854037
        1102DC25AE885327C138D0884FDFBFCC899ED1F5177D9B701A29F3E979D148A4
        2650A5F4D28524627A7512B33F2721F03C82F6A811243F80A68106F40E0F32A2
        BD3344F16F236DD5C5EAA414D1EF95D495F8C7859E8252EEBF7939A657A660EC
        6F87D9CACE8BE2EC2DF1B6613A5799A01121699C334C440C13B58789F2A4AEBF
        6878E730FD4BFD027D1196F03509C5820000000049454E44AE426082}
      ExplicitHeight = 27
    end
    object sbDelete: TPngSpeedButton
      AlignWithMargins = True
      Left = 36
      Top = 0
      Width = 32
      Height = 25
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1082#1086#1085#1090#1077#1081#1085#1077#1088
      Margins.Top = 0
      Align = alLeft
      Flat = True
      OnClick = sbDeleteClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
        610000001974455874536F6674776172650041646F626520496D616765526561
        647971C9653C000002554944415478DA95924D68D36018C79F7C746D92A6B5DD
        86B2B56C7810A141F0E8459C7810BC09A2CECFA907F13211C7BC7A134410069E
        046FC26EE2556D657EEC24F5D04C2F62B77569BA43ED47DA244BF2C6E74D6CB1
        743BEC8524BCE4FDFD9E27FF278CEFFBB09F65CFDD11A3AF5E767B7B6640B0F0
        680AEF7178FA44DD15BE793BE7BBCE19E2B84571F9F5CAA080C21C57A67BDFB6
        15F6F9B301897DE3560EDF9798E808ECD4B69751F4587EFBE64728F80743340A
        54E7B7DB402C4BE15F2C0512EBFA5C8E61D9129790814F26C1DEAC80A5EBF789
        E32C8582878B319F900E0802CBC83290560B48A3811253C17681C22CC2914412
        ACAD0AD855DD435848E5DF39FD4F20F30F62BEE7198C20702C1EF49A0D70FFD4
        C1470187D248320156658B56F6B0FD782AFFDE1A0AD1BD7B2F465CCF604591E3
        5307003C82667A795899C2B5102E7CB0769F4218164ADC8E3035CDC2C63A4A3C
        80CC24348ADF09711D29FD316FFD7F7E4860CE5EC3C098526C3203502E871DA0
        A0B9B6064EDB50C6BE7E52F7149897AFE600615E94303019613F689F4AAC5A0D
        4C5D879D765B3958FCA60E09BA97AE04954338016655A3104D9B89A5D2AC3471
        08BA551DBA9A86124399F8A9AA7D41F7E26C30675E14118E83A9E9606D53D88D
        6368804F43184D737236031DAD0A1D0C947692F9FD4B0D04C6F90B336C84CF47
        24095CA30326C238BEF8E897952030FDD8711CB16B48E3E31C3D63D089D4EBA7
        B31BE542FF139A67CFA12492775A4D8215250C6B206DEDC8D1E03F199165AE07
        0F85583F796A06E1D5B1D5CF03706F55A60F53C989ECE67A61CF31EE77FD05EB
        B7706A5FA737EA0000000049454E44AE426082}
      ExplicitLeft = 33
      ExplicitHeight = 23
    end
  end
  object dgSpec: TDBCtrlGrid [3]
    Left = 188
    Top = 28
    Width = 684
    Height = 611
    Align = alClient
    AllowDelete = False
    DataSource = dsSpec
    PanelHeight = 152
    PanelWidth = 667
    TabOrder = 3
    RowCount = 4
    OnPaintPanel = dgSpecPaintPanel
    object plContainer: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 515
      Height = 146
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object plFields1: TPanel
        Left = 0
        Top = 25
        Width = 515
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          AlignWithMargins = True
          Left = 1
          Top = 4
          Width = 91
          Height = 17
          Margins.Left = 1
          Margins.Top = 4
          Margins.Right = 1
          Align = alLeft
          AutoSize = False
          Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          ExplicitTop = 1
          ExplicitHeight = 19
        end
        object edContainerNum: TDBEdit
          AlignWithMargins = True
          Left = 94
          Top = 1
          Width = 420
          Height = 20
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alClient
          Ctl3D = True
          DataField = 'cnum'
          DataSource = dsSpec
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          StyleElements = [seFont, seClient]
          ExplicitHeight = 21
        end
      end
      object plFields2: TPanel
        Left = 0
        Top = 121
        Width = 515
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label2: TLabel
          AlignWithMargins = True
          Left = 1
          Top = 4
          Width = 91
          Height = 18
          Margins.Left = 1
          Margins.Top = 4
          Margins.Right = 1
          Align = alLeft
          AutoSize = False
          Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          ExplicitHeight = 22
        end
        object edNote: TDBEdit
          AlignWithMargins = True
          Left = 94
          Top = 1
          Width = 420
          Height = 21
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alClient
          Ctl3D = True
          DataField = 'note'
          DataSource = dsSpec
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          StyleElements = [seFont, seClient]
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 97
        Width = 515
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Label3: TLabel
          AlignWithMargins = True
          Left = 1
          Top = 4
          Width = 91
          Height = 17
          Margins.Left = 1
          Margins.Top = 4
          Margins.Right = 1
          Align = alLeft
          AutoSize = False
          Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          ExplicitHeight = 22
        end
        object edOwnerCode: TDBEdit
          AlignWithMargins = True
          Left = 94
          Top = 1
          Width = 420
          Height = 20
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alClient
          Ctl3D = True
          DataField = 'owner_code'
          DataSource = dsSpec
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          StyleElements = [seFont, seClient]
          ExplicitHeight = 21
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 73
        Width = 515
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object Label4: TLabel
          AlignWithMargins = True
          Left = 1
          Top = 4
          Width = 91
          Height = 17
          Margins.Left = 1
          Margins.Top = 4
          Margins.Right = 1
          Align = alLeft
          AutoSize = False
          Caption = #1058#1080#1087
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          ExplicitHeight = 22
        end
        object edKindCode: TDBEdit
          AlignWithMargins = True
          Left = 94
          Top = 1
          Width = 420
          Height = 20
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alClient
          Ctl3D = True
          DataField = 'kind_code'
          DataSource = dsSpec
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          StyleElements = [seFont, seClient]
          ExplicitHeight = 21
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 515
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 4
        object cbEmpty: TDBCheckBox
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 118
          Height = 19
          Align = alLeft
          Caption = #1055#1086#1088#1086#1078#1085#1080#1081
          Color = clBtnFace
          DataField = 'isempty'
          DataSource = dsSpec
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          StyleElements = []
          OnMouseUp = cbEmptyMouseUp
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 146
        Width = 515
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 5
        object Label5: TLabel
          AlignWithMargins = True
          Left = 1
          Top = 4
          Width = 91
          Height = 18
          Margins.Left = 1
          Margins.Top = 4
          Margins.Right = 1
          Align = alLeft
          AutoSize = False
          Caption = #1057#1090#1072#1085#1094#1080#1103
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          ExplicitTop = 1
        end
        object edStation: TDBEdit
          AlignWithMargins = True
          Left = 94
          Top = 1
          Width = 420
          Height = 21
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alClient
          DataField = 'station_code'
          DataSource = dsSpec
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 22
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 49
        Width = 515
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 6
        object Label6: TLabel
          AlignWithMargins = True
          Left = 1
          Top = 4
          Width = 91
          Height = 17
          Margins.Left = 1
          Margins.Top = 4
          Margins.Right = 1
          Align = alLeft
          AutoSize = False
          Caption = #1047#1072#1103#1074#1082#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          ExplicitHeight = 22
        end
        object edOrder: TDBEdit
          AlignWithMargins = True
          Left = 94
          Top = 1
          Width = 420
          Height = 20
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alClient
          Ctl3D = True
          DataField = 'order_info'
          DataSource = dsSpec
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          StyleElements = [seFont, seClient]
          ExplicitHeight = 21
        end
      end
    end
    object edTaskId: TDBEdit
      AlignWithMargins = True
      Left = 628
      Top = 1
      Width = 38
      Height = 148
      Margins.Left = 1
      Margins.Top = 1
      Margins.Right = 1
      Align = alRight
      DataField = 'order_num'
      DataSource = dsSpec
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      ExplicitHeight = 24
    end
    object plRight: TPanel
      Left = 521
      Top = 0
      Width = 106
      Height = 152
      Align = alRight
      TabOrder = 2
      object plSelectOwner: TPanel
        AlignWithMargins = True
        Left = 4
        Top = 106
        Width = 98
        Height = 31
        Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsUnderline]
        ParentFont = False
        TabOrder = 0
        Visible = False
        StyleElements = [seClient]
        OnClick = plSelectOwnerClick
        OnMouseDown = plSelectContMouseDown
        OnMouseUp = plSelectContMouseUp
      end
      object plSelectKind: TPanel
        AlignWithMargins = True
        Left = 4
        Top = 78
        Width = 98
        Height = 27
        Caption = #1058#1080#1087
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsUnderline]
        ParentFont = False
        TabOrder = 1
        Visible = False
        StyleElements = [seClient]
        OnClick = plSelectKindClick
        OnMouseDown = plSelectContMouseDown
        OnMouseUp = plSelectContMouseUp
      end
      object plSelectCont: TPanel
        AlignWithMargins = True
        Left = 5
        Top = 36
        Width = 98
        Height = 36
        Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsUnderline]
        ParentFont = False
        TabOrder = 2
        StyleElements = [seClient]
        OnClick = plSelectContClick
        OnMouseDown = plSelectContMouseDown
        OnMouseUp = plSelectContMouseUp
      end
      object plOrder: TPanel
        AlignWithMargins = True
        Left = 4
        Top = 3
        Width = 98
        Height = 37
        Caption = #1047#1072#1103#1074#1082#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsUnderline]
        ParentFont = False
        TabOrder = 3
        StyleElements = [seClient]
        OnClick = plOrderClick
        OnMouseDown = plSelectContMouseDown
        OnMouseUp = plSelectContMouseUp
      end
    end
  end
  object plUpDown: TPanel [4]
    Left = 872
    Top = 28
    Width = 36
    Height = 611
    Align = alRight
    BevelOuter = bvLowered
    TabOrder = 4
    object sbUp: TPngSpeedButton
      Left = 1
      Top = 1
      Width = 34
      Height = 304
      Align = alTop
      OnClick = sbUpClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
        610000001974455874536F6674776172650041646F626520496D616765526561
        647971C9653C000001634944415478DA63FCFFFF3F032580119F0125C7334EFE
        FDF3EF77BFED2C1B920D283E967E518C5542EFEFBFBF0C8F3F3CBA30DD7D9121
        5106949EC864FBF7F7DF5551363115051E25867FFFFE31DC78739DE1E19B47B7
        7EFFFAADBB2C72FD2FBC06141D49DBFDF7CF5F1B5176710E053E45867FFFFF31
        3CFDF094E1D2C34B3F80061C5C93B8D583282FA46D8DF96F2567CDF017E88293
        774E32BCFDF0C6726DF2B613448741C2BAF0FFD64AB64017FC65D8716607C3FA
        D4ED8C240562D4F2C0FF366A766003B61FDFCEB035772F690684CCF7FE6FA7E3
        C0F01768C0D6835B19F6941E26CD00DF696EFF1D8C20066CD9B385E150ED49D2
        0C70EF73F8EF68E1048CC6BF0C1BB76F6638D97A8E34031CDAACFE3BDBBA80C3
        60C3C68D0CE77BAF906680559DC97F576737B001EBD76C60B832E52669069894
        E9FD77F77067F80784EB566C60B839EB2E6906E8E56BFEFFF3FB2FC39FDF7F18
        802993E1EEFC87A419402C0000E67DD6E13146D0910000000049454E44AE4260
        82}
    end
    object sbDown: TPngSpeedButton
      Left = 1
      Top = 306
      Width = 34
      Height = 304
      Align = alBottom
      OnClick = sbDownClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
        610000001974455874536F6674776172650041646F626520496D616765526561
        647971C9653C0000015F4944415478DA63FCFFFF3F032580119701A52732FF83
        E4FEFEF907C47F192639CE6524C980E263E9FF2D856D18FEFDFFC7B0FFE15E86
        E9EE8B4833A0E060CA7F0B316B867FFFFE31ECBBB797618EEF32D20CC8DE93F0
        DF52D21AEC823DD777332C0A5D439A01695B63FE5BC95933FC05BA60F7A55D0C
        2B6237926640C2BAF0FFD64AB64017FC65D8716607C3FAD4EDA41910B53CF0BF
        8D9A1DD880EDC7B7336CCDDD4B9A0121F3BDFFDBE93830FC051AB0F5E056863D
        A5874933C0779ADB7F072388015BF66C6138547B9278037CA6BA5870B1711F37
        D2360646E35F868DDB37339C6C3D479C01DE939D77FCFEF5DB5E5D5183434C54
        021C06172E5E64B873FBCE8F3FBFFF1EB93EFDB62B5E035CBA6DD97EFFFC7359
        524C524D5D5D036CC0F9731718EEDCBC7BE7CF9FBFDA77E6DEFF4554189894E9
        9D97929236F8FBF72F48F3A59BB3EEEA93148820A095A576E4CFEF3FACB766DF
        33C7A58691D2EC0C00AFAED2E1D133C37F0000000049454E44AE426082}
      ExplicitTop = 1
    end
  end
  inherited dsLocal: TDataSource
    Left = 112
    Top = 256
  end
  inherited qrAux: TADOQuery
    Left = 64
    Top = 256
  end
  object dsSpec: TDataSource
    DataSet = meSpec
    Left = 168
    Top = 176
  end
  object meSpec: TMemTableEh
    Params = <>
    DataDriver = drvSpec
    AfterOpen = meSpecAfterOpen
    AfterInsert = meSpecAfterInsert
    AfterPost = meSpecAfterPost
    Left = 120
    Top = 176
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select l.*, v.cnum +'#39', '#39'+ (select (case when cr.seal_number like' +
        ' '#39'{%'#39' then '#39#39' else  cr.seal_number end) from cargos cr where cr.' +
        'id = t.object_id) as cnum,'
      
        '(case when isnull(l.container_id,0)=0 then (select code from cou' +
        'nteragents cr where cr.id = l.owner_id) else (select code from c' +
        'ounteragents cr where cr.id = v.owner_id) end) as owner_code,'
      
        '(select code from containerkinds k where k.id = l.kind_id) + '#39', ' +
        #1074#1077#1089'('#1090#1086#1085#1085'): '#39'+'
      
        'isnull((select ltrim(str((cr.weight_fact/1000), 10,2)) from carg' +
        'os cr where cr.id = t.object_id),'#39#39') as kind_code,'
      
        '(select name from stations st where st.id = l.station_id) as sta' +
        'tion_code,'
      '('
      
        ' select d.doc_number+'#39' '#1086#1090' '#39'+convert(varchar, d.doc_date, 104)+'#39',' +
        '  '#39'+'
      
        ' (select c.code from counteragents c where c.id = o.forwarder_id' +
        ') +'#39'->'#39'+(select c.code from counteragents c where c.id = o.consi' +
        'gnee_id)  '
      
        ' from docorderspec s, documents d, docorder o where o.id = s.doc' +
        '_id and s.id = l.orderspec_id and o.id = d.id'
      ') as order_info'
      'from docload l '
      'left outer join v_objects v on (v.id = l.container_id)'
      'left outer join tasks t on (t.id = l.task_id)'
      'where l.joint_id = :joint_id'
      'order by order_num')
    SelectCommand.Parameters = <
      item
        Name = 'joint_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update docload'
      'set'
      '  joint_id = :joint_id,'
      '  order_num = :order_num,'
      '  container_id = :container_id,'
      '  isempty = :isempty,'
      '  kind_id = :kind_id,'
      '  owner_id = :owner_id,'
      '  station_id = :station_id,'
      '  note = :note,'
      '  task_id = :task_id,'
      '  orderspec_id = :orderspec_id,'
      '  temp_move_sign = :temp_move_sign'
      'where'
      '  id = :id;')
    UpdateCommand.Parameters = <
      item
        Name = 'joint_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'order_num'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'station_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'task_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'orderspec_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'temp_move_sign'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'begin'
      ''
      '  declare @newid int, @doctypeid int;'
      ''
      
        '  select @doctypeid = id from doctypes where system_section='#39'out' +
        'come_loads'#39';'
      ''
      
        '  insert into documents (doctype_id, folder_id) values (@doctype' +
        'id, :folder_id);'
      ''
      '  select @newid = IDENT_CURRENT('#39'documents'#39');'
      ''
      ''
      'insert into docload'
      
        '  (id, joint_id, order_num, container_id, isempty, kind_id, owne' +
        'r_id, station_id, note, task_id, orderspec_id)'
      'values'
      
        '  (@newid, :joint_id, :order_num, :container_id, :isempty, :kind' +
        '_id, :owner_id, :station_id, :note, :task_id, :orderspec_id)'
      ''
      ''
      'end;')
    InsertCommand.Parameters = <
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'joint_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'order_num'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'station_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'task_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'orderspec_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      
        'delete from documents where id = :id and isnull(isconfirmed,0) =' +
        ' 0;'
      '')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select l.*, v.cnum +'#39', '#39'+ (select (case when cr.seal_number like' +
        ' '#39'{%'#39' then '#39#39' else  cr.seal_number end) from cargos cr where cr.' +
        'id = t.object_id) as cnum,'
      
        '(case when isnull(l.container_id,0)=0 then (select code from cou' +
        'nteragents cr where cr.id = l.owner_id) else (select code from c' +
        'ounteragents cr where cr.id = v.owner_id) end) as owner_code,'
      
        '(select code from containerkinds k where k.id = l.kind_id) + '#39', ' +
        #1074#1077#1089'('#1090#1086#1085#1085'): '#39'+'
      
        'isnull((select ltrim(str((cr.weight_fact/1000), 10,2)) from carg' +
        'os cr where cr.id = t.object_id),'#39#39') as kind_code,'
      
        '(select name from stations st where st.id = l.station_id) as sta' +
        'tion_code,'
      '('
      
        ' select d.doc_number+'#39' '#1086#1090' '#39'+convert(varchar, d.doc_date, 104)+'#39',' +
        '  '#39'+'
      
        ' (select c.code from counteragents c where c.id = o.forwarder_id' +
        ') +'#39'->'#39'+(select c.code from counteragents c where c.id = o.consi' +
        'gnee_id)  '
      
        ' from docorderspec s, documents d, docorder o where o.id = s.doc' +
        '_id and s.id = l.orderspec_id and o.id = d.id'
      ') as order_info'
      'from docload l '
      'left outer join v_objects v on (v.id = l.container_id)'
      'left outer join tasks t on (t.id = l.task_id)'
      'where l.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 70
    Top = 176
  end
end
