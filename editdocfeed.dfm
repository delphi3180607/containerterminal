﻿inherited FormEditDocFeed: TFormEditDocFeed
  Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088' '#1085#1072' '#1087#1086#1076#1072#1095#1077
  ClientHeight = 553
  ClientWidth = 636
  ExplicitWidth = 642
  ExplicitHeight = 581
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 366
    Top = 34
    Width = 91
    Height = 14
    Caption = #1053#1086#1084#1077#1088' '#1087#1086#1077#1079#1076#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel [1]
    Left = 18
    Top = 91
    Width = 71
    Height = 14
    Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbContainer: TSpeedButton [2]
    Left = 595
    Top = 112
    Width = 29
    Height = 24
    Caption = '...'
  end
  object sbCarriage: TSpeedButton [3]
    Left = 595
    Top = 174
    Width = 29
    Height = 24
    Caption = '...'
  end
  object Label4: TLabel [4]
    Left = 18
    Top = 220
    Width = 93
    Height = 14
    Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel [5]
    Left = 205
    Top = 221
    Width = 138
    Height = 14
    Caption = #1042#1077#1089' ('#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100'), '#1090'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel [6]
    Left = 347
    Top = 221
    Width = 102
    Height = 14
    Caption = #1042#1077#1089' '#1087#1086' '#1046#1044#1053', '#1082#1075'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel [7]
    Left = 481
    Top = 221
    Width = 94
    Height = 14
    Caption = #1042#1077#1089' ('#1092#1072#1082#1090'), '#1082#1075'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel [8]
    Left = 19
    Top = 115
    Width = 41
    Height = 14
    Caption = #1053#1086#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel [9]
    Left = 19
    Top = 178
    Width = 41
    Height = 14
    Caption = #1053#1086#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel [10]
    Left = 322
    Top = 178
    Width = 84
    Height = 14
    Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel [11]
    Left = 322
    Top = 115
    Width = 84
    Height = 14
    Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Bevel1: TBevel [12]
    Left = 16
    Top = 105
    Width = 608
    Height = 2
  end
  object Bevel2: TBevel [13]
    Left = 19
    Top = 168
    Width = 605
    Height = 2
  end
  object Label3: TLabel [14]
    Left = 18
    Top = 154
    Width = 38
    Height = 14
    Caption = #1042#1072#1075#1086#1085
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label18: TLabel [15]
    Left = 19
    Top = 325
    Width = 200
    Height = 14
    Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1081' '#1082#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label19: TLabel [16]
    Left = 203
    Top = 115
    Width = 25
    Height = 14
    Caption = #1060#1091#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label20: TLabel [17]
    Left = 203
    Top = 178
    Width = 25
    Height = 14
    Caption = #1060#1091#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label21: TLabel [18]
    Left = 19
    Top = 34
    Width = 114
    Height = 14
    Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label22: TLabel [19]
    Left = 205
    Top = 34
    Width = 105
    Height = 14
    Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel [20]
    Left = 87
    Top = 293
    Width = 110
    Height = 14
    Caption = #1060#1088#1086#1085#1090' '#1088#1072#1079#1075#1088#1091#1079#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 512
    Width = 636
    TabOrder = 16
    ExplicitTop = 512
    ExplicitWidth = 636
    inherited btnCancel: TButton
      Left = 520
      ExplicitLeft = 520
    end
    inherited btnOk: TButton
      Left = 401
      ExplicitLeft = 401
    end
  end
  object edSealNumber: TDBEditEh [22]
    Left = 18
    Top = 239
    Width = 171
    Height = 22
    DataField = 'seal_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object nuWeightSender: TDBNumberEditEh [23]
    Left = 205
    Top = 239
    Width = 124
    Height = 22
    DataField = 'weight_sender'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object nuWeightDocument: TDBNumberEditEh [24]
    Left = 347
    Top = 239
    Width = 119
    Height = 22
    DataField = 'weight_doc'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object nuWeightFact: TDBNumberEditEh [25]
    Left = 481
    Top = 239
    Width = 119
    Height = 22
    DataField = 'weight_fact'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object stState: TDBEditEh [26]
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 630
    Height = 24
    TabStop = False
    Align = alTop
    Color = 10808562
    DataField = 'state_code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Enabled = False
    TabOrder = 4
    Visible = True
  end
  object edContainerNum: TDBEditEh [27]
    Left = 64
    Top = 112
    Width = 125
    Height = 24
    AutoSize = False
    CharCase = ecUpperCase
    Color = clCream
    DataField = 'container_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    MaxLength = 11
    ParentFont = False
    TabOrder = 5
    Visible = True
    EditMask = 'cccc9999999;0;?'
  end
  object edCarriageNum: TDBEditEh [28]
    Left = 64
    Top = 176
    Width = 123
    Height = 24
    AutoSize = False
    Color = clCream
    DataField = 'carriage_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    MaxLength = 8
    ParentFont = False
    TabOrder = 6
    Visible = True
    EditMask = '99999999;0;?'
  end
  object edTrainNumber: TDBEditEh [29]
    Left = 366
    Top = 53
    Width = 258
    Height = 22
    Color = clCream
    DataField = 'train_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    Visible = True
  end
  object meText: TDBMemoEh [30]
    Left = 18
    Top = 344
    Width = 606
    Height = 155
    AutoSize = False
    DataField = 'cargo_description'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 15
    Visible = True
    WantReturns = True
  end
  object edContFut: TDBEditEh [31]
    Left = 236
    Top = 113
    Width = 65
    Height = 22
    DataField = 'container_foot'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    Visible = True
  end
  object edCarType: TDBEditEh [32]
    Left = 236
    Top = 175
    Width = 65
    Height = 23
    Color = clCream
    DataField = 'carriage_type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    Visible = True
  end
  object edDocNumber: TDBEditEh [33]
    Left = 19
    Top = 53
    Width = 172
    Height = 22
    Color = clCream
    DataField = 'doc_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 10
    Visible = True
  end
  object dtDocDate: TDBDateTimeEditEh [34]
    Left = 205
    Top = 53
    Width = 121
    Height = 22
    DataField = 'doc_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 11
    Visible = True
  end
  object laContainerOwner: TDBSQLLookUp [35]
    Left = 414
    Top = 112
    Width = 176
    Height = 22
    DataField = 'container_owner_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object laCarriageOwner: TDBSQLLookUp [36]
    Left = 414
    Top = 176
    Width = 176
    Height = 22
    DataField = 'carriage_owner_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 13
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object laFront: TDBSQLLookUp [37]
    Left = 203
    Top = 290
    Width = 397
    Height = 22
    DataField = 'unload_front_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 14
    Visible = True
    SqlSet = ssFront
    RowCount = 0
  end
  inherited dsLocal: TDataSource
    Left = 256
    Top = 325
  end
  inherited qrAux: TADOQuery
    Left = 184
    Top = 325
  end
  object ssFront: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from unloadfronts order by code')
    DownSql.Strings = (
      'select * from unloadfronts order by code')
    InitSql.Strings = (
      'select * from unloadfronts where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 32
    Top = 282
  end
end
