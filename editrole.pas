﻿unit editrole;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEditRole = class(TFormEdit)
    edReadOnlySections: TDBEditEh;
    edRestrictConfirmSections: TDBEditEh;
    edUnavailableSections: TDBEditEh;
    cbRestrictDicts: TDBCheckBoxEh;
    cbRestrictObjects: TDBCheckBoxEh;
    cbAllowEditPrice: TDBCheckBoxEh;
    edName: TDBEditEh;
    edAllowDeleteSections: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditRole: TFormEditRole;

implementation

{$R *.dfm}

end.
