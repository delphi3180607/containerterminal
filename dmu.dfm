﻿object dm: Tdm
  OldCreateOrder = False
  Height = 588
  Width = 1094
  object connMain: TADOConnection
    ConnectionTimeout = 300
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 68
    Top = 32
  end
  object qrAux: TADOQuery
    Connection = connMain
    Parameters = <>
    Left = 132
    Top = 32
  end
  object qrCommon: TADOQuery
    Connection = connMain
    Parameters = <>
    Left = 196
    Top = 32
  end
  object spUpdateRegisterDocument: TADOStoredProc
    Connection = connMain
    ProcedureName = 'UpdateRegisterDocument;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@DocId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RootDocId'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@ParentDocId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DocTypeId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DeliveryTypeId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 63
    Top = 128
  end
  object spConfirmDoc: TADOStoredProc
    Connection = connMain
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'ConfirmDoc;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@DocId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SpecId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DateFactExecution'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@UserId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 47
    Top = 200
  end
  object spCancelConfirmDoc: TADOStoredProc
    Connection = connMain
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'CancelConfirmDoc;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@DocId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CargoId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@UserId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 135
    Top = 200
  end
  object spImportExternalData: TADOStoredProc
    Connection = connMain
    ProcedureName = 'ImportExternalSheet;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@guid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@systemsection'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@folderid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@trainnum'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@prefix'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@startnumber'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@userid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@emptytype'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@loadtype'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 215
    Top = 128
  end
  object qrCounteragents: TADOQuery
    Connection = connMain
    Parameters = <>
    SQL.Strings = (
      'select * from counteragents')
    Left = 359
    Top = 128
  end
  object dsCounteragents: TDataSource
    DataSet = qrCounteragents
    Left = 359
    Top = 200
  end
  object spAdd2SelectList: TADOStoredProc
    Connection = connMain
    ProcedureName = 'Add2SelectList;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@guid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end>
    Left = 575
    Top = 136
  end
  object spClearSelectList: TADOStoredProc
    Connection = connMain
    ProcedureName = 'ClearSelectList;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@guid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end>
    Left = 599
    Top = 216
  end
  object spPrepareTransformDocuments: TADOStoredProc
    Connection = connMain
    ProcedureName = 'PrepareTransformDocument;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@selectguid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@end_doctype_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@userid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 343
    Top = 288
  end
  object spTransformDocuments: TADOStoredProc
    Connection = connMain
    ProcedureName = 'TransformDocument;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@routeid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@newdoctypeid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@folderid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@userid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 471
    Top = 288
  end
  object spGetPurposeDocumentTypes: TADOStoredProc
    Connection = connMain
    ProcedureName = 'GetPurposeDocumentTypes'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@selectguid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end>
    Left = 767
    Top = 288
  end
  object spMoveToFolder: TADOStoredProc
    Connection = connMain
    ProcedureName = 'MoveToFolder;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@selectguid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@folderid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 471
    Top = 352
  end
  object qrCarriages: TADOQuery
    Connection = connMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select c.*, k.footsize, c1.code as owner_code, c1.id as owner_id'
      'from objects o'
      'left outer join counteragents c1 on (c1.id = o.owner_id) , '
      'carriages c '
      'left outer join carriagekinds k on (k.id =  c.kind_id)'
      'where o.id = c.id and o.object_type = '#39'carriage'#39)
    Left = 455
    Top = 136
  end
  object dsCarriages: TDataSource
    DataSet = qrCarriages
    Left = 455
    Top = 208
  end
  object qrServices: TADOQuery
    Connection = connMain
    Parameters = <>
    SQL.Strings = (
      'select * from servicekinds')
    Left = 519
    Top = 24
  end
  object dsServices: TDataSource
    DataSet = qrServices
    Left = 519
    Top = 72
  end
  object spConfirmInspectDoc: TADOStoredProc
    Connection = connMain
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'ConfirmInspectDoc;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@DocId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 47
    Top = 280
  end
  object spCancelConfirmInspectDoc: TADOStoredProc
    Connection = connMain
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'CancelConfirmInspectDoc;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@DocId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 183
    Top = 280
  end
  object spCreateFilter: TADOStoredProc
    Connection = connMain
    ProcedureName = 'create_filter;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@userid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@doctypes'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@conditions'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end>
    Left = 55
    Top = 352
  end
  object spUpdateDeliveryType: TADOStoredProc
    Connection = connMain
    ProcedureName = 'UpdateDeliveryType;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TaskId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DlvTypeId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 191
    Top = 352
  end
  object spUnionCounteragents: TADOStoredProc
    Connection = connMain
    ProcedureName = 'UnionCounteragents;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@id1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@id2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 343
    Top = 352
  end
  object ssCounteragents: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select c.*, c.name+'#39', '#39'+isnull(c.inn,'#39#39') as codeinn from counter' +
        'agents c where (name like '#39'%@pattern%'#39' or code like '#39'%@pattern%'#39 +
        ') order by code')
    DownSql.Strings = (
      
        'select top 50 c.*, c.name+'#39', '#39'+isnull(c.inn,'#39#39') as codeinn from ' +
        'counteragents c order by code')
    InitSql.Strings = (
      
        'select c.*, c.name+'#39', '#39'+isnull(c.inn,'#39#39') as codeinn from counter' +
        'agents c where id = @id')
    KeyName = 'id'
    DisplayName = 'codeinn'
    Connection = connMain
    Left = 48
    Top = 464
  end
  object spUpdateRegisterContainer: TADOStoredProc
    Connection = connMain
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'UpdateRegisterContainer;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ObjectId'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cnum'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@KindId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Size'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@OwnerId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Note'
        Attributes = [paNullable]
        DataType = ftString
        Size = 450
        Value = Null
      end
      item
        Name = '@ParentObjectId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@containerweight'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@carrying'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@datemade'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@dateinspection'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@isdefective'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@datestiker'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@picture'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@acep'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@isnotready'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 591
    Top = 352
  end
  object ssCargoType: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select id, code+'#39' | '#39'+name as codename from cargotypes  where co' +
        'de like '#39'%@pattern%'#39' or name like '#39'%@pattern%'#39' order by code')
    DownSql.Strings = (
      
        'select top 50 id, code+'#39' | '#39'+name as codename from cargotypes wh' +
        'ere code like '#39'%@pattern%'#39' or name like '#39'%@pattern%'#39' order by co' +
        'de')
    InitSql.Strings = (
      
        'select id, code+'#39' | '#39'+name as codename from cargotypes  where id' +
        ' = @id')
    KeyName = 'id'
    DisplayName = 'codename'
    Connection = connMain
    Left = 200
    Top = 464
  end
  object spUpdateRegisterCargo: TADOStoredProc
    Connection = connMain
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'UpdateRegisterCargo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ObjectId'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cnum'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@TypeId'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@WeightSender'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@WeightDoc'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@WeightFact'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@SealNumber'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@IsEmpty'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Note'
        Attributes = [paNullable]
        DataType = ftString
        Size = 450
        Value = Null
      end
      item
        Name = '@ParentObjectId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 591
    Top = 432
  end
  object spRegisterCarriagesFromSheet: TADOStoredProc
    Connection = connMain
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'RegisterCarriagesFromSheet'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@g'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@dateincome'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 391
    Top = 432
  end
  object spImportCarLinks: TADOStoredProc
    Connection = connMain
    ProcedureName = 'ImportCarLinks;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@start'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@col1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@col2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@col3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@col4'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@col5'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@col6'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@col7'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end>
    Left = 383
    Top = 488
  end
  object spRegisterDataChange: TADOStoredProc
    Connection = connMain
    ProcedureName = 'RegisterDataChange;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 250
        Value = Null
      end
      item
        Name = '@TableId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EventType'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@UserId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 583
    Top = 496
  end
  object spSetParentCounteragent: TADOStoredProc
    Connection = connMain
    ProcedureName = 'SetParentCounteragent;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@selectguid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@ParentId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 775
    Top = 152
  end
  object spUpdateRegisterCarriageFull: TADOStoredProc
    Connection = connMain
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'UpdateRegisterCarriageFull;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cnum'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@KindCode'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@OwnerCode'
        Attributes = [paNullable]
        DataType = ftString
        Size = 150
        Value = Null
      end
      item
        Name = '@ModelCode'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@TechCondCode'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 775
    Top = 352
  end
  object spCreateCounteragent: TADOStoredProc
    Connection = connMain
    ProcedureName = 'CreateCounteragent'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@code'
        Attributes = [paNullable]
        DataType = ftString
        Size = 250
        Value = Null
      end>
    Left = 767
    Top = 232
  end
  object spGenerateUnloadDocs: TADOStoredProc
    Connection = connMain
    ProcedureName = 'GenerateUnloadDocs'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@selectguid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@loadplanid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@folderid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@userid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 255
    Top = 200
  end
  object spCopyCons2Payer: TADOStoredProc
    Connection = connMain
    ProcedureName = 'CopyCons2Payer'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@selectguid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end>
    Left = 319
    Top = 41
  end
  object spSetCarIncomeDate: TADOStoredProc
    Connection = connMain
    ProcedureName = 'SetCarIncomeDate;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@guid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@DateIncome'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@SheetNumber'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@TrainNum'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 647
    Top = 40
  end
  object spSetAlertDate: TADOStoredProc
    Connection = connMain
    ProcedureName = 'SetAlertDate;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@guid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        Name = '@alertdate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 727
    Top = 424
  end
  object spMakeAlertMessages: TADOStoredProc
    Connection = connMain
    ProcedureName = 'MakeAlertMessages'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@selectguid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end>
    Left = 767
    Top = 40
  end
  object spChangeCarriage: TADOStoredProc
    Connection = connMain
    ExecuteOptions = [eoExecuteNoRecords]
    ProcedureName = 'ChangeCarriage'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@car1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@car2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@planid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@setnew'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 751
    Top = 496
  end
  object spTransformDocumentAdd: TADOStoredProc
    Connection = connMain
    ProcedureName = 'TransformDocumentAdd;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@docid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@userid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 599
    Top = 288
  end
  object spAbolishDoc: TADOStoredProc
    Connection = connMain
    ProcedureName = 'AbolishDoc'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@DocId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@UserId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 823
    Top = 424
  end
  object spMoveToOrder: TADOStoredProc
    Connection = connMain
    ProcedureName = 'MoveToOrder;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@docid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@selectguid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end>
    Left = 479
    Top = 416
  end
  object ssContainerKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from containerkinds order by code')
    DownSql.Strings = (
      'select * from containerkinds order by code')
    InitSql.Strings = (
      'select * from containerkinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = connMain
    Left = 131
    Top = 464
  end
  object spCheckNonConfirmedChildDocs: TADOStoredProc
    Connection = connMain
    ProcedureName = 'CheckNonConfirmedChildDocs'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@selectguid'
        Attributes = [paNullable]
        DataType = ftString
        Size = 40
        Value = Null
      end>
    Left = 679
    Top = 120
  end
  object spGetDocumentTypeFromPassOp: TADOStoredProc
    Connection = connMain
    ProcedureName = 'GetDocumentTypeFromPassOp'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@passopid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@doctypeid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 919
    Top = 296
  end
  object spPrepareTransformFromPassOp: TADOStoredProc
    Connection = connMain
    ProcedureName = 'PrepareTransformFromPassOp'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@passopid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@docrouteid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@userid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 919
    Top = 360
  end
end
