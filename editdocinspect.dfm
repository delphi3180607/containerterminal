﻿inherited FormEditInspect: TFormEditInspect
  Caption = #1058#1077#1093#1086#1089#1084#1086#1090#1088
  ClientHeight = 189
  ClientWidth = 516
  ExplicitWidth = 522
  ExplicitHeight = 217
  PixelsPerInch = 96
  TextHeight = 16
  object Label11: TLabel [0]
    Left = 8
    Top = 5
    Width = 42
    Height = 16
    Caption = #1053#1086#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel [1]
    Left = 173
    Top = 5
    Width = 33
    Height = 16
    Caption = #1044#1072#1090#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel [2]
    Left = 335
    Top = 5
    Width = 166
    Height = 16
    Caption = #1044#1072#1090#1072' '#1074#1088#1077#1084#1103' '#1079#1072#1074#1077#1088#1096#1077#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbPersons: TSpeedButton [3]
    Left = 362
    Top = 85
    Width = 52
    Height = 24
    Caption = '...'
    OnClick = sbPersonsClick
  end
  object Label9: TLabel [4]
    Left = 8
    Top = 66
    Width = 104
    Height = 16
    Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 148
    Width = 516
    TabOrder = 1
    ExplicitTop = 148
    ExplicitWidth = 516
    inherited btnCancel: TButton
      Left = 400
      ExplicitLeft = 400
    end
    inherited btnOk: TButton
      Left = 281
      ExplicitLeft = 281
    end
  end
  object cbDocNumber: TDBEditEh [6]
    Left = 8
    Top = 27
    Width = 153
    Height = 24
    DataField = 'doc_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object cbDocDate: TDBDateTimeEditEh [7]
    Left = 183
    Top = 27
    Width = 146
    Height = 24
    DataField = 'doc_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object cdDateEnd: TDBDateTimeEditEh [8]
    Left = 335
    Top = 27
    Width = 166
    Height = 24
    DataField = 'fact_datetime'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 4
    Visible = True
  end
  object luResponsible: TDBLookupComboboxEh [9]
    Left = 8
    Top = 85
    Width = 348
    Height = 24
    DynProps = <>
    DataField = 'person_id'
    DataSource = dsLocal
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    KeyField = 'id'
    ListField = 'person_name'
    ListSource = FormPersons.dsData
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 480
    Top = 80
  end
  inherited qrAux: TADOQuery
    Left = 416
    Top = 80
  end
end
