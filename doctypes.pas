﻿unit doctypes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, Vcl.ComCtrls, functions,
  EXLReportExcelTLB, EXLReportBand, EXLReport, Vcl.Mask, DBCtrlsEh;

type
  TFormDocTypes = class(TFormGrid)
    Splitter1: TSplitter;
    Panel1: TPanel;
    sbDeleteSpec: TPngSpeedButton;
    sbExcelSpec: TPngSpeedButton;
    sbAddSpec: TPngSpeedButton;
    sbEditSpec: TPngSpeedButton;
    Bevel1: TBevel;
    dgSpec: TDBGridEh;
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    pmSpec: TPopupMenu;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    procedure sbAddSpecClick(Sender: TObject);
    procedure sbEditSpecClick(Sender: TObject);
    procedure sbDeleteSpecClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormDocTypes: TFormDocTypes;

implementation

{$R *.dfm}

uses editdoctype, EditDocTemplate;

procedure TFormDocTypes.Init;
begin
  inherited;
  meSpec.Open;
  tablename := 'doctypes';
  self.formEdit := FormEditDocType;
end;

procedure TFormDocTypes.sbAddSpecClick(Sender: TObject);
begin
  dgSpec.SetFocus;
  EA(self, FormEditDocTemplate, meSpec, 'doctemplates', 'doctype_id', meData.FieldByName('id').AsInteger);
end;

procedure TFormDocTypes.sbDeleteSpecClick(Sender: TObject);
begin
  ED(meSpec);
end;

procedure TFormDocTypes.sbEditSpecClick(Sender: TObject);
begin
  dgSpec.SetFocus;
  EE(self, FormEditDocTemplate, meSpec, 'doctemplates');
end;

end.
