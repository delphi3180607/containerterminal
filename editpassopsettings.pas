﻿unit EditPassOpSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  DBSQLLookUp, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEditPassOpSettings = class(TFormEdit)
    ssStateKind: TADOLookUpSqlSet;
    ssRealStateKind: TADOLookUpSqlSet;
    leRealStartstate: TDBSQLLookUp;
    leStartState: TDBSQLLookUp;
    laDlvTypes: TDBSQLLookUp;
    ssDlvTypes: TADOLookUpSqlSet;
    leSpareKind: TDBSQLLookUp;
    ssSpareKind: TADOLookUpSqlSet;
    cbEmpty: TDBCheckBoxEh;
    edSealNumber: TDBEditEh;
    cbIsMain: TDBCheckBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditPassOpSettings: TFormEditPassOpSettings;

implementation

{$R *.dfm}

end.
