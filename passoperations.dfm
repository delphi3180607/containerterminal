﻿inherited FormPassOperations: TFormPassOperations
  Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1087#1086' '#1087#1088#1086#1087#1091#1089#1082#1072#1084
  ClientHeight = 750
  ClientWidth = 1062
  Font.Height = -13
  ExplicitWidth = 1078
  ExplicitHeight = 788
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 709
    Width = 1062
    ExplicitTop = 709
    ExplicitWidth = 1062
    inherited btnOk: TButton
      Left = 827
      ExplicitLeft = 827
    end
    inherited btnCancel: TButton
      Left = 946
      ExplicitLeft = 946
    end
  end
  inherited plAll: TPanel
    Width = 1062
    Height = 709
    ExplicitWidth = 1062
    ExplicitHeight = 709
    object Splitter1: TSplitter [0]
      Left = 0
      Top = 427
      Width = 1062
      Height = 4
      Cursor = crVSplit
      Align = alBottom
      Color = 15132390
      ParentColor = False
      StyleElements = [seFont, seBorder]
      ExplicitTop = 269
      ExplicitWidth = 820
    end
    inherited plTop: TPanel
      Width = 1062
      ExplicitWidth = 1062
      inherited btTool: TPngSpeedButton
        Left = 1024
        ExplicitLeft = 1024
      end
      inherited plCount: TPanel
        Left = 827
        ExplicitLeft = 827
        inherited edCount: TDBEditEh
          ControlLabel.Width = 119
          ControlLabel.Height = 16
          ControlLabel.ExplicitTop = 6
          ControlLabel.ExplicitWidth = 119
          ControlLabel.ExplicitHeight = 16
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 1062
      Height = 398
      Font.Height = -13
      Font.Name = 'Calibri'
      TitleParams.Font.Height = -13
      TitleParams.Font.Name = 'Calibri'
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076' '#1086#1087#1077#1088#1072#1094#1080#1080
          Width = 133
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1054#1087#1077#1088#1072#1094#1080#1103
          Width = 376
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'short_code'
          Footers = <>
          Title.Caption = #1050#1086#1076' '#1076#1083#1103' '#1050#1055#1055
          Width = 115
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'direction_code'
          Footers = <>
          Title.Caption = #1053#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077
          Width = 102
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'end_doctype_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 188
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'system_section'
          Footers = <>
          Title.Caption = #1057#1080#1089#1090#1077#1084#1085#1099#1081' '#1088#1072#1079#1076#1077#1083
          Width = 160
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dlv_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
          Width = 100
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object plSpec: TPanel
      Left = 0
      Top = 431
      Width = 1062
      Height = 278
      Align = alBottom
      Color = 15921906
      ParentBackground = False
      TabOrder = 3
      object plToolSpec: TPanel
        Left = 1
        Top = 1
        Width = 1060
        Height = 27
        Align = alTop
        BevelOuter = bvNone
        Color = 15921906
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 28637
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        StyleElements = [seBorder]
        object sbAddSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 0
          Top = 0
          Width = 32
          Height = 27
          Hint = #1044#1086#1073#1072#1074#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbAddSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000B1200000B
            1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000002114944415478DA8D93CB4F135114C6
            BF33337D49A48D89AE5CF917D89DD4477C048B6B11790C2DE84277FE256ABA22
            6A5163BAD0A87B25C698311A6A03F5454B35A12ED45625406941B0A5E3D7B960
            54BA989B99B933F79CFB3BE77CE78EA0CDE83A3B9610D1C21041EB1635675FA4
            CE5FFADF57DA0122FDD7ADF8B9E83F6BA9D4533CBF337AC41D603069C547BAF1
            3EF703B5B506BC9D01E45FBD81753BE60E70C81CB786E3DD7899FE8CCA4A1DFE
            5D1DF89A2FC0BA39E40E703876CB32874F6032F3054B2B0DF8082813F02C39B0
            1DA004D3C3D0345E148C33341DA67914E9A9122AAB048476A094FF406F4D4594
            ADB8F24422FDD728588FF36943944D945366BA8CCACF06F6EC0D62FFBEE05F1B
            D5B87B2F0D890C50F1D128DE51B06A751DA23313DEAD4C6ADC6CD3D1F019E8EC
            F0FE69A918F4E15A21334BC0E00D2B367212934C7771797DB304C106776EF0A1
            E93A0CBF078647834E9BE89B657A75CC178A9083434947F1ECC7252CD6EA58AB
            37D160D4DD213F96175661530F5F28E00034BE3B3AD1DEB46D9467E60830C713
            9AA6858546380E2AFDDEBE08326F790EEA367CC100BEE78B2AF256194A8672DB
            361EBB70DFEA3DD385A99979D47ED94E17BEE5E63071F594BB7370BC05E85300
            9E23D546A63B71A5C725E0E203667000D3B30B4E09FE9D019472453CBE1C750F
            384DC0EB62952534E1F11AD4E0131EB9073C4C50B0704B50B08DA2C4CD12B0ED
            77FE0D6936C4A0656B515C0000000049454E44AE426082}
          ExplicitLeft = 1
          ExplicitTop = 1
          ExplicitHeight = 25
        end
        object sbDeleteSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 32
          Top = 0
          Width = 35
          Height = 27
          Hint = #1059#1076#1072#1083#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          OnClick = sbDeleteSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002554944415478DA95924D68D36018C79F7C746D92A6B5DD
            86B2B56C7810A141F0E8459C7810BC09A2CECFA907F13211C7BC7A134410069E
            046FC26EE2556D657EEC24F5D04C2F62B77569BA43ED47DA244BF2C6E74D6CB1
            743BEC8524BCE4FDFD9E27FF278CEFFBB09F65CFDD11A3AF5E767B7B6640B0F0
            680AEF7178FA44DD15BE793BE7BBCE19E2B84571F9F5CAA080C21C57A67BDFB6
            15F6F9B301897DE3560EDF9798E808ECD4B69751F4587EFBE64728F80743340A
            54E7B7DB402C4BE15F2C0512EBFA5C8E61D9129790814F26C1DEAC80A5EBF789
            E32C8582878B319F900E0802CBC83290560B48A3811253C17681C22CC2914412
            ACAD0AD855DD435848E5DF39FD4F20F30F62BEE7198C20702C1EF49A0D70FFD4
            C1470187D248320156658B56F6B0FD782AFFDE1A0AD1BD7B2F465CCF604591E3
            5307003C82667A795899C2B5102E7CB0769F4218164ADC8E3035CDC2C63A4A3C
            80CC24348ADF09711D29FD316FFD7F7E4860CE5EC3C098526C3203502E871DA0
            A0B9B6064EDB50C6BE7E52F7149897AFE600615E94303019613F689F4AAC5A0D
            4C5D879D765B3958FCA60E09BA97AE04954338016655A3104D9B89A5D2AC3471
            08BA551DBA9A86124399F8A9AA7D41F7E26C30675E14118E83A9E9606D53D88D
            6368804F43184D737236031DAD0A1D0C947692F9FD4B0D04C6F90B336C84CF47
            24095CA30326C238BEF8E897952030FDD8711CB16B48E3E31C3D63D089D4EBA7
            B31BE542FF139A67CFA12492775A4D8215250C6B206DEDC8D1E03F199165AE07
            0F85583F796A06E1D5B1D5CF03706F55A60F53C989ECE67A61CF31EE77FD05EB
            B7706A5FA737EA0000000049454E44AE426082}
          ExplicitLeft = 33
          ExplicitTop = 1
          ExplicitHeight = 25
        end
        object sbEditSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 67
          Top = 0
          Width = 31
          Height = 27
          Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbEditSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
            62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
            08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
            4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
            C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
            892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
            4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
            0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
            C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
            8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
            346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
            5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
            3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
            378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
            C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
            1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
            FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
            F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
            0049454E44AE426082}
          ExplicitLeft = 68
          ExplicitTop = 1
          ExplicitHeight = 25
        end
        object Bevel8: TBevel
          AlignWithMargins = True
          Left = 132
          Top = 2
          Width = 1
          Height = 23
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alLeft
          ExplicitLeft = 502
          ExplicitHeight = 43
        end
        object sbExcelSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 98
          Top = 0
          Width = 32
          Height = 25
          Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 2
          Align = alLeft
          Flat = True
          ParentShowHint = False
          ShowHint = True
          OnClick = btExcelClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C0000031E4944415478DA8D936B6C0C5114C7FF7776B7BB53CF6E
            29F508A11E4D89E8AA846AB32869D89520A1691B1111AFB6D2342A21AA443C2A
            F1F8209E1104ED1791F824E852DD967835969616F5A6655955DD99E9ECEC5C67
            6737F5D5C9993B7327F7FCCEB9FF7B2EE39C63C1CEFAD5E2406B81C0E00060A3
            8701B191D3C07568BADEDEF5937B58F7AF72EF09978A98B10860D1BE46EFC512
            47AA20B0C11C3081A3CFB8F1CDE1EF09A1FA71173A3FF64A2F9A5BED0D275CBD
            7D0057D5BDEF359B33EC2DDF61121833325352842399350E39A4C139A11F6ADB
            653C68F5A3E3A32ABF6A6B4BB87B6C71AF01701FBC2F5D2E76886F02268AE546
            013A0D615D874A00450D237B820D2D7E0E8DA0B77C9DB8ED6DAFF1EC73E6B3BC
            43F3DFEADC3E7670BCCEC2B4DFF4143702C11FB8E3BB6080AC96FE04D2B0383D
            1F85D9EB696E86A4A8283CF244BBB93BCB12015C59B7B06CB96CEDC0FB402B9A
            9EBC27C1240C4D48841AD620493274D8B16CF67E30533C74AA2A6B9C152B0E3F
            566E54648A118035A44F09962F596A7AADD6A323E0475B7307C24C8699C5C16C
            4A42C1BC43548D19210A26C78C5166AC3C42801DB3C4E8295479E5C949E76C05
            D96BF1E8E73574FD09C3E77B0A5B9C1DC5EEF3246A1C42B44E0B933EF44E1B2E
            208F2AB8591103382B77A98A7ADE32313915A5AE4A5CFF7411BA64C5CBD6E7C8
            759460DAF845241E10D2A202A724C200782A09E0281FB18E529CCA4C73222005
            C0551D65EE5DB8D4761C66251E9FDEB5E375E74B9C2EF94027C3490F20350970
            EF7DA8D4ED996D003ED3BF91916689F898A43424278C465E663EEE7EF1E06BD7
            37A87E091B5C97E934B8D198D39205E4EC68501AAAB2A35B985BD128576F71D8
            FC3D16A3FB7E07BFE16AE31E04955F604CC0003111AB728EF67567CA5060EE56
            AFF2E0A8330AC8DA562FD794CFB405640BFEC7C627524C699DD27432270A9855
            5617ACD93E53ECEEB5B2FF014C1AC6F48C8DB5D2B3330B071880199B3C6FAF1F
            98332418E2FD682E44B4D0638B0789E6BE4B255A04DA1274314EF83375CD8D2F
            2DE772D30C4046516D9116E24B1963D349A6F87F57D1F0D8ED88CEC914EAC626
            01A8F69DCD3DFB17534092F0545E8BC50000000049454E44AE426082}
          ExplicitLeft = 95
          ExplicitTop = 2
        end
      end
      object dgSpec: TDBGridEh
        Left = 1
        Top = 28
        Width = 1060
        Height = 249
        Align = alClient
        AllowedOperations = []
        Border.Color = 11645361
        ColumnDefValues.Title.TitleButton = True
        Ctl3D = False
        DataSource = dsSpec
        DynProps = <>
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
        GridLineParams.BrightColor = 15395041
        GridLineParams.DarkColor = 15395041
        GridLineParams.DataBoundaryColor = 15395041
        GridLineParams.DataHorzColor = clSilver
        GridLineParams.DataVertColor = clSilver
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
        IndicatorParams.HorzLineColor = clSilver
        IndicatorParams.VertLineColor = clSilver
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
        ParentCtl3D = False
        ParentFont = False
        ParentShowHint = False
        SelectionDrawParams.SelectionStyle = gsdsClassicEh
        SelectionDrawParams.DrawFocusFrame = False
        SelectionDrawParams.DrawFocusFrameStored = True
        ShowHint = True
        SortLocal = True
        STFilter.Color = 15725813
        STFilter.Font.Charset = DEFAULT_CHARSET
        STFilter.Font.Color = clWindowText
        STFilter.Font.Height = -11
        STFilter.Font.Name = 'Verdana'
        STFilter.Font.Style = []
        STFilter.HorzLineColor = clSilver
        STFilter.Local = True
        STFilter.ParentFont = False
        STFilter.VertLineColor = clSilver
        STFilter.Visible = True
        TabOrder = 1
        TitleParams.Color = clBtnFace
        TitleParams.FillStyle = cfstThemedEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -13
        TitleParams.Font.Name = 'Calibri'
        TitleParams.Font.Style = [fsBold]
        TitleParams.HorzLineColor = 11645361
        TitleParams.MultiTitle = True
        TitleParams.ParentFont = False
        TitleParams.SecondColor = 15987699
        TitleParams.SortMarkerStyle = smstDefaultEh
        TitleParams.VertLineColor = 13421772
        OnMouseDown = dgDataMouseDown
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'dlv_code'
            Footers = <>
            Title.Caption = #1042#1080#1076' '#1076#1086#1089#1090#1072#1074#1082#1080
            Width = 188
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'start_state_code'
            Footers = <>
            Title.Caption = #1048#1089#1093#1086#1076#1085#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 170
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'real_state_code'
            Footers = <>
            Title.Caption = #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 162
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isempty'
            Footers = <>
            Title.Caption = #1055#1086#1088#1086#1078#1085#1080#1081' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
            Width = 106
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'seal_number'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
            Width = 84
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'spare_kind_code'
            Footers = <>
            Title.Caption = #1055#1086#1076#1095#1080#1085#1077#1085#1085#1072#1103' '#1087#1072#1088#1085#1072#1103' '#1086#1087#1077#1088#1072#1094#1080#1103' '
            Width = 197
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'ismain'
            Footers = <>
            Title.Caption = #1043#1083#1072#1074#1085#1072#1103
            Width = 60
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select ok.*,  '
      
        '(case when direction=0 then '#39#1047#1072#1074#1086#1079#39' else '#39#1042#1099#1074#1086#1079#39' end) as directi' +
        'on_code,'
      
        '(select code from doctypes where id = ok.end_doctype_id) as end_' +
        'doctype_code,'
      
        '(select code from deliverytypes d where d.id = ok.end_dlv_type_i' +
        'd) as dlv_code'
      'from passoperationkinds ok order by name')
    UpdateCommand.CommandText.Strings = (
      'update passoperationkinds'
      'set'
      '  code = :code,'
      '  short_code = :short_code,'
      '  name = :name,'
      ' end_doctype_id = :end_doctype_id,'
      ' system_section = :system_section,'
      ' direction = :direction,'
      ' end_dlv_type_id = :end_dlv_type_id'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'code'
        Size = -1
        Value = Null
      end
      item
        Name = 'short_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'name'
        Size = -1
        Value = Null
      end
      item
        Name = 'end_doctype_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'system_section'
        Size = -1
        Value = Null
      end
      item
        Name = 'direction'
        Size = -1
        Value = Null
      end
      item
        Name = 'end_dlv_type_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into passoperationkinds'
      
        '  (code, short_code, name, end_doctype_id, system_section, direc' +
        'tion, end_dlv_type_id)'
      'values'
      
        '  (:code, :short_code, :name, :end_doctype_id, :system_section, ' +
        ':direction, :end_dlv_type_id)')
    InsertCommand.Parameters = <
      item
        Name = 'code'
        Size = -1
        Value = Null
      end
      item
        Name = 'short_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'name'
        Size = -1
        Value = Null
      end
      item
        Name = 'end_doctype_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'system_section'
        Size = -1
        Value = Null
      end
      item
        Name = 'direction'
        Size = -1
        Value = Null
      end
      item
        Name = 'end_dlv_type_id'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from passoperationkinds where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select ok.*,  '
      
        '(case when direction=0 then '#39#1047#1072#1074#1086#1079#39' else '#39#1042#1099#1074#1086#1079#39' end) as directi' +
        'on_code,'
      
        '(select code from doctypes where id = ok.end_doctype_id) as end_' +
        'doctype_code,'
      
        '(select code from deliverytypes d where d.id = ok.end_dlv_type_i' +
        'd) as dlv_code'
      'from passoperationkinds ok  where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select *, '
      
        '(select code from deliverytypes dt where dt.id = os.dlv_type_id)' +
        ' as dlv_code, '
      
        '(select code from objectstatekinds sk where sk.id = os.start_sta' +
        'te_id) as start_state_code, '
      
        '(select code from objectstatekinds sk1 where sk1.id = os.real_st' +
        'ate_id) as real_state_code, '
      
        '(select code from passoperationkinds ok where ok.id = os.spare_k' +
        'ind_id) as spare_kind_code '
      'from passopsettings os'
      'where kind_id = :kind_id'
      'order by 1,2')
    SelectCommand.Parameters = <
      item
        Name = 'kind_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update passopsettings'
      'set'
      '  kind_id = :kind_id,'
      '  dlv_type_id = :dlv_type_id,'
      '  start_state_id = :start_state_id,'
      '  real_state_id = :real_state_id,'
      '  spare_kind_id = :spare_kind_id,'
      '  seal_number = :seal_number,'
      '  isempty = :isempty, '
      '  ismain = :ismain'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'kind_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'start_state_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'real_state_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'spare_kind_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'seal_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'isempty'
        Size = -1
        Value = Null
      end
      item
        Name = 'ismain'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into passopsettings'
      
        '  (kind_id, dlv_type_id, start_state_id, real_state_id, spare_ki' +
        'nd_id, seal_number, isempty, ismain)'
      'values'
      
        '  (:kind_id, :dlv_type_id, :start_state_id, :real_state_id, :spa' +
        're_kind_id, :seal_number, :isempty, :ismain)')
    InsertCommand.Parameters = <
      item
        Name = 'kind_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'start_state_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'real_state_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'spare_kind_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'seal_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'isempty'
        Size = -1
        Value = Null
      end
      item
        Name = 'ismain'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from passopsettings where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select '
      
        '(select code from deliverytypes dt where dt.id = os.dlv_type_id)' +
        ' as dlv_code, '
      
        '(select code from objectstatekinds sk where sk.id = os.start_sta' +
        'te_id) as start_state_code, '
      
        '(select code from objectstatekinds sk1 where sk1.id = os.real_st' +
        'ate_id) as real_state_code, '
      
        '(select code from passoperationkinds ok where ok.id = os.spare_k' +
        'ind_id) as spare_kind_code '
      'from passopsettings os where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 240
    Top = 376
  end
  object meSpec: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvSpec
    Left = 288
    Top = 376
  end
  object dsSpec: TDataSource
    DataSet = meSpec
    Left = 344
    Top = 376
  end
end
