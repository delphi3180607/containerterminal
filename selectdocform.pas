﻿unit SelectDocForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls,
  DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh;

type
  TFormSelectDocForm = class(TFormEdit)
    DBGridEh1: TDBGridEh;
    qrTemplates: TADOQuery;
    dsTemplates: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSelectDocForm: TFormSelectDocForm;

implementation

{$R *.dfm}

end.
