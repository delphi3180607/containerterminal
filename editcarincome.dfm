﻿inherited FormEditCarIncome: TFormEditCarIncome
  Caption = #1055#1086#1076#1072#1095#1072' '#1074#1072#1075#1086#1085#1086#1074
  ClientHeight = 178
  ClientWidth = 466
  ExplicitWidth = 472
  ExplicitHeight = 206
  PixelsPerInch = 96
  TextHeight = 16
  object Label6: TLabel [0]
    Left = 8
    Top = 17
    Width = 133
    Height = 16
    Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1087#1086#1076#1072#1095#1080
  end
  object Label7: TLabel [1]
    Left = 176
    Top = 17
    Width = 124
    Height = 16
    Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086#1076#1072#1095#1080
  end
  object Label1: TLabel [2]
    Left = 8
    Top = 81
    Width = 64
    Height = 16
    Caption = #1053#1086#1084#1077#1088' '#1050#1055
  end
  object Label2: TLabel [3]
    Left = 168
    Top = 104
    Width = 279
    Height = 16
    Caption = #1045#1089#1083#1080' '#1087#1091#1089#1090#1086', '#1090#1086' '#1086#1089#1090#1072#1085#1077#1090#1089#1103' '#1085#1077#1080#1079#1084#1077#1085#1077#1085#1085#1099#1084
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    StyleElements = [seClient, seBorder]
  end
  inherited plBottom: TPanel
    Top = 137
    Width = 466
    TabOrder = 3
    ExplicitTop = 137
    ExplicitWidth = 466
    inherited btnCancel: TButton
      Left = 350
      ExplicitLeft = 350
    end
    inherited btnOk: TButton
      Left = 231
      ExplicitLeft = 231
    end
  end
  object dtServeDate: TDBDateTimeEditEh [5]
    Left = 7
    Top = 37
    Width = 152
    Height = 24
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
    EditFormat = 'DD/MM/YYYY HH:NN:SS'
  end
  object edServeSheet: TDBEditEh [6]
    Left = 176
    Top = 37
    Width = 147
    Height = 24
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object edKP: TDBEditEh [7]
    Left = 8
    Top = 101
    Width = 147
    Height = 24
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  inherited dsLocal: TDataSource
    Top = 24
  end
  inherited qrAux: TADOQuery
    Left = 400
    Top = 24
  end
end
