﻿unit docemptyincome;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, docflow, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.Mask, DBCtrlsEh, System.ImageList, Vcl.ImgList, PngImageList;

type
  TFormDocEmptyIncome = class(TFormDocFlow)
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    plSpec: TPanel;
    plToolSpec: TPanel;
    sbDeleteSpec: TPngSpeedButton;
    sbEditSpec: TPngSpeedButton;
    dgSpec: TDBGridEh;
    sbAddSpec: TPngSpeedButton;
    pmSpec: TPopupMenu;
    N15: TMenuItem;
    Splitter1: TSplitter;
    sbExcelSpec: TPngSpeedButton;
    sbCancelSpec: TPngSpeedButton;
    sbFilter: TPngSpeedButton;
    sbResetFilter: TPngSpeedButton;
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure sbEditSpecClick(Sender: TObject);
    procedure sbConfirmClick(Sender: TObject);
    procedure sbCancelConfirmClick(Sender: TObject);
    procedure btFlowClick(Sender: TObject);
    procedure sbAddSpecClick(Sender: TObject);
    procedure meSpecAfterInsert(DataSet: TDataSet);
    procedure meSpecBeforeEdit(DataSet: TDataSet);
    procedure sbDeleteSpecClick(Sender: TObject);
    procedure meDataAfterEdit(DataSet: TDataSet);
    procedure meDataAfterInsert(DataSet: TDataSet);
    procedure N15Click(Sender: TObject);
    procedure meSpecBeforeInsert(DataSet: TDataSet);
    procedure meDataBeforePost(DataSet: TDataSet);
    procedure dgDataApplyFilter(Sender: TObject);
    procedure sbExcelSpecClick(Sender: TObject);
    procedure sbCancelSpecClick(Sender: TObject);
    procedure meDataAfterOpen(DataSet: TDataSet);
    procedure dgSpecTitleBtnClick(Sender: TObject; ACol: Integer;
      Column: TColumnEh);
    procedure sbFilterClick(Sender: TObject);
    procedure sbResetFilterClick(Sender: TObject);
    procedure meSpecBeforeOpen(DataSet: TDataSet);
  private
    specguid: string;
    procedure ResetSpecFilter;
    procedure SetSpecFilter;
  public
    procedure Init; override;
    procedure PartialInit; override;
  end;

var
  FormDocEmptyIncome: TFormDocEmptyIncome;

implementation

{$R *.dfm}

uses EditDocEmptyIncome, functions, editdocincomespec, ObjectStatesHistory,
  main, dmu, filterlite;

procedure TFormDocEmptyIncome.btFlowClick(Sender: TObject);
begin
  inherited;
  meDataAfterScroll(meData);
end;

procedure TFormDocEmptyIncome.dgDataApplyFilter(Sender: TObject);
begin
  inherited;
  meDataAfterScroll(meData);
end;

procedure TFormDocEmptyIncome.dgSpecTitleBtnClick(Sender: TObject;
  ACol: Integer; Column: TColumnEh);
begin
  if Column.Title.SortMarker = smNoneEh then
    Column.Title.SortMarker := smDownEh
  else if Column.Title.SortMarker = smDownEh then
    Column.Title.SortMarker := smUpEh
  else if Column.Title.SortMarker = smUpEh then
    Column.Title.SortMarker := smNoneEh;
  RestoreSort(dgSpec);
end;

procedure TFormDocEmptyIncome.Init;
begin
 self.system_section := 'income_empty';
 self.formEdit := FormEditDocEmptyIncome;
 inherited;
 meSpec.Open;
end;

procedure TFormDocEmptyIncome.PartialInit;
begin
 meData.Close;
 meData.Open;
end;


procedure TFormDocEmptyIncome.meDataAfterEdit(DataSet: TDataSet);
begin
  inherited;
  if meData.FieldByName('isbased').AsBoolean
  then
    meData.FieldByName('dlv_type_id').ReadOnly := true
  else
    meData.FieldByName('dlv_type_id').ReadOnly := false;
end;

procedure TFormDocEmptyIncome.meDataAfterInsert(DataSet: TDataSet);
begin
  inherited;
  meData.FieldByName('dlv_type_id').ReadOnly := false;
end;

procedure TFormDocEmptyIncome.meDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  meDataAfterScroll(DataSet);
end;

procedure TFormDocEmptyIncome.meDataAfterScroll(DataSet: TDataSet);
begin

  inherited;

  meSpec.Close;
  meSpec.Open;

  meSpec.ReadOnly := false;

  if (meData.FieldByName('isconfirmed').AsInteger = 1) then meSpec.ReadOnly := true;

  if meData.FieldByName('isdeleted').AsBoolean then meSpec.ReadOnly := true;

  if pos(self.system_section,FormMain.readonly_sections)>0 then meSpec.ReadOnly := true;

end;

procedure TFormDocEmptyIncome.meDataBeforePost(DataSet: TDataSet);
begin
  if meData.FieldByName('isbased').AsBoolean then
  begin
    self.active_doctype_id := meData.FieldByName('doctype_id').AsInteger;
  end else
  begin
    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select dt.id from doctypes dt, deliverytypes dl where dt.system_section = '''+self.system_section+'''');
    qrAux.SQL.Add('and  charindex(dl.global_section, dt.global_section)>0 and dl.id = '+meData.FieldByName('dlv_type_id').AsString);
    qrAux.Open;
    self.active_doctype_id := qrAux.FieldByName('id').AsInteger;
  end;
  inherited;
end;

procedure TFormDocEmptyIncome.meSpecAfterInsert(DataSet: TDataSet);
begin
  inherited;
  meSpec.FieldByName('doc_id').Value := meData.FieldByName('id').AsInteger;
end;

procedure TFormDocEmptyIncome.meSpecBeforeEdit(DataSet: TDataSet);
begin
  inherited;

  if not (meData.FieldByName('isconfirmed').AsInteger = 1) then
     meSpec.ReadOnly := false
   else
     meSpec.ReadOnly := true;

  if meData.FieldByName('isdeleted').AsBoolean then meSpec.ReadOnly := true;

	meSpec.FieldByName('cargotype_id').Required := not meSpec.FieldByName('isempty').AsBoolean;

end;

procedure TFormDocEmptyIncome.meSpecBeforeInsert(DataSet: TDataSet);
begin
  meSpec.FieldByName('cargotype_id').Required := not meSpec.FieldByName('isempty').AsBoolean;
end;

procedure TFormDocEmptyIncome.meSpecBeforeOpen(DataSet: TDataSet);
begin
  drvSpec.SelectCommand.Parameters.ParamByName('doc_id').Value := meData.FieldByName('id').AsInteger;
  drvSpec.SelectCommand.Parameters.ParamByName('guid').Value := specguid;
end;

procedure TFormDocEmptyIncome.N15Click(Sender: TObject);
begin
  FormObjectStatesHistory.meLinkedObjects.Close;
  FormObjectStatesHistory.meData.Close;
  FormObjectStatesHistory.drvData.SelectCommand.Parameters.ParamByName('object_id').Value := meSpec.FieldByName('container_id').AsInteger;
  FormObjectStatesHistory.meData.Open;
  FormObjectStatesHistory.ShowModal;
  Screen.Cursor := crDefault;
end;

procedure TFormDocEmptyIncome.sbAddSpecClick(Sender: TObject);
begin

  if meData.FieldByName('isbased').AsBoolean then
  begin
    ShowMessage('Нельзя добавлять позиции в Акты, созданные на основании заявок.');
    exit;
  end;

  if meData.FieldByName('isdeleted').AsBoolean then exit;

  if pos(self.system_section,FormMain.readonly_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  if not (meData.FieldByName('isconfirmed').AsInteger = 1) then
  begin
    meSpec.ReadOnly := false;
    EA(nil, FormEditIncomeSpec, meSpec, 'docincomespec');
  end;
end;

procedure TFormDocEmptyIncome.sbCancelConfirmClick(Sender: TObject);
begin
  inherited;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormDocEmptyIncome.sbCancelSpecClick(Sender: TObject);
begin

  if not FormMain.isAdmin then exit;

  if meSpec.RecordCount < 1 then exit;

  RefreshRecord(meData);
  if meData.FieldByName('isconfirmed').AsInteger = 0 then
  begin
    ShowMessage('Документ не проведен.');
    exit;
  end;

  if meSpec.RecordsView.MemTableData.RecordsList.Count < 2 then
  begin
    ShowMessage('Снять проведение с одной записи нельзя. Нужно распроводить документ.');
    exit;
  end;

  if fQYN('Cнять проведение с записи?') then
  begin
    try
      dm.spCancelConfirmDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
      dm.spCancelConfirmDoc.Parameters.ParamByName('@CargoId').Value := self.meSpec.FieldByName('cargo_id').AsInteger;
      dm.spCancelConfirmDoc.Parameters.ParamByName('@UserId').Value := FormMain.currentUserId;
      dm.spCancelConfirmDoc.ExecProc;
    finally
      self.meDataAfterScroll(meData);
    end;
  end;

end;

procedure TFormDocEmptyIncome.sbConfirmClick(Sender: TObject);
begin
  inherited;
  self.meDataAfterScroll(meData);
  if meLinkedObjects.RecordCount = 0 then
  begin
    ShowMessage('Внимание! Проведение документа не имело результата. Вероятно нарушен порядок работы.'+#13+#10'Необходимо заново создать и провести документ.'+#13+#10+'Данный документ должен быть удален.');
  end;
end;

procedure TFormDocEmptyIncome.sbDeleteSpecClick(Sender: TObject);
begin

  if (meData.FieldByName('isbased').AsBoolean) and (meData.FieldByName('isconfirmed').AsInteger = 0) then
  begin
    ShowMessage('Нельзя добавлять удалять позиции в Актах, созданных на основании заявок.');
    exit;
  end;

  if meData.FieldByName('isdeleted').AsBoolean then exit;

  if pos(self.system_section,FormMain.readonly_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  if (meData.FieldByName('isconfirmed').AsInteger = 0) or (meSpec.FieldByName('state_code').AsString='') then
  ED(meSpec);

end;

procedure TFormDocEmptyIncome.sbEditSpecClick(Sender: TObject);
begin

  if pos(self.system_section,FormMain.readonly_sections)>0 then
    meSpec.ReadOnly := true;

  if meSpec.RecordCount>0 then
  EE(nil, FormEditIncomeSpec, meSpec, 'docincomespec')
  else ShowMessage('Нечего редактировать.');

end;




procedure TFormDocEmptyIncome.sbExcelSpecClick(Sender: TObject);
begin
  if fQYN('Экспортировать список в Excel ?') then
    ExportExcel(dgSpec, self.Caption);
end;

procedure TFormDocEmptyIncome.sbFilterClick(Sender: TObject);
begin


  sbFilter.Down := false;

  FormFilterLite.ShowModal;
  if FormFilterLite.ModalResult in [mrOk, mrYes] then
  begin
    ResetSpecFilter;
    SetSpecFilter;
  end;


end;


procedure TFormDocEmptyIncome.sbResetFilterClick(Sender: TObject);
begin
  ResetSpecFilter;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormDocEmptyIncome.ResetSpecFilter;
begin

  dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
  dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := specguid;
  dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := '';
  dm.spCreateFilter.ExecProc;
  specguid := '';
  sbFilter.Down := false;

end;


procedure TFormDocEmptyIncome.SetSpecFilter;
begin

    if FormFilterLite.conditions = '' then
    begin
      ResetSpecFilter;
      exit;
    end;

    //установка фильтра
    CreateGuid(gu);
    specguid := GuidToString(gu);

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := specguid;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := FormFilterLite.conditions;
    dm.spCreateFilter.ExecProc;
    sbFilter.Down := true;

    meSpec.Close;
    meSpec.Open;

end;


end.
