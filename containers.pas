﻿unit containers;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, objects, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Vcl.Mask, DBCtrlsEh;

type
  TFormContainers = class(TFormObjects)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormContainers: TFormContainers;

implementation

{$R *.dfm}

uses editcontainer;


procedure TFormContainers.Init;
begin
  inherited;
  self.formEdit := FormEditContainer;
  self.tablename := 'objects';
end;


end.
