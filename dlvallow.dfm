﻿inherited FormDlvAllow: TFormDlvAllow
  Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1080#1077' '#1074#1099#1076#1072#1095#1080' '#1073#1077#1079' '#1086#1087#1083#1072#1090#1099
  ClientHeight = 209
  ExplicitHeight = 237
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 168
    TabOrder = 2
  end
  object cbDlvAllowed: TDBCheckBoxEh [1]
    Left = 24
    Top = 28
    Width = 241
    Height = 17
    Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1072' '#1074#1099#1076#1072#1095#1072' '#1073#1077#1079' '#1086#1087#1083#1072#1090#1099
    DataField = 'dlvallowed'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 0
  end
  object edDvlAllowedNote: TDBEditEh [2]
    Left = 24
    Top = 96
    Width = 497
    Height = 24
    ControlLabel.Width = 185
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077' '#1082' '#1088#1072#1079#1088#1077#1096#1077#1085#1080#1102
    ControlLabel.Visible = True
    DataField = 'dlvallowed_note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 72
    Top = 152
  end
  inherited qrAux: TADOQuery
    Left = 24
    Top = 152
  end
end
