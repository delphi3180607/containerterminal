﻿inherited FormImportCargoSheet: TFormImportCargoSheet
  Caption = #1048#1084#1087#1086#1088#1090' '#1074#1077#1076#1086#1084#1086#1089#1090#1080' '#1075#1088#1091#1079#1086#1074
  ClientHeight = 166
  ExplicitWidth = 543
  ExplicitHeight = 194
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 27
    Top = 34
    Width = 125
    Height = 16
    Caption = #1060#1072#1081#1083' '#1076#1083#1103' '#1080#1084#1087#1086#1088#1090#1072
  end
  inherited plBottom: TPanel
    Top = 125
    ExplicitTop = 125
    inherited btnOk: TButton
      OnClick = btnOkClick
    end
  end
  object edFileName: TDBEditEh [2]
    Left = 27
    Top = 56
    Width = 481
    Height = 24
    DynProps = <>
    EditButtons = <
      item
        ShortCut = 45
        Style = ebsEllipsisEh
        OnClick = edFileNameEditButtons0Click
      end>
    TabOrder = 1
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 104
    Top = 120
  end
  object od: TOpenDialog
    Filter = '*.xls*|*.xls*'
    Left = 200
    Top = 112
  end
  object ExcelApplication1: TExcelApplication
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    AutoQuit = False
    Left = 312
    Top = 16
  end
end
