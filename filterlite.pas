﻿unit filterlite;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Buttons,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormFilterLite = class(TFormEdit)
    Label3: TLabel;
    cbClear: TSpeedButton;
    meNumbers: TMemo;
    btClear: TButton;
    procedure btnOkClick(Sender: TObject);
    procedure btClearClick(Sender: TObject);
  private
    { Private declarations }
  public
    conditions : string;
  end;

var
  FormFilterLite: TFormFilterLite;

implementation

{$R *.dfm}

procedure TFormFilterLite.btClearClick(Sender: TObject);
begin
  conditions := '';
end;

procedure TFormFilterLite.btnOkClick(Sender: TObject);
var i: integer;
begin

  conditions := '';

  for i := 0 to meNumbers.Lines.Count-1 do
  begin
   conditions := conditions +';object_num='+trim(meNumbers.Lines[i]);
  end;

end;

end.
