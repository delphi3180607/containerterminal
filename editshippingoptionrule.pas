﻿unit EditShippingOptionRule;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.CheckLst, Vcl.Mask, DBCtrlsEh, DBSQLLookUp;

type
  TFromEditShippingOptionRule = class(TFormEdit)
    luShippingOptions: TDBSQLLookUp;
    edPayer: TDBEditEh;
    Label18: TLabel;
    edExtraServices: TDBEditEh;
    ssShippingOption: TADOLookUpSqlSet;
    edDirection: TDBEditEh;
    edStation: TDBEditEh;
    Label1: TLabel;
    cbServices: TCheckListBox;
    nePriority: TDBNumberEditEh;
    cbExclude: TDBCheckBoxEh;
    Label2: TLabel;
    Label3: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure cbServicesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FromEditShippingOptionRule: TFromEditShippingOptionRule;

implementation

{$R *.dfm}

procedure TFromEditShippingOptionRule.cbServicesClick(Sender: TObject);
var i: integer; s: string;
begin
  s := '';
  for i := 0 to cbServices.Items.Count-1 do
  begin
    if cbServices.Checked[i] then
      s := s+','+cbServices.Items[i];
  end;
  edExtraServices.Value := s;
end;

procedure TFromEditShippingOptionRule.FormActivate(Sender: TObject);
var i: integer;
begin
  qrAux.Open;
  cbServices.Items.Clear;
  while not qrAux.Eof do
  begin
    cbServices.Items.Add(qrAux.FieldByName('scode').AsString);
    qrAux.Next;
  end;

  for I := 0 to cbServices.Items.Count-1 do
  begin
    if pos(cbServices.Items[i],edExtraServices.Text)>0 then
    cbServices.Checked[i] := true;
  end;

end;

end.
