﻿unit tablelinks;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, DBCtrlsEh, Vcl.Mask,
  Vcl.ExtCtrls, Data.DB, Data.Win.ADODB;

type
  TFormEditTableLinks = class(TFormEdit)
    edFieldName: TDBEditEh;
    edStoreTable: TDBEditEh;
    edSourceTable: TDBEditEh;
    edCreateRecordType: TDBComboBoxEh;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    edParentTable: TDBEditEh;
    Label1: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditTableLinks: TFormEditTableLinks;

implementation

{$R *.dfm}

end.
