﻿inherited FormEditDeliveryType: TFormEditDeliveryType
  Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
  ClientHeight = 251
  ClientWidth = 536
  ExplicitWidth = 542
  ExplicitHeight = 279
  PixelsPerInch = 96
  TextHeight = 16
  object Label3: TLabel [0]
    Left = 15
    Top = 69
    Width = 98
    Height = 16
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  object Label2: TLabel [1]
    Left = 15
    Top = 14
    Width = 24
    Height = 16
    Caption = #1050#1086#1076
  end
  object Label1: TLabel [2]
    Left = 15
    Top = 141
    Width = 131
    Height = 16
    Caption = #1043#1083#1086#1073#1072#1083#1100#1085#1099#1081' '#1088#1072#1079#1076#1077#1083
  end
  inherited plBottom: TPanel
    Top = 210
    Width = 536
    TabOrder = 3
    ExplicitTop = 210
    ExplicitWidth = 536
    inherited btnCancel: TButton
      Left = 420
      ExplicitLeft = 420
    end
    inherited btnOk: TButton
      Left = 301
      ExplicitLeft = 301
    end
  end
  object edName: TDBEditEh [4]
    Left = 15
    Top = 91
    Width = 505
    Height = 24
    DataField = 'name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object edCode: TDBEditEh [5]
    Left = 15
    Top = 32
    Width = 305
    Height = 24
    DataField = 'code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edGlobalSection: TDBEditEh [6]
    Left = 15
    Top = 163
    Width = 505
    Height = 24
    DataField = 'global_section'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  inherited qrAux: TADOQuery
    Left = 416
    Top = 40
  end
end
