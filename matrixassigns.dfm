﻿inherited FormMatrixAssigns: TFormMatrixAssigns
  Caption = #1047#1072#1076#1072#1085#1080#1103' '#1085#1072' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1103
  ClientHeight = 610
  ClientWidth = 927
  ExplicitWidth = 943
  ExplicitHeight = 648
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 569
    Width = 927
    inherited btnOk: TButton
      Left = 692
    end
    inherited btnCancel: TButton
      Left = 811
    end
  end
  inherited plAll: TPanel
    Width = 927
    Height = 569
    inherited plTop: TPanel
      Width = 927
      inherited sbDelete: TPngSpeedButton
        Visible = False
      end
      inherited sbAdd: TPngSpeedButton
        Visible = False
      end
      inherited sbEdit: TPngSpeedButton
        Visible = False
      end
      inherited Bevel2: TBevel
        Visible = False
      end
      inherited btTool: TPngSpeedButton
        Left = 889
      end
      inherited plCount: TPanel
        Left = 692
      end
    end
    inherited dgData: TDBGridEh
      Width = 927
      Height = 540
      TitleParams.MultiTitle = True
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_start'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103
          Width = 147
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnum'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
          Width = 133
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'sector_name'
          Footers = <>
          Title.Caption = #1059#1095#1072#1089#1090#1086#1082
          Width = 281
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'row_num'
          Footers = <>
          Title.Caption = #1064#1090#1072#1073#1077#1083#1100
          Width = 95
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'stack_num'
          Footers = <>
          Title.Caption = #1057#1077#1082#1094#1080#1103
          Width = 84
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'level_num'
          Footers = <>
          Title.Caption = #1059#1088#1086#1074#1077#1085#1100
          Width = 99
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cell_num'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1103#1095#1077#1081#1082#1080
          Width = 117
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select top 186 '
      'ma.id, ma.date_start, c.cnum, k.name as sector_name,  '
      
        'r.order_num as row_num, s.order_num as stack_num, level_num, cel' +
        'l_num'
      'from matrixassigns ma'
      'inner join containers c on (c.id = ma.container_id)'
      'inner join matrixsectors k on (k.id = ma.sector_id)'
      'inner join matrixrows r on (r.id = ma.row_id)'
      'inner join matrixstacks s on (s.id = ma.stack_id)'
      'order by ma.id desc')
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
    inherited aDelete: TAction
      Enabled = False
    end
    inherited aEdit: TAction
      Enabled = False
    end
  end
end
