﻿unit editorderspec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBGridEh, DBLookupEh, Vcl.Mask, DBCtrlsEh,
  Vcl.Buttons, MemTableEh, DBSQLLookUp, functions, PngSpeedButton,
  MemTableDataEh, DataDriverEh, ADODataDriverEh;

type
  TFormEditOrderSpec = class(TFormEdit)
    sbContainer: TSpeedButton;
    edContainerNum: TDBEditEh;
    edSealNumber: TDBEditEh;
    sbCargoType: TSpeedButton;
    cbIsEmpty: TDBCheckBoxEh;
    laContainerOwner: TDBSQLLookUp;
    laCargoType: TDBSQLLookUp;
    leContainerKind: TDBSQLLookUp;
    ssContainerKind: TADOLookUpSqlSet;
    edExternalOrderNum: TDBEditEh;
    drvContainers: TADODataDriverEh;
    meContainers: TMemTableEh;
    drvCargo: TADODataDriverEh;
    meCargo: TMemTableEh;
    Panel1: TPanel;
    nuWeight: TDBNumberEditEh;
    dtInspect: TDBDateTimeEditEh;
    cbPicture: TDBCheckBoxEh;
    dtDateStiker: TDBDateTimeEditEh;
    dtDateMade: TDBDateTimeEditEh;
    nuCarrying: TDBNumberEditEh;
    cbIsDefective: TDBCheckBoxEh;
    edNote: TDBEditEh;
    nuWeightCargo: TDBNumberEditEh;
    neAmount: TDBNumberEditEh;
    cbNotReady: TDBCheckBoxEh;
    cbACEP: TDBCheckBoxEh;
    cbIsSigned: TDBCheckBoxEh;
    plState: TPanel;
    txRealState: TDBEditEh;
    txFormalState: TDBEditEh;
    lbWarning: TLabel;
    cbSealWaste: TDBComboBoxEh;
    procedure cbIsEmptyClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure sbCargoTypeClick(Sender: TObject);
    procedure sbContainerClick(Sender: TObject);
    procedure edContainerNumKeyPress(Sender: TObject; var Key: Char);
    procedure edContainerNumExit(Sender: TObject);
    procedure btEditClick(Sender: TObject);
    procedure btEditCargoClick(Sender: TObject);
    procedure edContainerNumGetFieldData(Sender: TObject; var Value: Variant;
      var Handled: Boolean);
  private
    { Private declarations }
  public
    procedure AfterNumFieldExit;
    function UpdateRegisterContainer: boolean;
    function UpdateRegisterCargo: boolean;
  end;

var
  FormEditOrderSpec: TFormEditOrderSpec;

implementation

{$R *.dfm}

uses cargotypes, counteragents, dmu, editcontainer, editcargo, grid;

procedure TFormEditOrderSpec.btEditCargoClick(Sender: TObject);
begin
  drvCargo.SelectCommand.Parameters.ParamByName('id').Value := dsLocal.DataSet.FieldByName('cargo_id').AsInteger;
  meCargo.Close;
  meCargo.Open;
  EE(nil, FormEditCargo, meCargo, 'cargos');
  meCargo.Close;
  drvCargo.SelectCommand.Parameters.ParamByName('id').Value := dsLocal.DataSet.FieldByName('cargo_id').AsInteger;
  meCargo.Open;
  self.edSealNumber.Text := meCargo.FieldByName('seal_number').AsString;
  self.laCargoType.KeyValue := meCargo.FieldByName('type_id').AsString;
  self.cbIsEmpty.Checked := meCargo.FieldByName('isempty').AsBoolean;
end;

procedure TFormEditOrderSpec.btEditClick(Sender: TObject);
begin
  inherited;
  if UpdateRegisterContainer then
  begin
    EE(nil, FormEditContainer, meContainers, 'containers');
    edContainerNumExit(nil);
  end;

end;

procedure TFormEditOrderSpec.btnOkClick(Sender: TObject);
var allow: boolean;
begin

  inherited;

  TMemTableEh(dsLocal.DataSet).FieldByName('cargotype_id').Required := not cbIsEmpty.Checked;
  TMemTableEh(dsLocal.DataSet).FieldByName('container_owner_id').Required := true;

  laContainerOwner.Refresh;
  laCargoType.Refresh;

  if ((neAmount.Value = null) and (dsLocal.DataSet.FieldByName('container_num').AsString = '')) or
  ((not self.cbIsEmpty.Checked) and (dsLocal.DataSet.FieldByName('cargotype_id').AsString = '')) or
  (dsLocal.DataSet.FieldByName('container_owner_id').AsInteger = 0)
  then
  begin
    ShowMessage('Не заполнены обязательные поля!');
    self.ModalResult := mrNone;
    exit;
  end;

  if (dsLocal.DataSet.FieldByName('container_num').AsString <> '') then
  begin
    allow := UpdateRegisterContainer;
    if not allow then self.ModalResult := mrNone
    else
    allow := UpdateRegisterCargo;
  end;

end;


function TFormEditOrderSpec.UpdateRegisterContainer: boolean;
var allow: boolean;
begin

  if trim(edContainerNum.Text) = '' then
  begin
    result := false;
    exit;
  end;

   meContainers.Close;

  if dsLocal.DataSet.FieldByName('container_id').AsInteger = 0 then
  begin

    drvContainers.SelectCommand.Parameters.ParamByName('cnum').Value := edContainerNum.Text;
    drvContainers.SelectCommand.Parameters.ParamByName('docid').Value := dsLocal.DataSet.FieldByName('doc_id').AsInteger;
    meContainers.Open;

    if meContainers.RecordCount>0 then
    begin

      if VarToStr(self.leContainerKind.Value)<>'' then
        self.leContainerKind.Value := meContainers.FieldByName('kind_id').AsInteger;

      if VarToStr(self.laContainerOwner.Value)<>'' then
        self.laContainerOwner.Value := meContainers.FieldByName('owner_id').AsInteger;

      dsLocal.DataSet.FieldByName('container_id').Value := meContainers.FieldByName('id').AsInteger;

    end;

  end;

  allow := true;

  drvContainers.SelectCommand.Parameters.ParamByName('cnum').Value := '-1-';
  drvContainers.SelectCommand.Parameters.ParamByName('docid').Value := dsLocal.DataSet.FieldByName('doc_id').AsInteger;
  meContainers.Open;
  drvContainers.GetRecCommand.Parameters.ParamByName('current_id').Value := dsLocal.DataSet.FieldByName('container_id').AsInteger;
  meContainers.RefreshRecord;

  if dsLocal.DataSet.FieldByName('container_id').AsInteger>0 then
  if (self.laContainerOwner.Value <> meContainers.FieldByName('owner_id').Value) or
     (self.leContainerKind.Value <> meContainers.FieldByName('kind_id').Value)
   then allow := false;

  if not allow then
  if fQYN('В справочнике обнаружен контейнер с указанным номером: '+edContainerNum.Text
            +#10+#13+#10+#13+'Вид контейнера: '+meContainers.FieldByName('kind_code').AsString
            +#10+#13+#10+#13+'Собственник контейнера: '+meContainers.FieldByName('owner_code').AsString
            +#10+#13+#10+#13+'Вы действительно хотите переписать параметры контейнера на новые?'+#10+#13) then
  begin
    allow := true;
  end;

  if allow then
  begin

    dm.spUpdateRegisterContainer.Parameters.ParamByName('@ObjectId').Value := dsLocal.DataSet.FieldByName('container_id').Value;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@CNum').Value := edContainerNum.Text;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@KindId').Value := self.leContainerKind.KeyValue;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@Size').Value := null;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@OwnerId').Value := self.laContainerOwner.KeyValue;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@Note').Value := edNote.Text;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@ParentObjectId').Value := null;

    dm.spUpdateRegisterContainer.Parameters.ParamByName('@containerweight').Value := nuWeight.Value;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@carrying').Value := nuCarrying.Value;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@datemade').Value := dtDateMade.Value;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@dateinspection').Value := dtInspect.Value;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@isdefective').Value := cbIsDefective.Checked;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@datestiker').Value := dtDateStiker.Value;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@picture').Value := cbPicture.Checked;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@acep').Value := cbACEP.Checked;
    dm.spUpdateRegisterContainer.Parameters.ParamByName('@isnotready').Value := cbNotReady.Checked;

    try
      dm.spUpdateRegisterContainer.ExecProc;
    except
      On E : Exception do
      begin
        ShowMessage('Ошибка: '+E.Message);
        allow := false;
        edContainerNum.Text := meContainers.FieldByName('cnum').AsString;
        exit;
      end;
    end;

    dsLocal.DataSet.FieldByName('container_id').Value := dm.spUpdateRegisterContainer.Parameters.ParamByName('@ObjectId').Value;
    drvContainers.SelectCommand.Parameters.ParamByName('cnum').Value := edContainerNum.Text;
    drvContainers.SelectCommand.Parameters.ParamByName('docid').Value := dsLocal.DataSet.FieldByName('doc_id').AsInteger;
    meContainers.Close;
    meContainers.Open;

    edContainerNum.Text := meContainers.FieldByName('cnum').AsString;

  end;

  result := allow;

end;



function TFormEditOrderSpec.UpdateRegisterCargo: boolean;
var allow: boolean;
begin

    if dsLocal.DataSet.FieldByName('cargo_id').AsInteger<>0 then
    begin
      result := true;
      exit;
    end;

    dm.spUpdateRegisterCargo.Parameters.ParamByName('@ObjectId').Value := dsLocal.DataSet.FieldByName('cargo_id').Value;
    dm.spUpdateRegisterCargo.Parameters.ParamByName('@Cnum').Value := dsLocal.DataSet.FieldByName('container_num').Value;
    dm.spUpdateRegisterCargo.Parameters.ParamByName('@TypeId').Value := dsLocal.DataSet.FieldByName('cargotype_id').AsInteger;
    dm.spUpdateRegisterCargo.Parameters.ParamByName('@WeightSender').Value := null;
    dm.spUpdateRegisterCargo.Parameters.ParamByName('@WeightDoc').Value := null;
    dm.spUpdateRegisterCargo.Parameters.ParamByName('@WeightFact').Value := dsLocal.DataSet.FieldByName('weight_cargo').Value;
    dm.spUpdateRegisterCargo.Parameters.ParamByName('@SealNumber').Value := dsLocal.DataSet.FieldByName('seal_number').Value;
    dm.spUpdateRegisterCargo.Parameters.ParamByName('@IsEmpty').Value := dsLocal.DataSet.FieldByName('isempty').Value;
    dm.spUpdateRegisterCargo.Parameters.ParamByName('@Note').Value := null;
    dm.spUpdateRegisterCargo.Parameters.ParamByName('@ParentObjectId').Value := dsLocal.DataSet.FieldByName('container_id').Value;
    dm.spUpdateRegisterCargo.ExecProc;

    dsLocal.DataSet.FieldByName('cargo_id').Value := dm.spUpdateRegisterCargo.Parameters.ParamByName('@ObjectId').Value;
    drvCargo.SelectCommand.Parameters.ParamByName('id').Value := dsLocal.DataSet.FieldByName('cargo_id').Value;
    meCargo.Close;
    meCargo.Open;

end;


procedure TFormEditOrderSpec.cbIsEmptyClick(Sender: TObject);
begin
  if cbIsEmpty.Checked then self.laCargoType.Value := null;
  self.laCargoType.Enabled := not cbIsEmpty.Checked;
  self.sbCargoType.Enabled := not cbIsEmpty.Checked;
  TMemTableEh(dsLocal.DataSet).FieldByName('cargotype_id').Required := not cbIsEmpty.Checked;
  self.laCargoType.Refresh;
end;

procedure TFormEditOrderSpec.edContainerNumExit(Sender: TObject);
var k, i: integer;
begin

  lbWarning.Visible := false;

  if edContainerNum.Text<>'' then
  begin

    k:=0;
    i:=1;
    while(i<length(edContainerNum.Text))and(k=0) do
    if ((AnsiString(edContainerNum.Text)[i] in ['A'..'Z']) and (i<5))
    or ((AnsiString(edContainerNum.Text)[i] in ['0'..'9']) and (i>4))
    then
    begin
      k:=0;
      inc(i);
    end else
      k:=1;

    if k=1 then
    begin
      ShowMessage('В номере контейнера есть недопустимые символы, либо - неверный формат!');
      edContainerNum.Text := '           ';
      edContainerNum.SetFocus;
      exit;
    end;

    meContainers.Close;
    drvContainers.SelectCommand.Parameters.ParamByName('cnum').Value := edContainerNum.Text;
    drvContainers.SelectCommand.Parameters.ParamByName('docid').Value := dsLocal.DataSet.FieldByName('doc_id').AsInteger;
    meContainers.Open;

    if meContainers.FieldByName('extraorder').AsInteger = 1 then
    begin
      lbWarning.Caption := 'Внимание! На данный контейнер уже заведена первичная заявка с таким же типом операции.';
      lbWarning.Visible := true;
    end;

    if self.current_mode = 'insert' then
    if meContainers.FieldByName('extraorder').AsInteger = 2 then
    begin
      lbWarning.Caption := 'АСТАНАВИТЕСЬ!!! Этот контейнер уже есть в этой заявке!!!';
      lbWarning.Visible := true;
    end;

    if meContainers.FieldByName('id').AsInteger = 0 then
    begin
      lbWarning.Caption := 'Контейнер не проходил по учету! Вы уверены в правильности номера?';
      lbWarning.Visible := true;
    end;


  end;

  AfterNumFieldExit;

end;



procedure TFormEditOrderSpec.AfterNumFieldExit;
begin

  if edContainerNum.Text<>'' then
  begin

    txRealState.Text := meContainers.FieldByName('real_state_code').AsString;

    if meContainers.FieldByName('real_state_code').AsString =
        meContainers.FieldByName('formal_state_code').AsString
     then
      txFormalState.Text := '-'
     else
      txFormalState.Text := meContainers.FieldByName('formal_state_code').AsString;

    neAmount.Value := 1;
    neAmount.Enabled := false;

    if dsLocal.DataSet.FieldByName('container_id').AsInteger=0 then
    begin

      if (self.leContainerKind.KeyValue = -1) or (self.leContainerKind.KeyValue = null) then
        self.leContainerKind.KeyValue := meContainers.FieldByName('kind_id').AsInteger;

      if (self.laContainerOwner.KeyValue = -1) or (self.laContainerOwner.KeyValue = null) then
        self.laContainerOwner.KeyValue := meContainers.FieldByName('owner_id').AsInteger;

      nuWeight.Value := meContainers.FieldByName('container_weight').Value;
      nuCarrying.Value := meContainers.FieldByName('carrying').Value;
      dtDateMade.Value := meContainers.FieldByName('date_made').Value;
      cbPicture.Checked := meContainers.FieldByName('picture').AsBoolean;
      dtInspect.Value := meContainers.FieldByName('date_inspection').Value;
      dtDateStiker.Value := meContainers.FieldByName('date_stiker').Value;
      cbACEP.Checked := meContainers.FieldByName('acep').AsBoolean;
      cbIsDefective.Checked := meContainers.FieldByName('isdefective').AsBoolean;
      cbNotReady.Checked := meContainers.FieldByName('isnotready').AsBoolean;
      edNote.Text := meContainers.FieldByName('note').AsString;

      dsLocal.DataSet.FieldByName('container_id').Value := meContainers.FieldByName('id').AsInteger;

    end;

  end else
  begin
    neAmount.Enabled := true;
  end;

end;


procedure TFormEditOrderSpec.edContainerNumGetFieldData(Sender: TObject;
  var Value: Variant; var Handled: Boolean);
begin
  inherited;
  if edContainerNum.Text<>'' then
  begin

    //neAmount.Value := 1;
    //neAmount.Enabled := false;

    meContainers.Close;
    drvContainers.SelectCommand.Parameters.ParamByName('cnum').Value := edContainerNum.Text;
    drvContainers.SelectCommand.Parameters.ParamByName('docid').Value := dsLocal.DataSet.FieldByName('doc_id').AsInteger;
    meContainers.Open;

    txRealState.Text := meContainers.FieldByName('real_state_code').AsString;

    if meContainers.FieldByName('real_state_code').AsString =
        meContainers.FieldByName('formal_state_code').AsString
     then
      txFormalState.Text := '-'
     else
      txFormalState.Text := meContainers.FieldByName('formal_state_code').AsString;

    if meContainers.FieldByName('extraorder').AsInteger = 1 then
    begin
      lbWarning.Caption := 'Внимание! На данный контейнер уже заведена первичная заявка с таким же типом операции.';
    end;

  end;

end;

procedure TFormEditOrderSpec.edContainerNumKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Ord(Key)<32 then exit;
  if not (Key in [#8,'0'..'9','A'..'Z','a'..'z']) then Key := #0;
end;

procedure TFormEditOrderSpec.sbCargoTypeClick(Sender: TObject);
begin
  SFDE(TFormCargotypes,self.laCargoType);
end;

procedure TFormEditOrderSpec.sbContainerClick(Sender: TObject);
begin
  SFDE(TFormCounteragents,self.laContainerOwner);
end;

end.
