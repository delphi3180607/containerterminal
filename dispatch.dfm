﻿inherited FormDocDispatch: TFormDocDispatch
  ActiveControl = dgSpec
  Caption = #1059#1073#1086#1088#1082#1072
  ClientHeight = 657
  ExplicitHeight = 695
  PixelsPerInch = 96
  TextHeight = 13
  inherited SplitterVert: TSplitter
    Left = 189
    Height = 624
    ExplicitLeft = 153
    ExplicitHeight = 624
  end
  inherited plBottom: TPanel
    Top = 624
    ExplicitTop = 624
  end
  inherited plAll: TPanel
    Left = 196
    Width = 737
    Height = 624
    Caption = ''
    ExplicitLeft = 196
    ExplicitWidth = 737
    ExplicitHeight = 624
    inherited SplitterHor: TSplitter
      Left = 639
      Top = 63
      Width = 3
      Height = 67
      Cursor = crHSplit
      Align = alRight
      Visible = False
      ExplicitLeft = 685
      ExplicitTop = 63
      ExplicitWidth = 3
      ExplicitHeight = 68
    end
    object Splitter1: TSplitter [1]
      Left = 0
      Top = 130
      Width = 737
      Height = 6
      Cursor = crVSplit
      Align = alBottom
      Color = 15132390
      ParentColor = False
      StyleElements = [seFont, seBorder]
      ExplicitTop = 129
    end
    inherited plTop: TPanel
      Width = 737
      ExplicitWidth = 737
      inherited btFilter: TPngSpeedButton
        Left = 168
        Width = 35
        ExplicitLeft = 147
        ExplicitWidth = 35
      end
      inherited btExcel: TPngSpeedButton
        Left = 554
        ExplicitLeft = 612
      end
      inherited sbEdit: TPngSpeedButton
        Margins.Right = 2
      end
      inherited Bevel2: TBevel
        Left = 164
        Width = 2
        ExplicitLeft = 140
        ExplicitWidth = 2
        ExplicitHeight = 22
      end
      inherited btTool: TPngSpeedButton
        Left = 539
        ExplicitLeft = 698
      end
      inherited Bevel1: TBevel
        Left = 301
        Width = 2
        ExplicitLeft = 264
        ExplicitTop = 2
        ExplicitWidth = 2
        ExplicitHeight = 22
      end
      inherited btFlow: TPngSpeedButton
        Left = 375
        ExplicitLeft = 392
      end
      inherited sbHistory: TPngSpeedButton
        Left = 407
        ExplicitLeft = 431
      end
      inherited sbRestrictions: TPngSpeedButton
        Left = 480
        Width = 34
        ExplicitLeft = 525
        ExplicitWidth = 34
      end
      inherited sbConfirm: TPngSpeedButton
        Left = 267
        ExplicitLeft = 235
      end
      inherited sbCancelConfirm: TPngSpeedButton
        Left = 305
        ExplicitLeft = 273
      end
      inherited Bevel5: TBevel
        Left = 516
        Width = 2
        ExplicitLeft = 568
        ExplicitWidth = 2
        ExplicitHeight = 22
      end
      inherited sbReport: TPngSpeedButton
        Left = 520
        Width = 34
        ExplicitLeft = 577
        ExplicitWidth = 34
      end
      inherited Bevel6: TBevel
        Left = 371
        Width = 2
        ExplicitLeft = 387
        ExplicitTop = 2
        ExplicitWidth = 2
        ExplicitHeight = 22
      end
      inherited sbImport2: TPngSpeedButton
        Visible = False
      end
      inherited sbSearch: TPngSpeedButton
        Left = 235
        ExplicitLeft = 220
      end
      inherited Bevel7: TBevel
        Left = 441
        Width = 2
        ExplicitLeft = 476
        ExplicitTop = 2
        ExplicitWidth = 2
        ExplicitHeight = 22
      end
      inherited sbClearFilter: TPngSpeedButton
        Left = 203
        ExplicitLeft = 184
      end
      inherited Bevel3: TBevel
        Left = 588
        Width = 2
        ExplicitLeft = 658
        ExplicitTop = 2
        ExplicitWidth = 2
      end
      inherited sbClock: TPngSpeedButton
        Left = 592
        Width = 34
        ExplicitLeft = 665
        ExplicitWidth = 34
      end
      inherited sbBarrel: TPngSpeedButton
        Left = 337
        Enabled = False
        Visible = False
        ExplicitLeft = 305
      end
      object sbSumInfo: TPngSpeedButton [23]
        AlignWithMargins = True
        Left = 445
        Top = 0
        Width = 35
        Height = 24
        Hint = #1054#1075#1088#1072#1085#1080#1095#1077#1085#1080#1103' '#1080' '#1076#1086#1087'.'#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        Layout = blGlyphRight
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbSumInfoClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000028E4944415478DA85935F4853511CC77FE7DCFD7374D9CC6CE532EFD4
          CC70130A35D3C4CC1E64BD47104388FE4150BD04053D5496E193F4E7219F22A8
          A7FE8145415688A2242AB1749BCDD6769596B3B873737A77EFDDF576B6895878
          F5F7700EE7C7F97E387FBE5F04A4CE7D8A8C3BF20C15B2ACCC230094EE29A05A
          8A0621DACD253D5DCD167B6673C7E73F4167592E935852B26A8095F97F50BA4F
          91A17B221ABA599F6FCBECBB3D301B6C2A35335151019C16912E460AD07A8ACC
          081445F907803182D7DE68E8D6C16580EB79D07DA2665BA55E47C1A220438E06
          833B1C870FDE1818F5D49C9642E60C62290B217AE0F814FBAEB594C9000E3CF4
          19F3F4E899ABCEEACC37E920C1CBA0D722E89DE0E0497FB871FA46759FDA83A0
          D58BDA4E77D7D9E6C23345964DC02753401B28F8CAC6E1F147F6E4F0D5AA471B
          02D2E5681BB9D0B27FFB5D8B858604B9CE0EB30EC6D818F40C4C7578AFD75ED9
          10902ECBA5DE6BCEC3256D98368228C960A20D10FE1905DFE8D445DF9D867BEB
          028ACEF730FBEC5BFB6C766BE14C4206AD9602299582891136242C8AF59EF686
          B02AC07AFA6D4195DDE22EAF66B6782312180C18245186C050E087C027ABFD9D
          4738D52B585BBB3565C59B037B1A77EFF4FD1620877CA92CA4607A24E017620B
          7B035D4717551FB1C0F5AAA494C97D5379A8BCFC4B4404997C3ACF25607678D2
          CD2FF075732F8EAF295E01D45C7EFFDDD15C5132F44B029492C166D680F22D04
          122F8A48A791114239AB456977B2B30BECD87D67D6480DED8341A1A880E1E2C9
          8C8F1993164C460D39095E33118884C13F3819F2743465AD5CDBD61F1476314C
          844B828EC2C04B0AC8EBE411610CD83B198A3C68C9028A4FBD1C97685305C9C1
          3CD12355832C1F472171C65CD433F3F498FD2FA4CF0320451389780000000049
          454E44AE426082}
        ExplicitLeft = 482
        ExplicitTop = 2
        ExplicitHeight = 21
      end
      inherited sbPass: TPngSpeedButton
        Left = 130
        ExplicitLeft = 130
      end
      inherited plCount: TPanel
        Left = 576
        ExplicitLeft = 576
        inherited edCount: TDBEditEh
          ControlLabel.ExplicitLeft = 16
          ControlLabel.ExplicitTop = -16
          ControlLabel.ExplicitWidth = 103
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 639
      Height = 67
      Font.Height = -13
      TitleParams.MultiTitle = True
      Columns = <
        item
          Alignment = taLeftJustify
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 59
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 86
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 144
        end
        item
          CellButtons = <>
          Checkboxes = False
          DynProps = <>
          EditButtons = <>
          FieldName = 'dispatch_code'
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1086#1090#1087#1088#1072#1074#1082#1080
          Width = 137
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1087#1086#1077#1079#1076#1072
          Width = 115
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'station_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1085#1094#1080#1103
          Visible = False
          Width = 153
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'direction_code'
          Footers = <>
          Title.Caption = #1053#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077
          Visible = False
          Width = 148
        end
        item
          CellButtons = <>
          DisplayFormat = 'DD.MM.YYYY HH:NN:SS'
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_notification'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1091#1074#1077#1076#1086#1084#1083#1077#1085#1080#1103
          Width = 131
        end
        item
          CellButtons = <>
          DisplayFormat = 'DD.MM.YYYY HH:NN:SS'
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_remove'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1089#1098#1077#1084#1082#1080
          Width = 122
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'sheet_remove'
          Footers = <>
          Title.Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100' '#1091#1073#1086#1088#1082#1080
          Width = 120
        end>
    end
    inherited plHint: TPanel
      TabOrder = 6
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    inherited plStatus: TPanel
      Width = 737
      ExplicitWidth = 737
    end
    inherited plLinkedObjects: TPanel
      Left = 645
      Top = 66
      Width = 89
      Height = 61
      Align = alRight
      Visible = False
      ExplicitLeft = 645
      ExplicitTop = 66
      ExplicitWidth = 89
      ExplicitHeight = 61
      inherited dgLinkedObjects: TDBGridEh
        Width = 89
        Height = 61
        Border.EdgeBorders = [ebTop, ebRight]
        Visible = False
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'rus_object_type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1086#1073#1098#1077#1082#1090#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -13
            Title.Font.Name = 'Calibri'
            Title.Font.Style = []
            Width = 114
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088
            Width = 102
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'kind_code'
            Footers = <>
            Title.Caption = #1042#1080#1076
            Width = 105
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'owner_code'
            Footers = <>
            Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
            Width = 135
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'state_code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = [fsItalic]
            Footers = <>
            Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 134
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'dlvtype_code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7697781
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = []
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1086#1087#1077#1088#1072#1094#1080#1080
            Width = 125
          end>
      end
    end
    inherited plSearch: TPanel
      Width = 737
      ExplicitWidth = 737
      inherited Label1: TLabel
        Height = 14
      end
      inherited edSearch: TEdit
        Width = 555
        ExplicitWidth = 555
      end
    end
    object plSpec: TPanel
      Left = 0
      Top = 136
      Width = 737
      Height = 488
      Align = alBottom
      TabOrder = 5
      object plToolSpec: TPanel
        Left = 1
        Top = 1
        Width = 735
        Height = 28
        Align = alTop
        BevelOuter = bvNone
        Color = 15921906
        ParentBackground = False
        TabOrder = 0
        StyleElements = [seFont, seBorder]
        object sbDeleteSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 30
          Top = 0
          Width = 30
          Height = 28
          Hint = #1059#1076#1072#1083#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          OnClick = sbDeleteSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
            6FA864000001654944415478DA6364A0103052D5004731310B2B1919E9D673E7
            D662535CA5AC1C78F2EBD7977B5FBC38866180A38484F98E8C8CE39FFEFC6198
            B06D5B00D0904DC89ACB15143C4B5252B6F2FDFECDE0356386EDDE972F8FA218
            50252717589098B8965354F4DBB7A74FB9A6ECDA15D87CF6EC46905CB5B6B66F
            6E40C0462E09892F3F5FBEE499B86D5B680BD495285EA8D0D4F42A0808D8C22B
            23F3E3EBB3671C9D1B3678FEFDF4E96F657CFC2E1E09891F9F9F3EE598BC7327
            8AEB3002B15A4BCB372F2464A380A4E4BFB78F1E3131B0B030084B4BFFFBF8F0
            21D3642457E18D857C4949E7B29494DD0242423F181819193EBE78C1D1BB7DBB
            67EFC58B3B898AC602A801FCE41880E205A0B31958591984A4A4FE7C7AF48885
            A017C0811818B885575A1A6B207E0106E2245C81088AC642603472901B8D0413
            92BCBC674962E256BEFFFF193CA74FB7D9F7EAD531D293B29151D0F1274F9EED
            7FF5EA04DE582005506C000043E9DA11FA5170A10000000049454E44AE426082}
        end
        object sbEditSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 0
          Top = 0
          Width = 30
          Height = 28
          Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbEditSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
            62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
            08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
            4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
            C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
            892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
            4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
            0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
            C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
            8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
            346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
            5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
            3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
            378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
            C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
            1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
            FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
            F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
            0049454E44AE426082}
        end
        object sbExcelSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 60
          Top = 0
          Width = 30
          Height = 28
          Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          ParentShowHint = False
          ShowHint = True
          OnClick = sbExcelSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000B1200000B
            1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000002CC4944415478DA6D934B48545118C7
            FF67346A141B7332F35136A6398E8E332A6A48505A8E2F68D16634451745602F
            93162911F65A0411F82C082A3433CB686764D3C3458B462B17968A1486A8E942
            D42847C77BCEE9DC7BC7C16C2EDC73CFEBFBDDFFF73FDF2158F7D83BAFED5E9C
            9DCF5876BB5297D88A894ACCB4C8DDB503350F3BE1E321F6BB17AA9618B75026
            25AC4892516252B05B5A81442918631063700DCA3ED7B63FF20928BA5DCDA9D8
            4819555E3548F4054086C86BD1C161F321BA2D2E0E0E06C619E5728BA1E91F5A
            52D8728E5331A09ECD2A481E4B2A48F41342A31112B819C7F61F41ABB35B09B6
            46ED8163C439490A5AAAF8BF81E2EB01AA0A284C61BB10B23108259979E81AEC
            052780252216AF46FA26497EB30CA0D06D0A54729AFE358B2C8319A333E3189F
            9B5152326E8B865E1B84E2341B1E0F38E0B7C11FD61DF1022014E4359F5500E6
            F0585465DB71D3D18EDABC72D8EF5DC4E2F2920248100AF45A9D00E4A2E3530F
            888620D5605201B6A6337C55FED5A213B044C6E1D69B0E747F79EF353521CCA0
            28389A9E8FF6FE97C24A08057178F76D6092E4369E564C9437DA8C99A8CE29C1
            E13BE731FB67C17B2289DB63B0354027003601E8111E70240BC5AF473F4E9143
            8DA714133584E07EE925CCFE5EC0E0CFEFA87FDBA102849189E12AA02CBD006D
            FD2F201FA7252A1E8E61E71439D820009C629FC182AC986434F53E455B791D2A
            5AAF60627E46012445C62254004A3D002880383886FAA6484EC349EE2D1E4FCE
            1AA2518ED1E55E56E6CD1131D00706A33CA310AD7D721D70A4EC348A3AF83041
            B2EB2BD700642FE87F9568167F931554EC2DC4035148B2072991F1E819764E90
            0302A0160FF5E6BC5A50F23D90FB067DF89C4E1BE4E29C2BF2A968840FFCEBF4
            5800C9BA71BC9230C942FD4822A3CC24C243A8585E5B89F0F72B1EADEB7AE2F3
            32AD9FB05E2EDEC556581AD54856892349E46B9608A919BBFEFC992FC05FFC64
            D8FDA3D5A84C0000000049454E44AE426082}
        end
      end
      object dgSpec: TDBGridEh
        Left = 1
        Top = 29
        Width = 735
        Height = 431
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        Border.Color = clSilver
        Border.EdgeBorders = [ebLeft, ebTop]
        BorderStyle = bsNone
        Ctl3D = False
        DataSource = dsSpec
        DynProps = <>
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Calibri'
        Font.Style = []
        GridLineParams.DataHorzColor = 7794157
        GridLineParams.DataVertColor = 14540253
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
        IndicatorParams.FillStyle = cfstGradientEh
        IndicatorParams.HorzLineColor = clSilver
        IndicatorParams.VertLineColor = clSilver
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        OptionsEh = [dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
        ParentCtl3D = False
        ParentFont = False
        ParentShowHint = False
        PopupMenu = pmSpec
        RowHeight = 20
        SelectionDrawParams.SelectionStyle = gsdsClassicEh
        SelectionDrawParams.DrawFocusFrame = False
        SelectionDrawParams.DrawFocusFrameStored = True
        ShowHint = True
        STFilter.Color = clWindow
        STFilter.HorzLineColor = 14540253
        STFilter.Local = True
        STFilter.VertLineColor = 14540253
        STFilter.Visible = True
        TabOrder = 1
        TitleParams.FillStyle = cfstThemedEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -11
        TitleParams.Font.Name = 'Verdana'
        TitleParams.Font.Style = [fsBold]
        TitleParams.HorzLineColor = 14540253
        TitleParams.MultiTitle = True
        TitleParams.ParentFont = False
        TitleParams.VertLineColor = 14540253
        OnCellClick = dgSpecCellClick
        OnDrawColumnCell = dgSpecDrawColumnCell
        OnKeyPress = dgSpecKeyPress
        OnMouseDown = dgSpecMouseDown
        OnMouseUp = dgSpecMouseUp
        OnSelectionChanged = dgSpecSelectionChanged
        Columns = <
          item
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'receipt_ready'
            Footers = <>
            Title.Caption = #1050#1074'- '#1094#1080#1103
            Visible = False
            Width = 34
          end
          item
            ButtonStyle = cbsEllipsis
            CellButtons = <>
            DynProps = <>
            EditButton.Style = ebsEllipsisEh
            EditButton.Visible = True
            EditButtons = <>
            FieldName = 'receipt_number'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088' '#1082#1074#1080#1090#1072#1085#1094#1080#1080
            Visible = False
            Width = 83
          end
          item
            ButtonStyle = cbsNone
            CellButtons = <>
            DisplayFormat = '### ##0.00'
            DynProps = <>
            EditButton.Style = ebsEllipsisEh
            EditButton.Visible = False
            EditButtons = <>
            FieldName = 'receipt_summa'
            Footers = <>
            Title.Caption = #1057#1091#1084#1084#1072' '#1082#1074#1080#1090#1072#1085#1094#1080#1080
            Width = 81
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'dispatch_numbers'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088' '#1086#1090#1087#1088#1072#1074#1082#1080
            Width = 77
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'date_serve'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1086#1090#1087#1088#1072#1074#1082#1080
            Width = 89
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'pnum'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            Footers = <>
            Title.Caption = #8470' '#1074#1072#1075#1086#1085#1072
            Width = 89
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'carkind_code'
            Footers = <>
            Title.Caption = #1058#1080#1087
            Width = 48
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'model_code'
            Footers = <>
            Title.Caption = #1052#1086#1076#1077#1083#1100
            Width = 71
          end
          item
            Alignment = taCenter
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'amount_on_car'
            Footers = <>
            Title.Caption = #1050#1086#1083'. '#1082#1090#1082'/'#1074#1072#1075'.'
            Width = 64
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'kind_code'
            Footers = <>
            ReadOnly = True
            Title.Caption = #1058#1080#1087
            Width = 47
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            ReadOnly = True
            Title.Caption = #8470' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
            Width = 84
          end
          item
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isempty'
            Footers = <>
            ReadOnly = True
            Title.Caption = #1055#1086#1088'.'
            Width = 37
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'mtu_code'
            Footers = <>
            Title.Caption = #1052#1058#1059
            ToolTips = True
            Width = 82
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'picture_code'
            Footers = <>
            Title.Caption = #1056#1080#1089#1091#1085#1086#1082
            ToolTips = True
            Width = 89
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cargotype_code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Calibri'
            Font.Style = []
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1075#1088#1091#1079#1072
            Width = 95
          end
          item
            Alignment = taCenter
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'seal_number'
            Footers = <>
            Title.Caption = #1055#1083#1086#1084#1073#1072
            Width = 79
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'weight_fact'
            Footers = <>
            Title.Caption = #1042#1077#1089' '#1073#1088#1091#1090#1090#1086
            Width = 60
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'payer_code'
            Footers = <>
            Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
            Width = 107
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'consignee_code'
            Footers = <>
            Title.Caption = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
            Width = 130
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'station_code'
            Footers = <>
            Title.Caption = #1057#1090#1072#1085#1094#1080#1103
            Width = 147
          end
          item
            Alignment = taCenter
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'state_code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Verdana'
            Font.Style = [fsItalic]
            Footers = <>
            ReadOnly = True
            Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 100
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'base_doc_text'
            Footers = <>
            Title.Caption = #1047#1072#1103#1074#1082#1072
            Width = 164
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'car_feed_date'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1087#1086#1076#1072#1095#1080
            Width = 80
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object edExtraServices: TDBEdit
        AlignWithMargins = True
        Left = 1
        Top = 460
        Width = 735
        Height = 27
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alBottom
        Color = 14548991
        DataField = 'extra_services'
        DataSource = dsSpec
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        StyleElements = [seBorder]
      end
    end
  end
  inherited plLeft: TPanel
    Width = 189
    Height = 624
    ExplicitWidth = 189
    ExplicitHeight = 624
    inherited Bevel4: TBevel
      Left = 188
      Height = 599
      ExplicitLeft = 152
      ExplicitHeight = 591
    end
    inherited dgFolders: TDBGridEh
      Width = 188
      Height = 599
    end
    inherited plTool: TPanel
      Width = 189
      ExplicitWidth = 189
      inherited sbWrap: TSpeedButton
        Left = 95
        Width = 94
        ExplicitLeft = 153
        ExplicitWidth = 0
      end
      inherited cbAll: TCheckBox
        Width = 89
        ExplicitWidth = 89
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'declare @userid int, @doctypeid int, @datebegin datetime, @datee' +
        'nd datetime, @isconfirmed int;'
      ''
      
        'select @doctypeid = min(id) from doctypes where system_section =' +
        ' '#39'outcome_dispatch'#39';'
      ''
      'select @userid = :userid;'
      ''
      
        'select @datebegin = datebegin, @dateend  = dateend, @isconfirmed' +
        '  = isconfirmed '
      'from dbo.f_GetFilterParams(@userid,@doctypeid);'
      ''
      'select d.id as id, d.*, s.*, r.code as direction_code,'
      
        '(select name from stations st where st.id = s.station_id) as sta' +
        'tion_code,'
      
        '(select code from dispatchkinds k where k.id = s.dispatchkind_id' +
        ') as dispatch_code,'
      
        '(select count(*) where exists (select 1 from f_GetLinkedDocument' +
        's(d.id))) as docexists,'
      
        '(case when d.isconfirmed = 1 then '#39' +'#39' else '#39#39' end) as state_cod' +
        'e'
      'from documents d, docdispatch s '
      'left outer join directions r on (r.id = s.direction_id)'
      'where s.id = d.id and (d.folder_id = :folder_id or :showall = 1)'
      'and ('
      
        '    not exists (select 1 from dbo.f_GetFilterList(@userid, @doct' +
        'ypeid) fl1)'
      
        '    or exists (select 1 from dbo.f_GetFilterList(@userid, @docty' +
        'peid) fl, docdispatchspec sp, containers c where  sp.doc_id = s.' +
        'id and c.id = sp.container_id and charindex(fl.v, c.cnum)>0) '
      
        '    or exists (select 1 from dbo.f_GetFilterList(@userid, @docty' +
        'peid) fl, docdispatchspec sp, carriages c where  sp.doc_id = s.i' +
        'd and c.id = sp.carriage_id and charindex(fl.v, c.pnum)>0) '
      ')'
      'and (@datebegin is null or  d.doc_date >=@datebegin)'
      'and (@dateend is null or  d.doc_date <@dateend+1)'
      
        'and (isnull(@isconfirmed,0)=0 or d.isconfirmed = isnull(@isconfi' +
        'rmed,0))'
      'order by d.doc_date'
      '')
    SelectCommand.Parameters = <
      item
        Name = 'userid'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'folder_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'showall'
        DataType = ftInteger
        Size = -1
        Value = 1
      end>
    UpdateCommand.CommandText.Strings = (
      'update docdispatch'
      'set'
      '  direction_id = :direction_id,'
      '  dispatchkind_id = :dispatchkind_id,'
      '  dispatch_number = :dispatch_number,'
      '  date_notification = :date_notification,'
      '  station_id = :station_id,'
      '  train_num = :train_num, '
      '  date_serve = :date_serve,'
      '  sheet_serve = :sheet_serve,'
      '  date_remove = :date_remove,'
      '  sheet_remove = :sheet_remove'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'direction_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dispatchkind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dispatch_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'date_notification'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'station_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'train_num'
        Size = -1
        Value = Null
      end
      item
        Name = 'date_serve'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'sheet_serve'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'date_remove'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'sheet_remove'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from documents where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select d.id as id, d.*, s.*, r.code as direction_code,'
      
        '(select name from stations st where st.id = s.station_id) as sta' +
        'tion_code,'
      
        '(select code from dispatchkinds k where k.id = s.dispatchkind_id' +
        ') as dispatch_code,'
      
        '(select count(*) where exists (select 1 from f_GetLinkedDocument' +
        's(d.id))) as docexists,'
      
        '(case when d.isconfirmed = 1 then '#39' +'#39' else '#39#39' end) as state_cod' +
        'e'
      'from documents d, docdispatch s '
      'left outer join directions r on (r.id = s.direction_id)'
      'where s.id = d.id and d.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 296
    Top = 336
  end
  inherited meData: TMemTableEh
    AfterOpen = meDataAfterOpen
    Left = 344
    Top = 336
  end
  inherited dsData: TDataSource
    Left = 400
    Top = 336
  end
  inherited qrAux: TADOQuery
    Left = 480
    Top = 336
  end
  inherited drvForms: TADODataDriverEh
    Left = 612
    Top = 338
  end
  inherited meForms: TMemTableEh
    Left = 660
    Top = 338
  end
  inherited dsForms: TDataSource
    Left = 708
    Top = 338
  end
  inherited pmDocFlow: TPopupMenu
    Left = 288
    Top = 152
  end
  inherited al: TActionList
    Left = 528
    Top = 337
    inherited aAdd: TAction
      Enabled = False
      Visible = False
    end
  end
  inherited drvLinkedObjects: TADODataDriverEh
    Left = 657
    Top = 250
  end
  inherited meLinkedObjects: TMemTableEh
    Left = 705
    Top = 250
  end
  inherited dsLinkedObjects: TDataSource
    Left = 761
    Top = 250
  end
  inherited pmLinkedObjects: TPopupMenu
    Left = 840
    Top = 104
  end
  inherited exReport: TEXLReport
    Left = 430
    Top = 128
  end
  inherited qrReport: TADOQuery
    Left = 760
    Top = 336
  end
  inherited IL: TPngImageList
    Bitmap = {}
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select s.id as id, l.id as docload_id,'
      'isnull(s.receipt_ready,0) as receipt_ready, '
      's.receipt_number, s.receipt_summa,'
      's.*,cn.cnum, '
      
        '(select code from containerkinds ck where ck.id = cn.kind_id) as' +
        ' kind_code, '
      'c.isempty, '
      
        '(case when c.seal_number like '#39'{%'#39' then '#39#39' else c.seal_number en' +
        'd) as seal_number, '
      'c.weight_fact, cr.pnum, '
      
        '(select name from stations st where st.id = t.station_id) as sta' +
        'tion_code,'
      
        '(select count(*) from docdispatchspec s1 where s1.doc_id = s.doc' +
        '_id and s1.carriage_id = s.carriage_id) as amount_on_car,'
      
        '(select cr.pnum+'#39', '#39'+isnull((select code from carriagekinds ck w' +
        'here ck.id = cr.kind_id),'#39#39')+'#39', '#39'+isnull((select model_code from' +
        ' carriagemodels cm where cm.id = cr.model_id),'#39#39')) as car_text,'
      
        '(select code from counteragents c1 where c1.id = v.owner_id) as ' +
        'owner_code, '
      
        '(select code from objectstatekinds sk where sk.id = v.state_id) ' +
        'as state_code,'
      
        '(select name from cargotypes t where t.id = c.type_id) as cargot' +
        'ype_code,'
      
        '(select code from carriagekinds ck where ck.id = cr.kind_id) as ' +
        'carkind_code,'
      
        '(select model_code from carriagemodels cm where cm.id = cr.model' +
        '_id) as model_code,'
      
        '(select name from mtukinds m where m.id = s.mtukind_id) as mtu_c' +
        'ode,'
      
        '(select name from picturekinds p where p.id = s.picturekind_id) ' +
        'as picture_code,'
      
        '(select code from counteragents cn where cn.id = t.consignee_id)' +
        ' as consignee_code, '
      
        '(select code from counteragents cn where cn.id = t.payer_id) as ' +
        'payer_code, '
      'o.base_doc_text, o.extra_services'
      'from docdispatchspec s'
      'inner join docload l on (l.id = s.docload_id)'
      'inner join tasks t on (t.id = l.task_id)'
      'inner join containers cn on (s.container_id = cn.id)'
      'inner join objects v on (cn.id = v.id)'
      'inner join carriages cr on (cr.id = s.carriage_id) '
      'inner join cargos c on (s.cargo_id = c.id) '
      'left outer join'
      '('
      'select s.id, '
      
        'replace(substring(o.extra_services,2,450),'#39','#39','#39'; '#39') as extra_ser' +
        'vices,'
      
        'isnull(d.doc_number,'#39#39')+'#39' '#1086#1090' '#39'+convert(varchar, d.doc_date, 104)' +
        ' as base_doc_text'
      
        'from documents d, docorder o, docorderspec s where d.id= o.id an' +
        'd d.id = s.doc_id '
      ') o on (o.id = l.orderspec_id) '
      'where  s.doc_id = :doc_id'
      'order by cr.pnum, l.order_num')
    SelectCommand.Parameters = <
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'SET CONTEXT_INFO 0x001;'
      ''
      'update docdispatchspec'
      'set'
      '  mtukind_id = :mtukind_id,'
      '  picturekind_id = :picturekind_id,'
      '  dispatch_numbers = :dispatch_numbers, '
      '  date_serve = :date_serve, '
      '  sheet_serve = :sheet_serve,'
      '  receipt_number = :receipt_number,'
      '  receipt_summa =   :receipt_summa'
      'where'
      '  id = :id'
      ''
      'SET CONTEXT_INFO 0x0;')
    UpdateCommand.Parameters = <
      item
        Name = 'mtukind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'picturekind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dispatch_numbers'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'date_serve'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'sheet_serve'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'receipt_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'receipt_summa'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into docdispatchspec'
      '  (mtukind_id, picturekind_id, dispatch_numbers)'
      'values'
      '  (:mtukind_id, :picturekind_id, :dispatch_numbers)')
    InsertCommand.Parameters = <
      item
        Name = 'mtukind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'picturekind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dispatch_numbers'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete s '
      'from docdispatchspec s'
      
        'inner join docdispatchspec s1 on (s1.doc_id = s.doc_id and s1.ca' +
        'rriage_id = s.carriage_id)'
      'where s1.id = :id'
      '')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select s.id as id, l.id as docload_id,'
      'isnull(s.receipt_ready,0) as receipt_ready, '
      's.receipt_number, s.receipt_summa,'
      's.*,cn.cnum, '
      
        '(select code from containerkinds ck where ck.id = cn.kind_id) as' +
        ' kind_code, '
      'c.isempty, '
      
        '(case when c.seal_number like '#39'{%'#39' then '#39#39' else c.seal_number en' +
        'd) as seal_number, '
      'c.weight_fact, cr.pnum, '
      
        '(select name from stations st where st.id = t.station_id) as sta' +
        'tion_code,'
      
        '(select count(*) from docdispatchspec s1 where s1.doc_id = s.doc' +
        '_id and s1.carriage_id = s.carriage_id) as amount_on_car,'
      
        '(select cr.pnum+'#39', '#39'+isnull((select code from carriagekinds ck w' +
        'here ck.id = cr.kind_id),'#39#39')+'#39', '#39'+isnull((select model_code from' +
        ' carriagemodels cm where cm.id = cr.model_id),'#39#39')) as car_text,'
      
        '(select code from counteragents c1 where c1.id = v.owner_id) as ' +
        'owner_code, '
      
        '(select code from objectstatekinds sk where sk.id = v.state_id) ' +
        'as state_code,'
      
        '(select name from cargotypes t where t.id = c.type_id) as cargot' +
        'ype_code,'
      
        '(select code from carriagekinds ck where ck.id = cr.kind_id) as ' +
        'carkind_code,'
      
        '(select model_code from carriagemodels cm where cm.id = cr.model' +
        '_id) as model_code,'
      
        '(select name from mtukinds m where m.id = s.mtukind_id) as mtu_c' +
        'ode,'
      
        '(select name from picturekinds p where p.id = s.picturekind_id) ' +
        'as picture_code,'
      
        '(select code from counteragents cn where cn.id = t.consignee_id)' +
        ' as consignee_code, '
      
        '(select code from counteragents cn where cn.id = t.payer_id) as ' +
        'payer_code, '
      'o.base_doc_text, o.extra_services'
      'from docdispatchspec s'
      'inner join docload l on (l.id = s.docload_id)'
      'inner join tasks t on (t.id = l.task_id)'
      'inner join containers cn on (s.container_id = cn.id)'
      'inner join objects v on (cn.id = v.id)'
      'inner join carriages cr on (cr.id = s.carriage_id) '
      'inner join cargos c on (s.cargo_id = c.id) '
      'left outer join'
      '('
      'select s.id, '
      
        'replace(substring(o.extra_services,2,450),'#39','#39','#39'; '#39') as extra_ser' +
        'vices,'
      
        'isnull(d.doc_number,'#39#39')+'#39' '#1086#1090' '#39'+convert(varchar, d.doc_date, 104)' +
        ' as base_doc_text'
      
        'from documents d, docorder o, docorderspec s where d.id= o.id an' +
        'd d.id = s.doc_id '
      ') o on (o.id = l.orderspec_id) '
      'where s.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 320
    Top = 456
  end
  object meSpec: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvSpec
    AfterOpen = meSpecAfterOpen
    BeforeEdit = meSpecBeforeEdit
    AfterPost = meSpecAfterPost
    AfterDelete = meSpecAfterDelete
    Left = 368
    Top = 456
  end
  object dsSpec: TDataSource
    DataSet = meSpec
    Left = 424
    Top = 456
  end
  object pmSpec: TPopupMenu
    Left = 532
    Top = 456
    object N22: TMenuItem
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1103#1095#1077#1081#1082#1080
      ShortCut = 46
      OnClick = N22Click
    end
    object N21: TMenuItem
      Caption = '-'
    end
    object N15: TMenuItem
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1052#1058#1059' '#1080' '#1088#1080#1089#1091#1085#1086#1082' '#1087#1086' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1084
      OnClick = N15Click
    end
    object N16: TMenuItem
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1082#1074#1080#1090#1072#1085#1094#1080#1102' '#1087#1086' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1084
      OnClick = N16Click
    end
    object N17: TMenuItem
      Caption = '-'
    end
    object N24: TMenuItem
      Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1079#1072#1103#1074#1082#1091
      OnClick = N24Click
    end
    object N23: TMenuItem
      Caption = '-'
    end
    object N20: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ShortCut = 116
      OnClick = N20Click
    end
  end
end
