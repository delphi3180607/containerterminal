﻿inherited FormCatalogs: TFormCatalogs
  Caption = #1050#1072#1090#1072#1083#1086#1075#1080
  ClientWidth = 554
  ExplicitWidth = 570
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Width = 554
    ExplicitWidth = 554
    inherited btnOk: TButton
      Left = 319
      ExplicitLeft = 319
    end
    inherited btnCancel: TButton
      Left = 438
      ExplicitLeft = 438
    end
  end
  inherited plAll: TPanel
    Width = 554
    ExplicitWidth = 554
    inherited plTop: TPanel
      Width = 554
      ExplicitWidth = 554
      inherited btTool: TPngSpeedButton
        Left = 502
        ExplicitLeft = 409
      end
    end
    inherited dgData: TDBGridEh
      Width = 554
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'catalog_name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 413
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'select f.*, f.folder_name as catalog_name from folders f where  ' +
        'rtrim(folder_section) = rtrim( :folder_section0) order by folder' +
        '_name')
    SelectCommand.Parameters = <
      item
        Name = 'folder_section0'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update folders'
      'set'
      '  parent_id = :parent_id,'
      '  folder_name = :folder_name'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'parent_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'folder_name'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into folders'
      '  (parent_id, folder_name, folder_section)'
      'values'
      '  (:parent_id, :folder_name, :folder_section0)')
    InsertCommand.Parameters = <
      item
        Name = 'parent_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'folder_name'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'folder_section0'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from folders where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select f.*, f.folder_name as catalog_name from folders f where f' +
        '.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
  inherited meData: TMemTableEh
    BeforeOpen = meDataBeforeOpen
  end
  inherited drvForms: TADODataDriverEh
    Left = 76
    Top = 266
  end
  inherited meForms: TMemTableEh
    Left = 124
    Top = 266
  end
  inherited dsForms: TDataSource
    Left = 172
    Top = 266
  end
end
