﻿unit StorePrices;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand,
  EXLReport, PngBitBtn, Vcl.ImgList, PngImageList;

type
  TFormStorePrices = class(TFormGrid)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    sbDeleteCust: TPngSpeedButton;
    sbExcelCust: TPngSpeedButton;
    sbAddCust: TPngSpeedButton;
    sbEditCust: TPngSpeedButton;
    Bevel3: TBevel;
    dgCustomers: TDBGridEh;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    dgTypes: TDBGridEh;
    drvTypes: TADODataDriverEh;
    meTypes: TMemTableEh;
    dsTypes: TDataSource;
    drvCust: TADODataDriverEh;
    meCust: TMemTableEh;
    dsCust: TDataSource;
    Panel2: TPanel;
    sbDeleteType: TPngSpeedButton;
    sbExcelType: TPngSpeedButton;
    sbAddType: TPngSpeedButton;
    sbEditType: TPngSpeedButton;
    Bevel1: TBevel;
    cbShowHistory: TCheckBox;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    procedure btExcelClick(Sender: TObject);
    procedure sbAddTypeClick(Sender: TObject);
    procedure sbAddCustClick(Sender: TObject);
    procedure sbDeleteTypeClick(Sender: TObject);
    procedure sbDeleteCustClick(Sender: TObject);
    procedure sbEditTypeClick(Sender: TObject);
    procedure sbEditCustClick(Sender: TObject);
    procedure meCustBeforePost(DataSet: TDataSet);
    procedure meDataBeforePost(DataSet: TDataSet);
    procedure meTypesBeforePost(DataSet: TDataSet);
    procedure sbAddClick(Sender: TObject);
    procedure sbDeleteClick(Sender: TObject);
    procedure sbEditClick(Sender: TObject);
    procedure dsDataStateChange(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormStorePrices: TFormStorePrices;

implementation

{$R *.dfm}

uses SPTypes, SPCust, SPDays;

procedure TFormStorePrices.btExcelClick(Sender: TObject);
begin
  dgData.SetFocus;
  inherited btExcelClick(nil);
end;

procedure TFormStorePrices.dsDataStateChange(Sender: TObject);
begin
  inherited;
  if meData.State in [dsInsert] then
    meData.FieldByName('price').Value := meCust.FieldByName('price_default').AsFloat;
end;

procedure TFormStorePrices.Init;
begin
  meTypes.Open;
  meCust.Open;
  self.AddEditForm(dgTypes,FormEditSPTypes, nil, meCust);
  self.AddEditForm(dgCustomers,FormEditSPCusts, meTypes, meData);
  self.AddEditForm(dgData,FormEditSPDays, meCust);
  inherited;
end;

procedure TFormStorePrices.meCustBeforePost(DataSet: TDataSet);
begin
  meCust.FieldByName('type_id').Value := meTypes.FieldByName('id').AsInteger;
  inherited meDataBeforePost(DataSet);
end;

procedure TFormStorePrices.meDataBeforePost(DataSet: TDataSet);
begin
  meData.FieldByName('pricecust_id').Value := meCust.FieldByName('id').AsInteger;
  inherited meDataBeforePost(DataSet);
end;

procedure TFormStorePrices.meTypesBeforePost(DataSet: TDataSet);
begin
  inherited meDataBeforePost(DataSet);
end;

procedure TFormStorePrices.sbAddClick(Sender: TObject);
begin
  dgData.SetFocus;
  inherited;
end;

procedure TFormStorePrices.sbAddCustClick(Sender: TObject);
begin
  dgCustomers.SetFocus;
  inherited sbAddClick(nil);
end;

procedure TFormStorePrices.sbAddTypeClick(Sender: TObject);
begin
  dgTypes.SetFocus;
  inherited sbAddClick(nil);
end;

procedure TFormStorePrices.sbDeleteClick(Sender: TObject);
begin
  dgData.SetFocus;
  inherited;
end;

procedure TFormStorePrices.sbDeleteCustClick(Sender: TObject);
begin
  dgCustomers.SetFocus;
  inherited sbDeleteClick(nil);
end;

procedure TFormStorePrices.sbDeleteTypeClick(Sender: TObject);
begin
  dgTypes.SetFocus;
  inherited sbDeleteClick(nil);
end;

procedure TFormStorePrices.sbEditClick(Sender: TObject);
begin
  dgData.SetFocus;
  inherited;
end;

procedure TFormStorePrices.sbEditCustClick(Sender: TObject);
begin
  dgCustomers.SetFocus;
  inherited sbEditClick(nil);
end;

procedure TFormStorePrices.sbEditTypeClick(Sender: TObject);
begin
  dgTypes.SetFocus;
  inherited sbEditClick(nil);
end;

end.
