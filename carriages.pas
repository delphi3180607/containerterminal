﻿unit carriages;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, objects, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport;

type
  TFormCarriages = class(TFormObjects)
    N8: TMenuItem;
    N9: TMenuItem;
    procedure N9Click(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure AssignSelField(colname: string); override;
  public
    procedure Init; override;
  end;

var
  FormCarriages: TFormCarriages;

implementation

{$R *.dfm}

uses editcarriage, techconditions;


procedure TFormCarriages.AssignSelField(colname: string);
begin

  selfield := '';
  if colname = 'date_income' then selfield := 'date_income';
  if colname = 'car_paper_number' then selfield := 'car_paper_number';

end;


procedure TFormCarriages.Init;
begin
  self.formEdit := FormEditCarriage;
  FormTechConditions.meData.Open;
  inherited;
  isbiglist := true;
  tablename := 'objects';
end;


procedure TFormCarriages.N9Click(Sender: TObject);
begin
  ClearCells;
end;

end.
