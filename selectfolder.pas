﻿unit selectfolder;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, DBCtrlsEh;

type
  TFormSelectFolder = class(TFormEdit)
    DBGridEh1: TDBGridEh;
    drvFolders: TADODataDriverEh;
    meFolders: TMemTableEh;
    dsFolders: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSelectFolder: TFormSelectFolder;

implementation

{$R *.dfm}

end.
