﻿inherited FormEditColumn: TFormEditColumn
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1082#1086#1083#1086#1085#1082#1080
  ClientHeight = 394
  ExplicitHeight = 422
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 16
    Top = 14
    Width = 47
    Height = 16
    Caption = #1043#1088#1091#1087#1087#1072
  end
  object Label2: TLabel [1]
    Left = 16
    Top = 70
    Width = 63
    Height = 16
    Caption = #1048#1084#1103' '#1087#1086#1083#1103
  end
  object Label3: TLabel [2]
    Left = 16
    Top = 126
    Width = 70
    Height = 16
    Caption = #1047#1072#1075#1086#1083#1086#1074#1086#1082
  end
  object Label4: TLabel [3]
    Left = 16
    Top = 182
    Width = 47
    Height = 16
    Caption = #1050#1088#1072#1090#1082#1086
  end
  object Label5: TLabel [4]
    Left = 16
    Top = 246
    Width = 57
    Height = 16
    Caption = #1055#1086#1088#1103#1076#1086#1082
  end
  inherited plBottom: TPanel
    Top = 353
    TabOrder = 6
    ExplicitTop = 353
  end
  object edFieldName: TDBEditEh [6]
    Left = 16
    Top = 91
    Width = 305
    Height = 24
    DataField = 'field_name'
    DataSource = FormTool.dcColumns
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object edCaption: TDBEditEh [7]
    Left = 16
    Top = 147
    Width = 505
    Height = 24
    DataField = 'column_title'
    DataSource = FormTool.dcColumns
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edShortCaption: TDBEditEh [8]
    Left = 16
    Top = 203
    Width = 305
    Height = 24
    DataSource = FormTool.dcColumns
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object cbGroup: TDBComboBoxEh [9]
    Left = 16
    Top = 35
    Width = 305
    Height = 24
    DataSource = FormTool.dcColumns
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object cbShowInGrid: TDBCheckBoxEh [10]
    Left = 16
    Top = 308
    Width = 241
    Height = 17
    Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1074' '#1089#1087#1080#1089#1082#1077
    DataField = 'show_in_grid'
    DataSource = FormTool.dcColumns
    DynProps = <>
    TabOrder = 5
  end
  object neOrder: TDBNumberEditEh [11]
    Left = 16
    Top = 264
    Width = 121
    Height = 24
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    TabOrder = 4
    Visible = True
  end
end
