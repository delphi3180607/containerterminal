﻿unit editinspection;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBGridEh, Vcl.Buttons, DBLookupEh, Vcl.Mask,
  DBCtrlsEh, functions;

type
  TFormEditInspection = class(TFormEdit)
    dtFactDate: TDBDateTimeEditEh;
    Label5: TLabel;
    luPerson: TDBLookupComboboxEh;
    Label4: TLabel;
    sbPersons: TSpeedButton;
    procedure sbPersonsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditInspection: TFormEditInspection;

implementation

{$R *.dfm}

uses persons;

procedure TFormEditInspection.sbPersonsClick(Sender: TObject);
begin
  SFD(FormPersons, self.luPerson);
end;

end.
