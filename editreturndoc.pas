﻿unit editreturndoc;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBCtrlsEh, Vcl.StdCtrls, Vcl.Mask,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEditReturnDoc = class(TFormEdit)
    dtReturnDoc: TDBDateTimeEditEh;
    Label1: TLabel;
    cbReturnDoc: TDBCheckBoxEh;
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditReturnDoc: TFormEditReturnDoc;

implementation

{$R *.dfm}

procedure TFormEditReturnDoc.btnOkClick(Sender: TObject);
begin
  if self.cbReturnDoc.Checked then
  if self.dtReturnDoc.Value = null then
  begin
    ShowMessage('Заполните дату возврата. Это нужно для отчета.');
    self.ModalResult := mrNone;
  end;
end;

end.
