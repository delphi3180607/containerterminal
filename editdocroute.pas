﻿unit editdocroute;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBGridEh, DBCtrlsEh, Vcl.Mask, DBLookupEh;

type
  TFormEditDocRoute = class(TFormEdit)
    Label1: TLabel;
    luCreateDocType: TDBLookupComboboxEh;
    Label3: TLabel;
    edMethod: TDBEditEh;
    cbIsRestricted: TDBCheckBoxEh;
    edExcludeStatesIds: TDBEditEh;
    Label2: TLabel;
    Label4: TLabel;
    edPrefIds: TDBEditEh;
    cbCopyPrevState: TDBCheckBoxEh;
    cbCreateNewObject: TDBCheckBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDocRoute: TFormEditDocRoute;

implementation

{$R *.dfm}

end.
