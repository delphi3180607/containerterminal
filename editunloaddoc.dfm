﻿inherited FormEditUnloadDoc: TFormEditUnloadDoc
  Caption = #1056#1072#1079#1075#1088#1091#1079#1082#1072
  ClientHeight = 203
  ClientWidth = 516
  ExplicitWidth = 522
  ExplicitHeight = 231
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 15
    Top = 20
    Width = 119
    Height = 16
    Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object Label3: TLabel [1]
    Left = 197
    Top = 20
    Width = 110
    Height = 16
    Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object Label4: TLabel [2]
    Left = 349
    Top = 20
    Width = 127
    Height = 16
    Caption = #1044#1072#1090#1072' '#1091#1074#1077#1076#1086#1084#1083#1077#1085#1080#1103
  end
  object Label2: TLabel [3]
    Left = 15
    Top = 86
    Width = 104
    Height = 16
    Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1081
  end
  object sbForwarder: TSpeedButton [4]
    Left = 475
    Top = 108
    Width = 31
    Height = 24
    Caption = '...'
    OnClick = sbForwarderClick
  end
  inherited plBottom: TPanel
    Top = 162
    Width = 516
    TabOrder = 4
    ExplicitTop = 162
    ExplicitWidth = 516
    inherited btnCancel: TButton
      Left = 400
      ExplicitLeft = 400
    end
    inherited btnOk: TButton
      Left = 281
      ExplicitLeft = 281
    end
  end
  object edDocNumber: TDBEditEh [6]
    Left = 15
    Top = 42
    Width = 147
    Height = 24
    DataField = 'doc_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object dtDocDate: TDBDateTimeEditEh [7]
    Left = 197
    Top = 42
    Width = 121
    Height = 24
    DataField = 'doc_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object dtAlertDate: TDBDateTimeEditEh [8]
    Left = 349
    Top = 42
    Width = 121
    Height = 24
    DataField = 'alert_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object luPerson: TDBLookupComboboxEh [9]
    Left = 15
    Top = 108
    Width = 455
    Height = 24
    DynProps = <>
    DataField = 'person_id'
    DataSource = dsLocal
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    KeyField = 'id'
    ListField = 'person_name'
    ListSource = FormPersons.dsData
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 255
    Top = 75
  end
  inherited qrAux: TADOQuery
    Left = 295
    Top = 75
  end
end
