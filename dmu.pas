﻿unit dmu;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Data.Win.ADODB, DBSQLLookUp;

type
  Tdm = class(TDataModule)
    connMain: TADOConnection;
    qrAux: TADOQuery;
    qrCommon: TADOQuery;
    spUpdateRegisterDocument: TADOStoredProc;
    spConfirmDoc: TADOStoredProc;
    spCancelConfirmDoc: TADOStoredProc;
    spImportExternalData: TADOStoredProc;
    qrCounteragents: TADOQuery;
    dsCounteragents: TDataSource;
    spAdd2SelectList: TADOStoredProc;
    spClearSelectList: TADOStoredProc;
    spPrepareTransformDocuments: TADOStoredProc;
    spTransformDocuments: TADOStoredProc;
    spGetPurposeDocumentTypes: TADOStoredProc;
    spMoveToFolder: TADOStoredProc;
    qrCarriages: TADOQuery;
    dsCarriages: TDataSource;
    qrServices: TADOQuery;
    dsServices: TDataSource;
    spConfirmInspectDoc: TADOStoredProc;
    spCancelConfirmInspectDoc: TADOStoredProc;
    spCreateFilter: TADOStoredProc;
    spUpdateDeliveryType: TADOStoredProc;
    spUnionCounteragents: TADOStoredProc;
    ssCounteragents: TADOLookUpSqlSet;
    spUpdateRegisterContainer: TADOStoredProc;
    ssCargoType: TADOLookUpSqlSet;
    spUpdateRegisterCargo: TADOStoredProc;
    spRegisterCarriagesFromSheet: TADOStoredProc;
    spImportCarLinks: TADOStoredProc;
    spRegisterDataChange: TADOStoredProc;
    spSetParentCounteragent: TADOStoredProc;
    spUpdateRegisterCarriageFull: TADOStoredProc;
    spCreateCounteragent: TADOStoredProc;
    spGenerateUnloadDocs: TADOStoredProc;
    spCopyCons2Payer: TADOStoredProc;
    spSetCarIncomeDate: TADOStoredProc;
    spSetAlertDate: TADOStoredProc;
    spMakeAlertMessages: TADOStoredProc;
    spChangeCarriage: TADOStoredProc;
    spTransformDocumentAdd: TADOStoredProc;
    spAbolishDoc: TADOStoredProc;
    spMoveToOrder: TADOStoredProc;
    ssContainerKind: TADOLookUpSqlSet;
    spCheckNonConfirmedChildDocs: TADOStoredProc;
    spGetDocumentTypeFromPassOp: TADOStoredProc;
    spPrepareTransformFromPassOp: TADOStoredProc;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dm: Tdm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
