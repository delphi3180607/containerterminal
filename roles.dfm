﻿inherited FormRoles: TFormRoles
  Caption = #1056#1086#1083#1080
  ClientWidth = 917
  ExplicitWidth = 933
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Width = 917
    ExplicitWidth = 917
    inherited btnOk: TButton
      Left = 682
      ExplicitLeft = 682
    end
    inherited btnCancel: TButton
      Left = 801
      ExplicitLeft = 801
    end
  end
  inherited plAll: TPanel
    Width = 917
    ExplicitWidth = 917
    inherited plTop: TPanel
      Width = 917
      ExplicitWidth = 917
      inherited btTool: TPngSpeedButton
        Left = 865
        ExplicitLeft = 818
      end
    end
    inherited dgData: TDBGridEh
      Width = 917
      OptionsEh = [dghHighlightFocus, dghClearSelection, dghFitRowHeightToText, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      RowHeight = 2
      RowLines = 1
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1056#1086#1083#1100
          Width = 125
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'readonly_sections'
          Footers = <>
          Title.Caption = #1056#1072#1079#1076#1077#1083#1099' '#1090#1086#1083#1100#1082#1086' '#1095#1090#1077#1085#1080#1077
          Width = 186
          WordWrap = True
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'confirmrestrict_sections'
          Footers = <>
          Title.Caption = #1056#1072#1079#1076#1077#1083#1099' '#1089' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1077#1084' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1103
          Width = 196
          WordWrap = True
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'unavalable_sections'
          Footers = <>
          Title.Caption = #1053#1077#1076#1086#1089#1090#1091#1087#1085#1099#1077' '#1088#1072#1079#1076#1077#1083#1099
          Width = 137
          WordWrap = True
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'allowdelete_sections'
          Footers = <>
          Title.Caption = 
            #1056#1072#1079#1076#1077#1083#1099' - '#1088#1072#1079#1088#1077#1096#1077#1085#1086' '#1091#1076#1072#1083#1077#1085#1080#1077' ('#1080#1089#1082#1083#1102#1095#1072#1077#1090#1089#1103' '#1080#1079' '#1088#1072#1079#1076#1077#1083#1086#1074' '#1090#1086#1083#1100#1082#1086' '#1085#1072' ' +
            #1095#1090#1077#1085#1080#1077')'
          Width = 174
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'allow_edit_price'
          Footers = <>
          Title.Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1094#1077#1085#1099
          Width = 119
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'restrict_edit_dicts'
          Footers = <>
          Title.Caption = #1047#1072#1087#1088#1077#1097#1077#1085#1086' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
          Width = 140
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'restrict_edit_objects'
          Footers = <>
          Title.Caption = #1047#1072#1087#1088#1077#1096#1077#1085#1086' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1099' '#1080' '#1074#1072#1075#1086#1085#1099
          Width = 146
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from roles order by name')
    UpdateCommand.CommandText.Strings = (
      'update roles'
      'set'
      '  name = :name,'
      '  readonly_sections = :readonly_sections,'
      '  confirmrestrict_sections = :confirmrestrict_sections,'
      '  unavalable_sections = :unavalable_sections,'
      '  allow_edit_price = :allow_edit_price,'
      '  restrict_edit_dicts = :restrict_edit_dicts,'
      '  restrict_edit_objects = :restrict_edit_objects,'
      '  allowdelete_sections = :allowdelete_sections'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'name'
        Size = -1
        Value = Null
      end
      item
        Name = 'readonly_sections'
        Size = -1
        Value = Null
      end
      item
        Name = 'confirmrestrict_sections'
        Size = -1
        Value = Null
      end
      item
        Name = 'unavalable_sections'
        Size = -1
        Value = Null
      end
      item
        Name = 'allow_edit_price'
        Size = -1
        Value = Null
      end
      item
        Name = 'restrict_edit_dicts'
        Size = -1
        Value = Null
      end
      item
        Name = 'restrict_edit_objects'
        Size = -1
        Value = Null
      end
      item
        Name = 'allowdelete_sections'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into roles'
      
        '  (name, readonly_sections, confirmrestrict_sections, unavalable' +
        '_sections, '
      
        '   allow_edit_price, restrict_edit_dicts, restrict_edit_objects,' +
        ' allowdelete_sections)'
      'values'
      
        '  (:name, :readonly_sections, :confirmrestrict_sections, :unaval' +
        'able_sections, '
      
        '   :allow_edit_price, :restrict_edit_dicts, :restrict_edit_objec' +
        'ts, :allowdelete_sections)')
    InsertCommand.Parameters = <
      item
        Name = 'name'
        Size = -1
        Value = Null
      end
      item
        Name = 'readonly_sections'
        Size = -1
        Value = Null
      end
      item
        Name = 'confirmrestrict_sections'
        Size = -1
        Value = Null
      end
      item
        Name = 'unavalable_sections'
        Size = -1
        Value = Null
      end
      item
        Name = 'allow_edit_price'
        Size = -1
        Value = Null
      end
      item
        Name = 'restrict_edit_dicts'
        Size = -1
        Value = Null
      end
      item
        Name = 'restrict_edit_objects'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from roles where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from roles where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
end
