﻿unit EditOrderInWait;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh;

type
  TFormEditOrderInWait = class(TFormEdit)
    cbInWait: TDBCheckBoxEh;
    dtInWaitStop: TDBDateTimeEditEh;
    Label5: TLabel;
    procedure dtInWaitStopChange(Sender: TObject);
    procedure cbInWaitClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditOrderInWait: TFormEditOrderInWait;

implementation

{$R *.dfm}

procedure TFormEditOrderInWait.cbInWaitClick(Sender: TObject);
begin
  if cbInWait.Checked then
  dtInWaitStop.Value := null;
end;

procedure TFormEditOrderInWait.dtInWaitStopChange(Sender: TObject);
begin
  if dtInWaitStop.Value<>null then
  cbInWait.Checked := false;
end;

end.
