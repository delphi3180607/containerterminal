﻿inherited FormCarriagesList: TFormCarriagesList
  ActiveControl = dgData
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1074#1072#1075#1086#1085#1086#1074
  ClientHeight = 567
  ClientWidth = 886
  ExplicitWidth = 902
  ExplicitHeight = 605
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 526
    Width = 886
    ExplicitTop = 526
    ExplicitWidth = 886
    inherited btnOk: TButton
      Left = 651
      ExplicitLeft = 651
    end
    inherited btnCancel: TButton
      Left = 770
      ExplicitLeft = 770
    end
  end
  inherited plAll: TPanel
    Width = 886
    Height = 526
    ExplicitWidth = 886
    ExplicitHeight = 526
    inherited plTop: TPanel
      Width = 886
      ExplicitWidth = 886
      inherited sbDelete: TPngSpeedButton
        Left = 78
        ExplicitLeft = 79
      end
      inherited btFilter: TPngSpeedButton
        Left = 188
        OnClick = btFilterClick
        ExplicitLeft = 196
        ExplicitTop = -3
      end
      inherited btExcel: TPngSpeedButton
        Left = 156
        ExplicitLeft = 157
      end
      inherited sbAdd: TPngSpeedButton
        Left = 46
        ExplicitLeft = 47
      end
      inherited sbEdit: TPngSpeedButton
        Left = 110
        ExplicitLeft = 115
      end
      inherited Bevel2: TBevel
        Left = 152
        ExplicitLeft = 152
      end
      inherited btTool: TPngSpeedButton
        Left = 848
        ExplicitLeft = 834
      end
      object sbImport2: TPngSpeedButton [7]
        AlignWithMargins = True
        Left = 3
        Top = 2
        Width = 40
        Height = 24
        Hint = #1048#1084#1087#1086#1088#1090' '#1080#1079' Excel'
        Margins.Top = 2
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbImport2Click
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000014000000140803000000BA57ED
          3F0000000373424954080808DBE14FE0000000BD504C5445FFFFFFFDFEFEFCFD
          FEFBFCFDFBFCFCFBFAFAF8FAFCF9FAFBF6F8FAF5F7FAF3F6F9F3F5F7F1F3F5F0
          F3F7EFF1F4ECF0F3EAEFF4ECEEF2FFFF00E8ECF0E9ECEFE2E8EFE3E7EBE2E6EA
          E0E5EBE1E5E9DDE2E7D6DEE6D3DBE3CACED4C5CED7C0CBD6BFC9D2BCC6D0BEC0
          C4B3BFCCA6B4C25ACD2D85909C84909D808A967D889312AE1E78828D737C8770
          7A840AA30D6B747E666E78106810006500006400006300005E00005C00005A00
          36414C005300004C00004900004600004500004400B8D3A8CE0000003F74524E
          5300FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          486C17C1000000097048597300000B1200000B1201D2DD7EFC0000001C744558
          74536F6674776172650041646F62652046697265776F726B732043533571B5E3
          3600000016744558744372656174696F6E2054696D650030352F32322F31326B
          390341000000D04944415478DA8DD03B0BC2301007F0ABD6D6D28AA28B8B8308
          3EE36329F8F99D842AF8985A51297510B4A22831A5949A263593A2992EBFC0E5
          7F27C187237DC596B8DA025BB6405E526C1E6A0203C57923CEAA4ACE8008DFCA
          4581009A96CDC597C760554FB11D7B0CF109C90B0839765CD6131F914B6344D1
          3CC1DE1E67641508722B3E7D6C581C21920B3E72AB2C80CE10851E45F0CD0743
          6596607F9BF634AFBA468B4982C30D96206F18E46C92F5FBF7E10602304A494E
          D3EA2ED3D9470E47727FD676624B6331FBF4C73EFFC2178EE54B155E6C005A00
          00000049454E44AE426082}
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitHeight = 37
      end
      object sbClearFilter: TPngSpeedButton [8]
        AlignWithMargins = True
        Left = 221
        Top = 1
        Width = 34
        Height = 26
        Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1080#1083#1100#1090#1088
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbClearFilterClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000002334944415478DA63FCFFFF3F032580
          91A1FD0A7E13FEA3A8C66A8005903E7E26499541969785285B1F7FFEC36032EF
          3688690931136AC8C9445506691EA021606F01A518FF33349F7C0F56526326C8
          C008147AFAE50F83F9BC3B20694B864A9D130847810CF9FBF7F8E1247506492E
          16B873BBCF400C2833166478F6ED0F83EDBC9B0C0CCCCC60CD98BE0219F2F3C7
          F13D49DA0CE2DC2C60C9C6A3AFC02E4A3714667059709D8181950DAE197BB080
          0CF9F2F9F896140386EBA7CF322C58B78FE1C6DD67C0B08484A68682C4774B5D
          A56573EBE253188CD3B0852BC4109B67878F5FBD729321CEDB92212DD096414A
          949FE1DAFDE70CF3361E6558B8E5388393A9C69D5D27AEA9623520A676EEFA95
          BBCE041C9C55C2A0F6EF13039B800003AF820258EED981030CA7BFB13244B7AF
          640872345C8FD50059AFF2DFD9618E2CE5F1EE0C6F2F5C60D81910C0E0BE6103
          C3CD050B187E7DF8C0E000A40BFB5631ACDF7FE10F5603184DD2FFBFDFDFCFC0
          CFC309E6830CD9ECE0C02005C46E408340E0FCCDC70CA6B16D0C380D78BABD93
          4152841FCC3F909000B6F90DD020904B840D0C188E5DBACB6097D2C380D30BA1
          2EC62CBD85A160CD20007236B2774A365E64D877FAC61F9C81B8E9E0C580355D
          190CAEE69A28727FFFFD6398B1E61043C594750C810E38021114BF36663AFFCF
          5EBCC9E065ADCB90E46FCDA02E2FCE70F9CE538659EB0E3300A38FC1D90C1A8D
          D8B233300CC074B2BFF5ECE397EF45DF78F0821326A72E2FF1DD4A4F69293021
          A5822C02009244E8A4AC1C3E1E0000000049454E44AE426082}
        ExplicitLeft = 292
        ExplicitTop = -2
      end
      inherited plCount: TPanel
        Left = 651
        ExplicitLeft = 651
      end
    end
    inherited dgData: TDBGridEh
      Top = 46
      Width = 886
      Height = 480
      IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      ReadOnly = False
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_income'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1087#1086#1076#1072#1095#1080
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 123
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_paper_number'
          Footers = <>
          Title.Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086#1076#1072#1095#1080
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 88
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num'
          Footers = <>
          Title.Caption = #1050#1055
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Verdana'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 52
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'load_plan_text'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          Footers = <>
          Title.Caption = #1042' '#1087#1083#1072#1085#1077
          Width = 54
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'pnum'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 112
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          Title.Caption = #1042#1080#1076
          Width = 73
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          Title.Caption = #1058#1077#1082#1091#1097#1080#1081' '#1089#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
          Width = 128
        end
        item
          CellButtons = <>
          Color = 13499846
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1058#1077#1082#1091#1097#1077#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 105
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_next_repair'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1095#1077#1088#1077#1076'. '#1088#1077#1084#1086#1085#1090#1072
          Width = 70
        end
        item
          CellButtons = <>
          Color = 13036283
          DynProps = <>
          EditButtons = <>
          FieldName = 'techcond_code'
          Footers = <>
          Title.Caption = #1058#1077#1093'. '#1089#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 84
        end
        item
          CellButtons = <>
          Checkboxes = False
          Color = 13036283
          DisplayFormat = 'dd.mm.yyyy'
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_inspection'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1090#1077#1093'. '#1089#1086#1089#1090#1086#1103#1085#1080#1103
          Width = 104
        end
        item
          CellButtons = <>
          Checkboxes = True
          Color = 13036283
          DynProps = <>
          EditButtons = <>
          FieldName = 'isdefective'
          Footers = <>
          Title.Caption = #1053#1077'- '#1080#1089#1087#1088#1072'- '#1074#1077#1085'?'
          Width = 57
        end
        item
          CellButtons = <>
          Checkboxes = True
          Color = 8454143
          DynProps = <>
          EditButtons = <>
          FieldName = 'need_inspection'
          Footers = <>
          Title.Caption = #1054#1089#1084#1086#1090#1088
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'model_code'
          Footers = <>
          Title.Caption = #1052#1086#1076#1077#1083#1100
          Width = 142
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'start_datetime'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103
          Width = 82
        end>
    end
    inherited plHint: TPanel
      TabOrder = 3
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object plStatus: TPanel
      Left = 0
      Top = 29
      Width = 886
      Height = 17
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 2
    end
  end
  inherited pmGrid: TPopupMenu
    object N12: TMenuItem [3]
      Caption = '-'
    end
    object N13: TMenuItem [4]
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1103#1095#1077#1081#1082#1080
      ShortCut = 46
      OnClick = N13Click
    end
    object N8: TMenuItem [5]
      Caption = '-'
    end
    object N9: TMenuItem [6]
      Caption = #1055#1088#1086#1089#1090#1072#1074#1080#1090#1100' '#1076#1072#1090#1091'/'#1074#1088#1077#1084#1103', '#1074#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086#1076#1072#1095#1080', '#8470' '#1050#1055
      OnClick = N9Click
    end
    object N11: TMenuItem [7]
      Caption = #1055#1088#1086#1089#1090#1072#1074#1080#1090#1100' '#1089#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082#1072' '#1087#1086' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1084
      OnClick = N11Click
    end
    object N10: TMenuItem [8]
      Caption = #1055#1088#1086#1089#1090#1072#1074#1080#1090#1100' '#1084#1086#1076#1077#1083#1100
      OnClick = N10Click
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select c.*, o.start_datetime,  o.id as object_id, '
      
        'isnull(c.train_num, (select train_num from doccargosheet sh wher' +
        'e sh.id = (select max(id) from doccargosheet sh1 where sh1.carri' +
        'age_num = c.pnum))) as train_num,'
      '('
      'select '#39'+'#39' where exists '
      
        '(select 1 from docloadjoint j, docload l left outer join v_lasto' +
        'bjectstates ls on (ls.task_id = l.task_id and ls.object_id = l.c' +
        'ontainer_id) '
      
        'where j.id = l.joint_id and j.carriage_id = c.id and isnull(ls.o' +
        'bject_state_id,0) <> 12)'
      ') as load_plan_text,'
      
        '(select code from counteragents c1 where c1.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from carriagekinds k where k.id = c.kind_id) as kin' +
        'd_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select model_code from carriagemodels m where m.id = c.model_id' +
        ') as model_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code'
      
        'from objects o, carriages c where o.id = c.id and o.object_type ' +
        '= '#39'carriage'#39
      'and dbo.filter_objects(:guid, c.pnum) = 1'
      'order by date_income desc')
    SelectCommand.Parameters = <
      item
        Name = 'guid'
        Attributes = [paSigned]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update carriages'
      'set'
      '  pnum = :pnum,'
      '  kind_id = :kind_id,'
      '  techcond_id = :techcond_id,'
      '  model_id = :model_id,'
      '  note = :note,'
      '  isdefective = :isdefective,'
      '  need_inspection = :need_inspection,'
      '  date_income= :date_income,'
      '  car_paper_number= :car_paper_number,'
      '  train_num = :train_num,'
      '  date_next_repair = :date_next_repair'
      ' where'
      '  id = :id'
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'pnum'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'techcond_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'model_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'isdefective'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'need_inspection'
        Size = -1
        Value = Null
      end
      item
        Name = 'date_income'
        Size = -1
        Value = Null
      end
      item
        Name = 'car_paper_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'train_num'
        Size = -1
        Value = Null
      end
      item
        Name = 'date_next_repair'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'begin'
      ''
      'declare @newid int;'
      ''
      'exec InsertObject '#39'carriage'#39', @newid output;'
      ''
      'insert into carriages'
      
        '  (id, pnum, kind_id, techcond_id, model_id, note, isdefective, ' +
        'need_inspection, date_next_repair)'
      'values'
      
        '  (@newid, :pnum, :kind_id, :techcond_id, :model_id, :note, :isd' +
        'efective, :need_inspection, :date_next_repair)'
      ''
      'end;')
    InsertCommand.Parameters = <
      item
        Name = 'pnum'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'techcond_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'model_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'isdefective'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'need_inspection'
        Size = -1
        Value = Null
      end
      item
        Name = 'date_next_repair'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from objects where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select c.*, o.start_datetime,  o.id as object_id, '
      
        'isnull(c.train_num, (select train_num from doccargosheet sh wher' +
        'e sh.id = (select max(id) from doccargosheet sh1 where sh1.carri' +
        'age_num = c.pnum))) as train_num,'
      '('
      'select '#39'+'#39' where exists '
      
        '(select 1 from docloadjoint j, docload l left outer join v_lasto' +
        'bjectstates ls on (ls.task_id = l.task_id and ls.object_id = l.c' +
        'ontainer_id) '
      
        'where j.id = l.joint_id and j.carriage_id = c.id and isnull(ls.o' +
        'bject_state_id,0) <> 12)'
      ') as load_plan_text,'
      
        '(select code from counteragents c1 where c1.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from carriagekinds k where k.id = c.kind_id) as kin' +
        'd_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select model_code from carriagemodels m where m.id = c.model_id' +
        ') as model_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code'
      
        'from objects o, carriages c where o.id = c.id and o.object_type ' +
        '= '#39'carriage'#39
      'and o.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
end
