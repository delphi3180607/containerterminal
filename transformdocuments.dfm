﻿inherited FormTransformDocuments: TFormTransformDocuments
  Caption = #1055#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1099#1081' '#1087#1088#1086#1089#1084#1086#1090#1088
  ClientHeight = 531
  ClientWidth = 889
  ExplicitWidth = 905
  ExplicitHeight = 569
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 490
    Width = 889
    Visible = True
    ExplicitTop = 490
    ExplicitWidth = 889
    inherited btnOk: TButton
      Left = 654
      Caption = #1057#1086#1079#1076#1072#1090#1100
      OnClick = btnOkClick
      ExplicitLeft = 654
    end
    inherited btnCancel: TButton
      Left = 773
      ExplicitLeft = 773
    end
  end
  inherited plAll: TPanel
    Width = 889
    Height = 490
    ExplicitWidth = 889
    ExplicitHeight = 490
    inherited plTop: TPanel
      Width = 889
      ExplicitWidth = 889
      inherited sbDelete: TPngSpeedButton
        Left = 126
        Action = nil
        Visible = False
        OnClick = nil
        ExplicitLeft = 125
      end
      inherited btFilter: TPngSpeedButton
        Left = 240
        Width = 38
        AllowAllUp = True
        Flat = False
        OnClick = btFilterClick
        ExplicitLeft = 240
        ExplicitTop = -3
        ExplicitWidth = 38
        ExplicitHeight = 29
      end
      inherited btExcel: TPngSpeedButton
        Left = 201
        Width = 39
        Visible = False
        ExplicitLeft = 196
        ExplicitWidth = 39
      end
      inherited sbAdd: TPngSpeedButton
        Left = 92
        Action = nil
        Visible = False
        OnClick = nil
        ExplicitLeft = 93
      end
      inherited sbEdit: TPngSpeedButton
        Left = 160
        Action = nil
        Visible = False
        OnClick = nil
        ExplicitLeft = 161
      end
      inherited Bevel2: TBevel
        Left = 197
        ExplicitLeft = 198
      end
      inherited btTool: TPngSpeedButton
        Left = 851
        ExplicitLeft = 742
      end
      object sbClearAll: TPngSpeedButton [7]
        AlignWithMargins = True
        Left = 47
        Top = 0
        Width = 44
        Height = 28
        Hint = #1054#1090#1073#1086#1088
        Margins.Left = 1
        Margins.Top = 0
        Margins.Right = 1
        Margins.Bottom = 1
        Align = alLeft
        Flat = True
        OnClick = sbClearAllClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000180000001808040000004A7EF5
          730000000467414D410000B18F0BFC610500000002624B47440000AA8D233200
          0000097048597300000DD700000DD70142289B780000000774494D4507E20A1A
          043828C76F03AE000000854944415478DA636458CD10C2000117180C190418DE
          33A08240860D0C1318F2A1BC358C0CFFE15217190C086A6040D6F08A419A410B
          A80D15E4334C625807D486450303C34D06610611340D3F19AE301823B8A81A88
          00100D410CCF89502B09741A548322C30322342830DC1FD530AA61E869B06578
          4284061986C364E5384431430C580300AF974A595AF8767B0000002574455874
          646174653A63726561746500323031382D31302D32365430343A35363A34302B
          30323A3030B343CB480000002574455874646174653A6D6F6469667900323031
          382D31302D32365430343A35363A34302B30323A3030C21E73F4000000197445
          5874536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A00
          00000049454E44AE426082}
        ExplicitLeft = 0
        ExplicitTop = 3
      end
      object sbCheckAll: TPngSpeedButton [8]
        AlignWithMargins = True
        Left = 1
        Top = 0
        Width = 44
        Height = 28
        Hint = #1054#1090#1073#1086#1088
        Margins.Left = 1
        Margins.Top = 0
        Margins.Right = 1
        Margins.Bottom = 1
        Align = alLeft
        Flat = True
        OnClick = sbCheckAllClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000180000001808040000004A7EF5
          730000000467414D410000B18F0BFC610500000002624B47440000AA8D233200
          0000097048597300000DD700000DD70142289B780000000774494D4507E20A1A
          04371FF84ABA6E000001574944415478DA9594CD2B446114879FD9D8D0882CA8
          492994494A29F20F300B3B362C7C2C251B258446531465216B323BB1938FA490
          B2542C909918522C2CB032A8F1BBD7CC8831EFBD736EE7BDE79EFB3EDDF33BA7
          FB7AB02CC4205ECCF6CA0213E05158C515EEAC9A8805B4B2ADB58375C3D676D6
          B406D8F901CA78B45FD53189CF8EDE54C4059D562194F2900DD8A389189F8A8A
          E9661EBFD4C54D408C4B658AE48774314B9029F317BE811ACE39A392280D7CB8
          01A05F0A12347262670D408417C6758FCBF3D914E4004C339A6CE52D47F4DAF2
          8D00AA3AD5D60339B900FB2AAC84273330C3487ABE41F218D21CDE4D40946789
          4EA8F60A4E39D620036EDAEAD7C6103DD4EABA71037825B85E4FC3CC39894E0D
          AE803085B438B7759766EEEDC1AD70CD96043B007EC6D26D5D6635FB1C96D830
          FC406DF4FD057E9B55942F236B00CAE577D980FF0E8145F94046367908E474CC
          7C01864EA2197B68FC470000002574455874646174653A637265617465003230
          31382D31302D32365430343A35353A33312B30323A3030F4C672E60000002574
          455874646174653A6D6F6469667900323031382D31302D32365430343A35353A
          33312B30323A3030859BCA5A0000001974455874536F66747761726500777777
          2E696E6B73636170652E6F72679BEE3C1A0000000049454E44AE426082}
        ExplicitLeft = 0
        ExplicitTop = 3
      end
      inherited plCount: TPanel
        Left = 717
        Width = 131
        Visible = False
        ExplicitLeft = 717
        ExplicitWidth = 131
        inherited edCount: TDBEditEh
          Left = 59
          ControlLabel.ExplicitLeft = -47
          ControlLabel.ExplicitTop = 8
          ControlLabel.ExplicitWidth = 103
          Visible = False
          ExplicitLeft = 59
        end
      end
      object edContainerNum: TDBEditEh
        AlignWithMargins = True
        Left = 368
        Top = 3
        Width = 221
        Height = 23
        Margins.Left = 90
        Align = alClient
        ControlLabel.Width = 76
        ControlLabel.Height = 13
        ControlLabel.Caption = #8470' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        DynProps = <>
        EditButtons = <>
        TabOrder = 1
        Visible = True
        OnKeyPress = edContainerNumKeyPress
        ExplicitHeight = 21
      end
      object btRequery: TButton
        AlignWithMargins = True
        Left = 595
        Top = 1
        Width = 119
        Height = 25
        Margins.Top = 1
        Align = alRight
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1080#1083#1100#1090#1088
        TabOrder = 2
        OnClick = btRequeryClick
      end
    end
    inherited dgData: TDBGridEh
      Top = 70
      Width = 889
      Height = 420
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      ReadOnly = False
      STFilter.InstantApply = False
      STFilter.Local = False
      STFilter.Visible = False
      TitleParams.MultiTitle = True
      OnApplyFilter = nil
      OnCellClick = dgDataCellClick
      OnColumnMoved = nil
      OnColWidthsChanged = nil
      OnDblClick = nil
      OnKeyPress = nil
      OnMouseDown = nil
      OnMouseMove = nil
      OnMouseUp = nil
      Columns = <
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'checkcolumn'
          Footers = <>
          Title.Caption = #1054#1090#1084#1077#1090#1082#1072
          Title.TitleButton = False
          Width = 71
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'docexists'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1059#1078#1077' '#1089#1086#1079#1076#1072#1085' ?'
          Width = 74
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doctype_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1058#1080#1087' '#1089#1086#1079#1076#1072#1074#1072#1077#1084#1086#1075#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 161
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dlvtype_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
          Width = 137
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cont_num'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
          Width = 117
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnum'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1055#1083#1086#1084#1073#1072
          Width = 122
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
          Width = 89
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1042#1080#1076
          Width = 155
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object plInfo: TPanel
      Left = 0
      Top = 29
      Width = 889
      Height = 41
      Align = alTop
      BevelOuter = bvLowered
      Caption = 
        #1059#1089#1090#1072#1085#1086#1074#1080#1090#1077' '#1086#1090#1084#1077#1090#1082#1080' '#1090#1086#1083#1100#1082#1086' '#1085#1072' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072#1093', '#1082#1086#1090#1086#1088#1099#1077' '#1076#1086#1083#1078#1085#1099' '#1087#1086#1087#1072#1089#1090#1100 +
        ' '#1074' '#1085#1086#1074#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090'!!!'
      Color = clRed
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 3
      Visible = False
      StyleElements = [seBorder]
    end
  end
  inherited meData: TMemTableEh
    DataDriver = nil
  end
  inherited dsData: TDataSource
    AutoEdit = False
    DataSet = qrData
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
    inherited aDelete: TAction
      Enabled = False
    end
    inherited aEdit: TAction
      Enabled = False
    end
  end
  object qrData: TADOQuery
    Connection = dm.connMain
    Parameters = <
      item
        Name = 'selectguid'
        Size = -1
        Value = Null
      end
      item
        Name = 'userid'
        Size = -1
        Value = Null
      end
      item
        Name = 'userid1'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select t.*, (select cnum from containers c where c.id = t.linked' +
        '_object_id) as cont_num'
      
        'from ##temp_prepared_newdoc t, v_objects o where t.selectguid = ' +
        ':selectguid'
      'and t.object_id = o.id and ('
      
        'o.cnum in (select f.v from temp_filter f where f.doctypes = '#39'-1'#39 +
        ' and f.userid = :userid and f.c = '#39'object_num'#39')'
      
        'or not exists (select 1 from temp_filter f where f.doctypes = '#39'-' +
        '1'#39' and f.userid = :userid1 and f.c = '#39'object_num'#39')'
      ')')
    Left = 128
    Top = 256
  end
end
