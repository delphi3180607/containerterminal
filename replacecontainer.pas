﻿unit ReplaceContainer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  DBSQLLookUp, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEdit2 = class(TFormEdit)
    luOldContainer: TDBSQLLookUp;
    edNewContainer: TDBEditEh;
    lbInfo: TLabel;
    lbWarning: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEdit2: TFormEdit2;

implementation

{$R *.dfm}

end.
