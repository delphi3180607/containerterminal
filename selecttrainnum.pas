﻿unit SelectTrainNum;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFormSelectTrainNum = class(TFormEdit)
    cbTrains: TComboBox;
    Label1: TLabel;
  private
    { Private declarations }
  public
    folder_id: integer;
    procedure Init;
  end;

var
  FormSelectTrainNum: TFormSelectTrainNum;

implementation

{$R *.dfm}

procedure TFormSelectTrainNum.Init;
begin

  qrAux.Close;
  qrAux.Parameters.ParamByName('folder_id').Value := self.folder_id;
  qrAux.Open;

  cbTrains.Items.Clear;

  while not qrAux.Eof do
  begin
    cbTrains.Items.Add(qrAux.Fields[0].AsString);
    qrAux.Next;
  end;

end;


end.
