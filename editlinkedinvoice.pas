﻿unit editlinkedinvoice;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBGridEh, Vcl.Buttons, DBLookupEh, DBCtrlsEh,
  DBSQLLookUp, Vcl.Mask, functions;

type
  TFormEditLinkedInvoice = class(TFormEdit)
    edDocNumber: TDBEditEh;
    dtDocDate: TDBDateTimeEditEh;
    laDealer: TDBSQLLookUp;
    neSumma: TDBNumberEditEh;
    luService: TDBLookupComboboxEh;
    sbService: TSpeedButton;
    laTarifType: TDBSQLLookUp;
    Label17: TLabel;
    ssTarifType: TADOLookUpSqlSet;
    procedure sbServiceClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditLinkedInvoice: TFormEditLinkedInvoice;

implementation

{$R *.dfm}

uses servicekinds;

procedure TFormEditLinkedInvoice.sbServiceClick(Sender: TObject);
begin
  SFD(FormServiceKinds, luService);
end;

end.
