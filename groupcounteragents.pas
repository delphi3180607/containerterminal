﻿unit GroupCounteragents;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  DBSQLLookUp, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls, MemTableDataEh, MemTableEh,
  Vcl.Buttons;

type
  TFormGroupCounteragents = class(TFormEdit)
    Label1: TLabel;
    luCounteragent1: TDBSQLLookUp;
    meTable: TMemTableEh;
    sbRemoveLink: TSpeedButton;
    ssCounteragents: TADOLookUpSqlSet;
    procedure sbRemoveLinkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormGroupCounteragents: TFormGroupCounteragents;

implementation

{$R *.dfm}

procedure TFormGroupCounteragents.sbRemoveLinkClick(Sender: TObject);
begin
  self.luCounteragent1.KeyValue := null;
end;

end.
