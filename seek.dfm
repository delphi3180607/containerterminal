﻿object FormSeek: TFormSeek
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = #1055#1086#1080#1089#1082
  ClientHeight = 301
  ClientWidth = 270
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDefault
  PixelsPerInch = 96
  TextHeight = 16
  object edSeek: TEdit
    Left = 0
    Top = 0
    Width = 270
    Height = 22
    Align = alTop
    BorderStyle = bsNone
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    OnChange = edSeekChange
    OnKeyDown = edSeekKeyDown
  end
  object dgSeek: TDBGridEh
    Left = 0
    Top = 22
    Width = 270
    Height = 279
    Align = alClient
    AllowedOperations = []
    AutoFitColWidths = True
    BorderStyle = bsNone
    Ctl3D = False
    DataSource = dsAux
    DynProps = <>
    Flat = True
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
    ParentCtl3D = False
    ReadOnly = True
    TabOrder = 1
    TitleParams.FillStyle = cfstSolidEh
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -13
    TitleParams.Font.Name = 'Tahoma'
    TitleParams.Font.Style = [fsBold]
    TitleParams.ParentFont = False
    OnDblClick = dgSeekDblClick
    OnKeyDown = dgSeekKeyDown
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'code'
        Footers = <>
        TextEditing = False
        Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        Width = 237
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qrAux: TADOQuery
    Connection = dm.connMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end>
    SQL.Strings = (
      
        'select top 50 * from counteragents where code like :code or name' +
        ' like :name order by code')
    Left = 80
    Top = 120
  end
  object dsAux: TDataSource
    DataSet = qrAux
    Left = 152
    Top = 120
  end
end
