﻿object FormMain: TFormMain
  Left = 0
  Top = 0
  ClientHeight = 664
  ClientWidth = 1026
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  GlassFrame.SheetOfGlass = True
  Menu = mm
  OldCreateOrder = False
  Scaled = False
  WindowState = wsMaximized
  StyleElements = [seFont, seBorder]
  OnCreate = FormCreate
  DesignSize = (
    1026
    664)
  PixelsPerInch = 96
  TextHeight = 13
  object btClose: TPngSpeedButton
    Left = 1004
    Top = 6
    Width = 23
    Height = 22
    Anchors = [akTop, akRight]
    Flat = True
    Visible = False
    OnClick = N29Click
    PngImage.Data = {
      89504E470D0A1A0A0000000D4948445200000010000000100804000000B5FA37
      EA0000000467414D410000B18F0BFC610500000002624B47440000AA8D233200
      0000097048597300000DD700000DD70142289B780000000774494D4507E10B18
      081101D97C6ED8000000D34944415478DA8D913D0B416114C77FCF643179D9AC
      4C2C72BB8359592CD21D7D00C52D5F40B2AB4BF90046298394321B6E64616292
      D1CB64315DCF7D4329D77F783A2FFF73CEF33F47E02349952C6969EDD830E4E0
      8685F736A8D3C5642BBD0C2A4DFAF4B05C8260CA9D1A37DE8830204C09CB26E8
      E4D1F8C688258690B317289C297265E5A514A2CC894BBF20E870C1708263D9C7
      94962A6B2B0E59272698D162EDD4D909CD69ED1221475B7024C5C36B6D5378A5
      21C4FE0F42E088C04FFA32CB9C3E642698F8320317F5C7AA038FF5F3DC4FEF28
      528D0E2F1FCD0000002574455874646174653A63726561746500323031372D31
      312D32345430383A31373A30312B30313A3030CAB27DD4000000257445587464
      6174653A6D6F6469667900323031372D31312D32345430383A31373A30312B30
      313A3030BBEFC5680000001974455874536F667477617265007777772E696E6B
      73636170652E6F72679BEE3C1A0000000049454E44AE426082}
    ExplicitLeft = 880
  end
  object pc: TPageControl
    AlignWithMargins = True
    Left = 0
    Top = 3
    Width = 1026
    Height = 661
    Margins.Left = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    MultiLine = True
    ParentFont = False
    RaggedRight = True
    TabOrder = 0
    Visible = False
    OnChange = pcChange
  end
  object mm: TMainMenu
    Left = 768
    Top = 368
    object N1: TMenuItem
      Caption = #1059#1095#1077#1090
      object N14: TMenuItem
        Caption = #1042#1099#1076#1072#1095#1072
        object N8: TMenuItem
          Caption = #1046#1044' '#1085#1072#1082#1083#1072#1076#1085#1099#1077
          OnClick = N8Click
        end
        object N38: TMenuItem
          Caption = #1055#1086#1076#1072#1095#1072'/'#1056#1072#1079#1075#1088#1091#1079#1082#1072
          OnClick = N38Click
        end
        object N57: TMenuItem
          Caption = '-'
        end
        object N37: TMenuItem
          Caption = #1046#1091#1088#1085#1072#1083' '#1091#1074#1077#1076#1086#1084#1083#1077#1085#1080#1081
          OnClick = N37Click
        end
        object N27: TMenuItem
          Caption = '-'
        end
        object N56: TMenuItem
          Caption = #1047#1072#1076#1072#1085#1080#1103' '#1085#1072' '#1088#1072#1079#1075#1088#1091#1079#1082#1091
          OnClick = N56Click
        end
        object N82: TMenuItem
          Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088#1099' '#1074' '#1088#1072#1073#1086#1090#1077
          OnClick = N82Click
        end
      end
      object N40: TMenuItem
        Caption = #1054#1090#1087#1088#1072#1074#1082#1072'/'#1061#1088#1072#1085#1077#1085#1080#1077
        object N45: TMenuItem
          Caption = #1047#1072#1103#1074#1082#1080
          OnClick = N45Click
        end
        object N52: TMenuItem
          Caption = #1055#1086#1075#1088#1091#1079#1082#1072
          OnClick = N52Click
        end
        object N44: TMenuItem
          Caption = #1059#1073#1086#1088#1082#1072
          OnClick = N44Click
        end
        object N58: TMenuItem
          Caption = '-'
        end
        object N90: TMenuItem
          Caption = #1044#1080#1089#1083#1086#1082#1072#1094#1080#1103
          OnClick = N90Click
        end
        object N63: TMenuItem
          Caption = #1047#1072#1076#1072#1085#1080#1103' '#1085#1072' '#1086#1090#1087#1088#1072#1074#1082#1091
          Visible = False
          OnClick = N63Click
        end
        object N87: TMenuItem
          Caption = #1056#1072#1079#1084#1077#1097#1077#1085#1080#1077' '#1087#1083#1072#1090#1092#1086#1088#1084
          Visible = False
          OnClick = N87Click
        end
        object N83: TMenuItem
          Caption = '-'
        end
        object N84: TMenuItem
          Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088#1099' '#1074' '#1088#1072#1073#1086#1090#1077
          OnClick = N84Click
        end
        object N85: TMenuItem
          Caption = #1042#1072#1075#1086#1085#1099' '#1074' '#1088#1072#1073#1086#1090#1077
          OnClick = N85Click
        end
        object N103: TMenuItem
          Caption = #1040#1088#1093#1080#1074' '#1087#1086#1076#1072#1095#1080
          OnClick = N103Click
        end
        object N59: TMenuItem
          Caption = #1047#1072#1076#1072#1095#1080
          Visible = False
        end
      end
      object N64: TMenuItem
        Caption = #1040#1082#1090#1099' '#1085#1072' '#1087#1088#1080#1077#1084'/'#1074#1099#1076#1072#1095#1091
        OnClick = N64Click
      end
      object N51: TMenuItem
        Caption = #1040#1074#1090#1086#1074#1099#1074#1086#1079'/'#1040#1074#1090#1086#1076#1086#1089#1090#1072#1074#1082#1072
        OnClick = N51Click
      end
      object N108: TMenuItem
        Caption = #1052#1072#1090#1077#1088#1080#1072#1083#1100#1085#1099#1077' '#1087#1088#1086#1087#1091#1089#1082#1080
        OnClick = N108Click
      end
      object N61: TMenuItem
        Caption = '-'
        Visible = False
      end
      object N106: TMenuItem
        Caption = #1042#1099#1089#1090#1072#1074#1083#1077#1085#1085#1099#1077' '#1089#1095#1077#1090#1072
        OnClick = N106Click
      end
      object N60: TMenuItem
        Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1091#1089#1083#1091#1075#1080
        Visible = False
        OnClick = N60Click
      end
      object N79: TMenuItem
        Caption = '-'
        Visible = False
      end
      object N62: TMenuItem
        Caption = #1054#1089#1084#1086#1090#1088' '#1074#1072#1075#1086#1085#1086#1074
        Visible = False
        OnClick = N62Click
      end
      object N65: TMenuItem
        Caption = #1040#1082#1090#1099' '#1087#1086' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072#1084
        Visible = False
        OnClick = N65Click
      end
      object N50: TMenuItem
        Caption = '-'
      end
      object N31: TMenuItem
        Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088#1099
        OnClick = N31Click
      end
      object N32: TMenuItem
        Caption = #1042#1072#1075#1086#1085#1099'/'#1055#1083#1072#1090#1092#1086#1088#1084#1099
        OnClick = N32Click
      end
      object N39: TMenuItem
        Caption = #1043#1088#1091#1079#1099
        OnClick = N39Click
      end
      object N67: TMenuItem
        Caption = '-'
      end
      object N13: TMenuItem
        Caption = #1050#1086#1085#1090#1088#1072#1082#1090#1099
        OnClick = N13Click
      end
    end
    object N110: TMenuItem
      Caption = #1061#1088#1072#1085#1077#1085#1080#1077
      object N111: TMenuItem
        Caption = #1052#1072#1090#1088#1080#1094#1072
        OnClick = N111Click
      end
      object N115: TMenuItem
        Caption = #1047#1072#1076#1072#1085#1080#1103' '#1085#1072' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1103
        OnClick = N115Click
      end
      object N114: TMenuItem
        Caption = #1057#1090#1086#1082
        Visible = False
      end
      object N112: TMenuItem
        Caption = '-'
      end
    end
    object N2: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      object N16: TMenuItem
        Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090#1099
        OnClick = N16Click
      end
      object N33: TMenuItem
        Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1077' '#1083#1080#1094#1072
        OnClick = N33Click
      end
      object N21: TMenuItem
        Caption = #1042#1080#1076#1099' '#1091#1089#1083#1091#1075'/'#1086#1087#1077#1088#1072#1094#1080#1081
        OnClick = N21Click
      end
      object N30: TMenuItem
        Caption = #1058#1080#1087#1099' '#1090#1072#1088#1080#1092#1086#1074
        OnClick = N30Click
      end
      object N53: TMenuItem
        Caption = #1042#1080#1076#1099' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1081
        OnClick = N53Click
      end
      object N76: TMenuItem
        Caption = '-'
      end
      object N77: TMenuItem
        Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088#1099
        OnClick = N77Click
      end
      object N78: TMenuItem
        Caption = #1042#1072#1075#1086#1085#1099
        OnClick = N78Click
      end
      object N88: TMenuItem
        Caption = #1055#1091#1090#1080
        OnClick = N88Click
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object N17: TMenuItem
        Caption = #1058#1080#1087#1099' '#1074#1072#1075#1086#1085#1086#1074
        OnClick = N17Click
      end
      object N81: TMenuItem
        Caption = #1052#1086#1076#1077#1083#1080' '#1074#1072#1075#1086#1085#1086#1074
        OnClick = N81Click
      end
      object N74: TMenuItem
        Caption = #1058#1077#1093#1085#1080#1095#1077#1089#1082#1080#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1103
        OnClick = N74Click
      end
      object N18: TMenuItem
        Caption = #1058#1080#1087#1099' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074
        OnClick = N18Click
      end
      object N35: TMenuItem
        Caption = #1058#1080#1087#1099' '#1075#1088#1091#1079#1086#1074
        OnClick = N35Click
      end
      object N66: TMenuItem
        Caption = #1042#1080#1076#1099' '#1052#1058#1059
        OnClick = N66Click
      end
      object N68: TMenuItem
        Caption = #1056#1080#1089#1091#1085#1082#1080' ('#1087#1086#1075#1088#1091#1079#1082#1072')'
        OnClick = N68Click
      end
      object N43: TMenuItem
        Caption = '-'
      end
      object N24: TMenuItem
        Caption = #1042#1080#1076#1099' '#1086#1090#1087#1088#1072#1074#1082#1080
        OnClick = N24Click
      end
      object N105: TMenuItem
        Caption = #1042#1072#1088#1080#1072#1085#1090#1099' '#1076#1086#1089#1090#1072#1074#1082#1080
        OnClick = N105Click
      end
      object N46: TMenuItem
        Caption = #1055#1086#1088#1090#1099
        OnClick = N46Click
      end
      object N19: TMenuItem
        Caption = #1057#1090#1072#1085#1094#1080#1080
        OnClick = N19Click
      end
      object N12: TMenuItem
        Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088#1085#1099#1077' '#1087#1086#1077#1079#1076#1072
        OnClick = N12Click
      end
      object N42: TMenuItem
        Caption = #1060#1088#1086#1085#1090#1099' '#1088#1072#1079#1075#1088#1091#1079#1082#1080
        OnClick = N42Click
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object N34: TMenuItem
        Caption = #1057#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1077' '#1074#1085#1077#1096#1085#1080#1084' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072#1084
        OnClick = N34Click
      end
    end
    object N3: TMenuItem
      Caption = #1054#1090#1095#1077#1090#1099
      object N41: TMenuItem
        Caption = #1053#1072#1089#1090#1088#1072#1080#1074#1072#1077#1084#1099#1077' '#1086#1090#1095#1077#1090#1099
        OnClick = N3Click
      end
      object N91: TMenuItem
        Caption = '-'
      end
      object N89: TMenuItem
        Caption = #1055#1086#1080#1089#1082' '#1076#1072#1085#1085#1099#1093' '#1087#1086' '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090#1091
        OnClick = N89Click
      end
      object N92: TMenuItem
        Caption = #1055#1086#1080#1089#1082' '#1076#1072#1085#1085#1099#1093' '#1086#1090#1087#1088#1072#1074#1086#1082
        OnClick = N92Click
      end
      object N96: TMenuItem
        Caption = '-'
        Visible = False
      end
      object N97: TMenuItem
        Caption = #1054#1090#1095#1077#1090' '#1055#1057#1042#1046
        Visible = False
        OnClick = N97Click
      end
      object N98: TMenuItem
        Caption = #1052#1086#1085#1080#1090#1086#1088' '#1090#1077#1088#1084#1080#1085#1072#1083#1072
        Visible = False
        OnClick = N98Click
      end
    end
    object N70: TMenuItem
      Caption = #1056#1072#1089#1095#1077#1090#1099
      object N72: TMenuItem
        Caption = #1057#1077#1090#1082#1072' '#1094#1077#1085' '#1085#1072' '#1093#1088#1072#1085#1077#1085#1080#1077' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074
        OnClick = N72Click
      end
      object N11: TMenuItem
        Caption = #1057#1077#1090#1082#1072' '#1094#1077#1085' '#1085#1072' '#1087#1088#1086#1095#1080#1077' '#1091#1089#1083#1091#1075#1080
        OnClick = N11Click
      end
      object N71: TMenuItem
        Caption = '-'
        Visible = False
      end
      object N73: TMenuItem
        Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1087#1086' '#1082#1083#1080#1077#1085#1090#1072#1084
        Visible = False
        OnClick = N73Click
      end
      object N4: TMenuItem
        Caption = #1057#1095#1077#1090#1072' '#1079#1072' '#1091#1089#1083#1091#1075#1080
        Visible = False
        OnClick = N4Click
      end
    end
    object N99: TMenuItem
      Caption = #1053#1086#1074#1086#1089#1090#1080
      object N100: TMenuItem
        Caption = #1057#1087#1080#1089#1086#1082' '#1085#1086#1074#1086#1089#1090#1077#1081
      end
      object N101: TMenuItem
        Caption = '-'
      end
      object N102: TMenuItem
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1086#1074#1086#1089#1090#1100
      end
    end
    object N5: TMenuItem
      Caption = #1040#1076#1084#1080#1085#1080#1089#1090#1088#1080#1088#1086#1074#1072#1085#1080#1077
      object N22: TMenuItem
        Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1080
        OnClick = N22Click
      end
      object N86: TMenuItem
        Caption = #1056#1086#1083#1080
        OnClick = N86Click
      end
      object N23: TMenuItem
        Caption = '-'
      end
      object N55: TMenuItem
        Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1103' '#1086#1073#1100#1077#1082#1090#1086#1074
        OnClick = N55Click
      end
      object N25: TMenuItem
        Caption = #1057#1093#1077#1084#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1086#1073#1086#1088#1086#1090#1072
        OnClick = N25Click
      end
      object N15: TMenuItem
        Caption = '-'
      end
      object N26: TMenuItem
        Caption = #1058#1080#1087#1099' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
        OnClick = N26Click
      end
      object N36: TMenuItem
        Caption = #1058#1080#1087#1099' '#1076#1086#1089#1090#1072#1074#1082#1080'/'#1086#1087#1077#1088#1072#1094#1080#1081
        OnClick = N36Click
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object N48: TMenuItem
        Caption = #1040#1076#1088#1077#1089#1072' '#1085#1072#1093#1086#1078#1076#1077#1085#1080#1103' '#1074#1072#1075#1086#1085#1086#1074
        OnClick = N48Click
      end
      object N49: TMenuItem
        Caption = #1040#1076#1088#1077#1089#1072' '#1085#1072#1093#1086#1078#1076#1077#1085#1080#1103' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074
        OnClick = N49Click
      end
      object N47: TMenuItem
        Caption = '-'
      end
      object N54: TMenuItem
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1080' '#1082#1086#1085#1089#1090#1072#1085#1090#1099
        OnClick = N54Click
      end
      object N104: TMenuItem
        Caption = #1064#1072#1073#1083#1086#1085#1099' '#1101#1083#1077#1082#1090#1088#1086#1085#1085#1099#1093' '#1087#1080#1089#1077#1084
      end
      object N20: TMenuItem
        Caption = #1059#1095#1077#1090#1085#1099#1077' '#1079#1072#1087#1080#1089#1080' '#1101#1083#1077#1082#1090#1088#1086#1085#1085#1086#1081' '#1087#1086#1095#1090#1099
        OnClick = N20Click
      end
      object N69: TMenuItem
        Caption = '-'
      end
      object N75: TMenuItem
        Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100' SQL '#1079#1072#1087#1088#1086#1089
        OnClick = N75Click
      end
      object N80: TMenuItem
        Caption = #1055#1088#1086#1074#1077#1088#1082#1080' '#1074' '#1085#1072#1095#1072#1083#1077' '#1088#1072#1073#1086#1090#1099
        OnClick = N80Click
      end
      object N109: TMenuItem
        Caption = #1050#1083#1072#1089#1089#1080#1092#1080#1082#1072#1090#1086#1088#1099
        OnClick = N109Click
      end
      object N94: TMenuItem
        Caption = '-'
      end
      object N93: TMenuItem
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1089#1090#1088#1086#1082' '#1088#1072#1087#1086#1088#1090#1072
        OnClick = N93Click
      end
      object N95: TMenuItem
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1074#1072#1088#1080#1072#1085#1090#1086#1074' '#1076#1086#1089#1090#1072#1074#1082#1080
        OnClick = N95Click
      end
      object N107: TMenuItem
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1086#1087#1077#1088#1072#1094#1080#1081' '#1087#1086' '#1087#1088#1086#1087#1091#1089#1082#1072#1084
        OnClick = N107Click
      end
      object N116: TMenuItem
        Caption = '-'
      end
      object N113: TMenuItem
        Caption = #1057#1090#1088#1091#1082#1090#1091#1088#1072' '#1084#1072#1090#1088#1080#1094#1099
        OnClick = N113Click
      end
    end
    object N6: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1082#1072
      object N28: TMenuItem
        Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
        OnClick = N28Click
      end
    end
  end
  object pmPc: TPopupMenu
    Left = 816
    Top = 368
    object N29: TMenuItem
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1074#1082#1083#1072#1076#1082#1091
      OnClick = N29Click
    end
  end
  object drvData: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.Parameters = <>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 624
    Top = 480
  end
  object meData: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvData
    Left = 672
    Top = 480
  end
  object dsData: TDataSource
    DataSet = meData
    Left = 728
    Top = 480
  end
  object al: TActionList
    Left = 792
    Top = 480
  end
  object pmGrid: TPopupMenu
    Left = 848
    Top = 480
    object MenuItem5: TMenuItem
      Caption = #1060#1080#1083#1100#1090#1088
      ShortCut = 16454
    end
    object Excel1: TMenuItem
      Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
      ShortCut = 16453
    end
    object MenuItem6: TMenuItem
      Caption = '-'
    end
    object MenuItem7: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ShortCut = 116
    end
  end
end
