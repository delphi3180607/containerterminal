﻿unit EditPass;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBCtrlsEh, DBSQLLookUp, Vcl.Mask,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Buttons, PngSpeedButton, Vcl.ComCtrls, Passes;

type
  TFormEditPass = class(TFormEdit)
    Label18: TLabel;
    Label24: TLabel;
    Label27: TLabel;
    edCarData: TDBEditEh;
    edCarNumber: TDBEditEh;
    laDriver: TDBSQLLookUp;
    ssPersons: TADOLookUpSqlSet;
    edAttorney: TDBEditEh;
    Label25: TLabel;
    edDocNumber: TDBEditEh;
    Label1: TLabel;
    dtDocDate: TDBDateTimeEditEh;
    Label3: TLabel;
    btPrint: TButton;
    Label2: TLabel;
    dtDateStart: TDBDateTimeEditEh;
    cbPayKind: TDBComboBoxEh;
    Label5: TLabel;
    Label6: TLabel;
    pcSpec: TPageControl;
    TabSheet1: TTabSheet;
    Прочее: TTabSheet;
    plToolSpec: TPanel;
    sbAddSpec: TPngSpeedButton;
    sbDeleteSpec: TPngSpeedButton;
    Bevel8: TBevel;
    sbExcelSpec: TPngSpeedButton;
    Panel1: TPanel;
    sbAddSpec1: TPngSpeedButton;
    sbDeleteSpec1: TPngSpeedButton;
    Bevel1: TBevel;
    PngSpeedButton3: TPngSpeedButton;
    dgSpec: TDBGridEh;
    dgSpec1: TDBGridEh;
    sbEditSpec1: TPngSpeedButton;
    Label9: TLabel;
    laPayer: TDBSQLLookUp;
    sbTrash: TPngSpeedButton;
    sbEditSpec: TPngSpeedButton;
    luDealer: TDBSQLLookUp;
    Label8: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure sbAddSpecClick(Sender: TObject);
    procedure sbDeleteSpecClick(Sender: TObject);
    procedure sbDeleteSpec1Click(Sender: TObject);
    procedure sbAddSpec1Click(Sender: TObject);
    procedure sbEditSpec1Click(Sender: TObject);
    procedure sbTrashClick(Sender: TObject);
    procedure laDriverKeyValueChange(Sender: TObject);
    procedure btPrintClick(Sender: TObject);
    procedure sbEditSpecClick(Sender: TObject);
  private
    { Private declarations }
  public
    //--
  end;

var
  FormEditPass: TFormEditPass;

implementation

{$R *.dfm}


procedure TFormEditPass.btPrintClick(Sender: TObject);
begin

  if TFormPasses(self.ParentFormGrid).meData.State in [dsInsert, dsEdit] then
   TFormPasses(self.ParentFormGrid).meData.Post;

  if current_id = 0 then
  begin
    current_id := TFormPasses(ParentFormGrid).meData.FieldByName('id').AsInteger;
  end;
  TFormPasses(self.ParentFormGrid).meData.Edit;
  TFormPasses(self.ParentFormGrid).sbReportClick(nil);
end;

procedure TFormEditPass.FormActivate(Sender: TObject);
begin
  inherited;

  TFormPasses(ParentFormGrid).meSpec.Close;
  TFormPasses(ParentFormGrid).meSpec.Open;

  dgSpec.DataSource := TFormPasses(ParentFormGrid).dsSpec;
  dgSpec1.DataSource := TFormPasses(ParentFormGrid).dsSpec;

  pcSpec.Pages[0].TabVisible := false;
  pcSpec.Pages[1].TabVisible := false;
  pcSpec.Pages[dsLocal.DataSet.FieldByName('pass_type').AsInteger].TabVisible := true;
  pcSpec.ActivePageIndex := dsLocal.DataSet.FieldByName('pass_type').AsInteger;
end;

procedure TFormEditPass.laDriverKeyValueChange(Sender: TObject);
begin
  inherited;
  edCarData.Text := ssPersons.ListDataSet.FieldByName('car_data').AsString;
  edCarNumber.Text := ssPersons.ListDataSet.FieldByName('car_number').AsString;
  edAttorney.Text := ssPersons.ListDataSet.FieldByName('attorney').AsString;
end;

procedure TFormEditPass.sbAddSpec1Click(Sender: TObject);
begin

  if current_id = 0 then
  begin
    if TFormPasses(self.ParentFormGrid).meData.State in [dsInsert, dsEdit] then
     TFormPasses(self.ParentFormGrid).meData.Post;
    current_id := TFormPasses(ParentFormGrid).meData.FieldByName('id').AsInteger;
    TFormPasses(ParentFormGrid).meData.Edit;
  end;

  TFormPasses(ParentFormGrid).sbAddSpec1Click(nil);
end;

procedure TFormEditPass.sbDeleteSpec1Click(Sender: TObject);
begin
  TFormPasses(ParentFormGrid).sbDeleteSpecClick(nil);
end;

procedure TFormEditPass.sbAddSpecClick(Sender: TObject);
begin

  if current_id = 0 then
  begin
    if TFormPasses(self.ParentFormGrid).meData.State in [dsInsert, dsEdit] then
     TFormPasses(self.ParentFormGrid).meData.Post;
    current_id := TFormPasses(ParentFormGrid).meData.FieldByName('id').AsInteger;
    TFormPasses(ParentFormGrid).meData.Edit;
  end;
  TFormPasses(ParentFormGrid).sbAddSpecClick(nil);

end;

procedure TFormEditPass.sbDeleteSpecClick(Sender: TObject);
begin
  TFormPasses(ParentFormGrid).sbDeleteSpecClick(nil);
end;

procedure TFormEditPass.sbEditSpec1Click(Sender: TObject);
begin
  TFormPasses(ParentFormGrid).sbEditSpec1Click(nil)
end;

procedure TFormEditPass.sbEditSpecClick(Sender: TObject);
begin
  TFormPasses(ParentFormGrid).sbEditSpecClick(nil);
end;

procedure TFormEditPass.sbTrashClick(Sender: TObject);
begin
  if self.current_id<>0 then
  begin
    dsLocal.DataSet.Cancel;
    TFormPasses(self.ParentFormGrid).sbDeleteClick(nil);
    self.ModalResult := mrCancel;
    self.Close;
  end;
end;

end.
