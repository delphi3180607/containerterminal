﻿inherited FormEditPassSpec: TFormEditPassSpec
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
  ClientHeight = 210
  ClientWidth = 606
  ExplicitWidth = 612
  ExplicitHeight = 238
  PixelsPerInch = 96
  TextHeight = 16
  object sbContainer: TSpeedButton [0]
    Left = 562
    Top = 26
    Width = 31
    Height = 24
    Caption = '...'
  end
  inherited plBottom: TPanel
    Top = 169
    Width = 606
    TabOrder = 5
    inherited btnCancel: TButton
      Left = 490
    end
    inherited btnOk: TButton
      Left = 371
    end
  end
  object laContainerOwner: TDBSQLLookUp [2]
    Left = 341
    Top = 28
    Width = 218
    Height = 22
    Color = 14810109
    ControlLabel.Width = 88
    ControlLabel.Height = 16
    ControlLabel.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
    ControlLabel.Visible = True
    DataField = 'container_owner_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    HighlightRequired = True
    StyleElements = [seFont, seBorder]
    TabOrder = 0
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object leContainerKind: TDBSQLLookUp [3]
    Left = 180
    Top = 28
    Width = 155
    Height = 22
    Color = 14810109
    ControlLabel.Width = 108
    ControlLabel.Height = 16
    ControlLabel.Caption = #1042#1080#1076' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
    ControlLabel.Visible = True
    DataField = 'container_kind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    StyleElements = [seFont, seBorder]
    TabOrder = 1
    Visible = True
    SqlSet = ssContainerKind
    RowCount = 0
  end
  object edContainerNum: TDBEditEh [4]
    Left = 8
    Top = 28
    Width = 166
    Height = 24
    CharCase = ecUpperCase
    Color = 14810109
    ControlLabel.Width = 126
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
    ControlLabel.Visible = True
    DataField = 'container_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    MaxLength = 11
    StyleElements = [seFont, seBorder]
    TabOrder = 2
    Visible = True
    OnExit = edContainerNumExit
    EditMask = 'cccc9999999;0;?'
  end
  object edSealNumber: TDBEditEh [5]
    Left = 8
    Top = 89
    Width = 182
    Height = 22
    ControlLabel.Width = 97
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
    ControlLabel.Visible = True
    DataField = 'seal_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    TabOrder = 3
    Visible = True
  end
  object cbIsEmpty: TDBCheckBoxEh [6]
    Left = 213
    Top = 92
    Width = 97
    Height = 17
    Caption = #1055#1086#1088#1086#1078#1085#1080#1081
    DataField = 'isempty'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 4
  end
  inherited dsLocal: TDataSource
    Left = 24
    Top = 160
  end
  inherited qrAux: TADOQuery
    Left = 72
    Top = 160
  end
  object ssContainerKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from containerkinds order by code')
    DownSql.Strings = (
      'select * from containerkinds order by code')
    InitSql.Strings = (
      'select * from containerkinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 275
    Top = 16
  end
  object drvContainers: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'declare @cnum varchar(50), @docid int, @extraorder int;'
      ''
      'select @cnum = :cnum;'
      ''
      
        'select top 1 c.*, o.start_datetime,  o.id as object_id, o.owner_' +
        'id,'
      
        '(select code from counteragents c1 where c1.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from containerkinds k where k.id = c.kind_id) as ki' +
        'nd_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' formal_state_code,'
      
        '(select code from objectstatekinds s where s.id = o.real_state_i' +
        'd) as real_state_code'
      
        'from objects o, containers c where o.id = c.id and o.object_type' +
        ' = '#39'container'#39
      'and ((c.cnum = @cnum) or (@cnum = '#39'-1-'#39'))')
    SelectCommand.Parameters = <
      item
        Name = 'cnum'
        DataType = ftString
        Size = 1
        Value = ' '
      end>
    UpdateCommand.CommandText.Strings = (
      'update containers'
      'set'
      '  cnum = :cnum,'
      '  kind_id = :kind_id,'
      '  size = :size,'
      '  container_weight = :container_weight,'
      '  carrying = :carrying,'
      '  date_made = :date_made,'
      '  date_inspection = :date_inspection,'
      '  date_stiker = :date_stiker,'
      '  picture = :picture,'
      '  note = :note,'
      '  isdefective = :isdefective'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'cnum'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'size'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_weight'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'carrying'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'date_made'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_inspection'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_stiker'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'picture'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 850
        Value = Null
      end
      item
        Name = 'isdefective'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.CommandText.Strings = (
      
        'select top 1 c.*, o.start_datetime,  o.id as object_id, o.owner_' +
        'id,'
      
        '(select code from counteragents c1 where c1.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from containerkinds k where k.id = c.kind_id) as ki' +
        'nd_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' formal_state_code,'
      
        '(select code from objectstatekinds s where s.id = o.real_state_i' +
        'd) as real_state_code,'
      '0 as extraorder'
      'from objects o, containers c '
      'where o.id = c.id and o.object_type = '#39'container'#39
      'and c.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 152
    Top = 158
  end
  object meContainers: TMemTableEh
    Params = <>
    DataDriver = drvContainers
    Left = 224
    Top = 158
  end
end
