﻿inherited FormAddDriver: TFormAddDriver
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074#1086#1076#1080#1090#1077#1083#1103' '#1074' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082
  ClientHeight = 144
  ClientWidth = 534
  OnActivate = FormActivate
  ExplicitWidth = 540
  ExplicitHeight = 172
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 14
    Top = 17
    Width = 26
    Height = 16
    Caption = #1048#1084#1103
  end
  inherited plBottom: TPanel
    Top = 103
    Width = 534
    TabOrder = 1
    ExplicitTop = 103
    ExplicitWidth = 534
    inherited btnCancel: TButton
      Left = 418
      ExplicitLeft = 418
    end
    inherited btnOk: TButton
      Left = 299
      ExplicitLeft = 299
    end
  end
  object edFieldName: TDBEditEh [2]
    Left = 14
    Top = 35
    Width = 505
    Height = 24
    DataField = 'person_name'
    DataSource = FormPersons.dsData
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object kind: TDBNumberEditEh [3]
    Left = 184
    Top = 80
    Width = 121
    Height = 24
    DataField = 'person_kind'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = False
  end
  inherited dsLocal: TDataSource
    Left = 80
    Top = 88
  end
  inherited qrAux: TADOQuery
    Left = 32
    Top = 88
  end
end
