﻿inherited FormEditSql: TFormEditSql
  BorderStyle = bsSizeable
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1079#1072#1087#1088#1086#1089#1072
  ClientHeight = 626
  ClientWidth = 850
  ExplicitWidth = 866
  ExplicitHeight = 664
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 585
    Width = 850
    ExplicitTop = 585
    ExplicitWidth = 850
    inherited btnCancel: TButton
      Left = 734
      ExplicitLeft = 734
    end
    inherited btnOk: TButton
      Left = 615
      ExplicitLeft = 615
    end
  end
  object meSQL: TSynMemo [1]
    AlignWithMargins = True
    Left = 2
    Top = 2
    Width = 846
    Height = 581
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    ParentCustomHint = False
    Align = alClient
    ActiveLineColor = 16378331
    Ctl3D = True
    ParentCtl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentShowHint = False
    ShowHint = False
    TabOrder = 1
    CodeFolding.GutterShapeSize = 11
    CodeFolding.CollapsedLineColor = clGrayText
    CodeFolding.FolderBarLinesColor = clGrayText
    CodeFolding.IndentGuidesColor = clGray
    CodeFolding.IndentGuides = True
    CodeFolding.ShowCollapsedLine = False
    CodeFolding.ShowHintMark = True
    UseCodeFolding = False
    Gutter.Color = clSilver
    Gutter.BorderColor = clSilver
    Gutter.Font.Charset = DEFAULT_CHARSET
    Gutter.Font.Color = clBlack
    Gutter.Font.Height = -11
    Gutter.Font.Name = 'Courier New'
    Gutter.Font.Style = []
    Gutter.LeftOffset = 5
    Gutter.RightOffset = 10
    Gutter.ShowLineNumbers = True
    Highlighter = synSQL
    SearchEngine = synSearch
    FontSmoothing = fsmNone
    ExplicitLeft = -3
  end
  object synSQL: TSynSQLSyn
    Options.AutoDetectEnabled = True
    Options.AutoDetectLineLimit = 0
    Options.Visible = False
    CommentAttri.Foreground = clGreen
    ConditionalCommentAttri.Foreground = clTeal
    FunctionAttri.Foreground = clPurple
    KeyAttri.Foreground = clBlue
    SQLDialect = sqlMSSQL2K
    Left = 200
    Top = 240
  end
  object synSearch: TSynEditSearch
    Left = 304
    Top = 240
  end
  object dsTest: TDataSource
    Left = 320
    Top = 376
  end
end
