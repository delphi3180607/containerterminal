﻿inherited FormUserAlerts: TFormUserAlerts
  Caption = #1053#1086#1074#1086#1089#1090#1080
  ClientHeight = 618
  ClientWidth = 896
  ExplicitWidth = 912
  ExplicitHeight = 656
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 577
    Width = 896
    ExplicitTop = 577
    ExplicitWidth = 896
    inherited btnOk: TButton
      Left = 661
      ExplicitLeft = 661
    end
    inherited btnCancel: TButton
      Left = 780
      ExplicitLeft = 780
    end
  end
  inherited plAll: TPanel
    Width = 896
    Height = 577
    ExplicitWidth = 896
    ExplicitHeight = 577
    object Splitter1: TSplitter [0]
      Left = 0
      Top = 390
      Width = 896
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 464
      ExplicitWidth = 820
    end
    inherited plTop: TPanel
      Width = 896
      ExplicitWidth = 896
      inherited btTool: TPngSpeedButton
        Left = 844
        ExplicitLeft = 844
      end
    end
    inherited dgData: TDBGridEh
      Width = 896
      Height = 361
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1040#1074#1090#1086#1088
          Width = 96
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          Width = 107
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
          Width = 118
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1058#1077#1082#1089#1090' '#1085#1086#1074#1086#1089#1090#1080
          Width = 594
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 395
      Width = 896
      Height = 182
      Align = alBottom
      Caption = 'Panel1'
      TabOrder = 3
      object DBGridEh1: TDBGridEh
        Left = 1
        Top = 30
        Width = 894
        Height = 151
        Align = alClient
        Border.Color = clSilver
        ColumnDefValues.Title.TitleButton = True
        ColumnDefValues.ToolTips = True
        Ctl3D = False
        DataSource = dsData
        DynProps = <>
        FixedColor = clCream
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        GridLineParams.BrightColor = clSilver
        GridLineParams.ColorScheme = glcsClassicEh
        GridLineParams.DarkColor = clSilver
        GridLineParams.DataBoundaryColor = clSilver
        GridLineParams.DataHorzColor = 15395041
        GridLineParams.DataVertColor = 15395041
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
        IndicatorParams.Color = clBtnFace
        IndicatorParams.HorzLineColor = clSilver
        IndicatorParams.VertLineColor = clSilver
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
        ParentCtl3D = False
        ParentFont = False
        ParentShowHint = False
        PopupMenu = pmGrid
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        SearchPanel.Enabled = True
        SearchPanel.PersistentShowing = False
        SelectionDrawParams.SelectionStyle = gsdsClassicEh
        SelectionDrawParams.DrawFocusFrame = False
        SelectionDrawParams.DrawFocusFrameStored = True
        ShowHint = True
        SortLocal = True
        STFilter.Color = clWhite
        STFilter.HorzLineColor = clSilver
        STFilter.InstantApply = True
        STFilter.Local = True
        STFilter.VertLineColor = clSilver
        STFilter.Visible = True
        TabOrder = 0
        TitleParams.Color = clBtnFace
        TitleParams.FillStyle = cfstSolidEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -11
        TitleParams.Font.Name = 'Verdana'
        TitleParams.Font.Style = [fsBold]
        TitleParams.HorzLineColor = clSilver
        TitleParams.ParentFont = False
        TitleParams.SecondColor = clCream
        TitleParams.VertLineColor = clSilver
        OnActiveGroupingStructChanged = dgDataActiveGroupingStructChanged
        OnColumnMoved = dgDataColumnMoved
        OnColWidthsChanged = dgDataColWidthsChanged
        OnDblClick = sbEditClick
        OnDrawColumnCell = dgDataDrawColumnCell
        OnKeyPress = dgDataKeyPress
        OnMouseDown = dgDataMouseDown
        OnMouseMove = dgDataMouseMove
        OnMouseUp = dgDataMouseUp
        OnSelectionChanged = dgDataSelectionChanged
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            Footers = <>
            Title.Alignment = taCenter
            Title.Caption = #1040#1076#1088#1077#1089#1072#1090' - '#1088#1086#1083#1100
            Width = 156
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            Footers = <>
            Title.Alignment = taCenter
            Title.Caption = #1040#1076#1088#1077#1089#1072#1090' - '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
            Width = 218
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object Panel2: TPanel
        Left = 1
        Top = 1
        Width = 894
        Height = 29
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object PngSpeedButton1: TPngSpeedButton
          AlignWithMargins = True
          Left = 31
          Top = 0
          Width = 32
          Height = 29
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Action = aDelete
          Align = alLeft
          Flat = True
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002554944415478DA95924D68D36018C79F7C746D92A6B5DD
            86B2B56C7810A141F0E8459C7810BC09A2CECFA907F13211C7BC7A134410069E
            046FC26EE2556D657EEC24F5D04C2F62B77569BA43ED47DA244BF2C6E74D6CB1
            743BEC8524BCE4FDFD9E27FF278CEFFBB09F65CFDD11A3AF5E767B7B6640B0F0
            680AEF7178FA44DD15BE793BE7BBCE19E2B84571F9F5CAA080C21C57A67BDFB6
            15F6F9B301897DE3560EDF9798E808ECD4B69751F4587EFBE64728F80743340A
            54E7B7DB402C4BE15F2C0512EBFA5C8E61D9129790814F26C1DEAC80A5EBF789
            E32C8582878B319F900E0802CBC83290560B48A3811253C17681C22CC2914412
            ACAD0AD855DD435848E5DF39FD4F20F30F62BEE7198C20702C1EF49A0D70FFD4
            C1470187D248320156658B56F6B0FD782AFFDE1A0AD1BD7B2F465CCF604591E3
            5307003C82667A795899C2B5102E7CB0769F4218164ADC8E3035CDC2C63A4A3C
            80CC24348ADF09711D29FD316FFD7F7E4860CE5EC3C098526C3203502E871DA0
            A0B9B6064EDB50C6BE7E52F7149897AFE600615E94303019613F689F4AAC5A0D
            4C5D879D765B3958FCA60E09BA97AE04954338016655A3104D9B89A5D2AC3471
            08BA551DBA9A86124399F8A9AA7D41F7E26C30675E14118E83A9E9606D53D88D
            6368804F43184D737236031DAD0A1D0C947692F9FD4B0D04C6F90B336C84CF47
            24095CA30326C238BEF8E897952030FDD8711CB16B48E3E31C3D63D089D4EBA7
            B31BE542FF139A67CFA12492775A4D8215250C6B206DEDC8D1E03F199165AE07
            0F85583F796A06E1D5B1D5CF03706F55A60F53C989ECE67A61CF31EE77FD05EB
            B7706A5FA737EA0000000049454E44AE426082}
          ExplicitLeft = 33
          ExplicitHeight = 23
        end
        object PngSpeedButton3: TPngSpeedButton
          AlignWithMargins = True
          Left = 104
          Top = 0
          Width = 44
          Height = 28
          Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
          Margins.Left = 1
          Margins.Top = 0
          Margins.Right = 1
          Margins.Bottom = 1
          Align = alLeft
          Flat = True
          OnClick = btExcelClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C0000031E4944415478DA8D936B6C0C5114C7FF7776B7BB53CF6E
            29F508A11E4D89E8AA846AB32869D89520A1691B1111AFB6D2342A21AA443C2A
            F1F8209E1104ED1791F824E852DD967835969616F5A6655955DD99E9ECEC5C67
            6737F5D5C9993B7327F7FCCEB9FF7B2EE39C63C1CEFAD5E2406B81C0E00060A3
            8701B191D3C07568BADEDEF5937B58F7AF72EF09978A98B10860D1BE46EFC512
            47AA20B0C11C3081A3CFB8F1CDE1EF09A1FA71173A3FF64A2F9A5BED0D275CBD
            7D0057D5BDEF359B33EC2DDF61121833325352842399350E39A4C139A11F6ADB
            653C68F5A3E3A32ABF6A6B4BB87B6C71AF01701FBC2F5D2E76886F02268AE546
            013A0D615D874A00450D237B820D2D7E0E8DA0B77C9DB8ED6DAFF1EC73E6B3BC
            43F3DFEADC3E7670BCCEC2B4DFF4143702C11FB8E3BB6080AC96FE04D2B0383D
            1F85D9EB696E86A4A8283CF244BBB93BCB12015C59B7B06CB96CEDC0FB402B9A
            9EBC27C1240C4D48841AD620493274D8B16CF67E30533C74AA2A6B9C152B0E3F
            566E54648A118035A44F09962F596A7AADD6A323E0475B7307C24C8699C5C16C
            4A42C1BC43548D19210A26C78C5166AC3C42801DB3C4E8295479E5C949E76C05
            D96BF1E8E73574FD09C3E77B0A5B9C1DC5EEF3246A1C42B44E0B933EF44E1B2E
            208F2AB8591103382B77A98A7ADE32313915A5AE4A5CFF7411BA64C5CBD6E7C8
            759460DAF845241E10D2A202A724C200782A09E0281FB18E529CCA4C73222005
            C0551D65EE5DB8D4761C66251E9FDEB5E375E74B9C2EF94027C3490F20350970
            EF7DA8D4ED996D003ED3BF91916689F898A43424278C465E663EEE7EF1E06BD7
            37A87E091B5C97E934B8D198D39205E4EC68501AAAB2A35B985BD128576F71D8
            FC3D16A3FB7E07BFE16AE31E04955F604CC0003111AB728EF67567CA5060EE56
            AFF2E0A8330AC8DA562FD794CFB405640BFEC7C627524C699DD27432270A9855
            5617ACD93E53ECEEB5B2FF014C1AC6F48C8DB5D2B3330B071880199B3C6FAF1F
            98332418E2FD682E44B4D0638B0789E6BE4B255A04DA1274314EF83375CD8D2F
            2DE772D30C4046516D9116E24B1963D349A6F87F57D1F0D8ED88CEC914EAC626
            01A8F69DCD3DFB17534092F0545E8BC50000000049454E44AE426082}
          ExplicitLeft = 113
          ExplicitTop = 2
          ExplicitHeight = 37
        end
        object PngSpeedButton4: TPngSpeedButton
          AlignWithMargins = True
          Left = 0
          Top = 0
          Width = 31
          Height = 29
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Action = aAdd
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002BE4944415478DAA5935D4893511CC61FA74E97390D6B9B
            1F516C2E334C2535295A8A792549908124A120D145A6303023233FA2444A08F5
            C6ECA2B430B01681994E4D29511321B134F3634A7EA382B9ED7DA7BEE7BC6FAF
            1B2E28BDA9FFB9389C8BE777FEFF739EC7451004FC4FB9FC09286BBBAEA13C4D
            2394A6134A5484A7E0285910CFB5E25E579EFACCB423A0AC2D378550BE72BF42
            E31FA28A80CC7D3736F80D2C5AE7D033DE8EC5E5A57942487675FA4BC35F0087
            98D64407C77969154761328F62C1360B8EE7E023F581421688AE512386C68719
            B1B38CE7971B0C4EC083D65CB5D87667A4FA648056198E0FF32D6019169C40E0
            E9E901415CEBFC3A42E561E81E69C3C8A4698E2344F73AAB75C20E2835EA0B7C
            7DF615C78724A377A913D60D060CC3202FB6C8DE5DC9E77CD85833246E6E8855
            E950DBF1088C852B6CC8E9B86307DC7D97331675382E785DB201D3F298383707
            8BD58C9284723B20CB7009DEBEEE203CC1216504D6AD0C1A7BDBC69BF59D5A3B
            A0B0E12A1B1F992C1BB10C61C5BA829BC7EF6DFB657A631AE4325F84F945A3AA
            E9B1ED7D6ECF2E3BE0D69B2BEC89F044D9B0791036BA8682A8FB3B02BCA57284
            2B6250F1B6CAF6F1469F039067C81CD31C0C0DE63D04CC58A6C0726B6016CDA8
            BE50EF146E96C44502ADDF11B06616AF7A1AC7BBF3FB1D23E8EBD30BD6246CF1
            D963A9E89A69C11AE520E55D507AE68913C0530152891BE2354978D85881558B
            B5B0EFF657C7235E7B91A6E628ED542AF606C4A84FA3EB47B3FDC1C4AF854037
            1102DC25AE885327C138D0884FDFBFCC899ED1F5177D9B701A29F3E979D148A4
            2650A5F4D28524627A7512B33F2721F03C82F6A811243F80A68106F40E0F32A2
            BD3344F16F236DD5C5EAA414D1EF95D495F8C7859E8252EEBF7939A657A660EC
            6F87D9CACE8BE2EC2DF1B6613A5799A01121699C334C440C13B58789F2A4AEBF
            6878E730FD4BFD027D1196F03509C5820000000049454E44AE426082}
          ExplicitLeft = 1
          ExplicitHeight = 27
        end
        object PngSpeedButton5: TPngSpeedButton
          AlignWithMargins = True
          Left = 63
          Top = 0
          Width = 26
          Height = 29
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Action = aEdit
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
            62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
            08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
            4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
            C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
            892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
            4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
            0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
            C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
            8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
            346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
            5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
            3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
            378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
            C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
            1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
            FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
            F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
            0049454E44AE426082}
          ExplicitLeft = 60
          ExplicitHeight = 43
        end
        object Bevel1: TBevel
          AlignWithMargins = True
          Left = 99
          Top = 0
          Width = 1
          Height = 29
          Margins.Left = 10
          Margins.Top = 0
          Margins.Bottom = 0
          Align = alLeft
          ExplicitLeft = 107
          ExplicitTop = 1
          ExplicitHeight = 41
        end
        object PngSpeedButton6: TPngSpeedButton
          AlignWithMargins = True
          Left = 842
          Top = 3
          Width = 49
          Height = 23
          Hint = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1082#1086#1083#1086#1085#1086#1082
          Align = alRight
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = btToolClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002034944415478DA8D915D68526118C7FFC73C608B561195
            A3981074B18E4414AE9BADF0CA046F465117DD44C101C3035D268144D46E2D21
            C5E1F42218B21578D5551F585E0CA9083157B66A75820497AB7463EC7CF5BC3B
            C6A8B91D1F7878E13DEFEFF77C1CCE300C58452693B9AAEBBA486F0FD1095114
            F9BFDF382B413A9D66F06DBFDFDF43816C368B6030C8752D48A552159FCF37C0
            F33C4AA512AAD52A42A150F7826432A90402017BBD5EC70B0A49924E46A35183
            8D42A95A0A1289844A1D6CA9D56A28168BCF09B2B95CAE21411090CBE53617C4
            E3F1D3043CF27ABD5CABD5024B56D9E170C066B3219FCFCF74163CB8125B5AD1
            87C77F086E8FC76367F32B8A02F656D3B4D52C140A4B24BBBE5E3029C6D07B40
            82A6A0D96C624EB80C5996512E97D9DC5A7BF60F94639148E4CEBF822CC13BFB
            24EC130076FF791A0B8D058C350696097087C3E18FFF37BB2698B814C3AEFD12
            9C6EE0570DD01560DB1E60360F45AE14F96BD3273AEDC9144C5C8C6107B5EDA4
            CABF095E6C00FC5660FB5EE0FD53407E338548E55C67C1FD0BD476BF84BE76E5
            C579C0DED3869F11FCEA216E54CF6EF4A73863FC8C812323047F075AF366E55E
            A759F90BC1376737844DC13DBF8163E7819F32A0A9263CF318F8FA3A875B9F46
            60119C71F7D44BEC3E781CFD83B4380D78F7842A133C3A6709AF2D71F4E85BA8
            2B87A12E834E82BF7505B3F8035B6C1B8064E1E95A0000000049454E44AE4260
            82}
          ExplicitLeft = 768
          ExplicitTop = 0
        end
      end
    end
  end
end
