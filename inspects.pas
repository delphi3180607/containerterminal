﻿unit inspects;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, docflow, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Grid, functions;

type
  TFormInspects = class(TFormGrid)
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    pmSpec: TPopupMenu;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    plSpec: TPanel;
    Panel1: TPanel;
    sbDeleteSpec: TPngSpeedButton;
    sbExcelSpec: TPngSpeedButton;
    sbAddSpec: TPngSpeedButton;
    sbEditSpec: TPngSpeedButton;
    Bevel1: TBevel;
    dgSpec: TDBGridEh;
    Splitter1: TSplitter;
    N11: TMenuItem;
    N12: TMenuItem;
    sbConfirm: TPngSpeedButton;
    sbCancelConfirm: TPngSpeedButton;
    procedure N8Click(Sender: TObject);
    procedure sbDeleteSpecClick(Sender: TObject);
    procedure sbEditSpecClick(Sender: TObject);
    procedure sbExcelSpecClick(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure meDataBeforePost(DataSet: TDataSet);
    procedure sbConfirmClick(Sender: TObject);
    procedure sbCancelConfirmClick(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure meDataAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    objecttype: string;
    procedure Init; override;
  end;

var
  FormInspects: TFormInspects;

implementation

{$R *.dfm}

uses editdocinspect, selectobjects, selecttechcondition, editspecinspect,
  copypasteobjects, dateexecution, dmu;

procedure TFormInspects.dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
begin
  inherited;

  if Column.FieldName = 'state_code' then
   if meData.FieldByName('isconfirmed').Value = 1 then
    dgData.Canvas.Brush.Color := clLime;

  dgData.DefaultDrawColumnCell(Rect,DataCol,Column,State);

end;

procedure TFormInspects.Init;
begin
  drvData.SelectCommand.Parameters.ParamByName('objecttype').Value := self.objecttype;
  inherited;
  self.formEdit := FormEditInspect;
  self.AddEditForm(dgSpec,FormEditSpecInspect,meData);
  meSpec.Open;
end;



procedure TFormInspects.meDataAfterInsert(DataSet: TDataSet);
begin
  meData.FieldByName('doc_date').AsDateTime := now();
  meData.FieldByName('fact_datetime').AsDateTime := now();
end;

procedure TFormInspects.meDataBeforePost(DataSet: TDataSet);
begin
  meData.FieldByName('objecttype').Value := self.objecttype;
  inherited;
end;

procedure TFormInspects.N12Click(Sender: TObject);
begin
  FormCopyPasteObjects.Prepare(self.objecttype);
  FormCopyPasteObjects.ShowModal;
  if FormCopyPasteObjects.ModalResult  = mrOk then
  begin

    FormSelectTechCondition.ShowModal;
    if FormSelectTechCondition.ModalResult = mrOk then
    begin

      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('insert into docinspectspec (parent_id, object_id, techcond_id)');
      qrAux.SQL.Add('values (:parent_id, :object_id, :techcond_id)');

      qrAux.Parameters.ParamByName('parent_id').Value := meData.FieldByName('id').AsInteger;
      qrAux.Parameters.ParamByName('techcond_id').Value := FormSelectTechCondition.cbTechCond.Value;

      FormCopyPasteObjects.meSelected.First;
      while not FormCopyPasteObjects.meSelected.Eof do
      begin

        if FormCopyPasteObjects.meSelected.FieldByName('id').AsInteger>0 then
        begin
          qrAux.Parameters.ParamByName('object_id').Value := FormCopyPasteObjects.meSelected.FieldByName('id').AsInteger;
          qrAux.ExecSQL;
        end;

        FormCopyPasteObjects.meSelected.Next;

      end;

      self.meSpec.Close;
      self.meSpec.Open;

    end;

  end;
end;

procedure TFormInspects.N8Click(Sender: TObject);
begin

  FormSelectObjects.SetFilter(objecttype);
  FormSelectObjects.meSelected.EmptyTable;
  FormSelectObjects.ShowModal;

  if FormSelectObjects.ModalResult = mrOk then
  begin
    FormSelectTechCondition.ShowModal;
    if FormSelectTechCondition.ModalResult = mrOk then
    begin

      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('insert into docinspectspec (parent_id, object_id, techcond_id)');
      qrAux.SQL.Add('values (:parent_id, :object_id, :techcond_id)');

      qrAux.Parameters.ParamByName('parent_id').Value := meData.FieldByName('id').AsInteger;
      qrAux.Parameters.ParamByName('techcond_id').Value := FormSelectTechCondition.cbTechCond.Value;

      FormSelectObjects.meSelected.First;
      while not FormSelectObjects.meSelected.Eof do
      begin

        qrAux.Parameters.ParamByName('object_id').Value := FormSelectObjects.meSelected.FieldByName('id').AsInteger;
        qrAux.ExecSQL;

        FormSelectObjects.meSelected.Next;

      end;

      self.meSpec.Close;
      self.meSpec.Open;

    end;
  end;
end;

procedure TFormInspects.sbCancelConfirmClick(Sender: TObject);
var i: integer;
begin

  if (self.meData.FieldByName('isconfirmed').AsInteger = 0) then
  begin
    ShowMessage('Документ не проведен.');
    exit;
  end;

  if fQYN('Действительно снять проведение с документа(ов)?') then
  begin

    if self.dgData.Selection.SelectionType = (gstAll) then
    begin

      self.meData.First;
      while not self.meData.Eof do
      begin
        dm.spCancelConfirmInspectDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
        dm.spCancelConfirmInspectDoc.ExecProc;
        self.meData.Next;
      end;
      meData.Close;
      meData.Open;

    end else
    if self.dgData.Selection.SelectionType = (gstRecordBookmarks) then
    begin

      for i := 0 to self.dgData.Selection.Rows.Count-1 do
      begin
        self.meData.Bookmark := self.dgData.Selection.Rows[i];
        dm.spCancelConfirmInspectDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
        dm.spCancelConfirmInspectDoc.ExecProc;
      end;
      meData.Close;
      meData.Open;

    end else
    begin

      dm.spCancelConfirmInspectDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
      dm.spCancelConfirmInspectDoc.ExecProc;

      if drvData.GetrecCommand.Parameters.FindParam('current_id') <> nil then
      begin
         RefreshRecord(meData,self.meData.FieldByName('id').AsInteger);
      end else
      begin
         meData.Close;
         meData.Open;
      end;

    end;

    self.dgData.Selection.Clear;

  end;

end;

procedure TFormInspects.sbConfirmClick(Sender: TObject);
var i: integer;
begin


  if (self.meData.FieldByName('isconfirmed').AsInteger = 1) then
  begin
    ShowMessage('Документ уже проведен.');
    exit;
  end;

  FormDateExecution.ShowModal;

  if FormDateExecution.ModalResult = mrOk then
  begin

    if self.dgData.Selection.SelectionType = (gstAll) then
    begin

      self.meData.First;
      while not self.meData.Eof do
      begin
        dm.spConfirmInspectDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
        dm.spConfirmInspectDoc.ExecProc;
        self.meData.Next;
      end;
      meData.Close;
      meData.Open;

    end else
    if self.dgData.Selection.SelectionType = (gstRecordBookmarks) then
    begin

      for i := 0 to self.dgData.Selection.Rows.Count-1 do
      begin
        self.meData.Bookmark := self.dgData.Selection.Rows[i];
        dm.spConfirmInspectDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
        dm.spConfirmInspectDoc.ExecProc;
      end;
      meData.Close;
      meData.Open;

    end else
    begin

      dm.spConfirmInspectDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
      dm.spConfirmInspectDoc.ExecProc;

      if drvData.GetrecCommand.Parameters.FindParam('current_id') <> nil then
      begin
         RefreshRecord(meData,self.meData.FieldByName('id').AsInteger);
      end else
      begin
         meData.Close;
         meData.Open;
      end;

    end;

    self.dgData.Selection.Clear;

  end;

end;

procedure TFormInspects.sbDeleteSpecClick(Sender: TObject);
begin
  dgSpec.SetFocus;
  inherited sbDeleteClick(Sender);
end;

procedure TFormInspects.sbEditSpecClick(Sender: TObject);
begin
  dgSpec.SetFocus;
  inherited sbEditClick(Sender);
end;

procedure TFormInspects.sbExcelSpecClick(Sender: TObject);
begin
  dgSpec.SetFocus;
  inherited btExcelClick(Sender);
end;

end.
