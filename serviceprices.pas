﻿unit ServicePrices;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand,
  EXLReport;

type
  TFormServicePrices = class(TFormGrid)
    Panel1: TPanel;
    Panel5: TPanel;
    dgServices: TDBGridEh;
    Panel7: TPanel;
    Splitter2: TSplitter;
    drvService: TADODataDriverEh;
    meService: TMemTableEh;
    dsService: TDataSource;
    cbShowHistory: TCheckBox;
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormServicePrices: TFormServicePrices;

implementation

{$R *.dfm}

uses SPCust;

procedure TFormServicePrices.Init;
begin
  meService.Open;
  meData.Open;
  self.AddEditForm(dgData, FormEditSPCusts, meService);
end;


end.
