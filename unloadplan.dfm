﻿inherited FormUnLoadPlan: TFormUnLoadPlan
  Caption = #1055#1086#1076#1072#1095#1080' '#1085#1072' '#1088#1072#1079#1075#1088#1091#1079#1082#1091
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    inherited btnOk: TButton
      Default = True
      Font.Style = []
    end
  end
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'plan_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1042#1088#1077#1084#1103
          Width = 156
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'sheet_serve'
          Footers = <>
          Title.Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100
          Width = 149
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1087#1086#1077#1079#1076#1072
          Width = 128
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dispatch_code'
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1086#1090#1087#1088#1072#1074#1082#1080
          Width = 133
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Width = 249
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
end
