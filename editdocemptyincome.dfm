﻿inherited FormEditDocEmptyIncome: TFormEditDocEmptyIncome
  Caption = #1040#1082#1090' '#1087#1088#1080#1077#1084#1072' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074
  ClientHeight = 428
  ClientWidth = 499
  Font.Height = -12
  OnShow = FormShow
  ExplicitWidth = 505
  ExplicitHeight = 456
  PixelsPerInch = 96
  TextHeight = 14
  object Label14: TLabel [0]
    Left = 305
    Top = 11
    Width = 118
    Height = 14
    Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1091#1087#1083#1077#1085#1080#1103
  end
  object Label1: TLabel [1]
    Left = 9
    Top = 11
    Width = 114
    Height = 14
    Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object Label3: TLabel [2]
    Left = 166
    Top = 11
    Width = 105
    Height = 14
    Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object Label18: TLabel [3]
    Left = 9
    Top = 212
    Width = 130
    Height = 14
    Caption = #1044#1072#1085#1085#1099#1077' '#1072#1074#1090#1086#1084#1086#1073#1080#1083#1103
  end
  object Label4: TLabel [4]
    Left = 9
    Top = 164
    Width = 141
    Height = 14
    Caption = #1042#1086#1076#1080#1090#1077#1083#1100' '#1101#1082#1089#1087#1077#1076#1080#1090#1086#1088
  end
  object Label8: TLabel [5]
    Left = 9
    Top = 111
    Width = 77
    Height = 14
    Caption = #1055#1077#1088#1077#1074#1086#1079#1095#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbForwarder: TSpeedButton [6]
    Left = 455
    Top = 129
    Width = 30
    Height = 24
    Caption = '...'
    OnClick = sbForwarderClick
  end
  object Label5: TLabel [7]
    Left = 9
    Top = 271
    Width = 92
    Height = 14
    Caption = #1044#1086#1074#1077#1088#1077#1085#1085#1086#1089#1090#1100
  end
  object Label6: TLabel [8]
    Left = 9
    Top = 332
    Width = 81
    Height = 14
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
  end
  object Label16: TLabel [9]
    Left = 9
    Top = 60
    Width = 157
    Height = 14
    Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080'/'#1054#1087#1077#1088#1072#1094#1080#1103
  end
  object Label2: TLabel [10]
    Left = 224
    Top = 60
    Width = 100
    Height = 14
    Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1081
  end
  inherited plBottom: TPanel
    Top = 387
    Width = 499
    TabOrder = 10
    ExplicitTop = 387
    ExplicitWidth = 499
    inherited btnCancel: TButton
      Left = 383
      ExplicitLeft = 383
    end
    inherited btnOk: TButton
      Left = 264
      OnClick = btnOkClick
      ExplicitLeft = 264
    end
  end
  object dtIncome: TDBDateTimeEditEh [12]
    Left = 305
    Top = 30
    Width = 146
    Height = 22
    DataField = 'date_income'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edDocNumber: TDBEditEh [13]
    Left = 9
    Top = 30
    Width = 147
    Height = 22
    DataField = 'doc_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object dtDocDate: TDBDateTimeEditEh [14]
    Left = 166
    Top = 30
    Width = 121
    Height = 22
    DataField = 'doc_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object edCarData: TDBEditEh [15]
    Left = 9
    Top = 232
    Width = 442
    Height = 22
    DataField = 'car_data'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    Visible = True
  end
  object edDriver: TDBEditEh [16]
    Left = 9
    Top = 184
    Width = 442
    Height = 22
    DataField = 'driver_name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    Visible = True
  end
  object edAttorney: TDBEditEh [17]
    Left = 9
    Top = 291
    Width = 442
    Height = 22
    DataField = 'attorney'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    Visible = True
  end
  object edNote: TDBEditEh [18]
    Left = 9
    Top = 352
    Width = 442
    Height = 22
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    Visible = True
  end
  object laDealer: TDBSQLLookUp [19]
    Left = 9
    Top = 131
    Width = 441
    Height = 22
    DataField = 'dealer_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 5
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object laDlvTypes: TDBSQLLookUp [20]
    Left = 9
    Top = 80
    Width = 209
    Height = 22
    Color = 14810109
    DataField = 'dlv_type_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 3
    Visible = True
    SqlSet = ssDlvTypes
    RowCount = 0
  end
  object laPersons: TDBSQLLookUp [21]
    Left = 224
    Top = 80
    Width = 226
    Height = 22
    DataField = 'inspector_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 4
    Visible = True
    SqlSet = ssPersons
    RowCount = 0
  end
  inherited dsLocal: TDataSource
    Left = 145
    Top = 319
  end
  inherited qrAux: TADOQuery
    Left = 201
    Top = 319
  end
  object ssDlvTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from deliverytypes d where exists '
      '('
      'select 1 from docroutes r, doctypes t, docroute2objectstates dro'
      
        'where r.end_doctype_id = t.id and t.system_section = '#39'income_emp' +
        'ty'#39
      'and r.dlv_type_id = d.id and dro.docroute_id = r.id '
      
        'and isnull(dro.start_object_state_id,0) <= 0 and isnull(dro.star' +
        't_real_state_id,0) <= 0 '
      'and dro.objecttype_id = 1'
      ')'
      'order by code')
    DownSql.Strings = (
      'select * from deliverytypes d where exists '
      '('
      'select 1 from docroutes r, doctypes t, docroute2objectstates dro'
      
        'where r.end_doctype_id = t.id and t.system_section = '#39'income_emp' +
        'ty'#39
      'and r.dlv_type_id = d.id and dro.docroute_id = r.id '
      
        'and isnull(dro.start_object_state_id,0) = 0 and isnull(dro.start' +
        '_real_state_id,0) = 0 '
      'and dro.objecttype_id = 1'
      ')'
      'order by code'
      '')
    InitSql.Strings = (
      'select * from deliverytypes where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 113
    Top = 70
  end
  object ssPersons: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select * from persons where isnull(isblocked,0) = 0 and person_k' +
        'ind = 0'
      'order by person_name')
    DownSql.Strings = (
      
        'select * from persons where isnull(isblocked,0) = 0  and person_' +
        'kind = 0'
      'order by person_name')
    InitSql.Strings = (
      'select * from persons where id = @id')
    KeyName = 'id'
    DisplayName = 'person_name'
    Connection = dm.connMain
    Left = 329
    Top = 70
  end
end
