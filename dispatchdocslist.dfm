﻿inherited FormDispatchDocsList: TFormDispatchDocsList
  Caption = 
    #1042#1099#1073#1077#1088#1080#1090#1077' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1091#1073#1086#1088#1082#1080', '#1074' '#1082#1086#1090#1086#1088#1099#1081' '#1076#1086#1073#1072#1074#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1077' '#1082#1086#1085#1090#1077#1081#1085#1077#1088 +
    #1099
  ClientHeight = 500
  ClientWidth = 881
  ExplicitWidth = 897
  ExplicitHeight = 538
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 459
    Width = 881
    Visible = True
    ExplicitTop = 357
    ExplicitWidth = 695
    inherited btnOk: TButton
      Left = 400
      Width = 359
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1080' '#1074' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
      ExplicitLeft = 400
      ExplicitWidth = 359
    end
    inherited btnCancel: TButton
      Left = 765
      ExplicitLeft = 579
    end
  end
  inherited plAll: TPanel
    Width = 881
    Height = 459
    ExplicitWidth = 695
    ExplicitHeight = 357
    inherited plTop: TPanel
      Width = 881
      ExplicitWidth = 695
      inherited btTool: TPngSpeedButton
        Left = 829
        ExplicitLeft = 643
      end
    end
    inherited dgData: TDBGridEh
      Width = 881
      Height = 430
      TitleParams.MultiTitle = True
      Columns = <
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 77
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 129
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 115
        end
        item
          CellButtons = <>
          Checkboxes = False
          DynProps = <>
          EditButtons = <>
          FieldName = 'dispatch_code'
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1086#1090#1087#1088#1072#1074#1082#1080
          Width = 104
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1087#1086#1077#1079#1076#1072
          Width = 106
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'station_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1085#1094#1080#1103
          Visible = False
          Width = 153
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'direction_code'
          Footers = <>
          Title.Caption = #1053#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077
          Visible = False
          Width = 148
        end
        item
          CellButtons = <>
          DisplayFormat = 'DD.MM.YYYY HH:NN:SS'
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_notification'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1091#1074#1077#1076#1086#1084#1083#1077#1085#1080#1103
          Width = 131
        end
        item
          CellButtons = <>
          DisplayFormat = 'DD.MM.YYYY HH:NN:SS'
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_remove'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1089#1098#1077#1084#1082#1080
          Width = 114
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'sheet_remove'
          Footers = <>
          Title.Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100' '#1091#1073#1086#1088#1082#1080
          Width = 120
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select d.id as id, d.*, s.*, r.code as direction_code,'
      
        '(select name from stations st where st.id = s.station_id) as sta' +
        'tion_code,'
      
        '(select code from dispatchkinds k where k.id = s.dispatchkind_id' +
        ') as dispatch_code,'
      
        '(select count(*) where exists (select 1 from f_GetLinkedDocument' +
        's(d.id))) as docexists,'
      
        '(case when d.isconfirmed = 1 then '#39#43#39' else '#39#39' end) as sta' +
        'te_code'
      'from documents d, docdispatch s '
      'left outer join directions r on (r.id = s.direction_id)'
      'where s.id = d.id '
      'and isnull(d.isconfirmed,0) = 0 and isnull(d.isdeleted,0) = 0'
      'order by d.doc_date desc')
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
    inherited aDelete: TAction
      Enabled = False
    end
    inherited aEdit: TAction
      Enabled = False
    end
  end
end
