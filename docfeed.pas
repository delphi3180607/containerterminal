﻿unit docfeed;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, docflow, System.Actions, Vcl.ActnList, EXLReportExcelTLB,
  EXLReportBand, EXLReport, Vcl.ComCtrls, Functions, Vcl.Mask, DBCtrlsEh, Vcl.DBCtrls,
  System.ImageList, Vcl.ImgList, PngImageList;

type
  TFormDocFeed = class(TFormDocFlow)
    meMass: TMemTableEh;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N20: TMenuItem;
    sbCheck: TPngSpeedButton;
    N21: TMenuItem;
    plNotes: TPanel;
    edNote: TDBEdit;
    edUnloadFullText: TDBEdit;
    N22: TMenuItem;
    sbFilterPlan: TPngSpeedButton;
    Timer1: TTimer;
    procedure btGenerateClick(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure meDataAfterPost(DataSet: TDataSet);
    procedure meDataAfterInsert(DataSet: TDataSet);
    procedure meDataAfterEdit(DataSet: TDataSet);
    procedure sbAddClick(Sender: TObject);
    procedure sbEditClick(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure N20Click(Sender: TObject);
    procedure dgDataSelectionChanged(Sender: TObject);
    procedure sbCheckClick(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure sbImport2Click(Sender: TObject);
    procedure sbFilterPlanClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure sbClearFilterClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure dgDataCellClick(Column: TColumnEh);
    procedure meDataAfterOpen(DataSet: TDataSet);
  private
    flag: boolean;

    safeid: integer;
    safefield: string;

  public
    planid:  integer;
    procedure ImportSheet(data:Variant; DimX:integer; DimY:integer; trainnum: string; prefix: string; startnumber: integer);
    procedure ReplicateData;
    procedure SetRequiredFields;
    procedure UnSetRequiredFields;
    procedure Init; override;
  end;

var
  FormDocFeed: TFormDocFeed;

implementation

{$R *.dfm}

uses editcargosoncarriages, docgenerate, dmu,
  editcargomassfill, deliverytypes, main, setprefix, editdateregister, seek,
  ExtraInfo, EditCheck, NoteFilter, EditDocFeed, importdocfeed, setprefixshort,
  unloadplan;

procedure TFormDocFeed.btGenerateClick(Sender: TObject);
var doctype_id: integer;
begin
  FormDocGenerate.ShowModal;
  if FormDocGenerate.ModalResult = mrOk then
  begin
    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select t.id from settings s, doctypes t where s.code = ''cargodoctype'' and s.value = t.code');
    qrAux.Open;
    doctype_id := qrAux.Fields[0].AsInteger;

  end;
end;

procedure TFormDocFeed.Init;
begin
  self.formEdit := FormEditDocFeed;
  self.system_section := 'income_unload';
  flag := false;
  inherited;
  dgData.DataGrouping.GroupLevels[0].CollapseNodes;
  planid := 0;
  allowcancel := true;
end;

procedure TFormDocFeed.meDataAfterEdit(DataSet: TDataSet);
begin
  inherited;
  //if not flag then SetRequiredFields;
end;

procedure TFormDocFeed.meDataAfterInsert(DataSet: TDataSet);
begin
  inherited;
  //if not flag then SetRequiredFields;
end;

procedure TFormDocFeed.meDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  meDataAfterScroll(DataSet);
end;

procedure TFormDocFeed.meDataAfterPost(DataSet: TDataSet);
begin
  inherited;
  FormDeliveryTypes.meData.Filter := '';
  FormDeliveryTypes.meData.Filtered := false;
end;

procedure TFormDocFeed.meDataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  drvData.SelectCommand.Parameters.ParamByName('planid').Value := self.planid;
end;

procedure TFormDocFeed.SetRequiredFields;
begin
  {
  self.meData.FieldByName('container_num').Required := true;
  self.meData.FieldByName('carriage_num').Required := true;
  self.meData.FieldByName('container_owner_id').Required := true;
  self.meData.FieldByName('carriage_owner_id').Required := true;
  }
  self.meData.FieldByName('weight_fact').Required := true;

end;


procedure TFormDocFeed.Timer1Timer(Sender: TObject);
begin
  if self.Visible then
  begin
    meData.Close;
    meData.Open;
  end;
end;

procedure TFormDocFeed.UnSetRequiredFields;
begin

  self.meData.FieldByName('container_num').Required := false;
  self.meData.FieldByName('carriage_num').Required := false;
  self.meData.FieldByName('container_owner_id').Required := false;
  self.meData.FieldByName('carriage_owner_id').Required := false;
  self.meData.FieldByName('weight_fact').Required := false;

end;


procedure TFormDocFeed.N16Click(Sender: TObject);
var i: integer; b: TBookmark;
begin

  flag := true;
  UnSetRequiredFields;

  b := meData.Bookmark;

  meData.DisableControls;

  meMass.Close;
  meMass.LoadFromDataSet(meData,0,lmCopy,false);
  meMass.Open;
  meMass.EmptyTable;
  meMass.Append;
  FormEditCargoMassFill.ShowModal;

  if FormEditCargoMassFill.ModalResult = mrOk then
  begin

    //meMass.Post;

    if self.dgData.Selection.SelectionType = (gstAll) then
    begin

      self.meData.First;
      while not self.meData.Eof do
      begin
        ReplicateData;
        self.meData.Next;
      end;
      //meData.Close;
      //meData.Open;

    end else
    if self.dgData.Selection.SelectionType = (gstRecordBookmarks) then
    begin

      for i := 0 to self.dgData.Selection.Rows.Count-1 do
      begin
        self.meData.Bookmark := self.dgData.Selection.Rows[i];
        ReplicateData;
      end;
      //meData.Close;
      //meData.Open;

    end else
    begin

      ReplicateData;

      if drvData.GetrecCommand.Parameters.FindParam('current_id') <> nil then
      begin
         RefreshRecord(meData,self.meData.FieldByName('id').AsInteger);
      end else
      begin
         //meData.Close;
         //meData.Open;
      end;

    end;

    meMass.Close;

  end;

  flag := false;
  SetRequiredFields;

  meData.Bookmark := b;
  meData.EnableControls;

end;

procedure TFormDocFeed.N20Click(Sender: TObject);
begin
  dgData.DataGrouping.GroupLevels[0].CollapseNodes;
end;

procedure TFormDocFeed.N22Click(Sender: TObject);
begin
  FormNoteFilter.ShowModal;
  if FormNoteFilter.ModalResult = mrOk then
  begin
    meData.SetFilterText('cargo_description like ''%%'+trim(FormNoteFilter.leNote.Text)+'%''');
    meData.Filtered := true;
    plStatus.Color := $004080FF;
  end;

  if FormNoteFilter.ModalResult = mrYes then
  begin
    meData.Filtered := false;
    meData.SetFilterText('');
    plStatus.Color := clCream
  end;

end;

procedure TFormDocFeed.ReplicateData;
var i: integer;
begin
  if (meData.FieldByName('isconfirmed').Value = 1) then exit;
  meData.Edit;
  for i := 0 to meMass.FieldCount-1 do
  begin
    if (meMass.Fields[i].Value <> null) and (meMass.Fields[i].AsString <> '-1') and (meMass.Fields[i].AsString <> '') then
    begin
      meData.Fields[i].Value := meMass.Fields[i].Value;
    end;
  end;
  meData.Post;
end;

procedure TFormDocFeed.sbAddClick(Sender: TObject);
begin
  SetRequiredFields;
  inherited;
  UnSetRequiredFields;
end;

procedure TFormDocFeed.sbCheckClick(Sender: TObject);
begin

  if meData.FieldByName('isconfirmed').AsInteger = 1 then exit;

  meData.Edit;

  if meData.FieldByName('check_result').AsInteger = 0
  then meData.FieldByName('check_result').Value := 1;

  if meData.FieldByName('check_datetime').Value = null
  then meData.FieldByName('check_datetime').Value := now();

  FormEditCheck.dsLocal.DataSet := meData;
  FormEditCheck.ShowModal;

  if FormEditCheck.ModalResult = mrOk then
    meData.Post
  else if FormEditCheck.ModalResult = mrNo then
  begin
    meData.FieldByName('check_result').Value := null;
    meData.FieldByName('check_datetime').Value := null;
    meData.Post;
  end else
    meData.Cancel;

  FormEditCheck.dsLocal.DataSet := nil;

end;

procedure TFormDocFeed.sbClearFilterClick(Sender: TObject);
begin
  self.planid := 0;
  sbFilterPlan.Down := false;
  inherited;
end;

procedure TFormDocFeed.sbEditClick(Sender: TObject);
begin
  SetRequiredFields;
  inherited;
  UnSetRequiredFields;
end;

procedure TFormDocFeed.sbFilterPlanClick(Sender: TObject);
var lp: TPairValues; pp: TPairParam; fGrid: TForm;
begin

  fGrid := nil;

  pp.paramname := '';
  lp := SFDV(FormUnloadPlan, meData.FieldByName('load_plan_id').AsInteger, 'note', pp, fGrid);

  Screen.Cursor := crHourGlass;

  if lp.keyvalue>0 then
  begin
    self.planid := lp.keyvalue;
    meData.Close;
    meData.Open;
    sbFilterPlan.Down := true;
  end;

  if lp.keyvalue<0 then
  begin
    self.planid := 0;
    meData.Close;
    meData.Open;
    sbFilterPlan.Down := false;
  end;
  Screen.Cursor := crDefault;

end;

procedure TFormDocFeed.sbImport2Click(Sender: TObject);
begin
  FormImportDocFeed.ShowModal;
  if FormImportDocFeed.ModalResult = mrOk then
  begin
    Screen.Cursor:=crHourGlass;
    Application.ProcessMessages;
    try
      FormSetPrefixShort.edPrefix.Text := FormatDateTime('ddmm',now());
      FormSetPrefixShort.neStartNumber.Value := 1;
      FormSetPrefixShort.ShowModal;
      if FormSetPrefix.ModalResult = mrOk then
      ImportSheet(FormImportDocFeed.FData, FormImportDocFeed.DimX, FormImportDocFeed.DimY, FormSetPrefix.edKP.Text, FormSetPrefix.edPrefix.Text, FormSetPrefix.neStartNumber.Value);
    finally
      meData.Close;
      meData.Open;
      Screen.Cursor:=crDEFAULT;
      dgData.DataGrouping.GroupLevels[0].CollapseNodes;
    end;
  end;
end;


procedure TFormDocFeed.dgDataCellClick(Column: TColumnEh);
begin
  inherited;
  Timer1.Enabled := false;
  Timer1.Enabled := true;
end;

procedure TFormDocFeed.dgDataDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

  if Column.FieldName = 'check_text' then
  begin

   if meData.FieldByName('check_text').AsString  = '+' then
    dgData.Canvas.Font.Color := clGreen;

   if meData.FieldByName('check_text').AsString  = '~' then
    dgData.Canvas.Font.Color := clMaroon;

  end;

  inherited;

end;

procedure TFormDocFeed.dgDataSelectionChanged(Sender: TObject);
var i: integer;
begin
  if dgData.Selection.SelectionType = (gstRectangle) then
  begin

    if self.safefield = '' then exit;
    meData.Bookmark := dgData.Selection.Rect.BottomRow;

    i := 0;

    Screen.Cursor := crHourGlass;

    while true do
    begin

      if self.meData.FieldByName(self.safefield).Value <> self.safeid then
      begin
        try
          Screen.Cursor := crHourGlass;
          self.meData.Edit;
          self.meData.FieldByName(self.safefield).Value := self.safeid;
          self.meData.Post;
        except
          //--
          Screen.Cursor := crDefault;
        end;
      end;

      if meData.CompareBookmarks(meData.Bookmark , dgData.Selection.Rect.TopRow) = 0
      then break;

      meData.Prior;
      i := i+1;

      //if i>3 then break;

    end;

    Screen.Cursor := crDefault;

  end;
end;

procedure TFormDocFeed.ImportSheet(data:Variant; DimX:integer; DimY:integer; trainnum: string; prefix: string; startnumber: integer);
var i: integer; error: integer;
var conttype, contnumber, contowner, weight, datesent, carowner, carnumber, seal, descr: string;
var g: string; gg: TGuid;
begin

  CreateGUID(gg);
  g := GuidToString(gg);

  for i := 4 to DimY do
  begin

      error := 0;

      conttype := data[i,1];
      contnumber := data[i,2];
      contowner := data[i,3];
      weight := data[i,4];
      datesent := data[i,5];
      carowner := data[i,7];
      carnumber := data[i,8];
      seal := data[i,9];
      descr := StringReplace(data[i,11], '   ',' ', [rfReplaceAll]);
      descr := StringReplace(descr, #9,' ', [rfReplaceAll]);
      descr := StringReplace(descr, '   ',' ', [rfReplaceAll]);
      descr := StringReplace(descr, '  ',' ', [rfReplaceAll]);
      descr := StringReplace(descr, '  ',' ', [rfReplaceAll]);
      descr := StringReplace(descr, '  ',' ', [rfReplaceAll]);

      qrAux.ParamCheck := true;

      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('insert into ExternalCargoSheet (guid, conttype, contnumber, contowner, weight, datesent, carowner, carnumber, seal, descr)');
      qrAux.SQL.Add('values( :guid, :conttype, :contnumber, :contowner, :weight, :datesent, :carowner, :carnumber, :seal, :descr); commit;');

      qrAux.Parameters.ParamByName('guid').Value := g;
      qrAux.Parameters.ParamByName('conttype').Value := conttype;
      qrAux.Parameters.ParamByName('conttype').Value := conttype;
      qrAux.Parameters.ParamByName('contnumber').Value := contnumber;
      qrAux.Parameters.ParamByName('contowner').Value := contowner;
      qrAux.Parameters.ParamByName('weight').Value := weight;
      qrAux.Parameters.ParamByName('datesent').Value := datesent;
      qrAux.Parameters.ParamByName('carowner').Value := carowner;
      qrAux.Parameters.ParamByName('carnumber').Value := carnumber;
      qrAux.Parameters.ParamByName('seal').Value := seal;
      qrAux.Parameters.ParamByName('descr').Value := descr;

      try
        qrAux.ExecSQL;
      except
        on E : Exception do
        begin
          //ShowMessage(qrAux.SQL.Text);
          error := 1;
          ShowMessage('Ошибка при импорте: '+E.Message);
          exit;
        end;
      end;
  end;

  if error = 0 then
  begin

      dm.spImportExternalData.Parameters.ParamByName('@guid').Value := g;
      dm.spImportExternalData.Parameters.ParamByName('@systemsection').Value := 'income_unload';
      dm.spImportExternalData.Parameters.ParamByName('@trainnum').Value := trainnum;
      dm.spImportExternalData.Parameters.ParamByName('@folderid').Value := self.meFolders.FieldByName('id').AsInteger;
      dm.spImportExternalData.Parameters.ParamByName('@prefix').Value := prefix;
      dm.spImportExternalData.Parameters.ParamByName('@startnumber').Value := startnumber;
      dm.spImportExternalData.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
      dm.spImportExternalData.Parameters.ParamByName('@loadtype').Value := 0;
      dm.spImportExternalData.Parameters.ParamByName('@emptytype').Value := 0;

      try
        dm.spImportExternalData.ExecProc;
      except
        on E : Exception do
        begin
          ShowMessage('Сообщения при импорте(!): '+#10+E.Message);
        end;
      end;

  end;

end;



{

var id: integer;
begin

  if meData.ReadOnly then exit;

  id := ColumnSelectList(dgData, Text);

  self.safeid := id;

  meData.Edit;
  meData.FieldByName('container_owner_id').Value := id;
  Handled := true;
  meData.Post;

end;

}

end.
