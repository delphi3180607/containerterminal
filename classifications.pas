﻿unit Classifications;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  Vcl.ExtCtrls, EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions,
  Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus,
  Vcl.StdCtrls, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Mask, DBCtrlsEh,
  Vcl.Buttons, PngSpeedButton, functions;

type
  TFormClassifications = class(TFormGrid)
    dgSpec: TDBGridEh;
    Splitter1: TSplitter;
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    pmSpec: TPopupMenu;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure meSpecBeforePost(DataSet: TDataSet);
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure meSpecBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormClassifications: TFormClassifications;

implementation

{$R *.dfm}

uses EditClassification, EditClassifValue;

procedure TFormClassifications.Init;
begin
  self.tablename := 'classifications';
  self.formEdit := FormEditClassification;
  meData.Close;
  meData.Open;
  meSpec.Close;
  meSpec.Open;
end;


procedure TFormClassifications.meDataAfterScroll(DataSet: TDataSet);
begin
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormClassifications.meSpecBeforeOpen(DataSet: TDataSet);
begin
  drvSpec.SelectCommand.Parameters.ParamByName('classification_id').Value := meData.FieldByName('id').AsInteger;
end;

procedure TFormClassifications.meSpecBeforePost(DataSet: TDataSet);
begin
  meSpec.FieldByName('classification_id').Value := meData.FieldByName('id').AsInteger;
end;

procedure TFormClassifications.N10Click(Sender: TObject);
begin
  inherited;
  EE(nil, FormEditClassifValue, meSpec, 'classificationvalues');
end;

procedure TFormClassifications.N8Click(Sender: TObject);
begin
  EA(nil, FormEditClassifValue, meSpec, 'classificationvalues');
end;

procedure TFormClassifications.N9Click(Sender: TObject);
begin
  ED(meSpec);
end;

end.
