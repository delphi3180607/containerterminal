﻿unit CarriagesList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Vcl.Mask, DBCtrlsEh;

type
  TFormCarriagesList = class(TFormGrid)
    sbImport2: TPngSpeedButton;
    plStatus: TPanel;
    sbClearFilter: TPngSpeedButton;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    procedure sbImport2Click(Sender: TObject);
    procedure btFilterClick(Sender: TObject);
    procedure sbClearFilterClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure N9Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
  private
    g: string;
    gu: TGuid;
  protected
    procedure AssignSelField(colname: string); override;
  public
    procedure Init; override;
    procedure ImportSheet(data:Variant; DimX:integer; DimY:integer);
    procedure SetFilter(conditions: string);
  end;

var
  FormCarriagesList: TFormCarriagesList;

implementation

{$R *.dfm}

uses editcarriage, rotations, techconditions, Import, dmu, filterlite,
  editcarincome, editcarmodel, selectcounteragent, MissingValues;

procedure TFormCarriagesList.Init;
begin
  self.formEdit := FormEditCarriage;
  FormTechConditions.meData.Open;
  inherited;
  isbiglist := true;
  tablename := 'objects';
end;


procedure TFormCarriagesList.AssignSelField(colname: string);
begin
  selfield := '';
  if colname = 'date_income' then selfield := 'date_income';
  if colname = 'car_paper_number' then selfield := 'car_paper_number';
  if colname = 'train_num' then selfield := 'train_num';
end;


procedure TFormCarriagesList.meDataBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  if Assigned(drvData.SelectCommand.Parameters.FindParam('guid')) then
      drvData.SelectCommand.Parameters.ParamByName('guid').Value := g;
end;

procedure TFormCarriagesList.N10Click(Sender: TObject);
var id: integer;
begin
  FormEditCarModel.ShowModal;
  if FormEditCarModel.ModalResult = mrOk then
  begin

    CreateGuid(gu);
    g := GuidToString(gu);
    FillSelectListAll(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('update carriages set model_id = :model_id');
    qrAux.SQL.Add('where id in (select id from selectlist where guid = '''+g+''')');
    qrAux.Parameters.ParamByName('model_id').Value := FormEditCarModel.luModel.KeyValue;
    qrAux.ExecSQL;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from selectlist where guid = '''+g+'''');
    qrAux.ExecSQL;

    id := meData.FieldByName('id').AsInteger;
    meData.Close;
    meData.Open;
    meData.Locate('id', id, []);

  end;
end;

procedure TFormCarriagesList.N11Click(Sender: TObject);
var id: integer;
begin
  FormSelectCounteragent.ShowModal;
  if FormSelectCounteragent.ModalResult = mrOk then
  begin
    CreateGuid(gu);
    g := GuidToString(gu);
    FillSelectListAll(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('update objects set owner_id = :owner_id');
    qrAux.SQL.Add('where id in (select id from selectlist where guid = '''+g+''')');
    qrAux.Parameters.ParamByName('owner_id').Value := FormSelectCounteragent.luCustomer.KeyValue;
    qrAux.ExecSQL;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from selectlist where guid = '''+g+'''');
    qrAux.ExecSQL;

    id := meData.FieldByName('id').AsInteger;
    meData.Close;
    meData.Open;
    meData.Locate('id', id, []);

  end;
end;

procedure TFormCarriagesList.N13Click(Sender: TObject);
begin
  ClearCells;
end;

procedure TFormCarriagesList.N9Click(Sender: TObject);
begin
  FormEditCarIncome.ShowModal;
  if FormEditCarIncome.ModalResult = mrOk then
  begin
    CreateGuid(gu);
    g := GuidToString(gu);
    FillSelectListAll(g);
    dm.spSetCarIncomeDate.Parameters.ParamByName('@guid').Value := g;
    dm.spSetCarIncomeDate.Parameters.ParamByName('@dateincome').Value := FormEditCarIncome.dtServeDate.Value;
    dm.spSetCarIncomeDate.Parameters.ParamByName('@sheetnumber').Value := FormEditCarIncome.edServeSheet.Text;
    dm.spSetCarIncomeDate.Parameters.ParamByName('@trainnum').Value := FormEditCarIncome.edKP.Text;
    dm.spSetCarIncomeDate.ExecProc;
    SetFilter(FormFilterLite.conditions);
  end;
end;

procedure TFormCarriagesList.sbClearFilterClick(Sender: TObject);
begin
  SetFilter('');
end;

procedure TFormCarriagesList.sbImport2Click(Sender: TObject);
begin
  FormImport.ShowModal;
  if FormImport.ModalResult = mrOk then
  begin
    Screen.Cursor:=crHourGlass;
    Application.ProcessMessages;
    try
      ImportSheet(FormImport.FData, FormImport.DimX, FormImport.DimY);
    finally
      meData.Close;
      meData.Open;
      Screen.Cursor:=crDEFAULT;
    end;
  end;
end;


procedure TFormCarriagesList.btFilterClick(Sender: TObject);
var fl: TStringList;
begin
  FormFilterLite.ShowModal;
  if FormFilterLite.ModalResult in [mrOk, mrYes] then
  begin
    CreateGuid(gu);
    g := GuidToString(gu);
    SetFilter(FormFilterLite.conditions);
    fl := TStringList.Create;
    fl.AddStrings(FormFilterLite.meNumbers.Lines);
    CheckMissingFilterValues(fl,'pnum');
  end;
end;

procedure TFormCarriagesList.ImportSheet(data:Variant; DimX:integer; DimY:integer);
var i: integer; var carnumber, kindcode, model, techcond, owner: string;
begin

  for i := 2 to DimY do
  begin

    carnumber := data[i,1];
    kindcode := data[i,3];
    model := data[i,4];
    techcond := data[i,5];
    owner := data[i,15];

    dm.spUpdateRegisterCarriageFull.Parameters.ParamByName('@CNum').Value := carnumber;
    dm.spUpdateRegisterCarriageFull.Parameters.ParamByName('@KindCode').Value := kindcode;
    dm.spUpdateRegisterCarriageFull.Parameters.ParamByName('@OwnerCode').Value := owner;
    dm.spUpdateRegisterCarriageFull.Parameters.ParamByName('@ModelCode').Value := model;
    dm.spUpdateRegisterCarriageFull.Parameters.ParamByName('@TechCondCode').Value := techcond;

    try
      dm.spUpdateRegisterCarriageFull.ExecProc;
    except
      on E : Exception do
      begin
        //ShowMessage(qrAux.SQL.Text);
        ShowMessage('Ошибка при импорте: '+E.Message);
        exit;
      end;
    end;
  end;

end;

procedure TFormCarriagesList.SetFilter(conditions: string);
begin

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := conditions;
    dm.spCreateFilter.ExecProc;

    meData.Close;
    meData.Open;

    if conditions = '' then
      plStatus.Caption := ' Фильтр снят.'
    else
    begin
      plStatus.Caption := ' Установлен фильтр по списку номеров.';
    end;

end;



end.
