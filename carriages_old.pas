﻿unit carriages_old;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB, functions,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand, EXLReport;

type
  TFormCarriagesOld = class(TFormGrid)
    plObjects: TPanel;
    Panel2: TPanel;
    sbAddObject: TPngSpeedButton;
    sbDeleteObject: TPngSpeedButton;
    sbEditObject: TPngSpeedButton;
    dgObjects: TDBGridEh;
    Panel1: TPanel;
    dgStates: TDBGridEh;
    Splitter2: TSplitter;
    Splitter1: TSplitter;
    drvOwner: TADODataDriverEh;
    meOwner: TMemTableEh;
    dsOwner: TDataSource;
    drvState: TADODataDriverEh;
    meStates: TMemTableEh;
    dsStates: TDataSource;
    procedure sbDeleteObjectClick(Sender: TObject);
    procedure meDataAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormCarriagesOld: TFormCarriagesOld;

implementation

{$R *.dfm}

uses editcarriage, editowner;


procedure TFormCarriagesOld.Init;
begin
  inherited;
  self.formEdit := FormEditCarriage;
  self.meOwner.Open;
  //self.meStates.Open;
  meDataAfterScroll(nil);
end;


procedure TFormCarriagesOld.meDataAfterScroll(DataSet: TDataSet);
begin

  if self.meData.FieldByName('id').AsString <> '' then
  begin

    meOwner.Filter := 'carriage_id = '+self.meData.FieldByName('id').AsString;
    meOwner.Filtered := true;

  end else
  begin

    meOwner.Filter := 'carriage_id = -1';
    meOwner.Filtered := true;

  end;

end;

procedure TFormCarriagesOld.sbDeleteObjectClick(Sender: TObject);
begin
  ED(self.meOwner);
end;



end.
