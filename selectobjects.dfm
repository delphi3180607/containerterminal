﻿inherited FormSelectObjects: TFormSelectObjects
  Caption = #1042#1099#1073#1086#1088' '#1080' '#1076#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1086#1073#1100#1077#1082#1090#1086#1074
  ClientHeight = 645
  ClientWidth = 929
  Font.Height = -11
  ExplicitWidth = 935
  ExplicitHeight = 673
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 604
    Width = 929
    ExplicitTop = 604
    ExplicitWidth = 929
    inherited btnCancel: TButton
      Left = 813
      ExplicitLeft = 813
    end
    inherited btnOk: TButton
      Left = 576
      Width = 231
      Caption = #1055#1077#1088#1077#1081#1090#1080' '#1082' '#1087#1083#1072#1085#1091' '#1087#1086#1075#1088#1091#1079#1082#1080
      ExplicitLeft = 576
      ExplicitWidth = 231
    end
    object nuLen: TDBNumberEditEh
      AlignWithMargins = True
      Left = 442
      Top = 3
      Width = 121
      Height = 35
      Margins.Right = 10
      Align = alRight
      DisplayFormat = '### ##0.00'
      DynProps = <>
      Enabled = False
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      Visible = True
      ExplicitHeight = 31
    end
  end
  object plLeft: TPanel [1]
    Left = 0
    Top = 0
    Width = 929
    Height = 345
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object plCaption: TPanel
      Left = 0
      Top = 0
      Width = 929
      Height = 19
      Align = alTop
      BevelOuter = bvNone
      Caption = #1042#1072#1075#1086#1085#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object dgList: TDBGridEh
      Left = 0
      Top = 19
      Width = 929
      Height = 326
      Align = alClient
      AutoFitColWidths = True
      DataSource = dsData
      DynProps = <>
      Flat = True
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      IndicatorOptions = [gioShowRowIndicatorEh]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
      STFilter.Local = True
      STFilter.Visible = True
      STFilter.Color = 15269887
      STFilter.HorzLineColor = clTeal
      STFilter.VertLineColor = clTeal
      TabOrder = 1
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -11
      TitleParams.Font.Name = 'Verdana'
      TitleParams.Font.Style = [fsBold]
      TitleParams.ParentFont = False
      OnDblClick = sbAddClick
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'pnum'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 106
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_income'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1087#1088#1080#1073#1099#1090#1080#1103
          Width = 103
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 101
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          Title.Caption = #1058#1080#1087
          Width = 96
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'model_code'
          Footers = <>
          Title.Caption = #1052#1086#1076#1077#1083#1100
          Width = 93
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_length'
          Footers = <>
          Title.Caption = #1044#1083#1080#1085#1072
          Width = 65
        end
        item
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isdefective'
          Footers = <>
          Title.Caption = #1053#1077#1080#1089#1087#1088'?'
          Width = 61
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
          Width = 120
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object plMiddle: TPanel [2]
    Left = 0
    Top = 345
    Width = 929
    Height = 27
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    object sbAdd: TSpeedButton
      Left = 1
      Top = 1
      Width = 99
      Height = 25
      Align = alLeft
      Caption = '>'
      OnClick = sbAddClick
      ExplicitLeft = 100
    end
    object sbDelete: TSpeedButton
      Left = 829
      Top = 1
      Width = 99
      Height = 25
      Align = alRight
      Caption = '<'
      OnClick = sbDeleteClick
      ExplicitLeft = 281
      ExplicitTop = -1
    end
  end
  object plRight: TPanel [3]
    Left = 0
    Top = 372
    Width = 929
    Height = 232
    Margins.Left = 10
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object plCaption1: TPanel
      Left = 0
      Top = 0
      Width = 929
      Height = 24
      Align = alTop
      BevelOuter = bvNone
      Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1077' '#1074#1072#1075#1086#1085#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object DBGridEh1: TDBGridEh
      Left = 0
      Top = 24
      Width = 929
      Height = 208
      Align = alClient
      AutoFitColWidths = True
      DataSource = dsSelected
      DynProps = <>
      Flat = True
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      IndicatorOptions = [gioShowRowIndicatorEh]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
      STFilter.Local = True
      STFilter.Color = 15269887
      STFilter.HorzLineColor = clTeal
      STFilter.VertLineColor = clTeal
      TabOrder = 1
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -11
      TitleParams.Font.Name = 'Verdana'
      TitleParams.Font.Style = [fsBold]
      TitleParams.ParentFont = False
      OnDblClick = sbAddClick
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'pnum'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 106
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_income'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1087#1088#1080#1073#1099#1090#1080#1103
          Width = 114
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 111
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          Title.Caption = #1058#1080#1087
          Width = 95
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'model_code'
          Footers = <>
          Title.Caption = #1052#1086#1076#1077#1083#1100
          Width = 93
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_length'
          Footers = <>
          Title.Caption = #1044#1083#1080#1085#1072
          Width = 65
        end
        item
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isdefective'
          Footers = <>
          Title.Caption = #1053#1077#1080#1089#1087#1088'?'
          Width = 67
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
          Width = 120
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object drvData: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select c.*, o.start_datetime,  o.id as object_id, '
      
        '(select code from counteragents c1 where c1.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from carriagekinds k where k.id = c.kind_id) as kin' +
        'd_code,'
      
        '(select model_code from carriagemodels m where m.id = c.model_id' +
        ') as model_code,'
      
        '(select car_length from carriagemodels m where m.id = c.model_id' +
        ') as car_length,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code'
      
        'from objects o, carriages c where o.id = c.id and o.object_type ' +
        '= '#39'carriage'#39)
    SelectCommand.Parameters = <>
    UpdateCommand.CommandText.Strings = (
      'update techconditions'
      'set'
      '  code = :code,'
      '  name = :name'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into techconditions'
      '  (code, name)'
      'values'
      '  (:code, :name)')
    InsertCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from techconditions where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from techconditions where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 88
    Top = 184
  end
  object meData: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvData
    Left = 136
    Top = 184
  end
  object dsData: TDataSource
    DataSet = meData
    Left = 192
    Top = 184
  end
  object meSelected: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    Options = [mtoPersistentStructEh]
    Left = 312
    Top = 512
    object MemTableData: TMemTableDataEh
      object DataStruct: TMTDataStructEh
        object id: TMTNumericDataFieldEh
          FieldName = 'id'
          NumericDataType = fdtLargeintEh
          AutoIncrement = False
          DisplayWidth = 20
          currency = False
          Precision = 15
        end
        object cnum: TMTStringDataFieldEh
          FieldName = 'cnum'
          StringDataType = fdtStringEh
          DisplayWidth = 20
        end
        object state_code: TMTStringDataFieldEh
          FieldName = 'state_code'
          StringDataType = fdtStringEh
          DisplayWidth = 50
          Size = 50
        end
      end
      object RecordsList: TRecordsListEh
      end
    end
  end
  object dsSelected: TDataSource
    DataSet = meSelected
    Left = 376
    Top = 512
  end
end
