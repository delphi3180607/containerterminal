﻿unit AlertQue;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  Vcl.ButtonGroup, System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh,
  ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, functions,
  EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.StdCtrls, Vcl.CategoryButtons,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP, IdMessage,
  IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL,
  EXLReportExcelTLB, EXLReportBand, EXLReport, DBCtrlsEh, Vcl.Mask;

type
  TFormAlertQue = class(TFormGrid)
    Bevel1: TBevel;
    sbSend: TPngSpeedButton;
    plSwitch: TPanel;
    sbCurrent: TSpeedButton;
    sbRecent: TSpeedButton;
    IdEMAIL: TIdSMTP;
    IdMessage: TIdMessage;
    IdSSLHandler: TIdSSLIOHandlerSocketOpenSSL;
    Panel6: TPanel;
    dtStart: TDBDateTimeEditEh;
    procedure sbSendClick(Sender: TObject);
    procedure sbCurrentClick(Sender: TObject);
    procedure sbRecentClick(Sender: TObject);
    procedure sbDeleteClick(Sender: TObject);
  private
    { Private declarations }
  public
    n: TFormGrid;
    procedure MessageMail;
    procedure Init; override;
    procedure PartialInit; override;
  end;

var
  FormAlertQue: TFormAlertQue;

implementation

{$R *.dfm}

uses emailaccounts, dmu, log, main;

procedure TFormAlertQue.Init;
begin
  if dtStart.Value = null then dtStart.Value := IncMonth(now(),-2);
  self.drvData.SelectCommand.Parameters.ParamByName('issentcurrent').Value := 0;
  self.drvData.SelectCommand.Parameters.ParamByName('start_date').Value := dtStart.Value;
  inherited;
  self.tablename := 'alertque';
end;

procedure TFormAlertQue.PartialInit;
begin
  meData.Close;
  meData.Open;
end;

procedure TFormAlertQue.sbCurrentClick(Sender: TObject);
begin
  self.drvData.SelectCommand.Parameters.ParamByName('issentcurrent').Value := 0;
  self.meData.Close;
  self.meData.Open;
end;

procedure TFormAlertQue.sbDeleteClick(Sender: TObject);
begin

  if (sbRecent.Down) and (not FormMain.isAdmin) then
  begin
    ShowMessage('Удаление/очистка записей протокола отправки уведомлений допустимо только под правами администратора');
    exit;
  end;

  inherited;
end;

procedure TFormAlertQue.sbRecentClick(Sender: TObject);
begin
  self.drvData.SelectCommand.Parameters.ParamByName('issentcurrent').Value := 1;
  self.meData.Close;
  self.meData.Open;
end;

procedure TFormAlertQue.sbSendClick(Sender: TObject);
var i: integer;
begin
  inherited;

  if sbRecent.Down then
  begin
    ShowBigMessage('ВНИМАНИЕ!!! Вы находитесь в разделе отправленных сообщений!!!!'+chr(10)+' БУДЬТЕ ВНИМАТЕЛЬНЫ!!!');
  end;


  if fQYN('Отправить отмеченные уведомления ?') then
  begin

    if sbRecent.Down then
    if not fQYN('ВНИМАНИЕ!!! ВНИМАНИЕ !!!'+chr(10)+'ВЫ ПЫТАЕТЕСЬ ПОВТОРНО ОТПРАВИТЬ УЖЕ ОТПРАВЛЕННЫЕ СООБЩЕНИЯ!!!'+chr(10)+'ВЫПОЛНЯТЬ??? ВЫ ТОЧНО УВЕРЕНЫ????') then
    begin
      exit;
    end;

    FormLog.meLog.Lines.Clear;

    Application.CreateForm(TFormEmailAccounts, n);
    n.Caption := 'Выберите отправителя';
    n.Init;
    n.Position := poMainFormCenter;
    n.Width := Screen.Width div 2;
    n.Height := Screen.Height div 2;
    n.Parent := nil;
    n.plBottom.Show;
    n.al.Actions[0].Enabled := false;
    n.al.Actions[1].Enabled := false;
    n.al.Actions[2].Enabled := false;
    n.dgData.OnDblClick := nil;
    n.ShowModal;
    if n.ModalResult = mrOk then
    begin

      self.IdEMAIL.Host := n.meData.FieldByName('server_name').AsString;
      self.IdEMAIL.Username := n.meData.FieldByName('email_sender').AsString;
      self.IdEMAIL.Password := n.meData.FieldByName('password').AsString;
      self.IdEMAIL.Port := n.meData.FieldByName('port_number').AsInteger;

      if n.meData.FieldByName('ssl_check').AsBoolean then
      begin

        IdSSLHandler.SSLOptions.Method := sslvTLSv1_2;
        IdSSLHandler.SSLOptions.Mode := sslmUnassigned;
        IdSSLHandler.SSLOptions.VerifyMode := [];
        IdSSLHandler.SSLOptions.VerifyDepth := 0;

        self.IdEMAIL.IOHandler := IdSSLHandler;
        self.IdEMAIL.AuthType := satDefault;
        self.IdEMAIL.UseTLS := utUseExplicitTLS;

      end;

     Screen.Cursor := crHourGlass;
     Application.ProcessMessages;

     try
       self.IdEMAIL.connect;

       if self.dgData.Selection.SelectionType = (gstAll) then
       begin

        self.meData.First;
        while not self.meData.Eof do
        begin
          MessageMail;
          self.meData.Next;
        end;

       end else
       if self.dgData.Selection.SelectionType = (gstRecordBookmarks) then
       begin
        for i := 0 to self.dgData.Selection.Rows.Count-1 do
        begin
          try
            self.meData.Bookmark := self.dgData.Selection.Rows[i];
          except
            //--
          end;
          if self.dgData.SelectedRows.CurrentRowSelected
          then MessageMail;
        end;
       end else
       begin
          MessageMail;
       end;

       self.IdEMAIL.disconnect;
       Screen.Cursor := crDefault;

       FormLog.ShowModal;
       ShowMessage('Готово!');
     finally
       Screen.Cursor := crDefault;
     end;

    end;

  end;

end;


procedure TFormAlertQue.MessageMail;
begin

    if trim(self.meData.FieldByName('email').AsString) <> '' then
    begin

      self.IdMessage.Subject := 'Уведомление Стройоптторг';
      self.IdMessage.Recipients.EMailAddresses := self.meData.FieldByName('email').AsString;
      self.IdMessage.From.Address := n.meData.FieldByName('email_sender').AsString;
      self.IdMessage.ContentType := 'text/plain; charset=windows-1251';
      self.IdMessage.CharSet := 'windows-1251';
      self.IdMessage.IsEncoded := true;
      self.IdMessage.Body.Text := self.meData.FieldByName('message').AsString;

      try
        self.IdEMAIL.Send(self.IdMessage);
        self.meData.Edit;
        self.meData.FieldByName('issent').Value := 1;
        self.meData.Post;
      except
        on E : Exception do
        FormLog.meLog.Lines.Add(E.Message+', '+self.IdEMAIL.LastCmdResult.Code+', '+self.IdEMAIL.LastCmdResult.Text.Text);
      end;

    end else
    begin
      FormLog.meLog.Lines.Add('Не указан email получателя.');
    end;

end;

end.
