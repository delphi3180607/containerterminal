﻿unit dispatch;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, docflow, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, Vcl.ComCtrls, functions, Vcl.Mask, DBCtrlsEh, Vcl.DBCtrls,
  System.ImageList, Vcl.ImgList, PngImageList;

type
  TFormDocDispatch = class(TFormDocFlow)
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    plSpec: TPanel;
    plToolSpec: TPanel;
    sbDeleteSpec: TPngSpeedButton;
    sbEditSpec: TPngSpeedButton;
    dgSpec: TDBGridEh;
    pmSpec: TPopupMenu;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N20: TMenuItem;
    Splitter1: TSplitter;
    sbExcelSpec: TPngSpeedButton;
    N21: TMenuItem;
    N22: TMenuItem;
    sbSumInfo: TPngSpeedButton;
    edExtraServices: TDBEdit;
    N23: TMenuItem;
    N24: TMenuItem;
    procedure dgDataColExit(Sender: TObject);
    procedure dgDataKeyPress(Sender: TObject; var Key: Char);
    procedure sbEditSpecClick(Sender: TObject);
    procedure sbDeleteSpecClick(Sender: TObject);
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure sbConfirmClick(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure meSpecBeforeEdit(DataSet: TDataSet);
    procedure dgSpecDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure meSpecAfterOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure sbCancelConfirmClick(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure meSpecAfterDelete(DataSet: TDataSet);
    procedure meSpecAfterPost(DataSet: TDataSet);
    procedure dgSpecKeyPress(Sender: TObject; var Key: Char);
    procedure dgSpecSelectionChanged(Sender: TObject);
    procedure dgSpecCellClick(Column: TColumnEh);
    procedure sbExcelSpecClick(Sender: TObject);
    procedure dgSpecMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure N22Click(Sender: TObject);
    procedure dgDataSelectionChanged(Sender: TObject);
    procedure dgSpecMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure sbSumInfoClick(Sender: TObject);
    procedure meDataAfterOpen(DataSet: TDataSet);
    procedure N24Click(Sender: TObject);
  private
   //--
  protected
    procedure SetReadOnly; override;
  public
    UnicVals: TStringList;
    procedure Init; override;
    procedure FillSelectListSpec(g: string);
  end;

var
  FormDocDispatch: TFormDocDispatch;

implementation

{$R *.dfm}

uses editdispatch, editdispatchspec, dmu, editmtuandpicture, EditReceipt, main,
  DispatchInfo, ContainersInOrders;


procedure TFormDocDispatch.dgDataColExit(Sender: TObject);
begin
  if meData.State = dsEdit then meData.Post;
end;

procedure TFormDocDispatch.dgDataKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    if meData.State = dsEdit then meData.Post;
end;

procedure TFormDocDispatch.dgDataSelectionChanged(Sender: TObject);
begin
  update_rectangle := false;
end;

procedure TFormDocDispatch.dgSpecCellClick(Column: TColumnEh);
begin
  selfield := Column.FieldName;
  selvalue := meSpec.FieldByName(selfield).Value;
end;


procedure TFormDocDispatch.dgSpecDrawColumnCell(Sender: TObject;  const Rect: TRect; DataCol: Integer; Column: TColumnEh;  State: TGridDrawState);
var i: integer; pnum, pnum1: string;
begin

    pnum := meSpec.FieldByName('pnum').AsString;
    dgSpec.Canvas.Font.Color := clBlack;

    i:= meSpec.RecNo-1;
    i := i-1;

    if i >=0 then
    begin

      pnum1 := meSpec.RecordsView.Rec[i].DataValues['pnum', TDataValueVersionEh.dvvCurValueEh];
      if pnum1 = pnum then
      begin
          if Column.FieldName = 'pnum' then dgSpec.Canvas.Font.Color := clSilver;
      end;

    end;

    dgSpec.DefaultDrawColumnCell(Rect, DataCol, Column, State);

    if gdSelected in State then begin

      dgSpec.Canvas.Pen.Color := clSkyBlue;

      if dgSpec.Selection.SelectionType = (gstRectangle) then
      if (Column.Index = dgSpec.Col-1)then
        dgSpec.Canvas.Pen.Color := clBlue;

      dgSpec.Canvas.Brush.Style := bsClear;
      dgSpec.Canvas.Font.Style := Column.Font.Style;
      dgSpec.Canvas.Pen.Width := 1;
      dgSpec.Canvas.Rectangle(Rect.Left, Rect.Top, Rect.Right, Rect.Bottom);

    end;

    i:= meSpec.RecNo-1;
    i := i+1;

    if (i < meSpec.RecordsView.Count) and (i >= 0 ) then
    begin

      pnum1 := meSpec.RecordsView.Rec[i].DataValues['pnum', TDataValueVersionEh.dvvCurValueEh];
      if pnum1 <> pnum then
      begin
          dgSpec.Canvas.Pen.Width := 1;
          dgSpec.Canvas.Pen.Color := $00D2C38F;
          dgSpec.Canvas.MoveTo(Rect.Left, Rect.Bottom);
          dgSpec.Canvas.LineTo(Rect.Right, Rect.Bottom);
      end;

    end else
    begin
          dgSpec.Canvas.Pen.Width := 1;
          dgSpec.Canvas.Pen.Color := $00D2C38F;
          dgSpec.Canvas.MoveTo(Rect.Left, Rect.Bottom);
          dgSpec.Canvas.LineTo(Rect.Right, Rect.Bottom);
    end;

    dgSpec.Canvas.Pen.Width := 1;
    dgSpec.Canvas.Pen.Color := clSilver;

end;

procedure TFormDocDispatch.dgSpecKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    if meSpec.State in [dsInsert, dsEdit] then meSpec.Post
    else sbEditSpecClick(self);
  end;
end;

procedure TFormDocDispatch.dgSpecMouseDown(Sender: TObject;  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var col: string;
begin

    if Shift<>[ssLeft] then exit;

    col := dgSpec.SelectedField.FieldName;

    selfield := '';
    if (col <> 'dispatch_numbers')
    and (col <> 'receipt_summa')
    and (col <> 'train_num')
    and (col <> 'date_serve')
    then exit;

    selfield := col;

    if selfield<>'' then
    begin
      selvalue := meSpec.FieldByName(selfield).Value;
      seltextvalue := meSpec.FieldByName(col).AsString;
    end;

end;

procedure TFormDocDispatch.dgSpecMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var i: integer;
begin

  if not update_rectangle then exit;

  if dgSpec.Selection.SelectionType = (gstRectangle) then
  begin

    if selfield = '' then exit;
    if not fQYN('Заместить ячейки значением: `'+seltextvalue+'` ?') then exit;

    meData.Bookmark := dgData.Selection.Rect.BottomRow;

    i := 0;

    Screen.Cursor := crHourGlass;

    while true do
    begin

      if self.meSpec.FieldByName(selfield).Value <> selvalue then
      begin
        //try
          Screen.Cursor := crHourGlass;
          self.meSpec.Edit;
          self.meSpec.FieldByName(selfield).Value := selvalue;
          self.meSpec.Post;
        //except
          //--
          //Screen.Cursor := crDefault;
        //end;
      end;

      if meSpec.CompareBookmarks(meSpec.Bookmark , dgSpec.Selection.Rect.TopRow) = 0
      then break;

      meSpec.Prior;
      i := i+1;

    end;

    Screen.Cursor := crDefault;

  end;

  update_rectangle := false;

end;

procedure TFormDocDispatch.dgSpecSelectionChanged(Sender: TObject);
begin

  if dgSpec.Selection.SelectionType = (gstRectangle) then
  begin
    update_rectangle := true;
  end else
  begin
    update_rectangle := false;
  end;

end;

procedure TFormDocDispatch.Init;
begin
  self.system_section := 'outcome_dispatch';
  inherited;
  self.formEdit := FormEditDispatch;
  self.meDataAfterScroll(meData);
  allow_edit := true;
  allowcancel := true;
  update_rectangle := false;
end;

procedure TFormDocDispatch.SetReadOnly;
begin

   inherited;

   meSpec.ReadOnly := false;

  if pos(self.system_section,FormMain.readonly_sections)>0 then
    meSpec.ReadOnly := true;

  if meData.FieldByName('isdeleted').AsBoolean then
    meSpec.ReadOnly := true;

end;



procedure TFormDocDispatch.meDataAfterOpen(DataSet: TDataSet);
begin
  //inherited;

  Screen.Cursor := crDefault;
  edCount.Text := IntToStr(meData.RecordCount);

  meDataAfterScroll(DataSet);
end;

procedure TFormDocDispatch.meDataAfterScroll(DataSet: TDataSet);
begin
  //inherited;
  drvSpec.SelectCommand.Parameters.ParamByName('doc_id').Value := DataSet.FieldByName('id').AsInteger;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormDocDispatch.meSpecAfterDelete(DataSet: TDataSet);
begin
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormDocDispatch.meSpecAfterOpen(DataSet: TDataSet);
var i: integer; pnum: string;
begin

 UnicVals.Clear;

 for i := 0 to meSpec.RecordsView.MemTableData.RecordsList.Count-1 do
 begin
   pnum := meSpec.RecordsView.MemTableData.RecordsList[i].DataValues['pnum', TDataValueVersionEh.dvvCurValueEh];
   UnicVals.Add(pnum);
 end;

 dgSpec.FieldColumns['pnum'].Title.Caption := 'Вaгон/'+ UnicVals.Count.ToString();

end;

procedure TFormDocDispatch.meSpecAfterPost(DataSet: TDataSet);
begin
  drvSpec.GetrecCommand.Parameters.ParamByName('current_id').Value :=
  meSpec.FieldByName('id').AsInteger;
  meSpec.RefreshRecord;
end;

procedure TFormDocDispatch.meSpecBeforeEdit(DataSet: TDataSet);
begin

  inherited;

  if meData.FieldByName('isdeleted').AsBoolean then
    meSpec.ReadOnly := true;

  if pos(self.system_section,FormMain.readonly_sections)>0 then
    meSpec.ReadOnly := true;


end;

procedure TFormDocDispatch.N15Click(Sender: TObject);
var g: string; gu: TGUID;
begin

  FormEditMtuAndPicture.ShowModal;

  if FormEditMtuAndPicture.ModalResult = mrOk then
  begin

    CreateGUID(gu);
    g:= GuidToString(gu);

    FillSelectListSpec(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('update docdispatchspec set mtukind_id = :mtukind_id, picturekind_id = :picturekind_id');
    qrAux.SQL.Add('where id in (select id from selectlist where guid = '''+g+''')');
    qrAux.Parameters.ParamByName('mtukind_id').Value := FormEditMtuAndPicture.luMtuKind.KeyValue;
    qrAux.Parameters.ParamByName('picturekind_id').Value := FormEditMtuAndPicture.luPictureKind.KeyValue;
    qrAux.ExecSQL;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from selectlist where guid = '''+g+'''');
    qrAux.ExecSQL;

    meSpec.Close;
    meSpec.Open;

  end;

end;

procedure TFormDocDispatch.N16Click(Sender: TObject);
var id, i: integer; g: string; guid: TGuid;
begin

  FormEditReceipt.ShowModal;

  if FormEditReceipt.ModalResult = mrOk then
  begin

    CreateGUID(guid);
    g:= GuidToString(guid);

    FillSelectListSpec(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('SET CONTEXT_INFO 0x001;');
    qrAux.SQL.Add('update docdispatchspec set receipt_ready = :receipt_ready,');
    qrAux.SQL.Add('receipt_number = isnull( :receipt_number, receipt_number),');
    qrAux.SQL.Add('receipt_summa = (case when isnull( :receipt_summa, 0) = 0 then receipt_summa else :receipt_summa1 end)');
    qrAux.SQL.Add('where id in (select id from selectlist where guid = '''+g+''')');
    qrAux.Parameters.ParamByName('receipt_ready').Value := FormEditReceipt.cbCheck.Checked;
    qrAux.Parameters.ParamByName('receipt_number').Value := FormEditReceipt.edDocNumber.Text;
    qrAux.Parameters.ParamByName('receipt_summa').Value := FormEditReceipt.neSumma.Value;
    qrAux.Parameters.ParamByName('receipt_summa1').Value := FormEditReceipt.neSumma.Value;
    qrAux.ExecSQL;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from selectlist where guid = '''+g+''';');
    qrAux.SQL.Add('SET CONTEXT_INFO 0x0;');
    qrAux.ExecSQL;

    id := meSpec.FieldByName('id').AsInteger;
    meSpec.Close;
    meSpec.Open;
    meSpec.Locate('id', id, []);

  end;

end;

procedure TFormDocDispatch.N20Click(Sender: TObject);
begin
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormDocDispatch.N22Click(Sender: TObject);
var id: integer;
begin

  if dgSpec.Selection.SelectionType = (gstRectangle) then
  begin

    meSpec.Bookmark := dgSpec.Selection.Rect.TopRow;
    selfield :=  dgSpec.Columns[dgSpec.Col-1].FieldName;
    selvalue := meSpec.FieldByName(selfield).Value;

    if (selfield <> 'dispatch_numbers')
    and (selfield <> 'receipt_summa')
    and (selfield <> 'mtu_code')
    and (selfield <> 'picture_code')
    and (selfield <> 'date_serve')
    then exit;

    if not fQYN('Очистить ячейки?') then exit;

    id := meSpec.FieldByName('id').AsInteger;

    if selfield = 'mtu_code' then selfield := 'mtukind_id';
    if selfield = 'picture_code' then selfield := 'picturekind_id';

    Screen.Cursor := crHourGlass;

    allow_edit := true;

    self.meSpec.DisableControls;

    while true do
    begin

      try
        self.meSpec.Edit;
        self.meSpec.FieldByName(selfield).Value := null;
        self.meSpec.Post;
      except
        self.meSpec.EnableControls;
        Screen.Cursor := crDefault;
      end;

      if meSpec.CompareBookmarks(meSpec.Bookmark , dgSpec.Selection.Rect.BottomRow) = 0
      then break;

      meSpec.Next;

    end;

    allow_edit := false;

    Screen.Cursor := crDefault;
    self.meSpec.EnableControls;

    meSpec.Close;
    meSpec.Open;
    meSpec.Locate('id',id,[]);

  end;

end;

procedure TFormDocDispatch.N24Click(Sender: TObject);
var pv: TPairValues; pp: TPairparam; fGrid: TForm;
var gg: TGuid;
begin

  fGrid := nil;

  pv := SFDV(FormContainersInOrders, 0, 'id', pp, fGrid);

  if pv.keyvalue>0 then
  begin

    if not fQYN('Вы действительно хотите заменить исходную заявку (заявки)? Все параметры отправки будут также изменены!') then
    begin
      exit;
    end;

    CreateGUID(gg);
    g := GuidToString(gg);
    FillSelectListEx(g, dgSpec, 'docload_id');

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('exec dbo.ChangeOrder :guid, :neworder');
    qrAux.Parameters.ParamByName('guid').Value := g;
    qrAux.Parameters.ParamByName('neworder').Value := pv.keyvalue;
    qrAux.ExecSQL;

    ShowMessage('Готово.');

    FullRefresh(meSpec);

    //dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    //dm.spClearSelectList.ExecProc;

  end;

end;

procedure TFormDocDispatch.sbCancelConfirmClick(Sender: TObject);
begin
  inherited;
  meDataAfterScroll(meData);
end;

procedure TFormDocDispatch.sbConfirmClick(Sender: TObject);
begin
  inherited;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormDocDispatch.sbDeleteSpecClick(Sender: TObject);
begin

  if pos(self.system_section,FormMain.readonly_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  if not (meData.FieldByName('isconfirmed').AsInteger = 1) then
  begin
    if fQYN('Внимание! Будет удален весь вагон. Уверены ?') then
    begin
       ED(meSpec);
    end;
  end;
end;

procedure TFormDocDispatch.sbEditSpecClick(Sender: TObject);
begin

  SetReadOnly;

  if meSpec.RecordCount>0 then
  EE(nil, FormEditDispatchSpec, meSpec, 'docdispatchspec')
  else ShowMessage('Нечего редактировать.');
end;


procedure TFormDocDispatch.sbExcelSpecClick(Sender: TObject);
begin
  ExportExcel(dgSpec, 'Спецификация уборки');
end;

procedure TFormDocDispatch.sbSumInfoClick(Sender: TObject);
begin

  qrAux.Close;
  qrAux.SQL.Clear;

  qrAux.SQL.Add('select sum(car_length) as full_length, round(sum(car_length)/14,2) as norm_length, count(*) as cnt from (');
  qrAux.SQL.Add('select distinct cr.id, m.car_length from docdispatchspec sp, carriages cr, carriagemodels m');
  qrAux.SQL.Add('where sp.carriage_id = cr.id and cr.model_id = m.id and sp.doc_id = :docid');
  qrAux.SQL.Add(') s1');

  qrAux.Parameters.ParamByName('docid').Value := meData.FieldByName('id').AsInteger;

  qrAux.Open;
  FormDispatchInfo.dsLocal.DataSet := qrAux;
  FormDispatchInfo.ShowModal;

end;

procedure TFormDocDispatch.FillSelectListSpec(g: string);
var i: integer; e: integer;
begin

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    if self.dgSpec.Selection.SelectionType = (gstAll) then
    begin

      self.meSpec.First;
      while not self.meSpec.Eof do
      begin

        dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := self.meSpec.FieldByName('id').AsInteger;
        dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
        dm.spAdd2SelectList.ExecProc;

        self.meSpec.Next;

      end;

    end else
    if self.dgSpec.Selection.SelectionType = (gstRecordBookmarks) then
    begin

      for i := 0 to self.dgSpec.Selection.Rows.Count-1 do
      begin
        self.meSpec.Bookmark := self.dgSpec.Selection.Rows[i];

        dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := self.meSpec.FieldByName('id').AsInteger;
        dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
        dm.spAdd2SelectList.ExecProc;

      end;

    end else
    begin

      if drvData.GetrecCommand.Parameters.FindParam('current_id') <> nil then
      begin

        dm.spAdd2SelectList.Parameters.ParamByName('@id').Value := self.meSpec.FieldByName('id').AsInteger;
        dm.spAdd2SelectList.Parameters.ParamByName('@guid').Value := g;
        dm.spAdd2SelectList.ExecProc;

      end else
      begin
         //---
      end;
    end;

end;


procedure TFormDocDispatch.FormCreate(Sender: TObject);
begin
  inherited;
 UnicVals := TStringList.Create;
 UnicVals.Sorted := True;
 UnicVals.Duplicates := TDuplicates.dupIgnore;

end;

end.
