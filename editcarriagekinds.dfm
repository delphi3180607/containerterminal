﻿inherited FormEditCarriageKind: TFormEditCarriageKind
  Caption = #1058#1080#1087' '#1074#1072#1075#1086#1085#1072'('#1087#1083#1072#1090#1092#1086#1088#1084#1099')'
  ClientHeight = 265
  ExplicitWidth = 543
  ExplicitHeight = 293
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 15
    Top = 14
    Width = 24
    Height = 16
    Caption = #1050#1086#1076
  end
  object Label3: TLabel [1]
    Left = 15
    Top = 69
    Width = 98
    Height = 16
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  object Label1: TLabel [2]
    Left = 15
    Top = 141
    Width = 84
    Height = 16
    Caption = #1056#1072#1079#1084#1077#1088', '#1092#1091#1090
  end
  inherited plBottom: TPanel
    Top = 224
    TabOrder = 3
    ExplicitTop = 224
  end
  object edCode: TDBEditEh [4]
    Left = 15
    Top = 32
    Width = 305
    Height = 24
    DataField = 'code'
    DataSource = FormCarriageKinds.dsData
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edName: TDBEditEh [5]
    Left = 15
    Top = 91
    Width = 505
    Height = 24
    DataField = 'name'
    DataSource = FormCarriageKinds.dsData
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object neFootSize: TDBNumberEditEh [6]
    Left = 15
    Top = 163
    Width = 121
    Height = 24
    DataField = 'footsize'
    DataSource = FormCarriageKinds.dsData
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
end
