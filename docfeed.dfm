﻿inherited FormDocFeed: TFormDocFeed
  ActiveControl = dgData
  Caption = #1055#1086#1076#1072#1095#1072'/'#1056#1072#1079#1075#1088#1091#1079#1082#1072
  ClientHeight = 627
  ClientWidth = 916
  ExplicitWidth = 932
  ExplicitHeight = 665
  PixelsPerInch = 96
  TextHeight = 13
  inherited SplitterVert: TSplitter
    Height = 594
    ExplicitHeight = 594
  end
  inherited plBottom: TPanel
    Top = 594
    Width = 916
    ExplicitTop = 594
    ExplicitWidth = 916
    inherited btnOk: TButton
      Left = 681
      ExplicitLeft = 681
    end
    inherited btnCancel: TButton
      Left = 800
      ExplicitLeft = 800
    end
  end
  inherited plAll: TPanel
    Width = 716
    Height = 594
    ExplicitWidth = 716
    ExplicitHeight = 594
    inherited SplitterHor: TSplitter
      Top = 402
      Width = 716
      ExplicitLeft = 6
      ExplicitTop = 415
      ExplicitWidth = 715
    end
    inherited plTop: TPanel
      Width = 716
      ExplicitWidth = 716
      inherited sbDelete: TPngSpeedButton
        Left = 99
        ExplicitLeft = 118
      end
      inherited btFilter: TPngSpeedButton
        Left = 200
        ExplicitLeft = 245
      end
      inherited btExcel: TPngSpeedButton
        Left = 637
        ExplicitLeft = 680
        ExplicitTop = 4
      end
      inherited sbAdd: TPngSpeedButton
        Left = 67
        Visible = False
        ExplicitLeft = 87
      end
      inherited sbEdit: TPngSpeedButton
        Left = 163
        ExplicitLeft = 153
      end
      inherited Bevel2: TBevel
        Left = 197
        ExplicitLeft = 205
      end
      inherited btTool: TPngSpeedButton
        Left = 518
        ExplicitLeft = 809
      end
      inherited Bevel1: TBevel
        Left = 601
        Margins.Right = 3
        ExplicitLeft = 504
        ExplicitTop = -3
        ExplicitHeight = 43
      end
      inherited btFlow: TPngSpeedButton
        Left = 402
        Width = 38
        Enabled = False
        ExplicitLeft = 430
        ExplicitWidth = 38
      end
      inherited sbHistory: TPngSpeedButton
        Left = 440
        Width = 42
        ExplicitLeft = 382
        ExplicitWidth = 42
        ExplicitHeight = 37
      end
      inherited sbRestrictions: TPngSpeedButton
        Left = 530
        ExplicitLeft = 476
        ExplicitTop = -1
      end
      inherited sbConfirm: TPngSpeedButton
        Left = 301
        ExplicitLeft = 318
      end
      inherited sbCancelConfirm: TPngSpeedButton
        Left = 333
        ExplicitLeft = 260
        ExplicitTop = -3
      end
      inherited Bevel5: TBevel
        Left = 484
        ExplicitLeft = 489
        ExplicitTop = -1
        ExplicitHeight = 22
      end
      inherited sbReport: TPngSpeedButton
        Left = 605
        ExplicitLeft = 547
        ExplicitTop = 6
      end
      inherited Bevel6: TBevel
        Left = 399
        ExplicitLeft = 385
        ExplicitTop = 1
        ExplicitHeight = 29
      end
      inherited sbImport2: TPngSpeedButton
        Visible = False
        OnClick = sbImport2Click
      end
      inherited sbSearch: TPngSpeedButton
        Left = 264
        ExplicitLeft = 235
        ExplicitTop = 2
        ExplicitHeight = 29
      end
      inherited Bevel7: TBevel
        Left = 298
        ExplicitLeft = 239
        ExplicitTop = -3
      end
      inherited sbClearFilter: TPngSpeedButton
        Left = 232
        ExplicitLeft = 262
        ExplicitTop = -2
      end
      inherited Bevel3: TBevel
        Left = 564
        ExplicitLeft = 585
      end
      inherited sbClock: TPngSpeedButton
        Left = 567
        ExplicitLeft = 589
      end
      inherited sbBarrel: TPngSpeedButton
        Left = 365
        Enabled = False
        Visible = False
        ExplicitLeft = 412
      end
      object sbCheck: TPngSpeedButton [23]
        AlignWithMargins = True
        Left = 487
        Top = 0
        Width = 39
        Height = 26
        Hint = #1055#1088#1086#1074#1077#1088#1082#1072'/'#1074#1099#1075#1088#1091#1079#1082#1072
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 4
        Margins.Bottom = 0
        Align = alLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        Layout = blGlyphRight
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbCheckClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C0864880000001974455874536F6674776172
          65007777772E696E6B73636170652E6F72679BEE3C1A0000023C4944415478DA
          63FCFFFF3F03258091140318656AD8809431109FF8FFA4E53F49060035F36E5E
          14F7C8C95A4960DEF25357726BB6E9030DF9073700A840425B4D2CFDEAAD57D3
          8112AFD035AF9D1BF5C0CE524908A4FA1F90A86EDB7662EED273366003800A64
          964D0FBFA5A420C479EBCEEBEF7139CB55FF3FED7C0AD5CC37A73FE88EB999A2
          E87F46460606262686C5275F30FC07D2F3AAE6D6303248574B77D4B8DD909215
          E0F9F7F73F032B0B13C3AB979FBF15D66C36656064FED459E771DAC84C49E21F
          50030B0B33C3B66BEF191E7CFDCFA0C6F6FBFF84C2CA4246559BBEE91F59FF65
          F8DAAB31C84BF133B0B2B1317070B231B0313331802CE4E2E76160626561F8C7
          C4CC70F6D93786E73F1819C4BE7FFEBFBCBD63CAFB876B5A187955AB2C553465
          0E5F7AF696D9544F9EC1485B8A8197978B818D838D819D8395811968E0D3CFBF
          199E006D65E5E264607FF5EAFFA609BD933F3D59DF0AF4FE2B7018B04BA5F82B
          E8E9AF79CFCAC2C2C3C7C32026C6C72028C8C3C0CAC9C1F09B850548733270F2
          7032FCBEFFF4FF8E697D13BEBFD8DC01D28C128D5C32A9017AF656AB3FF2F0B0
          70F2723370F1703170F282347231700169E6272FFE6F9EDC3DE1F3934D70CD18
          E98057213DD031C863F57B3E41664E3E2EB866FEF7EFFEAFEAEA98F8FEE1BA36
          A0FAD77853A2B05A76507C7EF4EA7BAC7C4C2043D4597EFF9F51593BE9E59DD5
          2D40B56F884ACA327A8521858DA92B8161C0D859503DF9D9CD15CDD834E34DCA
          8C02AEBA0CBFDF68337CBBB00797669233134D0C00005658FCA41106A6E80000
          000049454E44AE426082}
        ExplicitLeft = 476
        ExplicitTop = -1
        ExplicitHeight = 38
      end
      object sbFilterPlan: TPngSpeedButton [24]
        AlignWithMargins = True
        Left = 33
        Top = 0
        Width = 33
        Height = 26
        Hint = #1054#1090#1073#1086#1088' '#1087#1086' '#1087#1083#1072#1085#1091' '#1087#1086#1075#1088#1091#1079#1082#1080
        Margins.Left = 1
        Margins.Top = 0
        Margins.Right = 1
        Margins.Bottom = 0
        Align = alLeft
        AllowAllUp = True
        GroupIndex = 1
        ParentShowHint = False
        ShowHint = True
        OnClick = sbFilterPlanClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C08648800000009704859730000004E000000
          4E01B1CD1F330000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000001FA4944415478DA636440020D0C0D4C
          B7CDDFF22C3D39F91303918011C6C8B5CD155510FEB74E41EC9FD2F5278C6B6A
          B64DCB27DA8042CB642105498E1D0196BF4D999919186E3D657ABFE71C4B64DB
          FE293B091A90669CC6A529C7BA3FC8EAB71948330CAC3BCA72FACA833FD6B3CE
          CEFA8DD7801A97BCDC28879F93F8B8FEA3487CFECEF86FF5619609F7DFFE9FC0
          C1CCA4C3CAC4F178D2B19E2B1806E4D995AA1ACB7FDDE968F04791891155F2E6
          63A637DFFF30328AF1FF17FEFC8DE1F3E5074C979FBC6049EF3936096E10584B
          A955AEB298F0FF690E7A7F6D2585FF72E272EEBD678C1FF65F646EA9DF3DAD17
          231640A0CE25375349F26F9EA3DE1F0DE4F0006B7EC1F4F9D065E69E9AED539B
          B046230C94DBE7E4C6B9FC9EC4CF8D08937BCF993EED3DC73CA571EFD46A9CE9
          0006DA7DB3E7C73AFE4E80F11FBC64FAF4418CFBEBED5BFF2E7F9CC9EF094C6C
          FFF01AD0E19B332FC6F157224CF3EE6BEC17420AB80C844499B816F57C9B5AB9
          A8BF00AF01558E7961C1363F177FFFC5F863CF4596FE861D531AEAC28A52CD9D
          58AA24E598F8372FF9915FBF6CC2629C0680C45A3C73B6FEF9CF700AA4196EB0
          7BB691A201FB6C7E616691F3FB7F78B66F9F7C0D97013841927A19AFA6CDEF15
          BF7EFE57B97189C568F1A59EAF24190073614348C1C4EF5FFF6B756E9FE8428E
          016050E19117F8FECBFF7B006B30C9507591C2870000000049454E44AE426082}
      end
      inherited sbPass: TPngSpeedButton
        Left = 131
        ExplicitLeft = 131
      end
      inherited plCount: TPanel
        Left = 555
        ExplicitLeft = 555
        inherited edCount: TDBEditEh
          ControlLabel.ExplicitLeft = 0
          ControlLabel.ExplicitTop = -16
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 716
      Height = 317
      DataGrouping.Active = True
      DataGrouping.GroupLevels = <
        item
          ColumnName = 'Column_2_plan_train_num'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end>
      OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDblClickOptimizeColWidth, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      ReadOnly = False
      TitleParams.MultiTitle = True
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 61
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num'
          Footers = <>
          Title.Caption = #1050#1055
          Width = 82
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'plan_train_num'
          Footers = <>
          Title.Caption = #1055#1083#1072#1085
          Visible = False
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'prefix'
          Footers = <>
          MaxWidth = 60
          Title.Caption = #1055#1088#1077#1092#1080#1082#1089
          Visible = False
          Width = 60
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doctype'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1058#1080#1087' '#1076#1086#1082'.'
          Visible = False
          Width = 57
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 70
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DisplayFormat = 'dd.mm.yyyy'
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 78
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'base_doc'
          Footers = <>
          Title.Caption = #1046#1044#1053
          Width = 85
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_modified'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
          Visible = False
          Width = 79
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'docexists'
          Footers = <>
          ReadOnly = True
          TextEditing = True
          Title.Caption = #1057#1086#1079#1076'. '#1076#1086#1082'.'
          Width = 57
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'check_text'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Footers = <>
          ReadOnly = True
          Title.Caption = #1056#1077#1079'- '#1090#1072#1090
          Width = 37
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_shift'
          Footers = <>
          ReadOnly = True
          TextEditing = True
          Title.Caption = #1057#1084'. '#1094'.'#1090'.'
          Width = 39
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'front'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsItalic]
          Footers = <>
          Title.Caption = #1060#1088#1086#1085#1090
          Width = 75
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_sender'
          Footers = <>
          Title.Caption = #1042#1077#1089', '#1086#1090#1087#1088'., '#1090#1086#1085#1085
          Width = 47
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_doc'
          Footers = <>
          Title.Caption = #1042#1077#1089' '#1046#1044#1053', '#1082#1075'.'
          Width = 47
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Color = 2866676
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_fact'
          Footers = <>
          Title.Caption = #1042#1077#1089' '#1092#1072#1082#1090', '#1082#1075'.'
          Width = 45
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'carriage_num'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1053#1086#1084#1077#1088' '#1074#1072#1075#1086#1085#1072
          Width = 86
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'container_num'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
          Width = 91
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'container_foot'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1058#1080#1087
          Width = 42
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Color = 2866676
          DynProps = <>
          EditButtons = <>
          FieldName = 'container_owner'
          Footers = <>
          Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
          Width = 96
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dlvtype'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
          Width = 81
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_created'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1089#1086#1079#1076'.'
          Width = 81
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'user_created'
          Footers = <>
          Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
          Width = 126
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'task_id'
          Footers = <>
        end>
    end
    inherited plHint: TPanel
      TabOrder = 6
    end
    inherited plStatus: TPanel
      Width = 716
      ExplicitWidth = 716
    end
    inherited plLinkedObjects: TPanel
      Top = 412
      Width = 710
      ExplicitTop = 412
      ExplicitWidth = 710
      inherited dgLinkedObjects: TDBGridEh
        Width = 710
        TitleParams.MultiTitle = True
      end
    end
    inherited plSearch: TPanel
      Width = 716
      ExplicitWidth = 716
      inherited edSearch: TEdit
        Width = 534
        Color = 8188154
        ExplicitWidth = 534
      end
    end
    object plNotes: TPanel
      Left = 0
      Top = 380
      Width = 716
      Height = 22
      Align = alBottom
      Constraints.MinHeight = 22
      TabOrder = 5
      object edNote: TDBEdit
        AlignWithMargins = True
        Left = 1
        Top = 1
        Width = 408
        Height = 20
        Margins.Left = 0
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alLeft
        BorderStyle = bsNone
        Constraints.MinHeight = 20
        DataField = 'cargo_description'
        DataSource = dsData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        StyleElements = [seClient, seBorder]
      end
      object edUnloadFullText: TDBEdit
        AlignWithMargins = True
        Left = 412
        Top = 1
        Width = 300
        Height = 20
        Margins.Left = 0
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alClient
        BorderStyle = bsNone
        DataField = 'check_fulltext'
        DataSource = dsData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        StyleElements = [seClient, seBorder]
      end
    end
  end
  inherited plLeft: TPanel
    Height = 594
    ExplicitHeight = 594
    inherited Bevel4: TBevel
      Height = 569
      ExplicitHeight = 448
    end
    inherited dgFolders: TDBGridEh
      Height = 569
    end
  end
  inherited pmGrid: TPopupMenu
    Left = 344
    Top = 200
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'declare @userid int, @planid int, @doctypeid int, @datebegin dat' +
        'etime, @dateend datetime, @isconfirmed int;'
      ''
      'select @userid = :userid, @planid = :planid;'
      ''
      
        'select @doctypeid = min(id) from doctypes where system_section =' +
        ' '#39'income_unload'#39';'
      ''
      
        'select @datebegin = datebegin, @dateend  = dateend, @isconfirmed' +
        '  = isconfirmed '
      'from dbo.f_GetFilterParams(@userid,@doctypeid);'
      ''
      'select '
      
        'd.id, d.doc_number, d.doc_date, d.folder_id, d.doctype_id, s.pre' +
        'fix,'
      
        '(select d1.doc_number+'#39' , '#39'+convert(varchar, d1.doc_date, 104) f' +
        'rom documents d1, doccargosheet sh where d1.id = sh.id and sh.un' +
        'load_doc_id = s.id) as base_doc, '
      's.load_plan_id, '
      
        '(select code from doctypes t2 where t2.id = d.doctype_id) as doc' +
        'type,'
      
        '(case when isnull(d.isconfirmed,0) = 1 then '#39' +'#39' else '#39#39' end) as' +
        ' state_code,'
      
        '(select count(*) where exists (select 1 from f_GetLinkedDocument' +
        's(d.id))) as docexists,'
      
        '(select note+'#39' '#1086#1090' '#39'+convert(varchar, l.plan_date, 104) from load' +
        'plan l where l.id = s.load_plan_id) as plan_train_num,'
      's.train_num,'
      's.container_num,'
      's.container_foot, '
      
        '(select code from counteragents c where c.id = s.container_owner' +
        '_id) as container_owner,'
      
        '(select plan_date from loadplan l where l.id = s.load_plan_id) a' +
        's income_plan_date, '
      
        '(select code from unloadfronts fr where fr.id = s.unload_front_i' +
        'd) as front,'
      's.carriage_num,'
      's.carriage_type,'
      
        '(select code from counteragents c where c.id = s.carriage_owner_' +
        'id) as carriage_owner,'
      's.seal_number,'
      's.weight_sender,'
      's.weight_doc,'
      's.weight_fact,'
      's.container_owner_id,'
      's.carriage_owner_id,'
      's.cargo_description,'
      'd.date_created,'
      'd.date_modified,'
      'd.isconfirmed,'
      'd.date_confirm, '
      'd.isdeleted,'
      'd.isabolished,'
      's.check_result,'
      
        '(case when s.check_result = 1 then '#39'+'#39' when s.check_result = 2 t' +
        'hen '#39'~'#39' when s.check_result>2 then '#39'?'#39' else '#39' '#39' end) as check_te' +
        'xt,'
      
        'dbo.unloadtext(s.check_result, s.check_datetime)+'#39', '#39'+check_note' +
        ' as check_fulltext,'
      's.check_datetime,'
      's.check_note,'
      'isnull(s.weight_shift,0) as weight_shift,'
      's.unload_front_id,'
      's.task_id,'
      
        '(select code from deliverytypes t where t.id = s.dlv_type_id) as' +
        ' dlvtype,'
      
        '(select user_name from users u where u.id = d.user_created) as u' +
        'ser_created'
      'from docfeed s, documents d '
      'where '
      'd.id = s.id and (1 = :showall or d.folder_id = :folder_id)'
      'and'
      '('
      
        '  not exists (select 1 from dbo.f_GetFilterList(@userid, @doctyp' +
        'eid) fl1)'
      
        '  or exists (select 1 from dbo.f_GetFilterList(@userid, @doctype' +
        'id) fl where charindex(fl.v, s.container_num)>0) '
      
        '  or exists (select 1 from dbo.f_GetFilterList(@userid, @doctype' +
        'id) fl where charindex(fl.v, s.carriage_num)>0) '
      ')'
      'and'
      '('
      '  ('
      '    (@datebegin is null or  d.doc_date >=@datebegin)'
      '    and (@dateend is null or  d.doc_date <@dateend+1)'
      
        '    and (isnull(@isconfirmed,0)=0 or d.isconfirmed = isnull(@isc' +
        'onfirmed,0))'
      '  )  or isnull(@planid,0) <> 0'
      ')'
      'and (isnull(@planid,0) = 0 or s.load_plan_id  = @planid)'
      'order by id')
    SelectCommand.Parameters = <
      item
        Name = 'userid'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'planid'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'showall'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'folder_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update docfeed'
      'set'
      '  train_num = :train_num,'
      '  container_num = :container_num,'
      '  container_foot = :container_foot,'
      '  container_owner_id = :container_owner_id,'
      '  carriage_num = :carriage_num,'
      '  carriage_type = :carriage_type,'
      '  carriage_owner_id = :carriage_owner_id,'
      '  cargo_description = :cargo_description,'
      '  seal_number = :seal_number,'
      '  weight_sender = :weight_sender,'
      '  weight_doc = :weight_doc,'
      '  weight_fact = :weight_fact,'
      '  check_result = :check_result,'
      '  check_datetime = :check_datetime,'
      '  check_note = :check_note,'
      '  weight_shift = :weight_shift,'
      '  unload_front_id  = :unload_front_id'
      'where'
      '  id = :id;'
      ''
      
        'update  documents set  date_modified = getdate(), doc_number = :' +
        'doc_number, doc_date = :doc_date where id = :id;'
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'train_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_foot'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'carriage_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'carriage_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'carriage_owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cargo_description'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2000
        Value = Null
      end
      item
        Name = 'seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'weight_sender'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_doc'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_fact'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'check_result'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'check_datetime'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'check_note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'weight_shift'
        Size = -1
        Value = Null
      end
      item
        Name = 'unload_front_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'declare @newid int, @doctypeid int;'
      ''
      
        'select @doctypeid = id from doctypes where system_section='#39'incom' +
        'e_sheet'#39';'
      ''
      
        'insert into documents (doctype_id, folder_id, doc_number,  doc_d' +
        'ate) values (@doctypeid, :folder_id, :doc_number,  :doc_date);'
      ''
      'select @newid = IDENT_CURRENT('#39'documents'#39');'
      ''
      ''
      'insert into docfeed'
      
        '  (id, train_num, container_num, container_foot, container_owner' +
        '_id, '
      
        '   carriage_num, carriage_type, carriage_owner_id, cargo_descrip' +
        'tion, '
      
        '   seal_number, weight_sender, weight_doc, weight_fact, unload_f' +
        'ront_id'
      '   )'
      'values'
      
        '  (@newid, :train_num, :container_num, :container_foot, :contain' +
        'er_owner_id, '
      
        '   :carriage_num, :carriage_type, :carriage_owner_id, :cargo_des' +
        'cription, '
      
        '   :seal_number, :weight_sender, :weight_doc, :weight_fact, :unl' +
        'oad_front_id'
      ');')
    InsertCommand.Parameters = <
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'train_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_foot'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'carriage_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'carriage_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'carriage_owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cargo_description'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2000
        Value = Null
      end
      item
        Name = 'seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'weight_sender'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_doc'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_fact'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'unload_front_id'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      
        'delete from documents where  id = :id and isnull(isconfirmed,0)=' +
        '0')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select '
      
        'd.id, d.doc_number, d.doc_date, d.folder_id, d.doctype_id, s.pre' +
        'fix,'
      
        '(select d1.doc_number+'#39' , '#39'+convert(varchar, d1.doc_date, 104) f' +
        'rom documents d1, doccargosheet sh where d1.id = sh.id and sh.un' +
        'load_doc_id = s.id) as base_doc, '
      's.load_plan_id, '
      
        '(select code from doctypes t2 where t2.id = d.doctype_id) as doc' +
        'type,'
      
        '(case when isnull(d.isconfirmed,0) = 1 then '#39' +'#39' else '#39#39' end) as' +
        ' state_code,'
      
        '(select count(*) where exists (select 1 from f_GetLinkedDocument' +
        's(d.id))) as docexists,'
      
        '(select note+'#39' '#1086#1090' '#39'+convert(varchar, l.plan_date, 104) from load' +
        'plan l where l.id = s.load_plan_id) as plan_train_num,'
      's.train_num,'
      's.container_num,'
      's.container_foot, '
      
        '(select code from counteragents c where c.id = s.container_owner' +
        '_id) as container_owner,'
      
        '(select plan_date from loadplan l where l.id = s.load_plan_id) a' +
        's income_plan_date, '
      
        '(select code from unloadfronts fr where fr.id = s.unload_front_i' +
        'd) as front,'
      's.carriage_num,'
      's.carriage_type,'
      
        '(select code from counteragents c where c.id = s.carriage_owner_' +
        'id) as carriage_owner,'
      's.seal_number,'
      's.weight_sender,'
      's.weight_doc,'
      's.weight_fact,'
      's.container_owner_id,'
      's.carriage_owner_id,'
      's.cargo_description,'
      'd.date_created,'
      'd.date_modified,'
      'd.isconfirmed,'
      'd.date_confirm, '
      'd.isdeleted,'
      'd.isabolished,'
      's.check_result,'
      
        '(case when s.check_result = 1 then '#39'+'#39' when s.check_result = 2 t' +
        'hen '#39'-'#39' when s.check_result>2 then '#39'?'#39' else '#39' '#39' end) as check_te' +
        'xt,'
      
        'dbo.unloadtext(s.check_result, s.check_datetime)+'#39', '#39'+check_note' +
        ' as check_fulltext,'
      's.check_datetime,'
      's.check_note,'
      'isnull(s.weight_shift,0) as weight_shift,'
      's.unload_front_id,'
      's.task_id,'
      
        '(select code from deliverytypes t where t.id = s.dlv_type_id) as' +
        ' dlvtype,'
      
        '(select user_name from users u where u.id = d.user_created) as u' +
        'ser_created'
      'from docfeed s, documents d '
      'where '
      'd.id = s.id and d.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 224
    Top = 280
  end
  inherited meData: TMemTableEh
    AfterOpen = meDataAfterOpen
    Left = 272
    Top = 280
  end
  inherited dsData: TDataSource
    Left = 320
    Top = 280
  end
  inherited qrAux: TADOQuery
    Left = 288
  end
  inherited meForms: TMemTableEh
    Left = 588
  end
  inherited dsForms: TDataSource
    Left = 636
  end
  inherited pmDocFlow: TPopupMenu
    Left = 224
    Top = 200
    object N17: TMenuItem [3]
      Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      ShortCut = 16465
    end
    object N21: TMenuItem [4]
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072'/'#1074#1099#1075#1088#1091#1079#1082#1072
      ShortCut = 32856
      OnClick = sbCheckClick
    end
    inherited N8: TMenuItem
      Enabled = False
    end
    object N20: TMenuItem [11]
      Caption = #1057#1074#1077#1088#1085#1091#1090#1100' '#1074#1089#1077
      ShortCut = 16471
      OnClick = N20Click
    end
    object N22: TMenuItem [15]
      Caption = #1054#1090#1073#1086#1088' '#1087#1086' '#1087#1088#1080#1084#1077#1095#1072#1085#1080#1102
      ShortCut = 32838
      OnClick = N22Click
    end
    object N15: TMenuItem
      Caption = '-'
      Visible = False
    end
    object N16: TMenuItem
      Caption = #1052#1072#1089#1089#1086#1074#1086#1077' '#1079#1072#1087#1086#1083#1085#1077#1085#1080#1077' '#1076#1072#1085#1085#1099#1093
      Visible = False
      OnClick = N16Click
    end
  end
  inherited al: TActionList
    Left = 408
    inherited aAdd: TAction
      Enabled = False
    end
  end
  inherited drvLinkedObjects: TADODataDriverEh
    Top = 482
  end
  inherited meLinkedObjects: TMemTableEh
    Top = 482
  end
  inherited dsLinkedObjects: TDataSource
    Top = 482
  end
  inherited pmLinkedObjects: TPopupMenu
    Top = 480
  end
  inherited qrReport: TADOQuery
    Left = 704
  end
  object meMass: TMemTableEh [24]
    Params = <>
    Left = 702
    Top = 256
  end
  object Timer1: TTimer [25]
    Enabled = False
    Interval = 100000
    OnTimer = Timer1Timer
    Left = 557
    Top = 280
  end
  inherited mePass: TMemTableEh
    BeforeOpen = nil
    AfterOpen = nil
    AfterInsert = nil
    BeforeEdit = nil
    AfterEdit = nil
    BeforePost = nil
    AfterPost = nil
    AfterScroll = nil
    Left = 440
  end
  inherited IL: TPngImageList
    Bitmap = {}
  end
end
