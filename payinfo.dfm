﻿inherited FormPayInfo: TFormPayInfo
  Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086#1073' '#1086#1087#1083#1072#1090#1077
  ClientHeight = 224
  ClientWidth = 457
  Font.Height = -12
  OnShow = FormShow
  ExplicitWidth = 463
  ExplicitHeight = 252
  PixelsPerInch = 96
  TextHeight = 14
  inherited plBottom: TPanel
    Top = 183
    Width = 457
    TabOrder = 4
    ExplicitTop = 183
    ExplicitWidth = 457
    inherited btnCancel: TButton
      Left = 341
      ExplicitLeft = 341
    end
    inherited btnOk: TButton
      Left = 222
      ExplicitLeft = 222
    end
  end
  object edPaidSumm: TDBNumberEditEh [1]
    Left = 19
    Top = 31
    Width = 145
    Height = 22
    ControlLabel.Width = 91
    ControlLabel.Height = 14
    ControlLabel.Caption = #1057#1091#1084#1084#1072' '#1086#1087#1083#1072#1090#1099
    ControlLabel.Visible = True
    DataField = 'paidsumm'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object cbPaidFull: TDBCheckBoxEh [2]
    Left = 19
    Top = 76
    Width = 219
    Height = 18
    Caption = #1054#1087#1083#1072#1095#1077#1085#1086' '#1087#1086#1083#1085#1086#1089#1090#1100#1102
    DataField = 'paidfull'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 1
  end
  object dtPaidFullDate: TDBDateTimeEditEh [3]
    Left = 211
    Top = 76
    Width = 143
    Height = 22
    ControlLabel.Width = 129
    ControlLabel.Height = 14
    ControlLabel.Caption = #1047#1072#1088#1077#1075#1080#1089#1090#1088#1080#1088#1086#1074#1072#1085#1085#1086
    ControlLabel.Visible = True
    DataField = 'paiddate_register'
    DataSource = dsLocal
    DynProps = <>
    Enabled = False
    EditButtons = <>
    Kind = dtkDateTimeEh
    ReadOnly = True
    TabOrder = 2
    Visible = True
  end
  object edPayDocs: TDBEditEh [4]
    Left = 19
    Top = 137
    Width = 420
    Height = 22
    ControlLabel.Width = 399
    ControlLabel.Height = 14
    ControlLabel.Caption = #1058#1080#1087', '#1085#1086#1084#1077#1088' '#1076#1072#1090#1072' '#1087#1086#1076#1090#1074#1077#1088#1078#1076#1072#1102#1097#1077#1075#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' ('#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074')'
    ControlLabel.Visible = True
    DataField = 'paydocs'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object dtConfirmed: TDBDateTimeEditEh [5]
    Left = 211
    Top = 31
    Width = 145
    Height = 22
    ControlLabel.Width = 72
    ControlLabel.Height = 14
    ControlLabel.Caption = #1055#1088#1086#1074#1077#1076#1077#1085#1086
    ControlLabel.Visible = True
    DataField = 'date_confirm'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    ReadOnly = True
    TabOrder = 5
    Visible = False
  end
  inherited dsLocal: TDataSource
    Left = 56
    Top = 184
  end
  inherited qrAux: TADOQuery
    Left = 104
    Top = 184
  end
end
