﻿unit editdeliverytype;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Vcl.ExtCtrls, Data.Win.ADODB, DBSQLLookUp;

type
  TFormEditDeliveryType = class(TFormEdit)
    edName: TDBEditEh;
    edCode: TDBEditEh;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    edGlobalSection: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDeliveryType: TFormEditDeliveryType;

implementation

{$R *.dfm}

end.
