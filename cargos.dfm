﻿inherited FormCargos: TFormCargos
  ActiveControl = dgData
  Caption = #1043#1088#1091#1079#1099
  PixelsPerInch = 96
  TextHeight = 13
  inherited Splitter2: TSplitter
    Left = 313
    ExplicitLeft = 513
  end
  inherited plAll: TPanel
    Width = 313
    ExplicitWidth = 313
    inherited Splitter1: TSplitter
      Top = 249
      Width = 313
      ExplicitTop = 249
      ExplicitWidth = 513
    end
    inherited plTop: TPanel
      Width = 313
      Caption = #1055#1054#1048#1057#1050' '#1058#1054#1051#1068#1050#1054' '#1055#1054' '#1058#1054#1063#1053#1054#1052#1059' '#1053#1054#1052#1045#1056#1059
      ExplicitWidth = 313
      inherited btTool: TPngSpeedButton
        Left = 275
        ExplicitLeft = 461
      end
      inherited sbClearFilter: TPngSpeedButton
        ExplicitLeft = 175
      end
      inherited plCount: TPanel
        Left = 78
        ExplicitLeft = 78
      end
    end
    inherited dgData: TDBGridEh
      Width = 313
      Height = 203
      Font.Height = -12
      Font.Name = 'Calibri'
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      ReadOnly = False
      RowHeight = 18
      STFilter.Local = False
      STFilter.Visible = False
      TitleParams.MultiTitle = True
      OnEnter = dgDataEnter
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'start_datetime'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1091#1087#1083#1077#1085#1080#1103
          Width = 128
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnum'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 143
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'type_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1058#1080#1087' '#1075#1088#1091#1079#1072
          Width = 125
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'seal_number'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
          Width = 115
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = False
          DynProps = <>
          EditButtons = <>
          FieldName = 'issealwaste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          Footers = <>
          PickList.Strings = (
            '+'
            '-'
            '?')
          Title.Caption = #1043#1086#1076'- '#1085#1072#1103'?'
          Width = 48
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_doc'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1042#1077#1089' '#1046#1044#1053
          Width = 83
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_fact'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1042#1077#1089', '#1092#1072#1082#1090
          Width = 72
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 108
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'real_state_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 107
        end>
    end
    inherited plHint: TPanel
      TabOrder = 5
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    inherited plObjects: TPanel
      Width = 313
      Visible = False
      ExplicitWidth = 313
      inherited Panel2: TPanel
        Width = 313
        Visible = False
        ExplicitWidth = 313
      end
      inherited dgObjects: TDBGridEh
        Width = 313
        Font.Height = -11
        TitleParams.Font.Height = -11
        Visible = False
      end
    end
    inherited plStatus: TPanel
      Width = 313
      ParentColor = False
      TabOrder = 4
      ExplicitWidth = 313
    end
    object Panel4: TPanel
      Left = 0
      Top = 255
      Width = 313
      Height = 180
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object dgTasks: TDBGridEh
        Left = 0
        Top = 0
        Width = 313
        Height = 180
        Align = alClient
        BorderStyle = bsNone
        DataSource = dsTasks
        DynProps = <>
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        GridLineParams.DataHorzColor = clSilver
        GridLineParams.DataVertColor = clSilver
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
        ParentFont = False
        ReadOnly = True
        SelectionDrawParams.DrawFocusFrame = True
        SelectionDrawParams.DrawFocusFrameStored = True
        TabOrder = 0
        TitleParams.FillStyle = cfstGradientEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -11
        TitleParams.Font.Name = 'Verdana'
        TitleParams.Font.Style = [fsBold]
        TitleParams.HorzLineColor = clSilver
        TitleParams.MultiTitle = True
        TitleParams.ParentFont = False
        TitleParams.VertLineColor = clSilver
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'start_date'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
            Width = 102
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'dlv_code'
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
            Width = 125
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
            Width = 131
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'forwarder_code'
            Footers = <>
            Title.Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
            Width = 127
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'consignee_code'
            Footers = <>
            Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
            Width = 128
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'payer_code'
            Footers = <>
            Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
            Width = 128
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited Panel1: TPanel
    Left = 320
    Width = 580
    ExplicitLeft = 320
    ExplicitWidth = 580
    inherited Splitter3: TSplitter
      Width = 580
      ExplicitWidth = 368
    end
    inherited dgStates: TDBGridEh
      Width = 580
      Font.Height = -11
      TitleParams.Font.Height = -11
    end
    inherited Panel3: TPanel
      Width = 580
      ExplicitWidth = 580
      inherited DBGridEh1: TDBGridEh
        Width = 580
        Font.Height = -11
        TitleParams.Font.Height = -11
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'declare @guid varchar(40);'
      ''
      'select @guid = :guid;'
      ''
      
        'select c.*, o.start_datetime,  o.id as object_id, c.weight_doc, ' +
        'c.weight_fact, c.isempty, c.seal_number,'
      
        '(select code from counteragents c1 where c1.id = t.forwarder_id)' +
        ' as forwarder_code,'
      
        '(select code from counteragents c2 where c2.id = t.consignee_id)' +
        ' as consignee_code,'
      
        '(case when  c.isempty=1 then '#39'{'#1087#1086#1088#1086#1078#1085#1080#1081'}'#39' else  (select code fro' +
        'm cargotypes t where t.id = c.type_id) end) as type_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select code from objectstatekinds s where s.id = o.real_state_i' +
        'd) as real_state_code'
      
        'from cargos c, objects o left outer join tasks t on (t.id = o.cu' +
        'rrent_task_id)'
      'where o.id = c.id and o.object_type = '#39'cargo'#39
      
        '--and (dbo.filter_objects(@guid, c.cnum) = 1 or dbo.filter_objec' +
        'ts(@guid, c.seal_number) = 1)'
      'and'
      '('
      '  not exists (select 1 from dbo.f_GetFilterList(0, @guid) fl1)'
      
        '  or exists (select 1 from dbo.f_GetFilterList(0, @guid) fl wher' +
        'e fl.v = c.cnum) '
      
        '  or exists (select 1 from dbo.f_GetFilterList(0, @guid) fl wher' +
        'e fl.v = c.seal_number) '
      ')'
      'order by o.start_datetime desc')
    SelectCommand.Parameters = <
      item
        Name = 'guid'
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update cargos'
      'set'
      '  cnum = :cnum,'
      '  type_id = :type_id,'
      '  isempty = :isempty,'
      '  note = :note,'
      '  weight_sender = :weight_sender, '
      '  weight_doc = :weight_doc, '
      '  weight_fact = :weight_fact,'
      '  seal_number = :seal_number,'
      '  issealwaste = :issealwaste'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'cnum'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'weight_sender'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_doc'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_fact'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'issealwaste'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'begin'
      'declare @newid int;'
      'exec InsertObject '#39'cargo'#39', @newid output;'
      'insert into cargos'
      
        '  (id, cnum, type_id, note, weight_sender, weight_doc, weight_fa' +
        'ct, seal_number,  isempty, issealwaste)'
      'values'
      
        '  (@newid,  :cnum, :type_id, :note, :weight_sender, :weight_doc,' +
        ' :weight_fact, :seal_number, :isempty, :issealwaste);'
      'end;')
    InsertCommand.Parameters = <
      item
        Name = 'cnum'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'weight_sender'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_doc'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_fact'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'issealwaste'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from objects where  id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select c.*, o.start_datetime,  o.id as object_id, c.weight_doc, ' +
        'c.weight_fact, c.isempty, c.seal_number,'
      
        '(select code from counteragents c1 where c1.id = t.forwarder_id)' +
        ' as forwarder_code,'
      
        '(select code from counteragents c2 where c2.id = t.consignee_id)' +
        ' as consignee_code,'
      
        '(case when  c.isempty=1 then '#39'{'#1087#1086#1088#1086#1078#1085#1080#1081'}'#39' else  (select code fro' +
        'm cargotypes t where t.id = c.type_id) end) as type_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select code from objectstatekinds s where s.id = o.real_state_i' +
        'd) as real_state_code'
      
        'from cargos c, objects o left outer join tasks t on (t.id = o.cu' +
        'rrent_task_id)'
      'where o.id = c.id and o.object_type = '#39'cargo'#39
      'and c.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited meData: TMemTableEh
    FetchAllOnOpen = False
  end
  inherited al: TActionList
    Left = 280
  end
  inherited meStates: TMemTableEh
    DetailFields = 'object_id'
    MasterFields = 'object_id'
  end
  inherited drvLinkedObjects: TADODataDriverEh
    UpdateCommand.CommandText.Strings = ()
  end
  object drvTasks: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select '
      't.id, '
      'c.cnum,'
      't.dlv_type_id,'
      
        '(select d.code from deliverytypes d where d.id = t.dlv_type_id) ' +
        'as dlv_code, '
      't.start_date,'
      't.plan_date,'
      't.fact_date'#9','
      't.forwarder_id,'
      
        '(select code from counteragents c1 where c1.id = t.forwarder_id)' +
        ' as forwarder_code, '
      't.consignee_id,'
      
        '(select code from counteragents c2 where c2.id = t.consignee_id)' +
        ' as consignee_code, '
      't.payer_id,'
      
        '(select code from counteragents c3 where c3.id = t.payer_id) as ' +
        'payer_code, '
      't.object_id'
      'from tasks t, objects o '
      'left outer join containers c on (c.id = o.linked_object_id)'
      'where t.object_id = :object_id and t.object_id = o.id'
      'order by t.start_date desc')
    SelectCommand.Parameters = <
      item
        Name = 'object_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.Parameters = <
      item
        Name = 'owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'date_change'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.Parameters = <
      item
        Name = 'object_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'date_change'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 136
    Top = 328
  end
  object meTasks: TMemTableEh
    DetailFields = 'object_id'
    MasterFields = 'id'
    MasterSource = dsData
    Params = <>
    DataDriver = drvTasks
    Left = 184
    Top = 328
  end
  object dsTasks: TDataSource
    DataSet = meTasks
    Left = 240
    Top = 328
  end
end
