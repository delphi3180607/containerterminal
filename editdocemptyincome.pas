﻿unit EditDocEmptyIncome;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, DBSQLLookUp, DBGridEh,
  Vcl.Buttons, DBLookupEh, functions;

type
  TFormEditDocEmptyIncome = class(TFormEdit)
    Label14: TLabel;
    dtIncome: TDBDateTimeEditEh;
    edDocNumber: TDBEditEh;
    Label1: TLabel;
    dtDocDate: TDBDateTimeEditEh;
    Label3: TLabel;
    edCarData: TDBEditEh;
    Label18: TLabel;
    edDriver: TDBEditEh;
    Label4: TLabel;
    Label8: TLabel;
    sbForwarder: TSpeedButton;
    edAttorney: TDBEditEh;
    Label5: TLabel;
    edNote: TDBEditEh;
    Label6: TLabel;
    laDealer: TDBSQLLookUp;
    ssDlvTypes: TADOLookUpSqlSet;
    laDlvTypes: TDBSQLLookUp;
    Label16: TLabel;
    Label2: TLabel;
    ssPersons: TADOLookUpSqlSet;
    laPersons: TDBSQLLookUp;
    procedure sbForwarderClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDocEmptyIncome: TFormEditDocEmptyIncome;

implementation

{$R *.dfm}

uses counteragents, persons;

procedure TFormEditDocEmptyIncome.btnOkClick(Sender: TObject);
begin
  inherited;
  if (laDlvTypes.KeyValue = -1) or (laDlvTypes.KeyValue = null)  then
  begin
    ShowMessage('Необходимо заполнить Тип доставки.');
    self.ModalResult := mrNone;
  end;
end;

procedure TFormEditDocEmptyIncome.FormShow(Sender: TObject);
begin
  inherited;

  if dsLocal.DataSet.FieldByName('isbased').AsBoolean
  then
    self.laDlvTypes.Enabled := false
   else
    self.laDlvTypes.Enabled := true;

end;

procedure TFormEditDocEmptyIncome.sbForwarderClick(Sender: TObject);
begin
  SFDE(TFormCounteragents, laDealer);
end;

end.
