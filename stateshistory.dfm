﻿inherited FormStatesHistory: TFormStatesHistory
  Caption = #1048#1089#1090#1086#1088#1080#1103' '#1086#1087#1077#1088#1072#1094#1080#1081
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'id'
          Footers = <>
          Title.Caption = #1055#1086#1088#1103#1076#1082#1086#1074#1099#1081' '#1085#1086#1084#1077#1088
          Width = 142
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'operation_datetime'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103
          Width = 136
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'k_code'
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1086#1087#1077#1088#1072#1094#1080#1080
          Width = 177
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 's_code'
          Footers = <>
          Title.Caption = #1050#1086#1085#1077#1095#1085#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 183
        end
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
          Width = 182
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'select o.*, k.statekind as state_id, k.code as k_code, s.code as' +
        ' s_code '
      'from operations2maindata o2m, operations o'
      'left outer join operationkinds k on (k.id = o.operationkind_id)'
      'left outer join statekinds s on (s.id = k.statekind)'
      'where o2m.operation_id = o.id and o2m.maindata_id = :parent_id'
      'order by operation_datetime')
    SelectCommand.Parameters = <
      item
        Name = 'parent_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update operations set'
      'operationkind_id =:operationkind_id,'
      'operation_datetime = :operation_datetime,'
      'previous_id = :previous_id,'
      'state_id = :state_id,'
      'doc_base_id = :doc_base_id,'
      'doc_invoice_id = :doc_invoice_id,'
      'event_type = :event_type,'
      'event_date = :event_date,'
      'service_id = :service_id,'
      'person_id = :person_id,'
      'note = :note'
      'where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'operationkind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'operation_datetime'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'previous_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_base_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_invoice_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'event_type'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'event_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'service_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'person_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2000
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into operations ('
      'operationkind_id,'
      'operation_datetime,'
      'previous_id,'
      'state_id,'
      'doc_base_id,'
      'doc_invoice_id,'
      'event_type,'
      'event_date,'
      'service_id,'
      'person_id,'
      'note'
      ') '
      'values'
      '('
      ':operationkind_id,'
      ':operation_datetime,'
      ':previous_id,'
      ':state_id,'
      ':doc_base_id,'
      ':doc_invoice_id,'
      ':event_type,'
      ':event_date,'
      ':service_id,'
      ':person_id,'
      ':note'
      ')')
    InsertCommand.Parameters = <
      item
        Name = 'operationkind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'operation_datetime'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'previous_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_base_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_invoice_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'event_type'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'event_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'service_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'person_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2000
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from operations where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select o.*, k.statekind as state_id, k.code as k_code, s.code as' +
        ' s_code from operations o'
      'left outer join operationkinds k on (k.id = o.operationkind_id)'
      'left outer join statekinds s on (s.id = k.statekind)'
      'where o.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
end
