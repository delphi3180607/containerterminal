﻿unit editoperation;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Vcl.ExtCtrls, DBSQLLookUp;

type
  TFormEditOperationKind = class(TFormEdit)
    Label2: TLabel;
    Label3: TLabel;
    edCode: TDBEditEh;
    edName: TDBEditEh;
    DBSQLLookUp1: TDBSQLLookUp;
    Label1: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditOperationKind: TFormEditOperationKind;

implementation

{$R *.dfm}

end.
