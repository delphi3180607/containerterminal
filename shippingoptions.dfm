﻿inherited FormShippingOptions: TFormShippingOptions
  Caption = #1042#1072#1088#1080#1072#1085#1090#1099' '#1076#1086#1089#1090#1072#1074#1082#1080
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076
          Width = 207
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 383
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from shippingoptions order by code')
    UpdateCommand.CommandText.Strings = (
      'update shippingoptions'
      'set'
      '  code = :code,'
      '  name = :name'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'code'
        Size = -1
        Value = Null
      end
      item
        Name = 'name'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into shippingoptions'
      '  (code, name)'
      'values'
      '  (:code, :name)')
    InsertCommand.Parameters = <
      item
        Name = 'code'
        Size = -1
        Value = Null
      end
      item
        Name = 'name'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from shippingoptions where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from shippingoptions where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
end
