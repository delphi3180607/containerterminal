﻿object FormTool: TFormTool
  Left = 0
  Top = 0
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
  ClientHeight = 614
  ClientWidth = 972
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 16
  object plBottom: TPanel
    Left = 0
    Top = 573
    Width = 972
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Button1: TButton
      AlignWithMargins = True
      Left = 856
      Top = 3
      Width = 113
      Height = 35
      Align = alRight
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 0
    end
    object btSave: TButton
      AlignWithMargins = True
      Left = 737
      Top = 3
      Width = 113
      Height = 35
      Align = alRight
      Caption = #1047#1072#1087#1080#1089#1072#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 1
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 44
    Width = 972
    Height = 529
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsUnderline]
    ParentFont = False
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Select'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object seSelect: TSynMemo
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 958
        Height = 492
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        TabOrder = 0
        CodeFolding.GutterShapeSize = 11
        CodeFolding.CollapsedLineColor = clGrayText
        CodeFolding.FolderBarLinesColor = clGrayText
        CodeFolding.IndentGuidesColor = clGray
        CodeFolding.IndentGuides = True
        CodeFolding.ShowCollapsedLine = False
        CodeFolding.ShowHintMark = True
        UseCodeFolding = False
        Gutter.Font.Charset = DEFAULT_CHARSET
        Gutter.Font.Color = clWindowText
        Gutter.Font.Height = -11
        Gutter.Font.Name = 'Courier New'
        Gutter.Font.Style = []
        Lines.Strings = (
          'seSelect')
        FontSmoothing = fsmNone
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Insert'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object seInsert: TSynMemo
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 958
        Height = 492
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        TabOrder = 0
        CodeFolding.GutterShapeSize = 11
        CodeFolding.CollapsedLineColor = clGrayText
        CodeFolding.FolderBarLinesColor = clGrayText
        CodeFolding.IndentGuidesColor = clGray
        CodeFolding.IndentGuides = True
        CodeFolding.ShowCollapsedLine = False
        CodeFolding.ShowHintMark = True
        UseCodeFolding = False
        Gutter.Font.Charset = DEFAULT_CHARSET
        Gutter.Font.Color = clWindowText
        Gutter.Font.Height = -11
        Gutter.Font.Name = 'Courier New'
        Gutter.Font.Style = []
        Lines.Strings = (
          'seSQL')
        FontSmoothing = fsmNone
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Update'
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object seUpdate: TSynMemo
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 958
        Height = 492
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        TabOrder = 0
        CodeFolding.GutterShapeSize = 11
        CodeFolding.CollapsedLineColor = clGrayText
        CodeFolding.FolderBarLinesColor = clGrayText
        CodeFolding.IndentGuidesColor = clGray
        CodeFolding.IndentGuides = True
        CodeFolding.ShowCollapsedLine = False
        CodeFolding.ShowHintMark = True
        UseCodeFolding = False
        Gutter.Font.Charset = DEFAULT_CHARSET
        Gutter.Font.Color = clWindowText
        Gutter.Font.Height = -11
        Gutter.Font.Name = 'Courier New'
        Gutter.Font.Style = []
        Lines.Strings = (
          'seSQL')
        FontSmoothing = fsmNone
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'Delete'
      ImageIndex = 6
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object seDelete: TSynMemo
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 958
        Height = 492
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        TabOrder = 0
        CodeFolding.GutterShapeSize = 11
        CodeFolding.CollapsedLineColor = clGrayText
        CodeFolding.FolderBarLinesColor = clGrayText
        CodeFolding.IndentGuidesColor = clGray
        CodeFolding.IndentGuides = True
        CodeFolding.ShowCollapsedLine = False
        CodeFolding.ShowHintMark = True
        UseCodeFolding = False
        Gutter.Font.Charset = DEFAULT_CHARSET
        Gutter.Font.Color = clWindowText
        Gutter.Font.Height = -11
        Gutter.Font.Name = 'Courier New'
        Gutter.Font.Style = []
        Lines.Strings = (
          'seSelect')
        FontSmoothing = fsmNone
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1050#1086#1083#1086#1085#1082#1080
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object dgColumns: TDBGridEh
        AlignWithMargins = True
        Left = 3
        Top = 52
        Width = 958
        Height = 443
        Align = alClient
        DataSource = dcColumns
        DynProps = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        TitleParams.MultiTitle = True
        OnDblClick = sbEditClick
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            Footers = <>
            Title.Caption = #1043#1088#1091#1087#1087#1072
            Width = 115
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'field_name'
            Footers = <>
            Title.Caption = #1048#1084#1103' '#1087#1086#1083#1103
            Width = 134
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'column_title'
            Footers = <>
            Title.Caption = #1047#1072#1075#1086#1083#1086#1074#1086#1082
            Width = 212
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            Footers = <>
            Title.Caption = #1050#1088#1072#1090#1082#1086
            Width = 152
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'show_in_grid'
            Footers = <>
            Title.Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1074' '#1089#1087#1080#1089#1082#1077
            Width = 101
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            Footers = <>
            Title.Caption = #1055#1088#1072#1074#1080#1090#1100' '#1074' '#1092#1086#1088#1084#1077
            Width = 111
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'order_num'
            Footers = <>
            Title.Caption = #1055#1086#1088#1103#1076#1086#1082
            Width = 90
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 964
        Height = 49
        Align = alTop
        TabOrder = 1
        object sbDelete: TPngSpeedButton
          AlignWithMargins = True
          Left = 34
          Top = 4
          Width = 32
          Height = 41
          Hint = #1059#1076#1072#1083#1080#1090#1100
          Align = alLeft
          Flat = True
          OnClick = sbDeleteClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
            0D0000000473424954080808087C086488000000097048597300000B1200000B
            1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000003804944415478DACD944F6C54451CC7
            BF33F3DEDB7DDBA5348B85A5DD5AFB0FDBAE848558A38D012F280703C40BE1A2
            3151C28518AA41A4468B89D58493A40749058D87CA81C6A01863A307046CB445
            AB586C698A6D714B6DD7EED276DFEEFB3333CE839274578F983893F726BF99DF
            7CF2FBFDE63B43A494B8978DFC67C0AEDADA86CDBB769EAAA88EC5FA4F7EF8D1
            1F63D78F72D7052780498180F271D4C785F47FD0198D94D7D6764637343C79FD
            527FCF8BE9F4EB05C08BAFB48D6CD9B1FD41E3D65F485EBCE05EE83D776AFCC6
            CDFD94518456006D9723C0D8A38D894D1FAC8DC737DC9C9C9A1CFBBEFFB5B69C
            7DA60038D87E78FAA1CDF1F5C18539606A1CCED5217C7D7EE8B781B97C6B8946
            323E3027048DAE2A3D934824766728B586AF5C39E8E6ED6ED7B5F172CE2E4CF9
            7845054C5D3BF8F0C6FAF6A6B0B786252760CD66F0C34C3E75D9124D26416393
            AE9FADACAC8C0C5BB981D1E4F4E3A6613825A5A5B09616FE097CBFBE0ED65C0A
            9A6323A2E3F8C630DB7F9FE0BA630B8CE6F892C96186358D0D0AF171DAE3CF11
            42A11B06CC92927F079E68A8879D4E23E8B9C82E65A1CEA22AAAD3CF1BA5D8C4
            3CA9D295B8C4D8B145C843868241D9011F180EC35ABC85B66260D703F7233B3F
            0FEA79D03C0E8B2054C5E5405CF06628179D11FCC8E8E0A8202DBABF41CD6994
            C0D095A582386C3B85C08EA0A1D4A0E440084C89AA06217EAAE362CD82B2AF32
            FA45A381A7AA4354EB5D44D78C230F68F40E54AA4E547F9BBB85C037033A841A
            35605DC2E3233542967115D5778C9E9E05D9AB4BECDABD4EEB0D9B8C9E18B713
            6AD72F7485A03B8B811D0AC801DACCC564B310315D27F899D0895F25A909A9F5
            BC5A5492DCB3AF2E787A68DE9B383FE7D40454CA77DB5BAA5405C0A3868E72C1
            7B5AA5DC6B06186628F1BE7110574BD7343F3B81DB07130BD2779F8E065EFD74
            3ABF2FE3CA6EB6CC7CA318D8A1D1B5DB2265C92A3BAB714DE05C16EFA43C1CD1
            88EFA442C71D3FA5223CB25A1B5D4D49F9B71927E2D7DC6FEDC5C0F7CA4A5FD8
            B9FD89EE55532398FF3329FB526E3525E486EFEE5FDF94C3C197DF112651B735
            AC0D8F587C8F2D71D6F779C92DAA6167C8DCF1FC91B62FD7F30CAEF5F5F1CF2E
            FF1E550F40EA6E8D2C5740C9D1D7E7EDB19C91633182B48AABD39F7B265F043C
            A442DFB2ADB5FFB1AD2D2D5F9DFCA4676C7AF6597D19E67B988C4157872057CC
            8509A82F71DF3E601701EFF97BF8BF05FE0DBECDBAE86B24A614000000004945
            4E44AE426082}
          ExplicitLeft = 76
        end
        object sbAdd: TPngSpeedButton
          AlignWithMargins = True
          Left = 2
          Top = 2
          Width = 28
          Height = 43
          Hint = #1044#1086#1073#1072#1074#1080#1090#1100
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbAddClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C0864880000027E4944415478DA75934F6813
            5110C6E765379B6C9B5453A2A68A076911F1107B502C8855C845A820251EBC89
            083D14D4A207A18A9E7A1005BD2854144FB607050551C4D47AA8ED6A6D7B6804
            D14BC13F4D9A9AEC66B3D924BBD9B7CE8BCD33ADEDC0B061BE995FDEFB6697C0
            DF0861466083E8ECEC944686878E6CAD7CD9EB48AD9AB1A5FB697B7BC70CD3C8
            4ACFC19CAA7E208400CF9A4AC0711C9724EF817FF60ED17D5190B23F80CADB68
            FE40FFB55DDDA786EA802E55D51449F282AAAA50AD56F9BFFB53634E68EAA240
            2D027ACF13F04F3F049A1C073BB4DB4D1DBF7D940332CBCB8AECF7432E9703DB
            B66B4504396D1F073C56F627A158124EDC073AFD184A9F2740344C300EF73DE0
            8042A1A00882004B4B4B1C804F7BC7D8192F3D390AC1E0262072108B1524DB60
            BEBC0BBF34F305071886A1783C1ED0348D5F01E79DA6F1EB44FEFADA03147DE9
            7B0EE4ED30946713406D01BEF75CBDF11F607171112CCBE21E54D2DFAC9DAF2E
            49A4688078EE19B86F103093007D4F2C3FB13DB68F038AC5A2C2DCC7AB30E739
            C0E7F3D15472CA0ABC7FE4F5E9BF05C715A1D07188BEAB848FF59FBF90E08065
            34D18F269AA6B90A500FDC4EC52C95DC70382CB6452202F6B66259E3001C54D8
            8F743ACD4D5C2F229108C8B20C922485D6053013D79E80794329ADE5CAB0BB19
            0325FD1FA05452C0756B1EB0C6C6E1402000994C060D4E4173731364B3D95BB1
            58EC32CA94034A087011A0EBFAAA133000F326994C42341A85B9B9B9D1DEDEDE
            D3F97CDE6EFC16BA908AAFB20468E6AA358AA2082D2D2DB0B0B000E572796470
            70F0ECE4E464B9AED701FB51FCC44EC056D918EC3AE88F3B3F3F7F331E8F5FC1
            6D541BF57AB70F53868D839952C074D70A7F002AF04A8E29B489580000000049
            454E44AE426082}
        end
        object sbEdit: TPngSpeedButton
          AlignWithMargins = True
          Left = 70
          Top = 2
          Width = 26
          Height = 43
          Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbEditClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D4948445200000014000000140803000000BA57ED
            3F0000000373424954080808DBE14FE00000012F504C5445FFFFFFFEFEFEFDFD
            FDFCFCFCFBFBFBFAFAFAF9F9F9F8F8F8F7F7F7F6F6F6F5F5F5FFF3E5FDF7C7F4
            F4F4F3F3F3FDF6C1FFF2E0FDF6C3F2F2F2F1F1F1F0F0F0EFEFEFF6EEE4F5EBDF
            F1E9E0F2E9DEEAEAEAE9E9E9F3E6D8F5E5D4F9E3B1F2E5BCF3E4AEE3E3E3EEE2
            D4FCE0C3F0E3B9E2E2E2DFDFDFF8E25CF7E25AF7DF62F7E159F5DF5AF6DD6BEE
            D8B0F5DC5DE8D9A7F5DC5EF6D3ADF0D2B3F2D679EFD2ABE4D5ABE5D5B1E5D4AE
            EED680EED47CF1D47AEED378F0D378F9C990EDD077F9C793CCCCCCCBCBCBEDCA
            6CE7CB76C9C9C9E4C954D7C2A4E7BF8AC2C2C2E7BD76C9BDAFD7B975D1B68FD5
            B579DBB084CBA76ED8A457D7A262E2A150D4A16ADD9C50D29E65D89953DD994A
            D09C45DC9748CC9558C49748C78D47BD894BC98440A98842BD7919AD6A258466
            2D88602A5942173C25EF5A000000097048597300000B1200000B1201D2DD7EFC
            0000001C74455874536F6674776172650041646F62652046697265776F726B73
            2043533571B5E336000000D54944415478DA6364C0021871092ACAB340798FEE
            FC83093A1CFFFB1F2CC6E2780F2C0A127439C0F99781819381E38591F43FAEAB
            972182FB5981241B1068BEBCA9A9B60C2AC80C24993965399F69BEFA64B0162A
            08365056F486B4183FCBC1574882323217557E70CBFFD8FE1F2168C27352E98F
            80EC31F5BD084163D12352FF44650FBC7244083AB3FFFFF94C58ECF0873F4882
            311BDDB87E331E78CD802C187F97419EF9E06320C71E2198F8FFD7A7936F408E
            7040B2080E1CF740FCEE78F42F5C8CD9EA0044505586192EF8E7E96D3C818C01
            004FE4561533982BD20000000049454E44AE426082}
          ExplicitLeft = 60
          ExplicitTop = 0
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1057#1074#1103#1079#1080' '#1090#1072#1073#1083#1080#1094
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object dgTableLinks: TDBGridEh
        AlignWithMargins = True
        Left = 3
        Top = 52
        Width = 958
        Height = 443
        Align = alClient
        DataSource = dsLinks
        DynProps = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        TitleParams.MultiTitle = True
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            Footers = <>
            Title.Caption = #1048#1084#1103' '#1087#1086#1083#1103
            Width = 134
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            Footers = <>
            Title.Caption = #1058#1072#1073#1083#1080#1094#1072' '#1093#1088#1072#1085#1077#1085#1080#1103
            Width = 164
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            Footers = <>
            Title.Caption = #1058#1072#1073#1083#1080#1094#1072' '#1080#1089#1090#1086#1095#1085#1080#1082
            Width = 160
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            Footers = <>
            Title.Caption = #1056#1086#1076#1080#1090#1077#1083#1100#1089#1082#1072#1103' '#1090#1072#1073#1083#1080#1094#1072
            Width = 167
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            Footers = <>
            Title.Caption = #1055#1086#1088#1086#1078#1076#1072#1090#1100' '#1079#1072#1087#1080#1089#1100'?'
            Width = 139
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 964
        Height = 49
        Align = alTop
        TabOrder = 1
        object PngSpeedButton1: TPngSpeedButton
          AlignWithMargins = True
          Left = 34
          Top = 4
          Width = 32
          Height = 41
          Hint = #1059#1076#1072#1083#1080#1090#1100
          Align = alLeft
          Flat = True
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
            0D0000000473424954080808087C086488000000097048597300000B1200000B
            1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000003804944415478DACD944F6C54451CC7
            BF33F3DEDB7DDBA5348B85A5DD5AFB0FDBAE848558A38D012F280703C40BE1A2
            3151C28518AA41A4468B89D58493A40749058D87CA81C6A01863A307046CB445
            AB586C698A6D714B6DD7EED276DFEEFB3333CE839274578F983893F726BF99DF
            7CF2FBFDE63B43A494B8978DFC67C0AEDADA86CDBB769EAAA88EC5FA4F7EF8D1
            1F63D78F72D7052780498180F271D4C785F47FD0198D94D7D6764637343C79FD
            527FCF8BE9F4EB05C08BAFB48D6CD9B1FD41E3D65F485EBCE05EE83D776AFCC6
            CDFD94518456006D9723C0D8A38D894D1FAC8DC737DC9C9C9A1CFBBEFFB5B69C
            7DA60038D87E78FAA1CDF1F5C18539606A1CCED5217C7D7EE8B781B97C6B8946
            323E3027048DAE2A3D934824766728B586AF5C39E8E6ED6ED7B5F172CE2E4CF9
            7845054C5D3BF8F0C6FAF6A6B0B786252760CD66F0C34C3E75D9124D26416393
            AE9FADACAC8C0C5BB981D1E4F4E3A6613825A5A5B09616FE097CBFBE0ED65C0A
            9A6323A2E3F8C630DB7F9FE0BA630B8CE6F892C96186358D0D0AF171DAE3CF11
            42A11B06CC92927F079E68A8879D4E23E8B9C82E65A1CEA22AAAD3CF1BA5D8C4
            3CA9D295B8C4D8B145C843868241D9011F180EC35ABC85B66260D703F7233B3F
            0FEA79D03C0E8B2054C5E5405CF06628179D11FCC8E8E0A8202DBABF41CD6994
            C0D095A582386C3B85C08EA0A1D4A0E440084C89AA06217EAAE362CD82B2AF32
            FA45A381A7AA4354EB5D44D78C230F68F40E54AA4E547F9BBB85C037033A841A
            35605DC2E3233542967115D5778C9E9E05D9AB4BECDABD4EEB0D9B8C9E18B713
            6AD72F7485A03B8B811D0AC801DACCC564B310315D27F899D0895F25A909A9F5
            BC5A5492DCB3AF2E787A68DE9B383FE7D40454CA77DB5BAA5405C0A3868E72C1
            7B5AA5DC6B06186628F1BE7110574BD7343F3B81DB07130BD2779F8E065EFD74
            3ABF2FE3CA6EB6CC7CA318D8A1D1B5DB2265C92A3BAB714DE05C16EFA43C1CD1
            88EFA442C71D3FA5223CB25A1B5D4D49F9B71927E2D7DC6FEDC5C0F7CA4A5FD8
            B9FD89EE55532398FF3329FB526E3525E486EFEE5FDF94C3C197DF112651B735
            AC0D8F587C8F2D71D6F779C92DAA6167C8DCF1FC91B62FD7F30CAEF5F5F1CF2E
            FF1E550F40EA6E8D2C5740C9D1D7E7EDB19C91633182B48AABD39F7B265F043C
            A442DFB2ADB5FFB1AD2D2D5F9DFCA4676C7AF6597D19E67B988C4157872057CC
            8509A82F71DF3E601701EFF97BF8BF05FE0DBECDBAE86B24A614000000004945
            4E44AE426082}
          ExplicitLeft = 76
        end
        object PngSpeedButton2: TPngSpeedButton
          AlignWithMargins = True
          Left = 2
          Top = 2
          Width = 28
          Height = 43
          Hint = #1044#1086#1073#1072#1074#1080#1090#1100
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C0864880000027E4944415478DA75934F6813
            5110C6E765379B6C9B5453A2A68A076911F1107B502C8855C845A820251EBC89
            083D14D4A207A18A9E7A1005BD2854144FB607050551C4D47AA8ED6A6D7B6804
            D14BC13F4D9A9AEC66B3D924BBD9B7CE8BCD33ADEDC0B061BE995FDEFB6697C0
            DF0861466083E8ECEC944686878E6CAD7CD9EB48AD9AB1A5FB697B7BC70CD3C8
            4ACFC19CAA7E208400CF9A4AC0711C9724EF817FF60ED17D5190B23F80CADB68
            FE40FFB55DDDA786EA802E55D51449F282AAAA50AD56F9BFFB53634E68EAA240
            2D027ACF13F04F3F049A1C073BB4DB4D1DBF7D940332CBCB8AECF7432E9703DB
            B66B4504396D1F073C56F627A158124EDC073AFD184A9F2740344C300EF73DE0
            8042A1A00882004B4B4B1C804F7BC7D8192F3D390AC1E0262072108B1524DB60
            BEBC0BBF34F305071886A1783C1ED0348D5F01E79DA6F1EB44FEFADA03147DE9
            7B0EE4ED30946713406D01BEF75CBDF11F607171112CCBE21E54D2DFAC9DAF2E
            49A4688078EE19B86F103093007D4F2C3FB13DB68F038AC5A2C2DCC7AB30E739
            C0E7F3D15472CA0ABC7FE4F5E9BF05C715A1D07188BEAB848FF59FBF90E08065
            34D18F269AA6B90A500FDC4EC52C95DC70382CB6452202F6B66259E3001C54D8
            8F743ACD4D5C2F229108C8B20C922485D6053013D79E80794329ADE5CAB0BB19
            0325FD1FA05452C0756B1EB0C6C6E1402000994C060D4E4173731364B3D95BB1
            58EC32CA94034A087011A0EBFAAA133000F326994C42341A85B9B9B9D1DEDEDE
            D3F97CDE6EFC16BA908AAFB20468E6AA358AA2082D2D2DB0B0B000E572796470
            70F0ECE4E464B9AED701FB51FCC44EC056D918EC3AE88F3B3F3F7F331E8F5FC1
            6D541BF57AB70F53868D839952C074D70A7F002AF04A8E29B489580000000049
            454E44AE426082}
        end
        object PngSpeedButton3: TPngSpeedButton
          AlignWithMargins = True
          Left = 70
          Top = 2
          Width = 26
          Height = 43
          Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          PngImage.Data = {
            89504E470D0A1A0A0000000D4948445200000014000000140803000000BA57ED
            3F0000000373424954080808DBE14FE00000012F504C5445FFFFFFFEFEFEFDFD
            FDFCFCFCFBFBFBFAFAFAF9F9F9F8F8F8F7F7F7F6F6F6F5F5F5FFF3E5FDF7C7F4
            F4F4F3F3F3FDF6C1FFF2E0FDF6C3F2F2F2F1F1F1F0F0F0EFEFEFF6EEE4F5EBDF
            F1E9E0F2E9DEEAEAEAE9E9E9F3E6D8F5E5D4F9E3B1F2E5BCF3E4AEE3E3E3EEE2
            D4FCE0C3F0E3B9E2E2E2DFDFDFF8E25CF7E25AF7DF62F7E159F5DF5AF6DD6BEE
            D8B0F5DC5DE8D9A7F5DC5EF6D3ADF0D2B3F2D679EFD2ABE4D5ABE5D5B1E5D4AE
            EED680EED47CF1D47AEED378F0D378F9C990EDD077F9C793CCCCCCCBCBCBEDCA
            6CE7CB76C9C9C9E4C954D7C2A4E7BF8AC2C2C2E7BD76C9BDAFD7B975D1B68FD5
            B579DBB084CBA76ED8A457D7A262E2A150D4A16ADD9C50D29E65D89953DD994A
            D09C45DC9748CC9558C49748C78D47BD894BC98440A98842BD7919AD6A258466
            2D88602A5942173C25EF5A000000097048597300000B1200000B1201D2DD7EFC
            0000001C74455874536F6674776172650041646F62652046697265776F726B73
            2043533571B5E336000000D54944415478DA6364C0021871092ACAB340798FEE
            FC83093A1CFFFB1F2CC6E2780F2C0A127439C0F99781819381E38591F43FAEAB
            972182FB5981241B1068BEBCA9A9B60C2AC80C24993965399F69BEFA64B0162A
            08365056F486B4183FCBC1574882323217557E70CBFFD8FE1F2168C27352E98F
            80EC31F5BD084163D12352FF44650FBC7244083AB3FFFFF94C58ECF0873F4882
            311BDDB87E331E78CD802C187F97419EF9E06320C71E2198F8FFD7A7936F408E
            7040B2080E1CF740FCEE78F42F5C8CD9EA0044505586192EF8E7E96D3C818C01
            004FE4561533982BD20000000049454E44AE426082}
          ExplicitLeft = 60
          ExplicitTop = 0
        end
      end
    end
  end
  object plTop: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 966
    Height = 38
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 6
      Top = 10
      Width = 86
      Height = 16
      Caption = #1048#1084#1103' '#1088#1072#1079#1076#1077#1083#1072
    end
    object Label2: TLabel
      Left = 360
      Top = 9
      Width = 132
      Height = 16
      Caption = #1056#1072#1079#1076#1077#1083' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
    end
    object Label3: TLabel
      Left = 584
      Top = 10
      Width = 74
      Height = 16
      Caption = #1042#1080#1076' '#1092#1086#1088#1084#1099
    end
    object edName: TDBEditEh
      Left = 102
      Top = 7
      Width = 243
      Height = 24
      ControlLabel.Caption = #1048#1084#1103' '#1092#1086#1088#1084#1099
      DataField = 'caption'
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      TabOrder = 0
      Visible = True
    end
    object neStartKind: TDBNumberEditEh
      Left = 498
      Top = 7
      Width = 56
      Height = 24
      DataField = 'doc_kind'
      DataSource = dsLocal
      DecimalPlaces = 0
      DynProps = <>
      EditButtons = <>
      TabOrder = 1
      Visible = True
    end
    object cbForms: TDBComboBoxEh
      Left = 664
      Top = 7
      Width = 300
      Height = 24
      DataField = 'form_type'
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      Items.Strings = (
        #1043#1088#1091#1079' '#1087#1086' '#1046#1044
        #1043#1088#1091#1079' '#1072#1074#1090#1086
        #1050#1086#1085#1090#1077#1081#1085#1077#1088
        #1042#1072#1075#1086#1085)
      KeyItems.Strings = (
        'cargosoncarriages'
        'cargosoncars'
        'containers'
        'carriages')
      TabOrder = 2
      Visible = True
    end
  end
  object drvColumns: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select * from form_columns where form_id = :form_id order  by or' +
        'der_num')
    SelectCommand.Parameters = <
      item
        Name = 'form_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update form_columns set '
      'field_name = :field_name, '
      'column_title = :column_title, '
      'show_in_grid = :show_in_grid,'
      'order_num = :order_num'
      'where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'field_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'column_title'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'show_in_grid'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'order_num'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      
        'insert into form_columns (form_id, field_name, column_title, sho' +
        'w_in_grid, order_num)'
      
        'values ( :form_id,  :field_name, :column_title, :show_in_grid, :' +
        'order_num)'
      '')
    InsertCommand.Parameters = <
      item
        Name = 'form_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'field_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'column_title'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'show_in_grid'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'order_num'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from form_columns where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.Parameters = <>
    Left = 100
    Top = 306
  end
  object meColumns: TMemTableEh
    Params = <>
    DataDriver = drvColumns
    Left = 148
    Top = 306
  end
  object dcColumns: TDataSource
    DataSet = meColumns
    Left = 212
    Top = 306
  end
  object drvLinks: TADODataDriverEh
    DynaSQLParams.Options = []
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select o.*, r.DisplayName from organizations o left outer join r' +
        'egions r on (o.region = r.id)'
      'where parent_id = :id')
    SelectCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    UpdateCommand.Parameters = <>
    InsertCommand.CommandText.Strings = (
      'update organizations set parent_id = :parent_id where id = :pid')
    InsertCommand.Parameters = <
      item
        Name = 'parent_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'pid'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'update organizations set parent_id = null where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.Parameters = <>
    Left = 340
    Top = 306
  end
  object meLinks: TMemTableEh
    Params = <>
    DataDriver = drvLinks
    Left = 388
    Top = 306
  end
  object dsLinks: TDataSource
    DataSet = meLinks
    Left = 436
    Top = 306
  end
  object dsLocal: TDataSource
    Left = 520
    Top = 86
  end
end
