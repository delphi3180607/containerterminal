﻿unit objectlinks;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBCtrlsEh, Vcl.StdCtrls,
  DBSQLLookUp, Vcl.Mask, Vcl.ExtCtrls, Vcl.Buttons, Data.DB, Data.Win.ADODB;

type
  TFormEditObjectLink = class(TFormEdit)
    leStateKind: TDBSQLLookUp;
    ssStateKind: TADOLookUpSqlSet;
    Label1: TLabel;
    cbSendMail: TDBCheckBoxEh;
    Label5: TLabel;
    meMailTemplate: TDBMemoEh;
    cbObjectType: TDBComboBoxEh;
    Label2: TLabel;
    cbRelation: TDBComboBoxEh;
    Label3: TLabel;
    sbClear: TSpeedButton;
    cbUpdateType: TDBComboBoxEh;
    Label4: TLabel;
    procedure sbClearClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditObjectLink: TFormEditObjectLink;

implementation

{$R *.dfm}

uses main;

procedure TFormEditObjectLink.FormCreate(Sender: TObject);
begin
  FormMain.InitObjectType(cbObjectType);
end;

procedure TFormEditObjectLink.sbClearClick(Sender: TObject);
begin
  cbRelation.ItemIndex := -1;
end;

end.
