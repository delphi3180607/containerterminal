﻿inherited FormEditLoadPlan: TFormEditLoadPlan
  Caption = #1055#1083#1072#1085' '#1087#1086#1075#1088#1091#1079#1082#1080
  ClientHeight = 293
  ClientWidth = 540
  ExplicitWidth = 546
  ExplicitHeight = 321
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 9
    Top = 7
    Width = 80
    Height = 16
    Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103
  end
  object Label2: TLabel [1]
    Left = 9
    Top = 63
    Width = 82
    Height = 16
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
  end
  object Label3: TLabel [2]
    Left = 9
    Top = 183
    Width = 91
    Height = 14
    Caption = #1053#1086#1084#1077#1088' '#1087#1086#1077#1079#1076#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel [3]
    Left = 9
    Top = 121
    Width = 92
    Height = 16
    Caption = #1042#1080#1076' '#1086#1090#1087#1088#1072#1074#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel [4]
    Left = 204
    Top = 11
    Width = 71
    Height = 16
    Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100
  end
  inherited plBottom: TPanel
    Top = 252
    Width = 540
    TabOrder = 5
    ExplicitTop = 282
    inherited btnCancel: TButton
      Left = 424
    end
    inherited btnOk: TButton
      Left = 305
    end
  end
  object dtPlanDate: TDBDateTimeEditEh [6]
    Left = 9
    Top = 29
    Width = 179
    Height = 24
    DataField = 'plan_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
    EditFormat = 'DD/MM/YYYY HH:NN:SS'
  end
  object edNote: TDBEditEh [7]
    Left = 9
    Top = 85
    Width = 520
    Height = 24
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object edTrainNumber: TDBEditEh [8]
    Left = 9
    Top = 203
    Width = 280
    Height = 22
    Color = clCream
    DataField = 'train_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = True
  end
  object luDispatchKind: TDBLookupComboboxEh [9]
    Left = 9
    Top = 143
    Width = 280
    Height = 24
    DynProps = <>
    DataField = 'dispatch_kind_id'
    DataSource = dsLocal
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    KeyField = 'id'
    ListField = 'code'
    ListSource = FormDispatchKinds.dsData
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object edSheetServe: TDBEditEh [10]
    Left = 204
    Top = 29
    Width = 147
    Height = 24
    DataField = 'sheet_serve'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 449
    Top = 31
  end
  inherited qrAux: TADOQuery
    Left = 385
    Top = 31
  end
end
