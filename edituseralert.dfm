﻿inherited FormEditUserAlert: TFormEditUserAlert
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1085#1086#1074#1086#1089#1090#1080
  ClientHeight = 275
  ClientWidth = 499
  ExplicitWidth = 505
  ExplicitHeight = 303
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 20
    Top = 98
    Width = 92
    Height = 16
    Caption = #1058#1077#1089#1090' '#1085#1086#1074#1086#1089#1090#1080
  end
  inherited plBottom: TPanel
    Top = 234
    Width = 499
    inherited btnCancel: TButton
      Left = 383
    end
    inherited btnOk: TButton
      Left = 264
    end
  end
  object DBMemo1: TDBMemo [2]
    Left = 16
    Top = 122
    Width = 465
    Height = 89
    TabOrder = 1
  end
  object DBDateTimeEditEh1: TDBDateTimeEditEh [3]
    Left = 24
    Top = 48
    Width = 169
    Height = 24
    ControlLabel.Width = 138
    ControlLabel.Height = 16
    ControlLabel.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072' '#1087#1086#1082#1072#1079#1072
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 2
    Visible = True
  end
  object DBDateTimeEditEh2: TDBDateTimeEditEh [4]
    Left = 268
    Top = 48
    Width = 169
    Height = 24
    ControlLabel.Width = 162
    ControlLabel.Height = 16
    ControlLabel.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103' '#1087#1086#1082#1072#1079#1072
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 3
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 120
    Top = 176
  end
  inherited qrAux: TADOQuery
    Left = 168
    Top = 176
  end
end
