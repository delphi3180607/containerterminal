﻿inherited FormMtuKinds: TFormMtuKinds
  Caption = #1042#1080#1076#1099' '#1052#1058#1059
  ClientWidth = 925
  ExplicitWidth = 941
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Width = 925
    ExplicitWidth = 925
    inherited btnOk: TButton
      Left = 690
      ExplicitLeft = 690
    end
    inherited btnCancel: TButton
      Left = 809
      ExplicitLeft = 809
    end
  end
  inherited plAll: TPanel
    Width = 925
    ExplicitWidth = 925
    inherited plTop: TPanel
      Width = 925
      ExplicitWidth = 925
      inherited btTool: TPngSpeedButton
        Left = 887
        ExplicitLeft = 873
      end
      inherited plCount: TPanel
        Left = 690
        ExplicitLeft = 690
      end
    end
    inherited dgData: TDBGridEh
      Width = 925
      DataGrouping.Active = True
      DataGrouping.DefaultStateExpanded = True
      DataGrouping.Font.Color = clGray
      DataGrouping.Font.Height = -12
      DataGrouping.GroupLevels = <
        item
          ColumnName = 'Column_0_load_code'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 9786394
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        item
          ColumnName = 'Column_1_amount_code'
        end>
      DataGrouping.ParentColor = False
      DataGrouping.ParentFont = False
      Font.Height = -13
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'load_code'
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1087#1086#1075#1088#1091#1079#1082#1080
          Width = 72
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'amount_code'
          Footers = <>
          Title.Caption = '*'
          Width = 89
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Width = 172
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076'/'#1074#1072#1088#1080#1072#1085#1090' '#1087#1086#1075#1088#1091#1079#1082#1080
          Visible = False
          Width = 178
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 314
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'picture_name'
          Footers = <>
          Title.Caption = #1056#1080#1089#1091#1085#1086#1082' ('#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102', '#1085#1072#1080#1073#1086#1083#1077#1077' '#1074#1077#1088#1086#1103#1090#1085#1099#1081', '#1089#1074#1103#1079#1072#1085#1085#1099#1081')'
          Width = 439
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'select *, (select name from picturekinds p where p.id = picture_' +
        'id) as picture_name from mtukinds order by code')
    UpdateCommand.CommandText.Strings = (
      'update mtukinds'
      'set'
      '  code = :code,'
      '  name = :name,'
      '  picture_id = :picture_id,'
      '  load_code = :load_code,  '
      ' amount_code = :amount_code, '
      ' note = :note'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'picture_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'load_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'amount_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'note'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into mtukinds'
      '  (code, name, picture_id, load_code, amount_code, note)'
      'values'
      '  (:code, :name, :picture_id, :load_code, :amount_code, :note)'
      '')
    InsertCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'picture_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'load_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'amount_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'note'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from mtukinds where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select *, (select name from picturekinds p where p.id = picture_' +
        'id) as picture_name from mtukinds where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
end
