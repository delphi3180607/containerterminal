﻿unit setguard;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, setcheck, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBCtrlsEh;

type
  TFormSetGuard = class(TFormSetCheck)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSetGuard: TFormSetGuard;

implementation

{$R *.dfm}

end.
