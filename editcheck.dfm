﻿inherited FormEditCheck: TFormEditCheck
  Caption = #1055#1088#1086#1074#1077#1088#1082#1072'/'#1088#1072#1079#1075#1088#1091#1079#1082#1072
  ClientHeight = 326
  ClientWidth = 465
  Font.Height = -15
  ExplicitWidth = 471
  ExplicitHeight = 354
  PixelsPerInch = 96
  TextHeight = 18
  object Label1: TLabel [0]
    Left = 9
    Top = 125
    Width = 93
    Height = 18
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
  end
  object cbUnloadResult: TDBComboBoxEh [1]
    Left = 8
    Top = 30
    Width = 449
    Height = 26
    ControlLabel.Width = 212
    ControlLabel.Height = 18
    ControlLabel.Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090' '#1087#1088#1086#1074#1077#1088#1082#1080'/'#1088#1072#1079#1075#1088#1091#1079#1082#1080
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -15
    ControlLabel.Font.Name = 'Tahoma'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'check_result'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      #1055#1088#1086#1074#1077#1088#1082#1091' '#1087#1088#1086#1096#1077#1083', '#1042#1099#1075#1088#1091#1078#1077#1085
      #1055#1088#1086#1074#1077#1088#1082#1091' '#1085#1077' '#1087#1088#1086#1096#1077#1083', '#1042#1099#1075#1088#1091#1078#1077#1085
      #1053#1077' '#1087#1088#1080#1073#1099#1083', '#1053#1077' '#1074#1099#1075#1088#1091#1078#1077#1085
      #1055#1086#1083#1091#1095#1077#1085' '#1086#1096#1080#1073#1086#1095#1085#1086', '#1053#1077' '#1074#1099#1075#1088#1091#1078#1077#1085)
    KeyItems.Strings = (
      '1'
      '2'
      '3'
      '4')
    TabOrder = 0
    Visible = True
  end
  object dtCheckDateTime: TDBDateTimeEditEh [2]
    Left = 8
    Top = 236
    Width = 223
    Height = 26
    ControlLabel.Width = 140
    ControlLabel.Height = 18
    ControlLabel.Caption = #1044#1072#1090#1072' '#1074#1088#1077#1084#1103' '#1089#1090#1072#1090#1091#1089#1072
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -15
    ControlLabel.Font.Name = 'Tahoma'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'check_datetime'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
    EditFormat = 'DD/MM/YYYY HH:NN:SS'
  end
  object meNote: TDBMemo [3]
    Left = 8
    Top = 146
    Width = 449
    Height = 57
    DataField = 'check_note'
    DataSource = dsLocal
    TabOrder = 2
  end
  inherited plBottom: TPanel
    Top = 285
    Width = 465
    TabOrder = 4
    ExplicitTop = 223
    ExplicitWidth = 465
    inherited btnCancel: TButton
      Left = 349
      ExplicitLeft = 349
    end
    inherited btnOk: TButton
      Left = 230
      ExplicitLeft = 230
    end
    object Button1: TButton
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 113
      Height = 35
      Align = alLeft
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = []
      ModalResult = 7
      ParentFont = False
      TabOrder = 2
    end
  end
  object cbShift: TDBCheckBoxEh [5]
    Left = 8
    Top = 84
    Width = 201
    Height = 17
    Caption = #1057#1084#1077#1097#1077#1085#1080#1077' '#1094#1077#1085#1090#1088#1072' '#1090#1103#1078#1077#1089#1090#1080
    DataField = 'weight_shift'
    DataSource = dsLocal
    DynProps = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  inherited dsLocal: TDataSource
    Left = 352
    Top = 221
  end
  inherited qrAux: TADOQuery
    Left = 296
    Top = 221
  end
end
