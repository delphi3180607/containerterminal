﻿inherited FormEditDeliveryComplete: TFormEditDeliveryComplete
  Caption = #1047#1072#1074#1077#1088#1096#1077#1085#1080#1077' '#1079#1072#1076#1072#1095#1080
  ClientHeight = 265
  ClientWidth = 499
  ExplicitWidth = 505
  ExplicitHeight = 293
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 8
    Top = 10
    Width = 84
    Height = 16
    Caption = #1058#1080#1087' '#1086#1073#1100#1077#1082#1090#1072
  end
  object Label6: TLabel [1]
    Left = 8
    Top = 76
    Width = 140
    Height = 16
    Caption = #1050#1086#1085#1077#1095#1085#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
  end
  object Label3: TLabel [2]
    Left = 8
    Top = 146
    Width = 138
    Height = 16
    Caption = #1058#1080#1087' '#1082#1086#1085#1077#1095#1085#1086#1081' '#1089#1074#1103#1079#1080
  end
  inherited plBottom: TPanel
    Top = 224
    Width = 499
    ExplicitTop = 224
    ExplicitWidth = 499
    inherited btnCancel: TButton
      Left = 383
      ExplicitLeft = 383
    end
    inherited btnOk: TButton
      Left = 264
      ExplicitLeft = 264
    end
  end
  object cbObjectType: TDBSQLLookUp [4]
    Left = 8
    Top = 32
    Width = 473
    Height = 24
    DataField = 'objecttype_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
    SqlSet = ssObjectTypes
    RowCount = 0
  end
  object leStartState: TDBSQLLookUp [5]
    Left = 8
    Top = 98
    Width = 473
    Height = 24
    DataField = 'object_state_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
    SqlSet = ssStateKind
    RowCount = 0
  end
  object cbRelationStart: TDBComboBoxEh [6]
    Left = 8
    Top = 169
    Width = 473
    Height = 24
    DataField = 'link_type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Items.Strings = (
      #1056#1072#1079#1100#1077#1076#1080#1085#1077#1085#1080#1077
      #1057#1086#1077#1076#1080#1085#1077#1085#1080#1077)
    KeyItems.Strings = (
      '0'
      '1')
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object ssObjectTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from objecttypes order by code')
    DownSql.Strings = (
      'select * from objecttypes order by code')
    InitSql.Strings = (
      'select * from objecttypes where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 226
    Top = 31
  end
  object ssStateKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from objectstatekinds order by code')
    DownSql.Strings = (
      'select * from objectstatekinds order by code')
    InitSql.Strings = (
      'select * from objectstatekinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 202
    Top = 95
  end
end
