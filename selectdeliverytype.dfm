﻿inherited FormSelectDeliveryType: TFormSelectDeliveryType
  Caption = #1048#1079#1084#1077#1085#1077#1085#1080#1077' '#1090#1080#1087#1072' '#1076#1086#1089#1090#1072#1074#1082#1080
  ClientHeight = 182
  ClientWidth = 448
  ExplicitWidth = 454
  ExplicitHeight = 210
  PixelsPerInch = 96
  TextHeight = 16
  object Label16: TLabel [0]
    Left = 26
    Top = 10
    Width = 93
    Height = 16
    Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 141
    Width = 448
    ExplicitTop = 141
    ExplicitWidth = 448
    inherited btnCancel: TButton
      Left = 332
      ExplicitLeft = 332
    end
    inherited btnOk: TButton
      Left = 213
      ExplicitLeft = 213
    end
  end
  object laDlvTypes: TDBSQLLookUp [2]
    Left = 26
    Top = 32
    Width = 280
    Height = 23
    AutoSize = False
    Color = 14810109
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 1
    Visible = True
    SqlSet = ssDlvTypes
    RowCount = 0
  end
  object ssDlvTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from deliverytypes d where exists '
      '('
      'select 1 from docroutes r, doctypes t '
      
        'where r.end_doctype_id = t.id and t.system_section = '#39'income_she' +
        'et'#39
      'and r.dlv_type_id = d.id'
      ')')
    DownSql.Strings = (
      'select * from deliverytypes d where exists '
      '('
      'select 1 from docroutes r, doctypes t '
      
        'where r.end_doctype_id = t.id and t.system_section = '#39'income_she' +
        'et'#39
      'and r.dlv_type_id = d.id'
      ')'
      '')
    InitSql.Strings = (
      'select * from deliverytypes where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 250
    Top = 66
  end
end
