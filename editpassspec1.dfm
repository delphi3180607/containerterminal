﻿inherited FormEditPassSpec1: TFormEditPassSpec1
  Caption = #1055#1088#1086#1095#1080#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
  ClientHeight = 228
  ClientWidth = 386
  ExplicitWidth = 392
  ExplicitHeight = 256
  PixelsPerInch = 96
  TextHeight = 16
  object Label5: TLabel [0]
    Left = 11
    Top = 10
    Width = 93
    Height = 14
    Caption = #1042#1080#1076' '#1086#1087#1077#1088#1072#1094#1080#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel [1]
    Left = 11
    Top = 64
    Width = 82
    Height = 16
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
  end
  inherited plBottom: TPanel
    Top = 187
    Width = 386
    TabOrder = 2
    inherited btnCancel: TButton
      Left = 270
    end
    inherited btnOk: TButton
      Left = 151
    end
  end
  object cbOpKind: TDBComboBoxEh [3]
    Left = 11
    Top = 30
    Width = 363
    Height = 24
    DataField = 'passoperation_type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Items.Strings = (
      #1055#1088#1080#1077#1084' ('#1079#1072#1074#1086#1079')'
      #1042#1099#1076#1072#1095#1072' ('#1074#1099#1074#1086#1079')')
    KeyItems.Strings = (
      '0'
      '1')
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edNote: TDBMemoEh [4]
    Left = 11
    Top = 82
    Width = 363
    Height = 90
    ScrollBars = ssVertical
    AutoSize = False
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
    WantReturns = True
  end
  inherited dsLocal: TDataSource
    Left = 24
    Top = 192
  end
  inherited qrAux: TADOQuery
    Left = 72
    Top = 192
  end
end
