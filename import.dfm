﻿inherited FormImport: TFormImport
  Caption = #1048#1084#1087#1086#1088#1090
  ClientHeight = 163
  ClientWidth = 539
  ExplicitWidth = 545
  ExplicitHeight = 191
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 27
    Top = 34
    Width = 125
    Height = 16
    Caption = #1060#1072#1081#1083' '#1076#1083#1103' '#1080#1084#1087#1086#1088#1090#1072
  end
  inherited plBottom: TPanel
    Top = 122
    Width = 539
    TabOrder = 1
    inherited btnCancel: TButton
      Left = 423
    end
    inherited btnOk: TButton
      Left = 304
      OnClick = btnOkClick
    end
  end
  object edFileName: TDBEditEh [2]
    Left = 27
    Top = 56
    Width = 481
    Height = 24
    DynProps = <>
    EditButtons = <
      item
        ShortCut = 45
        Style = ebsEllipsisEh
        OnClick = edFileNameEditButtons0Click
      end>
    TabOrder = 0
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 144
    Top = 112
  end
  inherited qrAux: TADOQuery
    Left = 248
    Top = 112
  end
  object ExcelApplication1: TExcelApplication
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    AutoQuit = False
    Left = 312
    Top = 16
  end
  object od: TOpenDialog
    Left = 200
    Top = 112
  end
end
