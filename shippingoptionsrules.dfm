﻿inherited FormShippingOptionsRules: TFormShippingOptionsRules
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1074#1072#1088#1080#1072#1085#1090#1086#1074' '#1076#1086#1089#1090#1072#1074#1082#1080
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      TitleParams.MultiTitle = True
      OnMouseDown = nil
      OnMouseMove = nil
      OnMouseUp = nil
      OnSelectionChanged = nil
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'priority'
          Footers = <>
          Title.Caption = #1055#1088#1080#1086#1088#1080#1090#1077#1090
          Width = 100
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer_code'
          Footers = <>
          TextEditing = False
          Title.Caption = #1050#1083#1080#1077#1085#1090
          Width = 119
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'direction_code'
          Footers = <>
          TextEditing = False
          Title.Caption = #1055#1086#1088#1090
          Width = 134
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'station_code'
          Footers = <>
          TextEditing = False
          Title.Caption = #1057#1090#1072#1085#1094#1080#1103
          Width = 124
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'exclude_sign'
          Footers = <>
          Title.Caption = #1048#1089#1082#1083#1102#1095#1072#1090#1100' '#1091#1089#1083#1091#1075#1080'?'
          Width = 90
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'extra_services'
          Footers = <>
          TextEditing = False
          Title.Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1091#1089#1083#1091#1075#1080
          Width = 396
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'sho_code'
          Footers = <>
          TextEditing = False
          Title.Caption = #1042#1072#1088#1080#1072#1085#1090' '#1076#1086#1089#1090#1072#1074#1082#1080
          Width = 177
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'select shor.id as id, shor.shippingoption_id, shor.payer_code, d' +
        'irection_code, station_code, extra_services, exclude_sign, sho.c' +
        'ode as sho_code, shor.priority'
      'from shippingoptionrules shor'
      
        'left outer join shippingoptions sho  on (shor.shippingoption_id ' +
        '= sho.id)'
      'order by shor.priority, shor.direction_code, shor.station_code')
    UpdateCommand.CommandText.Strings = (
      'update shippingoptionrules'
      'set'
      '  payer_code = :payer_code,'
      '  direction_code = :direction_code,'
      '  station_code = :station_code,'
      '  extra_services = :extra_services,'
      '  exclude_sign = :exclude_sign,'
      '  shippingoption_id = :shippingoption_id,'
      '  priority = :priority'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'payer_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'direction_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'station_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'extra_services'
        Size = -1
        Value = Null
      end
      item
        Name = 'exclude_sign'
        Size = -1
        Value = Null
      end
      item
        Name = 'shippingoption_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'priority'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into shippingoptionrules'
      
        '  (payer_code, direction_code, station_code, extra_services, exc' +
        'lude_sign, shippingoption_id, priority)'
      'values'
      
        '  (:payer_code, :direction_code, :station_code, :extra_services,' +
        ' :exclude_sign, :shippingoption_id, :priority)')
    InsertCommand.Parameters = <
      item
        Name = 'payer_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'direction_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'station_code'
        Size = -1
        Value = Null
      end
      item
        Name = 'extra_services'
        Size = -1
        Value = Null
      end
      item
        Name = 'exclude_sign'
        Size = -1
        Value = Null
      end
      item
        Name = 'shippingoption_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'priority'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from shippingoptionrules where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select shor.id as id, shor.shippingoption_id, shor.payer_code, d' +
        'irection_code, station_code, extra_services, exclude_sign, sho.c' +
        'ode as sho_code, shor.priority'
      'from shippingoptionrules shor'
      
        'left outer join shippingoptions sho  on (shor.shippingoption_id ' +
        '= sho.id)'
      'where shor.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
end
