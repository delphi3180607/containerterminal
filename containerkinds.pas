﻿unit containerkinds;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, MemTableDataEh, Data.DB,
  Data.Win.ADODB, MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport;

type
  TFormContainerKinds = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormContainerKinds: TFormContainerKinds;

implementation

{$R *.dfm}

uses editcontainerkind;


procedure TFormContainerKinds.Init;
begin
  inherited;
  tablename := 'containerkinds';
  self.formEdit := FormEditContainerKind;
end;

end.
