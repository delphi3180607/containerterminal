﻿unit filter;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, Vcl.Buttons;

type
  TFormFilter = class(TFormEdit)
    dtBeginDate: TDBDateTimeEditEh;
    dtEndDate: TDBDateTimeEditEh;
    meNumbers: TMemo;
    Label1: TLabel;
    Label3: TLabel;
    cbConfirmedOnly: TDBCheckBoxEh;
    btClear: TButton;
    cbClear: TSpeedButton;
    brTerm: TButton;
    procedure btClearClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure cbClearClick(Sender: TObject);
    procedure brTermClick(Sender: TObject);
    procedure meNumbersChange(Sender: TObject);
  private
    { Private declarations }
  public
    conditions: string;
    procedure ComposeConditions;
  end;

var
  FormFilter: TFormFilter;

implementation

{$R *.dfm}

procedure TFormFilter.brTermClick(Sender: TObject);
begin
  self.dtBeginDate.Value := now()-3;
  self.dtEndDate.Value := now();
end;

procedure TFormFilter.btClearClick(Sender: TObject);
begin
  self.dtBeginDate.Value := now()-8;
  self.dtEndDate.Value := now()+1;
  meNumbers.Lines.Clear;
end;

procedure TFormFilter.btnOkClick(Sender: TObject);
begin
  ComposeConditions;
end;

procedure TFormFilter.cbClearClick(Sender: TObject);
begin
  meNumbers.Lines.Clear;
  if dtBegindate.Value = null then
  begin
    self.dtBeginDate.Value := now()-3;
    self.dtEndDate.Value := now();
  end;
end;


procedure TFormFilter.ComposeConditions;
var i: integer;
begin

  conditions := '';

  if dtBeginDate.Value <> null then
   conditions := conditions +';date_begin='+dtBeginDate.Text;

  if dtEndDate.Value <> null then
   conditions := conditions +';date_end='+dtEndDate.Text;

  if cbConfirmedOnly.Checked then
   conditions := conditions +';isconfirmed=1';

  for i := 0 to meNumbers.Lines.Count-1 do
  begin
   conditions := conditions +';object_num='+trim(meNumbers.Lines[i]);
  end;

end;


procedure TFormFilter.meNumbersChange(Sender: TObject);
begin

  if Trim(meNumbers.Text) <> '' then
  begin
    dtBegindate.Value := null;
    dtEnddate.Value := null;
  end else
  begin
    if dtBegindate.Value = null then
    begin
      self.dtBeginDate.Value := now()-3;
      self.dtEndDate.Value := now();
    end;
  end;

  cbConfirmedOnly.Checked := false;

end;

end.
