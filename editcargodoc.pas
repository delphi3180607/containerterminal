﻿unit editcargodoc;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEh, DBGridEhGrouping,
  ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh,
  PngSpeedButton, DBCtrlsEh, Vcl.StdCtrls, Vcl.Mask, DBLookupEh, Vcl.ExtCtrls,
  Vcl.Buttons, Data.DB, Data.Win.ADODB, MemTableDataEh, MemTableEh,
  DataDriverEh, ADODataDriverEh, functions, Vcl.ComCtrls;

type
  TFormEditOder = class(TFormEdit)
    plTop: TPanel;
    Label8: TLabel;
    sbConsignee: TSpeedButton;
    Label9: TLabel;
    sbPayer: TSpeedButton;
    Label2: TLabel;
    sbForwarder: TSpeedButton;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Bevel1: TBevel;
    luConsignee: TDBLookupComboboxEh;
    luPayer: TDBLookupComboboxEh;
    luForwarder: TDBLookupComboboxEh;
    edDocNumber: TDBEditEh;
    dtDocDate: TDBDateTimeEditEh;
    dtPlanDate: TDBDateTimeEditEh;
    dtFactDate: TDBDateTimeEditEh;
    stDocType: TDBEditEh;
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    edObjectId: TDBLookupComboboxEh;
    plTool: TPanel;
    sbAddSpec: TPngSpeedButton;
    sbDeleteSpec: TPngSpeedButton;
    sbEditSpec: TPngSpeedButton;
    dgData: TDBGridEh;
    Label6: TLabel;
    luDirection: TDBLookupComboboxEh;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    luEndStation: TDBLookupComboboxEh;
    Label10: TLabel;
    procedure sbForwarderClick(Sender: TObject);
    procedure sbConsigneeClick(Sender: TObject);
    procedure sbPayerClick(Sender: TObject);
    procedure sbAddSpecClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure sbDeleteSpecClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditOder: TFormEditOder;

implementation

{$R *.dfm}

uses counteragents, selectobjecttype, cargos, carriages, containers;

procedure TFormEditOder.FormActivate(Sender: TObject);
begin
  inherited;
  meSpec.Open;
end;

procedure TFormEditOder.sbAddSpecClick(Sender: TObject);
var t: string; id: integer;
begin

  FormSelectObjectType.qrAux.Open;
  FormSelectObjectType.ShowModal;
  if FormSelectObjectType.ModalResult = mrOk then
  begin
    if FormSelectObjectType.qrAux.FieldByName('code').AsString = 'cargo' then
    begin
      SFD(FormCargos, self.edObjectId);
    end else
    if FormSelectObjectType.qrAux.FieldByName('code').AsString = 'carriage' then
    begin
      SFD(FormCarriages, self.edObjectId);
    end else
    if FormSelectObjectType.qrAux.FieldByName('code').AsString = 'container' then
    begin
      SFD(FormContainers, self.edObjectId);
    end;

    if dsLocal.DataSet.State in [dsInsert] then
    begin
      TMemTableEh(dsLocal.DataSet).Post;
      id := TAdoDataDriverEh(TMemTableEh(dsLocal.DataSet).DataDriver).GetRecCommand.Parameters.ParamByName('current_id').Value;
      TMemTableEh(dsLocal.DataSet).Edit;
    end else
    begin
      id := TMemTableEh(dsLocal.DataSet).FieldByName('id').AsInteger;
    end;

    meSpec.Append;
    meSpec.FieldByName('dochead_id').Value := id;
    meSpec.FieldByName('object_type').Value := FormSelectObjectType.qrAux.FieldByName('code').AsString;
    meSpec.FieldByName('typed_object_id').Value := self.edObjectId.Value;
    meSpec.FieldByName('parentobject_id').Value := null;
    meSpec.FieldByName('proper_state_id').Value := null;
    meSpec.Post;
    meSpec.Refresh;

  end;
end;

procedure TFormEditOder.sbConsigneeClick(Sender: TObject);
begin
  SFD(FormCounteragents,self.luConsignee);
end;

procedure TFormEditOder.sbDeleteSpecClick(Sender: TObject);
begin
  ED(self.meSpec);
end;

procedure TFormEditOder.sbForwarderClick(Sender: TObject);
begin
  SFD(FormCounteragents,self.luForwarder);
end;

procedure TFormEditOder.sbPayerClick(Sender: TObject);
begin
  SFD(FormCounteragents,self.luPayer);
end;

end.
