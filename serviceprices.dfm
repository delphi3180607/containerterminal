﻿inherited FormServicePrices: TFormServicePrices
  Caption = #1057#1077#1090#1082#1072' '#1094#1077#1085' '#1085#1072' '#1091#1089#1083#1091#1075#1080
  ClientWidth = 881
  ExplicitWidth = 897
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter [0]
    Left = 334
    Top = 0
    Width = 4
    Height = 470
    ExplicitLeft = 255
    ExplicitTop = -24
    ExplicitHeight = 410
  end
  inherited plBottom: TPanel
    Width = 881
    ExplicitWidth = 881
    inherited btnOk: TButton
      Left = 646
      ExplicitLeft = 646
    end
    inherited btnCancel: TButton
      Left = 765
      ExplicitLeft = 765
    end
  end
  inherited plAll: TPanel
    Left = 338
    Width = 543
    ExplicitLeft = 338
    ExplicitWidth = 543
    inherited plTop: TPanel
      Top = 29
      Width = 543
      ExplicitTop = 29
      ExplicitWidth = 543
      inherited btTool: TPngSpeedButton
        Left = 491
        ExplicitLeft = 183
      end
      object cbShowHistory: TCheckBox
        AlignWithMargins = True
        Left = 212
        Top = 3
        Width = 133
        Height = 23
        Margins.Left = 10
        Align = alLeft
        Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1080#1089#1090#1086#1088#1080#1102
        TabOrder = 0
      end
    end
    inherited dgData: TDBGridEh
      Top = 58
      Width = 543
      Height = 412
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_change'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
          Width = 180
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1083#1080#1077#1085#1090
          Width = 182
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'price_default'
          Footers = <>
          Title.Caption = #1062#1077#1085#1072
          Width = 111
        end>
    end
    object Panel7: TPanel
      Left = 0
      Top = 0
      Width = 543
      Height = 29
      Align = alTop
      BevelOuter = bvLowered
      Caption = #1050#1083#1080#1077#1085#1090' - '#1094#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 0
    Width = 334
    Height = 470
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'plAll'
    TabOrder = 2
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 334
      Height = 29
      Align = alTop
      BevelOuter = bvLowered
      Caption = #1059#1089#1083#1091#1075#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object dgServices: TDBGridEh
      Left = 0
      Top = 29
      Width = 334
      Height = 441
      Align = alClient
      AutoFitColWidths = True
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.Active = True
      DataGrouping.Color = clWindow
      DataGrouping.GroupLevels = <
        item
          ColumnName = 'Column_0_folder_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end>
      DataGrouping.ParentColor = False
      DataGrouping.DefaultStateExpanded = True
      DataSource = dsService
      DynProps = <>
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = []
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      IndicatorOptions = [gioShowRowIndicatorEh]
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
      ParentFont = False
      PopupMenu = pmGrid
      ReadOnly = True
      SearchPanel.Enabled = True
      SearchPanel.PersistentShowing = False
      SelectionDrawParams.SelectionStyle = gsdsGridThemedEh
      SelectionDrawParams.DrawFocusFrame = True
      SelectionDrawParams.DrawFocusFrameStored = True
      SortLocal = True
      STFilter.InstantApply = True
      STFilter.Local = True
      STFilter.Visible = True
      STFilter.Color = 16513004
      TabOrder = 1
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -12
      TitleParams.Font.Name = 'Verdana'
      TitleParams.Font.Style = [fsBold]
      TitleParams.ParentFont = False
      TitleParams.MultiTitle = True
      TitleParams.FillStyle = cfstGradientEh
      OnActiveGroupingStructChanged = dgDataActiveGroupingStructChanged
      OnColumnMoved = dgDataColumnMoved
      OnColWidthsChanged = dgDataColWidthsChanged
      OnDblClick = sbEditClick
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'folder_name'
          Footers = <>
          Title.Caption = #1050#1072#1090#1072#1083#1086#1075
          Visible = False
          Width = 168
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'sname'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1091#1089#1083#1091#1075#1080
          Width = 291
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'select p.*, c.code from price_service_custs p, counteragents c w' +
        'here p.customer_id = c.id order by p.date_change')
    UpdateCommand.CommandText.Strings = (
      'update price_service_custs'
      'set'
      '  servicekind_id = :servicekind_id,'
      '  customer_id = :customer_id,'
      '  date_change = :date_change,'
      '  price_default = :price_default'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'servicekind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'customer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'date_change'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'price_default'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into price_service_custs'
      '  (servicekind_id, customer_id, date_change, price_default)'
      'values'
      '  (:servicekind_id, :customer_id, :date_change, :price_default)')
    InsertCommand.Parameters = <
      item
        Name = 'servicekind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'customer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'date_change'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'price_default'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from price_service_custs where  id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select p.*, c.code from price_service_custs p, counteragents c '
      'where p.customer_id = c.id and p.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 688
    Top = 176
  end
  inherited meData: TMemTableEh
    DetailFields = 'servicekind_id'
    MasterFields = 'id'
    MasterSource = dsService
    Left = 736
    Top = 176
  end
  inherited dsData: TDataSource
    Left = 792
    Top = 176
  end
  object drvService: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select * from servicekinds k, folders f where k.folder_id = f.id')
    SelectCommand.Parameters = <>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 72
    Top = 176
  end
  object meService: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvService
    BeforeEdit = meDataBeforeEdit
    BeforePost = meDataBeforePost
    AfterPost = meDataAfterPost
    Left = 120
    Top = 176
  end
  object dsService: TDataSource
    DataSet = meService
    Left = 176
    Top = 176
  end
end
