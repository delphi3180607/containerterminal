﻿unit docroutes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, Functions, System.Actions, Vcl.ActnList, EXLReportExcelTLB,
  EXLReportBand, EXLReport, Vcl.Mask, DBCtrlsEh;

type
  TFormDocRoutes = class(TFormGrid)
    plLeft: TPanel;
    dgDocTypes: TDBGridEh;
    Splitter1: TSplitter;
    plObjects: TPanel;
    Panel1: TPanel;
    sbAddObject: TPngSpeedButton;
    sbDeleteObject: TPngSpeedButton;
    sbEditObject: TPngSpeedButton;
    dgObjects: TDBGridEh;
    drvDeliveryTypes: TADODataDriverEh;
    meDeliveryTypes: TMemTableEh;
    dsDeliveryTypes: TDataSource;
    drvObjects: TADODataDriverEh;
    meObjects: TMemTableEh;
    dsObjects: TDataSource;
    Splitter2: TSplitter;
    Panel2: TPanel;
    Panel3: TPanel;
    sbDeleteServiceLink: TPngSpeedButton;
    sbExcelServiceLink: TPngSpeedButton;
    sbAddServiceLink: TPngSpeedButton;
    sbEditServiceLink: TPngSpeedButton;
    Bevel1: TBevel;
    PngSpeedButton6: TPngSpeedButton;
    Splitter3: TSplitter;
    drvComplete: TADODataDriverEh;
    meComplete: TMemTableEh;
    dsComplete: TDataSource;
    dgComplete: TDBGridEh;
    procedure meDeliveryTypesAfterScroll(DataSet: TDataSet);
    procedure meDataBeforePost(DataSet: TDataSet);
    procedure dgDocTypesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbDeleteObjectClick(Sender: TObject);
    procedure sbAddObjectClick(Sender: TObject);
    procedure sbEditObjectClick(Sender: TObject);
    procedure meObjectsBeforePost(DataSet: TDataSet);
    procedure dgObjectsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbAddServiceLinkClick(Sender: TObject);
    procedure sbEditServiceLinkClick(Sender: TObject);
    procedure sbDeleteServiceLinkClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormDocRoutes: TFormDocRoutes;

implementation

{$R *.dfm}

uses editdocroute, objectlinks, editdocroute2objects, editcontractspec,
  EditDeliveryComplete;

procedure TFormDocRoutes.dgDocTypesKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_F5 then
  begin
    meDeliveryTypes.Close;
    meDeliveryTypes.Open;
  end;
end;

procedure TFormDocRoutes.dgObjectsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_F5 then
  begin
    meObjects.Close;
    meObjects.Open;
  end;
end;

procedure TFormDocRoutes.Init;
begin
  self.meDeliveryTypes.Open;
  inherited;
  meObjects.Open;
  meObjects.Close;
  meObjects.Open;
  meComplete.Open;
  self.formEdit := FormEditDocRoute;
end;


procedure TFormDocRoutes.meDataBeforePost(DataSet: TDataSet);
begin
  inherited;
  meData.FieldByName('dlv_type_id').Value := meDeliveryTypes.FieldByName('id').AsInteger;
end;

procedure TFormDocRoutes.meDeliveryTypesAfterScroll(DataSet: TDataSet);
begin
  meData.Filter := 'dlv_type_id =' + meDeliveryTypes.FieldByName('id').AsString;
  meData.Filtered := true;

  meComplete.Filter := 'dlv_type_id =' + meDeliveryTypes.FieldByName('id').AsString;
  meComplete.Filtered := true;

end;

procedure TFormDocRoutes.meObjectsBeforePost(DataSet: TDataSet);
begin
  inherited;
  meObjects.FieldByName('docroute_id').Value := meData.FieldByName('id').AsInteger;
end;

procedure TFormDocRoutes.sbDeleteServiceLinkClick(Sender: TObject);
begin
  ED(self.meComplete);
end;

procedure TFormDocRoutes.sbAddServiceLinkClick(Sender: TObject);
begin
  EA(self, FormEditDeliveryComplete, self.meComplete, 'deliverycomplete', 'dlv_type_id', self.meDeliveryTypes.FieldByName('id').AsInteger);
end;

procedure TFormDocRoutes.sbEditServiceLinkClick(Sender: TObject);
begin
  EE(self, FormEditDeliveryComplete, self.meComplete, 'deliverycomplete');
end;

procedure TFormDocRoutes.sbAddObjectClick(Sender: TObject);
begin
  EA(self, FormEditDocroute2Object, self.meObjects, 'docroute2objectstates', 'docroute_id', self.meData.FieldByName('id').AsInteger);
end;

procedure TFormDocRoutes.sbDeleteObjectClick(Sender: TObject);
begin
  ED(self.meObjects);
end;

procedure TFormDocRoutes.sbEditObjectClick(Sender: TObject);
begin
  EE(self, FormEditDocroute2Object, self.meObjects, 'docroute2objectstates');
end;

end.
