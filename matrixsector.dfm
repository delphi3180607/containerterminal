﻿object FormMatrixSector: TFormMatrixSector
  Left = 0
  Top = 0
  Caption = 'FormMatrixSector'
  ClientHeight = 827
  ClientWidth = 996
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object dgView: TDBGridEh
    Left = 0
    Top = 0
    Width = 861
    Height = 827
    Align = alClient
    AllowedOperations = []
    AllowedSelections = [gstRectangle]
    BorderStyle = bsNone
    Color = 15790320
    DataGrouping.Active = True
    DataGrouping.Color = 15921390
    DataGrouping.GroupRowDefValues.BottomLine.Color = 15921906
    DataGrouping.ParentColor = False
    DataSource = dsSector
    DefaultDrawing = False
    DynProps = <>
    FixedColor = 15921390
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'Arial'
    Font.Style = []
    GridLineParams.DataHorzColor = clWhite
    GridLineParams.DataHorzLines = True
    GridLineParams.DataVertColor = clWhite
    GridLineParams.DataVertLines = True
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    IndicatorParams.Color = 15724527
    IndicatorParams.FillStyle = cfstSolidEh
    IndicatorParams.VertLineColor = clSilver
    Options = [dgEditing, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghResizeWholeRightPart, dghHighlightFocus, dghClearSelection, dghDialogFind, dghHotTrack]
    ParentFont = False
    PopupMenu = pmGrid
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleParams.Color = 13553100
    TitleParams.FillStyle = cfstSolidEh
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -13
    TitleParams.Font.Name = 'Calibri'
    TitleParams.Font.Style = [fsBold]
    TitleParams.HorzLineColor = clSilver
    TitleParams.HorzLines = True
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    TitleParams.RowLines = 2
    TitleParams.SecondColor = clSilver
    TitleParams.VertLineColor = clSilver
    TitleParams.VertLines = True
    OnAdvDrawDataCell = dgViewAdvDrawDataCell
    OnCellMouseClick = dgViewCellMouseClick
    OnColEnter = dgViewColEnter
    OnDblClick = dgViewDblClick
    OnDrawColumnCell = dgViewDrawColumnCell
    OnKeyPress = dgViewKeyPress
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        Footers = <>
        Title.Color = clNavy
        Width = 173
        OnUpdateData = dgViewColumns0UpdateData
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object plProps: TPanel
    AlignWithMargins = True
    Left = 864
    Top = 3
    Width = 129
    Height = 821
    Align = alRight
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object edDlvType: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 20
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 107
      ControlLabel.Height = 14
      ControlLabel.Caption = #1053#1072#1095#1072#1083#1100#1085#1099#1081' '#1087#1088#1086#1094#1077#1089#1089
      ControlLabel.Visible = True
      DataField = 'current_process'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      Visible = True
    end
    object edNotReady: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 740
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 99
      ControlLabel.Height = 14
      ControlLabel.Caption = #1043#1086#1090#1086#1074' '#1082' '#1087#1077#1088#1077#1074#1086#1079#1082#1077
      ControlLabel.Visible = True
      DataField = 'isready'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      Visible = True
    end
    object edDefect: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 695
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 69
      ControlLabel.Height = 14
      ControlLabel.Caption = #1044#1077#1092#1077#1082#1090#1085#1086#1089#1090#1100
      ControlLabel.Visible = True
      DataField = 'defect_mark'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      Visible = True
    end
    object edShift: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 650
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 143
      ControlLabel.Height = 14
      ControlLabel.Caption = #1057#1084#1077#1097#1077#1085#1080#1077' '#1094#1077#1085#1090#1088#1072' '#1090#1103#1078#1077#1089#1090#1080
      ControlLabel.Visible = True
      DataField = 'weight_shift'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      Visible = True
    end
    object edWeightFact: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 605
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 57
      ControlLabel.Height = 14
      ControlLabel.Caption = #1042#1077#1089' '#1073#1088#1091#1090#1090#1086
      ControlLabel.Visible = True
      DataField = 'weight_fact'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      Visible = True
    end
    object edContainerWeight: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 560
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 86
      ControlLabel.Height = 14
      ControlLabel.Caption = #1042#1077#1089' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
      ControlLabel.Visible = True
      DataField = 'container_weight'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 5
      Visible = True
    end
    object edCarrying: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 515
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 98
      ControlLabel.Height = 14
      ControlLabel.Caption = #1043#1088#1091#1079#1086#1087#1086#1076#1100#1077#1084#1085#1086#1089#1090#1100
      ControlLabel.Visible = True
      DataField = 'carrying'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 6
      Visible = True
    end
    object edDateStriker: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 470
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 92
      ControlLabel.Height = 14
      ControlLabel.Caption = #1044#1072#1090#1072' '#1089#1083#1077#1076'.'#1086#1089#1074#1080#1076'.'
      ControlLabel.Visible = True
      DataField = 'date_stiker'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 7
      Visible = True
    end
    object edDateMade: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 425
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 101
      ControlLabel.Height = 14
      ControlLabel.Caption = #1044#1072#1090#1072' '#1080#1079#1075#1086#1090#1086#1074#1083#1077#1085#1080#1103
      ControlLabel.Visible = True
      DataField = 'date_made'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 8
      Visible = True
    end
    object edOwner: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 335
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 66
      ControlLabel.Height = 14
      ControlLabel.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
      ControlLabel.Visible = True
      DataField = 'owner'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 9
      Visible = True
    end
    object edInPlaceStart: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 830
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 187
      ControlLabel.Height = 14
      ControlLabel.Caption = #1053#1072#1095#1072#1083#1086' '#1085#1072#1093#1086#1078#1076#1077#1085#1080#1103' '#1085#1072' '#1090#1077#1088#1084#1080#1085#1072#1083#1077
      ControlLabel.Visible = True
      DataField = 'stay_start_date'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 10
      Visible = True
    end
    object edDlvStart: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 785
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 148
      ControlLabel.Height = 14
      ControlLabel.Caption = #1053#1072#1095#1072#1083#1086' '#1090#1077#1082#1091#1097#1077#1075#1086' '#1087#1088#1086#1094#1077#1089#1089#1072
      ControlLabel.Visible = True
      DataField = 'start_date'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 11
      Visible = True
    end
    object edEmptyLoaded: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 290
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 116
      ControlLabel.Height = 14
      ControlLabel.Caption = #1055#1086#1088#1086#1078#1085#1080#1081'/'#1043#1088#1091#1078#1077#1085#1085#1099#1081
      ControlLabel.Visible = True
      DataField = 'loaded'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 12
      Visible = True
    end
    object edKind: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 245
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 17
      ControlLabel.Height = 14
      ControlLabel.Caption = #1058#1080#1087
      ControlLabel.Visible = True
      DataField = 'kind_code'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 13
      Visible = True
    end
    object edState: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 65
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 55
      ControlLabel.Height = 14
      ControlLabel.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
      ControlLabel.Visible = True
      DataField = 'inplace_state_code'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 14
      Visible = True
    end
    object edNextState: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 155
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 120
      ControlLabel.Height = 14
      ControlLabel.Caption = #1057#1083#1077#1076#1091#1102#1097#1077#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
      ControlLabel.Visible = True
      DataField = 'next_state_code'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 15
      Visible = True
    end
    object edFormalState: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 110
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 126
      ControlLabel.Height = 14
      ControlLabel.Caption = #1060#1086#1088#1084#1072#1083#1100#1085#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
      ControlLabel.Visible = True
      DataField = 'formal_state_code'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 16
      Visible = True
    end
    object edCargoType: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 380
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 16119285
      ControlLabel.Width = 48
      ControlLabel.Height = 14
      ControlLabel.Caption = #1058#1080#1087' '#1075#1088#1091#1079#1072
      ControlLabel.Visible = True
      DataField = 'cargotype'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 17
      Visible = True
    end
    object edCarData: TDBEditEh
      AlignWithMargins = True
      Left = 3
      Top = 200
      Width = 123
      Height = 22
      Margins.Top = 20
      Align = alTop
      Alignment = taLeftJustify
      Color = 13826815
      ControlLabel.Width = 43
      ControlLabel.Height = 14
      ControlLabel.Caption = #1055#1088#1086#1087#1091#1089#1082
      ControlLabel.Visible = True
      DataField = 'pass_data'
      DataSource = dsContInfo
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      StyleElements = [seFont, seBorder]
      TabOrder = 18
      Visible = True
    end
  end
  object drvSector: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'declare @sql varchar(MAX)'
      'select @sql = dbo.BuildMatrixQuery( :sectorid, -1,-1)'
      'exec (@sql)'
      '')
    SelectCommand.Parameters = <
      item
        Name = 'sectorid'
        DataType = ftInteger
        Size = -1
        Value = 1
      end>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <
      item
        Name = 'container_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'mark'
        Size = -1
        Value = Null
      end>
    DeleteCommand.Parameters = <
      item
        Name = 'container_id'
        Size = -1
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'declare @sql varchar(MAX)'
      
        'select @sql = dbo.BuildMatrixQuery( :sectorid, :row_num, :level_' +
        'num)'
      'exec (@sql)')
    GetrecCommand.Parameters = <
      item
        Name = 'sectorid'
        Size = -1
        Value = Null
      end
      item
        Name = 'row_num'
        Size = -1
        Value = Null
      end
      item
        Name = 'level_num'
        Size = -1
        Value = Null
      end>
    Left = 368
    Top = 176
  end
  object meSector: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvSector
    AfterScroll = meSectorAfterScroll
    Left = 416
    Top = 176
  end
  object dsSector: TDataSource
    DataSet = meSector
    Left = 472
    Top = 176
  end
  object drvContInfo: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select top 1 * from v_containermatrixinfo '
      'where cnum = :cnum'
      'order by stay_start_date desc;')
    SelectCommand.Parameters = <
      item
        Name = 'cnum'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.CommandText.Strings = (
      'declare @sql varchar(MAX)'
      
        'select @sql = dbo.BuildMatrixQuery( :sectorid, :row_num, :level_' +
        'num)'
      'exec (@sql)')
    GetrecCommand.Parameters = <
      item
        Name = 'sectorid'
        Size = -1
        Value = Null
      end
      item
        Name = 'row_num'
        Size = -1
        Value = Null
      end
      item
        Name = 'level_num'
        Size = -1
        Value = Null
      end>
    Left = 360
    Top = 256
  end
  object meContInfo: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvContInfo
    ReadOnly = True
    Left = 408
    Top = 256
  end
  object dsContInfo: TDataSource
    DataSet = meContInfo
    Left = 464
    Top = 256
  end
  object spPlaceContainer: TADOStoredProc
    Connection = dm.connMain
    ProcedureName = 'MatrixPlaceContainer'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Operation'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ContainerId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SectorId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@RowNum'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@StackNum'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LevelNum'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CellNum'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 359
    Top = 344
  end
  object pmGrid: TPopupMenu
    Left = 64
    Top = 128
    object N5: TMenuItem
      Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100
      ShortCut = 16472
      OnClick = N5Click
    end
    object N11: TMenuItem
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100
      Enabled = False
      ShortCut = 16470
      OnClick = N11Click
    end
    object N6: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1086#1095#1077#1088#1077#1076#1080' '#1074#1099#1074#1086#1079#1072
      Visible = False
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object N8: TMenuItem
      Caption = #1055#1086#1084#1077#1090#1080#1090#1100': '#1042' '#1086#1095#1077#1088#1077#1076#1080' '#1085#1072' '#1074#1099#1076#1072#1095#1091
      OnClick = N8Click
    end
    object N9: TMenuItem
      Caption = #1055#1086#1084#1077#1090#1080#1090#1100': '#1047#1072#1087#1088#1077#1090' '#1074#1099#1076#1072#1095#1080'/'#1085#1072' '#1089#1086#1075#1083#1072#1089#1086#1074#1072#1085#1080#1080
      OnClick = N9Click
    end
    object N10: TMenuItem
      Caption = #1057#1085#1103#1090#1100' '#1086#1090#1084#1077#1090#1082#1091
      OnClick = N10Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 45
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 46
      OnClick = N3Click
    end
    object N1: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ShortCut = 116
      OnClick = N1Click
    end
  end
  object spMatrixMark: TADOStoredProc
    Connection = dm.connMain
    ProcedureName = 'MatrixMark'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@operation'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cnum'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@mark'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 455
    Top = 344
  end
end
