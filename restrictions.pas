﻿unit restrictions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions,
  Vcl.ActnList;

type
  TFormRestrictions = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormRestrictions: TFormRestrictions;

implementation

{$R *.dfm}

uses editcodename;

procedure TFormRestrictions.Init;
begin
  inherited;
  self.formEdit := FormEditCodeName;
end;

end.
