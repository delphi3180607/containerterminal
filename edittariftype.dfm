﻿inherited FormEditTarifType: TFormEditTarifType
  Caption = #1058#1080#1087' '#1090#1072#1088#1080#1092#1072
  ClientHeight = 245
  ClientWidth = 361
  ExplicitWidth = 367
  ExplicitHeight = 273
  PixelsPerInch = 96
  TextHeight = 16
  object Label3: TLabel [2]
    Left = 8
    Top = 133
    Width = 44
    Height = 16
    Caption = #1058#1072#1088#1080#1092
  end
  inherited plBottom: TPanel
    Top = 204
    Width = 361
    TabOrder = 3
    inherited btnCancel: TButton
      Left = 245
    end
    inherited btnOk: TButton
      Left = 126
    end
  end
  object nePrice: TDBNumberEditEh [6]
    Left = 8
    Top = 155
    Width = 121
    Height = 24
    DataField = 'tarif_value'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
end
