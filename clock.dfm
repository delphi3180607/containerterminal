﻿inherited FormClock: TFormClock
  Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1081
  ClientHeight = 409
  ClientWidth = 712
  ExplicitWidth = 728
  ExplicitHeight = 447
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 368
    Width = 712
    ExplicitTop = 368
    ExplicitWidth = 712
    inherited btnOk: TButton
      Left = 477
      ExplicitLeft = 477
    end
    inherited btnCancel: TButton
      Left = 596
      ExplicitLeft = 596
    end
  end
  inherited plAll: TPanel
    Width = 712
    Height = 368
    ExplicitWidth = 712
    ExplicitHeight = 368
    inherited plTop: TPanel
      Width = 712
      ExplicitWidth = 712
      inherited btTool: TPngSpeedButton
        Left = 480
        ExplicitLeft = 660
      end
      inherited plCount: TPanel
        Left = 518
      end
    end
    inherited dgData: TDBGridEh
      Width = 708
      Height = 335
      OnDblClick = nil
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'event_type'
          Footers = <>
          Title.Caption = #1042#1080#1076' '#1089#1086#1073#1099#1090#1080#1103
          Width = 109
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'event_datetime'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103
          Width = 127
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'user_name'
          Footers = <>
          Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
          Width = 344
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'select l.event_type, l.event_datetime, u.user_name from change_l' +
        'og l, users u '
      
        'where l.user_id =u.id and l.table_name = :table_name and l.table' +
        '_id = :table_id'
      'order by event_datetime desc')
    SelectCommand.Parameters = <
      item
        Name = 'table_name'
        Attributes = [paSigned, paNullable]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'table_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
    inherited aDelete: TAction
      Enabled = False
    end
    inherited aEdit: TAction
      Enabled = False
    end
  end
end
