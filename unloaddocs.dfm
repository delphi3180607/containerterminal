﻿inherited FormUnloadDocs: TFormUnloadDocs
  ActiveControl = dgData
  Caption = #1056#1072#1079#1075#1088#1091#1079#1082#1072' '#1074#1072#1075#1086#1085#1086#1074
  ClientHeight = 563
  ExplicitHeight = 601
  PixelsPerInch = 96
  TextHeight = 13
  inherited SplitterVert: TSplitter
    Width = 5
    Height = 39
    ExplicitWidth = 5
    ExplicitHeight = 530
  end
  inherited plBottom: TPanel
    Top = 39
    Height = 524
    ExplicitTop = 39
    ExplicitHeight = 524
    inherited btnOk: TButton
      Height = 518
    end
    inherited btnCancel: TButton
      Height = 518
    end
  end
  inherited plAll: TPanel
    Left = 198
    Width = 735
    Height = 39
    ExplicitLeft = 198
    ExplicitWidth = 735
    ExplicitHeight = 39
    inherited SplitterHor: TSplitter
      Left = 545
      Top = 63
      Width = 5
      Height = 219
      Cursor = crHSplit
      Align = alRight
      ExplicitLeft = 550
      ExplicitTop = 63
      ExplicitWidth = 5
      ExplicitHeight = 220
    end
    inherited plTop: TPanel
      Width = 735
      ExplicitWidth = 735
      inherited sbDelete: TPngSpeedButton
        Left = 105
        ExplicitLeft = 118
      end
      inherited btFilter: TPngSpeedButton
        Left = 206
        ExplicitLeft = 185
      end
      inherited btExcel: TPngSpeedButton
        Left = 546
        ExplicitLeft = 571
      end
      inherited sbAdd: TPngSpeedButton
        Left = 73
        Visible = False
        ExplicitLeft = 87
      end
      inherited sbEdit: TPngSpeedButton
        Left = 169
        ExplicitLeft = 153
      end
      inherited Bevel2: TBevel
        Left = 203
        ExplicitLeft = 181
      end
      inherited btTool: TPngSpeedButton
        Left = 537
        ExplicitLeft = 698
      end
      inherited Bevel1: TBevel
        Left = 336
        ExplicitLeft = 304
      end
      inherited btFlow: TPngSpeedButton
        Left = 408
        ExplicitLeft = 393
      end
      inherited sbHistory: TPngSpeedButton
        Left = 440
        ExplicitLeft = 350
        ExplicitHeight = 37
      end
      inherited sbRestrictions: TPngSpeedButton
        Left = 477
        ExplicitLeft = 478
      end
      inherited sbConfirm: TPngSpeedButton
        Left = 339
        ExplicitLeft = 310
      end
      inherited sbCancelConfirm: TPngSpeedButton
        Left = 371
        ExplicitLeft = 349
      end
      inherited Bevel5: TBevel
        Left = 511
        ExplicitLeft = 523
      end
      inherited sbReport: TPngSpeedButton
        Left = 514
        ExplicitLeft = 529
      end
      inherited Bevel6: TBevel
        Left = 405
        ExplicitLeft = 387
      end
      inherited sbImport2: TPngSpeedButton
        Visible = False
      end
      inherited sbSearch: TPngSpeedButton
        Left = 270
        ExplicitLeft = 262
      end
      inherited Bevel7: TBevel
        Left = 474
        ExplicitLeft = 475
      end
      inherited sbClearFilter: TPngSpeedButton
        Left = 238
        ExplicitLeft = 226
      end
      inherited Bevel3: TBevel
        Left = 580
        ExplicitLeft = 612
      end
      inherited sbClock: TPngSpeedButton
        Left = 583
        ExplicitLeft = 616
      end
      inherited sbBarrel: TPngSpeedButton
        Left = 302
        ExplicitLeft = 270
      end
      object sbFilterPlan: TPngSpeedButton [23]
        AlignWithMargins = True
        Left = 33
        Top = 0
        Width = 39
        Height = 26
        Hint = #1054#1090#1073#1086#1088' '#1087#1086' '#1087#1083#1072#1085#1091' '#1087#1086#1075#1088#1091#1079#1082#1080
        Margins.Left = 1
        Margins.Top = 0
        Margins.Right = 1
        Margins.Bottom = 0
        Align = alLeft
        AllowAllUp = True
        GroupIndex = 1
        ParentShowHint = False
        ShowHint = True
        OnClick = sbFilterPlanClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000000473424954080808087C08648800000009704859730000009D000000
          9D018F72E76E0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000002944944415478DAB5945F48537114C7
          CFB97FB6395B7AE76D53A70FB1202DACB77CC940ABA7DE8210A27C11C4963E24
          16081241F4D28B2F454811642991E09F8A9E0249DF8B429C2C4721D97457DDE6
          DADDDDEE9F5FBF4903D36D4EE7BE7039E7C739DFDF8773EFFDFD901002993452
          8E0267B67811C109B9B5002BCAC92B84243315311B6014D1B452C64A9A416CB4
          03B3ED6E6270ED665813B3D5B302D29AA8B2BEF6CB6AEBEFB8B6A326F08CBF3F
          A61FCBE5DF1D506D9DA6A1E9FB1F159695FF21368E59BF27EB1585015CD60520
          E04EE5C62901D8431A4CBF0D6ED62C2C261EC40D4BA113C468B0DA4F94434357
          3DC47F2CC0D487287CFE140216813C4C1066DF8051BBBD8CB728E154CEF00C34
          78EA80D1D7E0CDC0122C47D4CD1ED10262DF0659DB1760BCDA5C8FC0CEA5D729
          08B2006ADC80B98D048454035C66AEA527AA4EED0B305153721E0CFC98A96650
          DB2C8594706CD7ED48F2F19E006335D61AC6804734ADA58F2B9B39753E744256
          3944AFA162F7652916C80BF0C261EE4F6AE43EE4D07617FDA3EEB649891D9E8C
          8021115DB2CE0DD34A193DC9CE240187413FC1F63E0E216261F02783182E65D5
          ABD72492DF045BD563C2767A4F3C5368DBAA0E206F693FCCC093619D7872F977
          05F4F2789620CCA4D7B17FA0048D020B6D431A795910A00F51507918A874DBEA
          1CB5A58D6A2804B2C2FABF78E55F8A0EAD2384AC1404482BF2EE620731F4C1E8
          EC373009F641A7C7D7998F2F6F4068F2421382319D0298EDE275C78DF957070A
          88BE6F16751DA5142059E172BA3BBF060F14B039C5448B24FBBCE5AE3B013E5F
          CF5E0133F4363D5E7D6BD151144078B2F9697C71F17455B7FF4C5100EBE3CDBD
          C9C0527DA5C7D75E9C573476EE92160C3A8F74CE3F2F0A2032DA78540D4515B1
          632E90AFE72FB01C4BE0505107790000000049454E44AE426082}
        ExplicitLeft = 38
        ExplicitTop = -2
        ExplicitHeight = 23
      end
      inherited sbPass: TPngSpeedButton
        Left = 137
      end
      inherited plCount: TPanel
        Left = 574
        ExplicitLeft = 574
        inherited edCount: TDBEditEh
          ControlLabel.ExplicitLeft = 0
          ControlLabel.ExplicitTop = -16
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 545
      Height = 219
      STFilter.HorzLineColor = clSkyBlue
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 70
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doctype_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 78
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 101
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 89
        end
        item
          CellButtons = <>
          DisplayFormat = '##%'
          DynProps = <>
          EditButtons = <>
          FieldName = 'usedproc'
          Footers = <>
          Title.Caption = #1042#1099#1074#1086#1079'., %'
          Width = 60
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'person_code'
          Footers = <>
          Title.Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1081
          Width = 127
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'alert_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1091#1074#1077#1076#1086#1084#1083#1077#1085#1080#1103
          Width = 101
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_created'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072'.'#1089#1086#1079'.'
          Width = 83
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'user_created'
          Footers = <>
          Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
          Width = 140
        end>
    end
    inherited plHint: TPanel
      TabOrder = 6
    end
    inherited plStatus: TPanel
      Width = 735
      TabOrder = 5
      ExplicitWidth = 735
    end
    inherited plLinkedObjects: TPanel
      Left = 553
      Top = 66
      Width = 179
      Height = 213
      Align = alRight
      ExplicitLeft = 553
      ExplicitTop = 66
      ExplicitWidth = 179
      ExplicitHeight = 213
      inherited dgLinkedObjects: TDBGridEh
        Width = 179
        Height = 213
        Border.Color = clGray
        Border.EdgeBorders = [ebTop]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'rus_object_type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1086#1073#1098#1077#1082#1090#1072
            Width = 86
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'cnum'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088
            Width = 102
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'kind_code'
            Footers = <>
            Title.Caption = #1042#1080#1076
            Width = 101
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'owner_code'
            Footers = <>
            Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
            Width = 100
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'state_code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = [fsItalic]
            Footers = <>
            Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 109
          end>
      end
    end
    inherited plSearch: TPanel
      Width = 735
      ExplicitWidth = 735
      inherited edSearch: TEdit
        Width = 553
        ExplicitWidth = 553
      end
    end
    object dgSpec: TDBGridEh
      Left = 0
      Top = -203
      Width = 735
      Height = 242
      Align = alBottom
      BorderStyle = bsNone
      DataSource = dsSpec
      DynProps = <>
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
      ParentFont = False
      PopupMenu = pmSpec
      ReadOnly = True
      SelectionDrawParams.DrawFocusFrame = True
      SelectionDrawParams.DrawFocusFrameStored = True
      TabOrder = 2
      TitleParams.FillStyle = cfstGradientEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -11
      TitleParams.Font.Name = 'Verdana'
      TitleParams.Font.Style = [fsBold]
      TitleParams.ParentFont = False
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'basedoc_number'
          Footers = <>
          Title.Caption = #1044#1086#1082'.'#1086#1089#1085'.'
          Width = 76
        end
        item
          CellButtons = <>
          DisplayFormat = 'dd.mm.yyyy'
          DynProps = <>
          EditButtons = <>
          FieldName = 'basedoc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1076#1086#1082'.'#1086#1089#1085'.'
          Width = 94
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnum'
          Footers = <>
          Title.Caption = #8470' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
          Width = 110
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'size'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1060#1091#1090'.'
          Width = 59
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'seal_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1082#1083#1077#1097#1072
          Width = 100
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_doc'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1042#1077#1089' '#1046#1044
          Width = 56
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_fact'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1042#1077#1089' '#1092#1072#1082#1090
          Width = 68
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_code'
          Footers = <>
          Title.Caption = #8470' '#1087#1083#1072#1090#1092#1086#1088#1084#1099
          Width = 100
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 112
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dlvtype_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
          Width = 160
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  inherited plLeft: TPanel
    Height = 39
    inherited Bevel4: TBevel
      Height = 14
      ExplicitHeight = 452
    end
    inherited dgFolders: TDBGridEh
      Height = 14
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'declare @planid int;'
      ''
      'select @planid = :planid;'
      ''
      
        'select d.*, h.*, (case when d.isconfirmed = 1 then '#39#39' else '#39#39' en' +
        'd) as state_code,'
      
        '(select code from doctypes t where t.id = d.doctype_id) as docty' +
        'pe_code,'
      'dbo.f_GetDocUsedProcs(d.id,'#39#1040#1074#1090#1086#1076#1086#1089#1090#1072#1074#1082#1072#39') as usedproc,'
      
        '(select person_name from persons p where p.id = h.person_id) as ' +
        'person_code, '
      
        '(select user_name from users u where u.id = d.user_created) as u' +
        'ser_created'
      'from docunload h, documents d '
      'where d.id = h.id and (1 = :showall or d.folder_id = :folder_id)'
      
        'and (dbo.filter_documents(:userid, d.doctype_id, d.id) = 3 or is' +
        'null(@planid,0) <> 0)'
      'and (isnull(@planid,0) = 0 or k.load_plan_id  = @planid)'
      'order by d.doc_date')
    SelectCommand.Parameters = <
      item
        Name = 'planid'
        Size = -1
        Value = Null
      end
      item
        Name = 'showall'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'folder_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'userid'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update docunload'
      'set'
      '  doc_base = :doc_base,'
      '  person_id = :person_id,'
      '  plan_date = :plan_date,'
      '  fact_date = :fact_date,'
      '  alert_date = :alert_date'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'doc_base'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'person_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'plan_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'fact_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'alert_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'begin'
      ''
      '  declare @newid int, @doctypeid int;'
      ''
      
        '  select @doctypeid = id from doctypes where system_section='#39'inc' +
        'ome_unload'#39';'
      ''
      
        '  insert into documents (doctype_id, folder_id) values (@doctype' +
        'id, :folder_id);'
      ''
      '  select @newid = IDENT_CURRENT('#39'documents'#39');'
      ''
      '  insert into docunload'
      '    (id, doc_base, person_id, plan_date, fact_date, alert_date)'
      '  values'
      
        '    (@newid, :doc_base, :person_id, :plan_date, :fact_date, :ale' +
        'rt_date);'
      ''
      'end;')
    InsertCommand.Parameters = <
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_base'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'person_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'plan_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'fact_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'alert_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      
        'delete from documents where id = :id and isnull(isconfirmed,0)<>' +
        '1')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select d.*, h.*, (case when d.isconfirmed = 1 then '#39#39' else '#39#39' en' +
        'd) as state_code,'
      
        '(select code from doctypes t where t.id = d.doctype_id) as docty' +
        'pe_code,'
      'dbo.f_GetDocUsedProcs(d.id,'#39#1040#1074#1090#1086#1076#1086#1089#1090#1072#1074#1082#1072#39') as usedproc,'
      
        '(select person_name from persons p where p.id = h.person_id) as ' +
        'person_code, '
      
        '(select user_name from users u where u.id = d.user_created) as u' +
        'ser_created'
      'from docunload h, documents d '
      'where d.id = h.id '
      'and d.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited meData: TMemTableEh
    AfterOpen = meDataAfterOpen
  end
  inherited pmDocFlow: TPopupMenu
    object N17: TMenuItem [3]
      Caption = #1055#1088#1086#1089#1090#1072#1074#1080#1090#1100' '#1076#1072#1090#1091' '#1091#1074#1077#1076#1086#1084#1083#1077#1085#1080#1103
      ShortCut = 16452
      OnClick = N17Click
    end
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
  end
  inherited drvLinkedObjects: TADODataDriverEh
    Left = 776
    Top = 132
  end
  inherited meLinkedObjects: TMemTableEh
    Left = 824
    Top = 132
  end
  inherited dsLinkedObjects: TDataSource
    Left = 880
    Top = 132
  end
  inherited pmLinkedObjects: TPopupMenu
    Left = 696
    Top = 136
  end
  inherited IL: TPngImageList
    Bitmap = {}
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select ds.doc_id, o.id, o.object_type, o.cnum,  c.size ,g.weight' +
        '_doc, g.weight_fact, g.isempty, g.seal_number,'
      
        '(select o1.cnum from v_objects o1 where o1.id = ds.linked_object' +
        '_id and o1.object_type = '#39'carriage'#39')  as car_code,'
      
        '(select code from counteragents c where c.id = o.owner_id) as ow' +
        'ner_code, '
      
        '(select code from objectstatekinds k where k.id = o.state_id) as' +
        ' state_code,'
      
        '(select code from deliverytypes dt where t.dlv_type_id = dt.id) ' +
        'as dlvtype_code, '
      
        't.dlv_type_id, o.current_task_id, t.basedoc_number, t.basedoc_da' +
        'te'
      'from v_objects o, tasks t, objects2docspec ds, '
      'containers c left outer join '
      
        '(select o2.linked_object_id, o2.current_task_id,  g2.* from carg' +
        'os g2, objects o2 where o2.id = g2.id) as g '
      'on  (g.linked_object_id = c.id)'
      
        'where o.id = ds.object_id and c.id = o.id and ds.doc_id = :doc_i' +
        'd and t.id = o.current_task_id and g.current_task_id = t.id;'
      ''
      '')
    SelectCommand.Parameters = <
      item
        Name = 'doc_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      '')
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 336
    Top = 404
  end
  object meSpec: TMemTableEh
    Params = <>
    DataDriver = drvSpec
    TreeList.DefaultNodeExpanded = True
    Left = 384
    Top = 404
  end
  object dsSpec: TDataSource
    DataSet = meSpec
    Left = 440
    Top = 404
  end
  object pmSpec: TPopupMenu
    Left = 232
    Top = 400
    object MenuItem14: TMenuItem
      Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
      ShortCut = 16453
      OnClick = MenuItem14Click
    end
    object N15: TMenuItem
      Caption = '-'
    end
    object N16: TMenuItem
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1090#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
      OnClick = sbChangeClick
    end
  end
end
