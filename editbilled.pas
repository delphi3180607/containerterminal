﻿unit editbilled;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, DBCtrlsEh, Vcl.Mask,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, MemTableDataEh, MemTableEh, Vcl.Menus,
  Vcl.Buttons;

type
  TFormEditBilled = class(TFormEdit)
    edDocNumber: TDBEditEh;
    edDocDate: TDBDateTimeEditEh;
    nuDocSumma: TDBNumberEditEh;
    pcLayout: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    meNumbers: TMemo;
    plTool: TPanel;
    btSeek: TButton;
    btCheckAll: TButton;
    btUnCheckAll: TButton;
    meData: TMemTableEh;
    dgData: TDBGridEh;
    Label1: TLabel;
    pmMemo: TPopupMenu;
    N1: TMenuItem;
    sbDistrib: TSpeedButton;
    procedure btSeekClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btCheckAllClick(Sender: TObject);
    procedure btUnCheckAllClick(Sender: TObject);
    procedure dgDataSelectedRowsItemChanged(Sender: TCustomDBGridEh;
      Item: TArray<System.Byte>; Action: TListNotification);
    procedure meNumbersChange(Sender: TObject);
    procedure nuDocSummaExit(Sender: TObject);
    procedure sbDistribClick(Sender: TObject);
  private
    g: string;
  public
    procedure DistribSumma;
  end;

var
  FormEditBilled: TFormEditBilled;

implementation

{$R *.dfm}

uses dmu;

procedure TFormEditBilled.btCheckAllClick(Sender: TObject);
begin
  dgData.SelectedRows.SelectAll
end;

procedure TFormEditBilled.btnOkClick(Sender: TObject);
begin

  if not meData.Active then
  begin
    ShowMessage('Вы не распределили сумму по заявкам. Нажмите "Найти заявки".');
    self.ModalResult := mrNone;
    exit;
  end;

  if (trim(edDocNumber.Text) = '')
  or (edDocDate.Value = null)
  or (nuDocSumma.Value = null)
  or (nuDocSumma.Value = 0)
  then
  begin
    ShowMessage('Для привязки счета необходимо указать Номер, Дату и Сумму счета.');
    self.ModalResult := mrNone;
    exit;
  end;

  // сбрасываем таблицу с фильтром
  dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
  dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
  dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := '';
  dm.spCreateFilter.ExecProc;

  qrAux.Close;
  g := '';

  meData.First;
  while not meData.Eof do
  begin
    if not dgData.SelectedRows.CurrentRowSelected then
    begin
      meData.Edit;
      meData.FieldByName('Summa').Value := 0;
      meData.Post;
    end;
    meData.Next;
  end;

  inherited;
end;

procedure TFormEditBilled.btSeekClick(Sender: TObject);
var gu: TGUID; i: integer; conditions: string;
begin

    qrAux.Close;
    conditions := '';

    if (meNumbers.Lines.Count = 0)
    or (trim(meNumbers.Lines.Text) = '')
    then
    begin
      ShowMessage('Укажите, хотя бы один номер контейнера.');
      exit;
    end;

    for i := 0 to meNumbers.Lines.Count-1 do
    begin
     conditions := conditions +';object_num='+trim(meNumbers.Lines[i]);
    end;


    CreateGuid(gu);
    g := GuidToString(gu);

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := conditions;
    dm.spCreateFilter.ExecProc;

    qrAux.Parameters.ParamByName('guid').Value := g;
    Screen.Cursor := crHourGlass;
    qrAux.Open;
    meData.Close;
    meData.LoadFromDataSet(qrAux,0,lmCopy,false);
    meData.Open;

    DistribSumma;

    pcLayout.ActivePageIndex := 1;
    Screen.Cursor := crDefault;


end;

procedure TFormEditBilled.DistribSumma;
var rc: integer;
begin

    rc := meData.RecordCount;


    while not meData.Eof do
    begin
      meData.Edit;
      meData.FieldByName('Summa').Value := nuDocSumma.Value/rc;
      meData.Post;
      meData.Next;
    end;

    meData.First;

    if rc = 1 then
    begin
      dgData.SelectedRows.SelectAll;
    end;

end;


procedure TFormEditBilled.btUnCheckAllClick(Sender: TObject);
begin
  dgData.SelectedRows.Clear;
end;

procedure TFormEditBilled.dgDataSelectedRowsItemChanged(Sender: TCustomDBGridEh; Item: TArray<System.Byte>; Action: TListNotification);
var b: TBookmark; i,rc: integer;
begin

    if not meData.Active then exit;

    b := meData.Bookmark;
    meData.DisableControls;
    meData.First;

    rc := dgData.SelectedRows.Count;

    while not meData.Eof do
    begin
      meData.Edit;
      meData.FieldByName('Summa').Value := 0;
      meData.Post;
      meData.Next;
    end;

    meData.First;
    for i := 0 to dgData.SelectedRows.Count-1 do
    begin
      meData.Bookmark := dgData.SelectedRows.Items[i];
      meData.Edit;
      meData.FieldByName('Summa').Value := nuDocSumma.Value/rc;
      meData.Post;
      meData.Next;
    end;

    meData.Bookmark := b;
    meData.EnableControls;

end;

procedure TFormEditBilled.meNumbersChange(Sender: TObject);
begin
  meData.Close;
  qrAux.Close;
end;

procedure TFormEditBilled.nuDocSummaExit(Sender: TObject);
begin
  self.DistribSumma;
end;

procedure TFormEditBilled.sbDistribClick(Sender: TObject);
begin
  self.DistribSumma;
end;

end.
