﻿unit FilterChessExtra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, filterlite, Vcl.Mask, DBCtrlsEh,
  Vcl.StdCtrls, Vcl.CheckLst, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls, Vcl.Buttons,
  DBSQLLookUp;

type
  TFormFilterChessExtra = class(TFormFilterLite)
    cbCarKinds: TCheckListBox;
    Label1: TLabel;
    luStation: TDBSQLLookUp;
    sbAdd: TSpeedButton;
    Label2: TLabel;
    ssStations: TADOLookUpSqlSet;
    lbStations: TListBox;
    sbMinus: TSpeedButton;
    procedure cbCarKindsClickCheck(Sender: TObject);
    procedure sbAddClick(Sender: TObject);
    procedure sbMinusClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure luStationKeyValueChange(Sender: TObject);
    procedure btClearClick(Sender: TObject);
  private
    { Private declarations }
  public
    stationsids: TStringList;
    slCarKinds: TStringList;
    carkinds: string;
    procedure Init;
  end;

var
  FormFilterChessExtra: TFormFilterChessExtra;

implementation

{$R *.dfm}


procedure TFormFilterChessExtra.btClearClick(Sender: TObject);
begin
  inherited;
  self.carkinds := '';
  lbStations.Clear;
  self.stationsids.Clear;
  luStation.KeyValue := null;
  luStation.Text := '';
end;

procedure TFormFilterChessExtra.cbCarKindsClickCheck(Sender: TObject);
var i:integer;
begin
  self.carkinds := '';
  for i := 0 to cbCarKinds.Items.Count-1 do
  begin
    if cbCarKinds.Checked[i] then
    begin
      self.carkinds := self.carkinds + ',' + slCarKinds[i];
    end;
  end;
end;

procedure TFormFilterChessExtra.FormCreate(Sender: TObject);
begin
  inherited;
  stationsids := TStringList.Create;
  stationsids.Delimiter := ',';
  slCarKinds := TStringList.Create;
end;

procedure TFormFilterChessExtra.Init;
var i: integer;
begin

  slCarKinds.Clear;
  cbCarKinds.Items.Clear;

  qrAux.Close;
  qrAux.Open;

  while not qrAux.Eof do
  begin

    i:= cbCarKinds.Items.Add(qrAux.Fields[1].AsString);
    slCarKinds.Add(qrAux.Fields[0].AsString);

    if pos(qrAux.Fields[0].AsString, self.carkinds)>0 then
      cbCarKinds.Checked[i] := true;

    qrAux.Next;

  end;

end;

procedure TFormFilterChessExtra.luStationKeyValueChange(Sender: TObject);
begin
  if lbStations.Items.Count>0 then exit;
  if luStation.KeyValue>0 then
  begin
    lbStations.Items.Add(luStation.DisplayValue);
    self.stationsids.Add(VarToStr(luStation.KeyValue));
  end;
end;

procedure TFormFilterChessExtra.sbAddClick(Sender: TObject);
begin
  lbStations.Items.Add(luStation.Text);
  self.stationsids.Add(VarToStr(luStation.KeyValue));
end;

procedure TFormFilterChessExtra.sbMinusClick(Sender: TObject);
begin
  if lbStations.Items.Count<=0 then exit;
  if lbStations.ItemIndex < 0 then lbStations.ItemIndex := 0;
  self.stationsids.Delete(lbStations.ItemIndex);
  lbStations.DeleteSelected;
end;

end.
