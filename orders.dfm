﻿inherited FormDocOrders: TFormDocOrders
  ActiveControl = dgData
  Caption = #1047#1072#1103#1074#1082#1080
  ClientHeight = 649
  ClientWidth = 925
  ExplicitWidth = 941
  ExplicitHeight = 687
  PixelsPerInch = 96
  TextHeight = 13
  inherited SplitterVert: TSplitter
    Left = 201
    Height = 616
    ExplicitLeft = 145
    ExplicitHeight = 616
  end
  inherited plBottom: TPanel
    Top = 616
    Width = 925
    ExplicitTop = 616
    ExplicitWidth = 925
    inherited btnOk: TButton
      Left = 690
      ExplicitLeft = 690
    end
    inherited btnCancel: TButton
      Left = 809
      ExplicitLeft = 809
    end
  end
  inherited plAll: TPanel
    Left = 208
    Width = 717
    Height = 616
    Caption = ''
    ExplicitLeft = 208
    ExplicitWidth = 717
    ExplicitHeight = 616
    inherited SplitterHor: TSplitter
      Left = 526
      Top = 63
      Width = 6
      Height = 212
      Cursor = crHSplit
      Align = alRight
      ExplicitLeft = 526
      ExplicitTop = 64
      ExplicitWidth = 6
      ExplicitHeight = 211
    end
    object Splitter1: TSplitter [1]
      Left = 0
      Top = 292
      Width = 717
      Height = 7
      Cursor = crVSplit
      Align = alBottom
      StyleElements = [seFont, seBorder]
      ExplicitTop = 293
    end
    inherited plTop: TPanel
      Width = 717
      ExplicitWidth = 717
      inherited sbDelete: TPngSpeedButton
        ExplicitLeft = 82
      end
      inherited btFilter: TPngSpeedButton
        ExplicitLeft = 167
      end
      inherited btExcel: TPngSpeedButton
        Left = 537
        ExplicitLeft = 555
      end
      inherited sbAdd: TPngSpeedButton
        ExplicitLeft = 46
      end
      inherited Bevel2: TBevel
        ExplicitLeft = 162
      end
      inherited btTool: TPngSpeedButton
        Left = 519
        ExplicitLeft = 795
      end
      inherited Bevel1: TBevel
        ExplicitLeft = 256
      end
      inherited sbHistory: TPngSpeedButton
        ExplicitLeft = 394
      end
      inherited sbRestrictions: TPngSpeedButton
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000002FB4944415478DAA5934B4C534114864F5B4AA105FABCAD
          94A0506C29E5214408045DE20B420C3E10830BA31B4104C3CA08C4C4882C342C
          4801976C30A81042340D12E2C22858E419012B06852250A1256D6969692FBDCE
          4C29A971E9242773EFDCF9BF7BFE3367580CC340F838FD0C94684A4041A110EE
          2F3B506CA2581DBA056BE1FB59210012C6A0499B77C8999FAB70EA947C6FB680
          4B6BF177E72EDBB4E6E24D4F6C08E727AD6223DA674220D701605F9C733363BD
          4827F354C8299946C08F86285E04817BBC7ED876B9C1FC6B6DC1648BEE79BEA8
          1A46CB53181202E422716906E5AD4F494A8CC1428F6F0FBCBE00E0FC785C3608
          781C70797C30336B722148EB8B25F56B0418679DEA649428EDB20AADB5569D7C
          5883C556A70F0248192A0FC6E067A5984720A3C6C905C35A72DB9C53D18F0179
          55592BD70B5385D50A691C1157E9C780924AE1C1951422AED11B41269341CB35
          354862B8F06DC902C3E3CB1D03D6FC2E0C287E58F0BD212335A990C566A33FEC
          C1EDF63190CB24D0589E02D8626D070650F0B8520D5C0E0B767D7E300CBD1BE9
          DE2A69C680AB4F4FCEEB75BA3489C34D03BDC740B5FE1350940C1A2FA98885BA
          762348D13B0678FD01900B23A1B7B777ABCB5E5E43004F4ECCE9D3D37512BBCB
          0F3E04B8D31E4CB9E9B28AD4E26EFB2802C889851D5458119F03FD7D2FB7BADD
          950450DC94FBB52153AB2AE47038E0D8A1A1AE3308B87F51458A57DF89010A68
          DEB76073B8E1EDE0E0C880BF8258C8BB91BA888A28AA4E8C97C28ACD0B8F5E2D
          92D4EF5D085A68E9FB4132C11624B15CF8386182F7534B1D1F9873A488CA2CB1
          B5AC24E167EDB1AC4C8D242E8A9C4468D8515D62A3830D85C5668B030C86370B
          338182363373B4FFA091CEC77F293D2270D4E71DCF89918B04C48AD3439353C0
          001CCB163B1806875C16AFB0759C7336D848E1AD7C46F4B94811F1BB42AD49D3
          C42B2810C7F1C9A9586CDB605E5985D9998985CD80BC67925BFC772B875FA694
          C8E5FC84804927A037B223991D2D4DA3966678260748A6D72374F3ABECD47F2F
          D3FF5CE73FDBCD7BC47F4F9B730000000049454E44AE426082}
        ExplicitLeft = 456
      end
      inherited sbConfirm: TPngSpeedButton
        ExplicitLeft = 270
      end
      inherited sbCancelConfirm: TPngSpeedButton
        ExplicitLeft = 309
      end
      inherited Bevel5: TBevel
        Left = 502
        ExplicitLeft = 502
      end
      inherited sbReport: TPngSpeedButton
        Left = 505
        ExplicitLeft = 509
      end
      inherited Bevel6: TBevel
        ExplicitLeft = 348
      end
      inherited sbImport2: TPngSpeedButton
        Visible = False
        OnClick = sbImport2Click
      end
      inherited Bevel7: TBevel
        ExplicitLeft = 452
      end
      inherited sbClearFilter: TPngSpeedButton
        ExplicitLeft = 165
        ExplicitTop = -3
        ExplicitHeight = 24
      end
      inherited Bevel3: TBevel
        Left = 571
        ExplicitLeft = 585
      end
      inherited sbClock: TPngSpeedButton
        Left = 574
        ExplicitLeft = 589
      end
      inherited sbBarrel: TPngSpeedButton
        ExplicitLeft = 351
      end
      inherited sbPass: TPngSpeedButton
        Visible = True
      end
      object sbNote: TPngSpeedButton [24]
        AlignWithMargins = True
        Left = 468
        Top = 0
        Width = 32
        Height = 24
        Hint = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        OnClick = sbNoteClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000001F14944415478DA63FCFFFF3F032580916A064476EC1402
          529381380488D9F0E8D90DC459CB2BDCEFA01BB031D04AC92FC85A9981859909
          A7EEADA71E302CD97713A4591B68C82FB00140CD3C40810F4BCBDD983FFF6060
          F8F98781E12FD0DC7FFF20F45F10FDEF3F03235091B2281343CDC2130C779F7F
          34071A700A66809C081FC7C3C959F60C2F3FFD67F8F71F4923D8A0FF0C7FFE41
          0CD4926462E85A7D8EE1FCDDD73E4003B66218F0ECE37FB0C67F4836235CC1C0
          A0278DDD00B817DE7C6160F8F60B64F37FB82698018C0CFF81063033D42D3EC9
          70FBE9072BA001C7310231C45685818991111E688FDFFF6778F7F53F83243F23
          83182F237220EA010DF88E2D1AC381981924E66A28CB90E4AE0596DF746C0DC3
          FE8B9B19BEFDF8C8F0F9DBA7AF1FBEBCEDD8D67EB1056F42021A7AA73FDD5679
          F6D6790CFF39CE31186B9A30C808A932ECBFBA81E1C495C30C0FEEBCE9C56900
          5073AFA3BE4CD1FE8B4FBEB232F4B187F878B13000D3879F662E43EFDE34A013
          991856ACDFF403AB0140CD4A406A0F10B701FD39C7B7C6E87F56643683A77632
          5CCDE6CBD3185A67B6109717AC7265BE07F9B973FC61F8C750E13A9FA1635722
          0307333B6E17A0038B1CA9161945FE6A2B3D1B0655094386DB2FCE331CBB7484
          E1C9FD8FBD44E746A0211DA04C04C4BC40FC1988A79D98F2AC020020201EABD5
          9EF0790000000049454E44AE426082}
        ExplicitLeft = 483
        ExplicitTop = -3
      end
      inherited plCount: TPanel
        Left = 556
        ExplicitLeft = 556
        inherited edCount: TDBEditEh
          ControlLabel.ExplicitLeft = 0
          ControlLabel.ExplicitTop = -16
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 526
      Height = 212
      DrawGraphicData = True
      GridLineParams.BrightColor = 15395041
      GridLineParams.DarkColor = 15395041
      GridLineParams.DataBoundaryColor = 15395041
      TitleParams.MultiTitle = True
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 61
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          Color = 16623272
          DynProps = <>
          EditButtons = <>
          FieldName = 'pass_number'
          Footers = <>
          Title.Caption = #1055#1088#1086' '#1087#1091#1089#1082
          Width = 51
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doctype_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 69
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'delivery_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1086#1087#1077#1088#1072#1094#1080#1080
          Width = 111
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 72
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 70
        end
        item
          CellButtons = <>
          Checkboxes = False
          DisplayFormat = '##%'
          DynProps = <>
          EditButtons = <>
          FieldName = 'usedproc'
          Footers = <>
          Title.Caption = '% '#1087#1088#1080#1085'.'
          Width = 36
        end
        item
          CellButtons = <>
          DisplayFormat = '##%'
          DynProps = <>
          EditButtons = <>
          FieldName = 'ldproc'
          Footers = <>
          Title.Caption = '% '#1087#1086#1075#1088'.'
          Width = 39
        end
        item
          CellButtons = <>
          DisplayFormat = '##%'
          DynProps = <>
          EditButtons = <>
          FieldName = 'uplproc'
          Footers = <>
          Title.Caption = '% '#1086#1090#1087#1088'.'
          Width = 41
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'station_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1085#1094#1080#1103
          Width = 102
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'direction_code'
          Footers = <>
          Title.Caption = #1055#1086#1088#1090
          Width = 92
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'forwarder_code'
          Footers = <>
          Title.Caption = #1050#1083#1080#1077#1085#1090
          Width = 119
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 121
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer_code'
          Footers = <>
          Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
          Width = 94
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'shippingoption_code'
          Footers = <>
          Title.Caption = #1042#1072#1088#1080#1072#1085#1090' '#1076#1086#1089#1090#1072#1074#1082#1080
          Width = 110
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_created'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103
          Width = 65
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'user_created'
          Footers = <>
          Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
          Width = 107
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'id'
          Footers = <>
        end>
    end
    inherited plHint: TPanel
      TabOrder = 7
    end
    inherited plStatus: TPanel
      Width = 717
      TabOrder = 3
      ExplicitWidth = 717
    end
    inherited plLinkedObjects: TPanel
      Left = 535
      Top = 66
      Width = 179
      Height = 206
      Align = alRight
      TabOrder = 4
      ExplicitLeft = 535
      ExplicitTop = 66
      ExplicitWidth = 179
      ExplicitHeight = 206
      inherited dgLinkedObjects: TDBGridEh
        Width = 179
        Height = 206
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Border.EdgeBorders = [ebTop, ebBottom]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      end
    end
    inherited plSearch: TPanel
      Width = 717
      TabOrder = 2
      ExplicitWidth = 717
      inherited edSearch: TEdit
        Width = 535
        ExplicitWidth = 535
      end
    end
    object plSpec: TPanel
      Left = 0
      Top = 299
      Width = 717
      Height = 317
      Align = alBottom
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 5
      object plToolSpec: TPanel
        Left = 0
        Top = 0
        Width = 717
        Height = 28
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Align = alTop
        BevelOuter = bvNone
        Color = 15921906
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 28637
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        StyleElements = [seFont, seBorder]
        object sbAddSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 0
          Top = 0
          Width = 32
          Height = 28
          Hint = #1044#1086#1073#1072#1074#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbAddSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000E9C00000E
            9C01079453DD0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000002764944415478DAA5935B48145118C7
            FF733AB3EC8C0EDA4B82B5B5651042498B2CAB66FA12BD78698330A38788F212
            582F05AE2F696042493DD83E943E44411704A5072F154104BB8A2D51ACDD28A8
            DDBC4441E2B2BB33E3CE7AA63389646A60F41DCE8173F8F89DEFF2FF04AC3427
            DFC75082A3C8959D98562318C11DFE768BEFC85247D334212CB98BA0F0675E70
            D7B9F2F3B073B303D97605337A026FBF44F0EA7D14F1F6B11EA868E2BEC67280
            889342C8555255E02D2A522549228CC196349250C40C8020A5691AEB1B0BC8E1
            47C1D7B81F2BB420BF01327A0ABBBC270E979726E64C26A7611249B0B13D39C5
            087E1B8566A60885C02481AAB7034F32C3A7867AA0A36111E0543A3C9FCE790F
            A884523BE1DFA5F9CAB66591335B1AD115BD8ED9548C519E1FB3566A4EBF3A30
            24C77CA3DB38206201DACAFB6BCF57B83C7AD2487180C9A3674411159C7536E1
            4AC48FB811E7582B2B816488367D78FCB9FD69F53D1F075C1678B53F3474F9B6
            E7ADCF61DC039268278699FE957BFDC6E3E89EBAC9014988028566E8169C4D27
            7E507F4547C09C32CB041C92532D6D3E4A2961FB73F7A154F110FCC502F131F6
            70F2310311E9C5DA96CFE64B33EF9F239837E7D9D7E40CF557F10826AC0880D6
            BDFD35AD55AEE2B5D5609D4D1F7C17B23FABBCDBCC6BD0B9D0857637EF825725
            36710D5D30F4CEC10772A239B49503A20B3AB0E386CB5F5D77A4BC6CAD3AE8E6
            3A685CA1C4DDC51505073D1EAEC44C92668C2679E8193C154A485AD354D6170A
            CAE1E191307A67DD7F2A7111025C932F15D6BBF3776097C3892C49414C8B637C
            2A82176F3E22D11AB27E3EBDDA2C2C9FC61A6C422536C081EF98C02406F85BEF
            AAD3681DFF633F013BA960F0622C30B40000000049454E44AE426082}
          ExplicitLeft = 1
          ExplicitTop = 1
          ExplicitHeight = 25
        end
        object sbDeleteSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 32
          Top = 0
          Width = 32
          Height = 28
          Hint = #1059#1076#1072#1083#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          OnClick = sbDeleteSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000E9C00000E
            9C01079453DD0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000001484944415478DAAD53414B024114FE
            66095996FE417F41C49325EEBAE622B241D84442873A458724FA33051DEA545D
            0AF6B0A162BA048BE62EE8EF08BA7888080FD5343B90AC68896B8F8179CCBCEF
            9BF7BDF78630C6B088917F23208404DBDA11B0720E58D382F7017A0DBC70B73B
            C285085619A51E0603545C778B93DC87C165C0BC2B956A22D6B6358E7B1A23D8
            2384DE140A1692C977743ACA89E7D153C00EEE0E80CD4B4DB3914ABDA1DF5F3E
            76DDF21963D684049EE2C6553A5D452E37E424F24EBB6DBE029F4D5D6F72B038
            AB789EC86E9A048C5E330C1BAAFA854643822C03F9BCF0C359FD4A10D83A603C
            EA7A0BF1F8109204F8BEBCDBEB99B7C0C34FCC9F04065F4E54823109F5BA0445
            01B2D90F38CED24C09A288994C9503E62FA26863B168219188DCC6D983649A35
            C462C120A91CD79D7B940F81ED0BE099BBFE8484853F5354FB06D031E7E18A64
            5A800000000049454E44AE426082}
        end
        object sbEditSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 64
          Top = 0
          Width = 28
          Height = 28
          Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbEditSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
            62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
            08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
            4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
            C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
            892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
            4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
            0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
            C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
            8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
            346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
            5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
            3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
            378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
            C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
            1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
            FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
            F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
            0049454E44AE426082}
          ExplicitTop = 3
        end
        object sbConfirmSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 163
          Top = 0
          Width = 29
          Height = 28
          Hint = #1055#1088#1086#1074#1077#1089#1090#1080'/'#1087#1086#1076#1090#1074#1077#1088#1076#1080#1090#1100
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = sbConfirmSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
            6FA864000003244944415478DA7593594C13511486CFBD339D96564A4314B402
            4565D7884A106D10B7180141C525FAA0B8205188F10982467D70490C68F4C5B8
            2F9848D44834C6566A2086206E1525718B6888340641436AE974A6ED3073EBED
            A04614E7ED66CEF79F73EF7F7E04FFF9B48608CE5A967F0010A37B72C9BE3BC0
            FB83A3D5A13F0FD1E671A6BC5D2536D9171C8C4D89CF499B1817CD300C747DF9
            ECED7DDBF3486B32443A8E5C2D12DCBCF71F0184102C3FB0A571D1C2DC559698
            F1A00CC960341824444BBCA2C02106C337CF77B8D978F74CCBB11B9523040C89
            4653EEF6A2B3592933D7E64E9F2EB20CC35145AC84080EFF671026214208C648
            76BEEFD2B5BE6C6F787CCEBE537079075581D5872BDAF3E6655B93CD66916159
            3DA1E568581E8727230A211A96854151C0F410F82EF8758EE6B6F6C67DA7F3D4
            BAA2FDA5F66D1BD715E8599DAC10458510466A77DA18388D063C3C4F1CCF9F41
            766A3A498A8F638F9FBBDC643B786519D28D89D06E3A5BDDB77446B6698C5EAF
            76FE357A28141A867D3C389C4E48B758C88C2949E013FDD8D1E9F45C2CAF9D80
            16EC5A5D575CB2B42A33718A3444144E0C06C0A83780AC2823E08C440BCC4C4A
            017F30082C66A437AE4FDC9DDB4DC7C302B5256B0AABA7254C922445E69A3B9C
            307EEC58B0664C837EB71BEE3FFF0396A4F058A0C1ACF4AAA79BB3DB9ACF202D
            BD42D9F99ABE259959A648BD81B8791E1E7476E0B89858E81B1880C913CD9095
            9CAAC22804D4090C3E418496779DB87EFBD128F51197EDDD682F2F5D5F10A1D1
            CA0C66B057F4017D309C96608159C929BF61D5D2B04030801BEED85C8D35A727
            0DDB5857D136DF3A3B3729D61CA0BC8E1A4064590656C3E2B00BF013064C6D25
            20F50C7CE55A5FBCE8BD567922617891A28D5173CB0B4FCC9997BDD99A3A35C0
            D265A05662995AAA725490DE3DBC1E7247F7075D6BF3C3934F2EDCDB237A7CC2
            882C2CAE5A776AC5CAFC1D09E36280C884BAA197C2DBE497829C86D380EBDB57
            B879EB6E7D4BEDF5ADA386C9101D69CCDFB3C116F408BC392D31272D2E9E8609
            C3C7FE5EAFEB75F723AD511FD974A4A158707B074715F83BCE73CB0A0ED14C30
            4F2F35ED0DF0E2A871FE01B1FE59A9E857C97D0000000049454E44AE426082}
          ExplicitLeft = 123
          ExplicitTop = 1
          ExplicitHeight = 25
        end
        object Bevel8: TBevel
          AlignWithMargins = True
          Left = 160
          Top = 2
          Width = 1
          Height = 24
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alLeft
          ExplicitLeft = 502
          ExplicitHeight = 43
        end
        object sbCancelConfirmSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 192
          Top = 0
          Width = 29
          Height = 28
          Hint = #1057#1085#1103#1090#1100' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1077
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = sbCancelConfirmSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002214944415478DAA593414854411880BF79BBAE91A5E11A
            49B16EA9A965872048EA50770FA61B5114614851D02524E8D221F350874ED2A5
            8317C17318044114041156161DC4758B2D4D8AA4D5B0A0F6BD7D3B7FF3467775
            334F0EBC37EFBD99EF9BF9E7FDBF1211D6D3D42A815219738F165E75E9E89C23
            52B3B640A9EFB436D570E42098EF764CEBE2A55FBC45521F7F85452A570B0AF0
            D136487F45E6162C24019CCF235515B06B1B7AF41DF90FD3990D225B97052BE1
            851C5CBD86F4F723E9F4221C8FE3F4F591BF711D2A147A6C1C3F3D93D9682405
            8170E90CF2E91BEAF61D686C04CF23DFDD6D77111A1A824804999CC4BF700EEA
            6B71EF3F6593882A0AE4E269184B21B118CEE0A00502896D65658879F6130974
            2A856A6B21FBE019950581369D3A7F123102BBE5BABAC5550D689B81BDAE2E73
            80297B2681C07DF8FC1F41CF09E4551231024CCCA1E1E1A220583DD7D1813621
            04E3CEA13DB88F464B057427909713CBF05208C13F524108AE8BDBDE8E4E2609
            1DDECB9FC7AFD95210F8A673CE1E43BF9F2174F71EAAB9D902B9CE4EBBE5C8C8
            08949723E3E3B8A78EA39A76907DF26659E099EC53BBE35175A0157E64716EDE
            C2EFEDB50726BE8F6A682032308077E5325485C925A7C87DC9CC568BD4161329
            6B244E432CAAF69BD527A6D0B3F3365E7B26C155BD19A7258637398DBF04AF4A
            E5DF411DECDC1E0DEFAB376021858D449B39A2F1529F4BE0FF16D3CF358A69A9
            9F377074E57CB5DE72FE0B42C953F0B2C476DB0000000049454E44AE426082}
          ExplicitLeft = 154
          ExplicitTop = 1
          ExplicitHeight = 25
        end
        object sbMove: TPngSpeedButton
          AlignWithMargins = True
          Left = 285
          Top = 0
          Width = 29
          Height = 28
          Hint = #1055#1077#1088#1077#1085#1077#1089#1090#1080' '#1087#1086#1079#1080#1094#1080#1080' '#1074' '#1076#1088#1091#1075#1091#1102' '#1079#1072#1103#1074#1082#1091
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = sbMoveClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002684944415478DAA5535B48944114FE6605216DC52DD145
            2CD2A440C3088D0AC4C4324A28EAC597145936BA6025D5763334345D3224EC46
            0452A9490A516F615826225B2D9614D5B6995AB65B9114B8B05EFEFD67A633FF
            AE4FBD681DF83873E6F29D73BE9961524AFC8FB15D473ACB73F3731A7E4FCA58
            834B084802179CBC849014734E5E1A9ED35A5C7428E8F1F84FDEBF587C8DD5DF
            1BE66673AC69BE99873D5ED1549117C5CEDCFDF44F3D8CFB7CB8E1D8C858E59D
            2159BF3B7DDE047BCEF7A0F9540163A7DBBCD259B2029A4E3DD382EA9B0B9282
            22618C490F5D86417A685C22D56A86CDD98D5B958544D0FA413A4B5722441BA1
            842216AE7C38A4310C41B908CF09224A5ABC0065B55D68A9DECAD889DBEF6443
            598691C9A840412AF56154A04814AA1E6C46702680DA9DBDB0C6C7A1F4EC43B4
            D5143176FCE65B79C19619CE4A60111211219254821A3B3AF360B5A4E2BDFF15
            5AED83B0D574A3FDDC76C68E35BF968DF62C8360D6D44147473E791D9A20E83A
            922DE9C84AC985FB730FDEF8DDB08C37A1A3AE98B18AEB03B2697F36B87A5591
            C3CA2ADA37A030B3CC104E69A01AFC31E183253611CF461EE3E5581FA6F59978
            76F8AA5B5E2A5F1BCE1C816A7A6F4B368AB2ECF8F26B8804D6A18B10423C0493
            291A89E614F48F3CC28BD17ECE0E5E7E2EAF1C5AF7D73D97346760C7EA7D7490
            1B50957C9F18C3A285560C7E75E1A9C785491E58A3FE4267D292E462A1DE3B3D
            00AE3CDD4820E100A6358D34D0A8540DCB1332B03E6D0B06C65CE8F3BAB16CEA
            68577763D53636D7DFB8AACE14CC595A10D3FBF1895F13D8F4CD29BDC66F9C2B
            415A350B721115332578DA4FA71C9D9DFF03ED726F1DE2C21D28000000004945
            4E44AE426082}
          ExplicitLeft = 268
          ExplicitTop = 2
          ExplicitHeight = 27
        end
        object sbExcelSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 126
          Top = 0
          Width = 32
          Height = 28
          Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          ParentShowHint = False
          ShowHint = True
          OnClick = btExcelClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000B1200000B
            1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000002CC4944415478DA6D934B48545118C7
            FF67346A141B7332F35136A6398E8E332A6A48505A8E2F68D16634451745602F
            93162911F65A0411F82C082A3433CB686764D3C3458B462B17968A1486A8E942
            D42847C77BCEE9DC7BC7C16C2EDC73CFEBFBDDFFF73FDF2158F7D83BAFED5E9C
            9DCF5876BB5297D88A894ACCB4C8DDB503350F3BE1E321F6BB17AA9618B75026
            25AC4892516252B05B5A81442918631063700DCA3ED7B63FF20928BA5DCDA9D8
            4819555E3548F4054086C86BD1C161F321BA2D2E0E0E06C619E5728BA1E91F5A
            52D8728E5331A09ECD2A481E4B2A48F41342A31112B819C7F61F41ABB35B09B6
            46ED8163C439490A5AAAF8BF81E2EB01AA0A284C61BB10B23108259979E81AEC
            052780252216AF46FA26497EB30CA0D06D0A54729AFE358B2C8319A333E3189F
            9B5152326E8B865E1B84E2341B1E0F38E0B7C11FD61DF1022014E4359F5500E6
            F0585465DB71D3D18EDABC72D8EF5DC4E2F2920248100AF45A9D00E4A2E3530F
            888620D5605201B6A6337C55FED5A213B044C6E1D69B0E747F79EF353521CCA0
            28389A9E8FF6FE97C24A08057178F76D6092E4369E564C9437DA8C99A8CE29C1
            E13BE731FB67C17B2289DB63B0354027003601E8111E70240BC5AF473F4E9143
            8DA714133584E07EE925CCFE5EC0E0CFEFA87FDBA102849189E12AA02CBD006D
            FD2F201FA7252A1E8E61E71439D820009C629FC182AC986434F53E455B791D2A
            5AAF60627E46012445C62254004A3D002880383886FAA6484EC349EE2D1E4FCE
            1AA2518ED1E55E56E6CD1131D00706A33CA310AD7D721D70A4EC348A3AF83041
            B2EB2BD700642FE87F9568167F931554EC2DC4035148B2072991F1E819764E90
            0302A0160FF5E6BC5A50F23D90FB067DF89C4E1BE4E29C2BF2A968840FFCEBF4
            5800C9BA71BC9230C942FD4822A3CC24C243A8585E5B89F0F72B1EADEB7AE2F3
            32AD9FB05E2EDEC556581AD54856892349E46B9608A919BBFEFC992FC05FFC64
            D8FDA3D5A84C0000000049454E44AE426082}
          ExplicitLeft = 95
          ExplicitTop = 2
          ExplicitHeight = 25
        end
        object sbFilter: TPngSpeedButton
          AlignWithMargins = True
          Left = 221
          Top = 0
          Width = 32
          Height = 28
          Hint = #1054#1090#1073#1086#1088
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          AllowAllUp = True
          GroupIndex = 1
          ParentShowHint = False
          ShowHint = True
          OnClick = sbFilterClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000AEB00000A
            EB01828B0D5A0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000000A34944415478DA63FCFFFF3F032580
            D161CA0B8A4C607CF0E18740C2920FEF17C40890A411A88701A8479011E48587
            1F7F2A0005EE136B084CB33C3FFB07465818000D31004A9C276408B266B01790
            031168880350C17E5C86A06BC630006A480250E17C7443A09A0D819A2FA00422
            B668841952E1CA03E677ECFE8255334E0340C071EACBFF3057806CDF9F2DCE88
            351A470DC06D00B221641B00330444936D0021000075459B7A13DFF16F000000
            0049454E44AE426082}
          ExplicitLeft = 225
          ExplicitTop = -2
        end
        object sbResetFilter: TPngSpeedButton
          AlignWithMargins = True
          Left = 253
          Top = 0
          Width = 32
          Height = 28
          Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1080#1083#1100#1090#1088
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Flat = True
          ParentShowHint = False
          ShowHint = True
          OnClick = sbResetFilterClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000000473424954080808087C086488000000097048597300000AEB00000A
            EB01828B0D5A0000001C74455874536F6674776172650041646F626520466972
            65776F726B732043533571B5E336000000B94944415478DA63FCFFFF3F032580
            D161CA0B8A4C607CF0E18740C2920FEF17C40890A411A88701A8479011E48587
            1F7F2A0005EE136B084CB33C3FFB07465818000D31004A9C276408B266B01790
            031168880350C17E5C86A06BC630006A480250E17C7443A09A0D819A2FA00422
            B668749CFAF23F230E2F8054EFCF1667C46B8013D080291B0319B4761D4311BF
            E666C590E3BF9E611F310600158135C00C81B18172C41B00D308023083E86B00
            455EA02810298E46520000704CAE7A8505FCA90000000049454E44AE426082}
          ExplicitLeft = 241
          ExplicitTop = 2
          ExplicitHeight = 25
        end
        object sbPassSpec: TPngSpeedButton
          AlignWithMargins = True
          Left = 94
          Top = 0
          Width = 32
          Height = 28
          Hint = #1054#1092#1086#1088#1084#1080#1090#1100' '#1084#1072#1090#1077#1088#1080#1072#1083#1100#1085#1099#1081' '#1087#1088#1086#1087#1091#1089#1082
          Margins.Left = 2
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alLeft
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbPassSpecClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000180000001808040000004A7EF5
            730000000467414D410000B18F0BFC610500000002624B47440000AA8D233200
            0000097048597300000EC400000EC401952B0E1B0000000774494D4507E40304
            06271C438A8C2B000000E74944415478DA63642011300E4E0D090C050CE20C47
            194A181E10A3218921952191E115830F431D8331C347C21AAE31F8312C64D060
            C865B062B8C8309BB086F70C4A0C37800AA5188E3094325CC6ABFA0FC31C9006
            418674063BA0836C197C1916304C6758CFB0914199A10FE8A7DB408786316430
            7C63A86660666862E067980CD220CC90C6F088C19F410DC8FACEA0CEB0126896
            17C31786430C2C0C31407BEF0083C48F613950849F411E628339C307063D0607
            0605A00DF89DB40FA201021218F4190A89F1F408D4F00F18A4DF192281E9E83D
            8D6C78CD2002E7D530B412D6402218841A0054D6411794A43C2C000000257445
            5874646174653A63726561746500323032302D30332D30345430363A33393A32
            382B30303A3030CF2377D00000002574455874646174653A6D6F646966790032
            3032302D30332D30345430363A33393A32382B30303A3030BE7ECF6C00000019
            74455874536F667477617265007777772E696E6B73636170652E6F72679BEE3C
            1A0000000049454E44AE426082}
          ExplicitLeft = 98
          ExplicitTop = 2
          ExplicitHeight = 27
        end
      end
      object dgSpec: TDBGridEh
        Left = 0
        Top = 28
        Width = 717
        Height = 289
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alClient
        AllowedOperations = []
        Border.Color = clSilver
        BorderStyle = bsNone
        ColumnDefValues.Title.TitleButton = True
        Ctl3D = False
        DataSource = dsSpec
        DynProps = <>
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        GridLineParams.BrightColor = 15395041
        GridLineParams.DarkColor = 15395041
        GridLineParams.DataBoundaryColor = 15395041
        GridLineParams.DataHorzColor = 14540253
        GridLineParams.DataVertColor = 14540253
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
        IndicatorParams.HorzLineColor = clSilver
        IndicatorParams.VertLineColor = clSilver
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        OptionsEh = [dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
        ParentCtl3D = False
        ParentFont = False
        ParentShowHint = False
        PopupMenu = pmSpec
        SelectionDrawParams.SelectionStyle = gsdsClassicEh
        SelectionDrawParams.DrawFocusFrame = False
        SelectionDrawParams.DrawFocusFrameStored = True
        ShowHint = True
        SortLocal = True
        STFilter.Color = 15725813
        STFilter.Font.Charset = DEFAULT_CHARSET
        STFilter.Font.Color = clWindowText
        STFilter.Font.Height = -11
        STFilter.Font.Name = 'Verdana'
        STFilter.Font.Style = []
        STFilter.HorzLineColor = 14540253
        STFilter.Local = True
        STFilter.ParentFont = False
        STFilter.VertLineColor = 14540253
        STFilter.Visible = True
        TabOrder = 1
        TitleParams.Color = clBtnFace
        TitleParams.FillStyle = cfstThemedEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -11
        TitleParams.Font.Name = 'Arial'
        TitleParams.Font.Style = [fsBold]
        TitleParams.HorzLineColor = 13421772
        TitleParams.MultiTitle = True
        TitleParams.ParentFont = False
        TitleParams.SecondColor = 15987699
        TitleParams.SortMarkerStyle = smstDefaultEh
        TitleParams.VertLineColor = 13421772
        OnApplyFilter = dgSpecApplyFilter
        OnDblClick = sbEditSpecClick
        OnDrawColumnCell = dgSpecDrawColumnCell
        OnMouseDown = dgDataMouseDown
        OnMouseUp = dgSpecMouseUp
        OnSelectionChanged = dgSpecSelectionChanged
        OnTitleBtnClick = dgSpecTitleBtnClick
        Columns = <
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'isconfirmed'
            Footers = <>
            Title.Caption = '  '
            Width = 35
          end
          item
            Alignment = taCenter
            AutoFitColWidth = False
            CaseInsensitiveTextSearch = False
            CellButtons = <>
            CellDataIsLink = True
            Color = 16623272
            DynProps = <>
            EditButtons = <>
            FieldName = 'pass_number'
            Footers = <>
            Title.Caption = #1055#1088#1086' '#1087#1091#1089#1082
            Width = 51
            OnCellDataLinkClick = dgSpecColumns0CellDataLinkClick
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'container_num'
            Footers = <>
            ReadOnly = True
            Title.Caption = #8470' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
            Width = 110
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'kind_code'
            Footers = <>
            ReadOnly = True
            Title.Caption = #1058#1080#1087
            Width = 48
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'owner_code'
            Footers = <>
            ReadOnly = True
            Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
            Width = 115
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'amount'
            Footers = <>
            Title.Caption = #1050#1086#1083'-'#1074#1086
            Width = 53
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'used_amount'
            Footers = <>
            Title.Caption = #1055#1086#1075#1088#1091#1078#1077#1085#1086
            Width = 80
          end
          item
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isempty'
            Footers = <>
            ReadOnly = True
            Title.Caption = #1055#1086#1088#1086#1078#1085#1080#1081'?'
            Width = 77
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'seal_number'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
            Width = 112
          end
          item
            Alignment = taCenter
            AutoFitColWidth = False
            CellButtons = <>
            Checkboxes = False
            DynProps = <>
            EditButtons = <>
            FieldName = 'issealwaste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            Footers = <>
            ReadOnly = True
            Title.Caption = #1043#1086#1076'- '#1085#1072#1103'?'
            Width = 49
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'weight_container'
            Footers = <>
            Title.Caption = #1042#1077#1089' '#1085#1077#1090#1090#1086
            Visible = False
            Width = 79
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'weight_cargo'
            Footers = <>
            Title.Caption = #1042#1077#1089' '#1073#1088#1091#1090#1090#1086
            Width = 81
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'external_order_num'
            Footers = <>
            Title.Caption = #1053#1086#1084#1077#1088' '#1043#1059'-12'
            Width = 100
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'state_code'
            Footers = <>
            Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 100
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object edNote: TDBEdit
      Left = 0
      Top = 275
      Width = 717
      Height = 17
      Align = alBottom
      BorderStyle = bsNone
      Color = 16316664
      Constraints.MinHeight = 14
      DataField = 'note'
      DataSource = dsData
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 6
      StyleElements = [seFont, seBorder]
    end
  end
  inherited plLeft: TPanel
    Width = 201
    Height = 616
    ExplicitWidth = 201
    ExplicitHeight = 616
    inherited Bevel4: TBevel
      Left = 200
      Height = 591
      ExplicitLeft = 80
      ExplicitTop = 29
      ExplicitHeight = 585
    end
    inherited dgFolders: TDBGridEh
      Width = 200
      Height = 591
      Border.Color = clSilver
    end
    inherited plTool: TPanel
      Width = 201
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      ExplicitWidth = 201
      inherited sbWrap: TSpeedButton
        Left = 94
        Width = 107
        ExplicitLeft = 145
        ExplicitTop = 1
        ExplicitWidth = 0
      end
      inherited cbAll: TCheckBox
        Width = 88
        ExplicitWidth = 88
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'declare @userid int, @doctypeid int, @datebegin datetime, @datee' +
        'nd datetime, @isconfirmed int, @currentid int;'
      ''
      'select @currentid = IDENT_CURRENT('#39'docorder'#39');'
      ''
      
        'select @doctypeid = min(id) from doctypes where system_section =' +
        ' '#39'outcome_apps'#39';'
      ''
      'select @userid = :userid;'
      ''
      
        'select @datebegin = datebegin, @dateend  = dateend, @isconfirmed' +
        '  = isconfirmed '
      'from dbo.f_GetFilterParams(@userid,@doctypeid);'
      ''
      
        'select d.*, h.*, (case when d.isconfirmed = 1 then '#39' +'#39' else '#39#39' ' +
        'end) as state_code,'
      
        '(select code from doctypes t where t.id = d.doctype_id) as docty' +
        'pe_code,'
      
        '(case when ps1.cnt>1 then '#39'**'#39' else ps1.pass_number end) as pass' +
        '_number,'
      'dbo.GetIsStop(d.id) as isstop,'
      'usedproc,'
      'ldproc,'
      'uplproc,'
      
        '(select code from directions c where c.id = h.direction_id) as d' +
        'irection_code,'
      
        '(select name from stations c where c.id = h.station_id) as stati' +
        'on_code,'
      
        '(select code from counteragents c where c.id = h.forwarder_id) a' +
        's forwarder_code,'
      
        '(select code from counteragents c where c.id = h.consignee_id) a' +
        's consignee_code,'
      
        '(select code from counteragents c where c.id = h.payer_id) as pa' +
        'yer_code,'
      
        '(select code from deliverytypes t where t.id = h.dlv_type_id) as' +
        ' delivery_code, '
      
        '(select code from shippingoptions sh where sh.id = h.shippingopt' +
        'ion_id) as shippingoption_code, '
      
        '(select user_name from users u where u.id = d.user_created) as u' +
        'ser_created'
      'from docorder h, documents d '
      'left outer join ('
      
        '     select osh.doc_id, min(p.pass_number) as pass_number, count' +
        '(*) as cnt '
      '     from passes p, passoperations po, objectstatehistory osh '
      
        '     where p.id = po.parent_id and po.task_id = osh.task_id and ' +
        'po.container_id = osh.object_id'
      '     and isnull(p.isabolished,0) = 0  '
      '     group by osh.doc_id'
      ') ps1 on (d.id = ps1.doc_id)'
      'where d.id = h.id '
      'and (('
      '(1 = :showall or d.folder_id = :folder_id)'
      'and ('
      
        '    not exists (select 1 from dbo.f_GetFilterList(@userid, @doct' +
        'ypeid) fl1)'
      
        '    or exists (select 1 from dbo.f_GetFilterList(@userid, @docty' +
        'peid) fl, docorderspec s where  s.doc_id = h.id and charindex(fl' +
        '.v, s.container_num)>0) '
      
        '    or exists (select 1 from  dbo.f_GetFilterList(@userid, @doct' +
        'ypeid) fl, objects2docspec ods, containers c where  ods.doc_id =' +
        ' h.id and ods.object_id = c.id and charindex(fl.v, c.cnum)>0) '
      ')'
      'and (@datebegin is null or  d.doc_date >=@datebegin)'
      'and (@dateend is null or  d.doc_date <@dateend+1)'
      
        'and (isnull(@isconfirmed,0)=0 or d.isconfirmed = isnull(@isconfi' +
        'rmed,0))'
      ') or h.id = @currentid)'
      'order by d.doc_date')
    SelectCommand.Parameters = <
      item
        Name = 'userid'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'showall'
        DataType = ftInteger
        Size = -1
        Value = 1
      end
      item
        Name = 'folder_id'
        DataType = ftInteger
        Size = -1
        Value = 1
      end>
    UpdateCommand.CommandText.Strings = (
      'declare @id int;'
      'select @id = :id;'
      ''
      
        'update documents set  doc_number = :doc_number, doc_date = :doc_' +
        'date, doctype_id = :doctype_id, '
      
        'inwait = :inwait, datestopinwait = :datestopinwait where id = @i' +
        'd;'
      ''
      'update docorder'
      'set'
      '  forwarder_id = :forwarder_id,'
      '  consignee_id = :consignee_id,'
      '  direction_id = :direction_id,'
      '  station_id = :station_id,'
      '  consignee_address = :consignee_address,'
      '  consignee_contact = :consignee_contact,'
      '  payer_id = :payer_id,'
      '  contract_id = :contract_id,'
      '  els_payer = :els_payer,'
      '  plan_date = :plan_date,'
      '  dlv_type_id = :dlv_type_id,'
      '  begin_date = :begin_date,'
      '  end_date = :end_date,'
      '  shippingoption_id = :shippingoption_id,'
      '  extra_services = :extra_services,'
      '  tarif_type_id = :tarif_type_id,'
      '  train_num = :train_num,'
      '  note = :note,'
      '  endpoint_consignee_id = :endpoint_consignee_id'
      'where'
      '  id = @id;')
    UpdateCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'doc_date'
        Size = -1
        Value = Null
      end
      item
        Name = 'doctype_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'inwait'
        Size = -1
        Value = Null
      end
      item
        Name = 'datestopinwait'
        Size = -1
        Value = Null
      end
      item
        Name = 'forwarder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'consignee_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'direction_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'station_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'consignee_address'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'consignee_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'payer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'contract_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'els_payer'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'plan_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'begin_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'end_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'shippingoption_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'extra_services'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2000
        Value = Null
      end
      item
        Name = 'tarif_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'train_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'note'
        Size = -1
        Value = Null
      end
      item
        Name = 'endpoint_consignee_id'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'begin'
      ''
      '  declare @newid int;'
      ''
      
        '  insert into documents (doctype_id, folder_id, doc_number, doc_' +
        'date, inwait, datestopinwait) values (:doctype_id, :folder_id, :' +
        'doc_number, :doc_date, :inwait, :datestopinwait);'
      ''
      '  select @newid = IDENT_CURRENT('#39'documents'#39');'
      ''
      'insert into docorder'
      
        '  (id, forwarder_id, consignee_id, direction_id, station_id, con' +
        'signee_address, '
      
        '   consignee_contact, payer_id, contract_id, els_payer, plan_dat' +
        'e, dlv_type_id, shippingoption_id,'
      
        '   begin_date, end_date, extra_services, tarif_type_id, train_nu' +
        'm, note, endpoint_consignee_id)'
      'values'
      
        '  (@newid, :forwarder_id, :consignee_id, :direction_id, :station' +
        '_id, :consignee_address, '
      
        '   :consignee_contact, :payer_id, :contract_id, :els_payer, :pla' +
        'n_date, :dlv_type_id, :shippingoption_id,'
      
        '   :begin_date, :end_date, :extra_services, :tarif_type_id, :tra' +
        'in_num, :note, :endpoint_consignee_id)'
      ''
      'end;'
      ''
      ''
      '')
    InsertCommand.Parameters = <
      item
        Name = 'doctype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'doc_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'inwait'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'datestopinwait'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'forwarder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'consignee_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'direction_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'station_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'consignee_address'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'consignee_contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'payer_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'contract_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'els_payer'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'plan_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'shippingoption_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'begin_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'end_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'extra_services'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2000
        Value = Null
      end
      item
        Name = 'tarif_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'train_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'endpoint_consignee_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      
        'delete from documents where  id = :id and isnull(isconfirmed,0)=' +
        '0')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select d.*, h.*, (case when d.isconfirmed = 1 then '#39' +'#39' else '#39#39' ' +
        'end) as state_code,'
      
        '(select code from doctypes t where t.id = d.doctype_id) as docty' +
        'pe_code,'
      
        '(case when ps1.cnt>1 then '#39'**'#39' else ps1.pass_number end) as pass' +
        '_number,'
      'dbo.GetIsStop(d.id) as isstop,'
      'usedproc,'
      'ldproc,'
      'uplproc,'
      
        '(select code from directions c where c.id = h.direction_id) as d' +
        'irection_code,'
      
        '(select name from stations c where c.id = h.station_id) as stati' +
        'on_code,'
      
        '(select code from counteragents c where c.id = h.forwarder_id) a' +
        's forwarder_code,'
      
        '(select code from counteragents c where c.id = h.consignee_id) a' +
        's consignee_code,'
      
        '(select code from counteragents c where c.id = h.payer_id) as pa' +
        'yer_code,'
      
        '(select code from deliverytypes t where t.id = h.dlv_type_id) as' +
        ' delivery_code, '
      
        '(select code from shippingoptions sh where sh.id = h.shippingopt' +
        'ion_id) as shippingoption_code, '
      
        '(select user_name from users u where u.id = d.user_created) as u' +
        'ser_created'
      'from docorder h, documents d '
      'left outer join ('
      
        '     select osh.doc_id, min(p.pass_number) as pass_number, count' +
        '(*) as cnt '
      '     from passes p, passoperations po, objectstatehistory osh '
      
        '     where p.id = po.parent_id and po.task_id = osh.task_id and ' +
        'po.container_id = osh.object_id'
      '     and isnull(p.isabolished,0) = 0  '
      '     group by osh.doc_id'
      ') ps1 on (d.id = ps1.doc_id)'
      'where d.id = h.id and h.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
  inherited meData: TMemTableEh
    AfterOpen = meDataAfterOpen
    BeforeInsert = meDataBeforeInsert
  end
  inherited drvForms: TADODataDriverEh
    Top = 138
  end
  inherited meForms: TMemTableEh
    Top = 138
  end
  inherited dsForms: TDataSource
    Top = 138
  end
  inherited drvLinkedObjects: TADODataDriverEh
    Left = 768
    Top = 188
  end
  inherited meLinkedObjects: TMemTableEh
    Left = 816
    Top = 188
  end
  inherited dsLinkedObjects: TDataSource
    Left = 872
    Top = 188
  end
  inherited pmLinkedObjects: TPopupMenu
    Left = 816
    Top = 128
  end
  inherited qrReport: TADOQuery
    Left = 520
  end
  object drvSpec: TADODataDriverEh [24]
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'declare @guid varchar(42);'
      ''
      'select @guid = :guid;'
      ''
      'select s.*, '
      #39#39' as isconfirmed, '
      'ps1.pass_number,'
      
        '(select code from counteragents c1 where c1.id = s.container_own' +
        'er_id) as owner_code,'
      'st1.code as state_code, st1.stop_sign,'
      
        '(select code from containerkinds k where k.id = s.container_kind' +
        '_id) as kind_code,'
      'isnull(cr.issealwaste,0) as issealwaste, '
      'o2s.task_id, cr.id as cargoid '
      'from docorderspec s  '
      'left outer join containers v on (s.container_id = v.id)'
      'left outer join cargos cr on (cr.id = s.cargo_id)'
      
        'left outer join objects2docspec o2s on (o2s.doc_id = s.doc_id an' +
        'd o2s.object_id = v.id)'
      ''
      'left outer join ('
      
        '  select sk1.stop_sign, sk1.code, los1.object_id,  los1.task_id ' +
        'from v_lastobjectstates los1, objectstatekinds sk1 where sk1.id ' +
        '= los1.object_state_id'
      ') as st1 on (st1.object_id = v.id and st1.task_id = o2s.task_id)'
      ''
      'left outer join ('
      
        '     select po.task_id, osh.doc_id, min(p.pass_number) as pass_n' +
        'umber, count(*) as cnt '
      '     from passes p, passoperations po, objectstatehistory osh '
      
        '     where p.id = po.parent_id and po.task_id = osh.task_id and ' +
        'po.container_id = osh.object_id'
      '     and isnull(p.isabolished,0) = 0  '
      '     group by po.task_id, osh.doc_id'
      ') ps1 on (s.doc_id = ps1.doc_id and o2s.task_id = ps1.task_id)'
      ''
      'where s.doc_id = :doc_id '
      'and '
      '('
      '  not exists (select 1 from dbo.f_GetFilterList(0, @guid) fl1)'
      
        '  or exists (select 1 from dbo.f_GetFilterList(0, @guid) fl wher' +
        'e charindex(fl.v, v.cnum)>0) '
      ')'
      'order by s.id')
    SelectCommand.Parameters = <
      item
        Name = 'guid'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'declare @cargo_id int;'
      ''
      'select @cargo_id = :cargo_id;'
      ''
      'update docorderspec'
      'set'
      '  doc_id = :doc_id,'
      '  container_id = :container_id,'
      '  container_num = :container_num,'
      '  container_kind_id = :container_kind_id,'
      '  container_owner_id = :container_owner_id,'
      '  seal_number = :seal_number,'
      '  cargo_id = @cargo_id,'
      '  cargotype_id = :cargotype_id,'
      '  isempty = :isempty,'
      '  weight_cargo = :weight_cargo,'
      '  weight_container = :weight_container,'
      '  amount = :amount,'
      '  external_order_num = :external_order_num, '
      '  external_order_signed = :external_order_signed'
      'where'
      '  id = :id;'
      ''
      
        'update cargos set issealwaste = :issealwaste where id = @cargo_i' +
        'd;'
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'cargo_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'cargotype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'weight_cargo'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_container'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'amount'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'external_order_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'external_order_signed'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'issealwaste'
        Size = -1
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into docorderspec'
      
        '  (doc_id, container_id, container_num, container_kind_id, conta' +
        'iner_owner_id, '
      
        '   seal_number, cargo_id, cargotype_id, isempty, weight_cargo, w' +
        'eight_container, '
      '   amount, external_order_num, external_order_signed)'
      'values'
      
        '  (:doc_id, :container_id, :container_num, :container_kind_id, :' +
        'container_owner_id,'
      
        '   :seal_number, :cargo_id, :cargotype_id, :isempty, :weight_car' +
        'go, :weight_container, '
      '   :amount, :external_order_num, :external_order_signed)')
    InsertCommand.Parameters = <
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'container_kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'cargo_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cargotype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'weight_cargo'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'weight_container'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'amount'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'external_order_num'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'external_order_signed'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from docorderspec where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select s.*, '
      #39#39' as isconfirmed, '
      'ps1.pass_number,'
      
        '(select code from counteragents c1 where c1.id = s.container_own' +
        'er_id) as owner_code,'
      'st1.code as state_code, st1.stop_sign,'
      
        '(select code from containerkinds k where k.id = s.container_kind' +
        '_id) as kind_code,'
      'isnull(cr.issealwaste,0) as issealwaste, '
      'o2s.task_id, cr.id as cargoid '
      'from docorderspec s  '
      'left outer join containers v on (s.container_id = v.id)'
      'left outer join cargos cr on (cr.id = s.cargo_id)'
      
        'left outer join objects2docspec o2s on (o2s.doc_id = s.doc_id an' +
        'd o2s.object_id = v.id)'
      ''
      'left outer join ('
      
        '  select sk1.stop_sign, sk1.code, los1.object_id,  los1.task_id ' +
        'from v_lastobjectstates los1, objectstatekinds sk1 where sk1.id ' +
        '= los1.object_state_id'
      ') as st1 on (st1.object_id = v.id and st1.task_id = o2s.task_id)'
      ''
      'left outer join ('
      
        '     select po.task_id, osh.doc_id, min(p.pass_number) as pass_n' +
        'umber, count(*) as cnt '
      '     from passes p, passoperations po, objectstatehistory osh '
      
        '     where p.id = po.parent_id and po.task_id = osh.task_id and ' +
        'po.container_id = osh.object_id'
      '     and isnull(p.isabolished,0) = 0  '
      '     group by po.task_id, osh.doc_id'
      ') ps1 on (s.doc_id = ps1.doc_id and o2s.task_id = ps1.task_id)'
      'where s.id = :current_id ')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 320
    Top = 456
  end
  object meSpec: TMemTableEh [25]
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvSpec
    BeforeOpen = meSpecBeforeOpen
    BeforeInsert = meSpecBeforeInsert
    AfterInsert = meSpecAfterInsert
    BeforeEdit = meSpecBeforeEdit
    Left = 368
    Top = 456
  end
  object dsSpec: TDataSource [26]
    DataSet = meSpec
    Left = 424
    Top = 456
  end
  object pmSpec: TPopupMenu [27]
    Left = 484
    Top = 454
    object N21: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 45
      OnClick = sbAddSpecClick
    end
    object N22: TMenuItem
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
      ShortCut = 113
      OnClick = sbEditSpecClick
    end
    object N24: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ShortCut = 116
      OnClick = N24Click
    end
    object N23: TMenuItem
      Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1082#1086#1085#1090#1077#1081#1085#1077#1088
      Visible = False
    end
    object N20: TMenuItem
      Caption = '-'
    end
    object N17: TMenuItem
      Caption = #1055#1086#1089#1090#1072#1074#1080#1090#1100' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1091#1102' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102
      OnClick = N17Click
    end
    object N16: TMenuItem
      Caption = '-'
    end
    object N15: TMenuItem
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1080#1089#1090#1086#1088#1080#1102' '#1086#1073#1088#1072#1073#1086#1090#1082#1080' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
      OnClick = N15Click
    end
  end
  inherited drvPass: TADODataDriverEh
    Left = 640
    Top = 456
  end
  inherited mePass: TMemTableEh
    BeforeOpen = nil
    AfterOpen = nil
    AfterInsert = nil
    BeforeEdit = nil
    AfterEdit = nil
    BeforePost = nil
    AfterPost = nil
    AfterScroll = nil
    Left = 688
    Top = 456
  end
  inherited IL: TPngImageList
    PngImages = <
      item
        Background = clNone
        Name = 'stop'
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E33600000016744558744372656174696F6E2054
          696D650030342F32342F32302D0665C60000007C4944415478DA63FCFFFF3F03
          258011C300464607205900C4FE686A3702F10486FFFF0FE0368091710290CC27
          60E944A02105980610A719C31088011067EF27D1FB8E20EFC00CD880C5CF84C0
          46A00101300320FE203646181919A0EA198791011407A2030345D10871160509
          09E1370A9232C2109077C8CC4C6400009B6E6DE147AE976D0000000049454E44
          AE426082}
      end
      item
        Background = clWindow
        Name = 'control_pause_blue'
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000002FB4944415478DAA5934B4C534114864F5B4AA105FABCAD
          94A0506C29E5214408045DE20B420C3E10830BA31B4104C3CA08C4C4882C342C
          4801976C30A81042340D12E2C22858E419012B06852250A1256D6969692FBDCE
          4C29A971E9242773EFDCF9BF7BFE3367580CC340F838FD0C94684A4041A110EE
          2F3B506CA2581DBA056BE1FB59210012C6A0499B77C8999FAB70EA947C6FB680
          4B6BF177E72EDBB4E6E24D4F6C08E727AD6223DA674220D701605F9C733363BD
          4827F354C8299946C08F86285E04817BBC7ED876B9C1FC6B6DC1648BEE79BEA8
          1A46CB53181202E422716906E5AD4F494A8CC1428F6F0FBCBE00E0FC785C3608
          781C70797C30336B722148EB8B25F56B0418679DEA649428EDB20AADB5569D7C
          5883C556A70F0248192A0FC6E067A5984720A3C6C905C35A72DB9C53D18F0179
          55592BD70B5385D50A691C1157E9C780924AE1C1951422AED11B41269341CB35
          354862B8F06DC902C3E3CB1D03D6FC2E0C287E58F0BD212335A990C566A33FEC
          C1EDF63190CB24D0589E02D8626D070650F0B8520D5C0E0B767D7E300CBD1BE9
          DE2A69C680AB4F4FCEEB75BA3489C34D03BDC740B5FE1350940C1A2FA98885BA
          762348D13B0678FD01900B23A1B7B777ABCB5E5E43004F4ECCE9D3D37512BBCB
          0F3E04B8D31E4CB9E9B28AD4E26EFB2802C889851D5458119F03FD7D2FB7BADD
          950450DC94FBB52153AB2AE47038E0D8A1A1AE3308B87F51458A57DF89010A68
          DEB76073B8E1EDE0E0C880BF8258C8BB91BA888A28AA4E8C97C28ACD0B8F5E2D
          92D4EF5D085A68E9FB4132C11624B15CF8386182F7534B1D1F9873A488CA2CB1
          B5AC24E167EDB1AC4C8D242E8A9C4468D8515D62A3830D85C5668B030C86370B
          338182363373B4FFA091CEC77F293D2270D4E71DCF89918B04C48AD3439353C0
          001CCB163B1806875C16AFB0759C7336D848E1AD7C46F4B94811F1BB42AD49D3
          C42B2810C7F1C9A9586CDB605E5985D9998985CD80BC67925BFC772B875FA694
          C8E5FC84804927A037B223991D2D4DA3966678260748A6D72374F3ABECD47F2F
          D3FF5CE73FDBCD7BC47F4F9B730000000049454E44AE426082}
      end>
    Bitmap = {}
  end
end
