﻿inherited FormEditCargoMassFill: TFormEditCargoMassFill
  Caption = #1052#1072#1089#1089#1086#1074#1086#1077' '#1079#1072#1087#1086#1083#1085#1077#1085#1080#1077' '#1076#1072#1085#1085#1099#1093
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label2: TLabel
    Enabled = False
  end
  inherited sbContainer: TSpeedButton
    Enabled = False
  end
  inherited sbCarriage: TSpeedButton
    Enabled = False
  end
  inherited Label4: TLabel
    Enabled = False
  end
  inherited Label5: TLabel
    Enabled = False
  end
  inherited Label6: TLabel
    Enabled = False
  end
  inherited Label7: TLabel
    Enabled = False
  end
  inherited Label10: TLabel
    Enabled = False
  end
  inherited Label11: TLabel
    Enabled = False
  end
  inherited Label12: TLabel
    Enabled = False
  end
  inherited Label13: TLabel
    Enabled = False
  end
  inherited Label3: TLabel
    Enabled = False
  end
  inherited Label19: TLabel
    Enabled = False
  end
  inherited Label20: TLabel
    Enabled = False
  end
  inherited edSealNumber: TDBEditEh
    Enabled = False
  end
  inherited nuWeightSender: TDBNumberEditEh
    Enabled = False
  end
  inherited nuWeightDocument: TDBNumberEditEh
    Enabled = False
  end
  inherited nuWeightFact: TDBNumberEditEh
    Enabled = False
  end
  inherited edContainerNum: TDBEditEh
    Enabled = False
    EditMask = 'cccc9999999;0;?'
  end
  inherited edCarriageNum: TDBEditEh
    Enabled = False
    EditMask = '99999999;0;?'
  end
  inherited edContFut: TDBEditEh
    Enabled = False
  end
  inherited edCarType: TDBEditEh
    Enabled = False
  end
  inherited cbIsEmpty: TDBCheckBoxEh
    OnClick = nil
  end
  inherited laContainerOwner: TDBSQLLookUp
    Enabled = False
  end
  inherited laCarriageOwner: TDBSQLLookUp
    Enabled = False
  end
  inherited dsLocal: TDataSource
    DataSet = FormCargoSheet.meMass
  end
end
