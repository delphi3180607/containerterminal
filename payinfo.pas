﻿unit payinfo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask;

type
  TFormPayInfo = class(TFormEdit)
    edPaidSumm: TDBNumberEditEh;
    cbPaidFull: TDBCheckBoxEh;
    dtPaidFullDate: TDBDateTimeEditEh;
    edPayDocs: TDBEditEh;
    dtConfirmed: TDBDateTimeEditEh;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPayInfo: TFormPayInfo;

implementation

{$R *.dfm}

procedure TFormPayInfo.FormShow(Sender: TObject);
begin
  inherited;
  if (dtConfirmed.Value <> null)
  and (dtPaidFullDate.Value <> null)
  and (dtConfirmed.Value > dtPaidFullDate.Value)
  and (cbPaidFull.Checked)
  then
  begin
    cbPaidFull.ReadOnly := true;
    cbPaidFull.Enabled := true;
  end;
end;

end.
