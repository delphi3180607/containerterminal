﻿inherited FormEditMatrixSector: TFormEditMatrixSector
  Caption = #1057#1077#1082#1090#1086#1088
  ClientHeight = 333
  ClientWidth = 645
  ExplicitWidth = 651
  ExplicitHeight = 361
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 292
    Width = 645
    TabOrder = 10
    ExplicitTop = 292
    ExplicitWidth = 645
    inherited btnCancel: TButton
      Left = 529
      ExplicitLeft = 529
    end
    inherited btnOk: TButton
      Left = 410
      ExplicitLeft = 410
    end
  end
  object edCode: TDBEditEh [1]
    Left = 8
    Top = 30
    Width = 241
    Height = 24
    ControlLabel.Width = 157
    ControlLabel.Height = 16
    ControlLabel.Caption = #1050#1088#1072#1090#1082#1086#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edName: TDBEditEh [2]
    Left = 8
    Top = 86
    Width = 625
    Height = 24
    ControlLabel.Width = 66
    ControlLabel.Height = 16
    ControlLabel.Caption = #1054#1087#1080#1089#1072#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object neOrder: TDBNumberEditEh [3]
    Left = 264
    Top = 30
    Width = 121
    Height = 24
    ControlLabel.Width = 57
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1086#1088#1103#1076#1086#1082
    ControlLabel.Visible = True
    DataField = 'place_order'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object edRows: TDBNumberEditEh [4]
    Left = 8
    Top = 142
    Width = 121
    Height = 24
    ControlLabel.Width = 68
    ControlLabel.Height = 16
    ControlLabel.Caption = #1064#1090#1072#1073#1077#1083#1077#1081
    ControlLabel.Visible = True
    DataField = 'rows'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Visible = True
  end
  object edStaks: TDBNumberEditEh [5]
    Left = 8
    Top = 197
    Width = 121
    Height = 24
    ControlLabel.Width = 50
    ControlLabel.Height = 16
    ControlLabel.Caption = #1057#1077#1082#1094#1080#1081
    ControlLabel.Visible = True
    DataField = 'stacks'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Visible = True
  end
  object edLevels: TDBNumberEditEh [6]
    Left = 8
    Top = 251
    Width = 121
    Height = 24
    ControlLabel.Width = 48
    ControlLabel.Height = 16
    ControlLabel.Caption = #1071#1088#1091#1089#1086#1074
    ControlLabel.Visible = True
    DataField = 'levels'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    Visible = True
  end
  object cbIsActive: TDBCheckBoxEh [7]
    Left = 426
    Top = 32
    Width = 159
    Height = 17
    Caption = #1048#1089#1087#1086#1083#1100#1079#1091#1077#1090#1089#1103'?'
    DataField = 'isactive'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 2
  end
  object cbRowOrder: TDBCheckBoxEh [8]
    Left = 264
    Top = 146
    Width = 217
    Height = 17
    Caption = #1054#1073#1088#1072#1090#1085#1099#1081' '#1087#1086#1088#1103#1076#1086#1082' '#1096#1090#1072#1073#1077#1083#1077#1081
    DataField = 'roworderreverse'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 5
  end
  object cbLevelOrder: TDBCheckBoxEh [9]
    Left = 264
    Top = 251
    Width = 217
    Height = 17
    Caption = #1054#1073#1088#1072#1090#1085#1099#1081' '#1087#1086#1088#1103#1076#1086#1082' '#1103#1088#1091#1089#1086#1074
    DataField = 'levelorderreverse'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 9
  end
  object cbStackOrder: TDBCheckBoxEh [10]
    Left = 264
    Top = 199
    Width = 217
    Height = 17
    Caption = #1054#1073#1088#1072#1090#1085#1099#1081' '#1087#1086#1088#1103#1076#1086#1082' '#1089#1077#1082#1094#1080#1081
    DataField = 'stackorderreverse'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 7
  end
  inherited dsLocal: TDataSource
    Left = 160
    Top = 272
  end
  inherited qrAux: TADOQuery
    Left = 200
    Top = 272
  end
end
