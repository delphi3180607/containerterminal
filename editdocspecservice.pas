﻿unit editdocspecservice;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, DBGridEh, Vcl.Buttons, Vcl.Mask, DBCtrlsEh, DBLookupEh,
  Data.Win.ADODB, functions, DBSQLLookUp;

type
  TFormEditDocSpecService = class(TFormEdit)
    luService: TDBLookupComboboxEh;
    Label2: TLabel;
    sbService: TSpeedButton;
    Цена: TLabel;
    nePrice: TDBNumberEditEh;
    Label1: TLabel;
    neAmount: TDBNumberEditEh;
    Label3: TLabel;
    neSumma: TDBNumberEditEh;
    Label4: TLabel;
    sbObject: TSpeedButton;
    luObject: TDBSQLLookUp;
    ssObject: TADOLookUpSqlSet;
    procedure sbServiceClick(Sender: TObject);
    procedure sbObjectClick(Sender: TObject);
    procedure nePriceChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDocSpecService: TFormEditDocSpecService;

implementation

{$R *.dfm}

uses servicekinds, containers, dmu;

procedure TFormEditDocSpecService.nePriceChange(Sender: TObject);
begin
  try
    neSumma.Value := nePrice.Value * neAmount.Value;
  except
    //--
  end;
end;

procedure TFormEditDocSpecService.sbObjectClick(Sender: TObject);
begin
  SFDE(TFormContainers,self.luObject);
end;

procedure TFormEditDocSpecService.sbServiceClick(Sender: TObject);
begin
  SFD(FormServiceKinds,self.luService);
end;

end.
