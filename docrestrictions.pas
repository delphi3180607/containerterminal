﻿unit DocRestrictions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls;

type
  TFormDocRestrictions = class(TFormGrid)
    procedure meDataAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormDocRestrictions: TFormDocRestrictions;

implementation

{$R *.dfm}

uses editdocrestrict;

procedure TFormDocRestrictions.Init;
begin
  self.tablename := 'doccargorestrictions';
  meData.Close;
  drvData.SelectCommand.Parameters.ParamByName('doc_id').Value := self.parent_id;
  inherited;
  if (not Assigned(FormEditDocRestriction)) or (FormEditDocRestriction=nil) then
  Application.CreateForm(TFormEditDocRestriction, FormEditDocRestriction);

  self.formEdit := FormEditDocRestriction;
end;


procedure TFormDocRestrictions.meDataAfterInsert(DataSet: TDataSet);
begin
  inherited;
  meData.FieldByName('doc_id').Value := self.parent_id;
  meData.FieldByName('isactive').Value := 1;
  meData.FieldByName('datestart').Value := now();
end;

end.
