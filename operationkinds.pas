﻿unit operationkinds;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport;

type
  TFormOperationKinds = class(TFormGrid)
    drvState: TADODataDriverEh;
    meState: TMemTableEh;
    dsState: TDataSource;
    pmState: TPopupMenu;
    MenuItem8: TMenuItem;
    Splitter2: TSplitter;
    drvObjects: TADODataDriverEh;
    meObjects: TMemTableEh;
    dsObjects: TDataSource;
    Panel1: TPanel;
    dgStates: TDBGridEh;
    plTool: TPanel;
    sbAddState: TPngSpeedButton;
    sbDelState: TPngSpeedButton;
    sbEditState: TPngSpeedButton;
    Splitter1: TSplitter;
    drvStateObject: TADODataDriverEh;
    meStateObject: TMemTableEh;
    dsStateObject: TDataSource;
    plObjects: TPanel;
    Splitter3: TSplitter;
    Panel2: TPanel;
    sbAddObject: TPngSpeedButton;
    sbDeleteObject: TPngSpeedButton;
    sbEditObject: TPngSpeedButton;
    dgObjects: TDBGridEh;
    Panel3: TPanel;
    DBGridEh1: TDBGridEh;
    Panel4: TPanel;
    sbAddObjectRoute: TPngSpeedButton;
    sbDeleteObjectRoute: TPngSpeedButton;
    sbEditObjectRoute: TPngSpeedButton;
    Panel5: TPanel;
    dgServices: TDBGridEh;
    Splitter4: TSplitter;
    drvServices: TADODataDriverEh;
    meServices: TMemTableEh;
    dsServices: TDataSource;
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure sbAddStateClick(Sender: TObject);
    procedure sbDelStateClick(Sender: TObject);
    procedure sbEditStateClick(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure sbAddObjectClick(Sender: TObject);
    procedure sbEditObjectClick(Sender: TObject);
    procedure sbDeleteObjectClick(Sender: TObject);
    procedure meObjectsAfterScroll(DataSet: TDataSet);
    procedure sbAddObjectRouteClick(Sender: TObject);
    procedure sbDeleteObjectRouteClick(Sender: TObject);
    procedure sbEditObjectRouteClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormOperationKinds: TFormOperationKinds;

implementation

{$R *.dfm}

uses editoperationkind, editoperationrout, functions, objectlinks;


procedure TFormOperationKinds.Init;
begin
  inherited;
  tablename := 'operationkinds';
  self.meServices.Open;
  self.meObjects.Open;
  self.meState.Open;
  self.meStateObject.Open;
  self.formEdit := FormEditOperationKind;
end;


procedure TFormOperationKinds.meDataAfterScroll(DataSet: TDataSet);
begin
  if self.meData.FieldByName('id').AsString <> '' then
  begin

    meState.Filter := 'operationkind_id = '+self.meData.FieldByName('id').AsString;
    meState.Filtered := true;

    meObjects.Filter := 'operationkind_id = '+self.meData.FieldByName('id').AsString;
    meObjects.Filtered := true;

  end else
  begin

    meState.Filter := 'operationkind_id = -1000';
    meState.Filtered := true;

    meObjects.Filter := 'operationkind_id = -1000';
    meObjects.Filtered := true;

  end;

  meObjectsAfterScroll(meStateObject);

end;

procedure TFormOperationKinds.MenuItem8Click(Sender: TObject);
begin
  meState.Close;
  meState.Open;
end;

procedure TFormOperationKinds.meObjectsAfterScroll(DataSet: TDataSet);
begin

  if not meObjects.Active then meObjects.Open;
  if not meStateObject.Active then meStateObject.Open;

  if self.meObjects.FieldByName('id').AsString <> '' then
  begin

    meStateObject.Filter := 'operationobjectkind_id = '+self.meObjects.FieldByName('id').AsString;
    meStateObject.Filtered := true;

  end else
  begin

    meStateObject.Filter := 'operationobjectkind_id = -1000';
    meStateObject.Filtered := true;

  end;

end;

procedure TFormOperationKinds.sbAddObjectClick(Sender: TObject);
begin

  FormEditObjectLink.dsLocal.DataSet := nil;
  meObjects.Append;
  FormEditObjectLink.dsLocal.DataSet := meObjects;
  meObjects.FieldByName('operationkind_id').Value := self.meData.FieldByName('id').AsInteger;
  FormEditObjectLink.ShowModal;

  if FormEditObjectLink.ModalResult = mrOk then
  begin
     meObjects.FieldByName('operationkind_id').Value := self.meData.FieldByName('id').AsInteger;
     meObjects.Post;
  end else
  begin
     meObjects.Cancel;
  end;

  if self.meData.FieldByName('id').AsString <> '' then
  begin
    meObjects.Filter := 'operationkind_id = '+self.meData.FieldByName('id').AsString;
    meObjects.Filtered := true;
    meObjects.Refresh;
  end;

end;

procedure TFormOperationKinds.sbAddObjectRouteClick(Sender: TObject);
begin

  FormEditOperationRoute.dsLocal.DataSet := nil;
  meStateObject.Append;
  meStateObject.FieldByName('operationobjectkind_id').Value := self.meObjects.FieldByName('id').AsInteger;
  FormEditOperationRoute.dsLocal.DataSet := FormOperationKinds.meStateObject;
  FormEditOperationRoute.ShowModal;

  if FormEditOperationRoute.ModalResult = mrOk then
  begin
     meStateObject.FieldByName('operationobjectkind_id').Value := self.meObjects.FieldByName('id').AsInteger;
     meStateObject.Post;
  end else
  begin
     meStateObject.Cancel;
  end;

  if self.meObjects.FieldByName('id').AsString <> '' then
  begin
    meStateObject.Filter := 'operationobjectkind_id = '+self.meObjects.FieldByName('id').AsString;
    meStateObject.Filtered := true;
    meStateObject.Refresh;
  end;


end;

procedure TFormOperationKinds.sbAddStateClick(Sender: TObject);
begin

  FormEditOperationRoute.dsLocal.DataSet := nil;
  meState.Append;
  meState.FieldByName('operationkind_id').Value := self.meData.FieldByName('id').AsInteger;
  FormEditOperationRoute.dsLocal.DataSet := FormOperationKinds.meState;
  FormEditOperationRoute.ShowModal;

  if FormEditOperationRoute.ModalResult = mrOk then
  begin
     meState.FieldByName('operationkind_id').Value := self.meData.FieldByName('id').AsInteger;
     meState.Post;
  end else
  begin
     meState.Cancel;
  end;

  if self.meData.FieldByName('id').AsString <> '' then
  begin
    meState.Filter := 'operationkind_id = '+self.meData.FieldByName('id').AsString;
    meState.Filtered := true;
    meState.Refresh;
  end;

end;

procedure TFormOperationKinds.sbDeleteObjectClick(Sender: TObject);
begin

  if fQYN('Удалить запись ?') then
  begin
     self.meObjects.Delete;
  end;

  if self.meData.FieldByName('id').AsString <> '' then
  begin
    meObjects.Filter := 'operationkind_id = '+self.meData.FieldByName('id').AsString;
    meObjects.Filtered := true;
    meObjects.Refresh;
  end;

end;

procedure TFormOperationKinds.sbDeleteObjectRouteClick(Sender: TObject);
begin


  if fQYN('Удалить запись ?') then
  begin
     meStateObject.Delete;
  end;

  if self.meObjects.FieldByName('id').AsString <> '' then
  begin
    meStateObject.Filter := 'operationobjectkind_id = '+self.meObjects.FieldByName('id').AsString;
    meState.Filtered := true;
    meState.Refresh;
  end;


end;

procedure TFormOperationKinds.sbDelStateClick(Sender: TObject);
begin

  if fQYN('Удалить запись ?') then
  begin
     self.meState.Delete;
  end;

  if self.meData.FieldByName('id').AsString <> '' then
  begin
    meState.Filter := 'operationkind_id = '+self.meData.FieldByName('id').AsString;
    meState.Filtered := true;
    meState.Refresh;
  end;

end;

procedure TFormOperationKinds.sbEditObjectClick(Sender: TObject);
begin

  FormEditObjectLink.dsLocal.DataSet := nil;
  meObjects.Edit;
  FormEditObjectLink.dsLocal.DataSet := meObjects;
  meObjects.FieldByName('operationkind_id').Value := self.meData.FieldByName('id').AsInteger;
  FormEditObjectLink.ShowModal;

  if FormEditObjectLink.ModalResult = mrOk then
  begin
     meObjects.FieldByName('operationkind_id').Value := self.meData.FieldByName('id').AsInteger;
     meObjects.Post;
  end else
  begin
     meObjects.Cancel;
  end;

  if self.meData.FieldByName('id').AsString <> '' then
  begin
    meObjects.Filter := 'operationkind_id = '+self.meData.FieldByName('id').AsString;
    meObjects.Filtered := true;
    meObjects.Refresh;
  end;

end;

procedure TFormOperationKinds.sbEditObjectRouteClick(Sender: TObject);
begin

  FormEditOperationRoute.dsLocal.DataSet := nil;
  meStateObject.Edit;
  meStateObject.FieldByName('operationobjectkind_id').Value := self.meObjects.FieldByName('id').AsInteger;
  FormEditOperationRoute.dsLocal.DataSet := FormOperationKinds.meStateObject;
  FormEditOperationRoute.ShowModal;

  if FormEditOperationRoute.ModalResult = mrOk then
  begin
     meStateObject.FieldByName('operationobjectkind_id').Value := self.meObjects.FieldByName('id').AsInteger;
     meStateObject.Post;
  end else
  begin
     meStateObject.Cancel;
  end;

  if self.meObjects.FieldByName('id').AsString <> '' then
  begin
    meStateObject.Filter := 'operationobjectkind_id = '+self.meObjects.FieldByName('id').AsString;
    meStateObject.Filtered := true;
    meStateObject.Refresh;
  end;

end;

procedure TFormOperationKinds.sbEditStateClick(Sender: TObject);
begin

  FormEditOperationRoute.dsLocal.DataSet := nil;
  meState.Edit;
  FormEditOperationRoute.dsLocal.DataSet := FormOperationKinds.meState;
  FormEditOperationRoute.ShowModal;

  if FormEditOperationRoute.ModalResult = mrOk then
  begin
     meState.Post;
  end else
  begin
     meState.Cancel;
  end;

  if self.meData.FieldByName('id').AsString <> '' then
  begin
    meState.Filter := 'operationkind_id = '+self.meData.FieldByName('id').AsString;
    meState.Filtered := true;
    meState.Refresh;
  end;

end;

end.
