﻿unit unloadplan;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, loadplan, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls, functions;

type
  TFormUnLoadPlan = class(TFormLoadPlan)
    procedure FormCreate(Sender: TObject);
    procedure meDataBeforePost(DataSet: TDataSet);
    procedure meDataAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
  end;

var
  FormUnLoadPlan: TFormUnLoadPlan;

implementation

{$R *.dfm}


procedure TFormUnLoadPlan.FormCreate(Sender: TObject);
begin
  inherited;
  system_section := 'income_unload';
end;

procedure TFormUnLoadPlan.meDataAfterPost(DataSet: TDataSet);
begin
  inherited;
  Screen.Cursor := crDefault;
end;

procedure TFormUnLoadPlan.meDataBeforePost(DataSet: TDataSet);
begin
  inherited;
  Screen.Cursor := crHourGlass;
end;

end.
