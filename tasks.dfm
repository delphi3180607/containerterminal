﻿inherited FormTasks: TFormTasks
  ActiveControl = dgData
  Caption = #1047#1072#1076#1072#1095#1080
  ClientHeight = 513
  ClientWidth = 933
  ExplicitWidth = 949
  ExplicitHeight = 551
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter3: TSplitter [0]
    Left = 0
    Top = 310
    Width = 933
    Height = 7
    Cursor = crVSplit
    Align = alBottom
    Beveled = True
    ExplicitTop = 492
    ExplicitWidth = 695
  end
  inherited plBottom: TPanel
    Top = 472
    Width = 933
    ExplicitTop = 472
    ExplicitWidth = 933
    inherited btnOk: TButton
      Left = 698
      ExplicitLeft = 698
    end
    inherited btnCancel: TButton
      Left = 817
      ExplicitLeft = 817
    end
  end
  inherited plAll: TPanel
    Width = 933
    Height = 310
    ExplicitWidth = 933
    ExplicitHeight = 310
    inherited plTop: TPanel
      Width = 933
      ExplicitWidth = 933
      inherited btTool: TPngSpeedButton
        Left = 881
        ExplicitLeft = 881
      end
    end
    inherited dgData: TDBGridEh
      Width = 933
      Height = 281
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'start_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          Width = 149
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'deliverytype'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
          Width = 130
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'forwarder'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          Width = 137
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 125
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer'
          Footers = <>
          Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
          Width = 119
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'object_type'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1086#1073#1100#1077#1082#1090#1072
          Width = 110
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnum'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088', '#1086#1087#1080#1089#1072#1085#1080#1077
          Width = 102
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'plan_date'
          Footers = <>
          Title.Caption = #1055#1083#1072#1085#1086#1074#1072#1103' '#1076#1072#1090#1072
          Width = 126
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'fact_date'
          Footers = <>
          Title.Caption = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1072#1103' '#1076#1072#1090#1072
        end>
    end
  end
  object dgLinkedObjects: TDBGridEh [3]
    Left = 0
    Top = 317
    Width = 933
    Height = 155
    Align = alBottom
    BorderStyle = bsNone
    DynProps = <>
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    IndicatorOptions = [gioShowRowIndicatorEh]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
    ParentFont = False
    ReadOnly = True
    SelectionDrawParams.DrawFocusFrame = True
    SelectionDrawParams.DrawFocusFrameStored = True
    TabOrder = 2
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -11
    TitleParams.Font.Name = 'Verdana'
    TitleParams.Font.Style = [fsBold]
    TitleParams.ParentFont = False
    TitleParams.FillStyle = cfstGradientEh
    Columns = <
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'object_type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Footers = <>
        Title.Caption = #1058#1080#1087' '#1086#1073#1098#1077#1082#1090#1072
        Width = 169
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'cnum'
        Footers = <>
        Title.Caption = #1053#1086#1084#1077#1088
        Width = 102
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'kind_code'
        Footers = <>
        Title.Caption = #1042#1080#1076
        Width = 138
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'owner_code'
        Footers = <>
        Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
        Width = 135
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'state_code'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = [fsItalic]
        Footers = <>
        Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
        Width = 134
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'dlvtype_code'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7697781
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        Footers = <>
        Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
        Width = 125
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select '
      
        ' (select code from counteragents c1 where c1.id =   t.forwarder_' +
        'id) as  forwarder,'
      
        ' (select code from counteragents c2 where c2.id =   t.consignee_' +
        'id) as  consignee,'
      
        ' (select code from counteragents c3 where c3.id =   t.payer_id) ' +
        'as  payer,'
      
        ' (select code from deliverytypes t4 where t4.id =   t.deliveryty' +
        'pe_id) as  deliverytype,'
      ' o.cnum,'
      ' o.object_type,'
      ' t.root_doc_id,'
      ' t.deliverytype_id,'
      ' t.start_date,'
      ' t.plan_date,'
      ' t.fact_date,'
      ' t.forwarder_id,'
      ' t.consignee_id,'
      ' t.payer_id,'
      ' t.object_id'
      'from tasks t '
      'left outer join v_objects o on (t.object_id = o.id)'
      'order by t.start_date')
    GetrecCommand.CommandText.Strings = (
      'select '
      
        ' (select code from counteragents c1 where c1.id =   t.forwarder_' +
        'id) as  forwarder,'
      
        ' (select code from counteragents c2 where c2.id =   t.consignee_' +
        'id) as  consignee,'
      
        ' (select code from counteragents c3 where c3.id =   t.payer_id) ' +
        'as  payer,'
      
        ' (select code from deliverytypes t4 where t4.id =   t.deliveryty' +
        'pe_id) as  deliverytype,'
      ' o.cnum,'
      ' o.object_type,'
      ' t.root_doc_id,'
      ' t.deliverytype_id,'
      ' t.start_date,'
      ' t.plan_date,'
      ' t.fact_date,'
      ' t.forwarder_id,'
      ' t.consignee_id,'
      ' t.payer_id,'
      ' t.object_id'
      'from tasks t '
      'left outer join v_objects o on (t.object_id = o.id)'
      'where t.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
    inherited aDelete: TAction
      Enabled = False
    end
    inherited aEdit: TAction
      Enabled = False
    end
  end
end
