﻿inherited FromEditShippingOptionRule: TFromEditShippingOptionRule
  Caption = #1055#1088#1072#1074#1080#1083#1086' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#1080#1103' '#1074#1072#1088#1080#1072#1085#1090#1072' '#1076#1086#1089#1090#1072#1074#1082#1080
  ClientHeight = 626
  ClientWidth = 484
  OnActivate = FormActivate
  ExplicitWidth = 490
  ExplicitHeight = 654
  PixelsPerInch = 96
  TextHeight = 16
  object Label18: TLabel [0]
    Left = 13
    Top = 172
    Width = 190
    Height = 16
    Caption = #1044#1086#1087'.'#1091#1089#1083#1091#1075#1080' ('#1087#1077#1088#1077#1095#1080#1089#1083#1077#1085#1080#1077')'
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 511
    Width = 281
    Height = 16
    Caption = '* - '#1083#1102#1073#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077', '#1074' '#1090#1086#1084' '#1095#1080#1089#1083#1077' '#1087#1091#1089#1090#1086#1077
  end
  object Label2: TLabel [2]
    Left = 7
    Top = 533
    Width = 270
    Height = 16
    Caption = '# - '#1086#1073#1103#1079#1072#1090#1077#1083#1100#1085#1086#1077' '#1086#1090#1089#1091#1090#1089#1090#1074#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103
  end
  object Label3: TLabel [3]
    Left = 12
    Top = 555
    Width = 244
    Height = 16
    Caption = '! - '#1086#1073#1103#1079#1072#1090#1077#1083#1100#1085#1086#1077' '#1085#1072#1083#1080#1095#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103
  end
  inherited plBottom: TPanel
    Top = 585
    Width = 484
    TabOrder = 8
    ExplicitTop = 531
    ExplicitWidth = 484
    inherited btnCancel: TButton
      Left = 368
      ExplicitLeft = 368
    end
    inherited btnOk: TButton
      Left = 249
      ExplicitLeft = 249
    end
  end
  object luShippingOptions: TDBSQLLookUp [5]
    Left = 13
    Top = 455
    Width = 308
    Height = 22
    TabStop = False
    ControlLabel.Width = 123
    ControlLabel.Height = 16
    ControlLabel.Caption = #1042#1072#1088#1080#1072#1085#1090' '#1076#1086#1089#1090#1072#1074#1082#1080
    ControlLabel.Visible = True
    DataField = 'shippingoption_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 7
    Visible = True
    SqlSet = ssShippingOption
    RowCount = 0
  end
  object edPayer: TDBEditEh [6]
    Left = 13
    Top = 30
    Width = 434
    Height = 22
    Color = clCream
    ControlLabel.Width = 146
    ControlLabel.Height = 16
    ControlLabel.Caption = #1050#1083#1080#1077#1085#1090' ('#1087#1083#1072#1090#1077#1083#1100#1097#1080#1082')'
    ControlLabel.Visible = True
    DataField = 'payer_code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edExtraServices: TDBEditEh [7]
    Left = 13
    Top = 192
    Width = 429
    Height = 22
    TabStop = False
    Color = clCream
    DataField = 'extra_services'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    Visible = False
  end
  object edDirection: TDBEditEh [8]
    Left = 13
    Top = 81
    Width = 434
    Height = 22
    Color = clCream
    ControlLabel.Width = 132
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1086#1088#1090' ('#1095#1072#1089#1090#1100' '#1089#1083#1086#1074#1072')'
    ControlLabel.Visible = True
    DataField = 'direction_code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object edStation: TDBEditEh [9]
    Left = 13
    Top = 133
    Width = 434
    Height = 22
    Color = clCream
    ControlLabel.Width = 241
    ControlLabel.Height = 16
    ControlLabel.Caption = #1057#1090#1072#1085#1094#1080#1103' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1103' ('#1095#1072#1089#1090#1100' '#1089#1083#1086#1074#1072')'
    ControlLabel.Visible = True
    DataField = 'station_code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object cbServices: TCheckListBox [10]
    Left = 12
    Top = 192
    Width = 435
    Height = 169
    OnClickCheck = cbServicesClick
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ItemHeight = 14
    Items.Strings = (
      '')
    ParentFont = False
    TabOrder = 4
    OnClick = cbServicesClick
  end
  object nePriority: TDBNumberEditEh [11]
    Left = 323
    Top = 409
    Width = 121
    Height = 24
    ControlLabel.Width = 71
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1088#1080#1086#1088#1080#1090#1077#1090
    ControlLabel.Visible = True
    DataField = 'priority'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    TabOrder = 6
    Visible = True
  end
  object cbExclude: TDBCheckBoxEh [12]
    Left = 13
    Top = 369
    Width = 268
    Height = 17
    Caption = #1048#1089#1082#1083#1102#1095#1072#1090#1100' '#1087#1077#1088#1077#1095#1080#1089#1083#1077#1085#1085#1099#1077' '#1091#1089#1083#1091#1075#1080
    DataField = 'exclude_sign'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 5
  end
  inherited dsLocal: TDataSource
    Left = 445
    Top = 442
  end
  inherited qrAux: TADOQuery
    SQL.Strings = (
      'select * from servicekinds k where k.isextra=1 order by scode')
    Left = 405
    Top = 434
  end
  object ssShippingOption: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from shippingoptions order by code')
    DownSql.Strings = (
      'select * from shippingoptions order by code')
    InitSql.Strings = (
      'select * from shippingoptions where id = @id'
      '')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 162
    Top = 459
  end
end
