﻿inherited FormObjectMove: TFormObjectMove
  Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1086#1073#1100#1077#1082#1090#1086#1074
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1086#1073#1100#1077#1082#1090#1072
          Width = 133
        end
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
          Width = 139
        end
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1053#1072#1095#1072#1083#1100#1085#1099#1081' '#1072#1076#1088#1077#1089
          Width = 157
        end
        item
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Caption = #1050#1086#1085#1077#1095#1085#1099#1081' '#1072#1076#1088#1077#1089
          Width = 165
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from objectsmoves')
    UpdateCommand.CommandText.Strings = (
      'update objectsmoves'
      'set'
      '  object_type = :object_type,'
      '  date_change = :date_change,'
      '  start_address_id = :start_address_id,'
      '  end_address_id = :end_address_id'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'object_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'date_change'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'start_address_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'end_address_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into objectsmoves'
      '  (object_type, date_change, start_address_id, end_address_id)'
      'values'
      
        '  (:object_type, :date_change, :start_address_id, :end_address_i' +
        'd)')
    InsertCommand.Parameters = <
      item
        Name = 'object_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'date_change'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'start_address_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'end_address_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from objectsmoves where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from objectsmoves where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
end
