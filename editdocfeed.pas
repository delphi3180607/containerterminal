﻿unit EditDocFeed;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBSQLLookUp, DBCtrlsEh, Vcl.Mask, Vcl.Buttons;

type
  TFormEditDocFeed = class(TFormEdit)
    Label1: TLabel;
    Label2: TLabel;
    sbContainer: TSpeedButton;
    sbCarriage: TSpeedButton;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Label3: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    edSealNumber: TDBEditEh;
    nuWeightSender: TDBNumberEditEh;
    nuWeightDocument: TDBNumberEditEh;
    nuWeightFact: TDBNumberEditEh;
    stState: TDBEditEh;
    edContainerNum: TDBEditEh;
    edCarriageNum: TDBEditEh;
    edTrainNumber: TDBEditEh;
    meText: TDBMemoEh;
    edContFut: TDBEditEh;
    edCarType: TDBEditEh;
    edDocNumber: TDBEditEh;
    dtDocDate: TDBDateTimeEditEh;
    laContainerOwner: TDBSQLLookUp;
    laCarriageOwner: TDBSQLLookUp;
    Label8: TLabel;
    laFront: TDBSQLLookUp;
    ssFront: TADOLookUpSqlSet;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDocFeed: TFormEditDocFeed;

implementation

{$R *.dfm}

end.
