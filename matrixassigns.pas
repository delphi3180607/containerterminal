﻿unit MatrixAssigns;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Mask, DBCtrlsEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton;

type
  TFormMatrixAssigns = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure PartialInit; override;
  end;

var
  FormMatrixAssigns: TFormMatrixAssigns;

implementation

{$R *.dfm}

procedure TFormMatrixAssigns.PartialInit;
begin
  inherited;
  meData.Close;
  meData.Open;
end;


end.
