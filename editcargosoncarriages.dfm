﻿inherited FormEditCargosOnCarriages: TFormEditCargosOnCarriages
  Caption = #1043#1088#1091#1079' '#1087#1086' '#1046#1044' - '#1087#1077#1088#1074#1080#1095#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100
  ClientHeight = 581
  ClientWidth = 929
  Font.Height = -12
  ExplicitWidth = 935
  ExplicitHeight = 609
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel [0]
    Left = 358
    Top = 13
    Width = 82
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1087#1086#1077#1079#1076#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel [1]
    Left = 10
    Top = 65
    Width = 62
    Height = 13
    Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbContainer: TSpeedButton [2]
    Left = 592
    Top = 82
    Width = 31
    Height = 24
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = sbContainerClick
  end
  object sbCarriage: TSpeedButton [3]
    Left = 592
    Top = 153
    Width = 31
    Height = 24
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = sbCarriageClick
  end
  object Label4: TLabel [4]
    Left = 10
    Top = 196
    Width = 84
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel [5]
    Left = 10
    Top = 254
    Width = 125
    Height = 13
    Caption = #1042#1077#1089' ('#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100'), '#1090'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel [6]
    Left = 184
    Top = 254
    Width = 94
    Height = 13
    Caption = #1042#1077#1089' '#1087#1086' '#1046#1044#1053', '#1082#1075'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel [7]
    Left = 346
    Top = 254
    Width = 87
    Height = 13
    Caption = #1042#1077#1089' ('#1092#1072#1082#1090'), '#1082#1075'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel [8]
    Left = 10
    Top = 309
    Width = 106
    Height = 13
    Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbForwarder: TSpeedButton [9]
    Left = 564
    Top = 325
    Width = 36
    Height = 24
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = sbForwarderClick
  end
  object Label9: TLabel [10]
    Left = 10
    Top = 360
    Width = 101
    Height = 13
    Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbConsignee: TSpeedButton [11]
    Left = 564
    Top = 376
    Width = 36
    Height = 24
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = sbConsigneeClick
  end
  object Label10: TLabel [12]
    Left = 11
    Top = 85
    Width = 36
    Height = 13
    Caption = #1053#1086#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel [13]
    Left = 11
    Top = 158
    Width = 36
    Height = 13
    Caption = #1053#1086#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel [14]
    Left = 333
    Top = 158
    Width = 76
    Height = 13
    Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel [15]
    Left = 333
    Top = 85
    Width = 76
    Height = 13
    Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TLabel [16]
    Left = 10
    Top = 409
    Width = 72
    Height = 13
    Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbPayer: TSpeedButton [17]
    Left = 564
    Top = 424
    Width = 36
    Height = 24
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = sbPayerClick
  end
  object Label15: TLabel [18]
    Left = 195
    Top = 196
    Width = 57
    Height = 13
    Caption = #1058#1080#1087' '#1075#1088#1091#1079#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel [19]
    Left = 10
    Top = 136
    Width = 34
    Height = 13
    Caption = #1042#1072#1075#1086#1085
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label18: TLabel [20]
    Left = 641
    Top = 12
    Width = 178
    Height = 13
    Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1081' '#1082#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label19: TLabel [21]
    Left = 190
    Top = 85
    Width = 21
    Height = 13
    Caption = #1060#1091#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label20: TLabel [22]
    Left = 190
    Top = 158
    Width = 21
    Height = 13
    Caption = #1060#1091#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbCargoType: TSpeedButton [23]
    Left = 501
    Top = 212
    Width = 31
    Height = 23
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = sbCargoTypeClick
  end
  object Label21: TLabel [24]
    Left = 11
    Top = 13
    Width = 102
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label22: TLabel [25]
    Left = 197
    Top = 13
    Width = 93
    Height = 13
    Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label16: TLabel [26]
    Left = 10
    Top = 488
    Width = 143
    Height = 13
    Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080'/'#1054#1087#1077#1088#1072#1094#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbCopy: TPngSpeedButton [27]
    Left = 315
    Top = 403
    Width = 23
    Height = 22
    Hint = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100
    Flat = True
    OnClick = sbCopyClick
    PngImage.Data = {
      89504E470D0A1A0A0000000D4948445200000010000000100403000000EDDDE2
      520000000373424954080808DBE14FE000000030504C5445FFFFFFE1E7FDCDD7
      F7C9D5FBC3CFF4B3C4F1A4B6EF9BB1EE93AAEB8DA5E98CA0E8829CE67B97E475
      8EE46E89E26480E0729ACC780000001074524E5300FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFC0508B43000000097048597300000AEB00000AEB01828B0D5A000000
      1C74455874536F6674776172650041646F62652046697265776F726B73204353
      3571B5E3360000006E4944415478DA6364E0FFC0C0C0F0518091819F09C8B80F
      62A800197B410C9814142089F00B0319E70418FFFFCA05322C13187F791B0119
      77A319F94F6E003258931919FEAE05324C15818AEF5C6060360299F3EF1C83B2
      20D01C06FE770F8504400C86FF2012C4E07F28C0C000004D7E1AAFAA77017F00
      00000049454E44AE426082}
  end
  object GroupBox1: TGroupBox [28]
    Left = 641
    Top = 320
    Width = 277
    Height = 137
    Caption = #1055#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077' '#1082' '#1101#1083#1077#1082#1090#1088#1086#1089#1077#1090#1080
    TabOrder = 24
    object dtPlugStart: TDBDateTimeEditEh
      Left = 16
      Top = 45
      Width = 209
      Height = 23
      ControlLabel.Width = 123
      ControlLabel.Height = 14
      ControlLabel.Caption = #1053#1072#1095#1072#1083#1086' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103
      ControlLabel.Visible = True
      AutoSize = False
      DataField = 'plugstart'
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      Kind = dtkDateEh
      ParentFont = False
      TabOrder = 0
      Visible = True
    end
    object dtPlugEnd: TDBDateTimeEditEh
      Left = 16
      Top = 96
      Width = 209
      Height = 22
      ControlLabel.Width = 145
      ControlLabel.Height = 14
      ControlLabel.Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103
      ControlLabel.Visible = True
      AutoSize = False
      DataField = 'plugend'
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      Kind = dtkDateEh
      ParentFont = False
      TabOrder = 1
      Visible = True
    end
  end
  inherited plBottom: TPanel
    Top = 540
    Width = 929
    TabOrder = 23
    ExplicitTop = 540
    ExplicitWidth = 929
    inherited btnCancel: TButton
      Left = 813
      Font.Height = -11
      ExplicitLeft = 813
    end
    inherited btnOk: TButton
      Left = 694
      Font.Height = -11
      OnClick = btnOkClick
      ExplicitLeft = 694
    end
  end
  object edSealNumber: TDBEditEh [30]
    Left = 10
    Top = 213
    Width = 173
    Height = 23
    AutoSize = False
    DataField = 'seal_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 10
    Visible = True
  end
  object nuWeightSender: TDBNumberEditEh [31]
    Left = 10
    Top = 271
    Width = 126
    Height = 23
    AutoSize = False
    DataField = 'weight_sender'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 13
    Visible = True
  end
  object nuWeightDocument: TDBNumberEditEh [32]
    Left = 184
    Top = 271
    Width = 121
    Height = 23
    AutoSize = False
    Color = 14810109
    DataField = 'weight_doc'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 14
    Visible = True
  end
  object nuWeightFact: TDBNumberEditEh [33]
    Left = 346
    Top = 271
    Width = 121
    Height = 23
    AutoSize = False
    DataField = 'weight_fact'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 15
    Visible = True
  end
  object edContainerNum: TDBEditEh [34]
    Left = 50
    Top = 82
    Width = 127
    Height = 23
    AutoSize = False
    CharCase = ecUpperCase
    Color = clCream
    DataField = 'container_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    MaxLength = 11
    ParentFont = False
    TabOrder = 3
    Visible = True
    OnKeyPress = edContainerNumKeyPress
    EditMask = 'cccc9999999;0;?'
  end
  object edCarriageNum: TDBEditEh [35]
    Left = 50
    Top = 154
    Width = 123
    Height = 23
    AutoSize = False
    Color = clCream
    DataField = 'carriage_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    MaxLength = 8
    ParentFont = False
    TabOrder = 8
    Visible = True
    EditMask = '99999999;0;?'
  end
  object edTrainNumber: TDBEditEh [36]
    Left = 358
    Top = 32
    Width = 258
    Height = 23
    AutoSize = False
    Color = clCream
    DataField = 'train_num'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object meText: TDBMemoEh [37]
    Left = 641
    Top = 32
    Width = 276
    Height = 245
    AutoSize = False
    DataField = 'cargo_description'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 19
    Visible = True
    WantReturns = True
  end
  object cbGuard: TDBCheckBoxEh [38]
    Left = 10
    Top = 463
    Width = 97
    Height = 17
    Caption = #1054#1093#1088#1072#1085#1072
    DataField = 'guard_sign'
    DataSource = dsLocal
    DynProps = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 20
  end
  object cbReturnCont: TDBCheckBoxEh [39]
    Left = 117
    Top = 463
    Width = 244
    Height = 17
    Caption = #1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1088#1086#1078#1085#1077#1075#1086' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
    DataField = 'emptyreturn_sign'
    DataSource = dsLocal
    DynProps = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 21
  end
  object edContFut: TDBEditEh [40]
    Left = 215
    Top = 114
    Width = 65
    Height = 23
    AutoSize = False
    DataField = 'container_foot'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentColor = True
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
  object edCarType: TDBEditEh [41]
    Left = 215
    Top = 154
    Width = 65
    Height = 23
    AutoSize = False
    Color = clCream
    DataField = 'carriage_type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    Visible = True
  end
  object cbIsEmpty: TDBCheckBoxEh [42]
    Left = 539
    Top = 214
    Width = 97
    Height = 17
    Caption = #1055#1086#1088#1086#1078#1085#1080#1081
    DataField = 'isempty'
    DataSource = dsLocal
    DynProps = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    OnClick = cbIsEmptyClick
  end
  object edDocNumber: TDBEditEh [43]
    Left = 11
    Top = 32
    Width = 172
    Height = 23
    AutoSize = False
    Color = clCream
    DataField = 'doc_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object dtDocDate: TDBDateTimeEditEh [44]
    Left = 197
    Top = 32
    Width = 121
    Height = 23
    AutoSize = False
    DataField = 'doc_date'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object laForwarder: TDBSQLLookUp [45]
    Left = 10
    Top = 327
    Width = 550
    Height = 23
    AutoSize = False
    Color = 14810109
    DataField = 'forwarder_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 16
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object laConsignee: TDBSQLLookUp [46]
    Left = 10
    Top = 377
    Width = 550
    Height = 23
    AutoSize = False
    Color = 14810109
    DataField = 'consignee_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 17
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object laPayer: TDBSQLLookUp [47]
    Left = 10
    Top = 426
    Width = 550
    Height = 23
    AutoSize = False
    Color = 14810109
    DataField = 'payer_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 18
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object laContainerOwner: TDBSQLLookUp [48]
    Left = 413
    Top = 82
    Width = 176
    Height = 23
    AutoSize = False
    Color = 14810109
    DataField = 'container_owner_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 6
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object laCarriageOwner: TDBSQLLookUp [49]
    Left = 413
    Top = 154
    Width = 176
    Height = 23
    AutoSize = False
    Color = 14810109
    DataField = 'carriage_owner_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 7
    Visible = True
    SqlSet = dm.ssCounteragents
    RowCount = 0
  end
  object laCargoType: TDBSQLLookUp [50]
    Left = 193
    Top = 213
    Width = 305
    Height = 23
    AutoSize = False
    DataField = 'cargotype_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 11
    Visible = True
    SqlSet = dm.ssCargoType
    RowCount = 0
  end
  object laDlvTypes: TDBSQLLookUp [51]
    Left = 10
    Top = 505
    Width = 280
    Height = 23
    AutoSize = False
    Color = 14810109
    DataField = 'dlv_type_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 22
    Visible = True
    SqlSet = ssDlvTypes
    RowCount = 0
  end
  object leContainerKind: TDBSQLLookUp [52]
    Left = 215
    Top = 82
    Width = 103
    Height = 22
    ControlLabel.Width = 95
    ControlLabel.Height = 13
    ControlLabel.Caption = #1042#1080#1076' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
    ControlLabel.Font.Charset = DEFAULT_CHARSET
    ControlLabel.Font.Color = clWindowText
    ControlLabel.Font.Height = -11
    ControlLabel.Font.Name = 'Verdana'
    ControlLabel.Font.Style = []
    ControlLabel.ParentFont = False
    ControlLabel.Visible = True
    DataField = 'container_kind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 4
    Visible = True
    SqlSet = dm.ssContainerKind
    RowCount = 0
  end
  inherited dsLocal: TDataSource
    Left = 752
    Top = 116
  end
  inherited qrAux: TADOQuery
    Left = 712
    Top = 122
  end
  object ssDlvTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from deliverytypes d where exists '
      '('
      'select 1 from docroutes r, doctypes t '
      
        'where r.end_doctype_id = t.id and t.system_section = '#39'income_she' +
        'et'#39
      'and r.dlv_type_id = d.id'
      ')')
    DownSql.Strings = (
      'select * from deliverytypes d where exists '
      '('
      'select 1 from docroutes r, doctypes t '
      
        'where r.end_doctype_id = t.id and t.system_section = '#39'income_she' +
        'et'#39
      'and r.dlv_type_id = d.id'
      ')'
      '')
    InitSql.Strings = (
      'select * from deliverytypes where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 114
    Top = 495
  end
end
