﻿inherited FormEditStation: TFormEditStation
  Caption = #1057#1090#1072#1085#1094#1080#1103
  ClientHeight = 192
  ExplicitWidth = 543
  ExplicitHeight = 220
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 15
    Top = 14
    Width = 24
    Height = 16
    Caption = #1050#1086#1076
  end
  object Label3: TLabel [1]
    Left = 15
    Top = 69
    Width = 98
    Height = 16
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
  end
  inherited plBottom: TPanel
    Top = 151
    TabOrder = 2
    ExplicitTop = 151
  end
  object edCode: TDBEditEh [3]
    Left = 15
    Top = 32
    Width = 305
    Height = 24
    DataField = 'code'
    DataSource = FormStations.dsData
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object edName: TDBEditEh [4]
    Left = 15
    Top = 88
    Width = 505
    Height = 24
    DataField = 'name'
    DataSource = FormStations.dsData
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
end
