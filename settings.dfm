﻿inherited FormSettings: TFormSettings
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited plTop: TPanel
      inherited sbDelete: TPngSpeedButton
        Visible = False
      end
      inherited sbAdd: TPngSpeedButton
        Visible = False
      end
      inherited sbEdit: TPngSpeedButton
        Visible = False
      end
      inherited Bevel2: TBevel
        Visible = False
      end
    end
    inherited dgData: TDBGridEh
      ReadOnly = False
      OnDblClick = nil
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076' '#1085#1072#1089#1090#1088#1086#1081#1082#1080
          Width = 121
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1056#1072#1089#1096#1080#1092#1088#1086#1074#1082#1072
          Width = 174
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'value'
          Footers = <>
          Title.Caption = #1047#1085#1072#1095#1077#1085#1080#1077
          Width = 190
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited pmGrid: TPopupMenu
    inherited N1: TMenuItem
      Enabled = False
      Visible = False
    end
    inherited N2: TMenuItem
      Enabled = False
      Visible = False
    end
    inherited N3: TMenuItem
      Enabled = False
      Visible = False
    end
    inherited N6: TMenuItem
      Enabled = False
      Visible = False
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from settings order by code')
    UpdateCommand.CommandText.Strings = (
      'update settings'
      'set'
      '  code = :code,'
      '  name = :name,'
      '  value = :value'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'value'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into settings'
      '  (code, name, value)'
      'values'
      '  (:code, :name, :value)')
    InsertCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'value'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 150
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from settings where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from settings where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
end
