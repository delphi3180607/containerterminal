﻿inherited FormMtu2Picture: TFormMtu2Picture
  Caption = #1052#1058#1059' <-> '#1056#1080#1089#1091#1085#1086#1082
  ClientHeight = 185
  ClientWidth = 552
  ExplicitWidth = 558
  ExplicitHeight = 213
  PixelsPerInch = 96
  TextHeight = 16
  object Label3: TLabel [0]
    Left = 8
    Top = 14
    Width = 25
    Height = 14
    Caption = #1052#1058#1059
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel [1]
    Left = 8
    Top = 75
    Width = 52
    Height = 14
    Caption = #1056#1080#1089#1091#1085#1086#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 144
    Width = 552
    inherited btnCancel: TButton
      Left = 436
    end
    inherited btnOk: TButton
      Left = 317
    end
  end
  object luMtuKind: TDBSQLLookUp [3]
    Left = 8
    Top = 34
    Width = 529
    Height = 22
    DataField = 'mtukind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
    SqlSet = ssMtuKind
  end
  object luPictureKind: TDBSQLLookUp [4]
    Left = 8
    Top = 94
    Width = 529
    Height = 22
    DataField = 'picturekind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
    SqlSet = ssPictureKinds
  end
  inherited dsLocal: TDataSource
    Left = 224
    Top = 80
  end
  inherited qrAux: TADOQuery
    Left = 296
    Top = 80
  end
  object ssPictureKinds: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from picturekinds order by code')
    DownSql.Strings = (
      'select * from picturekinds order by code')
    InitSql.Strings = (
      'select * from picturekinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 134
    Top = 80
  end
  object ssMtuKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from mtukinds order by code')
    DownSql.Strings = (
      'select * from mtukinds order by code')
    InitSql.Strings = (
      'select * from mtukinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 129
    Top = 22
  end
end
