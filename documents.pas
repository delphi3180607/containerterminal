﻿unit documents;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, Vcl.ComCtrls, EXLReportExcelTLB, EXLReportBand, EXLReport,
  System.Actions, Vcl.ActnList;

type
  TFormCargoDocs = class(TFormGrid)
    Bevel1: TBevel;
    btFlow: TPngSpeedButton;
    sbHistory: TPngSpeedButton;
    DBGridEh1: TDBGridEh;
    Splitter1: TSplitter;
    Bevel3: TBevel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormCargoDocs: TFormCargoDocs;

implementation

{$R *.dfm}

end.
