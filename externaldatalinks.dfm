﻿inherited FormExternalDatalinks: TFormExternalDatalinks
  Caption = #1057#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1077' '#1074#1085#1077#1096#1085#1080#1084' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072#1084
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'data_type'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1072#1085#1085#1099#1093
          Width = 156
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'text_caption'
          Footers = <>
          Title.Caption = #1058#1077#1082#1089#1090#1086#1074#1086#1077' '#1076#1072#1085#1085#1086#1077' '#1080#1079' '#1074#1085#1077#1096#1085#1077#1081' '#1089#1080#1089#1090#1077#1084#1099
          Width = 284
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'data_caption'
          Footers = <>
          Title.Caption = #1047#1085#1072#1095#1077#1085#1080#1077' '#1074#1086' '#1074#1085#1091#1090#1088#1077#1085#1085#1077#1084' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1077
          Width = 272
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from externaldatalinks order by data_type, text_caption')
    UpdateCommand.CommandText.Strings = (
      'update externaldatalinks'
      'set'
      '  data_type = :data_type,'
      '  text_caption = :text_caption,'
      '  data_id = :data_id,'
      '  data_caption = :data_caption'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'data_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'text_caption'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'data_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'data_caption'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into externaldatalinks'
      '  (data_type, text_caption, data_id, data_caption)'
      'values'
      '  (:data_type, :text_caption, :data_id, :data_caption)')
    InsertCommand.Parameters = <
      item
        Name = 'data_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'text_caption'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'data_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'data_caption'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from externaldatalinks where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from externaldatalinks where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
end
