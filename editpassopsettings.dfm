﻿inherited FormEditPassOpSettings: TFormEditPassOpSettings
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1086#1087#1077#1088#1072#1094#1080#1080' '#1087#1086' '#1087#1088#1086#1087#1091#1089#1082#1091
  ClientHeight = 449
  ClientWidth = 378
  ExplicitWidth = 384
  ExplicitHeight = 477
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 408
    Width = 378
    TabOrder = 7
    ExplicitTop = 365
    ExplicitWidth = 377
    inherited btnCancel: TButton
      Left = 262
      ExplicitLeft = 261
    end
    inherited btnOk: TButton
      Left = 143
      ExplicitLeft = 142
    end
  end
  object leRealStartstate: TDBSQLLookUp [1]
    Left = 16
    Top = 151
    Width = 340
    Height = 24
    ControlLabel.Width = 158
    ControlLabel.Height = 16
    ControlLabel.Caption = #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'real_state_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
    SqlSet = ssRealStateKind
    RowCount = 0
  end
  object leStartState: TDBSQLLookUp [2]
    Left = 16
    Top = 90
    Width = 340
    Height = 24
    ControlLabel.Width = 153
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1072#1095#1072#1083#1100#1085#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077' '
    ControlLabel.Visible = True
    DataField = 'start_state_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
    SqlSet = ssStateKind
    RowCount = 0
  end
  object laDlvTypes: TDBSQLLookUp [3]
    Left = 16
    Top = 33
    Width = 340
    Height = 23
    AutoSize = False
    Color = 14810109
    ControlLabel.Width = 92
    ControlLabel.Height = 16
    ControlLabel.Caption = #1042#1080#1076' '#1076#1086#1089#1090#1072#1074#1082#1080
    ControlLabel.Visible = True
    DataField = 'dlv_type_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    StyleElements = [seFont, seBorder]
    TabOrder = 0
    Visible = True
    SqlSet = ssDlvTypes
    RowCount = 0
  end
  object leSpareKind: TDBSQLLookUp [4]
    Left = 16
    Top = 316
    Width = 340
    Height = 24
    ControlLabel.Width = 212
    ControlLabel.Height = 16
    ControlLabel.Caption = #1055#1086#1076#1095#1080#1085#1077#1085#1085#1072#1103' '#1087#1072#1088#1085#1072#1103' '#1086#1087#1077#1088#1072#1094#1080#1103
    ControlLabel.Visible = True
    DataField = 'spare_kind_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = True
    SqlSet = ssSpareKind
    RowCount = 0
  end
  object cbEmpty: TDBCheckBoxEh [5]
    Left = 16
    Top = 205
    Width = 321
    Height = 17
    Caption = #1055#1086#1088#1086#1078#1085#1080#1081' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    DataField = 'isempty'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 3
  end
  object edSealNumber: TDBEditEh [6]
    Left = 16
    Top = 256
    Width = 182
    Height = 24
    ControlLabel.Width = 294
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102' (@ = '#1055#1059#1057#1058#1054')'
    ControlLabel.Visible = True
    DataField = 'seal_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    TabOrder = 5
    Visible = True
  end
  object cbIsMain: TDBCheckBoxEh [7]
    Left = 16
    Top = 373
    Width = 321
    Height = 17
    Caption = #1043#1083#1072#1074#1085#1072#1103
    DataField = 'ismain'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 6
  end
  inherited dsLocal: TDataSource
    Left = 40
    Top = 336
  end
  inherited qrAux: TADOQuery
    Left = 208
    Top = 336
  end
  object ssStateKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from objectstatekinds order by code')
    DownSql.Strings = (
      'select * from objectstatekinds order by code')
    InitSql.Strings = (
      'select * from objectstatekinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 130
    Top = 87
  end
  object ssRealStateKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select * from objectstatekinds where isformalsign = 0 order by c' +
        'ode')
    DownSql.Strings = (
      
        'select * from objectstatekinds where isformalsign = 0 order by c' +
        'ode')
    InitSql.Strings = (
      'select * from objectstatekinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 146
    Top = 151
  end
  object ssDlvTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from deliverytypes d order by code')
    DownSql.Strings = (
      'select * from deliverytypes d order by code')
    InitSql.Strings = (
      'select * from deliverytypes where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 120
    Top = 23
  end
  object ssSpareKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from passoperationkinds order by code')
    DownSql.Strings = (
      'select * from passoperationkinds order by code')
    InitSql.Strings = (
      'select * from passoperationkinds where id = @id')
    KeyName = 'id'
    DisplayName = 'code'
    Connection = dm.connMain
    Left = 210
    Top = 311
  end
end
