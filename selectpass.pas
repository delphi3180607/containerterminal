﻿unit SelectPass;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, MemTableDataEh, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  MemTableEh, DataDriverEh, ADODataDriverEh;

type
  TFormSelectPass = class(TFormEdit)
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    dgSpec: TDBGridEh;
  private
    { Private declarations }
  public
    taskid: integer;
    procedure Init;
  end;

var
  FormSelectPass: TFormSelectPass;

implementation

{$R *.dfm}


procedure TFormSelectPass.Init;
begin
  meSpec.Close;
  drvSpec.SelectCommand.Parameters.ParamByName('task_id').Value := taskid;
  meSpec.Open;
end;


end.
