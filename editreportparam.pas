﻿unit editreportparam;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, Vcl.DBCtrls;

type
  TFormEditReportParam = class(TFormEdit)
    edName: TDBEditEh;
    edTitle: TDBEditEh;
    cbObligatory: TDBCheckBoxEh;
    neOrder: TDBNumberEditEh;
    cbType: TDBComboBoxEh;
    edKeyName: TDBEditEh;
    edDisplayName: TDBEditEh;
    meSQL: TDBMemo;
    Label1: TLabel;
    edDefaultExpression: TDBEditEh;
    edLen: TDBNumberEditEh;
    edDecimalLen: TDBNumberEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditReportParam: TFormEditReportParam;

implementation

{$R *.dfm}

end.
