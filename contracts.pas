﻿unit Contracts;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  System.Actions, Vcl.ActnList, functions, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Vcl.Mask, DBCtrlsEh;

type
  TFormContracts = class(TFormGrid)
    dgSpec: TDBGridEh;
    pmSpec: TPopupMenu;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    Panel1: TPanel;
    sbDeleteSpec: TPngSpeedButton;
    sbExcelSpec: TPngSpeedButton;
    sbAddSpec: TPngSpeedButton;
    sbEditSpec: TPngSpeedButton;
    Bevel1: TBevel;
    JvxSplitter1: TSplitter;
    procedure sbAddClick(Sender: TObject);
    procedure sbAddSpecClick(Sender: TObject);
    procedure sbDeleteSpecClick(Sender: TObject);
    procedure sbEditSpecClick(Sender: TObject);
    procedure sbExcelSpecClick(Sender: TObject);
    procedure sbDeleteClick(Sender: TObject);
    procedure sbEditClick(Sender: TObject);
    procedure btExcelClick(Sender: TObject);
    procedure meSpecBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormContracts: TFormContracts;

implementation

{$R *.dfm}

uses editcontract, editcontractspec;


procedure TFormContracts.btExcelClick(Sender: TObject);
begin
  dgData.SetFocus;
  inherited btExcelClick(Sender);
end;

procedure TFormContracts.Init;
begin
  inherited;
  tablename := 'contracts';
  self.formEdit := FormEditContract;
  meSpec.Open;
end;

procedure TFormContracts.meSpecBeforePost(DataSet: TDataSet);
begin
  meSpec.FieldByName('contract_id').Value := meData.FieldByName('id').AsInteger;
  inherited meDataBeforePost(DataSet);
end;

procedure TFormContracts.sbAddClick(Sender: TObject);
begin
  dgData.SetFocus;
  inherited sbAddClick(Sender);
end;

procedure TFormContracts.sbAddSpecClick(Sender: TObject);
begin
  dgSpec.SetFocus;
  EA(self, FormEditContractSpec, meSpec, 'contractspec', 'contract_id');
end;

procedure TFormContracts.sbDeleteClick(Sender: TObject);
begin
  dgData.SetFocus;
  inherited sbDeleteClick(Sender);
end;

procedure TFormContracts.sbDeleteSpecClick(Sender: TObject);
begin
  dgSpec.SetFocus;
  ED(meSpec);
end;

procedure TFormContracts.sbEditClick(Sender: TObject);
begin
  dgData.SetFocus;
  inherited sbEditClick(Sender);
end;

procedure TFormContracts.sbEditSpecClick(Sender: TObject);
begin
  dgSpec.SetFocus;
  EE(self, FormEditContractSpec, meSpec, 'contractspec)');
end;

procedure TFormContracts.sbExcelSpecClick(Sender: TObject);
begin
  dgSpec.SetFocus;
  inherited btExcelClick(Sender);
end;

end.
