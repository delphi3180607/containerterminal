﻿unit Import;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.OleServer, Excel2000, ComObj,
  Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormImport = class(TFormEdit)
    edFileName: TDBEditEh;
    Label1: TLabel;
    ExcelApplication1: TExcelApplication;
    od: TOpenDialog;
    procedure btnOkClick(Sender: TObject);
    procedure edFileNameEditButtons0Click(Sender: TObject;
      var Handled: Boolean);
  private
    { Private declarations }
  public
    FData: Variant;
    DimY : integer;
    DimX : integer;
  end;

var
  FormImport: TFormImport;

implementation

{$R *.dfm}

procedure TFormImport.btnOkClick(Sender: TObject);
const
  xlCellTypeLastCell = $0000000B;
var
  IIndex : OleVariant;
  WorkBk : Variant;
  WorkSheet : Variant;
  XLApp  : Variant;
  fileName : string;
begin
  fileName := edFileName.Text;
  Screen.Cursor:=crHOURGLASS;
  try
    IIndex := 1;
    XLApp  := CreateOleObject('Excel.Application');
    // Открываем файл Excel
    XLApp.WorkBooks.Open(FileName,EmptyParam,true,EmptyParam,EmptyParam, EmptyParam,EmptyParam,
    EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam,0);
    WorkBk := XLApp.WorkBooks.Item[IIndex];
    WorkSheet := WorkBk.WorkSheets[1];
    // Для того, чтобы знать размеры Листа (WorkSheet), то есть число строк
    // и колонок, мы активизмруем последнюю непустую ячейку
    WorkSheet.Cells.SpecialCells(xlCellTypeLastCell,EmptyParam).Activate;
    // Получаем значение последней строки
    DimX := XLApp.ActiveCell.Column;
    // Получаем значение последней колонки
    DimY := XLApp.ActiveCell.Row;
    // Связываем  Variant переменные Листа с  Delphi Variant Matrix
    FData := XLApp.Range['A1',XLApp.Cells.Item[DimY,DimX]].Value;
  finally
    // Закрываем Excel и отсоединяемся от сервера
    XLApp.Quit;
    XLApp := null;
    Screen.Cursor:=crDEFAULT;
  end
end;

procedure TFormImport.edFileNameEditButtons0Click(Sender: TObject;
  var Handled: Boolean);
begin
  if od.Execute then
  begin
    self.edFileName.Text := od.FileName;
  end;
end;

end.
