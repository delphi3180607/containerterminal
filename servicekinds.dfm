﻿inherited FormServiceKinds: TFormServiceKinds
  Caption = #1042#1080#1076#1099' '#1091#1089#1083#1091#1075
  ClientWidth = 880
  ExplicitWidth = 896
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 217
    Top = 0
    Width = 5
    Height = 410
    Color = clWhite
    ParentColor = False
    ExplicitTop = 43
    ExplicitHeight = 367
  end
  inherited plBottom: TPanel
    Width = 880
    ExplicitWidth = 880
    inherited btnOk: TButton
      Left = 645
      ExplicitLeft = 645
    end
    inherited btnCancel: TButton
      Left = 764
      ExplicitLeft = 764
    end
  end
  inherited plAll: TPanel
    Left = 222
    Width = 658
    ExplicitLeft = 222
    ExplicitWidth = 658
    inherited plTop: TPanel
      Width = 658
      ExplicitWidth = 658
      inherited btTool: TPngSpeedButton
        Left = 606
        ExplicitLeft = 414
      end
    end
    inherited dgData: TDBGridEh
      Width = 658
      TitleParams.MultiTitle = True
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'scode'
          Footers = <>
          Title.Caption = #1050#1086#1076
          Width = 150
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'sname'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 222
        end
        item
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isextra'
          Footers = <>
          Title.Caption = #1044#1086#1087'. '#1091#1089#1083#1091#1075#1072' ?'
          Width = 100
        end>
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 0
    Width = 217
    Height = 410
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'plLeft'
    TabOrder = 2
    object Bevel1: TBevel
      Left = 216
      Top = 25
      Width = 1
      Height = 385
      Align = alRight
      ExplicitLeft = 215
      ExplicitHeight = 342
    end
    object dgFolders: TDBGridEh
      Left = 0
      Top = 25
      Width = 216
      Height = 385
      Align = alClient
      AutoFitColWidths = True
      BorderStyle = bsNone
      ColumnDefValues.Title.TitleButton = True
      DataSource = dsFolders
      DynProps = <>
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      GridLineParams.DataVertLines = False
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      IndicatorOptions = []
      Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghDialogFind, dghColumnResize, dghColumnMove]
      ParentFont = False
      PopupMenu = pmFolders
      ReadOnly = True
      SelectionDrawParams.DrawFocusFrame = True
      SelectionDrawParams.DrawFocusFrameStored = True
      SortLocal = True
      TabOrder = 0
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -13
      TitleParams.Font.Name = 'Tahoma'
      TitleParams.Font.Style = [fsBold]
      TitleParams.ParentFont = False
      TitleParams.VertLines = False
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'folder_name'
          Footers = <>
          TextEditing = False
          Title.Caption = #1050#1072#1090#1072#1083#1086#1075
          Width = 214
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object plTool: TPanel
      Left = 0
      Top = 0
      Width = 217
      Height = 25
      Align = alTop
      TabOrder = 1
      object cbAll: TCheckBox
        AlignWithMargins = True
        Left = 4
        Top = 4
        Width = 209
        Height = 17
        Align = alClient
        Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1074#1089#1077
        TabOrder = 0
        OnClick = cbAllClick
      end
    end
  end
  inherited pmGrid: TPopupMenu
    object N9: TMenuItem [3]
      Caption = '-'
    end
    object N8: TMenuItem [4]
      Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100
      ShortCut = 16461
      OnClick = N8Click
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from Servicekinds order by scode')
    UpdateCommand.CommandText.Strings = (
      'Update servicekinds set '
      'scode = :scode, '
      'sname = :sname, '
      'code1C = :code1C,'
      'folder_id = :folder_id, '
      'isextra = :isextra'
      'where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'scode'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'sname'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'code1C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isextra'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      
        'insert into servicekinds (scode, sname, code1C, folder_id, isext' +
        'ra) values ( :scode, :sname, :code1C, :folder_id, :isextra)')
    InsertCommand.Parameters = <
      item
        Name = 'scode'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'sname'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'code1C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isextra'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from servicekinds where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from Servicekinds where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 488
    Top = 200
  end
  inherited meData: TMemTableEh
    DetailFields = 'folder_id'
    MasterFields = 'id'
    MasterSource = dsFolders
    Left = 536
    Top = 200
  end
  inherited dsData: TDataSource
    Left = 592
    Top = 200
  end
  inherited exReport: TEXLReport
    Left = 662
  end
  object drvFolders: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select * from folders where folder_section = :folder_section ord' +
        'er by folder_name')
    SelectCommand.Parameters = <
      item
        Name = 'folder_section'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update folders set '
      'folder_name = :folder_name,'
      'parent_id = :parent_id'
      'where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'folder_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'parent_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into folders (folder_name, folder_section, parent_id)'
      'values ( :folder_name, :folder_section, :parent_id )')
    InsertCommand.Parameters = <
      item
        Name = 'folder_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'folder_section'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'parent_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from folders where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.Parameters = <>
    Left = 32
    Top = 184
  end
  object meFolders: TMemTableEh
    Params = <>
    DataDriver = drvFolders
    TreeList.Active = True
    TreeList.KeyFieldName = 'id'
    TreeList.RefParentFieldName = 'parent_id'
    BeforePost = meFoldersBeforePost
    Left = 96
    Top = 184
  end
  object dsFolders: TDataSource
    DataSet = meFolders
    Left = 152
    Top = 184
  end
  object pmFolders: TPopupMenu
    Left = 88
    Top = 272
    object MenuItem1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 45
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 46
      OnClick = MenuItem2Click
    end
    object MenuItem3: TMenuItem
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
      ShortCut = 113
      OnClick = MenuItem3Click
    end
    object MenuItem4: TMenuItem
      Caption = '-'
    end
    object MenuItem10: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ShortCut = 116
      OnClick = MenuItem10Click
    end
  end
end
