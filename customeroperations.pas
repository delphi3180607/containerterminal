﻿unit CustomerOperations;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand,
  EXLReport;

type
  TFormCustomerOperations = class(TFormGrid)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormCustomerOperations: TFormCustomerOperations;

implementation

{$R *.dfm}

end.
