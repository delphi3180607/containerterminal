﻿unit editdocrestrict;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask, DBSQLLookUp;

type
  TFormEditDocRestriction = class(TFormEdit)
    cbIsActive: TDBCheckBoxEh;
    dtStart: TDBDateTimeEditEh;
    edBase: TDBEditEh;
    edNote: TDBEditEh;
    luType: TDBSQLLookUp;
    ssRestTypes: TADOLookUpSqlSet;
    dtEnd: TDBDateTimeEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDocRestriction: TFormEditDocRestriction;

implementation

{$R *.dfm}

end.
