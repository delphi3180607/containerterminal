﻿inherited FormEditTableLinks: TFormEditTableLinks
  Caption = #1057#1074#1103#1079#1100' '#1090#1072#1073#1083#1080#1094
  ClientHeight = 379
  ClientWidth = 388
  ExplicitWidth = 394
  ExplicitHeight = 407
  PixelsPerInch = 96
  TextHeight = 16
  object Label2: TLabel [0]
    Left = 16
    Top = 14
    Width = 63
    Height = 16
    Caption = #1048#1084#1103' '#1087#1086#1083#1103
  end
  object Label3: TLabel [1]
    Left = 16
    Top = 70
    Width = 126
    Height = 16
    Caption = #1058#1072#1073#1083#1080#1094#1072' '#1093#1088#1072#1085#1077#1085#1080#1103
  end
  object Label4: TLabel [2]
    Left = 16
    Top = 126
    Width = 126
    Height = 16
    Caption = #1058#1072#1073#1083#1080#1094#1072' '#1080#1089#1090#1086#1095#1085#1080#1082
  end
  object Label6: TLabel [3]
    Left = 16
    Top = 254
    Width = 125
    Height = 16
    Caption = #1055#1086#1088#1086#1078#1076#1072#1090#1100' '#1079#1072#1087#1080#1089#1100
  end
  object Label1: TLabel [4]
    Left = 16
    Top = 190
    Width = 155
    Height = 16
    Caption = #1056#1086#1076#1080#1090#1077#1083#1100#1089#1082#1072#1103' '#1090#1072#1073#1083#1080#1094#1072
  end
  inherited plBottom: TPanel
    Top = 338
    Width = 388
    ExplicitTop = 338
    ExplicitWidth = 388
    inherited btnCancel: TButton
      Left = 272
      ExplicitLeft = 272
    end
    inherited btnOk: TButton
      Left = 153
      ExplicitLeft = 153
    end
  end
  object edFieldName: TDBEditEh [6]
    Left = 16
    Top = 32
    Width = 305
    Height = 24
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edStoreTable: TDBEditEh [7]
    Left = 16
    Top = 88
    Width = 305
    Height = 24
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object edSourceTable: TDBEditEh [8]
    Left = 16
    Top = 144
    Width = 305
    Height = 24
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object edCreateRecordType: TDBComboBoxEh [9]
    Left = 16
    Top = 272
    Width = 305
    Height = 24
    DynProps = <>
    EditButtons = <>
    TabOrder = 4
    Visible = True
  end
  object edParentTable: TDBEditEh [10]
    Left = 16
    Top = 208
    Width = 305
    Height = 24
    DynProps = <>
    EditButtons = <>
    TabOrder = 5
    Visible = True
  end
end
