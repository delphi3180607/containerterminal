﻿unit Catalogs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls;

type
  TFormCatalogs = class(TFormGrid)
    procedure meDataBeforeOpen(DataSet: TDataSet);
  private
  public
    system_section: string;
    procedure Init; override;
  end;

var
  FormCatalogs: TFormCatalogs;

implementation

{$R *.dfm}

uses editcatalog;

procedure TFormCatalogs.Init;
begin
  self.formEdit := FormEditCatalog;
  FormEditCatalog.system_section := system_section;
  inherited;
  meData.Close;
  meData.Open;
end;



procedure TFormCatalogs.meDataBeforeOpen(DataSet: TDataSet);
begin
 drvData.SelectCommand.Parameters.ParamByName('folder_section0').Value := system_section;
 drvData.InsertCommand.Parameters.ParamByName('folder_section0').Value := system_section;
end;

end.
