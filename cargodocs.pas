﻿unit cargodocs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, Vcl.ComCtrls, docflow, System.Actions, Vcl.ActnList,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.ImageList, Vcl.ImgList,
  PngImageList, Vcl.Mask, DBCtrlsEh;

type
  TFormCargoDocs = class(TFormDocFlow)
    Splitter1: TSplitter;
    DBGridEh1: TDBGridEh;
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormCargoDocs: TFormCargoDocs;

implementation

{$R *.dfm}

uses editcargodoc;


procedure TFormCargoDocs.Init;
begin
  //--
end;


end.
