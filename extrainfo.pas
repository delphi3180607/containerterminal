﻿unit ExtraInfo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.ComCtrls, Data.DB,
  Data.Win.ADODB, Vcl.StdCtrls, Vcl.ExtCtrls, PageControlExtra,
  Vcl.ButtonGroup, Vcl.Buttons, PngSpeedButton, grid;

type
  TFormExtraInfo = class(TFormEdit)
    plLeft: TPanel;
    sbInv: TPngSpeedButton;
    sbNotes: TPngSpeedButton;
    sbRestr: TPngSpeedButton;
    plAll: TPanel;
    procedure sbRestrClick(Sender: TObject);
    procedure sbNotesClick(Sender: TObject);
    procedure sbInvClick(Sender: TObject);
  private
    { Private declarations }
  public
    docid: integer;
    taskid: integer;
    procedure InPlugForm(f: TFormGrid);
    procedure UnPlugForm(f: TFormGrid);
    procedure Init;
  end;

var
  FormExtraInfo: TFormExtraInfo;

implementation

{$R *.dfm}

uses DocRestrictions, docletters, linkedinvoices;

procedure TFormExtraInfo.sbInvClick(Sender: TObject);
begin
  InPlugForm(FormDocLinkedInvoices);
end;

procedure TFormExtraInfo.Init;
begin
  if sbRestr.Down then InPlugForm(FormDocRestrictions)
  else if sbNotes.Down then InPlugForm(FormDocLetters)
  else if sbInv.Down then InPlugForm(FormDocLinkedInvoices)
  else begin
    sbRestr.Down := true;
    InPlugForm(FormDocRestrictions);
  end;
end;

procedure TFormExtraInfo.sbNotesClick(Sender: TObject);
begin
  InPlugForm(FormDocLetters);
end;

procedure TFormExtraInfo.sbRestrClick(Sender: TObject);
begin
  InPlugForm(FormDocRestrictions);
end;


procedure TFormExtraInfo.InPlugForm(f: TFormGrid);
begin
  try
    if plAll.ControlCount>0 then UnPlugForm(TFormGrid(plAll.Controls[0]));
  except
    //--
  end;
  f.Parent := plAll;
  f.BorderStyle := bsNone;
  f.Align := alClient;
  f.parent_id := docid;
  f.Init;
  f.Show;
end;


procedure TFormExtraInfo.UnPlugForm(f: TFormGrid);
begin
  f.Hide;
  f.BorderStyle := bsSingle;
  f.Align := alNone;
  f.Parent := nil;
end;



end.
