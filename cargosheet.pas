﻿unit cargosheet;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, docflow, System.Actions, Vcl.ActnList, EXLReportExcelTLB,
  EXLReportBand, EXLReport, Vcl.ComCtrls, Functions, Vcl.Mask, DBCtrlsEh, Vcl.DBCtrls,
  System.ImageList, Vcl.ImgList, PngImageList;

type
  TFormCargoSheet = class(TFormDocFlow)
    meMass: TMemTableEh;
    N15: TMenuItem;
    N16: TMenuItem;
    sbRegisterDate: TPngSpeedButton;
    sbExtraInfo: TPngSpeedButton;
    N17: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    N25: TMenuItem;
    sbChange: TPngSpeedButton;
    N24: TMenuItem;
    N26: TMenuItem;
    sbEarlyRegister: TPngSpeedButton;
    N27: TMenuItem;
    sbFilterPlan: TPngSpeedButton;
    plNotes: TPanel;
    edNote: TDBEdit;
    Bevel8: TBevel;
    Bevel9: TBevel;
    Bevel11: TBevel;
    Bevel12: TBevel;
    edPaidSumm: TDBEdit;
    cbPaidFull: TDBCheckBox;
    Label2: TLabel;
    cbDlvAllowed: TDBCheckBox;
    edDlvallowedDate: TDBEdit;
    pgPlug: TPngSpeedButton;
    procedure btGenerateClick(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure meDataAfterPost(DataSet: TDataSet);
    procedure meDataAfterInsert(DataSet: TDataSet);
    procedure meDataAfterEdit(DataSet: TDataSet);
    procedure sbRegisterDateClick(Sender: TObject);
    procedure sbAddClick(Sender: TObject);
    procedure sbEditClick(Sender: TObject);
    procedure sbExtraInfoClick(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure N20Click(Sender: TObject);
    procedure dgDataColumnUpdateData(Sender: TObject; var Text: string;
      var Value: Variant; var UseText, Handled: Boolean);
    procedure N22Click(Sender: TObject);
    procedure sbImport2Click(Sender: TObject);
    procedure N25Click(Sender: TObject);
    procedure sbChangeClick(Sender: TObject);
    procedure N24Click(Sender: TObject);
    procedure N26Click(Sender: TObject);
    procedure sbEarlyRegisterClick(Sender: TObject);
    procedure N27Click(Sender: TObject);
    procedure sbFilterPlanClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure meDataAfterOpen(DataSet: TDataSet);
    procedure sbConfirmClick(Sender: TObject);
    procedure sbCancelConfirmClick(Sender: TObject);
    procedure dgDataColumns7CellDataLinkClick(Grid: TCustomDBGridEh;
      Column: TColumnEh);
    procedure dgDataColumns8CellDataLinkClick(Grid: TCustomDBGridEh;
      Column: TColumnEh);
    procedure dgDataColumns5CellDataLinkClick(Grid: TCustomDBGridEh;
      Column: TColumnEh);
    procedure dgDataColumns6CellDataLinkClick(Grid: TCustomDBGridEh;
      Column: TColumnEh);
    procedure dgDataColumns15CellDataLinkClick(Grid: TCustomDBGridEh;
      Column: TColumnEh);
    procedure dgDataColumns1CellDataLinkClick(Grid: TCustomDBGridEh;
      Column: TColumnEh);
  private
    flag: boolean;

    train_num: string;

    UnicVals: TStringList;

    function CheckBeforeDelete(id: integer; guid: string): boolean; override;
  protected
    procedure AssignSelField(colname: string); override;
  public
    procedure ReplicateData;
    procedure SetRequiredFields;
    procedure UnSetRequiredFields;
    procedure ImportSheet(data:Variant; DimX:integer; DimY:integer; trainnum: string; prefix: string; startnumber: integer; loadtype: integer; emptytype: integer);
    procedure Init; override;
  end;

var
  FormCargoSheet: TFormCargoSheet;

implementation

{$R *.dfm}

uses editcargosoncarriages, docgenerate, dmu,
  editcargomassfill, deliverytypes, main, setprefix, editdateregister, seek,
  ExtraInfo, EditCheck, NoteFilter, importcargosheet, unloadplan, selectfolder,
  selectdeliverytype, editdatealert, editreturndoc, SelectTrainNum, payinfo,
  dlvallow, setguard;

procedure TFormCargoSheet.btGenerateClick(Sender: TObject);
var doctype_id: integer;
begin
  FormDocGenerate.ShowModal;
  if FormDocGenerate.ModalResult = mrOk then
  begin
    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select t.id from settings s, doctypes t where s.code = ''cargodoctype'' and s.value = t.code');
    qrAux.Open;
    doctype_id := qrAux.Fields[0].AsInteger;
  end;
end;

procedure TFormCargoSheet.Init;
begin
  UnicVals := TStringList.Create;
  UnicVals.Sorted := True;
  UnicVals.Duplicates := TDuplicates.dupIgnore;

  self.formEdit := FormEditCargosOnCarriages;
  self.system_section := 'income_sheet';
  flag := false;
  inherited;
  dgData.DataGrouping.GroupLevels[0].CollapseNodes;
  save_folder_position := true;
end;


function TFormCargoSheet.CheckBeforeDelete(id: integer; guid: string): boolean;
var mess: string;
begin
  result := inherited CheckBeforeDelete(id, guid);
  if not result then exit;

  qrAux.Close;
  qrAux.SQL.Clear;

  if id<>0 then
  begin
    qrAux.SQL.Add('select 1 where exists ');
    qrAux.SQL.Add('(select 1 from docfeed f where f.id = 0'+meData.FieldByName('unload_doc_id').AsString+')');
    mess := 'По данному документу созданы документы подачи. Удаление невозможно.';
  end else
  begin
    qrAux.SQL.Add('select 1 where exists ');
    qrAux.SQL.Add('(select 1 from docfeed f, doccargosheet s, selectlist sl where sl.id = s.id');
    qrAux.SQL.Add('and sl.guid = '''+guid+'''');
    qrAux.SQL.Add('and f.id = s.unload_doc_id)');
    mess := 'В выборке существуют документы по которым созданы документы подачи. Удаление невозможно.';
  end;

  qrAux.Open;
  if qrAux.Fields[0].AsInteger = 1 then
  begin
    result := false;
    ShowMessage(mess);
    exit;
  end;

end;


procedure TFormCargoSheet.meDataAfterEdit(DataSet: TDataSet);
begin
  inherited;
  //if not flag then SetRequiredFields;
end;

procedure TFormCargoSheet.meDataAfterInsert(DataSet: TDataSet);
begin
  inherited;
  //if not flag then SetRequiredFields;
end;

procedure TFormCargoSheet.meDataAfterOpen(DataSet: TDataSet);
var i: integer; vnum: Variant; pnum: string;
begin

 inherited;
 self.meDataAfterScroll(meData);

 UnicVals.Clear;
 UnicVals.Duplicates := dupIgnore;

 for i := 0 to meData.RecordsView.Count-1 do
 begin
   vnum := meData.RecordsView.Rec[i].DataValues['carriage_num', TDataValueVersionEh.dvvCurValueEh];
   pnum := VarToStr(vnum);
   UnicVals.Add(pnum);
 end;

 dgData.FieldColumns['carriage_num'].Title.Caption := 'Вaгон/'+ UnicVals.Count.ToString();

end;

procedure TFormCargoSheet.meDataAfterPost(DataSet: TDataSet);
begin
  inherited;
  FormDeliveryTypes.meData.Filter := '';
  FormDeliveryTypes.meData.Filtered := false;
end;

procedure TFormCargoSheet.meDataBeforeOpen(DataSet: TDataSet);
begin
  drvData.SelectCommand.Parameters.ParamByName('trainnum').Value := self.train_num;
  inherited;
end;

procedure TFormCargoSheet.SetRequiredFields;
begin

  {self.meData.FieldByName('forwarder_id').Required := true;
  self.meData.FieldByName('consignee_id').Required := true;
  self.meData.FieldByName('payer_id').Required := true;
  self.meData.FieldByName('dlv_type_id').Required := true;
  self.meData.FieldByName('container_num').Required := true;
  self.meData.FieldByName('carriage_num').Required := true;
  self.meData.FieldByName('container_owner_id').Required := true;
  self.meData.FieldByName('carriage_owner_id').Required := true;
  self.meData.FieldByName('weight_doc').Required := true;
  self.meData.FieldByName('cargotype_id').Required := not self.meData.FieldByName('isempty').AsBoolean;}

end;


procedure TFormCargoSheet.UnSetRequiredFields;
begin

  {self.meData.FieldByName('forwarder_id').Required := false;
  self.meData.FieldByName('consignee_id').Required := false;
  self.meData.FieldByName('payer_id').Required := false;
  self.meData.FieldByName('dlv_type_id').Required := false;
  self.meData.FieldByName('container_num').Required := false;
  self.meData.FieldByName('carriage_num').Required := false;
  self.meData.FieldByName('container_owner_id').Required := false;
  self.meData.FieldByName('carriage_owner_id').Required := false;
  self.meData.FieldByName('weight_doc').Required := false;
  self.meData.FieldByName('cargotype_id').Required := false;}

end;


procedure TFormCargoSheet.N16Click(Sender: TObject);
var i: integer; b: TBookmark;
begin

  flag := true;
  UnSetRequiredFields;

  b := meData.Bookmark;

  meData.DisableControls;

  meMass.Close;
  meMass.LoadFromDataSet(meData,0,lmCopy,false);
  meMass.Open;
  meMass.EmptyTable;
  meMass.Append;
  FormEditCargoMassFill.ShowModal;

  if FormEditCargoMassFill.ModalResult = mrOk then
  begin

    //meMass.Post;

    if self.dgData.Selection.SelectionType = (gstAll) then
    begin

      self.meData.First;
      while not self.meData.Eof do
      begin
        ReplicateData;
        self.meData.Next;
      end;
      //meData.Close;
      //meData.Open;

    end else
    if self.dgData.Selection.SelectionType = (gstRecordBookmarks) then
    begin

      for i := 0 to self.dgData.Selection.Rows.Count-1 do
      begin
        self.meData.Bookmark := self.dgData.Selection.Rows[i];
        ReplicateData;
      end;
      //meData.Close;
      //meData.Open;

    end else
    begin

      ReplicateData;

      if drvData.GetrecCommand.Parameters.FindParam('current_id') <> nil then
      begin
         RefreshRecord(meData,self.meData.FieldByName('id').AsInteger);
      end else
      begin
         //meData.Close;
         //meData.Open;
      end;

    end;

    meMass.Close;

  end;

  flag := false;
  SetRequiredFields;

  meData.Bookmark := b;
  meData.EnableControls;

end;

procedure TFormCargoSheet.N20Click(Sender: TObject);
begin
  dgData.DataGrouping.GroupLevels[0].CollapseNodes;
end;

procedure TFormCargoSheet.N22Click(Sender: TObject);
begin
  FormNoteFilter.ShowModal;
  if FormNoteFilter.ModalResult = mrOk then
  begin
    meData.SetFilterText('cargo_description like ''%%'+trim(FormNoteFilter.leNote.Text)+'%''');
    meData.Filtered := true;
    plStatus.Color := $004080FF;
  end;

  if FormNoteFilter.ModalResult = mrYes then
  begin
    meData.Filtered := false;
    meData.SetFilterText('');
    plStatus.Color := clCream
  end;

end;

procedure TFormCargoSheet.N24Click(Sender: TObject);
var g: string; gu: TGuid; id: integer;
begin
  FormDateAlert.ShowModal;
  if FormDateAlert.ModalResult = mrOk then
  begin

    CreateGuid(gu);
    g := GuidToString(gu);
    FillSelectListAll(g);

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select 1 where exists (select 1 from doccargosheet d, selectlist s');
    qrAux.SQL.Add('where s.id = d.id and s.guid = '''+g+''' and not d.alert_date is null)');
    qrAux.Open;

    if qrAux.Fields[0].AsInteger = 1 then
    if not fQYN('Среди отмеченных записей уже есть записи с установленной датой уведомления. Хотите перезаписать?')
    then exit;

{    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select 1 where exists (select 1 from doccargosheet d, selectlist s');
    qrAux.SQL.Add('where s.id = d.id and s.guid = '''+g+''' and d.task_id is null)');
    qrAux.Open;

    if qrAux.Fields[0].AsInteger = 1 then
    begin
      ShowMessage('Среди отмеченных записей есть неразгруженные контейнеры. Операция отменена.');
      exit;
    end;
}

    dm.spSetAlertDate.Parameters.ParamByName('@guid').Value := g;
    dm.spSetAlertDate.Parameters.ParamByName('@alertdate').Value := FormDateAlert.dtAlertDate.Value;
    dm.spSetAlertDate.ExecProc;

    id := meData.FieldByName('id').AsInteger;
    meData.Close;
    meData.Open;
    meData.Locate('id', id, []);

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

  end;
end;

procedure TFormCargoSheet.N25Click(Sender: TObject);
var gu: TGuid; g: string; id: integer;
begin

  if fQYN('Скопировать грузополучателя в плательщика?') then
  begin

    id := meData.FieldByName('id').AsInteger;

    CreateGUID(gu);
    g := GuidToString(gu);
    self.FillSelectListAll(g);

    dm.spCopyCons2Payer.Parameters.ParamByName('@selectguid').Value := g;
    dm.spCopyCons2Payer.ExecProc;


    meData.Close;
    meData.Open;

    meData.Locate('id',id,[]);

  end;

end;

procedure TFormCargoSheet.N26Click(Sender: TObject);
begin
  inherited;

  CreateGuid(gu);
  g := GuidToString(gu);
  FillSelectListAll(g);

  FormEditReturnDoc.ShowModal;

  if FormEditReturnDoc.ModalResult = mrOk then
  begin

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('update doccargosheet set emptyreturn_sign = :emptyreturn_sign, emptyreturn_date = :emptyreturn_date where');
    qrAux.SQL.Add('id in (select id from selectlist where guid = :guid);');
    qrAux.Parameters.ParamByName('emptyreturn_sign').Value := FormEditReturnDoc.cbReturnDoc.Checked;
    qrAux.Parameters.ParamByName('emptyreturn_date').Value := FormEditReturnDoc.dtReturnDoc.Value;
    qrAux.Parameters.ParamByName('guid').Value := g;
    qrAux.ExecSQL;

    FullRefresh(meData, true);

  end;

  allow_edit := false;

  dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
  dm.spClearSelectList.ExecProc;

end;

procedure TFormCargoSheet.N27Click(Sender: TObject);
var gu: TGuid; g: string;
begin
  if fQYN('Сформировать уведомления ?') then
  begin

    CreateGUID(gu);
    g := GuidToString(gu);
    self.FillSelectListAll(g);

    dm.spMakeAlertMessages.Parameters.ParamByName('@selectguid').Value := g;
    dm.spMakeAlertMessages.ExecProc;

    ShowMessage('Готово!');

  end;
end;

procedure TFormCargoSheet.ReplicateData;
var i: integer;
begin
  if (meData.FieldByName('isconfirmed').Value = 1) then exit;
  meData.Edit;
  for i := 0 to meMass.FieldCount-1 do
  begin
    if (meMass.Fields[i].Value <> null) and (meMass.Fields[i].AsString <> '-1') and (meMass.Fields[i].AsString <> '') then
    begin
      meData.Fields[i].Value := meMass.Fields[i].Value;
    end;
  end;
  meData.Post;
end;

procedure TFormCargoSheet.sbAddClick(Sender: TObject);
begin
  SetRequiredFields;
  inherited;
  UnSetRequiredFields;
end;

procedure TFormCargoSheet.sbCancelConfirmClick(Sender: TObject);
begin
  allowcancel := true;
  inherited;
end;

procedure TFormCargoSheet.sbChangeClick(Sender: TObject);
begin

  FormSelectDeliveryType.laDlvTypes.KeyValue := meData.FieldByName('dlv_type_id').AsInteger;
  FormSelectDeliveryType.ShowModal;

  if FormSelectDeliveryType.ModalResult = mrOk then
  begin

    meLinkedObjects.First;

    while not meLinkedObjects.Eof do
    begin

      dm.spUpdateDeliveryType.Parameters.ParamByName('@taskid').Value := meData.FieldByName('task_id').AsInteger;
      dm.spUpdateDeliveryType.Parameters.ParamByName('@dlvtypeid').Value := FormSelectDeliveryType.laDlvTypes.KeyValue;
      dm.spUpdateDeliveryType.ExecProc;

      meLinkedObjects.Next;

    end;

  end;

  meLinkedObjects.Close;
  meLinkedObjects.Open;

  FormDeliveryTypes.meData.Filter := '';
  FormDeliveryTypes.meData.Filtered := false;

end;

procedure TFormCargoSheet.sbConfirmClick(Sender: TObject);
var restrictconfirm: string;
begin

   drvData.GetrecCommand.Parameters.ParamByName('current_id').Value := meData.FieldByName('id').AsInteger;
   meData.RefreshRecord;

   if meData.FieldByName('restr_sign').AsString <> '' then
   begin
    if meData.FieldByName('restr_closed').AsInteger <> 1 then
    begin
      ShowMessage('При наличии действующего запрета проведение документа запрещено.');
      exit;
    end;
   end;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select s.value from settings s where s.code = ''restrictconfirm''');
  qrAux.Open;
  restrictconfirm := qrAux.Fields[0].AsString;

  if restrictconfirm = 'ДА' then
  begin
    if not (meData.FieldByName('paidfull').AsBoolean or meData.FieldByName('dlvallowed').AsBoolean) then
    begin
      ShowMessage('При отсутствии отметок об оплате проведение документа запрещено.');
      exit;
    end;
  end;

  inherited;
end;

procedure TFormCargoSheet.sbEarlyRegisterClick(Sender: TObject);
var g: string; gu: TGUID;
begin
  FormEditDateRegister.dtDateReg.Value := now()+1;
  FormEditDateRegister.ShowModal;
  if FormEditDateRegister.ModalResult = mrOk then
  begin
    CreateGUID(gu);
    g := GuidToString(gu);
    self.FillSelectListAll(g);
    dm.spRegisterCarriagesFromSheet.Parameters.ParamByName('@g').Value := g;
    dm.spRegisterCarriagesFromSheet.Parameters.ParamByName('@dateincome').Value := FormEditDateRegister.dtDateReg.Value;
    dm.spRegisterCarriagesFromSheet.ExecProc;
    meData.Close;
    meData.Open;
  end;
end;

procedure TFormCargoSheet.sbEditClick(Sender: TObject);
begin
  SetRequiredFields;
  inherited;
  UnSetRequiredFields;
end;

procedure TFormCargoSheet.sbExtraInfoClick(Sender: TObject);
begin
  FormExtraInfo.docid := self.meData.FieldByName('id').AsInteger;
  FormExtraInfo.taskid := self.meLinkedObjects.FieldByName('task_id').AsInteger;
  FormExtraInfo.Init;
  FormExtraInfo.ShowModal;
  drvData.GetrecCommand.Parameters.ParamByName('current_id').Value := self.meData.FieldByName('id').AsInteger;
  meData.RefreshRecord;
end;

procedure TFormCargoSheet.sbFilterPlanClick(Sender: TObject);
var lp: TPairValues; pp: TPairParam;
begin

  if self.train_num <> '' then
  begin

    self.train_num := '';
    meData.Close;
    meData.Open;
    sbFilterPlan.Down := false;
    exit;

  end else
  begin

    //FormSelectTrainNum.folder_id := meFolders.FieldByName('id').AsInteger;
    FormSelectTrainNum.Init;
    FormSelectTrainNum.ShowModal;

    if FormSelectTrainNum.ModalResult = mrOk then
    begin
      self.train_num := FormSelectTrainNum.cbTrains.Text;
      meData.Close;
      meData.Open;
      sbFilterPlan.Down := true;
    end else
    begin
      self.train_num := '';
      sbFilterPlan.Down := false;
    end;

  end;

end;

procedure TFormCargoSheet.sbImport2Click(Sender: TObject);
begin
  FormImportCargoSheet.ShowModal;
  if FormImportCargoSheet.ModalResult = mrOk then
  begin
    Screen.Cursor:=crHourGlass;
    Application.ProcessMessages;
    try
      FormSetPrefix.edPrefix.Text := FormatDateTime('ddmm',now());
      FormSetPrefix.neStartNumber.Value := 1;
      FormSetPrefix.laDlvTypeLoad.SetFirstValue;
      FormSetPrefix.laDlvTypeEmpty.SetFirstValue;
      FormSetPrefix.ShowModal;
      if FormSetPrefix.ModalResult = mrOk then
      ImportSheet(FormImportCargoSheet.FData, FormImportCargoSheet.DimX, FormImportCargoSheet.DimY, FormSetPrefix.edKP.Text, FormSetPrefix.edPrefix.Text, FormSetPrefix.neStartNumber.Value, FormSetPrefix.laDlvTypeLoad.KeyValue, FormSetPrefix.laDlvTypeEmpty.KeyValue);
    finally
      meData.Close;
      meData.Open;
      Screen.Cursor:=crDEFAULT;
      dgData.DataGrouping.GroupLevels[0].CollapseNodes;
    end;
  end;
end;

procedure TFormCargoSheet.sbRegisterDateClick(Sender: TObject);
var g: string; gu: TGUID; id: integer;
var lp: TPairValues; pp: TPairParam; fGrid: TForm;
begin
  fGrid := nil;
  pp.paramname := '';
  lp := SFDV(FormUnloadPlan, 0, 'note', pp, fGrid);
  if lp.keyvalue>0 then
  begin

    FormSelectFolder.meFolders.Close;
    FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('folder_section').Value := 'income_unload';
    FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('startid').Value := 0;
    FormSelectFolder.meFolders.Open;
    FormSelectFolder.ShowModal;

    if FormSelectFolder.ModalResult = mrOk then
    begin

      CreateGUID(gu);
      g := GuidToString(gu);
      self.FillSelectListAll(g);

      dm.spGenerateUnloadDocs.Parameters.ParamByName('@selectguid').Value := g;
      dm.spGenerateUnloadDocs.Parameters.ParamByName('@loadplanid').Value := lp.keyvalue;
      dm.spGenerateUnloadDocs.Parameters.ParamByName('@folderid').Value := FormSelectFolder.meFolders.FieldByName('id').AsInteger;
      dm.spGenerateUnloadDocs.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
      dm.spGenerateUnloadDocs.ExecProc;

      id := meData.FieldByName('id').AsInteger;

      meData.Close;
      meData.Open;

      meData.Locate('id',id,[]);

    end;

  end;

end;

procedure TFormCargoSheet.dgDataColumns15CellDataLinkClick(
  Grid: TCustomDBGridEh; Column: TColumnEh);
begin
  EE(nil, FormSetGuard, meData, 'doccargosheet');
end;

procedure TFormCargoSheet.dgDataColumns1CellDataLinkClick(Grid: TCustomDBGridEh;
  Column: TColumnEh);
begin
  sbPassClick(nil);
end;

procedure TFormCargoSheet.dgDataColumns5CellDataLinkClick(Grid: TCustomDBGridEh;
  Column: TColumnEh);
begin
  sbExtraInfoClick(nil);
end;

procedure TFormCargoSheet.dgDataColumns6CellDataLinkClick(Grid: TCustomDBGridEh;
  Column: TColumnEh);
begin
  sbExtraInfoClick(nil);
end;

procedure TFormCargoSheet.dgDataColumns7CellDataLinkClick(Grid: TCustomDBGridEh;
  Column: TColumnEh);
begin

  if pos('income_pmnt',FormMain.unavalable_sections)>0 then
  begin
    ShowMessage('У вас нет прав на редактирование оплаты.');
    exit;
  end;

  self.allow_edit := true;
  special_current_mode := 'updpmnt';
  EE(nil, FormPayInfo, meData, 'doccargosheet');
  special_current_mode := '';
  self.allow_edit := false;
end;

procedure TFormCargoSheet.dgDataColumns8CellDataLinkClick(Grid: TCustomDBGridEh;
  Column: TColumnEh);
begin
  special_current_mode := 'updpermit';
  EE(nil, FormDlvAllow, meData, 'doccargosheet');
  special_current_mode := '';
end;

procedure TFormCargoSheet.dgDataColumnUpdateData(Sender: TObject;
  var Text: string; var Value: Variant; var UseText, Handled: Boolean);
var id: integer; col: string;
begin

  if meData.ReadOnly then exit;

  AvoidKey := true;

  col := dgData.SelectedField.FieldName;

  selfield := '';
  if col = 'forwarder' then selfield := 'forwarder_id';
  if col = 'consignee' then selfield := 'consignee_id';
  if col = 'payer' then selfield := 'payer_id';
  if col = 'container_owner' then selfield := 'container_owner_id';
  if col = 'dlvtype' then selfield := 'dlv_type_id';

  if selfield<>'' then
  begin

    selvalue := ColumnSelectList(dgData, Text);
    seltextvalue := Text;

    if selvalue=0 then
    begin
     Handled := true;
     dgData.CancelMode;
     Handled := true;
     AvoidKey := false;
     exit;
    end;

    meData.Edit;
    meData.FieldByName(selfield).Value := selvalue;
    Handled := true;
    meData.Post;

  end;

  AvoidKey := false;

end;

procedure TFormCargoSheet.dgDataDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

  if Column.FieldName = 'emptyreturn_sign' then
   if meData.FieldByName('emptyreturn_sign').AsBoolean then
    begin
      dgData.Canvas.Brush.Color := clSilver;
      dgData.Canvas.Font.Color := clWhite;
    end;

  if Column.FieldName = 'restr_sign' then
   if meData.FieldByName('restr_sign').AsString <> '' then
   begin
    if meData.FieldByName('restr_closed').AsInteger = 1 then
    begin
      dgData.Canvas.Brush.Color := clMoneyGreen;
      dgData.Canvas.Font.Color := clBlack;
    end else
    begin
      dgData.Canvas.Brush.Color := $0097A4FF;
      dgData.Canvas.Font.Color := clBlack;
    end;
   end;

  if Column.FieldName = 'letter_sign' then
   if meData.FieldByName('letter_sign').AsString = 'Есть' then
    dgData.Canvas.Brush.Color := clYellow;

  if Column.FieldName = 'services_summa' then
   if meData.FieldByName('services_summa').AsFloat >0 then
    dgData.Canvas.Brush.Color := clMoneyGreen;

  inherited;

end;

procedure TFormCargoSheet.AssignSelField(colname: string);
begin

    selfield := '';
    if colname = 'train_num' then selfield := 'train_num';
    if colname = 'datesent' then selfield := 'datesent';
    if colname = 'forwarder' then selfield := 'forwarder_id';
    if colname = 'consignee' then selfield := 'consignee_id';
    if colname = 'payer' then selfield := 'payer_id';
    if colname = 'dlvtype' then selfield := 'dlv_type_id';
    if colname = 'doc_date' then selfield := 'doc_date';

end;


procedure TFormCargoSheet.ImportSheet(data:Variant; DimX:integer; DimY:integer; trainnum: string; prefix: string; startnumber: integer; loadtype: integer; emptytype: integer);
var i: integer; error: integer;
var conttype, contnumber, contowner, weight, datesent, carowner, carnumber, seal, descr, guard: string;
var g: string; gg: TGuid;
begin

  CreateGUID(gg);
  g := GuidToString(gg);

  for i := 4 to DimY do
  begin

      error := 0;

      conttype := data[i,1];
      contnumber := data[i,2];
      contowner := data[i,3];
      weight := data[i,4];
      datesent := data[i,5];
      carowner := data[i,7];
      carnumber := data[i,8];
      seal := data[i,9];
      guard := data[i,17];
      descr := StringReplace(data[i,11], '   ',' ', [rfReplaceAll]);
      descr := StringReplace(descr, #9,' ', [rfReplaceAll]);
      descr := StringReplace(descr, '   ',' ', [rfReplaceAll]);
      descr := StringReplace(descr, '  ',' ', [rfReplaceAll]);
      descr := StringReplace(descr, '  ',' ', [rfReplaceAll]);
      descr := StringReplace(descr, '  ',' ', [rfReplaceAll]);

      qrAux.ParamCheck := true;

      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('insert into ExternalCargoSheet (guid, conttype, contnumber, contowner, weight, datesent, carowner, carnumber, seal, descr, guard)');
      qrAux.SQL.Add('values( :guid, :conttype, :contnumber, :contowner, :weight, :datesent, :carowner, :carnumber, :seal, :descr, :guard); commit;');

      qrAux.Parameters.ParamByName('guid').Value := g;
      qrAux.Parameters.ParamByName('conttype').Value := conttype;
      qrAux.Parameters.ParamByName('conttype').Value := conttype;
      qrAux.Parameters.ParamByName('contnumber').Value := contnumber;
      qrAux.Parameters.ParamByName('contowner').Value := contowner;
      qrAux.Parameters.ParamByName('weight').Value := weight;
      qrAux.Parameters.ParamByName('datesent').Value := datesent;
      qrAux.Parameters.ParamByName('carowner').Value := carowner;
      qrAux.Parameters.ParamByName('carnumber').Value := carnumber;
      qrAux.Parameters.ParamByName('seal').Value := seal;
      qrAux.Parameters.ParamByName('descr').Value := descr;
      qrAux.Parameters.ParamByName('guard').Value := guard;

      try
        qrAux.ExecSQL;
      except
        on E : Exception do
        begin
          //ShowMessage(qrAux.SQL.Text);
          error := 1;
          ShowMessage('Ошибка при импорте: '+E.Message);
          exit;
        end;
      end;
  end;

  if error = 0 then
  begin

      dm.spImportExternalData.Parameters.ParamByName('@guid').Value := g;
      dm.spImportExternalData.Parameters.ParamByName('@systemsection').Value := 'income_sheet';
      dm.spImportExternalData.Parameters.ParamByName('@trainnum').Value := trainnum;
      dm.spImportExternalData.Parameters.ParamByName('@folderid').Value := self.meFolders.FieldByName('id').AsInteger;
      dm.spImportExternalData.Parameters.ParamByName('@prefix').Value := prefix;
      dm.spImportExternalData.Parameters.ParamByName('@startnumber').Value := startnumber;
      dm.spImportExternalData.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
      dm.spImportExternalData.Parameters.ParamByName('@loadtype').Value := loadtype;
      dm.spImportExternalData.Parameters.ParamByName('@emptytype').Value := emptytype;

    try
        dm.spImportExternalData.ExecProc;
      except
        on E : Exception do
        begin
          ShowMessage('Сообщения при импорте(!): '+#10+E.Message);
        end;
      end;

  end;

end;


end.
