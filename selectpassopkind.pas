﻿unit SelectPassOpKind;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Mask, DBCtrlsEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton;

type
  TFormSelectPassOpKind = class(TFormGrid)
  private
    { Private declarations }
  public
    passid: integer;
    containerid: integer;
    procedure Init; override;
  end;

var
  FormSelectPassOpKind: TFormSelectPassOpKind;

implementation

{$R *.dfm}


procedure TFormSelectPassOpKind.Init;
begin
  meData.Close;
  drvData.SelectCommand.Parameters.ParamByName('passid').Value := passid;
  drvData.SelectCommand.Parameters.ParamByName('containerid').Value := containerid;
  inherited;
  meData.Open;
end;


end.
