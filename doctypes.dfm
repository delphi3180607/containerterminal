﻿inherited FormDocTypes: TFormDocTypes
  ActiveControl = dgData
  Caption = #1058#1080#1087#1099' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
  ClientHeight = 492
  ExplicitHeight = 530
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 244
    Width = 820
    Height = 7
    Cursor = crVSplit
    Align = alBottom
    Beveled = True
    ExplicitLeft = -56
    ExplicitTop = 203
    ExplicitWidth = 744
  end
  inherited plBottom: TPanel
    Top = 451
    ExplicitTop = 451
  end
  inherited plAll: TPanel
    Height = 244
    ExplicitHeight = 244
    inherited dgData: TDBGridEh
      Height = 215
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076
          Width = 162
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 146
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'system_section'
          Footers = <>
          Title.Caption = #1056#1072#1079#1076#1077#1083
          Width = 110
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'global_section'
          Footers = <>
          Title.Caption = #1043#1083#1086#1073#1072#1083#1100#1085#1099#1081' '#1088#1072#1079#1076#1077#1083
          Width = 97
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dlv_type_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
          Width = 109
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'avoid_object_update'
          Footers = <>
          Title.Caption = #1048#1079#1073#1077#1075#1072#1090#1100' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103' '#1086#1073#1098#1077#1082#1090#1072
          Width = 108
        end>
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 251
    Width = 820
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object sbDeleteSpec: TPngSpeedButton
      AlignWithMargins = True
      Left = 28
      Top = 0
      Width = 32
      Height = 35
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alLeft
      Flat = True
      OnClick = sbDeleteSpecClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
        610000001974455874536F6674776172650041646F626520496D616765526561
        647971C9653C000002554944415478DA95924D68D36018C79F7C746D92A6B5DD
        86B2B56C7810A141F0E8459C7810BC09A2CECFA907F13211C7BC7A134410069E
        046FC26EE2556D657EEC24F5D04C2F62B77569BA43ED47DA244BF2C6E74D6CB1
        743BEC8524BCE4FDFD9E27FF278CEFFBB09F65CFDD11A3AF5E767B7B6640B0F0
        680AEF7178FA44DD15BE793BE7BBCE19E2B84571F9F5CAA080C21C57A67BDFB6
        15F6F9B301897DE3560EDF9798E808ECD4B69751F4587EFBE64728F80743340A
        54E7B7DB402C4BE15F2C0512EBFA5C8E61D9129790814F26C1DEAC80A5EBF789
        E32C8582878B319F900E0802CBC83290560B48A3811253C17681C22CC2914412
        ACAD0AD855DD435848E5DF39FD4F20F30F62BEE7198C20702C1EF49A0D70FFD4
        C1470187D248320156658B56F6B0FD782AFFDE1A0AD1BD7B2F465CCF604591E3
        5307003C82667A795899C2B5102E7CB0769F4218164ADC8E3035CDC2C63A4A3C
        80CC24348ADF09711D29FD316FFD7F7E4860CE5EC3C098526C3203502E871DA0
        A0B9B6064EDB50C6BE7E52F7149897AFE600615E94303019613F689F4AAC5A0D
        4C5D879D765B3958FCA60E09BA97AE04954338016655A3104D9B89A5D2AC3471
        08BA551DBA9A86124399F8A9AA7D41F7E26C30675E14118E83A9E9606D53D88D
        6368804F43184D737236031DAD0A1D0C947692F9FD4B0D04C6F90B336C84CF47
        24095CA30326C238BEF8E897952030FDD8711CB16B48E3E31C3D63D089D4EBA7
        B31BE542FF139A67CFA12492775A4D8215250C6B206DEDC8D1E03F199165AE07
        0F85583F796A06E1D5B1D5CF03706F55A60F53C989ECE67A61CF31EE77FD05EB
        B7706A5FA737EA0000000049454E44AE426082}
      ExplicitLeft = 33
      ExplicitHeight = 29
    end
    object sbExcelSpec: TPngSpeedButton
      AlignWithMargins = True
      Left = 100
      Top = 0
      Width = 44
      Height = 35
      Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alLeft
      Flat = True
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
        610000001974455874536F6674776172650041646F626520496D616765526561
        647971C9653C0000031E4944415478DA8D936B6C0C5114C7FF7776B7BB53CF6E
        29F508A11E4D89E8AA846AB32869D89520A1691B1111AFB6D2342A21AA443C2A
        F1F8209E1104ED1791F824E852DD967835969616F5A6655955DD99E9ECEC5C67
        6737F5D5C9993B7327F7FCCEB9FF7B2EE39C63C1CEFAD5E2406B81C0E00060A3
        8701B191D3C07568BADEDEF5937B58F7AF72EF09978A98B10860D1BE46EFC512
        47AA20B0C11C3081A3CFB8F1CDE1EF09A1FA71173A3FF64A2F9A5BED0D275CBD
        7D0057D5BDEF359B33EC2DDF61121833325352842399350E39A4C139A11F6ADB
        653C68F5A3E3A32ABF6A6B4BB87B6C71AF01701FBC2F5D2E76886F02268AE546
        013A0D615D874A00450D237B820D2D7E0E8DA0B77C9DB8ED6DAFF1EC73E6B3BC
        43F3DFEADC3E7670BCCEC2B4DFF4143702C11FB8E3BB6080AC96FE04D2B0383D
        1F85D9EB696E86A4A8283CF244BBB93BCB12015C59B7B06CB96CEDC0FB402B9A
        9EBC27C1240C4D48841AD620493274D8B16CF67E30533C74AA2A6B9C152B0E3F
        566E54648A118035A44F09962F596A7AADD6A323E0475B7307C24C8699C5C16C
        4A42C1BC43548D19210A26C78C5166AC3C42801DB3C4E8295479E5C949E76C05
        D96BF1E8E73574FD09C3E77B0A5B9C1DC5EEF3246A1C42B44E0B933EF44E1B2E
        208F2AB8591103382B77A98A7ADE32313915A5AE4A5CFF7411BA64C5CBD6E7C8
        759460DAF845241E10D2A202A724C200782A09E0281FB18E529CCA4C73222005
        C0551D65EE5DB8D4761C66251E9FDEB5E375E74B9C2EF94027C3490F20350970
        EF7DA8D4ED996D003ED3BF91916689F898A43424278C465E663EEE7EF1E06BD7
        37A87E091B5C97E934B8D198D39205E4EC68501AAAB2A35B985BD128576F71D8
        FC3D16A3FB7E07BFE16AE31E04955F604CC0003111AB728EF67567CA5060EE56
        AFF2E0A8330AC8DA562FD794CFB405640BFEC7C627524C699DD27432270A9855
        5617ACD93E53ECEEB5B2FF014C1AC6F48C8DB5D2B3330B071880199B3C6FAF1F
        98332418E2FD682E44B4D0638B0789E6BE4B255A04DA1274314EF83375CD8D2F
        2DE772D30C4046516D9116E24B1963D349A6F87F57D1F0D8ED88CEC914EAC626
        01A8F69DCD3DFB17534092F0545E8BC50000000049454E44AE426082}
      ExplicitLeft = 113
      ExplicitTop = 2
      ExplicitHeight = 37
    end
    object sbAddSpec: TPngSpeedButton
      AlignWithMargins = True
      Left = 0
      Top = 0
      Width = 28
      Height = 35
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alLeft
      Flat = True
      Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      OnClick = sbAddSpecClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
        610000001974455874536F6674776172650041646F626520496D616765526561
        647971C9653C000002BE4944415478DAA5935D4893511CC61FA74E97390D6B9B
        1F516C2E334C2535295A8A792549908124A120D145A6303023233FA2444A08F5
        C6ECA2B430B01681994E4D29511321B134F3634A7EA382B9ED7DA7BEE7BC6FAF
        1B2E28BDA9FFB9389C8BE777FEFF739EC7451004FC4FB9FC09286BBBAEA13C4D
        2394A6134A5484A7E0285910CFB5E25E579EFACCB423A0AC2D378550BE72BF42
        E31FA28A80CC7D3736F80D2C5AE7D033DE8EC5E5A57942487675FA4BC35F0087
        98D64407C77969154761328F62C1360B8EE7E023F581421688AE512386C68719
        B1B38CE7971B0C4EC083D65CB5D87667A4FA648056198E0FF32D6019169C40E0
        E9E901415CEBFC3A42E561E81E69C3C8A4698E2344F73AAB75C20E2835EA0B7C
        7DF615C78724A377A913D60D060CC3202FB6C8DE5DC9E77CD85833246E6E8855
        E950DBF1088C852B6CC8E9B86307DC7D97331675382E785DB201D3F298383707
        8BD58C9284723B20CB7009DEBEEE203CC1216504D6AD0C1A7BDBC69BF59D5A3B
        A0B0E12A1B1F992C1BB10C61C5BA829BC7EF6DFB657A631AE4325F84F945A3AA
        E9B1ED7D6ECF2E3BE0D69B2BEC89F044D9B0791036BA8682A8FB3B02BCA57284
        2B6250F1B6CAF6F1469F039067C81CD31C0C0DE63D04CC58A6C0726B6016CDA8
        BE50EF146E96C44502ADDF11B06616AF7A1AC7BBF3FB1D23E8EBD30BD6246CF1
        D963A9E89A69C11AE520E55D507AE68913C0530152891BE2354978D85881558B
        B5B0EFF657C7235E7B91A6E628ED542AF606C4A84FA3EB47B3FDC1C4AF854037
        1102DC25AE885327C138D0884FDFBFCC899ED1F5177D9B701A29F3E979D148A4
        2650A5F4D28524627A7512B33F2721F03C82F6A811243F80A68106F40E0F32A2
        BD3344F16F236DD5C5EAA414D1EF95D495F8C7859E8252EEBF7939A657A660EC
        6F87D9CACE8BE2EC2DF1B6613A5799A01121699C334C440C13B58789F2A4AEBF
        6878E730FD4BFD027D1196F03509C5820000000049454E44AE426082}
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitHeight = 43
    end
    object sbEditSpec: TPngSpeedButton
      AlignWithMargins = True
      Left = 60
      Top = 0
      Width = 26
      Height = 35
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alLeft
      Flat = True
      Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      OnClick = sbEditSpecClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
        610000001974455874536F6674776172650041646F626520496D616765526561
        647971C9653C000002934944415478DAA5535B485451145D6746276A9A06C71A
        C9B4D044A9ACB0E78F59F4611F8954E6449488199158496896F515A4A8696816
        26589409F942A20744458345638489D3EB3AA999A0D58CD330533ACF7BEFE9DC
        AB46487D581B36FB6CCED96B3FCEDA84528AFF11B2F378736EE29675650E3755
        CB58A208CA541005662944CA7C416096CA566077F35481718E1B39D97EC17099
        14B70D081A8D5A31D3CC039C45ACCA4B529233B7FAFFA987D1E161D4156C26E4
        74631F2DDE1733638083A54F507F6A2B2145372DB4647F2CFC3CEB995D487D0B
        221B05F344F9CCE6C15359037623DCDD8D50CE5D89717B07887724931435F4D2
        928C3804D843488362288264275C76863C50BFF52EE8501766CFDF046D54029C
        839DE87D586F2585D7DFD1B2CCE57226B90249A9347DC81548203EEB0390F16E
        A83509700C58A00D5D0A95460FCED8E02527AEBDA5E5592B26B232259320E224
        90DFF614BCC3086DF436F8BE34C1F38DC06671E3BB9B71C0E78823F9F5665A91
        BD4A0698922972B96D1D10EC8FA08D498577E40A142A1E81B125707671A8E8DF
        8EEAB3D984E4D576D1AAC36B2148ACFA2DF82B77074A7727F4F13B58702D14C1
        3CFC3FA26037F5203CF53C322ADFA3B53C8D9063975ED2EADCF5139927D5CADD
        836BF819621353D8F06E8028FDF0BA2258B0190BD3EB30272402BB0B5AD15691
        4EC8918B2F68CDD18DBFCAFF606A02EF7A039EE811AA7B8E10BD8A0547C2F9CA
        82C8B41A046B17C9EF76E5B7A0BDD240A45D680E8B0C378812DF1901924931D2
        73AEC2D25480CF9F4C98B578355C560FEE7BF7C2CAEBE43D917E9C490BDB853D
        64FA369E3B141F283C90130485809EC7B73134D837A65DA04B4C2E319BFFB88D
        D3010C493ACF9AE8B0A00DCBA240837D268F6D342BA5F4F5C7BF51FA27F18163
        102C0858EF0000000049454E44AE426082}
      ExplicitLeft = 69
      ExplicitTop = -2
      ExplicitHeight = 31
    end
    object Bevel1: TBevel
      AlignWithMargins = True
      Left = 96
      Top = 0
      Width = 1
      Height = 35
      Margins.Left = 10
      Margins.Top = 0
      Margins.Bottom = 0
      Align = alLeft
      ExplicitLeft = 107
      ExplicitTop = 1
      ExplicitHeight = 41
    end
  end
  object dgSpec: TDBGridEh [4]
    Left = 0
    Top = 286
    Width = 820
    Height = 165
    Align = alBottom
    DataSource = dsSpec
    DynProps = <>
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
    ParentFont = False
    PopupMenu = pmSpec
    ReadOnly = True
    SelectionDrawParams.DrawFocusFrame = True
    SelectionDrawParams.DrawFocusFrameStored = True
    TabOrder = 3
    TitleParams.FillStyle = cfstGradientEh
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -11
    TitleParams.Font.Name = 'Verdana'
    TitleParams.Font.Style = [fsBold]
    TitleParams.ParentFont = False
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'title'
        Footers = <>
        Title.Caption = #1047#1072#1075#1086#1083#1086#1074#1086#1082' '#1092#1086#1088#1084#1099
        Width = 251
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'template'
        Footers = <>
        Title.Caption = #1064#1072#1073#1083#1086#1085
        Width = 389
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select t.* ,'
      
        '(select code from deliverytypes d where d.id = t.dlv_type_id) as' +
        ' dlv_type_code'
      'from doctypes t order by code')
    UpdateCommand.CommandText.Strings = (
      'update doctypes'
      'set'
      '  code = :code,'
      '  name = :name,'
      '  confirm_procedure = :confirm_procedure,'
      '  system_section = :system_section, '
      '  global_section = :global_section,'
      ' dlv_type_id = :dlv_type_id,'
      ' avoid_object_update = :avoid_object_update'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'confirm_procedure'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'system_section'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'global_section'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'avoid_object_update'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into doctypes'
      
        '  (code, name, confirm_procedure, system_section, global_section' +
        ', dlv_type_id, avoid_object_update)'
      'values'
      
        '  (:code, :name, :confirm_procedure, :system_section, :global_se' +
        'ction, :dlv_type_id, :avoid_object_update)')
    InsertCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'confirm_procedure'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'system_section'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'global_section'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'dlv_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'avoid_object_update'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from doctypes where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select t.* ,'
      
        '(select code from deliverytypes d where d.id = t.dlv_type_id) as' +
        ' dlv_type_code'
      'from doctypes t where t.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select * from doctemplates order by title'
      '')
    SelectCommand.Parameters = <>
    UpdateCommand.CommandText.Strings = (
      'update doctemplates'
      'set'
      '  doctype_id = :doctype_id,'
      '  title = :title,'
      '  template = :template,'
      '  sql_text = :sql_text,'
      '  group_field = :group_field'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'doctype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'title'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'template'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'sql_text'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'group_field'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into doctemplates'
      '  (doctype_id, title, template, sql_text, group_field)'
      'values'
      '  (:doctype_id, :title, :template, :sql_text, :group_field)')
    InsertCommand.Parameters = <
      item
        Name = 'doctype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'title'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'template'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'sql_text'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'group_field'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from doctemplates where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from doctemplates where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 88
    Top = 312
  end
  object meSpec: TMemTableEh
    DetailFields = 'doctype_id'
    FetchAllOnOpen = True
    MasterFields = 'id'
    MasterSource = dsData
    Params = <>
    DataDriver = drvSpec
    AfterPost = meDataAfterPost
    Left = 136
    Top = 312
  end
  object dsSpec: TDataSource
    DataSet = meSpec
    Left = 192
    Top = 312
  end
  object pmSpec: TPopupMenu
    Left = 368
    Top = 304
    object N8: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 45
      OnClick = sbAddSpecClick
    end
    object N9: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 46
      OnClick = sbDeleteSpecClick
    end
    object N10: TMenuItem
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
      ShortCut = 113
      OnClick = sbEditSpecClick
    end
  end
end
