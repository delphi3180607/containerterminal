﻿inherited FormEditRole: TFormEditRole
  Caption = #1056#1086#1083#1100
  ClientHeight = 461
  ClientWidth = 511
  ExplicitWidth = 517
  ExplicitHeight = 489
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 420
    Width = 511
    TabOrder = 8
    ExplicitTop = 381
    inherited btnCancel: TButton
      Left = 395
    end
    inherited btnOk: TButton
      Left = 276
    end
  end
  object edReadOnlySections: TDBEditEh [1]
    Left = 24
    Top = 87
    Width = 457
    Height = 24
    ControlLabel.Width = 180
    ControlLabel.Height = 16
    ControlLabel.Caption = #1056#1072#1079#1076#1077#1083#1099' '#1090#1086#1083#1100#1082#1086' '#1085#1072' '#1095#1090#1077#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'readonly_sections'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object edRestrictConfirmSections: TDBEditEh [2]
    Left = 24
    Top = 143
    Width = 457
    Height = 24
    ControlLabel.Width = 254
    ControlLabel.Height = 16
    ControlLabel.Caption = #1056#1072#1079#1076#1077#1083#1099' '#1089' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1077#1084' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1103
    ControlLabel.Visible = True
    DataField = 'confirmrestrict_sections'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 2
    Visible = True
  end
  object edUnavailableSections: TDBEditEh [3]
    Left = 24
    Top = 199
    Width = 457
    Height = 24
    ControlLabel.Width = 150
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1077#1076#1086#1089#1090#1091#1087#1085#1099#1077' '#1088#1072#1079#1076#1077#1083#1099
    ControlLabel.Visible = True
    DataField = 'unavalable_sections'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  object cbRestrictDicts: TDBCheckBoxEh [4]
    Left = 24
    Top = 346
    Width = 321
    Height = 17
    Caption = #1047#1072#1087#1088#1077#1090' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1103' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1086#1074
    DataField = 'restrict_edit_dicts'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 5
  end
  object cbRestrictObjects: TDBCheckBoxEh [5]
    Left = 24
    Top = 386
    Width = 417
    Height = 17
    Caption = #1047#1072#1087#1088#1077#1090' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1103' '#1076#1072#1085#1085#1099#1093' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074' '#1080' '#1074#1072#1075#1086#1085#1086#1074
    DataField = 'restrict_edit_objects'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 6
  end
  object cbAllowEditPrice: TDBCheckBoxEh [6]
    Left = 24
    Top = 304
    Width = 321
    Height = 17
    Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1094#1077#1085#1099
    DataField = 'allow_edit_price'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 7
  end
  object edName: TDBEditEh [7]
    Left = 24
    Top = 31
    Width = 457
    Height = 24
    ControlLabel.Width = 135
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1088#1086#1083#1080
    ControlLabel.Visible = True
    DataField = 'name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object edAllowDeleteSections: TDBEditEh [8]
    Left = 24
    Top = 253
    Width = 457
    Height = 24
    ControlLabel.Width = 143
    ControlLabel.Height = 16
    ControlLabel.Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086' '#1091#1076#1072#1083#1077#1085#1080#1077
    ControlLabel.Visible = True
    DataField = 'allowdelete_sections'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 4
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 240
    Top = 346
  end
  inherited qrAux: TADOQuery
    Left = 176
    Top = 346
  end
end
