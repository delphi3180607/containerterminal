﻿inherited FormSelectContainer: TFormSelectContainer
  Caption = #1042#1099#1073#1086#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
  ClientHeight = 214
  ClientWidth = 386
  ExplicitWidth = 392
  ExplicitHeight = 242
  PixelsPerInch = 96
  TextHeight = 16
  object lbMark: TLabel [0]
    Left = 10
    Top = 136
    Width = 7
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -19
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    StyleElements = [seClient, seBorder]
  end
  object lbExplain: TLabel [1]
    Left = 9
    Top = 8
    Width = 369
    Height = 36
    AutoSize = False
    Color = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    WordWrap = True
    StyleElements = [seClient, seBorder]
  end
  inherited plBottom: TPanel
    Top = 173
    Width = 386
    TabOrder = 1
    ExplicitTop = 134
    ExplicitWidth = 433
    inherited btnCancel: TButton
      Left = 270
      ExplicitLeft = 317
    end
    inherited btnOk: TButton
      Left = 151
      Enabled = False
      ExplicitLeft = 198
    end
  end
  object edContainerNum: TDBEditEh [3]
    Left = 8
    Top = 71
    Width = 366
    Height = 40
    ControlLabel.Width = 126
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = True
    OnChange = edContainerNumChange
  end
  inherited dsLocal: TDataSource
    Left = 56
    Top = 94
  end
  inherited qrAux: TADOQuery
    Parameters = <
      item
        Name = 'cnum'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select top 2 id, cnum, '
      
        '(select code from containerkinds ck where ck.id = c.kind_id) as ' +
        'kind_code '
      'from containers c where charindex(:cnum, c.cnum)>0'
      'order by 2')
    Left = 8
    Top = 94
  end
  object t1: TTimer
    Enabled = False
    Interval = 300
    OnTimer = t1Timer
    Left = 192
    Top = 118
  end
end
