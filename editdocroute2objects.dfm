﻿inherited FormEditDocroute2Object: TFormEditDocroute2Object
  Caption = #1054#1073#1100#1077#1082#1090#1099' '#1086#1087#1077#1088#1072#1094#1080#1080
  ClientHeight = 383
  ClientWidth = 874
  ExplicitWidth = 880
  ExplicitHeight = 411
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 445
    Top = 76
    Width = 134
    Height = 16
    Caption = #1062#1077#1083#1077#1074#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
  end
  object Label2: TLabel [1]
    Left = 8
    Top = 10
    Width = 84
    Height = 16
    Caption = #1058#1080#1087' '#1086#1073#1100#1077#1082#1090#1072
  end
  object Label3: TLabel [2]
    Left = 8
    Top = 199
    Width = 90
    Height = 16
    Caption = #1058#1080#1087' '#1089#1074#1103#1079#1080' '#1076#1086
  end
  object sbClear: TSpeedButton [3]
    Left = 396
    Top = 222
    Width = 23
    Height = 24
    Caption = 'X'
    OnClick = sbClearClick
  end
  object Label4: TLabel [4]
    Left = 445
    Top = 199
    Width = 114
    Height = 16
    Caption = #1058#1080#1087' '#1089#1074#1103#1079#1080' '#1087#1086#1089#1083#1077
  end
  object SpeedButton1: TSpeedButton [5]
    Left = 832
    Top = 222
    Width = 23
    Height = 24
    Caption = 'X'
    OnClick = SpeedButton1Click
  end
  object Label6: TLabel [6]
    Left = 8
    Top = 76
    Width = 140
    Height = 16
    Caption = #1048#1089#1093#1086#1076#1085#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
  end
  object Label5: TLabel [7]
    Left = 8
    Top = 137
    Width = 419
    Height = 16
    Caption = #1048#1089#1093#1086#1076#1085#1086#1077' '#1092#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077' ('#1086#1075#1088#1072#1085#1080#1095#1080#1090#1077#1083#1100' '#1088#1077#1072#1083#1100#1085#1086#1089#1090#1080')'
  end
  inherited plBottom: TPanel
    Top = 339
    Width = 874
    Height = 44
    TabOrder = 8
    ExplicitTop = 339
    ExplicitWidth = 874
    ExplicitHeight = 44
    inherited btnCancel: TButton
      Left = 758
      Height = 38
      ExplicitLeft = 758
      ExplicitHeight = 38
    end
    inherited btnOk: TButton
      Left = 639
      Height = 38
      ExplicitLeft = 639
      ExplicitHeight = 38
    end
  end
  object leEndState: TDBSQLLookUp [9]
    Left = 445
    Top = 98
    Width = 385
    Height = 24
    DataField = 'end_object_state_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    Visible = True
    SqlSet = ssStateKind
    RowCount = 0
  end
  object leStartState: TDBSQLLookUp [10]
    Left = 8
    Top = 98
    Width = 385
    Height = 24
    DataField = 'start_object_state_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
    SqlSet = ssStateKind
    RowCount = 0
  end
  object cbRelationEnd: TDBComboBoxEh [11]
    Left = 445
    Top = 222
    Width = 385
    Height = 24
    DataField = 'end_link_type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Items.Strings = (
      #1056#1072#1079#1100#1077#1076#1080#1085#1077#1085#1080#1077
      #1057#1086#1077#1076#1080#1085#1077#1085#1080#1077)
    KeyItems.Strings = (
      '0'
      '1')
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
  object cbRelationStart: TDBComboBoxEh [12]
    Left = 8
    Top = 222
    Width = 385
    Height = 24
    DataField = 'start_link_type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Items.Strings = (
      #1056#1072#1079#1100#1077#1076#1080#1085#1077#1085#1080#1077
      #1057#1086#1077#1076#1080#1085#1077#1085#1080#1077)
    KeyItems.Strings = (
      '0'
      '1')
    ParentFont = False
    TabOrder = 4
    Visible = True
  end
  object cbObjectType: TDBSQLLookUp [13]
    Left = 8
    Top = 32
    Width = 385
    Height = 24
    DataField = 'objecttype_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
    SqlSet = ssObjectTypes
    RowCount = 0
  end
  object leRealStartstate: TDBSQLLookUp [14]
    Left = 8
    Top = 159
    Width = 385
    Height = 24
    DataField = 'start_real_state_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
    SqlSet = ssRealStateKind
    RowCount = 0
  end
  object cbEmpty: TDBCheckBoxEh [15]
    Left = 16
    Top = 293
    Width = 217
    Height = 17
    Caption = #1055#1086#1088#1086#1078#1085#1080#1081' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    DataField = 'default_isempty'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 6
  end
  object edSealNumber: TDBEditEh [16]
    Left = 445
    Top = 288
    Width = 182
    Height = 24
    ControlLabel.Width = 294
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102' (@ = '#1055#1059#1057#1058#1054')'
    ControlLabel.Visible = True
    DataField = 'default_seal_number'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    HighlightRequired = True
    TabOrder = 7
    Visible = True
  end
  inherited qrAux: TADOQuery
    Top = 189
  end
  object ssStateKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from objectstatekinds order by code')
    DownSql.Strings = (
      'select * from objectstatekinds order by code')
    InitSql.Strings = (
      'select * from objectstatekinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 705
    Top = 79
  end
  object ssObjectTypes: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from objecttypes order by code')
    DownSql.Strings = (
      'select * from objecttypes order by code')
    InitSql.Strings = (
      'select * from objecttypes where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 226
    Top = 31
  end
  object ssRealStateKind: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select * from objectstatekinds where isformalsign = 0 order by c' +
        'ode')
    DownSql.Strings = (
      
        'select * from objectstatekinds where isformalsign = 0 order by c' +
        'ode')
    InitSql.Strings = (
      'select * from objectstatekinds where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 258
    Top = 151
  end
end
