﻿inherited FormFilterChessExtra: TFormFilterChessExtra
  Caption = #1060#1080#1083#1100#1090#1088
  ClientHeight = 523
  ClientWidth = 786
  ExplicitWidth = 792
  ExplicitHeight = 551
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [2]
    Left = 317
    Top = 10
    Width = 111
    Height = 13
    Caption = #1060#1091#1090#1086#1074#1086#1089#1090#1100' '#1074#1072#1075#1086#1085#1086#1074
  end
  object sbAdd: TSpeedButton [3]
    Left = 752
    Top = 219
    Width = 25
    Height = 23
    Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1089#1087#1080#1089#1086#1082
    Caption = '+'
    ParentShowHint = False
    ShowHint = True
    OnClick = sbAddClick
  end
  object Label2: TLabel [4]
    Left = 320
    Top = 204
    Width = 50
    Height = 13
    Caption = #1057#1090#1072#1085#1094#1080#1080
  end
  object sbMinus: TSpeedButton [5]
    Left = 752
    Top = 264
    Width = 25
    Height = 25
    Hint = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1089#1087#1080#1089#1082#1072
    Caption = '-'
    ParentShowHint = False
    ShowHint = True
    OnClick = sbMinusClick
  end
  inherited plBottom: TPanel
    Top = 485
    Width = 786
    TabOrder = 3
    ExplicitTop = 485
    ExplicitWidth = 786
    inherited btnCancel: TButton
      Left = 670
      ExplicitLeft = 670
    end
    inherited btnOk: TButton
      Left = 551
      ExplicitLeft = 551
    end
    inherited btClear: TButton
      Left = 432
      ExplicitLeft = 432
    end
  end
  inherited meNumbers: TMemo
    Width = 280
    ExplicitWidth = 280
  end
  object cbCarKinds: TCheckListBox [8]
    Left = 317
    Top = 29
    Width = 428
    Height = 152
    OnClickCheck = cbCarKindsClickCheck
    ItemHeight = 13
    TabOrder = 1
    OnClick = cbCarKindsClickCheck
  end
  object luStation: TDBSQLLookUp [9]
    Left = 320
    Top = 221
    Width = 425
    Height = 21
    DynProps = <>
    EditButtons = <
      item
      end>
    TabOrder = 2
    Visible = True
    SqlSet = ssStations
    KeyValue = Null
    OnKeyValueChange = luStationKeyValueChange
  end
  object lbStations: TListBox [10]
    Left = 320
    Top = 264
    Width = 425
    Height = 208
    ItemHeight = 13
    TabOrder = 4
  end
  inherited qrAux: TADOQuery
    SQL.Strings = (
      'select id, code from carriagekinds order by 2')
  end
  object ssStations: TADOLookUpSqlSet
    TmplSql.Strings = (
      
        'select * from stations where name like '#39'%@pattern%'#39' or code like' +
        ' '#39'%pattern%'#39' order by name')
    DownSql.Strings = (
      'select top 100 * from stations order by code')
    InitSql.Strings = (
      'select * from stations where id = @id')
    KeyName = 'id'
    DisplayName = 'name'
    Connection = dm.connMain
    Left = 472
    Top = 223
  end
end
