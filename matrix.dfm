﻿inherited FormMatrix: TFormMatrix
  Caption = #1052#1072#1090#1088#1080#1094#1072
  ClientHeight = 778
  ClientWidth = 1268
  ExplicitWidth = 1284
  ExplicitHeight = 816
  PixelsPerInch = 96
  TextHeight = 13
  object SplitterVert: TSplitter [0]
    Left = 201
    Top = 0
    Width = 5
    Height = 737
    Color = 15658734
    ParentColor = False
    StyleElements = [seFont, seBorder]
    ExplicitLeft = 233
  end
  inherited plBottom: TPanel
    Top = 737
    Width = 1268
    ExplicitTop = 737
    ExplicitWidth = 1268
    inherited btnOk: TButton
      Left = 1033
      ExplicitLeft = 1033
    end
    inherited btnCancel: TButton
      Left = 1152
      ExplicitLeft = 1152
    end
  end
  inherited plAll: TPanel
    Width = 201
    Height = 737
    Align = alLeft
    Font.Name = 'Calibri'
    ParentFont = False
    ExplicitWidth = 201
    ExplicitHeight = 737
    inherited plTop: TPanel
      Width = 201
      ExplicitWidth = 201
      inherited btFilter: TPngSpeedButton
        Visible = False
        ExplicitLeft = 224
      end
      inherited btExcel: TPngSpeedButton
        ExplicitLeft = 190
      end
      inherited sbAdd: TPngSpeedButton
        Visible = False
      end
      inherited sbEdit: TPngSpeedButton
        Visible = False
      end
      inherited Bevel2: TBevel
        Visible = False
        ExplicitLeft = 186
      end
      inherited btTool: TPngSpeedButton
        Left = 163
        ExplicitLeft = 195
      end
      object sbWrap: TSpeedButton [7]
        AlignWithMargins = True
        Left = 177
        Top = 0
        Width = 101
        Height = 28
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 1
        Align = alClient
        Caption = #1054#1073#1085#1086#1074#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 16278023
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsUnderline]
        ParentFont = False
        StyleElements = [seClient, seBorder]
        OnClick = sbRefreshClick
        ExplicitLeft = 25
        ExplicitTop = 1
      end
      inherited plCount: TPanel
        Left = 157
        Width = 3
        ExplicitLeft = 157
        ExplicitWidth = 3
        inherited edCount: TDBEditEh
          Left = -69
          ControlLabel.Width = 97
          ControlLabel.ExplicitLeft = -169
          ControlLabel.ExplicitTop = 8
          ControlLabel.ExplicitWidth = 97
          ExplicitLeft = -69
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 201
      Height = 708
      AutoFitColWidths = True
      Font.Height = -12
      Font.Name = 'Calibri'
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
      TitleParams.Font.Height = -12
      TitleParams.Font.Name = 'Calibri'
      OnDblClick = N8Click
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'defect_column'
          Footers = <>
          Title.Caption = ' '
          Width = 8
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'container_code'
          Footers = <>
          Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
          Width = 99
        end
        item
          AlwaysShowEditButton = True
          ButtonStyle = cbsEllipsis
          CellButtons = <>
          CellDataIsLink = True
          DynProps = <>
          EditButton.Style = ebsEllipsisEh
          EditButton.Visible = True
          EditButton.OnClick = dgDataColumns2EditButtonClick
          EditButtons = <>
          FieldName = 'operation'
          Footers = <>
          Title.Caption = #1054#1087#1077#1088#1072#1094#1080#1103
          Width = 70
          OnCellDataLinkClick = dgDataColumns2CellDataLinkClick
          OnEditButtonClick = dgDataColumns2EditButtonClick
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 206
    Top = 0
    Width = 1062
    Height = 737
    Margins.Right = 0
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 2
    StyleElements = [seFont, seBorder]
    object Bevel4: TBevel
      Left = 1061
      Top = 29
      Width = 1
      Height = 673
      Align = alRight
      ExplicitLeft = 215
      ExplicitTop = 25
      ExplicitHeight = 600
    end
    object plTool: TPanel
      Left = 0
      Top = 0
      Width = 1062
      Height = 29
      Align = alTop
      BevelOuter = bvNone
      Color = 15921906
      ParentBackground = False
      TabOrder = 0
      StyleElements = [seFont, seBorder]
      object btnDeleteContainer: TPngSpeedButton
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 32
        Height = 29
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        OnClick = btnDeleteContainerClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000E9C00000E
          9C01079453DD0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000001484944415478DAAD53414B024114FE
          66095996FE417F41C49325EEBAE622B241D84442873A458724FA33051DEA545D
          0AF6B0A162BA048BE62EE8EF08BA7888080FD5343B90AC68896B8F8179CCBCEF
          9BF7BDF78630C6B088917F23208404DBDA11B0720E58D382F7017A0DBC70B73B
          C285085619A51E0603545C778B93DC87C165C0BC2B956A22D6B6358E7B1A23D8
          2384DE140A1692C977743ACA89E7D153C00EEE0E80CD4B4DB3914ABDA1DF5F3E
          76DDF21963D684049EE2C6553A5D452E37E424F24EBB6DBE029F4D5D6F72B038
          AB789EC86E9A048C5E330C1BAAFA854643822C03F9BCF0C359FD4A10D83A603C
          EA7A0BF1F8109204F8BEBCDBEB99B7C0C34FCC9F04065F4E54823109F5BA0445
          01B2D90F38CED24C09A288994C9503E62FA26863B168219188DCC6D983649A35
          C462C120A91CD79D7B940F81ED0BE099BBFE8484853F5354FB06D031E7E18A64
          5A800000000049454E44AE426082}
        ExplicitLeft = 34
      end
      object Bevel1: TBevel
        AlignWithMargins = True
        Left = 69
        Top = 0
        Width = 1
        Height = 29
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 113
        ExplicitHeight = 25
      end
      object PngSpeedButton4: TPngSpeedButton
        AlignWithMargins = True
        Left = 73
        Top = 0
        Width = 34
        Height = 29
        Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        OnClick = btExcelClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000010000000100803000000282D0F
          530000000373424954080808DBE14FE000000072504C544590FFD08CFDCB7FE5
          B772CFA524DAE56DCCA191BDA923D4E059C29468B7AC52BE8E4DBB8A1AB7C119
          B3BD46AC7E236A4A2169481D60431A5B3F1A583D165239005156005258124A31
          124B310F462C00403F003D3F003C3A053320042E19002B2C012C1700240F0023
          1000210F0B0B0BFFFFFFCD5AC7F30000002674524E53FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A77A81BC
          00000009704859730000004E0000004E01B1CD1F330000001C74455874536F66
          74776172650041646F62652046697265776F726B732043533571B5E336000000
          8F4944415478DA8D8F4D0B82501045E7BE34D0BEC8C8089116CDFFFF472D4224
          925244AA85F95E77F90A17CD62863970B83388E4BBA02DFC7D092D066F9FE4D0
          ABF3851DF4C519F46CE1604562E8C989C90A82BCB482E308B8D15CDF09360D95
          2DF4E224482A82B47E0BF623CAD94A98960459D58B39403B11133F08664F5E34
          FF27B661ECA22558758C4D30FD79FF03D1143A18699A33D30000000049454E44
          AE426082}
        ExplicitLeft = 117
        ExplicitHeight = 25
      end
      object btRemoveQueue: TPngSpeedButton
        AlignWithMargins = True
        Left = 32
        Top = 0
        Width = 34
        Height = 29
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Visible = False
        OnClick = btRemoveQueueClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000140000001208060000005BD0FE
          100000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000000C14944415478DABDD3D11182300C06
          60F20E874EE2B98C6E8033E9063A8D4EA21CBCD7BF5CCBA52D918407F3422F3D
          BE26A19073AECA8388CAA43228073D761F861ECBD68A9D9B26051FE3B843F20D
          70537505A86D553AB0007F85A67A35A81D850AB4CC7515D4B619F727104FD39D
          E3B807786E06DD5587D1257D19EB3DEF4006BB50F48D16C1889DEAFA137E84ED
          6068F108EC39E544B05B1925F008E243CE2731F005F0600639C6C01E58EBF7CC
          2D2F81BCEA3F8142486032D3CA78B173308F2F3B34BBF7BF80A5A30000000049
          454E44AE426082}
        ExplicitLeft = 72
      end
      object edContainerNum: TDBEditEh
        AlignWithMargins = True
        Left = 197
        Top = 3
        Width = 226
        Height = 23
        Margins.Left = 90
        Align = alLeft
        ControlLabel.Width = 76
        ControlLabel.Height = 13
        ControlLabel.Caption = #8470' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        DynProps = <>
        EditButtons = <>
        TabOrder = 0
        Visible = True
        OnKeyPress = edContainerNumKeyPress
        ExplicitHeight = 21
      end
      object plLegend: TPanel
        Left = 426
        Top = 0
        Width = 636
        Height = 29
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Shape1: TShape
          AlignWithMargins = True
          Left = 20
          Top = 7
          Width = 9
          Height = 15
          Margins.Left = 20
          Margins.Top = 7
          Margins.Bottom = 7
          Align = alLeft
          Brush.Color = clRed
          Pen.Style = psClear
          Shape = stCircle
          ExplicitTop = 3
          ExplicitHeight = 23
        end
        object Label1: TLabel
          AlignWithMargins = True
          Left = 139
          Top = 8
          Width = 47
          Height = 18
          Margins.Top = 8
          Align = alLeft
          Caption = #1050' '#1074#1099#1074#1086#1079#1091
          ExplicitHeight = 13
        end
        object Label2: TLabel
          AlignWithMargins = True
          Left = 35
          Top = 8
          Width = 66
          Height = 18
          Margins.Top = 8
          Align = alLeft
          Caption = #1054#1090#1089#1091#1090#1089#1090#1074#1091#1077#1090
          ExplicitHeight = 13
        end
        object Shape2: TShape
          AlignWithMargins = True
          Left = 409
          Top = 7
          Width = 9
          Height = 15
          Margins.Left = 20
          Margins.Top = 7
          Margins.Bottom = 7
          Align = alLeft
          Brush.Color = clLime
          Pen.Style = psClear
          Shape = stCircle
          ExplicitLeft = 384
          ExplicitTop = 5
          ExplicitHeight = 19
        end
        object Label3: TLabel
          AlignWithMargins = True
          Left = 224
          Top = 8
          Width = 49
          Height = 18
          Margins.Top = 8
          Align = alLeft
          Caption = #1050' '#1074#1099#1076#1072#1095#1077
          ExplicitHeight = 13
        end
        object Shape3: TShape
          AlignWithMargins = True
          Left = 124
          Top = 7
          Width = 9
          Height = 15
          Margins.Left = 20
          Margins.Top = 7
          Margins.Bottom = 7
          Align = alLeft
          Brush.Color = clBlue
          Pen.Style = psClear
          Shape = stCircle
          ExplicitLeft = 220
          ExplicitTop = 5
          ExplicitHeight = 19
        end
        object Label4: TLabel
          AlignWithMargins = True
          Left = 328
          Top = 8
          Width = 58
          Height = 18
          Margins.Top = 8
          Align = alLeft
          Caption = #1050' '#1086#1090#1087#1088#1072#1074#1082#1077
          ExplicitHeight = 13
        end
        object Shape4: TShape
          AlignWithMargins = True
          Left = 209
          Top = 7
          Width = 9
          Height = 15
          Margins.Left = 20
          Margins.Top = 7
          Margins.Bottom = 7
          Align = alLeft
          Brush.Color = clAqua
          Pen.Style = psClear
          Shape = stCircle
          ExplicitLeft = 300
          ExplicitTop = 5
          ExplicitHeight = 19
        end
        object Shape5: TShape
          AlignWithMargins = True
          Left = 313
          Top = 7
          Width = 9
          Height = 15
          Margins.Left = 5
          Margins.Top = 7
          Margins.Bottom = 7
          Align = alLeft
          Brush.Color = clYellow
          Pen.Style = psClear
          Shape = stCircle
          ExplicitLeft = 272
          ExplicitTop = 5
          ExplicitHeight = 19
        end
        object Label5: TLabel
          AlignWithMargins = True
          Left = 424
          Top = 8
          Width = 122
          Height = 18
          Margins.Top = 8
          Align = alLeft
          Caption = #1057#1087#1083#1072#1085#1080#1088#1086#1074#1072#1085#1072' '#1087#1086#1075#1088#1091#1079#1082#1072
          ExplicitHeight = 13
        end
        object Shape6: TShape
          AlignWithMargins = True
          Left = 296
          Top = 7
          Width = 9
          Height = 15
          Margins.Left = 20
          Margins.Top = 7
          Margins.Bottom = 7
          Align = alLeft
          Brush.Color = 12710143
          Pen.Style = psClear
          Shape = stCircle
          ExplicitLeft = 272
          ExplicitTop = 5
          ExplicitHeight = 19
        end
        object sbMinus: TSpeedButton
          AlignWithMargins = True
          Left = 581
          Top = 3
          Width = 23
          Height = 23
          Align = alRight
          Caption = '-'
          Visible = False
          OnClick = sbMinusClick
          ExplicitLeft = 752
          ExplicitTop = 8
          ExplicitHeight = 22
        end
        object sbPlus: TSpeedButton
          AlignWithMargins = True
          Left = 610
          Top = 3
          Width = 23
          Height = 23
          Align = alRight
          Caption = '+'
          Visible = False
          OnClick = sbPlusClick
          ExplicitLeft = 629
        end
      end
    end
    object pg: TProgressBar
      AlignWithMargins = True
      Left = 300
      Top = 712
      Width = 362
      Height = 22
      Margins.Left = 300
      Margins.Top = 10
      Margins.Right = 400
      Align = alBottom
      BarColor = 3853363
      BackgroundColor = 15329769
      TabOrder = 1
      Visible = False
    end
    object pc: TJvgPageControl
      Left = 0
      Top = 29
      Width = 1061
      Height = 673
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Style = tsButtons
      TabOrder = 2
      TabStop = False
      StyleElements = [seFont, seBorder]
      TabStyle.Borders = [fsdLeft, fsdTop, fsdRight, fsdBottom]
      TabStyle.BevelInner = bvNone
      TabStyle.BevelOuter = bvNone
      TabStyle.Bold = False
      TabStyle.BackgrColor = clWhite
      TabStyle.Font.Charset = DEFAULT_CHARSET
      TabStyle.Font.Color = clBlack
      TabStyle.Font.Height = -16
      TabStyle.Font.Name = 'Arial'
      TabStyle.Font.Style = []
      TabStyle.CaptionHAlign = fhaCenter
      TabStyle.Gradient.Active = False
      TabStyle.Gradient.Orientation = fgdHorizontal
      TabSelectedStyle.Borders = [fsdLeft, fsdTop, fsdRight, fsdBottom]
      TabSelectedStyle.BevelInner = bvNone
      TabSelectedStyle.BevelOuter = bvNone
      TabSelectedStyle.Bold = False
      TabSelectedStyle.BackgrColor = 225
      TabSelectedStyle.Font.Charset = DEFAULT_CHARSET
      TabSelectedStyle.Font.Color = clWhite
      TabSelectedStyle.Font.Height = -16
      TabSelectedStyle.Font.Name = 'Arial'
      TabSelectedStyle.Font.Style = []
      TabSelectedStyle.CaptionHAlign = fhaCenter
      TabSelectedStyle.Gradient.Active = False
      TabSelectedStyle.Gradient.Orientation = fgdHorizontal
      Options = [ftoAutoFontDirection, ftoExcludeGlyphs]
    end
  end
  inherited pmGrid: TPopupMenu
    Left = 96
    Top = 280
    object N8: TMenuItem [0]
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
      OnClick = N8Click
    end
    inherited N1: TMenuItem
      Visible = False
    end
    inherited N2: TMenuItem
      Visible = False
    end
    inherited N3: TMenuItem
      Visible = False
    end
    inherited N7: TMenuItem
      Visible = False
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'delete from matrixhelper where operation_type =1'
      
        'and exists (select 1 from matrix2containers m2c where m2c.contai' +
        'ner_id = matrixhelper.container_id);'
      ''
      'delete from matrixhelper where operation_type =2'
      
        'and not exists (select 1 from matrix2containers m2c where m2c.co' +
        'ntainer_id = matrixhelper.container_id);'
      ''
      'delete from matrixhelper where state_mark = '#39'<'#39';'
      ''
      'update h set '
      'loaded_mark = mi.loaded, '
      'isempty = mi.isempty, '
      'state_mark = mi.state_mark, '
      'formalstate_mark = mi.formalstate_mark, '
      'loadplanned = mi.loadplanned, '
      'nextstate_mark = mi.next_state_mark, '
      'isdefective = mi.isdefective, '
      'ownmark = mi.ownmark'
      'from matrixhelper h (NOLOCK), v_containermatrixinfo mi'
      'where h.container_id = mi.container_id'
      'and h.isempty is null;'#9
      ''
      'select top 200'
      'h.id, '
      #39#39' as defect_column,'
      'h.container_id, '
      'h.iscompleted, '
      'h.ownmark +'#39' '#39'+'
      
        'c.cnum+'#39', '#39'+isnull((select ck.code from containerkinds ck where ' +
        'ck.id = c.kind_id), '#39'???'#39') as container_code, '
      'c.cnum, '
      'h.operation, '
      'h.operation_type, '
      'h.loaded_mark, '
      'h.state_mark, '
      'h.formalstate_mark, '
      'h.loadplanned, '
      'h.nextstate_mark, '
      'h.isdefective, '
      'h.isempty, '
      'h.ownmark'
      'from matrixhelper h (NOLOCK),  containers c, objects o'
      'where c.id = h.container_id'
      'and o.id = c.id'
      'and isnull(h.iscompleted, 0) = 0'
      'order by h.id desc')
    UpdateCommand.CommandText.Strings = (
      'update matrixhelper set iscompleted = 1 where id = :id;')
    UpdateCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from matrixhelper where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select h.id, '
      #39#39' as defect_column,'
      'h.container_id, '
      'h.iscompleted, '
      'h.ownmark +'#39' '#39'+'
      
        'c.cnum+'#39', '#39'+isnull((select ck.code from containerkinds ck where ' +
        'ck.id = c.kind_id), '#39'???'#39') as container_code, '
      'c.cnum, '
      'h.operation, '
      'h.operation_type, '
      'h.loaded_mark, '
      'h.state_mark, '
      'h.formalstate_mark, '
      'h.loadplanned, '
      'h.nextstate_mark, '
      'h.isdefective, '
      'h.isempty, '
      'h.ownmark'
      'from matrixhelper h (NOLOCK),  containers c, objects o'
      'where c.id = h.container_id'
      'and o.id = c.id'
      'and h.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited qrAux: TADOQuery
    Parameters = <
      item
        Name = 'cnum'
        Attributes = [paSigned]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      
        'select m2c.sector_id, mr.order_num as row_num, ms.order_num as s' +
        'tack_num, m2c.level_num, m2c.cell_num'
      
        'from matrix2containers m2c, containers c, matrixrows mr, matrixs' +
        'tacks ms '
      
        'where c.id = m2c.container_id and m2c.row_id = mr.id and m2c.sta' +
        'ck_id = ms.id'
      'and charindex(:cnum, c.cnum)>0')
    Left = 152
    Top = 280
  end
  inherited drvForms: TADODataDriverEh
    Left = 68
    Top = 498
  end
  inherited meForms: TMemTableEh
    Left = 116
    Top = 498
  end
  inherited dsForms: TDataSource
    Left = 164
    Top = 498
  end
  inherited al: TActionList
    Left = 40
    Top = 280
    inherited aAdd: TAction
      Enabled = False
    end
    inherited aEdit: TAction
      Enabled = False
    end
  end
  inherited exReport: TEXLReport
    Left = 198
    Top = 280
  end
  object drvSectors: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select ms.*, isnull(ms.roworderreverse,0) as rowreverse from mat' +
        'rixsectors ms'
      'where isactive=1'
      'order by place_order')
    SelectCommand.Parameters = <>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 392
    Top = 168
  end
  object meSectors: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvSectors
    Left = 440
    Top = 168
  end
  object Timer1: TTimer
    Interval = 10000
    OnTimer = Timer1Timer
    Left = 112
    Top = 104
  end
end
