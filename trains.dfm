﻿inherited FormTrains: TFormTrains
  Caption = #1055#1086#1077#1079#1076#1072
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited plTop: TPanel
      Visible = False
    end
    inherited dgData: TDBGridEh
      ReadOnly = False
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'tnum'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 311
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Width = 308
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_create'
          Footers = <>
          TextEditing = False
          Title.Caption = #1044#1072#1090#1072'/'#1074#1088#1077#1084#1103' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
          Width = 225
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from trains order by date_create, tnum')
    UpdateCommand.CommandText.Strings = (
      'update trains set tnum = :tnum, note = :note where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'tnum'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into trains (tnum, note) values ( :tnum, :note)')
    InsertCommand.Parameters = <
      item
        Name = 'tnum'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from trains where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
end
