﻿unit linkedinvoices;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls;

type
  TFormDocLinkedInvoices = class(TFormGrid)
    procedure meDataAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormDocLinkedInvoices: TFormDocLinkedInvoices;

implementation

{$R *.dfm}

uses editlinkedinvoice;

procedure TFormDocLinkedInvoices.Init;
begin
  drvData.SelectCommand.Parameters.ParamByName('doc_id').Value := self.parent_id;
  meData.Close;
  inherited;
  self.formEdit := FormEditLinkedInvoice;
end;

procedure TFormDocLinkedInvoices.meDataAfterInsert(DataSet: TDataSet);
begin
  inherited;
  meData.FieldByName('doc_id').Value := self.parent_id;
  meData.FieldByName('doc_date').Value := now();
end;

end.
