﻿inherited FormContainers: TFormContainers
  ActiveControl = dgData
  Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088#1099
  PixelsPerInch = 96
  TextHeight = 13
  inherited Splitter2: TSplitter
    Left = 273
    ExplicitLeft = 273
  end
  inherited plAll: TPanel
    Width = 273
    ExplicitWidth = 273
    inherited Splitter1: TSplitter
      Width = 273
      ExplicitWidth = 273
    end
    inherited plTop: TPanel
      Width = 273
      ExplicitWidth = 273
      inherited btTool: TPngSpeedButton
        Left = 235
        ExplicitLeft = 221
      end
      inherited sbClearFilter: TPngSpeedButton
        ExplicitLeft = 175
      end
      inherited plCount: TPanel
        Left = 38
        ExplicitLeft = 38
      end
    end
    inherited dgData: TDBGridEh
      Width = 273
      TitleParams.MultiTitle = True
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnum'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 89
        end
        item
          CellButtons = <>
          Color = 12117758
          DisplayFormat = 'dd.mm.yyyy'
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_inspection'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1089#1074#1080#1076#1077#1090'.'
          Width = 90
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isdefective'
          Footers = <>
          Title.Caption = #1053#1077'- '#1080#1089#1087#1088#1072'- '#1074#1077#1085
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isnotready'
          Footers = <>
          Title.Caption = #1053#1077' '#1075#1086#1090#1086#1074
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          Title.Caption = #1042#1080#1076
          Width = 85
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          Title.Caption = #1058#1077#1082#1091#1097#1080#1081' '#1089#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
          Width = 130
        end
        item
          CellButtons = <>
          Color = 12975322
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Verdana'
          Font.Style = []
          Footers = <>
          Title.Caption = #1058#1077#1082#1091#1097#1077#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 132
        end
        item
          CellButtons = <>
          Color = 9107892
          DynProps = <>
          EditButtons = <>
          FieldName = 'real_state_code'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsItalic]
          Footers = <>
          Title.Caption = #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 103
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'container_weight'
          Footers = <>
          Title.Caption = #1042#1077#1089
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'carrying'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086' '#1087#1086#1076#1100#1077#1084#1085#1086#1089#1090#1100
          Width = 97
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Width = 346
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    inherited plObjects: TPanel
      Width = 273
      ExplicitWidth = 273
      inherited Panel2: TPanel
        Width = 273
        ExplicitWidth = 273
      end
      inherited dgObjects: TDBGridEh
        Width = 273
        Font.Height = -11
        TitleParams.Font.Height = -11
      end
    end
    inherited plStatus: TPanel
      Width = 273
      Color = 15593971
      StyleElements = [seFont, seBorder]
      ExplicitWidth = 273
    end
  end
  inherited Panel1: TPanel
    Left = 280
    Width = 620
    ExplicitLeft = 280
    ExplicitWidth = 620
    inherited Splitter3: TSplitter
      Width = 620
      ExplicitWidth = 620
    end
    inherited dgStates: TDBGridEh
      Width = 620
      Font.Height = -11
      TitleParams.Font.Height = -11
    end
    inherited Panel3: TPanel
      Width = 620
      ExplicitWidth = 620
      inherited DBGridEh1: TDBGridEh
        Width = 620
        Font.Height = -11
        TitleParams.Font.Height = -11
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select c.*, o.start_datetime,  o.id as object_id, '
      
        '(select isnull(code,name) from counteragents c1 where c1.id = o.' +
        'owner_id) as owner_code,'
      
        '(select code from containerkinds k where k.id = c.kind_id) as ki' +
        'nd_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select code from objectstatekinds s where s.id = o.real_state_i' +
        'd) as real_state_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code'
      
        'from objects o, containers c where o.id = c.id --and o.object_ty' +
        'pe = '#39'container'#39
      'and dbo.filter_objects(:guid, c.cnum) = 1')
    SelectCommand.Parameters = <
      item
        Name = 'guid'
        Attributes = [paSigned]
        DataType = ftString
        Precision = 10
        Size = 4
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update containers'
      'set'
      '  cnum = :cnum,'
      '  kind_id = :kind_id,'
      '  size = :size,'
      '  container_weight = :container_weight,'
      '  carrying = :carrying,'
      '  date_made = :date_made,'
      '  date_inspection = :date_inspection,'
      '  date_stiker = :date_stiker,'
      '  picture = :picture,'
      '  note = :note,'
      '  isdefective = :isdefective,'
      ' acep = :acep, '
      ' isnotready = :isnotready'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'cnum'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'size'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_weight'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'carrying'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'date_made'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_inspection'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_stiker'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'picture'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 850
        Value = Null
      end
      item
        Name = 'isdefective'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'acep'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'isnotready'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'begin'
      ''
      'declare @newid int;'
      'exec InsertObject '#39'container'#39', @newid output;'
      ''
      'insert into containers'
      
        '  (id, cnum, kind_id, size, container_weight, carrying, date_mad' +
        'e, date_inspection, date_stiker, picture, note, isdefective, ace' +
        'p, isnotready)'
      'values'
      
        '  (@newid, :cnum, :kind_id, :size, :container_weight, :carrying,' +
        ' :date_made, :date_inspection, :date_stiker, :picture,  :note, :' +
        'isdefective, :acep, :isnotready)'
      ''
      ''
      ''
      'end;')
    InsertCommand.Parameters = <
      item
        Name = 'cnum'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'size'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_weight'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'carrying'
        Attributes = [paSigned, paNullable]
        DataType = ftFloat
        NumericScale = 255
        Precision = 15
        Size = 8
        Value = Null
      end
      item
        Name = 'date_made'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_inspection'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'date_stiker'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'picture'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 850
        Value = Null
      end
      item
        Name = 'isdefective'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'acep'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'isnotready'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from objects where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select c.*, o.start_datetime,  o.id as object_id, '
      
        '(select code from counteragents c1 where c1.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from containerkinds k where k.id = c.kind_id) as ki' +
        'nd_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code'
      
        'from objects o, containers c where o.id = c.id and o.object_type' +
        ' = '#39'container'#39
      'and o.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
  inherited meStates: TMemTableEh
    AggregatesActive = True
    DetailFields = 'object_id'
    MasterFields = 'object_id'
  end
  inherited meOwner: TMemTableEh
    MasterFields = 'object_id'
  end
end
