﻿inherited FormTasksRestrictControl: TFormTasksRestrictControl
  ActiveControl = dgData
  Caption = #1059#1087#1088#1072#1074#1083#1077#1085#1080#1077' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1103#1084#1080
  ClientHeight = 542
  ClientWidth = 925
  ExplicitWidth = 941
  ExplicitHeight = 580
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 501
    Width = 925
    ExplicitTop = 501
    ExplicitWidth = 925
    inherited btnOk: TButton
      Left = 690
      Caption = #1054#1050
      ExplicitLeft = 690
    end
    inherited btnCancel: TButton
      Left = 809
      ExplicitLeft = 809
    end
  end
  inherited plAll: TPanel
    Width = 925
    Height = 501
    ExplicitWidth = 925
    ExplicitHeight = 501
    inherited plTop: TPanel
      Width = 925
      ExplicitWidth = 925
      inherited btTool: TPngSpeedButton
        Left = 873
        ExplicitLeft = 873
      end
    end
    inherited dgData: TDBGridEh
      Width = 925
      Height = 472
      Columns = <
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'start_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1103
          Width = 132
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'delivery_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
          Width = 129
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 143
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer_code'
          Footers = <>
          Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
          Width = 116
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'object_code'
          Footers = <>
          Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
          Width = 127
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'restr_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1103
          Width = 138
        end
        item
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isactive'
          Footers = <>
          Title.Caption = #1040#1082#1090#1091#1072#1083#1100#1085#1086#1089#1090#1100
          Width = 130
        end
        item
          DynProps = <>
          EditButtons = <>
          FieldName = 'cancel_base'
          Footers = <>
          Title.Caption = #1044#1086#1082#1091#1084#1077#1085#1090'/'#1086#1089#1085#1086#1074#1072#1085#1080#1077
          Width = 302
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select r.*,'
      
        '(select code from deliverytypes t1 where t1.id = t.deliverytype_' +
        'id) as delivery_code,'
      
        '(select code from counteragents c1 where c1.id = t.consignee_id)' +
        ' as consignee_code,'
      
        '(select code from counteragents c2 where c2.id = t.payer_id) as ' +
        'payer_code,'
      '('
      #9'select '
      
        #9'(select name from objecttypes ot where ot.code = o.object_type)' +
        '+'#39': '#39'+'
      #9'cnum from v_objects o where o.id = t.object_id'
      ') as object_code,'
      
        '(select code from restrictkinds k where k.id = r.kind_id) as res' +
        'tr_code'
      'from restrictions r, tasks t where r.task_id = t.id')
    UpdateCommand.CommandText.Strings = (
      'update restrictions'
      'set'
      '  start_date = :start_date,'
      '  kind_id = :kind_id,'
      '  task_id = :task_id,'
      '  isactive = :isactive,'
      '  cancel_base = :cancel_base'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'start_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'task_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isactive'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'cancel_base'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 850
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into restrictions'
      '  (start_date, kind_id, task_id, isactive, cancel_base)'
      'values'
      '  (:start_date, :kind_id, :task_id, :isactive, :cancel_base)')
    InsertCommand.Parameters = <
      item
        Name = 'start_date'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'task_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isactive'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'cancel_base'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 850
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from restrictions where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select r.*,'
      
        '(select code from deliverytypes t1 where t1.id = t.deliverytype_' +
        'id) as delivery_code,'
      
        '(select code from counteragents c1 where c1.id = t.consignee_id)' +
        ' as consignee_code,'
      
        '(select code from counteragents c2 where c2.id = t.payer_id) as ' +
        'payer_code,'
      '('
      #9'select '
      
        #9'(select name from objecttypes ot where ot.code = o.object_type)' +
        '+'#39': '#39'+'
      #9'cnum from v_objects o where o.id = t.object_id'
      ') as object_code,'
      
        '(select code from restrictkinds k where k.id = r.kind_id) as res' +
        'tr_code'
      'from restrictions r, tasks t where r.task_id = t.id'
      'and r.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
end
