﻿unit removaldocs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, docflow, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, Functions, Vcl.Mask, DBCtrlsEh, System.ImageList, Vcl.ImgList,
  PngImageList;

type
  TFormRemovalDocs = class(TFormDocFlow)
    plSpec: TPanel;
    sbAddSpec: TPngSpeedButton;
    sbDeleteSpec: TPngSpeedButton;
    sbEditSpec: TPngSpeedButton;
    N15: TMenuItem;
    N16: TMenuItem;
    sbCheck: TPngSpeedButton;
    Bevel8: TBevel;
    sbCancelConfirmSpec: TPngSpeedButton;
    procedure N15Click(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure sbCheckClick(Sender: TObject);
    procedure sbCancelConfirmSpecClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Init; override;
    procedure SetFilter(conditions: string); override;
  end;

var
  FormRemovalDocs: TFormRemovalDocs;

implementation

{$R *.dfm}

uses editremovaldoc, deliverytypes, selectdeliverytype, dmu, EditFactArrivalGo,
  editinspection, main;


procedure TFormRemovalDocs.SetFilter(conditions: string);

begin
  if pos('date_begin', conditions)=0 then
  conditions := conditions+'date_begin=01.01.1900;';
  inherited SetFilter(conditions);
end;


procedure TFormRemovalDocs.dgDataDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

  if Column.FieldName = 'restr_sign' then
   if meData.FieldByName('restr_sign').AsString = 'Есть' then
   begin
    dgData.Canvas.Brush.Color := clRed;
    dgData.Canvas.Font.Color := clWhite;
   end;

  if Column.FieldName = 'letter_sign' then
   if meData.FieldByName('letter_sign').AsString = 'Есть' then
    dgData.Canvas.Brush.Color := clYellow;

  inherited;

end;

procedure TFormRemovalDocs.Init;
begin
  self.system_section := 'income_removal';
  inherited;
  self.formEdit := FormEditRemovalDoc;
  if FormMain.isAdmin then allow_edit := true;
end;


procedure TFormRemovalDocs.N15Click(Sender: TObject);
begin
  allow_edit := true;
  meData.ReadOnly := false;
  EE(nil, FormEditFactArrivalGo, meData, 'docremoval');
  allow_edit := false;
  if FormMain.isAdmin then allow_edit := true;
end;

procedure TFormRemovalDocs.sbCancelConfirmSpecClick(Sender: TObject);
begin

  if not FormMain.isAdmin then exit;

  //if meSpec.RecordCount < 1 then exit;

  RefreshRecord(meData);
  if meData.FieldByName('isconfirmed').AsInteger = 0 then
  begin
    ShowMessage('Документ не проведен.');
    exit;
  end;

  //if meSpec.RecordCount < 2 then
  //begin
  //  ShowMessage('Снять проведение с одной записи нельзя. Нужно распроводить документ.');
  //  exit;
  //end;

  if self.meLinkedObjects.FieldByName('object_type').AsString <> 'cargo' then
  begin
    ShowMessage('Установите курсор на записи с типом обьекта "cargo".');
    exit;
  end;

  if fQYN('Cнять проведение с записи?') then
  begin
    try
      dm.spCancelConfirmDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
      dm.spCancelConfirmDoc.Parameters.ParamByName('@CargoId').Value := self.meLinkedObjects.FieldByName('id').AsInteger;
      dm.spCancelConfirmDoc.Parameters.ParamByName('@UserId').Value := FormMain.currentUserId;
      dm.spCancelConfirmDoc.ExecProc;
    finally
      self.meDataAfterScroll(meData);
    end;
  end;

end;

procedure TFormRemovalDocs.sbCheckClick(Sender: TObject);
begin
  EE(nil, FormEditInspection, meData, 'docremoval');
end;

end.
