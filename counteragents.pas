﻿unit counteragents;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, MemTableDataEh, Data.DB,
  Data.Win.ADODB, MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Vcl.Mask, DBCtrlsEh, Vcl.DBCGrids, DBVertGridsEh;

type
  TFormCounteragents = class(TFormGrid)
    plLeft: TPanel;
    Splitter1: TSplitter;
    plTool: TPanel;
    Bevel1: TBevel;
    N10: TMenuItem;
    N11: TMenuItem;
    plFilter: TPanel;
    procedure meDataAfterInsert(DataSet: TDataSet);
    procedure N10Click(Sender: TObject);
    procedure meFoldersBeforePost(DataSet: TDataSet);
    procedure N11Click(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure meDataAfterEdit(DataSet: TDataSet);
  private
    procedure FilterByClassif(Sender: TObject);
  public
    procedure Init; override;
  end;

var
  FormCounteragents: TFormCounteragents;

implementation

{$R *.dfm}

uses editcounteragent, editcatalog, selectfolder, functions, dmu, GroupCounteragents;


procedure TFormCounteragents.dgDataDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

   if meData.FieldByName('parent_id').AsInteger > 0 then
   begin
    dgData.Canvas.Font.Color := $004F4F4F;
    dgData.Canvas.Font.Style := [fsItalic];
   end;

   if meData.FieldByName('exists_child').AsInteger > 0 then
   begin
    dgData.Canvas.Font.Style := [fsBold];
   end;

  inherited;
end;

procedure TFormCounteragents.Init;
var s: TDbGridEhStyle;
begin

  tablename := 'counteragents';

  self.formEdit := FormEditCounteragent;

  if  isinited then exit
  else MakeClassifFilterSet(plFilter,'counteragents', 5, FilterByClassif);

  inherited;

end;


procedure TFormCounteragents.FilterByClassif(Sender: TObject);
begin
 ShowMessage('!');
end;


procedure TFormCounteragents.meDataAfterEdit(DataSet: TDataSet);
begin
  inherited;
  meData.FieldByName('code').Required := true;
  meData.FieldByName('name').Required := true;
  meData.FieldByName('inn').Required := true;
end;

procedure TFormCounteragents.meDataAfterInsert(DataSet: TDataSet);
begin
  meData.FieldByName('code').Required := true;
  meData.FieldByName('name').Required := true;
  meData.FieldByName('inn').Required := true;
end;

procedure TFormCounteragents.meFoldersBeforePost(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('folder_section').Value := 'counteragents';
end;

procedure TFormCounteragents.N10Click(Sender: TObject);
begin
  if fQYN('Обьединить всех контрагентов группы?') then
  begin
    dm.spUnionCounteragents.Parameters.ParamByName('@id').Value := meData.FieldByName('id').AsInteger;
    dm.spUnionCounteragents.ExecProc;
    ShowMessage('Готово!');
  end;
end;

procedure TFormCounteragents.N11Click(Sender: TObject);
var id: integer; gg: TGuid;
begin

    FormGroupCounteragents.meTable.Close;
    FormGroupCounteragents.qrAux.Close;
    FormGroupCounteragents.qrAux.Open;
    FormGroupCounteragents.meTable.LoadFromDataSet(FormGroupCounteragents.qrAux,0,lmCopy,false);
    FormGroupCounteragents.meTable.Edit;
    FormGroupCounteragents.meTable.Fields[0].Value := meData.FieldByName('parent_id').AsInteger;
    FormGroupCounteragents.ssCounteragents.Parameters.SetPairValue('selfid', meData.FieldByName('id').Value);
    FormGroupCounteragents.ShowModal;

    if (FormGroupCounteragents.ModalResult = mrOk)
      //and (FormGroupCounteragents.luCounteragent1.KeyValue > 0)
    then
    begin

      CreateGUID(gg);
      g := GuidToString(gg);

      id := meData.FieldByName('id').AsInteger;

      self.FillSelectListAll(g);

      dm.spSetParentCounteragent.Parameters.ParamByName('@selectguid').Value := g;
      dm.spSetParentCounteragent.Parameters.ParamByName('@ParentId').Value := FormGroupCounteragents.luCounteragent1.KeyValue;
      dm.spSetParentCounteragent.ExecProc;

      meData.Close;
      meData.Open;
      meData.Locate('id', FormGroupCounteragents.luCounteragent1.KeyValue, []);

      dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
      dm.spClearSelectList.ExecProc;

    end;

    FormGroupCounteragents.qrAux.Close;

    meData.Locate('id',id,[]);

end;

end.
