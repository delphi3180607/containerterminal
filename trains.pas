﻿unit trains;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand, EXLReport;

type
  TFormTrains = class(TFormGrid)
    procedure N5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormTrains: TFormTrains;

implementation

{$R *.dfm}

procedure TFormTrains.N5Click(Sender: TObject);
begin
  meData.Close;
  meData.Open;
end;

end.
