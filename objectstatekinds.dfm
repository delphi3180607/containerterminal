﻿inherited FormObjectStateKinds: TFormObjectStateKinds
  Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1103' '#1086#1073#1100#1077#1082#1090#1086#1074
  ClientWidth = 891
  ExplicitWidth = 907
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Width = 891
    ExplicitWidth = 891
    inherited btnOk: TButton
      Left = 656
      ExplicitLeft = 656
    end
    inherited btnCancel: TButton
      Left = 775
      ExplicitLeft = 775
    end
  end
  inherited plAll: TPanel
    Width = 891
    ExplicitWidth = 891
    inherited plTop: TPanel
      Width = 891
      ExplicitWidth = 891
      inherited btTool: TPngSpeedButton
        Left = 853
        ExplicitLeft = 853
      end
      inherited plCount: TPanel
        Left = 656
        ExplicitLeft = 656
      end
    end
    inherited dgData: TDBGridEh
      Width = 891
      TitleParams.RowLines = 3
      Columns = <
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'id'
          Footers = <>
          Title.Caption = 'Id'
          Width = 55
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1050#1086#1076
          Width = 128
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'name'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Width = 217
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'mark'
          Footers = <>
          Title.Caption = #1054#1090#1084#1077#1090#1082#1072
          Width = 65
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'inplacesign'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1055#1088#1080#1079#1085#1072#1082' "'#1085#1072' '#1090#1077#1088#1084#1080#1085#1072#1083#1077'"'
          Width = 98
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'inworksign'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1055#1088#1080#1079#1085#1072#1082' "'#1074' '#1088#1072#1073#1086#1090#1077'"'
          Width = 94
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isformalsign'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1055#1088#1080#1079#1085#1072#1082' "'#1092#1086#1088#1084#1072#1083#1100#1085#1099#1081'"'
          Width = 101
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'allow_repeat'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086' '#1087#1086#1074#1090#1086#1088#1085#1086#1077' '#1086#1092#1086#1088#1084#1083#1077#1085#1080#1077' '
          Width = 139
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'stop_sign'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1055#1088#1080#1079#1085#1072#1082' '#1057#1058#1054#1055
          Width = 80
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from objectstatekinds order by code')
    UpdateCommand.CommandText.Strings = (
      'update objectstatekinds set '
      'code = :code, '
      'name = :name,'
      'inplacesign = :inplacesign,'
      'inworksign = :inworksign, '
      'isformalsign = :isformalsign,'
      'allow_repeat = :allow_repeat,'
      'stop_sign = :stop_sign, '
      'mark = :mark'
      'where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'inplacesign'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'inworksign'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'isformalsign'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'allow_repeat'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'stop_sign'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'mark'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      
        'insert into objectstatekinds (code, name, inplacesign, inworksig' +
        'n, isformalsign, allow_repeat, stop_sign, mark) '
      
        'values ( :code, :name, :inplacesign, :inworksign, :isformalsign,' +
        ' :allow_repeat, :stop_sign, :mark )')
    InsertCommand.Parameters = <
      item
        Name = 'code'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'inplacesign'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'inworksign'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'isformalsign'
        Size = -1
        Value = Null
      end
      item
        Name = 'allow_repeat'
        Size = -1
        Value = Null
      end
      item
        Name = 'stop_sign'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from objectstatekinds where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from objectstatekinds where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
end
