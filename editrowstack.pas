﻿unit EditRowStack;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBCtrlsEh, Vcl.StdCtrls, Vcl.Mask,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEditRowStack = class(TFormEdit)
    cbActive: TDBCheckBoxEh;
    neOrder: TDBNumberEditEh;
    edNote: TDBEditEh;
    cbNarrow: TDBCheckBoxEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditRowStack: TFormEditRowStack;

implementation

{$R *.dfm}

end.
