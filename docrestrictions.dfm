﻿inherited FormDocRestrictions: TFormDocRestrictions
  ActiveControl = dgData
  Caption = #1047#1072#1087#1088#1077#1090#1099' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1084
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 474
    Height = 37
    ExplicitTop = 474
    ExplicitHeight = 37
    inherited btnOk: TButton
      Height = 31
      ExplicitHeight = 31
    end
    inherited btnCancel: TButton
      Height = 31
      ExplicitHeight = 31
    end
  end
  inherited plAll: TPanel
    Height = 474
    ExplicitHeight = 474
    inherited dgData: TDBGridEh
      Height = 445
      IndicatorParams.Color = clBtnFace
      IndicatorParams.FillStyle = cfstGradientEh
      TitleParams.Color = clBtnFace
      TitleParams.FillStyle = cfstGradientEh
      TitleParams.HorzLineColor = clSilver
      TitleParams.MultiTitle = True
      TitleParams.VertLineColor = clSilver
      Columns = <
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isactive'
          Footers = <>
          Title.Caption = #1040#1082#1090#1091#1072#1083#1100#1085#1086'?'
          Width = 92
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'datestart'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 136
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dateend'
          Footers = <>
          Title.Caption = #1047#1072#1082#1088#1099#1090#1086
          Width = 86
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'type'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1079#1072#1087#1088#1077#1090#1072
          Width = 187
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'object_num'
          Footers = <>
          Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
          Width = 135
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1058#1077#1089#1090
          Width = 335
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'base'
          Footers = <>
          Title.Caption = #1054#1089#1085#1086#1074#1072#1085#1080#1077
          Width = 116
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'declare @docid int;'
      ''
      'select @docid = :doc_id;'
      ''
      'select r.*,'
      '(select code from restrictkinds where id = r.type_id) as type,'
      
        '(select cnum from v_objects where id = r.object_id) as object_nu' +
        'm '
      'from doccargorestrictions r'
      
        'where (doc_id = @docid or task_id in (select task_id from object' +
        's2docspec where doc_id = @docid))'
      'order by datestart')
    SelectCommand.Parameters = <
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update doccargorestrictions'
      'set'
      '  isactive = :isactive,'
      '  type_id = :type_id,'
      '  datestart = :datestart,'
      '  dateend = :dateend,'
      '  base = :base,'
      '  note = :note'
      'where'
      '  id = :OLD_id')
    UpdateCommand.Parameters = <
      item
        Name = 'isactive'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'datestart'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'dateend'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'base'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 850
        Value = Null
      end
      item
        Name = 'OLD_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into doccargorestrictions'
      '  (doc_id, isactive, type_id, datestart, dateend, base, note)'
      'values'
      
        '  (:doc_id, :isactive, :type_id, :datestart, :dateend, :base, :n' +
        'ote)')
    InsertCommand.Parameters = <
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isactive'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'datestart'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'dateend'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'base'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 850
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from doccargorestrictions where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select r.*,'
      '(select code from restrictkinds where id = r.type_id) as type,'
      
        '(select cnum from v_objects where id = r.object_id) as object_nu' +
        'm '
      'from doccargorestrictions r'
      'where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
end
