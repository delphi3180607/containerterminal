﻿unit selectobjecttype;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls,
  DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh;

type
  TFormSelectObjectType = class(TFormEdit)
    dbData: TDBGridEh;
    dsData: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSelectObjectType: TFormSelectObjectType;

implementation

{$R *.dfm}

end.
