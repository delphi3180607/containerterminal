﻿unit editdocroute2objects;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBCtrlsEh, Vcl.StdCtrls,
  DBSQLLookUp, Vcl.Mask, Vcl.ExtCtrls, Vcl.Buttons, Data.DB, Data.Win.ADODB,
  DBGridEh, DBLookupEh;

type
  TFormEditDocroute2Object = class(TFormEdit)
    leEndState: TDBSQLLookUp;
    ssStateKind: TADOLookUpSqlSet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    sbClear: TSpeedButton;
    Label4: TLabel;
    SpeedButton1: TSpeedButton;
    Label6: TLabel;
    leStartState: TDBSQLLookUp;
    cbRelationEnd: TDBComboBoxEh;
    cbRelationStart: TDBComboBoxEh;
    cbObjectType: TDBSQLLookUp;
    ssObjectTypes: TADOLookUpSqlSet;
    Label5: TLabel;
    leRealStartstate: TDBSQLLookUp;
    ssRealStateKind: TADOLookUpSqlSet;
    cbEmpty: TDBCheckBoxEh;
    edSealNumber: TDBEditEh;
    procedure sbClearClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditDocroute2Object: TFormEditDocroute2Object;

implementation

{$R *.dfm}

uses main;

procedure TFormEditDocroute2Object.sbClearClick(Sender: TObject);
begin
  self.cbRelationStart.ItemIndex := -1;
end;

procedure TFormEditDocroute2Object.SpeedButton1Click(Sender: TObject);
begin
  self.cbRelationEnd.ItemIndex := -1;
end;

end.
