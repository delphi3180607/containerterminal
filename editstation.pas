﻿unit editstation;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, DBCtrlsEh, Data.DB, Data.Win.ADODB;

type
  TFormEditStation = class(TFormEdit)
    Label2: TLabel;
    edCode: TDBEditEh;
    Label3: TLabel;
    edName: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditStation: TFormEditStation;

implementation

{$R *.dfm}

end.
