﻿inherited FormSelectCounteragent: TFormSelectCounteragent
  Caption = #1042#1099#1073#1086#1088' '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090#1072
  ClientHeight = 126
  ClientWidth = 401
  ExplicitWidth = 407
  ExplicitHeight = 154
  PixelsPerInch = 96
  TextHeight = 16
  object Label8: TLabel [0]
    Left = 8
    Top = 16
    Width = 76
    Height = 16
    Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  inherited plBottom: TPanel
    Top = 85
    Width = 401
    TabOrder = 1
    inherited btnCancel: TButton
      Left = 285
    end
    inherited btnOk: TButton
      Left = 166
    end
  end
  object luCustomer: TDBSQLLookUp [2]
    Left = 8
    Top = 38
    Width = 387
    Height = 23
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    HighlightRequired = True
    ParentFont = False
    TabOrder = 0
    Visible = True
    SqlSet = dm.ssCounteragents
  end
  inherited dsLocal: TDataSource
    Left = 144
    Top = 56
  end
  inherited qrAux: TADOQuery
    Left = 96
    Top = 56
  end
end
