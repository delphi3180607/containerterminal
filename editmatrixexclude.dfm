﻿inherited FormEditMatrixExclude: TFormEditMatrixExclude
  Caption = #1048#1089#1082#1083#1102#1095#1077#1085#1080#1077
  ClientHeight = 279
  ClientWidth = 342
  ExplicitWidth = 348
  ExplicitHeight = 307
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 238
    Width = 342
    TabOrder = 7
    ExplicitTop = 178
    ExplicitWidth = 279
    inherited btnCancel: TButton
      Left = 226
      ExplicitLeft = 163
    end
    inherited btnOk: TButton
      Left = 107
      ExplicitLeft = 44
    end
  end
  object edRowStart: TDBNumberEditEh [1]
    Left = 8
    Top = 89
    Width = 121
    Height = 24
    ControlLabel.Width = 72
    ControlLabel.Height = 16
    ControlLabel.Caption = #1064#1090#1072#1073#1077#1083#1100' '#1089
    ControlLabel.Visible = True
    DataField = 'row_start'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object edStackStart: TDBNumberEditEh [2]
    Left = 8
    Top = 145
    Width = 121
    Height = 24
    ControlLabel.Width = 63
    ControlLabel.Height = 16
    ControlLabel.Caption = #1057#1077#1082#1094#1080#1103' '#1089
    ControlLabel.Visible = True
    DataField = 'stack_start'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
  object edLevels: TDBNumberEditEh [3]
    Left = 8
    Top = 198
    Width = 121
    Height = 24
    ControlLabel.Width = 48
    ControlLabel.Height = 16
    ControlLabel.Caption = #1071#1088#1091#1089#1086#1074
    ControlLabel.Visible = True
    DataField = 'levels'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
  object edStackEnd: TDBNumberEditEh [4]
    Left = 148
    Top = 145
    Width = 117
    Height = 24
    ControlLabel.Width = 71
    ControlLabel.Height = 16
    ControlLabel.Caption = #1057#1077#1082#1094#1080#1103' '#1087#1086
    ControlLabel.Visible = True
    DataField = 'stack_end'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Visible = True
  end
  object edRowEnd: TDBNumberEditEh [5]
    Left = 144
    Top = 89
    Width = 121
    Height = 24
    ControlLabel.Width = 80
    ControlLabel.Height = 16
    ControlLabel.Caption = #1064#1090#1072#1073#1077#1083#1100' '#1087#1086
    ControlLabel.Visible = True
    DataField = 'row_end'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object edCells: TDBNumberEditEh [6]
    Left = 148
    Top = 198
    Width = 121
    Height = 24
    ControlLabel.Width = 40
    ControlLabel.Height = 16
    ControlLabel.Caption = #1071#1095#1077#1077#1082
    ControlLabel.Visible = True
    DataField = 'cells'
    DataSource = dsLocal
    DecimalPlaces = 0
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Visible = True
  end
  object cbLimitType: TDBComboBoxEh [7]
    Left = 8
    Top = 27
    Width = 261
    Height = 24
    ControlLabel.Width = 116
    ControlLabel.Height = 16
    ControlLabel.Caption = #1058#1080#1087' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1103
    ControlLabel.Visible = True
    DataField = 'limit_type'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Items.Strings = (
      #1071#1088#1091#1089#1099
      #1071#1095#1077#1081#1082#1080)
    KeyItems.Strings = (
      '1'
      '2')
    TabOrder = 0
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 64
    Top = 256
  end
  inherited qrAux: TADOQuery
    Left = 16
    Top = 256
  end
end
