﻿inherited FormEditMtuKind: TFormEditMtuKind
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1052#1058#1059
  ClientHeight = 309
  ClientWidth = 679
  ExplicitWidth = 685
  ExplicitHeight = 337
  PixelsPerInch = 96
  TextHeight = 16
  object Label4: TLabel [2]
    Left = 8
    Top = 139
    Width = 379
    Height = 14
    Caption = #1056#1080#1089#1091#1085#1086#1082' ('#1085#1072#1080#1073#1086#1083#1077#1077' '#1074#1077#1088#1086#1103#1090#1085#1099#1081', '#1089#1074#1103#1079#1072#1085#1085#1099#1081', '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel [3]
    Left = 232
    Top = 8
    Width = 90
    Height = 16
    Caption = #1042#1080#1076' '#1087#1086#1075#1088#1091#1079#1082#1080
  end
  object Label5: TLabel [4]
    Left = 453
    Top = 8
    Width = 79
    Height = 16
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
  end
  object Label6: TLabel [5]
    Left = 8
    Top = 200
    Width = 82
    Height = 16
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
  end
  inherited plBottom: TPanel
    Top = 268
    Width = 679
    TabOrder = 6
    ExplicitTop = 268
    ExplicitWidth = 679
    inherited btnCancel: TButton
      Left = 563
      ExplicitLeft = 563
    end
    inherited btnOk: TButton
      Left = 444
      ExplicitLeft = 444
    end
  end
  inherited edCode: TDBEditEh
    Width = 209
    ExplicitWidth = 209
  end
  inherited edName: TDBEditEh
    Width = 654
    TabOrder = 3
    ExplicitWidth = 654
  end
  object luPictureKind: TDBSQLLookUp [9]
    Left = 8
    Top = 158
    Width = 654
    Height = 22
    DataField = 'picture_id'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = True
    SqlSet = ssPictureKinds
    RowCount = 0
  end
  object edLoadCode: TDBEditEh [10]
    Left = 232
    Top = 30
    Width = 209
    Height = 24
    DataField = 'load_code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object edAmount: TDBEditEh [11]
    Left = 453
    Top = 30
    Width = 209
    Height = 24
    DataField = 'amount_code'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object edNote: TDBEditEh [12]
    Left = 8
    Top = 222
    Width = 654
    Height = 24
    DataField = 'note'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    Visible = True
  end
  object ssPictureKinds: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select id, name as codename from picturekinds '
      'where code like '#39'%@pattern%'#39' or name like '#39'%@pattern%'#39
      'order by code')
    DownSql.Strings = (
      'select id, name as codename from picturekinds'
      'order by code'
      '')
    InitSql.Strings = (
      'select id, name as codename from picturekinds where id = @id'
      '')
    KeyName = 'id'
    DisplayName = 'codename'
    Connection = dm.connMain
    Left = 134
    Top = 144
  end
end
