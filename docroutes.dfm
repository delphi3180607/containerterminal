﻿inherited FormDocRoutes: TFormDocRoutes
  ActiveControl = dgObjects
  Caption = #1057#1093#1077#1084#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1086#1073#1086#1088#1086#1090#1072
  ClientHeight = 748
  ClientWidth = 1088
  ExplicitWidth = 1104
  ExplicitHeight = 786
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 185
    Top = 0
    Width = 6
    Height = 707
    ExplicitLeft = 121
    ExplicitHeight = 587
  end
  object Splitter3: TSplitter [1]
    Left = 1057
    Top = 0
    Width = 5
    Height = 707
    ExplicitLeft = 833
    ExplicitTop = 29
    ExplicitHeight = 587
  end
  inherited plBottom: TPanel
    Top = 707
    Width = 1088
    ExplicitTop = 707
    ExplicitWidth = 1088
    inherited btnOk: TButton
      Left = 853
      ExplicitLeft = 853
    end
    inherited btnCancel: TButton
      Left = 972
      ExplicitLeft = 972
    end
  end
  inherited plAll: TPanel
    Left = 191
    Width = 866
    Height = 707
    Align = alLeft
    Constraints.MaxWidth = 1200
    ExplicitLeft = 191
    ExplicitWidth = 866
    ExplicitHeight = 707
    object Splitter2: TSplitter [0]
      Left = 0
      Top = 509
      Width = 866
      Height = 4
      Cursor = crVSplit
      Align = alBottom
      Beveled = True
      ExplicitTop = 388
      ExplicitWidth = 641
    end
    inherited plTop: TPanel
      Width = 866
      ParentFont = False
      ExplicitWidth = 866
      inherited btTool: TPngSpeedButton
        Left = 634
        ExplicitLeft = 265
      end
      inherited plCount: TPanel
        Left = 672
        ExplicitLeft = 672
        inherited edCount: TDBEditEh
          ControlLabel.ExplicitLeft = 16
          ControlLabel.ExplicitTop = 8
          ControlLabel.ExplicitWidth = 103
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 866
      Height = 480
      Border.Color = clSilver
      TitleParams.RowLines = 3
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'end_doc_code'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1055#1086#1088#1086#1078#1076#1072#1077#1084#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
          Width = 133
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'method_name'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1055#1088#1086#1094#1077#1076#1091#1088#1072' '#1090#1088#1072#1085#1089#1092#1086#1088#1084#1072#1094#1080#1080
          Width = 126
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isrestricted'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1055#1088#1103#1084#1086#1081' '#1087#1077#1088#1077#1093#1086#1076' '#1079#1072#1087#1088#1077#1097#1077#1085
          Width = 120
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'exclude_states_ids'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1048#1089#1082#1083#1102#1095#1072#1077#1084#1099#1077' ID '#1089#1086#1089#1090#1086#1103#1085#1080#1081
          Width = 114
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'pref_states_ids'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1055#1088#1077#1076#1087#1086#1095#1080#1090#1072#1077#1084#1099#1077' ID '#1089#1086#1089#1090#1086#1103#1085#1080#1081
          Width = 126
        end
        item
          Alignment = taCenter
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'copy_prev_state'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1087#1086#1089#1083#1077#1076#1085#1077#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 110
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'create_new_object'
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1057#1086#1079#1076#1072#1074#1072#1090#1100' '#1085#1086#1074#1099#1081' '#1086#1073#1098#1077#1082#1090
          Width = 80
        end>
    end
    inherited plHint: TPanel
      TabOrder = 3
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object plObjects: TPanel
      Left = 0
      Top = 513
      Width = 866
      Height = 194
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 866
        Height = 26
        Align = alTop
        BevelOuter = bvLowered
        Caption = #1057#1054#1057#1058#1054#1071#1053#1048#1071' '#1057#1042#1071#1047#1040#1053#1053#1067#1061' '#1054#1041#1066#1045#1050#1058#1054#1042
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object sbAddObject: TPngSpeedButton
          AlignWithMargins = True
          Left = 2
          Top = 2
          Width = 28
          Height = 20
          Hint = #1044#1086#1073#1072#1074#1080#1090#1100
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbAddObjectClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002BE4944415478DAA5935D4893511CC61FA74E97390D6B9B
            1F516C2E334C2535295A8A792549908124A120D145A6303023233FA2444A08F5
            C6ECA2B430B01681994E4D29511321B134F3634A7EA382B9ED7DA7BEE7BC6FAF
            1B2E28BDA9FFB9389C8BE777FEFF739EC7451004FC4FB9FC09286BBBAEA13C4D
            2394A6134A5484A7E0285910CFB5E25E579EFACCB423A0AC2D378550BE72BF42
            E31FA28A80CC7D3736F80D2C5AE7D033DE8EC5E5A57942487675FA4BC35F0087
            98D64407C77969154761328F62C1360B8EE7E023F581421688AE512386C68719
            B1B38CE7971B0C4EC083D65CB5D87667A4FA648056198E0FF32D6019169C40E0
            E9E901415CEBFC3A42E561E81E69C3C8A4698E2344F73AAB75C20E2835EA0B7C
            7DF615C78724A377A913D60D060CC3202FB6C8DE5DC9E77CD85833246E6E8855
            E950DBF1088C852B6CC8E9B86307DC7D97331675382E785DB201D3F298383707
            8BD58C9284723B20CB7009DEBEEE203CC1216504D6AD0C1A7BDBC69BF59D5A3B
            A0B0E12A1B1F992C1BB10C61C5BA829BC7EF6DFB657A631AE4325F84F945A3AA
            E9B1ED7D6ECF2E3BE0D69B2BEC89F044D9B0791036BA8682A8FB3B02BCA57284
            2B6250F1B6CAF6F1469F039067C81CD31C0C0DE63D04CC58A6C0726B6016CDA8
            BE50EF146E96C44502ADDF11B06616AF7A1AC7BBF3FB1D23E8EBD30BD6246CF1
            D963A9E89A69C11AE520E55D507AE68913C0530152891BE2354978D85881558B
            B5B0EFF657C7235E7B91A6E628ED542AF606C4A84FA3EB47B3FDC1C4AF854037
            1102DC25AE885327C138D0884FDFBFCC899ED1F5177D9B701A29F3E979D148A4
            2650A5F4D28524627A7512B33F2721F03C82F6A811243F80A68106F40E0F32A2
            BD3344F16F236DD5C5EAA414D1EF95D495F8C7859E8252EEBF7939A657A660EC
            6F87D9CACE8BE2EC2DF1B6613A5799A01121699C334C440C13B58789F2A4AEBF
            6878E730FD4BFD027D1196F03509C5820000000049454E44AE426082}
          ExplicitLeft = 1
          ExplicitTop = -2
          ExplicitHeight = 22
        end
        object sbDeleteObject: TPngSpeedButton
          AlignWithMargins = True
          Left = 34
          Top = 4
          Width = 32
          Height = 18
          Hint = #1059#1076#1072#1083#1080#1090#1100
          Align = alLeft
          Flat = True
          OnClick = sbDeleteObjectClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002554944415478DA95924D68D36018C79F7C746D92A6B5DD
            86B2B56C7810A141F0E8459C7810BC09A2CECFA907F13211C7BC7A134410069E
            046FC26EE2556D657EEC24F5D04C2F62B77569BA43ED47DA244BF2C6E74D6CB1
            743BEC8524BCE4FDFD9E27FF278CEFFBB09F65CFDD11A3AF5E767B7B6640B0F0
            680AEF7178FA44DD15BE793BE7BBCE19E2B84571F9F5CAA080C21C57A67BDFB6
            15F6F9B301897DE3560EDF9798E808ECD4B69751F4587EFBE64728F80743340A
            54E7B7DB402C4BE15F2C0512EBFA5C8E61D9129790814F26C1DEAC80A5EBF789
            E32C8582878B319F900E0802CBC83290560B48A3811253C17681C22CC2914412
            ACAD0AD855DD435848E5DF39FD4F20F30F62BEE7198C20702C1EF49A0D70FFD4
            C1470187D248320156658B56F6B0FD782AFFDE1A0AD1BD7B2F465CCF604591E3
            5307003C82667A795899C2B5102E7CB0769F4218164ADC8E3035CDC2C63A4A3C
            80CC24348ADF09711D29FD316FFD7F7E4860CE5EC3C098526C3203502E871DA0
            A0B9B6064EDB50C6BE7E52F7149897AFE600615E94303019613F689F4AAC5A0D
            4C5D879D765B3958FCA60E09BA97AE04954338016655A3104D9B89A5D2AC3471
            08BA551DBA9A86124399F8A9AA7D41F7E26C30675E14118E83A9E9606D53D88D
            6368804F43184D737236031DAD0A1D0C947692F9FD4B0D04C6F90B336C84CF47
            24095CA30326C238BEF8E897952030FDD8711CB16B48E3E31C3D63D089D4EBA7
            B31BE542FF139A67CFA12492775A4D8215250C6B206DEDC8D1E03F199165AE07
            0F85583F796A06E1D5B1D5CF03706F55A60F53C989ECE67A61CF31EE77FD05EB
            B7706A5FA737EA0000000049454E44AE426082}
          ExplicitLeft = 42
          ExplicitTop = 8
        end
        object sbEditObject: TPngSpeedButton
          AlignWithMargins = True
          Left = 70
          Top = 2
          Width = 26
          Height = 20
          Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbEditObjectClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
            62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
            08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
            4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
            C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
            892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
            4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
            0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
            C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
            8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
            346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
            5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
            3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
            378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
            C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
            1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
            FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
            F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
            0049454E44AE426082}
          ExplicitLeft = 78
          ExplicitTop = 6
        end
      end
      object dgObjects: TDBGridEh
        Left = 0
        Top = 26
        Width = 866
        Height = 168
        Align = alClient
        Border.Color = clSilver
        Ctl3D = False
        DataSource = dsObjects
        DynProps = <>
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        GridLineParams.DataHorzColor = 14540253
        GridLineParams.DataVertColor = 14540253
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        SelectionDrawParams.DrawFocusFrame = True
        SelectionDrawParams.DrawFocusFrameStored = True
        TabOrder = 1
        TitleParams.FillStyle = cfstThemedEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -11
        TitleParams.Font.Name = 'Verdana'
        TitleParams.Font.Style = [fsBold]
        TitleParams.MultiTitle = True
        TitleParams.ParentFont = False
        OnDblClick = sbEditObjectClick
        OnKeyDown = dgObjectsKeyDown
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'objectype_code'
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1086#1073#1100#1077#1082#1090#1072
            Width = 116
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'start_object_state'
            Footers = <>
            Title.Caption = #1048#1089#1093#1086#1076#1085#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 155
          end
          item
            AutoFitColWidth = False
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'start_real_state'
            Footers = <>
            Title.Caption = #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1080#1089#1093#1086#1076#1085#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 151
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'start_link_type_code'
            Footers = <>
            Title.Caption = #1048#1089#1093#1086#1076#1085#1086#1077' '#1085#1072#1083#1080#1095#1080#1077' '#1089#1074#1103#1079#1080
            Width = 146
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'end_object_state'
            Footers = <>
            Title.Caption = #1050#1086#1085#1077#1095#1085#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
            Width = 147
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'end_link_type_code'
            Footers = <>
            Title.Caption = #1050#1086#1085#1077#1095#1085#1086#1077' '#1085#1072#1083#1080#1095#1080#1077' '#1089#1074#1103#1079#1080
            Width = 147
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object plLeft: TPanel [4]
    Left = 0
    Top = 0
    Width = 185
    Height = 707
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 2
    object dgDocTypes: TDBGridEh
      Left = 0
      Top = 0
      Width = 185
      Height = 707
      Align = alClient
      AutoFitColWidths = True
      Border.Color = clSilver
      Border.ExtendedDraw = True
      ColumnDefValues.Title.TitleButton = True
      Ctl3D = False
      DataSource = dsDeliveryTypes
      DynProps = <>
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = []
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      SearchPanel.Enabled = True
      SearchPanel.PersistentShowing = False
      SelectionDrawParams.SelectionStyle = gsdsGridThemedEh
      SelectionDrawParams.DrawFocusFrame = True
      SelectionDrawParams.DrawFocusFrameStored = True
      SortLocal = True
      STFilter.Color = 16513004
      STFilter.InstantApply = True
      STFilter.Local = True
      STFilter.Visible = True
      TabOrder = 0
      TitleParams.FillStyle = cfstThemedEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -12
      TitleParams.Font.Name = 'Verdana'
      TitleParams.Font.Style = [fsBold]
      TitleParams.ParentFont = False
      TitleParams.VTitleMargin = 9
      OnActiveGroupingStructChanged = dgDataActiveGroupingStructChanged
      OnColumnMoved = dgDataColumnMoved
      OnColWidthsChanged = dgDataColWidthsChanged
      OnKeyDown = dgDocTypesKeyDown
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1089#1090#1072#1074#1082#1080
          Width = 196
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object Panel2: TPanel [5]
    Left = 1062
    Top = 0
    Width = 26
    Height = 707
    Align = alClient
    BevelOuter = bvNone
    Caption = 'plAll'
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 26
      Height = 29
      Align = alTop
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object sbDeleteServiceLink: TPngSpeedButton
        AlignWithMargins = True
        Left = 33
        Top = 1
        Width = 32
        Height = 28
        Margins.Top = 1
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        OnClick = sbDeleteServiceLinkClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000002554944415478DA95924D68D36018C79F7C746D92A6B5DD
          86B2B56C7810A141F0E8459C7810BC09A2CECFA907F13211C7BC7A134410069E
          046FC26EE2556D657EEC24F5D04C2F62B77569BA43ED47DA244BF2C6E74D6CB1
          743BEC8524BCE4FDFD9E27FF278CEFFBB09F65CFDD11A3AF5E767B7B6640B0F0
          680AEF7178FA44DD15BE793BE7BBCE19E2B84571F9F5CAA080C21C57A67BDFB6
          15F6F9B301897DE3560EDF9798E808ECD4B69751F4587EFBE64728F80743340A
          54E7B7DB402C4BE15F2C0512EBFA5C8E61D9129790814F26C1DEAC80A5EBF789
          E32C8582878B319F900E0802CBC83290560B48A3811253C17681C22CC2914412
          ACAD0AD855DD435848E5DF39FD4F20F30F62BEE7198C20702C1EF49A0D70FFD4
          C1470187D248320156658B56F6B0FD782AFFDE1A0AD1BD7B2F465CCF604591E3
          5307003C82667A795899C2B5102E7CB0769F4218164ADC8E3035CDC2C63A4A3C
          80CC24348ADF09711D29FD316FFD7F7E4860CE5EC3C098526C3203502E871DA0
          A0B9B6064EDB50C6BE7E52F7149897AFE600615E94303019613F689F4AAC5A0D
          4C5D879D765B3958FCA60E09BA97AE04954338016655A3104D9B89A5D2AC3471
          08BA551DBA9A86124399F8A9AA7D41F7E26C30675E14118E83A9E9606D53D88D
          6368804F43184D737236031DAD0A1D0C947692F9FD4B0D04C6F90B336C84CF47
          24095CA30326C238BEF8E897952030FDD8711CB16B48E3E31C3D63D089D4EBA7
          B31BE542FF139A67CFA12492775A4D8215250C6B206DEDC8D1E03F199165AE07
          0F85583F796A06E1D5B1D5CF03706F55A60F53C989ECE67A61CF31EE77FD05EB
          B7706A5FA737EA0000000049454E44AE426082}
        ExplicitLeft = 76
        ExplicitTop = 4
        ExplicitHeight = 41
      end
      object sbExcelServiceLink: TPngSpeedButton
        AlignWithMargins = True
        Left = 111
        Top = 1
        Width = 44
        Height = 28
        Hint = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1074' Excel'
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C0000031E4944415478DA8D936B6C0C5114C7FF7776B7BB53CF6E
          29F508A11E4D89E8AA846AB32869D89520A1691B1111AFB6D2342A21AA443C2A
          F1F8209E1104ED1791F824E852DD967835969616F5A6655955DD99E9ECEC5C67
          6737F5D5C9993B7327F7FCCEB9FF7B2EE39C63C1CEFAD5E2406B81C0E00060A3
          8701B191D3C07568BADEDEF5937B58F7AF72EF09978A98B10860D1BE46EFC512
          47AA20B0C11C3081A3CFB8F1CDE1EF09A1FA71173A3FF64A2F9A5BED0D275CBD
          7D0057D5BDEF359B33EC2DDF61121833325352842399350E39A4C139A11F6ADB
          653C68F5A3E3A32ABF6A6B4BB87B6C71AF01701FBC2F5D2E76886F02268AE546
          013A0D615D874A00450D237B820D2D7E0E8DA0B77C9DB8ED6DAFF1EC73E6B3BC
          43F3DFEADC3E7670BCCEC2B4DFF4143702C11FB8E3BB6080AC96FE04D2B0383D
          1F85D9EB696E86A4A8283CF244BBB93BCB12015C59B7B06CB96CEDC0FB402B9A
          9EBC27C1240C4D48841AD620493274D8B16CF67E30533C74AA2A6B9C152B0E3F
          566E54648A118035A44F09962F596A7AADD6A323E0475B7307C24C8699C5C16C
          4A42C1BC43548D19210A26C78C5166AC3C42801DB3C4E8295479E5C949E76C05
          D96BF1E8E73574FD09C3E77B0A5B9C1DC5EEF3246A1C42B44E0B933EF44E1B2E
          208F2AB8591103382B77A98A7ADE32313915A5AE4A5CFF7411BA64C5CBD6E7C8
          759460DAF845241E10D2A202A724C200782A09E0281FB18E529CCA4C73222005
          C0551D65EE5DB8D4761C66251E9FDEB5E375E74B9C2EF94027C3490F20350970
          EF7DA8D4ED996D003ED3BF91916689F898A43424278C465E663EEE7EF1E06BD7
          37A87E091B5C97E934B8D198D39205E4EC68501AAAB2A35B985BD128576F71D8
          FC3D16A3FB7E07BFE16AE31E04955F604CC0003111AB728EF67567CA5060EE56
          AFF2E0A8330AC8DA562FD794CFB405640BFEC7C627524C699DD27432270A9855
          5617ACD93E53ECEEB5B2FF014C1AC6F48C8DB5D2B3330B071880199B3C6FAF1F
          98332418E2FD682E44B4D0638B0789E6BE4B255A04DA1274314EF83375CD8D2F
          2DE772D30C4046516D9116E24B1963D349A6F87F57D1F0D8ED88CEC914EAC626
          01A8F69DCD3DFB17534092F0545E8BC50000000049454E44AE426082}
        ExplicitLeft = 113
        ExplicitTop = 2
        ExplicitHeight = 37
      end
      object sbAddServiceLink: TPngSpeedButton
        AlignWithMargins = True
        Left = 1
        Top = 1
        Width = 28
        Height = 28
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        OnClick = sbAddServiceLinkClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000002BE4944415478DAA5935D4893511CC61FA74E97390D6B9B
          1F516C2E334C2535295A8A792549908124A120D145A6303023233FA2444A08F5
          C6ECA2B430B01681994E4D29511321B134F3634A7EA382B9ED7DA7BEE7BC6FAF
          1B2E28BDA9FFB9389C8BE777FEFF739EC7451004FC4FB9FC09286BBBAEA13C4D
          2394A6134A5484A7E0285910CFB5E25E579EFACCB423A0AC2D378550BE72BF42
          E31FA28A80CC7D3736F80D2C5AE7D033DE8EC5E5A57942487675FA4BC35F0087
          98D64407C77969154761328F62C1360B8EE7E023F581421688AE512386C68719
          B1B38CE7971B0C4EC083D65CB5D87667A4FA648056198E0FF32D6019169C40E0
          E9E901415CEBFC3A42E561E81E69C3C8A4698E2344F73AAB75C20E2835EA0B7C
          7DF615C78724A377A913D60D060CC3202FB6C8DE5DC9E77CD85833246E6E8855
          E950DBF1088C852B6CC8E9B86307DC7D97331675382E785DB201D3F298383707
          8BD58C9284723B20CB7009DEBEEE203CC1216504D6AD0C1A7BDBC69BF59D5A3B
          A0B0E12A1B1F992C1BB10C61C5BA829BC7EF6DFB657A631AE4325F84F945A3AA
          E9B1ED7D6ECF2E3BE0D69B2BEC89F044D9B0791036BA8682A8FB3B02BCA57284
          2B6250F1B6CAF6F1469F039067C81CD31C0C0DE63D04CC58A6C0726B6016CDA8
          BE50EF146E96C44502ADDF11B06616AF7A1AC7BBF3FB1D23E8EBD30BD6246CF1
          D963A9E89A69C11AE520E55D507AE68913C0530152891BE2354978D85881558B
          B5B0EFF657C7235E7B91A6E628ED542AF606C4A84FA3EB47B3FDC1C4AF854037
          1102DC25AE885327C138D0884FDFBFCC899ED1F5177D9B701A29F3E979D148A4
          2650A5F4D28524627A7512B33F2721F03C82F6A811243F80A68106F40E0F32A2
          BD3344F16F236DD5C5EAA414D1EF95D495F8C7859E8252EEBF7939A657A660EC
          6F87D9CACE8BE2EC2DF1B6613A5799A01121699C334C440C13B58789F2A4AEBF
          6878E730FD4BFD027D1196F03509C5820000000049454E44AE426082}
        ExplicitLeft = -1
        ExplicitTop = -2
        ExplicitHeight = 39
      end
      object sbEditServiceLink: TPngSpeedButton
        AlignWithMargins = True
        Left = 69
        Top = 1
        Width = 26
        Height = 28
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        OnClick = sbEditServiceLinkClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C0000022B4944415478DA8D93DF6BD35014C7BF49BAD6FD08C546
          62655DB54E0A42654C9D4350984F7D50863E08BEF8E34918F64518F837F4C11F
          08431FEA83825041C640E8834CA46AD7FA635004D9A20C45C9666B1DAD6DB324
          4D52EF2D8D1869370F5C3884F3F97072EF394CB3D904C33001003BC9E1B07914
          C9F94A18CBFEC0B4058754557DC6711C4F72B6136918061289C4ED582C76F36F
          892D38D26834B29AA6B1247740966541D775088280EFD21C6AF9C7E0060EA05E
          4A8351E58BB49A9E3152FC9A48C0B26C0BB205A669B604BD7A06B58F39F4EE38
          0E6F6814E5CF592C3F4D141C025A6C0795D0EEA844594BC16D4AE8E747B1BE22
          C12B0CC3CD8B587AFE407508DABFD312D09C0AD51F2F60555EC2BB370A6D2D89
          8D9F0C8A920255D12AF57A75DC21F8F7E294421AC6FA3CBCFB26A1CA77C1BA0D
          346ABB517EB78478F2FDF999D4CAC3AE82C2F213B01B398891D304BE03B6C780
          5E0DA1B49087FF641CFD83917152F6B6A3A020A5F04B7E85F0B153D00BF7C170
          3AD44A00A54C1EE29919F40941B85CAECE824FD947B0AA1F603022045F06DB45
          378187505E94E09FBC018EDF45E1EE82D9EB519C9DBA0729398DD52F0BF00447
          C02A4D44CEDD428F6F4FABC34D05F12B07317DE932C09AC8CFCF415E9531716D
          1603FEF09FFBA1B3D25570211AC04850C0D8FE10F8E030C227AEA2CF37E8781D
          FAD49D04B976FE3F6111C151322B6FEC5D384CF6204D96691BED702B980C98E2
          F1782608BB680B86DAEBBC156C079DF92261BFFD0669742B219F76125F000000
          0049454E44AE426082}
        ExplicitLeft = 60
        ExplicitTop = 0
        ExplicitHeight = 43
      end
      object Bevel1: TBevel
        AlignWithMargins = True
        Left = 106
        Top = 0
        Width = 1
        Height = 29
        Margins.Left = 10
        Margins.Top = 0
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 107
        ExplicitTop = 1
        ExplicitHeight = 41
      end
      object PngSpeedButton6: TPngSpeedButton
        AlignWithMargins = True
        Left = -26
        Top = 3
        Width = 49
        Height = 23
        Hint = #1048#1079#1084#1077#1085#1080#1090#1100' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
        Align = alRight
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Visible = False
        OnClick = btToolClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
          F40000078A4944415478DAB557095055E7153E8FF778205B02A804D16119590C
          5821A84052D249C45481D6084229CA12251190C8220A3C36051E9B8808012251
          C412A4FA22150A365894A48234828A1A944D065183D21A6495F5D1EF7F131825
          880F939E993B73EFF9B7EF9EFB9DEF9CCBA1FF9355555519AAA8A87CB470E142
          8F9B376F76AC5DBBD606EEBEE9F3389337090909AA616161DDBFF460272727AE
          4020085353530BD2D0D050EDEBEBA36DDBB6894A4A4A3EC1F09319016091E98E
          1D3B8ACACACA2AF6EFDFBF1BAE9E570570F8F0E1740707874F65656569626282
          8442E1D5949414370CDD62E3DEDEDE96E7CE9D6B83754D01D0D6D6364A4E4E3E
          BF64C992453D3D3DB5A1A1A19F5CBF7EBD7EAE875B5A5ABE75E2C4893A252525
          0E9FCFA7F2F2F26E373737AF91919122368E7DAD109D7FECDEBDFB73000D7DEE
          13E8EBEBDB1C3F7EFC2B2B2BABD76EDCB8D1B862C50A6BB8FF3B1700AEAEAE1F
          676565E50C0E0E9282820221A2ED9B376F7E07433F989B9B1B01DCD7060606DA
          E9E9E9D5FEFEFE6BE17FCA79760323232377842C05215CE0EBEBFB597676F6A7
          7301E0ECECEC8B3599C3C3C3242323432C0A67CF9EBDD0DDDD7DC1DEDEDE5347
          4767299B979898580BBE7DC038C199619F0DE04324109A878787478B44A21869
          01804BDE999999D9A3A3A3C4E170240094959589C7E34900311B1B1B234F4FCF
          A2828202173C8E725EB097E5962D5BE201E0BDD8D858214217210D0090AD70EB
          D6AD2EEC13306344949393935CF2F2F2125F6D6D2D21C27EF7EFDFCF7C8E0333
          983EBE5F0640FC3E2929E910F81130DBE188584C484848E4C3870F0924945C5C
          2E77EAED19A8D6D6568A8F8F3F0B72FA6049C7CB00305B0CB49F4547476FC8C8
          C8C83E72E4C84E16C5E993FCFCFC0498236C6F6FA7E6E66632333323757575C2
          5BD2952B5706AE5DBBD6067F4B7D7D7DCDE3C78FCF6049EBE4DA970160A6E9E8
          E898191111B131272727172463E84726074156414C4C8CB0B3B39390DBA4A7A7
          471020AAAEAEA6B4B4B4B29A9A9AE3484396D29DB8FAA76F2E0D000908160930
          D7213F3FFF24D2C803BE61888A202E2E4E0896534747074147485555952A2B2B
          69DFBE7DC71A1A1AA230EFFE6C1B4B0B603212194141418EE043A1582CBE06F1
          4A66DF96BD3D349F141515092A4720EEB1DBB76F87FFF4D6B3DA5C00305B6063
          6393B86BD7AEAD8C5C86868684F092A29232F1C1F892926231089BD7D8D81821
          CDE1AF02801005014817D7DBDBCB6139FD96B905C9F126E8A44834909A967EEC
          DEDDF6584CEB9276BF390140C5F4F2F0F0F81CE2C24589A5B6F607A4AFDA4FDA
          EA43F4D5E93337FC92CA6D31EDC15CF6941A407070B01514ECBCB1B1F13CF6FC
          A477805A6A0A4857E13B9A6F7D94C49D9746F48DDF59D9D64D377F75007BF6EC
          59BD69D3A6BFAD5AB56AD1A4AFEFA9981E5FF4251DB52334A2954C7C1955121D
          F02AF2CF166FEFEC97BE88BD14C0FAF5EB5782D5C5A8665387B362F3972F45F4
          7D797483FF8636038DD74856F137EE441ABFA5A693B1D7FD84F7FC2A9AA9EA17
          03404E2F3F75EA540998AF33E963AC476D607C380D758B539425931867F20B0A
          D4B320D31C22AE989EFE2B70203CAE61CFC17F52D62B03B0B5B5B580CA9DB0B3
          B3D39BF4B12A070D20A45A11743D847E925464A0D9F9702AB1767873312DCB20
          E2F189BE0FA650C177E1497FA7786900C842CB9D50B1CC50B9D4A0685A08B9B5
          AEAEAEC2E4C4A1A121CACBCB636F5E08D513C0D5FEEC46CBDE9071AB8C17E76A
          98BFC9A365A9D891473F967A3CFDC0EB81DD954754391B002ED42D372020C09D
          29192BA1AC863F6B4CED72737309FD223B9CB5521D33EC25E364C94D2D0C1DF7
          E76A69810F1F12A99B524F6DFEC0C5F375C5074E0E667DD34CD53F038016694D
          71717185A6A6A6A47CB2DA0D99959450068659696929A14911E1F0E0171C3E69
          AFFFF96DDE81F03F8DB91AEB933C59E173BC0E696871A3813BB7867C127B77E6
          7F2BFEE23900681A85070F1E14B00EA6ABAB8BA070B47CF9724937C30031430B
          F50485C80EB7975E462A98DC524D9EF7A5F8B1D405666632B40800461FA192FC
          817AAE7E3162EF52EA58D54AA55300A2A2A20A90E7AE4D4D4D84B27AE7F2E5CB
          456844DEC5BDC5BC7912CD21F0E32E9E7F87DBBB520060269FF2915CF92E97E1
          7749499568D5517062A564F9ADD3819D6B3CEBD63CECA7DB12009191915FFAF8
          F86CDEBB77EF8FA8F73BE0FB2B2ADB1F2B2A2A8A592498050606DE416D7F0FB7
          F7A404408BD5B81B2F26CA88740C894BBACE44F3D72336FA38F12E152704FCFB
          C3F01FD6615A0F07EAE6B56EDDBAB443870E2520FC49708E2102D978F6665D0D
          EB7020C1E7D05838D20C0DC52C26B35A9F1F72D49FF69A988CF069BE01D1427B
          D4534FC8E8D714BB33E658545EFF7696052CD5D83FC0555CFFB1B0B0B045EB55
          6C6262C26B696961B5BD0BC2B37D7C7CFCCC1C0E9F322D3559F78CEDDCC48DEF
          0F6992BA06915E20918A36F57D1341A62E776CA60B9132427D153DFCD2BABA3A
          263A4D682085C88A429AA11794D6D03ABC1DE1AC981AE63C64C15FA442A4A649
          8FEA3BC9745BAFE37400F2484B7FFCD3E920231EE137AD0CBE3A5C13AF7AF8B3
          C1B05DAD101EED32EEAEA830AE202CE45F28FC76F0E3174931FB8B10FF0A87FE
          2C18486C6B68CC1BA368E9F0DCF83FF51F156BC11A83740000000049454E44AE
          426082}
        ExplicitLeft = 414
        ExplicitTop = 4
        ExplicitHeight = 41
      end
    end
    object dgComplete: TDBGridEh
      Left = 0
      Top = 29
      Width = 26
      Height = 678
      Align = alClient
      Border.Color = clSilver
      Ctl3D = False
      DataSource = dsComplete
      DynProps = <>
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = []
      GridLineParams.DataHorzColor = 14540253
      GridLineParams.DataVertColor = 14540253
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      SelectionDrawParams.DrawFocusFrame = True
      SelectionDrawParams.DrawFocusFrameStored = True
      TabOrder = 1
      TitleParams.FillStyle = cfstThemedEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -11
      TitleParams.Font.Name = 'Verdana'
      TitleParams.Font.Style = [fsBold]
      TitleParams.MultiTitle = True
      TitleParams.ParentFont = False
      OnDblClick = sbEditObjectClick
      OnKeyDown = dgObjectsKeyDown
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'objectype_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1086#1073#1100#1077#1082#1090#1072
          Width = 131
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'object_state'
          Footers = <>
          Title.Caption = #1050#1086#1085#1077#1095#1085#1086#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 168
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'link_type_code'
          Footers = <>
          Title.Caption = #1050#1086#1085#1077#1095#1085#1072#1103' '#1089#1074#1103#1079#1100
          Width = 162
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select r.*,'
      
        '(select code from deliverytypes t0 where t0.id = r.dlv_type_id) ' +
        'as deliverytype_code,'
      
        '(select code from doctypes t1 where t1.id = r.end_doctype_id) as' +
        ' end_doc_code'
      'from docroutes r')
    UpdateCommand.CommandText.Strings = (
      'update docroutes'
      'set'
      '  end_doctype_id = :end_doctype_id,'
      '  method_name = :method_name,'
      '  isrestricted = :isrestricted, '
      '  exclude_states_ids = :exclude_states_ids,'
      '  pref_states_ids = :pref_states_ids, '
      '  copy_prev_state = :copy_prev_state,'
      '  create_new_object = :create_new_object'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'end_doctype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'method_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'isrestricted'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'exclude_states_ids'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'pref_states_ids'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'copy_prev_state'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'create_new_object'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into docroutes'
      
        '  (dlv_type_id, end_doctype_id, method_name, isrestricted, exclu' +
        'de_states_ids, pref_states_ids, copy_prev_state, create_new_obje' +
        'ct)'
      'values'
      
        '  (:dlv_type_id, :end_doctype_id, :method_name, :isrestricted, :' +
        'exclude_states_ids, :pref_states_ids, :copy_prev_state, :create_' +
        'new_object)')
    InsertCommand.Parameters = <
      item
        Name = 'dlv_type_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'end_doctype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'method_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'isrestricted'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'exclude_states_ids'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'pref_states_ids'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'copy_prev_state'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'create_new_object'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from docroutes where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select r.*,'
      
        '(select code from deliverytypes t0 where t0.id = r.dlv_type_id) ' +
        'as deliverytype_code,'
      
        '(select code from doctypes t1 where t1.id = r.end_doctype_id) as' +
        ' end_doc_code'
      'from docroutes r where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 384
    Top = 280
  end
  inherited meData: TMemTableEh
    Left = 432
    Top = 280
  end
  inherited dsData: TDataSource
    Left = 488
    Top = 280
  end
  object drvDeliveryTypes: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select * from deliverytypes ')
    SelectCommand.Parameters = <>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.CommandText.Strings = (
      'select * from deliverytypes where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 48
    Top = 224
  end
  object meDeliveryTypes: TMemTableEh
    Params = <>
    DataDriver = drvDeliveryTypes
    AfterOpen = meDeliveryTypesAfterScroll
    BeforePost = meDataBeforePost
    AfterScroll = meDeliveryTypesAfterScroll
    Left = 96
    Top = 224
  end
  object dsDeliveryTypes: TDataSource
    DataSet = meDeliveryTypes
    Left = 152
    Top = 224
  end
  object drvObjects: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select d.*,'
      
        '(select name from objecttypes t1 where t1.id = d.objecttype_id) ' +
        'as objectype_code,'
      
        '(select code from objectstatekinds t2 where t2.id = d.start_obje' +
        'ct_state_id) as start_object_state,'
      
        '(select code from objectstatekinds t21 where t21.id = d.start_re' +
        'al_state_id) as start_real_state,'
      
        '(select code from objectstatekinds t3 where t3.id = d.end_object' +
        '_state_id) as end_object_state,'
      
        '(case when start_link_type = 1 then '#39#1057#1086#1077#1076#1080#1085#1077#1085#1080#1077#39' when start_link' +
        '_type = 0 then '#39#1056#1072#1079#1100#1077#1076#1080#1085#1077#1085#1080#1077#39' else '#39#39' end) as start_link_type_co' +
        'de,'
      
        '(case when end_link_type = 1 then '#39#1057#1086#1077#1076#1080#1085#1077#1085#1080#1077#39' when end_link_typ' +
        'e = 0 then '#39#1056#1072#1079#1100#1077#1076#1080#1085#1077#1085#1080#1077#39' else '#39#39' end) as end_link_type_code'
      'from docroute2objectstates d')
    SelectCommand.Parameters = <>
    UpdateCommand.CommandText.Strings = (
      'update docroute2objectstates'
      'set'
      '  objecttype_id = :objecttype_id,'
      '  start_object_state_id = :start_object_state_id,'
      '  start_real_state_id = :start_real_state_id,'
      '  end_object_state_id = :end_object_state_id,'
      '  start_link_type = :start_link_type,'
      '  end_link_type = :end_link_type, '
      ' fesco_sign  = :fesco_sign,'
      ' default_isempty = :default_isempty, '
      ' default_seal_number = :default_seal_number'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'objecttype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'start_object_state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'start_real_state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'end_object_state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'start_link_type'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'end_link_type'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'fesco_sign'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'default_isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'default_seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into docroute2objectstates'
      
        '  (docroute_id, objecttype_id, start_object_state_id, start_real' +
        '_state_id, end_object_state_id, start_link_type, end_link_type, ' +
        'fesco_sign, default_isempty, default_seal_number)'
      'values'
      
        '  (:docroute_id, :objecttype_id, :start_object_state_id, :start_' +
        'real_state_id, :end_object_state_id, :start_link_type, :end_link' +
        '_type, :fesco_sign, :default_isempty, :default_seal_number)')
    InsertCommand.Parameters = <
      item
        Name = 'docroute_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'objecttype_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'start_object_state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'start_real_state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'end_object_state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'start_link_type'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'end_link_type'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'fesco_sign'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'default_isempty'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'default_seal_number'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from docroute2objectstates where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select d.*,'
      
        '(select name from objecttypes t1 where t1.id = d.objecttype_id) ' +
        'as objectype_code,'
      
        '(select code from objectstatekinds t2 where t2.id = d.start_obje' +
        'ct_state_id) as start_object_state,'
      
        '(select code from objectstatekinds t21 where t21.id = d.start_re' +
        'al_state_id) as start_real_state,'
      
        '(select code from objectstatekinds t3 where t3.id = d.end_object' +
        '_state_id) as end_object_state,'
      
        '(case when start_link_type = 1 then '#39#1057#1086#1077#1076#1080#1085#1077#1085#1080#1077#39' when start_link' +
        '_type = 0 then '#39#1056#1072#1079#1100#1077#1076#1080#1085#1077#1085#1080#1077#39' else '#39#39' end) as start_link_type_co' +
        'de,'
      
        '(case when end_link_type = 1 then '#39#1057#1086#1077#1076#1080#1085#1077#1085#1080#1077#39' when end_link_typ' +
        'e = 0 then '#39#1056#1072#1079#1100#1077#1076#1080#1085#1077#1085#1080#1077#39' else '#39#39' end) as end_link_type_code'
      'from docroute2objectstates d where d.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 304
    Top = 504
  end
  object meObjects: TMemTableEh
    DetailFields = 'docroute_id'
    MasterFields = 'id'
    MasterSource = dsData
    Params = <>
    DataDriver = drvObjects
    BeforePost = meObjectsBeforePost
    Left = 352
    Top = 504
  end
  object dsObjects: TDataSource
    DataSet = meObjects
    Left = 408
    Top = 504
  end
  object drvComplete: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select d.*,'
      
        '(select name from objecttypes t1 where t1.id = d.objecttype_id) ' +
        'as objectype_code,'
      
        '(select code from objectstatekinds t2 where t2.id = d.object_sta' +
        'te_id) as object_state,'
      
        '(case when link_type = 1 then '#39#1057#1086#1077#1076#1080#1085#1077#1085#1080#1077#39' when link_type = 0 th' +
        'en '#39#1056#1072#1079#1100#1077#1076#1080#1085#1077#1085#1080#1077#39' else '#39#39' end) as link_type_code'
      'from deliverycomplete d order by id')
    SelectCommand.Parameters = <>
    UpdateCommand.CommandText.Strings = (
      'update deliverycomplete'
      
        'set dlv_type_id = :dlv_type_id, objecttype_id = :objecttype_id, ' +
        'object_state_id = :object_state_id, link_type = :link_type'
      'where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'dlv_type_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'objecttype_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'object_state_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'link_type'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into deliverycomplete'
      '('
      'dlv_type_id,'
      'objecttype_id,'
      'object_state_id,'
      'link_type'
      ')'
      'values'
      '('
      ':dlv_type_id,'
      ':objecttype_id,'
      ':object_state_id,'
      ':link_type'
      ')')
    InsertCommand.Parameters = <
      item
        Name = 'dlv_type_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'objecttype_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'object_state_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'link_type'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from docroute2services where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select d.*,'
      
        '(select name from objecttypes t1 where t1.id = d.objecttype_id) ' +
        'as objectype_code,'
      
        '(select code from objectstatekinds t2 where t2.id = d.object_sta' +
        'te_id) as object_state,'
      
        '(case when link_type = 1 then '#39#1057#1086#1077#1076#1080#1085#1077#1085#1080#1077#39' when link_type = 0 th' +
        'en '#39#1056#1072#1079#1100#1077#1076#1080#1085#1077#1085#1080#1077#39' else '#39#39' end) as link_type_code'
      'from deliverycomplete d where id =:current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 632
    Top = 264
  end
  object meComplete: TMemTableEh
    FetchAllOnOpen = True
    MasterFields = 'id'
    MasterSource = dsData
    Params = <>
    DataDriver = drvComplete
    AfterPost = meDataAfterPost
    Left = 712
    Top = 264
  end
  object dsComplete: TDataSource
    DataSet = meComplete
    Left = 768
    Top = 264
  end
end
