﻿inherited FormEditDocRoute: TFormEditDocRoute
  Caption = #1052#1072#1088#1096#1088#1091#1090' '#1090#1088#1072#1085#1089#1092#1086#1088#1084#1072#1094#1080#1080' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 245
  ClientWidth = 682
  ExplicitWidth = 688
  ExplicitHeight = 273
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 14
    Top = 15
    Width = 201
    Height = 16
    Caption = #1058#1080#1087' '#1089#1086#1079#1076#1072#1074#1072#1077#1084#1086#1075#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object Label3: TLabel [1]
    Left = 14
    Top = 70
    Width = 187
    Height = 16
    Caption = #1055#1088#1086#1094#1077#1076#1091#1088#1072' '#1090#1088#1072#1085#1089#1092#1086#1088#1084#1072#1094#1080#1080
  end
  object Label2: TLabel [2]
    Left = 14
    Top = 137
    Width = 189
    Height = 16
    Caption = #1048#1089#1082#1083#1102#1095#1072#1077#1084#1099#1077' ID '#1089#1086#1089#1090#1086#1103#1085#1080#1081
  end
  object Label4: TLabel [3]
    Left = 366
    Top = 137
    Width = 295
    Height = 16
    Caption = #1055#1088#1077#1076#1087#1086#1095#1080#1090#1072#1077#1084#1099#1077' ID '#1092#1080#1079#1080#1095#1077#1089#1082#1080#1093' '#1089#1086#1089#1090#1086#1103#1085#1080#1081
  end
  inherited plBottom: TPanel
    Top = 204
    Width = 682
    TabOrder = 7
    ExplicitTop = 204
    ExplicitWidth = 682
    inherited btnCancel: TButton
      Left = 566
      ExplicitLeft = 566
    end
    inherited btnOk: TButton
      Left = 447
      ExplicitLeft = 447
    end
  end
  object luCreateDocType: TDBLookupComboboxEh [5]
    Left = 14
    Top = 37
    Width = 326
    Height = 24
    DynProps = <>
    DataField = 'end_doctype_id'
    DataSource = dsLocal
    EditButtons = <>
    KeyField = 'id'
    ListField = 'code'
    ListSource = FormDocTypes.dsData
    TabOrder = 0
    Visible = True
  end
  object edMethod: TDBEditEh [6]
    Left = 14
    Top = 92
    Width = 326
    Height = 24
    DataField = 'method_name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object cbIsRestricted: TDBCheckBoxEh [7]
    Left = 366
    Top = 37
    Width = 209
    Height = 17
    Caption = #1055#1088#1103#1084#1086#1081' '#1087#1077#1088#1077#1093#1086#1076' '#1079#1072#1087#1088#1077#1097#1077#1085
    DataField = 'isrestricted'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 2
  end
  object edExcludeStatesIds: TDBEditEh [8]
    Left = 14
    Top = 159
    Width = 326
    Height = 24
    DataField = 'exclude_states_ids'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 5
    Visible = True
  end
  object edPrefIds: TDBEditEh [9]
    Left = 366
    Top = 159
    Width = 295
    Height = 24
    DataField = 'pref_states_ids'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 6
    Visible = True
  end
  object cbCopyPrevState: TDBCheckBoxEh [10]
    Left = 366
    Top = 66
    Width = 281
    Height = 18
    Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1087#1086#1089#1083#1077#1076#1085#1077#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
    DataField = 'copy_prev_state'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 3
  end
  object cbCreateNewObject: TDBCheckBoxEh [11]
    Left = 366
    Top = 99
    Width = 281
    Height = 17
    Caption = #1057#1086#1079#1076#1072#1074#1072#1090#1100' '#1085#1086#1074#1099#1081' '#1086#1073#1098#1077#1082#1090
    DataField = 'create_new_object'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 4
  end
  inherited dsLocal: TDataSource
    Left = 236
    Top = 92
  end
  inherited qrAux: TADOQuery
    Left = 300
    Top = 92
  end
end
