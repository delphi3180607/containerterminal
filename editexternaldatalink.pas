﻿unit editexternaldatalink;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEditExternalDataLink = class(TFormEdit)
    Label1: TLabel;
    cbDictType: TDBComboBoxEh;
    Label2: TLabel;
    edExtText: TDBEditEh;
    edDictValue: TDBEditEh;
    Label3: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditExternalDataLink: TFormEditExternalDataLink;

implementation

{$R *.dfm}

end.
