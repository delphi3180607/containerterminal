﻿object FormReportItem: TFormReportItem
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'FormReportItem'
  ClientHeight = 632
  ClientWidth = 846
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object dpReports: TDBGridEh
    Left = 0
    Top = 0
    Width = 846
    Height = 632
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    Border.Color = 14540253
    Border.ExtendedDraw = True
    Color = clWhite
    ColumnDefValues.ToolTips = True
    Ctl3D = False
    DataGrouping.Color = clWindow
    DataGrouping.Font.Charset = DEFAULT_CHARSET
    DataGrouping.Font.Color = clWindowText
    DataGrouping.Font.Height = -12
    DataGrouping.Font.Name = 'Arial'
    DataGrouping.Font.Style = [fsBold]
    DataGrouping.GroupRowDefValues.FooterInGroupRow = True
    DataGrouping.ParentColor = False
    DataGrouping.ParentFont = False
    DataSource = dsReport
    DrawMemoText = True
    DynProps = <>
    EditActions = [geaCutEh, geaCopyEh, geaPasteEh, geaDeleteEh, geaSelectAllEh]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    FooterParams.Color = clSilver
    FooterParams.FillStyle = cfstThemedEh
    FooterParams.HorzLineColor = clSilver
    FooterParams.VertLineColor = clSilver
    GridLineParams.BrightColor = clSilver
    GridLineParams.DarkColor = clSilver
    GridLineParams.DataBoundaryColor = clSilver
    GridLineParams.DataHorzColor = 14607074
    GridLineParams.DataVertColor = 14607074
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    ImeMode = imDisable
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    IndicatorParams.Color = clBtnFace
    IndicatorParams.FillStyle = cfstGradientEh
    IndicatorParams.HorzLineColor = 14540253
    IndicatorParams.VertLineColor = 12303291
    EmptyDataInfo.Text = #1053#1072#1078#1084#1080#1090#1077' Ctrl-Insert '#1076#1083#1103' '#1089#1086#1079#1076#1072#1085#1080#1103' '#1085#1086#1074#1086#1075#1086' '#1086#1090#1095#1077#1090#1072
    OddRowColor = 15791348
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove]
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ReadOnly = True
    SelectionDrawParams.SelectionStyle = gsdsGridThemedEh
    SelectionDrawParams.DrawFocusFrame = False
    SelectionDrawParams.DrawFocusFrameStored = True
    ShowHint = True
    SortLocal = True
    STFilter.Color = clWhite
    STFilter.Font.Charset = DEFAULT_CHARSET
    STFilter.Font.Color = clNavy
    STFilter.Font.Height = -13
    STFilter.Font.Name = 'Arial'
    STFilter.Font.Style = []
    STFilter.HorzLineColor = 14540253
    STFilter.Local = True
    STFilter.ParentFont = False
    STFilter.VertLineColor = 12303291
    STFilter.Visible = True
    TabOrder = 0
    TitleParams.Color = 15790320
    TitleParams.FillStyle = cfstThemedEh
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -12
    TitleParams.Font.Name = 'Arial'
    TitleParams.Font.Style = [fsBold]
    TitleParams.HorzLineColor = clSilver
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    TitleParams.SecondColor = clBtnFace
    TitleParams.SortMarkerStyle = smstDefaultEh
    TitleParams.VertLineColor = clSilver
    TreeViewParams.GlyphStyle = tvgsClassicEh
    Visible = False
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qrReport: TADOQuery
    Connection = dm.connMain
    Parameters = <>
    Left = 368
    Top = 183
  end
  object dsReport: TDataSource
    DataSet = meReport
    Left = 424
    Top = 183
  end
  object exReport: TEXLReport
    About = 'EMS Advanced Excel Report(tm) Component Suite for Delphi(R)'
    DataSet = meReport
    Dictionary = <>
    _Version = '1.9.0.2'
    Left = 502
    Top = 184
  end
  object meReport: TMemTableEh
    Params = <>
    Left = 384
    Top = 263
  end
  object qrAux: TADOQuery
    Connection = dm.connMain
    Parameters = <>
    Left = 448
    Top = 256
  end
end
