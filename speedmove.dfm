﻿inherited FormSpeedMove: TFormSpeedMove
  Caption = #1041#1099#1089#1090#1088#1086#1077' '#1088#1072#1079#1084#1077#1097#1077#1085#1080#1077' '#1074#1072#1075#1086#1085#1086#1074
  ClientHeight = 464
  ClientWidth = 814
  ExplicitWidth = 820
  ExplicitHeight = 492
  PixelsPerInch = 96
  TextHeight = 16
  object lbMark: TLabel [0]
    Left = 345
    Top = 92
    Width = 5
    Height = 16
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    StyleElements = [seClient, seBorder]
  end
  object lbEnter: TLabel [1]
    Left = 716
    Top = 38
    Width = 45
    Height = 18
    Caption = 'Enter!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object lbcars: TLabel [2]
    Left = 344
    Top = 122
    Width = 47
    Height = 14
    Caption = #1042#1072#1075#1086#1085#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object sbClear: TSpeedButton [3]
    Left = 544
    Top = 118
    Width = 73
    Height = 22
    Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1089#1077' '#1074#1099#1073#1088#1072#1085#1085#1099#1077' '#1074#1072#1075#1086#1085#1099
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    OnClick = sbClearClick
  end
  object sbDelete: TSpeedButton [4]
    Left = 316
    Top = 144
    Width = 23
    Height = 22
    Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1091#1102' '#1089#1090#1088#1086#1082#1091
    Caption = 'X'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    OnClick = sbDeleteClick
  end
  inherited plBottom: TPanel
    Top = 423
    Width = 814
    ExplicitTop = 423
    ExplicitWidth = 814
    inherited btnCancel: TButton
      Left = 698
      Visible = False
      ExplicitLeft = 698
    end
    inherited btnOk: TButton
      Left = 579
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Default = False
      ExplicitLeft = 579
    end
  end
  object edCarriageNum: TDBEditEh [6]
    Left = 344
    Top = 28
    Width = 358
    Height = 40
    ControlLabel.Width = 93
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1086#1084#1077#1088' '#1074#1072#1075#1086#1085#1072
    ControlLabel.Visible = True
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = True
    OnChange = edCarriageNumChange
    OnKeyPress = edCarriageNumKeyPress
  end
  object plButtons: TPanel [7]
    AlignWithMargins = True
    Left = 0
    Top = 0
    Width = 297
    Height = 423
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 2
    object plTop: TPanel
      Left = 0
      Top = 0
      Width = 297
      Height = 33
      Align = alTop
      BevelOuter = bvNone
      Color = 13368056
      ParentBackground = False
      TabOrder = 0
      StyleElements = [seFont, seBorder]
      object lbMessage: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 5
        Width = 291
        Height = 18
        Margins.Top = 5
        Align = alTop
        Alignment = taCenter
        Caption = #1042#1067#1041#1045#1056#1048#1058#1045' '#1055#1059#1058#1068
        Color = 14680063
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 10180900
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
        StyleElements = [seBorder]
        ExplicitWidth = 138
      end
    end
  end
  object btPlace: TButton [8]
    Left = 632
    Top = 144
    Width = 161
    Height = 267
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = btPlaceClick
  end
  object meCars: TListBox [9]
    Left = 345
    Top = 145
    Width = 272
    Height = 266
    TabOrder = 4
    OnEnter = meCarsEnter
    OnExit = meCarsExit
  end
  inherited dsLocal: TDataSource
    Left = 72
    Top = 104
  end
  inherited qrAux: TADOQuery
    Left = 16
    Top = 104
  end
  object meCach: TMemTableEh
    Params = <>
    Left = 232
    Top = 28
  end
end
