﻿unit adddriver;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormAddDriver = class(TFormEdit)
    edFieldName: TDBEditEh;
    Label2: TLabel;
    kind: TDBNumberEditEh;
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormAddDriver: TFormAddDriver;

implementation

{$R *.dfm}

procedure TFormAddDriver.FormActivate(Sender: TObject);
begin
  inherited;
  kind.Value := 1;
end;

end.
