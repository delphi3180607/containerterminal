﻿unit objectstatekinds;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, statekinds, DBGridEhGrouping,
  ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB,
  Data.Win.ADODB, MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus,
  Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, Vcl.StdCtrls, System.Actions, Vcl.ActnList, Grid, EXLReportExcelTLB,
  EXLReportBand, EXLReport, Vcl.Mask, DBCtrlsEh;

type
  TFormObjectStateKinds = class(TFormGrid)
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormObjectStateKinds: TFormObjectStateKinds;

implementation

{$R *.dfm}

uses editstatekind;


procedure TFormObjectStateKinds.Init;
begin
  inherited;
  tablename := 'objectstatekinds';
  self.formEdit := FormEditStateKind;
end;


end.
