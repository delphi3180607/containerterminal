﻿unit EditStartCheck;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, SynEdit, SynMemo;

type
  TFormEditStartCheck = class(TFormEdit)
    meSQL: TSynMemo;
    edName: TDBEditEh;
    edTitle: TDBEditEh;
    edWidths: TDBEditEh;
    edSection: TDBEditEh;
    edFilter: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditStartCheck: TFormEditStartCheck;

implementation

{$R *.dfm}

end.
