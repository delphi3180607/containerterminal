﻿inherited FormEditContractSpec: TFormEditContractSpec
  Caption = #1059#1089#1083#1091#1075#1072
  ClientHeight = 209
  ClientWidth = 430
  ExplicitWidth = 436
  ExplicitHeight = 237
  PixelsPerInch = 96
  TextHeight = 16
  object sbContainer: TSpeedButton [0]
    Left = 358
    Top = 40
    Width = 52
    Height = 24
    Caption = '...'
    OnClick = sbContainerClick
  end
  object Label2: TLabel [1]
    Left = 18
    Top = 18
    Width = 47
    Height = 16
    Caption = #1059#1089#1083#1091#1075#1072
  end
  object Цена: TLabel [2]
    Left = 231
    Top = 88
    Width = 34
    Height = 16
    Caption = #1062#1077#1085#1072
  end
  object Label1: TLabel [3]
    Left = 18
    Top = 88
    Width = 110
    Height = 16
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' "'#1076#1086'"'
  end
  inherited plBottom: TPanel
    Top = 168
    Width = 430
    TabOrder = 2
    ExplicitTop = 168
    ExplicitWidth = 430
    inherited btnCancel: TButton
      Left = 314
      ExplicitLeft = 314
    end
    inherited btnOk: TButton
      Left = 195
      ExplicitLeft = 195
    end
  end
  object luService: TDBLookupComboboxEh [5]
    Left = 18
    Top = 40
    Width = 334
    Height = 24
    DynProps = <>
    DataField = 'service_id'
    DataSource = dsLocal
    EditButtons = <>
    KeyField = 'id'
    ListField = 'scode'
    ListSource = dm.dsServices
    TabOrder = 0
    Visible = True
  end
  object nePrice: TDBNumberEditEh [6]
    Left = 231
    Top = 110
    Width = 121
    Height = 24
    DataField = 'price'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object neAmount: TDBNumberEditEh [7]
    Left = 18
    Top = 110
    Width = 121
    Height = 24
    DataField = 'amount'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 3
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 224
    Top = 152
  end
  inherited qrAux: TADOQuery
    Left = 272
    Top = 152
  end
end
