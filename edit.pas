﻿unit edit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Data.DB,
  Data.Win.ADODB, MemTableEh, Vcl.CheckLst;

type
  TFormEdit = class(TForm)
    plBottom: TPanel;
    btnCancel: TButton;
    btnOk: TButton;
    dsLocal: TDataSource;
    qrAux: TADOQuery;
    procedure FormCreate(Sender: TObject);
  private
    //--
  protected
    flag: boolean;
    procedure SaveChecksValues(id: integer; classification_id: integer; c: TCheckListBox);
    procedure SaveComboValue(id: integer; classification_id: integer; c: TComboBox);
  public
    ParentFormGrid: TForm;
    current_id: integer;
    current_mode: string;
    classif_panel: TWinControl;
    procedure SaveClassifications(id: integer);
  end;

var
  FormEdit: TFormEdit;

implementation

{$R *.dfm}

uses main, dmu;

procedure TFormEdit.FormCreate(Sender: TObject);
begin
 current_id := 0;
 flag := false;
end;

procedure TFormEdit.SaveClassifications(id: integer);
var i, cls_id: integer; c: TControl; n: string;
begin

    exit;

    if not Assigned(classif_panel) then exit;

    for i := 0 to classif_panel.ControlCount-1 do
    begin
      c := classif_panel.Controls[i];
      n := c.Name;
      if n = '' then continue;
      cls_id := StrToInt(copy(n,3,255));
      if c is TCheckListBox then SaveChecksValues(id, cls_id, TCheckListBox(c));
      if c is TComboBox then SaveComboValue(id, cls_id, TComboBox(c));
    end;

end;


procedure TFormEdit.SaveChecksValues(id: integer; classification_id: integer; c: TCheckListBox);
var i: integer; s: string;
begin

  exit;

  dm.qrAux.Close;
  dm.qrAux.SQL.Clear;
  dm.qrAux.SQL.Add('delete from classifvaluesets where parent_table_id = '+IntToStr(id)+' and classification_id = '+IntToStr(classification_id));
  dm.qrAux.ExecSQL;

  dm.qrAux.Close;
  dm.qrAux.SQL.Clear;

  for i := 0 to c.Items.Count -1 do
  begin
    s := c.Items[i];
    if c.Checked[i] then
    begin
      dm.qrAux.SQL.Add('insert into classifvaluesets (parent_table_id, classification_id, classif_value_id)');
      dm.qrAux.SQL.Add('select '+IntToStr(id)+', '+IntToStr(classification_id)+', c.id from classificationvalues c');
      dm.qrAux.SQL.Add('where c.classification_id = '+IntToStr(classification_id)+'and c.string_value = '''+c.Items[i]+''';');
    end;

  end;

  if dm.qrAux.SQL.Count>0 then
  begin
    dm.qrAux.ExecSQL;
  end;

end;

procedure TFormEdit.SaveComboValue(id: integer; classification_id: integer; c: TComboBox);
begin
 //--
end;


end.
