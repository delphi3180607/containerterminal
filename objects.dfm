﻿inherited FormObjects: TFormObjects
  ActiveControl = dgStates
  Caption = #1054#1073#1098#1077#1082#1090#1099
  ClientHeight = 656
  ClientWidth = 900
  OnActivate = FormActivate
  ExplicitWidth = 916
  ExplicitHeight = 694
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter [0]
    Left = 249
    Top = 0
    Width = 7
    Height = 615
    Align = alRight
    Color = 14341069
    ParentColor = False
    StyleElements = [seFont, seBorder]
    ExplicitLeft = 409
    ExplicitTop = -150
    ExplicitHeight = 601
  end
  inherited plBottom: TPanel
    Top = 615
    Width = 900
    ExplicitTop = 615
    ExplicitWidth = 900
    inherited btnOk: TButton
      Left = 665
      ExplicitLeft = 665
    end
    inherited btnCancel: TButton
      Left = 784
      ExplicitLeft = 784
    end
  end
  inherited plAll: TPanel
    Width = 249
    Height = 615
    ExplicitWidth = 249
    ExplicitHeight = 615
    object Splitter1: TSplitter [0]
      Left = 0
      Top = 429
      Width = 249
      Height = 6
      Cursor = crVSplit
      Align = alBottom
      Color = 14341069
      ParentColor = False
      StyleElements = [seFont, seBorder]
      ExplicitTop = 224
      ExplicitWidth = 243
    end
    inherited plTop: TPanel
      Width = 249
      ExplicitWidth = 249
      inherited btFilter: TPngSpeedButton
        OnClick = btFilterClick
        ExplicitTop = -3
      end
      inherited btTool: TPngSpeedButton
        Left = 17
        ExplicitLeft = 391
      end
      object sbClearFilter: TPngSpeedButton [7]
        AlignWithMargins = True
        Left = 178
        Top = 1
        Width = 34
        Height = 26
        Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1080#1083#1100#1090#1088
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 2
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbClearFilterClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000002334944415478DA63FCFFFF3F032580
          91A1FD0A7E13FEA3A8C66A8005903E7E26499541969785285B1F7FFEC36032EF
          3688690931136AC8C9445506691EA021606F01A518FF33349F7C0F56526326C8
          C008147AFAE50F83F9BC3B20694B864A9D130847810CF9FBF7F8E1247506492E
          16B873BBCF400C2833166478F6ED0F83EDBC9B0C0CCCCC60CD98BE0219F2F3C7
          F13D49DA0CE2DC2C60C9C6A3AFC02E4A3714667059709D8181950DAE197BB080
          0CF9F2F9F896140386EBA7CF322C58B78FE1C6DD67C0B08484A68682C4774B5D
          A56573EBE253188CD3B0852BC4109B67878F5FBD729321CEDB92212DD096414A
          949FE1DAFDE70CF3361E6558B8E5388393A9C69D5D27AEA9623520A676EEFA95
          BBCE041C9C55C2A0F6EF13039B800003AF820258EED981030CA7BFB13244B7AF
          640872345C8FD50059AFF2DFD9618E2CE5F1EE0C6F2F5C60D81910C0E0BE6103
          C3CD050B187E7DF8C0E000A40BFB5631ACDF7FE10F5603184DD2FFBFDFDFCFC0
          CFC309E6830CD9ECE0C02005C46E408340E0FCCDC70CA6B16D0C380D78BABD93
          4152841FCC3F909000B6F90DD020904B840D0C188E5DBACB6097D2C380D30BA1
          2EC62CBD85A160CD20007236B2774A365E64D877FAC61F9C81B8E9E0C580355D
          190CAEE69A28727FFFFD6398B1E61043C594750C810E38021114BF36663AFFCF
          5EBCC9E065ADCB90E46FCDA02E2FCE70F9CE538659EB0E3300A38FC1D90C1A8D
          D8B233300CC074B2BFF5ECE397EF45DF78F0821326A72E2FF1DD4A4F69293021
          A5822C02009244E8A4AC1C3E1E0000000049454E44AE426082}
        ExplicitLeft = 204
      end
      inherited plCount: TPanel
        Left = 55
        ExplicitLeft = 55
      end
    end
    inherited dgData: TDBGridEh
      Top = 46
      Width = 249
      Height = 383
    end
    inherited plHint: TPanel
      TabOrder = 4
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    object plObjects: TPanel
      Left = 0
      Top = 435
      Width = 249
      Height = 180
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 249
        Height = 26
        Align = alTop
        BevelOuter = bvLowered
        Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082#1080
        Color = 15921906
        ParentBackground = False
        TabOrder = 0
        object sbAddObject: TPngSpeedButton
          AlignWithMargins = True
          Left = 2
          Top = 2
          Width = 28
          Height = 20
          Hint = #1044#1086#1073#1072#1074#1080#1090#1100
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbAddObjectClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002BE4944415478DAA5935D4893511CC61FA74E97390D6B9B
            1F516C2E334C2535295A8A792549908124A120D145A6303023233FA2444A08F5
            C6ECA2B430B01681994E4D29511321B134F3634A7EA382B9ED7DA7BEE7BC6FAF
            1B2E28BDA9FFB9389C8BE777FEFF739EC7451004FC4FB9FC09286BBBAEA13C4D
            2394A6134A5484A7E0285910CFB5E25E579EFACCB423A0AC2D378550BE72BF42
            E31FA28A80CC7D3736F80D2C5AE7D033DE8EC5E5A57942487675FA4BC35F0087
            98D64407C77969154761328F62C1360B8EE7E023F581421688AE512386C68719
            B1B38CE7971B0C4EC083D65CB5D87667A4FA648056198E0FF32D6019169C40E0
            E9E901415CEBFC3A42E561E81E69C3C8A4698E2344F73AAB75C20E2835EA0B7C
            7DF615C78724A377A913D60D060CC3202FB6C8DE5DC9E77CD85833246E6E8855
            E950DBF1088C852B6CC8E9B86307DC7D97331675382E785DB201D3F298383707
            8BD58C9284723B20CB7009DEBEEE203CC1216504D6AD0C1A7BDBC69BF59D5A3B
            A0B0E12A1B1F992C1BB10C61C5BA829BC7EF6DFB657A631AE4325F84F945A3AA
            E9B1ED7D6ECF2E3BE0D69B2BEC89F044D9B0791036BA8682A8FB3B02BCA57284
            2B6250F1B6CAF6F1469F039067C81CD31C0C0DE63D04CC58A6C0726B6016CDA8
            BE50EF146E96C44502ADDF11B06616AF7A1AC7BBF3FB1D23E8EBD30BD6246CF1
            D963A9E89A69C11AE520E55D507AE68913C0530152891BE2354978D85881558B
            B5B0EFF657C7235E7B91A6E628ED542AF606C4A84FA3EB47B3FDC1C4AF854037
            1102DC25AE885327C138D0884FDFBFCC899ED1F5177D9B701A29F3E979D148A4
            2650A5F4D28524627A7512B33F2721F03C82F6A811243F80A68106F40E0F32A2
            BD3344F16F236DD5C5EAA414D1EF95D495F8C7859E8252EEBF7939A657A660EC
            6F87D9CACE8BE2EC2DF1B6613A5799A01121699C334C440C13B58789F2A4AEBF
            6878E730FD4BFD027D1196F03509C5820000000049454E44AE426082}
          ExplicitTop = 0
        end
        object sbDeleteObject: TPngSpeedButton
          AlignWithMargins = True
          Left = 34
          Top = 4
          Width = 32
          Height = 18
          Hint = #1059#1076#1072#1083#1080#1090#1100
          Align = alLeft
          Flat = True
          OnClick = sbDeleteObjectClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            610000001974455874536F6674776172650041646F626520496D616765526561
            647971C9653C000002554944415478DA95924D68D36018C79F7C746D92A6B5DD
            86B2B56C7810A141F0E8459C7810BC09A2CECFA907F13211C7BC7A134410069E
            046FC26EE2556D657EEC24F5D04C2F62B77569BA43ED47DA244BF2C6E74D6CB1
            743BEC8524BCE4FDFD9E27FF278CEFFBB09F65CFDD11A3AF5E767B7B6640B0F0
            680AEF7178FA44DD15BE793BE7BBCE19E2B84571F9F5CAA080C21C57A67BDFB6
            15F6F9B301897DE3560EDF9798E808ECD4B69751F4587EFBE64728F80743340A
            54E7B7DB402C4BE15F2C0512EBFA5C8E61D9129790814F26C1DEAC80A5EBF789
            E32C8582878B319F900E0802CBC83290560B48A3811253C17681C22CC2914412
            ACAD0AD855DD435848E5DF39FD4F20F30F62BEE7198C20702C1EF49A0D70FFD4
            C1470187D248320156658B56F6B0FD782AFFDE1A0AD1BD7B2F465CCF604591E3
            5307003C82667A795899C2B5102E7CB0769F4218164ADC8E3035CDC2C63A4A3C
            80CC24348ADF09711D29FD316FFD7F7E4860CE5EC3C098526C3203502E871DA0
            A0B9B6064EDB50C6BE7E52F7149897AFE600615E94303019613F689F4AAC5A0D
            4C5D879D765B3958FCA60E09BA97AE04954338016655A3104D9B89A5D2AC3471
            08BA551DBA9A86124399F8A9AA7D41F7E26C30675E14118E83A9E9606D53D88D
            6368804F43184D737236031DAD0A1D0C947692F9FD4B0D04C6F90B336C84CF47
            24095CA30326C238BEF8E897952030FDD8711CB16B48E3E31C3D63D089D4EBA7
            B31BE542FF139A67CFA12492775A4D8215250C6B206DEDC8D1E03F199165AE07
            0F85583F796A06E1D5B1D5CF03706F55A60F53C989ECE67A61CF31EE77FD05EB
            B7706A5FA737EA0000000049454E44AE426082}
          ExplicitLeft = 42
          ExplicitTop = 8
        end
        object sbEditObject: TPngSpeedButton
          AlignWithMargins = True
          Left = 70
          Top = 2
          Width = 26
          Height = 20
          Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Align = alLeft
          Flat = True
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          OnClick = sbEditObjectClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D4948445200000014000000140803000000BA57ED
            3F0000000373424954080808DBE14FE00000012F504C5445FFFFFFFEFEFEFDFD
            FDFCFCFCFBFBFBFAFAFAF9F9F9F8F8F8F7F7F7F6F6F6F5F5F5FFF3E5FDF7C7F4
            F4F4F3F3F3FDF6C1FFF2E0FDF6C3F2F2F2F1F1F1F0F0F0EFEFEFF6EEE4F5EBDF
            F1E9E0F2E9DEEAEAEAE9E9E9F3E6D8F5E5D4F9E3B1F2E5BCF3E4AEE3E3E3EEE2
            D4FCE0C3F0E3B9E2E2E2DFDFDFF8E25CF7E25AF7DF62F7E159F5DF5AF6DD6BEE
            D8B0F5DC5DE8D9A7F5DC5EF6D3ADF0D2B3F2D679EFD2ABE4D5ABE5D5B1E5D4AE
            EED680EED47CF1D47AEED378F0D378F9C990EDD077F9C793CCCCCCCBCBCBEDCA
            6CE7CB76C9C9C9E4C954D7C2A4E7BF8AC2C2C2E7BD76C9BDAFD7B975D1B68FD5
            B579DBB084CBA76ED8A457D7A262E2A150D4A16ADD9C50D29E65D89953DD994A
            D09C45DC9748CC9558C49748C78D47BD894BC98440A98842BD7919AD6A258466
            2D88602A5942173C25EF5A000000097048597300000B1200000B1201D2DD7EFC
            0000001C74455874536F6674776172650041646F62652046697265776F726B73
            2043533571B5E336000000D54944415478DA6364C0021871092ACAB340798FEE
            FC83093A1CFFFB1F2CC6E2780F2C0A127439C0F99781819381E38591F43FAEAB
            972182FB5981241B1068BEBCA9A9B60C2AC80C24993965399F69BEFA64B0162A
            08365056F486B4183FCBC1574882323217557E70CBFFD8FE1F2168C27352E98F
            80EC31F5BD084163D12352FF44650FBC7244083AB3FFFFF94C58ECF0873F4882
            311BDDB87E331E78CD802C187F97419EF9E06320C71E2198F8FFD7A7936F408E
            7040B2080E1CF740FCEE78F42F5C8CD9EA0044505586192EF8E7E96D3C818C01
            004FE4561533982BD20000000049454E44AE426082}
          ExplicitLeft = 78
          ExplicitTop = 6
        end
      end
      object dgObjects: TDBGridEh
        Left = 0
        Top = 26
        Width = 249
        Height = 154
        Align = alClient
        BorderStyle = bsNone
        DataSource = dsOwner
        DynProps = <>
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = []
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
        ParentFont = False
        PopupMenu = pmOwners
        ReadOnly = True
        SelectionDrawParams.DrawFocusFrame = True
        SelectionDrawParams.DrawFocusFrameStored = True
        TabOrder = 1
        TitleParams.FillStyle = cfstSolidEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -12
        TitleParams.Font.Name = 'Verdana'
        TitleParams.Font.Style = [fsBold]
        TitleParams.ParentFont = False
        OnDblClick = sbEditObjectClick
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'date_change'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
            Width = 141
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'owner_code'
            Footers = <>
            Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
            Width = 288
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object plStatus: TPanel
      Left = 0
      Top = 29
      Width = 249
      Height = 17
      Align = alTop
      BevelOuter = bvNone
      Color = 15921906
      ParentBackground = False
      TabOrder = 3
    end
  end
  object Panel1: TPanel [3]
    Left = 256
    Top = 0
    Width = 644
    Height = 615
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 2
    object Splitter3: TSplitter
      Left = 0
      Top = 429
      Width = 644
      Height = 6
      Cursor = crVSplit
      Align = alBottom
      Color = 14341069
      ParentColor = False
      StyleElements = [seFont, seBorder]
      ExplicitLeft = 168
      ExplicitTop = 413
      ExplicitWidth = 438
    end
    object dgStates: TDBGridEh
      Left = 0
      Top = 0
      Width = 644
      Height = 429
      Align = alClient
      BorderStyle = bsNone
      DataSource = dsStates
      DynProps = <>
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Verdana'
      Font.Style = []
      GridLineParams.DataHorzColor = clSilver
      GridLineParams.DataVertColor = clSilver
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      IndicatorParams.HorzLineColor = clSilver
      IndicatorParams.VertLineColor = clSilver
      OptionsEh = [dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
      ParentFont = False
      ReadOnly = True
      RowHeight = 18
      SelectionDrawParams.DrawFocusFrame = True
      SelectionDrawParams.DrawFocusFrameStored = True
      TabOrder = 0
      TitleParams.FillStyle = cfstGradientEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -12
      TitleParams.Font.Name = 'Verdana'
      TitleParams.Font.Style = [fsBold]
      TitleParams.HorzLineColor = clSilver
      TitleParams.MultiTitle = True
      TitleParams.ParentFont = False
      TitleParams.VertLineColor = clSilver
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_factexecution'
          Footers = <>
          HideDuplicates = True
          Title.Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
          Width = 81
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer_code'
          Footers = <>
          Title.Caption = #1050#1083#1080#1077#1085#1090
          Width = 108
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          HideDuplicates = True
          Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 92
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_code'
          Footers = <>
          Title.Caption = #1044#1086#1082#1091#1084#1077#1085#1090
          Width = 147
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'forwarder_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          Width = 139
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 144
          WordWrap = False
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 435
      Width = 644
      Height = 180
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 644
        Height = 180
        Align = alClient
        AllowedOperations = []
        BorderStyle = bsNone
        DataSource = dsLinkedObjects
        DynProps = <>
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = []
        GridLineParams.DataHorzColor = clSilver
        GridLineParams.DataVertColor = clSilver
        GridLineParams.VertEmptySpaceStyle = dessNonEh
        IndicatorParams.HorzLineColor = clSilver
        IndicatorParams.VertLineColor = clSilver
        OptionsEh = [dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
        ParentFont = False
        ReadOnly = True
        SelectionDrawParams.DrawFocusFrame = True
        SelectionDrawParams.DrawFocusFrameStored = True
        TabOrder = 0
        TitleParams.FillStyle = cfstGradientEh
        TitleParams.Font.Charset = DEFAULT_CHARSET
        TitleParams.Font.Color = clWindowText
        TitleParams.Font.Height = -12
        TitleParams.Font.Name = 'Verdana'
        TitleParams.Font.Style = [fsBold]
        TitleParams.HorzLineColor = clSilver
        TitleParams.ParentFont = False
        TitleParams.VertLineColor = clSilver
        Columns = <
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'date_history'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
            Width = 113
          end
          item
            CellButtons = <>
            DynProps = <>
            EditButtons = <>
            FieldName = 'object_code'
            Footers = <>
            Title.Caption = #1054#1073#1098#1077#1082#1090#1099' '#1074' '#1089#1094#1077#1087#1082#1077
            Width = 172
          end
          item
            CellButtons = <>
            Checkboxes = True
            DynProps = <>
            EditButtons = <>
            FieldName = 'isempty'
            Footers = <>
            Title.Caption = #1055#1086#1088#1086#1078#1085#1080#1081'?'
            Width = 103
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from objects')
  end
  inherited drvForms: TADODataDriverEh
    Left = 612
    Top = 50
  end
  inherited meForms: TMemTableEh
    Left = 660
    Top = 50
  end
  inherited dsForms: TDataSource
    Left = 708
    Top = 50
  end
  object drvState: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select odh.*,'
      
        '(select code from counteragents c where c.id = t.forwarder_id) a' +
        's forwarder_code,'
      
        '(select code from counteragents c where c.id = t.consignee_id) a' +
        's consignee_code,'
      
        '(select code from counteragents c where c.id = t.payer_id) as pa' +
        'yer_code,'
      
        '(select code from doctypes t where t.id = d.doctype_id)+'#39' '#8470' '#39'+d.' +
        'doc_number+'#39' '#1086#1090'  '#39'+convert(varchar(10), d.doc_date, 104) as doc_' +
        'code, '
      
        '(select code from objectstatekinds k where k.id = odh.object_sta' +
        'te_id) as state_code,'
      'odh.date_history '
      'from objectstatehistory odh, documents d, tasks t'
      'where d.id = odh.doc_id and odh.object_id = :object_id'
      'and odh.task_id = t.id'
      'order by date_factexecution')
    SelectCommand.Parameters = <
      item
        Name = 'object_id'
        Size = -1
        Value = Null
      end>
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 568
    Top = 216
  end
  object meStates: TMemTableEh
    Params = <>
    DataDriver = drvState
    AfterScroll = meStatesAfterScroll
    Left = 616
    Top = 216
  end
  object dsStates: TDataSource
    DataSet = meStates
    Left = 672
    Top = 216
  end
  object drvOwner: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select h.*, (select code from counteragents c where c.id = h.own' +
        'er_id ) as owner_code '
      'from objectownerhistory h where h.object_id = :object_id'
      'order by date_change desc')
    SelectCommand.Parameters = <
      item
        Name = 'object_id'
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update objectownerhistory'
      'set'
      '  owner_id = :owner_id,'
      '  date_change = :date_change'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'date_change'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into objectownerhistory'
      '  (object_id, owner_id, date_change)'
      'values'
      '  (:object_id, :owner_id, :date_change)')
    InsertCommand.Parameters = <
      item
        Name = 'object_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'date_change'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from objectownerhistory where  id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select h.*, (select code from counteragents c where c.id = h.own' +
        'er_id ) from objectownerhistory h where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 184
    Top = 520
  end
  object meOwner: TMemTableEh
    Params = <>
    DataDriver = drvOwner
    AfterPost = meOwnerAfterPost
    Left = 232
    Top = 520
  end
  object dsOwner: TDataSource
    DataSet = meOwner
    Left = 288
    Top = 520
  end
  object drvLinkedObjects: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select odh.object_id, odh.date_history, vl.rus_object_type+'#39': '#39'+' +
        'vl.cnum as object_code, vl.isempty'
      'from objectstatehistory odh'
      'left outer join v_objects vl on (vl.id = odh.object_id)'
      
        'where  odh.linked_object_id = :object_id and odh.doc_id = :doc_i' +
        'd'
      'order by odh.date_history desc')
    SelectCommand.Parameters = <
      item
        Name = 'object_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      '')
    UpdateCommand.Parameters = <>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 560
    Top = 512
  end
  object meLinkedObjects: TMemTableEh
    Params = <>
    DataDriver = drvLinkedObjects
    Left = 608
    Top = 512
  end
  object dsLinkedObjects: TDataSource
    DataSet = meLinkedObjects
    Left = 664
    Top = 512
  end
  object pmOwners: TPopupMenu
    Left = 80
    Top = 512
    object MenuItem1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 45
      OnClick = sbAddObjectClick
    end
    object MenuItem2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 46
      OnClick = sbDeleteObjectClick
    end
    object MenuItem3: TMenuItem
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
      ShortCut = 113
      OnClick = sbEditObjectClick
    end
    object MenuItem7: TMenuItem
      Caption = '-'
    end
    object MenuItem8: TMenuItem
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ShortCut = 116
      OnClick = MenuItem8Click
    end
  end
end
