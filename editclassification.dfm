﻿inherited FormEditClassification: TFormEditClassification
  Caption = #1050#1083#1072#1089#1089#1080#1092#1080#1082#1072#1094#1080#1103
  ClientHeight = 254
  ClientWidth = 394
  ExplicitWidth = 400
  ExplicitHeight = 282
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 213
    Width = 394
    TabOrder = 3
    inherited btnCancel: TButton
      Left = 278
    end
    inherited btnOk: TButton
      Left = 159
    end
  end
  object edTableName: TDBEditEh [1]
    Left = 24
    Top = 40
    Width = 329
    Height = 24
    ControlLabel.Width = 88
    ControlLabel.Height = 16
    ControlLabel.Caption = #1048#1084#1103' '#1090#1072#1073#1083#1080#1094#1099
    ControlLabel.Visible = True
    DataField = 'table_name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object edClassifName: TDBEditEh [2]
    Left = 24
    Top = 104
    Width = 329
    Height = 24
    ControlLabel.Width = 217
    ControlLabel.Height = 16
    ControlLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1082#1083#1072#1089#1089#1080#1092#1080#1082#1072#1090#1086#1088#1072
    ControlLabel.Visible = True
    DataField = 'classification_name'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object cbMulti: TDBCheckBoxEh [3]
    Left = 24
    Top = 168
    Width = 153
    Height = 17
    Caption = #1052#1091#1083#1100#1090#1080#1074#1099#1073#1086#1088
    DataField = 'ismulty'
    DataSource = dsLocal
    DynProps = <>
    TabOrder = 2
  end
end
