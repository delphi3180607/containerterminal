﻿unit editdocincomespec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, editorderspec, MemTableDataEh, Data.DB,
  Data.Win.ADODB, MemTableEh, DataDriverEh, ADODataDriverEh, DBSQLLookUp,
  DBCtrlsEh, Vcl.StdCtrls, Vcl.Mask, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton;

type
  TFormEditInComeSpec = class(TFormEditOrderSpec)
    laAddress: TDBSQLLookUp;
    ssAddress: TADOLookUpSqlSet;
    cbShift: TDBCheckBoxEh;
    procedure edContainerNumExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditInComeSpec: TFormEditInComeSpec;

implementation

{$R *.dfm}

procedure TFormEditInComeSpec.edContainerNumExit(Sender: TObject);
var k, i: integer;
begin

  lbWarning.Hide;

  if edContainerNum.Text<>'' then
  begin

    k:=0;
    i:=1;
    while(i<length(edContainerNum.Text))and(k=0) do
    if ((AnsiString(edContainerNum.Text)[i] in ['A'..'Z']) and (i<5))
    or ((AnsiString(edContainerNum.Text)[i] in ['0'..'9']) and (i>4))
    then
    begin
      k:=0;
      inc(i);
    end else
      k:=1;

    if k=1 then
    begin
      ShowMessage('В номере контейнера есть недопустимые символы, либо - неверный формат!');
      edContainerNum.Text := '           ';
      edContainerNum.SetFocus;
      exit;
    end;


    meContainers.Close;
    drvContainers.SelectCommand.Parameters.ParamByName('cnum').Value := edContainerNum.Text;
    drvContainers.SelectCommand.Parameters.ParamByName('docid').Value := dsLocal.DataSet.FieldByName('doc_id').AsInteger;
    meContainers.Open;

    if meContainers.FieldByName('extraorder').AsInteger = 1 then
    begin
      lbWarning.Caption := 'Внимание! На данный контейнер уже заведена первичная заявка с таким же типом операции.';
      lbWarning.Show;
    end;

  end;

  AfterNumFieldExit;

end;

end.
