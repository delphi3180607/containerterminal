﻿inherited FormEditOwner: TFormEditOwner
  Caption = #1042#1083#1072#1076#1077#1083#1077#1094
  ClientHeight = 201
  ClientWidth = 532
  ExplicitWidth = 538
  ExplicitHeight = 229
  PixelsPerInch = 96
  TextHeight = 16
  object Label8: TLabel [0]
    Left = 8
    Top = 10
    Width = 76
    Height = 16
    Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
  end
  object sbCounteragent: TSpeedButton [1]
    Left = 471
    Top = 29
    Width = 52
    Height = 24
    Caption = '...'
    OnClick = sbCounteragentClick
  end
  object Label1: TLabel [2]
    Left = 8
    Top = 84
    Width = 110
    Height = 16
    Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
  end
  inherited plBottom: TPanel
    Top = 160
    Width = 532
    TabOrder = 2
    ExplicitTop = 160
    ExplicitWidth = 532
    inherited btnCancel: TButton
      Left = 416
      ExplicitLeft = 416
    end
    inherited btnOk: TButton
      Left = 297
      ExplicitLeft = 297
    end
  end
  object luCounteragent: TDBLookupComboboxEh [4]
    Left = 8
    Top = 29
    Width = 457
    Height = 24
    DynProps = <>
    DataField = 'owner_id'
    DataSource = dsLocal
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    KeyField = 'id'
    ListField = 'code'
    ListSource = dm.dsCounteragents
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object dtChange: TDBDateTimeEditEh [5]
    Left = 8
    Top = 104
    Width = 121
    Height = 24
    DataField = 'date_change'
    DataSource = dsLocal
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  inherited dsLocal: TDataSource
    Left = 480
    Top = 64
  end
end
