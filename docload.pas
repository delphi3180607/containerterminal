﻿unit docload;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, docflow, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, JvExExtCtrls, JvExtComponent, JvSplit;

type
  TFormDocFlow1 = class(TFormDocFlow)
    dgSpec: TDBGridEh;
    Panel2: TPanel;
    sbAddSpec: TPngSpeedButton;
    sbDeleteSpec: TPngSpeedButton;
    sbEditSpec: TPngSpeedButton;
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormDocFlow1: TFormDocFlow1;

implementation

{$R *.dfm}

end.
