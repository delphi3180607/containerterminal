﻿object FormStart: TFormStart
  Left = 0
  Top = 0
  Caption = 'FormStart'
  ClientHeight = 570
  ClientWidth = 832
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 577
    Top = 0
    Width = 4
    Height = 570
    ExplicitLeft = 289
    ExplicitHeight = 522
  end
  object plLeft: TPanel
    Left = 0
    Top = 0
    Width = 577
    Height = 570
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object dgChecks: TDBGridEh
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 571
      Height = 564
      Align = alClient
      AutoFitColWidths = True
      Border.Color = clSilver
      Border.EdgeBorders = [ebTop]
      Border.ExtendedDraw = True
      BorderStyle = bsNone
      Ctl3D = False
      DynProps = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      GridLineParams.BrightColor = clWhite
      GridLineParams.DarkColor = clWhite
      GridLineParams.DataBoundaryColor = clWhite
      GridLineParams.DataHorzColor = clWhite
      GridLineParams.DataVertColor = clWhite
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      IndicatorOptions = []
      IndicatorParams.FillStyle = cfstGradientEh
      IndicatorParams.HorzLineColor = clSilver
      IndicatorParams.VertLineColor = clSilver
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove, dghAutoFitRowHeight]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleParams.FillStyle = cfstSolidEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clNavy
      TitleParams.Font.Height = -13
      TitleParams.Font.Name = 'Tahoma'
      TitleParams.Font.Style = []
      TitleParams.HorzLineColor = clWhite
      TitleParams.ParentFont = False
      TitleParams.SecondColor = clCream
      TitleParams.VertLineColor = clWhite
      Columns = <
        item
          CellButtons = <>
          Color = 14011803
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1055#1088#1086#1074#1077#1088#1082#1072
          Width = 504
          WordWrap = True
        end
        item
          CellButtons = <>
          Color = 14011803
          DynProps = <>
          EditButtons = <>
          Footers = <>
          Title.Alignment = taCenter
          Title.Caption = #1053#1072#1081#1076#1077#1085#1086
          Width = 65
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
end
