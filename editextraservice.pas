﻿unit editextraservice;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBCtrlsEh, Vcl.Mask, DBGridEh, DBLookupEh,
  Vcl.Buttons, functions;

type
  TFormEditExtraService = class(TFormEdit)
    edDocNumber: TDBEditEh;
    Label1: TLabel;
    dtDocDate: TDBDateTimeEditEh;
    Label3: TLabel;
    sbCustomer: TSpeedButton;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    luCustomer: TDBLookupComboboxEh;
    cbBaseDoc: TDBComboBoxEh;
    cbBaseNumber: TDBEditEh;
    cbBaseDate: TDBDateTimeEditEh;
    dtIncome: TDBDateTimeEditEh;
    procedure sbCustomerClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditExtraService: TFormEditExtraService;

implementation

{$R *.dfm}

uses counteragents;

procedure TFormEditExtraService.sbCustomerClick(Sender: TObject);
begin
  SFD(FormCounteragents, self.luCustomer);
end;

end.
