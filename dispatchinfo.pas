﻿unit DispatchInfo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormDispatchInfo = class(TFormEdit)
    edAmount: TDBEditEh;
    edLen1: TDBEditEh;
    edLen: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormDispatchInfo: TFormDispatchInfo;

implementation

{$R *.dfm}

end.
