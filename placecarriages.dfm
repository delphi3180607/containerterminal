﻿inherited FormPlaceCarriages: TFormPlaceCarriages
  Caption = #1056#1072#1079#1084#1077#1097#1077#1085#1080#1077' '#1087#1083#1072#1090#1092#1086#1088#1084
  ClientHeight = 668
  ClientWidth = 900
  ExplicitWidth = 916
  ExplicitHeight = 706
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 627
    Width = 900
    inherited btnOk: TButton
      Left = 665
    end
    inherited btnCancel: TButton
      Left = 784
    end
  end
  inherited plAll: TPanel
    Width = 900
    Height = 627
    object Splitter1: TSplitter [0]
      Left = 301
      Top = 29
      Height = 598
      Align = alRight
      ExplicitLeft = 376
      ExplicitTop = 304
      ExplicitHeight = 100
    end
    inherited plTop: TPanel
      Width = 900
      inherited sbDelete: TPngSpeedButton
        Left = 64
      end
      inherited btFilter: TPngSpeedButton
        Left = 183
      end
      inherited btExcel: TPngSpeedButton
        Left = 137
      end
      inherited sbAdd: TPngSpeedButton
        Left = 33
      end
      inherited sbEdit: TPngSpeedButton
        Left = 96
      end
      inherited Bevel2: TBevel
        Left = 132
      end
      inherited btTool: TPngSpeedButton
        Left = 848
      end
      object sbFilterPlan: TPngSpeedButton
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 33
        Height = 29
        Hint = #1054#1090#1073#1086#1088' '#1087#1086' '#1087#1083#1072#1085#1091' '#1087#1086#1075#1088#1091#1079#1082#1080
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        AllowAllUp = True
        GroupIndex = 1
        ParentShowHint = False
        ShowHint = True
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000001EA4944415478DACD534D4B5B41143DF35E44F191B42F1A
          7521D6454B4129A506455104511766634AA1206ABB2C5DD5D216EA7F7021BA14
          04411144D055A91B9528221831368AC58F48C4DA2450431AF5E57DCD7813A11B
          290A6E1CB8CC3033E7DC33E7DE614208DC65B0FB4120FABF74D37A8842CE6E0A
          9B03B60D615A109675351BE6559814BA6971C3E853E6BE8D33FEF5F36BC28CB2
          9636054E17A028406121401779228EE4AF134844E6E01CB66EE04CBB004E9378
          B8B87C2E0CA33B4B10644F9E7A51520664324055550E8C8303F0D5559CA4D390
          49450129B5751D47A91494C646C8BFE3F004568EB304CFC1F9088A8A6B59F923
          A0A909A8A8000201F0E161C4350D32810B48E61F4AA0B7B622FF300AF75A2846
          4FFA90F3807FEA53E99D13ACA8B8437A5103D4D501B118C4C000A94D42662CE7
          C9697333A4BD08D4F550841BD62B756723F4AF0AD6FB772E61589392C7E393FD
          7EC0E984181CC4DF68140E49824699CF7776F1606D234246FAD59F9BE16B65D4
          7B7A5D7438295756FAF2DEF4428C8DE1221884C3E743E2E818CEF9A504C96E53
          777F84FFDB075AE74B52624E3BBC35ED79CFAA6193823419C6E6163244DEA5EE
          85676F6CA4B396F6722259C8AFAF7DCCA97CDAF7F92CF8AD7B7F6BEAD69D98F2
          365413C928014BC99B8FEEC8F6CC3DFE0B771997929F1C899745E5BA00000000
          49454E44AE426082}
      end
    end
    inherited dgData: TDBGridEh
      Width = 301
      Height = 598
    end
    object gpRight: TGridPanel
      Left = 304
      Top = 29
      Width = 596
      Height = 598
      Align = alRight
      ColumnCollection = <
        item
          Value = 100.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = pl1
          Row = 0
        end
        item
          Column = 0
          Control = Panel1
          Row = 1
        end
        item
          Column = 0
          Control = Panel2
          Row = 2
        end
        item
          Column = 0
          Control = Panel3
          Row = 3
        end
        item
          Column = 0
          Control = Panel4
          Row = 4
        end>
      RowCollection = <
        item
          Value = 19.999781647316450000
        end
        item
          Value = 19.999939843542380000
        end
        item
          Value = 20.000152584339240000
        end
        item
          Value = 20.000115309074450000
        end
        item
          Value = 20.000010615727470000
        end>
      TabOrder = 2
      object pl1: TPanel
        Left = 1
        Top = 1
        Width = 594
        Height = 119
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 48
        ExplicitTop = 32
        ExplicitWidth = 185
        ExplicitHeight = 41
        object PngSpeedButton1: TPngSpeedButton
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 57
          Height = 113
          Align = alLeft
          Caption = '>'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitHeight = 119
        end
        object DBGrid1: TDBGrid
          AlignWithMargins = True
          Left = 66
          Top = 3
          Width = 525
          Height = 113
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          Columns = <
            item
              Expanded = False
              Title.Caption = #1055#1091#1090#1100' 1'
              Width = 166
              Visible = True
            end>
        end
      end
      object Panel1: TPanel
        Left = 1
        Top = 120
        Width = 594
        Height = 119
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 48
        ExplicitTop = 32
        ExplicitWidth = 185
        ExplicitHeight = 41
        object PngSpeedButton2: TPngSpeedButton
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 57
          Height = 113
          Align = alLeft
          Caption = '>'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitHeight = 119
        end
        object DBGrid2: TDBGrid
          AlignWithMargins = True
          Left = 66
          Top = 3
          Width = 525
          Height = 113
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          Columns = <
            item
              Expanded = False
              Title.Caption = #1055#1091#1090#1100' 2'
              Width = 164
              Visible = True
            end>
        end
      end
      object Panel2: TPanel
        Left = 1
        Top = 239
        Width = 594
        Height = 119
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        ExplicitLeft = 48
        ExplicitTop = 32
        ExplicitWidth = 185
        ExplicitHeight = 41
        object PngSpeedButton3: TPngSpeedButton
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 57
          Height = 113
          Align = alLeft
          Caption = '>'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitHeight = 119
        end
        object DBGrid3: TDBGrid
          AlignWithMargins = True
          Left = 66
          Top = 3
          Width = 525
          Height = 113
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          Columns = <
            item
              Expanded = False
              Title.Caption = #1055#1091#1090#1100' 3'
              Width = 162
              Visible = True
            end>
        end
      end
      object Panel3: TPanel
        Left = 1
        Top = 358
        Width = 594
        Height = 119
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 3
        ExplicitLeft = 48
        ExplicitTop = 32
        ExplicitWidth = 185
        ExplicitHeight = 41
        object PngSpeedButton4: TPngSpeedButton
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 57
          Height = 113
          Align = alLeft
          Caption = '>'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitHeight = 119
        end
        object DBGrid4: TDBGrid
          AlignWithMargins = True
          Left = 66
          Top = 3
          Width = 525
          Height = 113
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          Columns = <
            item
              Expanded = False
              Title.Caption = #1055#1091#1090#1100' 4'
              Width = 160
              Visible = True
            end>
        end
      end
      object Panel4: TPanel
        Left = 1
        Top = 477
        Width = 594
        Height = 120
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 4
        ExplicitLeft = 48
        ExplicitTop = 32
        ExplicitWidth = 185
        ExplicitHeight = 41
        object PngSpeedButton5: TPngSpeedButton
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 57
          Height = 114
          Align = alLeft
          Caption = '>'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitHeight = 119
        end
        object DBGrid5: TDBGrid
          AlignWithMargins = True
          Left = 66
          Top = 3
          Width = 525
          Height = 114
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          Columns = <
            item
              Expanded = False
              Title.Caption = #1055#1091#1090#1100' 5'
              Width = 156
              Visible = True
            end>
        end
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select 1')
  end
  inherited al: TActionList
    inherited aEdit: TAction
      Enabled = False
      Visible = False
    end
  end
end
