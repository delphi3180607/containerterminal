﻿inherited FormEditCarModel: TFormEditCarModel
  Caption = #1055#1088#1086#1089#1090#1072#1085#1086#1074#1082#1072' '#1084#1086#1076#1077#1083#1080
  ClientHeight = 119
  ClientWidth = 297
  ExplicitWidth = 303
  ExplicitHeight = 147
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel [0]
    Left = 20
    Top = 9
    Width = 50
    Height = 16
    Caption = #1052#1086#1076#1077#1083#1100
  end
  inherited plBottom: TPanel
    Top = 78
    Width = 297
    TabOrder = 1
    inherited btnCancel: TButton
      Left = 181
    end
    inherited btnOk: TButton
      Left = 62
    end
  end
  object luModel: TDBSQLLookUp [2]
    Left = 20
    Top = 29
    Width = 247
    Height = 24
    DynProps = <>
    EditButtons = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Visible = True
    SqlSet = ssModels
  end
  inherited dsLocal: TDataSource
    Left = 120
    Top = 64
  end
  inherited qrAux: TADOQuery
    Left = 32
    Top = 64
  end
  object ssModels: TADOLookUpSqlSet
    TmplSql.Strings = (
      'select * from carriagemodels order by model_code')
    DownSql.Strings = (
      'select * from carriagemodels order by model_code')
    InitSql.Strings = (
      'select * from carriagemodels where id = @id')
    KeyName = 'id'
    DisplayName = 'model_code'
    Connection = dm.connMain
    Left = 197
    Top = 17
  end
end
