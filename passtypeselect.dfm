﻿inherited FormPassTypeSelect: TFormPassTypeSelect
  BorderStyle = bsToolWindow
  Caption = #1058#1080#1087' '#1087#1088#1086#1087#1091#1089#1082#1072
  ClientHeight = 180
  ClientWidth = 304
  ExplicitWidth = 310
  ExplicitHeight = 204
  PixelsPerInch = 96
  TextHeight = 16
  object sbType0: TSpeedButton [0]
    Left = 32
    Top = 16
    Width = 241
    Height = 49
    GroupIndex = 1
    Caption = #1055#1088#1080#1077#1084'/'#1074#1099#1076#1072#1095#1072' '#1050#1058#1050
    OnClick = sbType0Click
  end
  object sbType1: TSpeedButton [1]
    Left = 32
    Top = 71
    Width = 241
    Height = 49
    GroupIndex = 1
    Caption = #1055#1088#1086#1095#1077#1077
    OnClick = sbType0Click
  end
  inherited plBottom: TPanel
    Top = 139
    Width = 304
    ExplicitTop = 139
    ExplicitWidth = 304
    inherited btnCancel: TButton
      Left = 188
      ExplicitLeft = 188
    end
    inherited btnOk: TButton
      Left = 69
      Caption = #1055#1088#1086#1076#1086#1083#1078#1080#1090#1100
      OnClick = btnOkClick
      ExplicitLeft = 69
    end
  end
  inherited dsLocal: TDataSource
    Left = 48
    Top = 136
  end
  inherited qrAux: TADOQuery
    Left = 8
  end
end
