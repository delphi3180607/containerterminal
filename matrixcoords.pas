﻿unit matrixcoords;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  Vcl.ExtCtrls, Vcl.StdCtrls, EXLReportExcelTLB, EXLReportBand, EXLReport,
  System.Actions, Vcl.ActnList, MemTableEh, DataDriverEh, ADODataDriverEh,
  Vcl.Menus, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Mask, DBCtrlsEh,
  Vcl.Buttons, PngSpeedButton, functions;

type
  TFormMatrixCoords = class(TFormGrid)
    plRight: TPanel;
    Bevel4: TBevel;
    dgRows: TDBGridEh;
    plTool: TPanel;
    SplitterVert: TSplitter;
    btEditRow: TPngSpeedButton;
    plSpec: TPanel;
    plToolSpec: TPanel;
    dgStacks: TDBGridEh;
    Splitter1: TSplitter;
    drvRows: TADODataDriverEh;
    meRows: TMemTableEh;
    dsRows: TDataSource;
    drvStacks: TADODataDriverEh;
    meStacks: TMemTableEh;
    dsStacks: TDataSource;
    btDisableRow: TPngSpeedButton;
    btEditStack: TPngSpeedButton;
    btDisableStack: TPngSpeedButton;
    Panel1: TPanel;
    plToolExclude: TPanel;
    dgExclude: TDBGridEh;
    Splitter2: TSplitter;
    sbAddExcl: TPngSpeedButton;
    sdDelExcl: TPngSpeedButton;
    sdEditExcl: TPngSpeedButton;
    drvExcludes: TADODataDriverEh;
    meExcludes: TMemTableEh;
    dsExcludes: TDataSource;
    procedure meRowsAfterInsert(DataSet: TDataSet);
    procedure meDataAfterOpen(DataSet: TDataSet);
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure btEditRowClick(Sender: TObject);
    procedure meDataAfterPost(DataSet: TDataSet);
    procedure btEditStackClick(Sender: TObject);
    procedure btDisableRowClick(Sender: TObject);
    procedure btDisableStackClick(Sender: TObject);
    procedure sdEditExclClick(Sender: TObject);
    procedure sbAddExclClick(Sender: TObject);
    procedure sdDelExclClick(Sender: TObject);
    procedure meExcludesAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure Init; override;
  end;

var
  FormMatrixCoords: TFormMatrixCoords;

implementation

{$R *.dfm}

uses editmatrixsector, EditRowStack, EditMatrixExclude;

procedure TFormMatrixCoords.btDisableRowClick(Sender: TObject);
begin

  if fQYN('Деактивировать ряд?') then
  begin
    meRows.Edit;
    meRows.FieldByName('isactive').Value := 0;
    meRows.Post;
  end;

end;

procedure TFormMatrixCoords.btDisableStackClick(Sender: TObject);
begin

  if fQYN('Деактивировать штабель?') then
  begin
    meStacks.Edit;
    meStacks.FieldByName('isactive').Value := 0;
    meStacks.Post;
  end;

end;

procedure TFormMatrixCoords.btEditRowClick(Sender: TObject);
begin
  EE(nil, FormEditRowStack, meRows, 'matrixrows');
end;

procedure TFormMatrixCoords.btEditStackClick(Sender: TObject);
begin
  EE(nil, FormEditRowStack, meStacks, 'matrixstacks');
end;

procedure TFormMatrixCoords.Init;
begin
  inherited;
  meRows.Open;
  self.formEdit := FormEditMatrixSector;
  tablename := 'matrixsectors';
end;


procedure TFormMatrixCoords.meDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  meDataAfterScroll(DataSet);

end;

procedure TFormMatrixCoords.meDataAfterPost(DataSet: TDataSet);
begin
  inherited;
  meDataAfterScroll(DataSet);
end;

procedure TFormMatrixCoords.meDataAfterScroll(DataSet: TDataSet);
begin

  inherited;

  drvRows.SelectCommand.Parameters.ParamByName('sector_id').Value := meData.FieldByName('id').AsInteger;
  meRows.Close;
  meRows.Open;

  drvStacks.SelectCommand.Parameters.ParamByName('sector_id').Value := meData.FieldByName('id').AsInteger;
  meStacks.Close;
  meStacks.Open;

  drvExcludes.SelectCommand.Parameters.ParamByName('sector_id').Value := meData.FieldByName('id').AsInteger;
  meExcludes.Close;
  meExcludes.Open;

end;

procedure TFormMatrixCoords.meExcludesAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('sector_id').Value := meData.FieldByName('id').AsInteger;
end;

procedure TFormMatrixCoords.meRowsAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('sector_id').Value := meData.FieldByName('id').AsInteger;
end;

procedure TFormMatrixCoords.sbAddExclClick(Sender: TObject);
begin
  EA(nil, FormEditMatrixExclude, meExcludes, 'matrixexcludes');
end;

procedure TFormMatrixCoords.sdDelExclClick(Sender: TObject);
begin
  ED(meExcludes);
end;

procedure TFormMatrixCoords.sdEditExclClick(Sender: TObject);
begin
  EE(nil, FormEditMatrixExclude, meExcludes, 'matrixexcludes');
end;

end.
