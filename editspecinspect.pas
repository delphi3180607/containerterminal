﻿unit editspecinspect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEh, DBCtrlsEh, Vcl.StdCtrls,
  Vcl.Mask, DBLookupEh, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormEditSpecInspect = class(TFormEdit)
    cbTechCond: TDBLookupComboboxEh;
    Label1: TLabel;
    Label2: TLabel;
    edNote: TDBEditEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEditSpecInspect: TFormEditSpecInspect;

implementation

{$R *.dfm}

end.
