﻿unit PassTypeSelect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons;

type
  TFormPassTypeSelect = class(TFormEdit)
    sbType0: TSpeedButton;
    sbType1: TSpeedButton;
    procedure sbType0Click(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    passType: integer;
  end;

var
  FormPassTypeSelect: TFormPassTypeSelect;

implementation

{$R *.dfm}

procedure TFormPassTypeSelect.btnOkClick(Sender: TObject);
begin
  sbType0Click(nil);
  inherited;
end;

procedure TFormPassTypeSelect.sbType0Click(Sender: TObject);
begin
  self.passType := -1;
  if sbType0.Down then self.passType := 0;
  if sbType1.Down then self.passType := 1;
end;

end.
