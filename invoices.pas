﻿unit Invoices;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand, EXLReport;

type
  TFormInvoices = class(TFormGrid)
    Panel1: TPanel;
    dgSpec: TDBGridEh;
    plTool: TPanel;
    sbAddState: TPngSpeedButton;
    sbDelState: TPngSpeedButton;
    sbEditState: TPngSpeedButton;
    Splitter1: TSplitter;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormInvoices: TFormInvoices;

implementation

{$R *.dfm}

end.
