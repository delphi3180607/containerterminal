﻿inherited FormObjectsInProcess: TFormObjectsInProcess
  ActiveControl = nil
  Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088#1099' '#1074' '#1088#1072#1073#1086#1090#1077
  PixelsPerInch = 96
  TextHeight = 13
  inherited SplitterVert: TSplitter
    Visible = False
  end
  inherited plAll: TPanel
    inherited SplitterHor: TSplitter
      Visible = False
    end
    inherited plTop: TPanel
      inherited plCount: TPanel
        inherited edCount: TDBEditEh
          ControlLabel.ExplicitLeft = 0
          ControlLabel.ExplicitTop = -16
        end
      end
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
    inherited plLinkedObjects: TPanel
      inherited dgLinkedObjects: TDBGridEh
        Visible = False
      end
    end
    inherited plSearch: TPanel
      inherited Label1: TLabel
        Height = 14
      end
    end
  end
  inherited plLeft: TPanel
    Visible = False
  end
  inherited IL: TPngImageList
    Bitmap = {}
  end
end
