﻿inherited FormFilter: TFormFilter
  Caption = #1060#1080#1083#1100#1090#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
  ClientHeight = 497
  ClientWidth = 696
  Font.Height = -11
  ExplicitWidth = 702
  ExplicitHeight = 525
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 5
    Top = 9
    Width = 121
    Height = 13
    Caption = #1055#1077#1088#1080#1086#1076' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
  end
  object Label3: TLabel [1]
    Left = 5
    Top = 93
    Width = 192
    Height = 13
    Caption = #1053#1086#1084#1077#1088#1072' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074' ('#1087#1083#1072#1090#1092#1086#1088#1084')'
  end
  object cbClear: TSpeedButton [2]
    Left = 202
    Top = 89
    Width = 81
    Height = 22
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = cbClearClick
  end
  inherited plBottom: TPanel
    Top = 456
    Width = 696
    ExplicitTop = 456
    ExplicitWidth = 696
    inherited btnCancel: TButton
      Left = 580
      Font.Height = -11
      ParentFont = False
      ExplicitLeft = 580
    end
    inherited btnOk: TButton
      Left = 461
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      Font.Height = -11
      OnClick = btnOkClick
      ExplicitLeft = 461
    end
    object btClear: TButton
      AlignWithMargins = True
      Left = 342
      Top = 3
      Width = 113
      Height = 35
      Align = alRight
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btClearClick
    end
  end
  object dtBeginDate: TDBDateTimeEditEh [4]
    Left = 5
    Top = 28
    Width = 124
    Height = 22
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object dtEndDate: TDBDateTimeEditEh [5]
    Left = 146
    Top = 28
    Width = 121
    Height = 22
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object meNumbers: TMemo [6]
    Left = 5
    Top = 112
    Width = 681
    Height = 332
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 4
    OnChange = meNumbersChange
  end
  object cbConfirmedOnly: TDBCheckBoxEh [7]
    Left = 341
    Top = 31
    Width = 233
    Height = 17
    Caption = #1058#1086#1083#1100#1082#1086' '#1087#1088#1086#1074#1077#1076#1077#1085#1085#1099#1077
    DynProps = <>
    TabOrder = 3
  end
  object brTerm: TButton [8]
    Left = 5
    Top = 51
    Width = 262
    Height = 25
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1090#1077#1082#1091#1097#1080#1081
    TabOrder = 5
    OnClick = brTermClick
  end
  inherited dsLocal: TDataSource
    Left = 664
    Top = 336
  end
  inherited qrAux: TADOQuery
    Left = 672
    Top = 432
  end
end
