﻿inherited FormEmailAccounts: TFormEmailAccounts
  Caption = #1059#1095#1077#1090#1085#1099#1077' '#1079#1072#1087#1080#1089#1080' '#1101#1083#1077#1082#1090#1088#1086#1085#1085#1086#1081' '#1087#1086#1095#1090#1099
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    inherited btnOk: TButton
      Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100
    end
  end
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      AllowedOperations = []
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'email_sender'
          Footers = <>
          Title.Caption = #1040#1076#1088#1077#1089
          Width = 241
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'server_name'
          Footers = <>
          Title.Caption = #1057#1077#1088#1074#1077#1088
          Width = 196
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'ssl_check'
          Footers = <>
          Title.Caption = #1047#1072#1097#1080#1090#1072' SSL'
          Width = 111
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select * from emailaccounts')
    UpdateCommand.CommandText.Strings = (
      'update emailaccounts set'
      'server_name = :server_name,'
      'ssl_check = :ssl_check,'
      'port_number = :port_number,'
      'email_sender = :email_sender,'
      'password = :password'
      'where id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'server_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'ssl_check'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'port_number'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'email_sender'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'password'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into emailaccounts '
      '('
      'server_name,'
      'ssl_check,'
      'port_number,'
      'email_sender,'
      'password'
      ')'
      'values'
      '('
      ':server_name,'
      ':ssl_check,'
      ':port_number,'
      ':email_sender,'
      ':password'
      ')')
    InsertCommand.Parameters = <
      item
        Name = 'server_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'ssl_check'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'port_number'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'email_sender'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'password'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from emailaccounts where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select * from emailaccounts where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
end
