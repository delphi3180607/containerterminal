﻿inherited FormUsers: TFormUsers
  ActiveControl = dgData
  Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1080
  ClientWidth = 694
  ExplicitWidth = 710
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Width = 694
    ExplicitWidth = 694
    inherited btnOk: TButton
      Left = 459
      ExplicitLeft = 459
    end
    inherited btnCancel: TButton
      Left = 578
      ExplicitLeft = 578
    end
  end
  inherited plAll: TPanel
    Width = 694
    ExplicitWidth = 694
    inherited plTop: TPanel
      Width = 694
      ExplicitWidth = 694
      inherited btTool: TPngSpeedButton
        Left = 642
        ExplicitLeft = 642
      end
    end
    inherited dgData: TDBGridEh
      Width = 694
      Columns = <
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isblocked'
          Footers = <>
          Title.Caption = #1047#1072#1073#1083#1086#1082#1080#1088#1086#1074#1072#1085
          Width = 109
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'user_login'
          Footers = <>
          Title.Caption = #1051#1086#1075#1080#1085
          Width = 118
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'user_name'
          Footers = <>
          Title.Caption = #1048#1084#1103
          Width = 170
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'role_name'
          Footers = <>
          Title.Caption = #1056#1086#1083#1100
          Width = 157
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isAdmin'
          Footers = <>
          Title.Caption = #1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1086#1088
          Width = 109
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'IsSuperVisor'
          Footers = <>
          Title.Caption = #1057#1091#1087#1077#1088#1074#1080#1079#1086#1088
          Width = 91
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'short_name'
          Footers = <>
          Title.Caption = #1048#1084#1103' '#1082#1088#1072#1090#1082#1086
          Width = 137
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'post_name'
          Footers = <>
          Title.Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
          Width = 127
        end>
    end
    inherited plHint: TPanel
      inherited lbText: TLabel
        Width = 185
        Height = 49
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'select u.*, '
      '(select name from roles r where r.id = u.user_role) as role_name'
      'from users u '
      'order by user_login')
    UpdateCommand.CommandText.Strings = (
      'update users set '
      'user_login = :user_login, '
      'user_name = :user_name, '
      'password = :password, '
      'user_role = :user_role,'
      'post_name = :post_name,'
      'short_name = :short_name,'
      'contact = :contact,'
      'isblocked = :isblocked,'
      'isadmin = :isadmin,'
      'issupervisor = :issupervisor'
      'where id = :id'
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'user_login'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'user_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'password'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'user_role'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'post_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'short_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'isblocked'
        Size = -1
        Value = Null
      end
      item
        Name = 'isadmin'
        Size = -1
        Value = Null
      end
      item
        Name = 'issupervisor'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      
        'insert into users (user_login, user_name, password, user_role, i' +
        'sadmin, post_name, contact, short_name, issupervisor)'
      
        'values ( :user_login, :user_name, :password, :user_role, :isadmi' +
        'n, :post_name, :contact, :short_name, :issupervisor)')
    InsertCommand.Parameters = <
      item
        Name = 'user_login'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'user_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'password'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'user_role'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isadmin'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'post_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'contact'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'short_name'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'issupervisor'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from users where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select u.*, '
      '(select name from roles r where r.id = u.user_role) as role_name'
      'from users u where u.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
  end
end
