﻿unit orders;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, docflow, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, Functions, Vcl.Mask, Vcl.DBCtrls, DBCtrlsEh, System.ImageList,
  Vcl.ImgList, PngImageList;

type
  TFormDocOrders = class(TFormDocFlow)
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    plSpec: TPanel;
    plToolSpec: TPanel;
    sbAddSpec: TPngSpeedButton;
    sbDeleteSpec: TPngSpeedButton;
    sbEditSpec: TPngSpeedButton;
    dgSpec: TDBGridEh;
    edNote: TDBEdit;
    pmSpec: TPopupMenu;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    Splitter1: TSplitter;
    sbConfirmSpec: TPngSpeedButton;
    Bevel8: TBevel;
    sbCancelConfirmSpec: TPngSpeedButton;
    sbMove: TPngSpeedButton;
    N24: TMenuItem;
    sbExcelSpec: TPngSpeedButton;
    sbFilter: TPngSpeedButton;
    sbResetFilter: TPngSpeedButton;
    sbPassSpec: TPngSpeedButton;
    sbNote: TPngSpeedButton;
    procedure sbAddSpecClick(Sender: TObject);
    procedure sbDeleteSpecClick(Sender: TObject);
    procedure sbEditSpecClick(Sender: TObject);
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure meDataAfterPost(DataSet: TDataSet);
    procedure meDataBeforeEdit(DataSet: TDataSet);
    procedure meSpecBeforeEdit(DataSet: TDataSet);
    procedure meDataAfterInsert(DataSet: TDataSet);
    procedure sbImport2Click(Sender: TObject);
    procedure sbConfirmClick(Sender: TObject);
    procedure sbCancelConfirmClick(Sender: TObject);
    procedure btFlowClick(Sender: TObject);
    procedure meDataBeforeInsert(DataSet: TDataSet);
    procedure N15Click(Sender: TObject);
    procedure meDataBeforePost(DataSet: TDataSet);
    procedure meSpecBeforeInsert(DataSet: TDataSet);
    procedure N17Click(Sender: TObject);
    procedure dgDataApplyFilter(Sender: TObject);
    procedure meSpecAfterInsert(DataSet: TDataSet);
    procedure sbConfirmSpecClick(Sender: TObject);
    procedure sbCancelConfirmSpecClick(Sender: TObject);
    procedure dgSpecDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure meDataAfterOpen(DataSet: TDataSet);
    procedure dgSpecSelectionChanged(Sender: TObject);
    procedure dgSpecMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure sbMoveClick(Sender: TObject);
    procedure N24Click(Sender: TObject);
    procedure btExcelClick(Sender: TObject);
    procedure meSpecBeforeOpen(DataSet: TDataSet);
    procedure sbFilterClick(Sender: TObject);
    procedure sbResetFilterClick(Sender: TObject);
    procedure sbPassSpecClick(Sender: TObject);
    procedure dgSpecColumns0CellDataLinkClick(Grid: TCustomDBGridEh;
      Column: TColumnEh);
    procedure sbPassClick(Sender: TObject);
    procedure sbNoteClick(Sender: TObject);
    procedure dgSpecApplyFilter(Sender: TObject);
    procedure dgSpecTitleBtnClick(Sender: TObject; ACol: Integer;
      Column: TColumnEh);
    procedure sbRestrictionsClick(Sender: TObject);
  private
    oldcargotype: Variant;
    oldspecorder: string;
    specguid: string;
  protected
    procedure SetReadOnly; override;
    procedure AssignSelField(colname: string); override;
  public
    procedure Init; override;
    procedure ResetSpecFilter;
    procedure SetSpecFilter;
  end;

var
  FormDocOrders: TFormDocOrders;

implementation

{$R *.dfm}

uses editorder, editorderspec, deliverytypes, importload, ObjectStatesHistory,
  editordernumber, dateexecution, dmu, main, MoveOrderSpec, filterlite, Passes,
  EditNote, EditOrderInWait;

procedure TFormDocOrders.btExcelClick(Sender: TObject);
begin
  if fQYN('Экспортировать список в Excel ?') then
    ExportExcel(dgSpec, self.Caption);
end;

procedure TFormDocOrders.btFlowClick(Sender: TObject);
begin
  inherited;
  meDataAfterScroll(meData);
end;

procedure TFormDocOrders.dgDataApplyFilter(Sender: TObject);
begin
  inherited;
  meDataAfterScroll(meData);
end;

procedure TFormDocOrders.dgSpecApplyFilter(Sender: TObject);
begin
  dgSpec.DefaultApplyFilter;
  RestoreSort(dgSpec);
end;

procedure TFormDocOrders.dgSpecColumns0CellDataLinkClick(Grid: TCustomDBGridEh;
  Column: TColumnEh);
begin
  sbPassSpecClick(nil);
end;

procedure TFormDocOrders.dgSpecDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

  if not ((dgSpec.Columns[dgSpec.Col-1].FieldName = 'external_order_num') and (gdSelected in State))
  then
  if Column.FieldName = 'external_order_num' then
  if meSpec.FieldByName('external_order_signed').AsBoolean then
  begin
    dgSpec.Canvas.Brush.Color := clLime;
  end;

  dgSpec.DefaultDrawColumnCell(Rect,DataCol,Column,State);


  if Column.FieldName = 'isconfirmed' then
   if meSpec.FieldByName('state_code').AsString <> '' then
   begin

     if meData.FieldByName('isabolished').AsBoolean then
      dgSpec.Canvas.Brush.Color := $000E2F43
     else
      dgSpec.Canvas.Brush.Color := $000CA570;

      dgSpec.Canvas.Font.Color := clGreen;

      dgSpec.Canvas.Pen.Style := psClear;
      dgSpec.Canvas.Rectangle(Rect.Left+5,Rect.Top+2,Rect.Left+24,Rect.Top+14);
      dgSpec.Canvas.Brush.Color := clWindow;
      dgSpec.Canvas.Pen.Style := psSolid;

     if meSpec.FieldDefs.IndexOf('stop_sign') > -1 then
     if meSpec.FieldByName('stop_sign').AsBoolean then
     begin
      IL.Draw(dgSpec.Canvas, Rect.Left+16, Rect.Top, 0, true);
     end;

   end;


  if gdSelected in State then begin

    dgSpec.Canvas.Pen.Color := $00D2D2D2;

    if dgSpec.Selection.SelectionType = (gstRectangle) then
    if (Column.Index = dgSpec.Col-1)then
      dgSpec.Canvas.Pen.Color := clBlue;

    dgSpec.Canvas.Brush.Style := bsClear;
    dgSpec.Canvas.Font.Style := Column.Font.Style;
    dgSpec.Canvas.Pen.Width := 1;
    dgSpec.Canvas.Rectangle(Rect.Left, Rect.Top, Rect.Right, Rect.Bottom);

  end;

end;

procedure TFormDocOrders.dgSpecMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin

  if selfield = 'external_order_num' then
  begin
    allow_edit := true;
    meSpec.ReadOnly := false;

    dgDataMouseUp(Sender, Button, Shift, X, Y);

    if (meData.FieldByName('isconfirmed').AsInteger = 1) then meSpec.ReadOnly := true;
    allow_edit := false;
  end;

end;

procedure TFormDocOrders.dgSpecSelectionChanged(Sender: TObject);
begin

  if dgSpec.Selection.SelectionType = (gstRectangle) then
  begin
    update_rectangle := true;
  end else
  begin
    update_rectangle := false;
  end;

end;

procedure TFormDocOrders.dgSpecTitleBtnClick(Sender: TObject; ACol: Integer; Column: TColumnEh);
begin
  if Column.Title.SortMarker = smNoneEh then
    Column.Title.SortMarker := smDownEh
  else if Column.Title.SortMarker = smDownEh then
    Column.Title.SortMarker := smUpEh
  else if Column.Title.SortMarker = smUpEh then
    Column.Title.SortMarker := smNoneEh;
  RestoreSort(dgSpec);
end;

procedure TFormDocOrders.Init;
begin
  self.system_section := 'outcome_apps';
  inherited;
  self.formEdit := FormEditOrder;
  self.meSpec.Open;
end;

procedure TFormDocOrders.meDataAfterInsert(DataSet: TDataSet);
begin
  inherited;
  meData.FieldByName('doc_date').Value := now();
  meData.FieldByName('begin_date').Value := now();
  meData.FieldByName('end_date').Value := now()+90;
end;

procedure TFormDocOrders.meDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  meDataAfterScroll(DataSet);
end;

procedure TFormDocOrders.meDataAfterPost(DataSet: TDataSet);
begin

  inherited;

  if (meSpec.State = dsInsert) then
  begin
    meSpec.FieldByName('doc_id').Value := DataSet.FieldByName('id').AsInteger;
    meSpec.Post;
  end;

end;

procedure TFormDocOrders.meDataAfterScroll(DataSet: TDataSet);
begin

  //сначала сброс фильтра
  ResetSpecFilter;

  CreateGuid(gu);
  specguid := '';

  FullRefresh(meSpec);
  inherited;
end;


procedure TFormDocOrders.ResetSpecFilter;
begin

  dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
  dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := specguid;
  dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := '';
  dm.spCreateFilter.ExecProc;
  specguid := '';
  sbFilter.Down := false;

  FullRefresh(meSpec);

end;


procedure TFormDocOrders.SetSpecFilter;
begin

    if FormFilterLite.conditions = '' then
    begin
      ResetSpecFilter;
      exit;
    end;

    //установка фильтра
    CreateGuid(gu);
    specguid := GuidToString(gu);

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := specguid;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := FormFilterLite.conditions;
    dm.spCreateFilter.ExecProc;
    sbFilter.Down := true;

    meSpec.Close;
    meSpec.Open;

end;

procedure TFormDocOrders.SetReadOnly;
begin

  inherited;

  meSpec.ReadOnly := false;

  if pos(self.system_section,FormMain.readonly_sections)>0 then
  begin
    meSpec.ReadOnly := true;
    exit;
  end;

  if (meData.FieldByName('isabolished').AsBoolean) then
  begin
    meSpec.ReadOnly := true;
    exit;
  end;

  if pos(self.system_section,FormMain.readonly_sections)>0 then
  begin
    meSpec.ReadOnly := true;
    exit;
  end;

  if allow_edit then
  begin
    meSpec.ReadOnly := false;
    exit;
  end;

  if (meData.FieldByName('isconfirmed').AsInteger = 1) then meSpec.ReadOnly := true;

  if meData.FieldByName('isdeleted').AsBoolean then
    meSpec.ReadOnly := true;

   if meSpec.FieldByName('state_code').AsString = '' then
    meSpec.ReadOnly := false;


end;


procedure TFormDocOrders.meDataBeforeEdit(DataSet: TDataSet);
begin

  if meData.FieldByName('uplproc').AsFloat = 0 then allow_edit := true;

  inherited;
  //FormDeliveryTypes.meData.Filter := 'global_section=''output''';
  //FormDeliveryTypes.meData.Filtered := true;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select global_section from deliverytypes where id='+IntToStr(meData.FieldByName('dlv_type_id').AsInteger));
  qrAux.Open;

  if qrAux.Fields[0].AsString = 'output'
  then begin
    meData.FieldByName('consignee_id').Required := true;
    meData.FieldByName('station_id').Required := true;
    //meData.FieldByName('tarif_type_id').Required := true;
  	meData.FieldByName('shippingoption_id').Required := true;
  end else
  begin
    meData.FieldByName('consignee_id').Required := false;
    meData.FieldByName('station_id').Required := false;
    //meData.FieldByName('tarif_type_id').Required := false;
  	meData.FieldByName('shippingoption_id').Required := false;
  end;

  meData.FieldByName('dlv_type_id').Required := true;
	meData.FieldByName('forwarder_id').Required := true;
	meData.FieldByName('payer_id').Required := true;

	meSpec.FieldByName('cargotype_id').Required := not meSpec.FieldByName('isempty').AsBoolean;

end;

procedure TFormDocOrders.meDataBeforeInsert(DataSet: TDataSet);
begin
  inherited;

  meData.FieldByName('dlv_type_id').Required := true;
	meData.FieldByName('forwarder_id').Required := true;
	meData.FieldByName('consignee_id').Required := true;
	meData.FieldByName('station_id').Required := true;
	meData.FieldByName('payer_id').Required := true;
  meData.FieldByName('shippingoption_id').Required := false;

	meSpec.FieldByName('cargotype_id').Required := not meSpec.FieldByName('isempty').AsBoolean;

end;

procedure TFormDocOrders.meDataBeforePost(DataSet: TDataSet);
begin
  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select dt.id from doctypes dt, deliverytypes dl where dt.system_section = '''+self.system_section+'''');
  qrAux.SQL.Add('and charindex(dl.global_section,dt.global_section)>0 and dl.id = '+self.meData.FieldByName('dlv_type_id').AsString);
  qrAux.SQL.Add('order by len(dt.global_section)');
  qrAux.Open;
  self.active_doctype_id := qrAux.FieldByName('id').AsInteger;
  inherited;
end;

procedure TFormDocOrders.meSpecAfterInsert(DataSet: TDataSet);
begin
  inherited;
  meSpec.FieldByName('cargotype_id').Value := oldcargotype;
	meSpec.FieldByName('doc_id').Value := meData.FieldByName('id').AsInteger;
end;

procedure TFormDocOrders.meSpecBeforeEdit(DataSet: TDataSet);
begin
  inherited;

  meSpec.ReadOnly := false;

  if meSpec.FieldByName('state_code').AsString = '' then meSpec.ReadOnly := false;

  if meData.FieldByName('isdeleted').AsBoolean then meSpec.ReadOnly := true;

	meSpec.FieldByName('cargotype_id').Required := not meSpec.FieldByName('isempty').AsBoolean;

  SetReadOnly;

end;

procedure TFormDocOrders.meSpecBeforeInsert(DataSet: TDataSet);
begin
  meSpec.FieldByName('cargotype_id').Required := not meSpec.FieldByName('isempty').AsBoolean;
  oldcargotype := null;
  if meSpec.Active then oldcargotype := meSpec.FieldByName('cargotype_id').Value;
end;

procedure TFormDocOrders.meSpecBeforeOpen(DataSet: TDataSet);
begin
  drvSpec.SelectCommand.Parameters.ParamByName('doc_id').Value := meData.FieldByName('id').AsInteger;
  drvSpec.SelectCommand.Parameters.ParamByName('guid').Value := specguid;
end;

procedure TFormDocOrders.N15Click(Sender: TObject);
begin
  FormObjectStatesHistory.meLinkedObjects.Close;
  FormObjectStatesHistory.meData.Close;
  FormObjectStatesHistory.drvData.SelectCommand.Parameters.ParamByName('object_id').Value := meSpec.FieldByName('container_id').AsInteger;
  FormObjectStatesHistory.meData.Open;
  FormObjectStatesHistory.ShowModal;
  Screen.Cursor := crDefault;
end;

procedure TFormDocOrders.N17Click(Sender: TObject);
begin
  if meData.FieldByName('isdeleted').AsBoolean then exit;
  allow_edit := true;
  meSpec.ReadOnly := false;
  EE(nil, FormEditOrderNumber, meSpec, 'docorderspec');
  if (meData.FieldByName('isconfirmed').AsInteger = 1) then meSpec.ReadOnly := true;
  allow_edit := false;
end;

procedure TFormDocOrders.N24Click(Sender: TObject);
begin
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormDocOrders.sbAddSpecClick(Sender: TObject);
begin

  if meData.FieldByName('isdeleted').AsBoolean then exit;

  if (meData.FieldByName('isabolished').AsBoolean) then
  begin
    ShowMessage('Документ аннулирован, изменения запрещены.');
    exit;
  end;

  if pos(self.system_section,FormMain.readonly_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  meSpec.ReadOnly := false;
  allow_edit := true;
  if EA(self, FormEditOrderSpec, meSpec, 'docorderspec', 'doc_id', meData.FieldByName('id').AsInteger) then
  begin
    if meData.FieldByName('isconfirmed').AsInteger = 1 then
    begin
     ShowMessage('Внимание! Вы добавили контейнер в проведенную заявку. ЭТОТ КОНТЕЙНЕР ТЕПЕРЬ НАДО ПРОВЕСТИ ОТДЕЛЬНО!');
    end;
  end;
  allow_edit := false;
  meSpec.Cancel;

end;

procedure TFormDocOrders.sbCancelConfirmClick(Sender: TObject);
begin
  allowcancel := true;
  inherited;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormDocOrders.sbCancelConfirmSpecClick(Sender: TObject);
begin

  if pos(self.system_section,FormMain.confirmrestrict_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  if meSpec.RecordCount < 1 then exit;

  RefreshRecord(meData);
  if meData.FieldByName('isconfirmed').AsInteger = 0 then
  begin
    ShowMessage('Документ не проведен.');
    exit;
  end;

  if meSpec.RecordsView.MemTableData.RecordsList.Count < 2 then
  begin
    ShowMessage('Снять проведение с одной записи нельзя. Нужно распроводить документ.');
    exit;
  end;

  if fQYN('Cнять проведение с записи?') then
  begin
    try
      dm.spCancelConfirmDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
      dm.spCancelConfirmDoc.Parameters.ParamByName('@CargoId').Value := self.meSpec.FieldByName('cargo_id').AsInteger;
      dm.spCancelConfirmDoc.Parameters.ParamByName('@UserId').Value := FormMain.currentUserId;
      dm.spCancelConfirmDoc.ExecProc;
    finally
      self.meDataAfterScroll(meData);
    end;
  end;
end;

procedure TFormDocOrders.sbConfirmClick(Sender: TObject);
begin
  inherited;
  self.meDataAfterScroll(meData);
end;

procedure TFormDocOrders.sbConfirmSpecClick(Sender: TObject);
begin

  if pos(self.system_section,FormMain.confirmrestrict_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;


  RefreshRecord(meData);
  if meData.FieldByName('isconfirmed').AsInteger = 0 then
  begin
    ShowMessage('Проведение записей спецификации возможно только после проведения документа.');
    exit;
  end;

  FormDateExecution.edUserName.Text := FormMain.currentUser;
  FormDateExecution.dtConfirm.Value := now();
  FormDateExecution.dtFactExecution.Value := now();
  FormDateExecution.ShowModal;

  if FormDateExecution.ModalResult = mrOk then
  begin

    try
      dm.spConfirmDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
      dm.spConfirmDoc.Parameters.ParamByName('@SpecId').Value := self.meSpec.FieldByName('id').AsInteger;
      dm.spConfirmDoc.Parameters.ParamByName('@DateFactExecution').Value := FormDateExecution.dtFactExecution.Value;
      dm.spConfirmDoc.Parameters.ParamByName('@UserId').Value := FormMain.currentUserId;
      dm.spConfirmDoc.ExecProc;
    finally
      self.meDataAfterScroll(meData);
    end;

  end;

end;

procedure TFormDocOrders.sbDeleteSpecClick(Sender: TObject);
begin

  if meData.FieldByName('isdeleted').AsBoolean then exit;

  if pos(self.system_section,FormMain.readonly_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  if (meData.FieldByName('isabolished').AsBoolean) then
  begin
    ShowMessage('Документ аннулирован, изменения запрещены.');
    exit;
  end;

  if not (meData.FieldByName('isconfirmed').AsInteger = 1) then
  ED(meSpec)
  else begin
    if fQYN('Документ проведен. Удалить запись?') then
    begin

      dm.spCancelConfirmDoc.Parameters.ParamByName('@DocId').Value := self.meData.FieldByName('id').AsInteger;
      dm.spCancelConfirmDoc.Parameters.ParamByName('@CargoId').Value := self.meSpec.FieldByName('cargo_id').AsInteger;
      dm.spCancelConfirmDoc.Parameters.ParamByName('@UserId').Value := FormMain.currentUserId;
      try
        dm.spCancelConfirmDoc.ExecProc;
      except
        on E: Exception do
        begin
          ShowMessage(E.Message);
          exit;
        end;
      end;
      ED(meSpec);
    end;
  end;

end;

procedure TFormDocOrders.sbEditSpecClick(Sender: TObject);
begin

  SetReadOnly;

  if meSpec.RecordCount>0 then
  EE(self, FormEditOrderSpec, meSpec,'docorderspec')
  else ShowMessage('Нечего редактировать.');

end;

procedure TFormDocOrders.sbFilterClick(Sender: TObject);
begin

  sbFilter.Down := false;

  FormFilterLite.ShowModal;
  if FormFilterLite.ModalResult in [mrOk, mrYes] then
  begin
    ResetSpecFilter;
    SetSpecFilter;
  end;

end;

procedure TFormDocOrders.sbImport2Click(Sender: TObject);
begin
  FormImportLoad.meData.Close;
  FormImportLoad.btnOk.Enabled := false;
  FormImportLoad.pc.ActivePageIndex := 0;
  FormImportLoad.ShowModal;
end;


procedure TFormDocOrders.sbMoveClick(Sender: TObject);
var i: integer;
begin

  if pos(self.system_section,FormMain.readonly_sections)>0 then
  begin
    ShowMessage('У вас нет прав на эту операцию.');
    exit;
  end;

  FormMoveOrderSpec.meData.Close;
  FormMoveOrderSpec.meData.EmptyTable;
  FormMoveOrderSpec.meData.LoadFromDataSet(meSpec,1,lmCopyStructureOnly,false);
  FormMoveOrderSpec.meData.Open;

  for i := 0 to dgSpec.SelectedRows.Count-1 do
  begin

    meSpec.Bookmark := dgSpec.SelectedRows[i];

    FormMoveOrderSpec.meData.LoadFromDataSet(meSpec,1,lmAppend,false);
    FormMoveOrderSpec.meData.Last;
    FormMoveOrderSpec.meData.Edit;
    FormMoveOrderSpec.meData.FieldByName('id').Value := meSpec.FieldByName('id').AsInteger;
    FormMoveOrderSpec.meData.Post;

  end;

  try
    FormMoveOrderSpec.meData.Open;

    if FormMoveOrderSpec.meData.RecordCount=0 then
    begin
      ShowMessage('Вы не выделили (не отметили) ни одной записи для переноса.');
      exit;
    end;

    FormMoveOrderSpec.orderform := self;
    FormMoveOrderSpec.orderid := self.meData.FieldByName('id').AsInteger;
    FormMoveOrderSpec.Show;
  except

  end;

end;

procedure TFormDocOrders.sbNoteClick(Sender: TObject);
begin
  allow_edit := true;
  EE(nil, FormEditNote, meData, 'docorders');
  allow_edit := false;
end;

procedure TFormDocOrders.sbPassClick(Sender: TObject);
begin
  inherited;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormDocOrders.sbPassSpecClick(Sender: TObject);
begin

 if meData.FieldByName('isconfirmed').AsInteger <> 1 then
 begin
   ShowMessage('Нельзя создавать пропуск, если документ не проведен.');
   exit;
 end;

 if meSpec.FieldByName('state_code').AsString = '' then
 begin
   ShowMessage('Нельзя создавать пропуск, если спецификация не проведена.');
   exit;
 end;

  if meData.FieldByName('inwait').AsBoolean then
  begin
    ShowMessage('Заявка на согласовании.');
    exit;
  end;

 FormPasses.Init;
 FormPasses.CreatePassByDoc(meData.FieldByName('id').AsInteger,meSpec.FieldByName('container_id').AsInteger, meSpec.FieldByName('task_id').AsInteger);
 RefreshRecord(meData);
 RefreshRecord(meSpec, meSpec.FieldByName('id').AsInteger);
end;

procedure TFormDocOrders.sbResetFilterClick(Sender: TObject);
begin
  ResetSpecFilter;
end;

procedure TFormDocOrders.sbRestrictionsClick(Sender: TObject);
begin
  //inherited;
  allow_edit := true;
  EE(nil, FormEditOrderInWait, meData, 'docorders');
  allow_edit := false;
end;

procedure TFormDocOrders.AssignSelField(colname: string);
begin

  selfield := '';
  if colname = 'external_order_num' then selfield := 'external_order_num';

end;




end.
