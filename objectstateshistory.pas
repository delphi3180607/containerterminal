﻿unit ObjectStatesHistory;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.Buttons, PngSpeedButton,
  Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh;

type
  TFormObjectStatesHistory = class(TFormGrid)
    Panel3: TPanel;
    DBGridEh1: TDBGridEh;
    Splitter3: TSplitter;
    drvLinkedObjects: TADODataDriverEh;
    meLinkedObjects: TMemTableEh;
    dsLinkedObjects: TDataSource;
    procedure meDataAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormObjectStatesHistory: TFormObjectStatesHistory;

implementation

{$R *.dfm}

procedure TFormObjectStatesHistory.meDataAfterScroll(DataSet: TDataSet);
begin
  meLinkedObjects.Close;
  drvLinkedObjects.SelectCommand.Parameters.ParamByName('object_id').Value := meData.FieldByName('object_id').AsInteger;
  drvLinkedObjects.SelectCommand.Parameters.ParamByName('doc_id').Value := meData.FieldByName('doc_id').AsInteger;
  meLinkedObjects.Open;
end;

end.
