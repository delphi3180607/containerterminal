﻿unit setprefix;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, DBCtrlsEh, DBSQLLookUp;

type
  TFormSetPrefix = class(TFormEdit)
    Label1: TLabel;
    Label2: TLabel;
    edPrefix: TDBEditEh;
    neStartNumber: TDBNumberEditEh;
    Label3: TLabel;
    edKP: TDBEditEh;
    laDlvTypeLoad: TDBSQLLookUp;
    Label4: TLabel;
    laDlvTypeEmpty: TDBSQLLookUp;
    Label5: TLabel;
    ssDlvTypes: TADOLookUpSqlSet;
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSetPrefix: TFormSetPrefix;

implementation

{$R *.dfm}

procedure TFormSetPrefix.btnOkClick(Sender: TObject);
begin
  inherited;
  if self.edKP.Text = '' then
  begin
    ShowMessage('Необходимо что-то написать в поле Номер КП.');
    self.ModalResult := mrNone;
    exit;
  end;
  self.ModalResult := mrOk;
end;

end.
