﻿unit editorder;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, DBGridEh, DBGridEhGrouping,
  ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh,
  PngSpeedButton, DBCtrlsEh, Vcl.StdCtrls, Vcl.Mask, DBLookupEh, Vcl.ExtCtrls,
  Vcl.Buttons, Data.DB, Data.Win.ADODB, MemTableDataEh, MemTableEh,
  DataDriverEh, ADODataDriverEh, functions, Vcl.ComCtrls, DBSQLLookUp,
  Vcl.CheckLst;

type
  TFormEditOrder = class(TFormEdit)
    sbPayer: TSpeedButton;
    Label9: TLabel;
    Label16: TLabel;
    sbForwarder: TSpeedButton;
    Label2: TLabel;
    Label8: TLabel;
    sbConsignee: TSpeedButton;
    Label6: TLabel;
    dtDocDate: TDBDateTimeEditEh;
    Label3: TLabel;
    Label1: TLabel;
    edDocNumber: TDBEditEh;
    sbDirections: TSpeedButton;
    Label13: TLabel;
    edDlvAddress: TDBEditEh;
    Label14: TLabel;
    edDlvContact: TDBEditEh;
    laContract: TDBSQLLookUp;
    Label11: TLabel;
    ssContracts: TADOLookUpSqlSet;
    laPayer: TDBSQLLookUp;
    Label12: TLabel;
    laELSPayer: TDBSQLLookUp;
    SpeedButton1: TSpeedButton;
    laForwarder: TDBSQLLookUp;
    laConsignee: TDBSQLLookUp;
    ssDirections: TADOLookUpSqlSet;
    laDirection: TDBSQLLookUp;
    laTarifType: TDBSQLLookUp;
    ssTarifType: TADOLookUpSqlSet;
    Label17: TLabel;
    Label18: TLabel;
    edExtraServices: TDBEditEh;
    ssDlvTypes: TADOLookUpSqlSet;
    laDlvTypes: TDBSQLLookUp;
    edNote: TDBEditEh;
    laStation: TDBSQLLookUp;
    ssStations: TADOLookUpSqlSet;
    sbStations: TSpeedButton;
    Label10: TLabel;
    laEndPointConsignee: TDBSQLLookUp;
    sbEndPoint: TSpeedButton;
    Label4: TLabel;
    luShippingOptions: TDBSQLLookUp;
    ssShippingOption: TADOLookUpSqlSet;
    cbServices: TCheckListBox;
    qrAux2: TADOQuery;
    cbInWait: TDBCheckBoxEh;
    dtInWaitStop: TDBDateTimeEditEh;
    Label5: TLabel;
    qrAux1: TADOQuery;
    procedure sbForwarderClick(Sender: TObject);
    procedure sbConsigneeClick(Sender: TObject);
    procedure sbPayerClick(Sender: TObject);
    procedure sbDirectionsClick(Sender: TObject);
    procedure sbStationsClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure luPayerChange(Sender: TObject);
    procedure cbServicesClickCheck(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure laDlvTypesKeyValueChange(Sender: TObject);
    procedure laForwarderKeyValueChange(Sender: TObject);
    procedure laEndPointConsigneeKeyValueChange(Sender: TObject);
    procedure sbEndPointClick(Sender: TObject);
    procedure laPayerKeyValueChange(Sender: TObject);
    procedure luShippingOptionsKeyValueChange(Sender: TObject);
    procedure cbInWaitClick(Sender: TObject);
    procedure dtInWaitStopChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    nOldKey: integer;
  public
    s: TPairList;
  end;

var
  FormEditOrder: TFormEditOrder;

implementation

{$R *.dfm}

uses counteragents, selectobjecttype, cargos, carriages, containers, directions, stations,
  dmu;

procedure TFormEditOrder.btnOkClick(Sender: TObject);
begin
  inherited;
  if (dsLocal.DataSet.FieldByName('dlv_type_id').AsString = '') or
  (dsLocal.DataSet.FieldByName('forwarder_id').AsString = '') or
  (dsLocal.DataSet.FieldByName('payer_id').AsString = '') or
  (dsLocal.DataSet.FieldByName('shippingoption_id').Required and (dsLocal.DataSet.FieldByName('shippingoption_id').AsString = '')) or
  (dsLocal.DataSet.FieldByName('tarif_type_id').Required and (dsLocal.DataSet.FieldByName('tarif_type_id').AsString = '')) or
  (dsLocal.DataSet.FieldByName('consignee_id').Required and (dsLocal.DataSet.FieldByName('consignee_id').AsString = '')) or
  (dsLocal.DataSet.FieldByName('station_id').Required and (dsLocal.DataSet.FieldByName('station_id').AsString = ''))
  then
  begin
    ShowMessage('Не заполнены обязательные поля!');
    self.ModalResult := mrNone;
  end;
end;

procedure TFormEditOrder.cbInWaitClick(Sender: TObject);
begin
  if not self.Visible then exit;
  if TMemTableEh(dsLocal.DataSet).ReadOnly then exit;
  if cbInWait.Checked then
  dtInWaitStop.Value := null;
end;

procedure TFormEditOrder.cbServicesClickCheck(Sender: TObject);
var i: integer; s: string;
begin
  s := '';
  for i := 0 to cbServices.Items.Count-1 do
  begin
    if cbServices.Checked[i] then
      s := s+','+cbServices.Items[i];
  end;
  edExtraServices.Value := s;
end;

procedure TFormEditOrder.dtInWaitStopChange(Sender: TObject);
begin
  if not self.Visible then exit;
  if TMemTableEh(dsLocal.DataSet).ReadOnly then exit;
  if dtInWaitStop.Value<>null then
  cbInWait.Checked := false;
end;

procedure TFormEditOrder.FormActivate(Sender: TObject);
var i: integer;
begin

  if dsLocal.DataSet.FieldByName('isconfirmed').AsInteger = 0
    then luShippingOptions.KeyValue := null;

  luShippingOptions.Enabled := true;

  qrAux1.Close;
  qrAux1.Open;
  cbServices.Items.Clear;
  while not qrAux1.Eof do
  begin
    cbServices.Items.Add(qrAux1.FieldByName('scode').AsString);
    qrAux1.Next;
  end;

  for I := 0 to cbServices.Items.Count-1 do
  begin
    if pos(cbServices.Items[i],edExtraServices.Text)>0 then
    cbServices.Checked[i] := true;
  end;

  laPayerKeyValueChange(self);

end;

procedure TFormEditOrder.FormCreate(Sender: TObject);
begin
  inherited;
  nOldKey := 0;
end;

procedure TFormEditOrder.laDlvTypesKeyValueChange(Sender: TObject);
begin

  if VarToInt(laDlvTypes.KeyValue)=0 then
  begin
    if dsLocal.DataSet.FieldByName('isconfirmed').AsInteger = 0
      then luShippingOptions.KeyValue := null;
    luShippingOptions.Enabled := true;
  end;

  if VarToInt(laForwarder.KeyValue)>0 then
    if VarToInt(laPayer.KeyValue)=0 then
      laPayer.KeyValue := laForwarder.KeyValue;

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select global_section from deliverytypes where id='+IntToStr(VarToInt(laDlvTypes.KeyValue)));
  qrAux.Open;

  if qrAux.Fields[0].AsString = 'output'
  then begin
    TMemTableEh(dsLocal.DataSet).FieldByName('consignee_id').Required := true;
    TMemTableEh(dsLocal.DataSet).FieldByName('station_id').Required := true;
    TMemTableEh(dsLocal.DataSet).FieldByName('shippingoption_id').Required := true;
    //TMemTableEh(dsLocal.DataSet).FieldByName('tarif_type_id').Required := true;
  end else
  begin
    TMemTableEh(dsLocal.DataSet).FieldByName('consignee_id').Required := false;
    TMemTableEh(dsLocal.DataSet).FieldByName('station_id').Required := false;
    TMemTableEh(dsLocal.DataSet).FieldByName('shippingoption_id').Required := false;
    //TMemTableEh(dsLocal.DataSet).FieldByName('tarif_type_id').Required := false;
  end;

  laPayerKeyValueChange(self);
  luShippingOptionsKeyValueChange(self);

  laConsignee.Refresh;
  laStation.Refresh;
  luShippingOptions.Refresh;
  laTarifType.Refresh;

end;

procedure TFormEditOrder.laEndPointConsigneeKeyValueChange(Sender: TObject);
begin
  self.edDlvAddress.Text := dm.ssCounteragents.ListDataSet.FieldByName('location').AsString;
  self.edDlvContact.Text := dm.ssCounteragents.ListDataSet.FieldByName('contacts').AsString;
end;

procedure TFormEditOrder.laForwarderKeyValueChange(Sender: TObject);
begin
  if VarToInt(laForwarder.KeyValue)>0 then
    if ((VarToInt(laPayer.KeyValue)=0) or (VarToInt(laPayer.KeyValue)=nOldKey)) then
    begin
      laPayer.KeyValue := laForwarder.KeyValue;
      nOldKey := VarToInt(laForwarder.KeyValue);
    end;
end;

procedure TFormEditOrder.laPayerKeyValueChange(Sender: TObject);
begin

  self.luShippingOptions.Enabled := true;

  if (dsLocal.DataSet.FieldByName('shippingoption_id').Required) then
  begin

    qrAux2.Close;
    qrAux2.Parameters.ParamByName('direction_code').Value := self.laDirection.DisplayValue;
    qrAux2.Parameters.ParamByName('station_code').Value := self.laStation.DisplayValue;
    qrAux2.Parameters.ParamByName('payer_code').Value := self.laPayer.DisplayValue;
    qrAux2.Parameters.ParamByName('extra_services').Value := self.edExtraServices.Text;
    qrAux2.Open;

    if qrAux2.Fields[0].AsInteger = 0 then
    begin
      if dsLocal.DataSet.FieldByName('isconfirmed').AsInteger = 0
        then luShippingOptions.KeyValue := null;
      self.luShippingOptions.Enabled := true;
    end else
    begin
      self.luShippingOptions.KeyValue := qrAux2.Fields[0].AsInteger;
      self.luShippingOptions.Enabled := false;
    end;

    self.luShippingOptions.Refresh;

  end;

end;

procedure TFormEditOrder.luPayerChange(Sender: TObject);
begin
  ssContracts.Parameters.SetPairValue('customerid', VarToStr(laPayer.Value));
end;

procedure TFormEditOrder.luShippingOptionsKeyValueChange(Sender: TObject);
begin

  if (dsLocal.DataSet.FieldByName('shippingoption_id').Required) then
  begin

    if self.luShippingOptions.KeyValue = null then
    begin
        self.luShippingOptions.Color := $008080FF;
    end else
    begin
      self.luShippingOptions.Color := clMoneyGreen;
    end;

  end else
  begin
     self.luShippingOptions.Color := clWindow;
     self.luShippingOptions.Enabled := true;
  end;

  self.luShippingOptions.Refresh;

end;

procedure TFormEditOrder.sbConsigneeClick(Sender: TObject);
begin
  SFDE(TFormCounteragents,self.laConsignee);
end;

procedure TFormEditOrder.sbDirectionsClick(Sender: TObject);
begin
  SFDE(TFormDirections,self.laDirection);
end;

procedure TFormEditOrder.sbEndPointClick(Sender: TObject);
begin
  SFDE(TFormCounteragents,self.laEndPointConsignee);
end;

procedure TFormEditOrder.sbForwarderClick(Sender: TObject);
begin
  SFDE(TFormCounteragents,self.laForwarder);
end;

procedure TFormEditOrder.sbPayerClick(Sender: TObject);
begin
  SFDE(TFormCounteragents,self.laPayer);
end;

procedure TFormEditOrder.sbStationsClick(Sender: TObject);
begin
  SFDE(TFormStations,self.laStation);
end;

end.
