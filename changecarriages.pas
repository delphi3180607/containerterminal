﻿unit ChangeCarriages;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Data.DB, Data.Win.ADODB,
  Vcl.StdCtrls, Vcl.ExtCtrls, DBSQLLookUp, Vcl.Mask, DBCtrlsEh;

type
  TFormChangeCarriages = class(TFormEdit)
    ssCarriages: TADOLookUpSqlSet;
    laCarriage1: TDBSQLLookUp;
    laCarriage2: TDBSQLLookUp;
    laCarriageNew: TDBSQLLookUp;
    cbNew: TCheckBox;
    ssCarFromLoad: TADOLookUpSqlSet;
    procedure cbNewClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormChangeCarriages: TFormChangeCarriages;

implementation

{$R *.dfm}

procedure TFormChangeCarriages.cbNewClick(Sender: TObject);
begin
  if cbNew.Checked then
  begin
    laCarriage2.KeyValue := null;
    laCarriage2.Enabled := false;
    laCarriageNew.Enabled := true;
  end else
  begin
    laCarriage2.Enabled := true;
    laCarriageNew.KeyValue := null;
    laCarriageNew.Enabled := false;
  end;
end;

end.
