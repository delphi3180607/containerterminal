﻿inherited FormDocLetters: TFormDocLetters
  Caption = #1055#1080#1089#1100#1084#1072
  PixelsPerInch = 96
  TextHeight = 13
  inherited plAll: TPanel
    inherited dgData: TDBGridEh
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'dateregister'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 99
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'object_num'
          Footers = <>
          Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
          Width = 108
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'sender_name'
          Footers = <>
          Title.Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          Width = 164
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'text'
          Footers = <>
          Title.Caption = #1058#1077#1082#1089#1090
          Width = 509
        end>
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      'declare @docid int;'
      ''
      'select @docid = :doc_id;'
      ''
      'select l.*,'
      
        '(select name from counteragents c where c.id = l.sender_id) as s' +
        'ender_name,'
      
        '(select cnum from v_objects where id = l.object_id) as object_nu' +
        'm '
      'from doccargoletters l '
      
        'where (doc_id = @docid or task_id in (select task_id from object' +
        's2docspec where doc_id = @docid))'
      'order by dateregister ')
    SelectCommand.Parameters = <
      item
        Name = 'doc_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update doccargoletters'
      'set'
      '  dateregister = :dateregister,'
      '  sender_id = :sender_id,'
      '  text = :text'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'dateregister'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'sender_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'text'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2000
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into doccargoletters'
      '  (doc_id, dateregister, sender_id, text)'
      'values'
      '  (:doc_id, :dateregister, :sender_id, :text)')
    InsertCommand.Parameters = <
      item
        Name = 'doc_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dateregister'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'sender_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'text'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2000
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from doccargoletters where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select l.*,'
      
        '(select name from counteragents c where c.id = l.sender_id) as s' +
        'ender_name,'
      
        '(select cnum from v_objects where id = l.object_id) as object_nu' +
        'm '
      'from doccargoletters l where id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
  end
end
