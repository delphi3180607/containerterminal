﻿unit Passes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, grid, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  EXLReportExcelTLB, EXLReportBand, EXLReport, System.Actions, Vcl.ActnList,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.StdCtrls, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Mask, DBCtrlsEh, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, Vcl.ComCtrls;

type
  TFormPasses = class(TFormGrid)
    plSpec: TPanel;
    Splitter1: TSplitter;
    drvSpec: TADODataDriverEh;
    meSpec: TMemTableEh;
    dsSpec: TDataSource;
    sbReport: TPngSpeedButton;
    qrReport: TADOQuery;
    pcSpec: TPageControl;
    TabSheet1: TTabSheet;
    dgSpec: TDBGridEh;
    plToolSpec: TPanel;
    sbAddSpec: TPngSpeedButton;
    sbDeleteSpec: TPngSpeedButton;
    Bevel8: TBevel;
    sbExcelSpec: TPngSpeedButton;
    Прочее: TTabSheet;
    Panel1: TPanel;
    sbAddSpec1: TPngSpeedButton;
    sbDeleteSpec1: TPngSpeedButton;
    Bevel1: TBevel;
    sbExcel1: TPngSpeedButton;
    dgSpec1: TDBGridEh;
    sbEditSpec1: TPngSpeedButton;
    btFlow: TPngSpeedButton;
    sbBarrel: TPngSpeedButton;
    Panel6: TPanel;
    dtStart: TDBDateTimeEditEh;
    btRequery: TButton;
    sbClearFilter: TPngSpeedButton;
    plStatus: TPanel;
    sbEditSpec: TPngSpeedButton;
    sbClock: TPngSpeedButton;
    procedure sbReportClick(Sender: TObject);
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure meDataAfterOpen(DataSet: TDataSet);
    procedure sbDeleteSpecClick(Sender: TObject);
    procedure sbAddSpecClick(Sender: TObject);
    procedure meDataAfterInsert(DataSet: TDataSet);
    procedure sbAddSpec1Click(Sender: TObject);
    procedure sbEditSpec1Click(Sender: TObject);
    procedure meSpecBeforeOpen(DataSet: TDataSet);
    procedure meSpecBeforePost(DataSet: TDataSet);
    procedure sbBarrelClick(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure sbDeleteClick(Sender: TObject);
    procedure btFilterClick(Sender: TObject);
    procedure sbClearFilterClick(Sender: TObject);
    procedure meDataBeforeOpen(DataSet: TDataSet);
    procedure btRequeryClick(Sender: TObject);
    procedure sbAddClick(Sender: TObject);
    procedure sbEditSpecClick(Sender: TObject);
    procedure sbClockClick(Sender: TObject);
    procedure meDataAfterPost(DataSet: TDataSet);
    procedure btFlowClick(Sender: TObject);
  private
    currentopid: integer;
    g: string;
    procedure SetFilter(conditions: string);
  public
    id: integer;
    procedure Init; override;
    function CheckBeforeAppend: boolean; override;
    function CheckBeforePrint: boolean;
    procedure CreatePassSpecByBase(opkindid: integer; sparechild: integer; spareparent: integer);
    procedure CreatePassSpecByEmpty(opkindid: integer);
    procedure CreatePassByDoc(docid: integer; containerid: integer; taskid: integer);
    procedure PartialInit; override;
  end;

var
  FormPasses, FormPasses0: TFormPasses;

implementation

{$R *.dfm}

uses EditPass, SelectDocForm, dmu, main, functions, EditPassSpec,
  PassTypeSelect, EditPassSpec1, AddPassSpec, SelectPassOpKind, filterlite,
  Clock, SelectPass, doctypeselect, transformdocuments, selectfolder;


procedure TFormPasses.Init;
begin

  if dtStart.Value = null then dtStart.Value := now()-10;
  self.tablename := 'passes';
  id := 0;

  inherited;
  self.formEdit := FormEditPass;
  self.meSpec.Close;
  self.meSpec.Open;
end;


procedure TFormPasses.PartialInit;
begin
  //meData.Close;
  //meData.Open;
end;


procedure TFormPasses.CreatePassByDoc(docid: integer; containerid: integer; taskid: integer);
var current_id: integer;

procedure AddPassHeader;
begin
    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('exec CreatePassHeader :docid');
    qrAux.Parameters.ParamByName('docid').Value := docid;
    qrAux.ExecSQL;
end;

procedure AddPassSpec;
begin
    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('exec CreatePassSpec :currentid, :docid, :containerid');
    qrAux.Parameters.ParamByName('currentid').Value := current_id;
    qrAux.Parameters.ParamByName('docid').Value := docid;
    qrAux.Parameters.ParamByName('containerid').Value := containerid;
    qrAux.ExecSQL;
end;

begin

  qrAux.Close;
  qrAux.SQL.Clear;
  qrAux.SQL.Add('select min(p.id), count(distinct p.id) from passes p, passoperations po, objectstatehistory osh');
  qrAux.SQL.Add('where p.id = po.parent_id and osh.task_id = po.task_id and po.container_id = osh.object_id');
  qrAux.SQL.Add('and isnull(p.isabolished,0) = 0');
  qrAux.SQL.Add('and osh.doc_id = :doc_id and (osh.object_id = :containerid or :containerid1 = 0)');
  qrAux.Parameters.ParamByName('doc_id').Value := docid;
  qrAux.Parameters.ParamByName('containerid').Value := containerid;
  qrAux.Parameters.ParamByName('containerid1').Value := containerid;
  qrAux.Open;

  current_id := qrAux.Fields[0].AsInteger;

  if (current_id=0) and (containerid=0) then
  begin

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select min(p.id), count(distinct p.id) from passes p');
    qrAux.SQL.Add('where p.base_doc_id = :doc_id');
    qrAux.SQL.Add('and isnull(p.isabolished,0) = 0');
    qrAux.Parameters.ParamByName('doc_id').Value := docid;
    qrAux.Open;

    current_id := qrAux.Fields[0].AsInteger;

  end;


  if (qrAux.Fields[1].AsInteger>1) and (containerid=0) then
  begin
    ShowMessage('К документу привязано больше одного пропуска. Перейдите к нужному пропуску через спецификацию.');
    exit;
  end;

  if (qrAux.Fields[1].AsInteger>1) and (containerid>0) then
  begin
   FormSelectPass.taskid := taskid;
   FormSelectPass.Init;
   FormSelectPass.ShowModal;
   if FormSelectPass.ModalResult = mrOk then
   begin
    current_id := FormSelectPass.meSpec.FieldByName('pass_id').AsInteger;
   end else
   begin
     exit;
   end;
  end;

  if current_id>0 then
  begin

    meData.Close;
    id := current_id;
    meData.Open;

    meData.First;

    if meData.Locate('id', current_id, []) then
    begin

      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('select count(*) from passoperations where parent_id ='+IntToStr(current_id));
      qrAux.Open;

      if qrAux.Fields[0].AsInteger = 0 then
      begin
        AddPassSpec;
      end;

      EE(FormPasses, FormEditPass, meData, 'passes');
    end;

  end else
  begin

    AddPassHeader;

    if qrAux.RowsAffected = 0 then exit;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select IDENT_CURRENT(''passes'')');
    qrAux.Open;
    current_id := qrAux.Fields[0].AsInteger;

    AddPassSpec;

    qrAux.Close;
    qrAux.SQL.Clear;

    meData.Close;
    id := current_id;
    meData.Open;

    meData.First;

    if meData.Locate('id', current_id, []) then
    begin
      EE(FormPasses, FormEditPass, meData, 'passes');
    end;

  end;

  id := 0;

end;


procedure TFormPasses.meDataAfterInsert(DataSet: TDataSet);
begin
  inherited;
  FormPassTypeSelect.passType := -1;
  FormPassTypeSelect.ShowModal;
  if FormPassTypeSelect.ModalResult = mrOk then
  begin
    meData.FieldByName('pass_type').Value := FormPassTypeSelect.passType;
  end else
  begin
    FormPassTypeSelect.passType := -1;
  end;

  meData.FieldByName('onedaypass_term_start').Value := now();

end;

procedure TFormPasses.meDataAfterOpen(DataSet: TDataSet);
begin
  inherited;
  meDataAfterScroll(DataSet);
end;

procedure TFormPasses.meDataAfterPost(DataSet: TDataSet);
begin
  inherited;

  if old_current_mode = 'none' then exit;

  dm.spRegisterDataChange.Parameters.ParamByName('@tablename').Value := 'passes';
  dm.spRegisterDataChange.Parameters.ParamByName('@tableid').Value := meData.FieldByName('id').AsInteger;
  dm.spRegisterDataChange.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
  dm.spRegisterDataChange.Parameters.ParamByName('@EventType').Value := old_current_mode;
  dm.spRegisterDataChange.ExecProc;

end;

procedure TFormPasses.meDataAfterScroll(DataSet: TDataSet);
begin

  pcSpec.Pages[0].TabVisible := false;
  pcSpec.Pages[1].TabVisible := false;
  pcSpec.Pages[meData.FieldByName('pass_type').AsInteger].TabVisible := true;
  pcSpec.ActivePageIndex := meData.FieldByName('pass_type').AsInteger;

  drvSpec.SelectCommand.Parameters.ParamByName('parent_id').Value := self.meData.FieldByName('id').AsInteger;
  meSpec.Close;
  meSpec.Open;
end;

procedure TFormPasses.meDataBeforeOpen(DataSet: TDataSet);
begin
  drvData.SelectCommand.Parameters.ParamByName('datestart').Value := dtStart.Value;
  drvData.SelectCommand.Parameters.ParamByName('guid').Value := g;
  drvData.SelectCommand.Parameters.ParamByName('id').Value := id;
  inherited;
end;

procedure TFormPasses.meSpecBeforeOpen(DataSet: TDataSet);
begin
  drvSpec.SelectCommand.Parameters.ParamByName('parent_id').Value := self.meData.FieldByName('id').AsInteger;
end;

procedure TFormPasses.meSpecBeforePost(DataSet: TDataSet);
begin
  inherited;
  meSpec.FieldByName('parent_id').Value := meData.FieldByName('id').AsInteger;
  meSpec.FieldByName('passoperation_id').Value := currentopid;
end;

procedure TFormPasses.sbAddClick(Sender: TObject);
begin
  id := 0;
  inherited;
end;

procedure TFormPasses.sbAddSpec1Click(Sender: TObject);
begin

  if meData.FieldByName('isabolished').AsBoolean then
  begin
    ShowMessage('Добавление позиций в аннулированый пропуск невозможно.');
    exit;
  end;


  currentopid := 0;
  if EA(self, FormEditPassSpec1, self.meSpec, 'passoperations', 'parent_id', self.meData.FieldByName('id').AsInteger) then
  begin
    dm.spRegisterDataChange.Parameters.ParamByName('@tablename').Value := 'passes';
    dm.spRegisterDataChange.Parameters.ParamByName('@tableid').Value := meData.FieldByName('id').AsInteger;
    dm.spRegisterDataChange.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
    dm.spRegisterDataChange.Parameters.ParamByName('@EventType').Value := 'addspec';
    dm.spRegisterDataChange.ExecProc;
  end;
end;

procedure TFormPasses.sbAddSpecClick(Sender: TObject);
begin

  FormSelectPassOpKind.passid := meData.FieldByName('id').AsInteger;
  FormSelectPassOpKind.containerid := meSpec.FieldByName('container_id').AsInteger;
  FormSelectPassOpKind.Init;

  if FormSelectPassOpKind.meData.RecordCount=0 then
  begin
    ShowMessage('Больше ничего добавить нельзя.');
    exit;
  end;

  FormSelectPassOpKind.ShowModal;

  if FormSelectPassOpKind.ModalResult <> mrOk then
  begin
    exit;
  end;

  if FormSelectPassOpKind.meData.FieldByName('spec_amount').AsInteger = 0 then
  begin
     CreatePassSpecByEmpty(FormSelectPassOpKind.meData.FieldByName('id').AsInteger);
  end else
  begin
     CreatePassSpecByBase(
     FormSelectPassOpKind.meData.FieldByName('id').AsInteger,
     FormSelectPassOpKind.meData.FieldByName('spare_child').AsInteger,
     FormSelectPassOpKind.meData.FieldByName('spare_parent').AsInteger);
  end;


  dm.spRegisterDataChange.Parameters.ParamByName('@tablename').Value := 'passes';
  dm.spRegisterDataChange.Parameters.ParamByName('@tableid').Value := meData.FieldByName('id').AsInteger;
  dm.spRegisterDataChange.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
  dm.spRegisterDataChange.Parameters.ParamByName('@EventType').Value := 'addspec';
  dm.spRegisterDataChange.ExecProc;

  meSpec.Close;
  meSpec.Open;

end;

procedure TFormPasses.sbBarrelClick(Sender: TObject);
begin
  if fQYN('Аннулировать материальный пропуск? Действие необратимо!') then
  begin
    meData.Edit;
    meData.FieldByName('isabolished').Value := 1;
    try
       self.current_mode := 'abolish';
       meData.Post;
       RefreshRecord(meData);
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  end;
end;

procedure TFormPasses.sbClearFilterClick(Sender: TObject);
begin
  SetFilter('');
end;

procedure TFormPasses.sbClockClick(Sender: TObject);
begin
  FormClock.table_name := 'passes';
  FormClock.table_id := meData.FieldByName('id').AsInteger;
  FormClock.Init;
  FormClock.ShowModal;
end;

procedure TFormPasses.CreatePassSpecByBase(opkindid: integer; sparechild: integer; spareparent: integer);
var meBuffer: TMemTableEh;
begin
  FormAddPassSpec.ReInit(opkindid, sparechild, spareparent, meSpec.FieldByName('cnum').AsString);
  FormAddPassSpec.ShowModal;
  if FormAddPassSpec.ModalResult = mrOk then
  begin

    Screen.Cursor := crHourGlass;

    meBuffer :=  FormAddPassSpec.meData;
    meBuffer.First;

    while not meBuffer.Eof do
    begin

      if (meBuffer.FieldByName('checkcolumn').Value = 0)
      then begin
        meBuffer.Next;
        continue;
      end;

      meSpec.Append;
      meSpec.FieldByName('parent_id').Value := meData.FieldByName('id').Value;
      meSpec.FieldByName('ischild').Value := sparechild;
      meSpec.FieldByName('isparent').Value := spareparent;
      meSpec.FieldByName('passoperation_id').Value := meBuffer.FieldByName('pok_id').Value;
      meSpec.FieldByName('container_id').Value := meBuffer.FieldByName('container_id').Value;
      meSpec.FieldByName('seal_number').Value := meBuffer.FieldByName('seal_number').Value;
      meSpec.FieldByName('isempty').Value := meBuffer.FieldByName('isempty').Value;
      meSpec.FieldByName('task_id').Value := meBuffer.FieldByName('task_id').Value;
      currentopid := meBuffer.FieldByName('pok_id').Value;
      try
        meSpec.Post;
      except
        on E:Exception do
        begin
          Screen.Cursor := crDefault;
          ShowMessage(E.Message);
          exit;
        end;
      end;
      meBuffer.Next;

    end;

  end;
  meSpec.Close;
  meSpec.Open;

  Screen.Cursor := crDefault;
end;


procedure TFormPasses.CreatePassSpecByEmpty(opkindid: integer);
begin
  currentopid := opkindid;
  EA(self, FormEditPassSpec, self.meSpec, 'passoperations', 'parent_id', self.meData.FieldByName('id').AsInteger);
end;


procedure TFormPasses.dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
begin

   if meData.FieldByName('isabolished').AsBoolean then
   begin
    dgData.Canvas.Font.Color := clGray;
   end;

  inherited;
end;

procedure TFormPasses.sbDeleteClick(Sender: TObject);
begin

  if meData.FieldByName('isabolished').AsBoolean then
  begin
    ShowMessage('Удаление аннулированого пропуска невозможно.');
    exit;
  end;

  if meData.FieldByName('isprinted').AsBoolean then
  begin
    ShowMessage('Удаление распечатанного пропуска невозможно. Используйте аннулирование.');
    exit;
  end;

  meSpec.Close;
  meSpec.Open;

  while not meSpec.Eof do
  begin
    if meSpec.FieldByName('doctext').AsString <> '' then
    begin
      ShowMessage('По пропуску уже созданы Акты приемки/выдачи. Удаление невозможно.');
      exit;
    end;
    meSpec.Next;
  end;

  inherited;

end;

procedure TFormPasses.sbDeleteSpecClick(Sender: TObject);
begin

  if meData.FieldByName('isabolished').AsBoolean then
  begin
    ShowMessage('Удаление позиций из аннулированого пропуска невозможно.');
    exit;
  end;

  if meSpec.FieldByName('doctext').AsString <> '' then
  begin
    ShowMessage('По выбранной позиции пропуска уже создан Акт приемки/выдачи. Удаление невозможно.');
    exit;
  end;

  if ED(meSpec) then
  begin
    dm.spRegisterDataChange.Parameters.ParamByName('@tablename').Value := 'passes';
    dm.spRegisterDataChange.Parameters.ParamByName('@tableid').Value := meData.FieldByName('id').AsInteger;
    dm.spRegisterDataChange.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
    dm.spRegisterDataChange.Parameters.ParamByName('@EventType').Value := 'deletespec';
    dm.spRegisterDataChange.ExecProc;
  end;
end;

procedure TFormPasses.sbEditSpec1Click(Sender: TObject);
begin

  if meData.FieldByName('isabolished').AsBoolean then
  begin
    ShowMessage('Редактирование позиций аннулированого пропуска невозможно.');
    exit;
  end;

  if EE(self, FormEditPassSpec1, self.meSpec, 'passoperations') then
  begin
    dm.spRegisterDataChange.Parameters.ParamByName('@tablename').Value := 'passes';
    dm.spRegisterDataChange.Parameters.ParamByName('@tableid').Value := meData.FieldByName('id').AsInteger;
    dm.spRegisterDataChange.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
    dm.spRegisterDataChange.Parameters.ParamByName('@EventType').Value := 'addspec';
    dm.spRegisterDataChange.ExecProc;
  end;
end;

procedure TFormPasses.sbEditSpecClick(Sender: TObject);
begin

  if meData.FieldByName('isabolished').AsBoolean then
  begin
    ShowMessage('Редактирование позиций аннулированого пропуска невозможно.');
    exit;
  end;

  if meSpec.RecordCount > 0 then
  if meSpec.FieldByName('container_id').AsInteger = 0 then
  if EE(self, FormEditPassSpec, self.meSpec, 'passoperations') then
  begin
    dm.spRegisterDataChange.Parameters.ParamByName('@tablename').Value := 'passes';
    dm.spRegisterDataChange.Parameters.ParamByName('@tableid').Value := meData.FieldByName('id').AsInteger;
    dm.spRegisterDataChange.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
    dm.spRegisterDataChange.Parameters.ParamByName('@EventType').Value := 'editspec';
    dm.spRegisterDataChange.ExecProc;
  end;

end;

procedure TFormPasses.sbReportClick(Sender: TObject);
var gg: TGuid; g: Variant; groupfield: string; dt: TFieldType; doctype: integer;
begin

  if meData.FieldByName('isabolished').AsBoolean then
  begin
    ShowMessage('Печать аннулированного пропуска невозможна.');
    exit;
  end;

  if not CheckBeforePrint then
  begin
    ShowMessage('В одном пропуске не может быть две операции в одном направлении. Удалите часть записей. Печать невозможна.');
    exit;
  end;

  if (Trim(meData.FieldByName('payer_id').AsString)='')
  or (Trim(meData.FieldByName('dealer_id').AsString)='')
  or (Trim(meData.FieldByName('driver_id').AsString)='')
  or (Trim(meData.FieldByName('car_data').AsString)='')
  or (Trim(meData.FieldByName('car_number').AsString)='')
  or (Trim(meData.FieldByName('attorney').AsString)='')
  or (Trim(meData.FieldByName('onedaypass_term_start').AsString)='')
  or (Trim(meData.FieldByName('onedaypass_paytype').AsString)='')
  then begin
    ShowMessage('Не все поля пропуска заполнены. Печать невозможна.');
    exit;
  end;

  FormSelectDocForm.qrTemplates.Close;
  FormSelectDocForm.qrTemplates.ParamCheck := true;
  FormSelectDocForm.qrTemplates.Parameters.ParamByName('doctype_id').Value := meData.FieldByName('doctype_id').AsInteger;
  FormSelectDocForm.qrTemplates.Open;

  if FormSelectDocForm.qrTemplates.RecordCount = 0 then
  begin
    ShowMessage('Печать данного вида документов не предусмотрена.');
    exit;
  end;

  FormSelectDocForm.ShowModal;
  if FormSelectDocForm.ModalResult = mrOk then
  begin

    dm.spRegisterDataChange.Parameters.ParamByName('@tablename').Value := 'passes';
    dm.spRegisterDataChange.Parameters.ParamByName('@tableid').Value := meData.FieldByName('id').AsInteger;
    dm.spRegisterDataChange.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
    dm.spRegisterDataChange.Parameters.ParamByName('@EventType').Value := 'print';
    dm.spRegisterDataChange.ExecProc;


    CreateGUID(gg);
    g := GuidToString(gg);
    FillSelectListAll(g, 'id');

    qrReport.Close;
    qrReport.SQL.Clear;
    qrReport.Parameters.Clear;
    qrReport.SQL.Text := FormSelectDocForm.qrTemplates.FieldByName('sql_text').AsString;
    if Assigned(qrReport.Parameters.FindParam('userid')) then
      qrReport.Parameters.ParamByName('userid').Value := FormMain.currentUserId;
    if Assigned(qrReport.Parameters.FindParam('docid')) then
      qrReport.Parameters.ParamByName('docid').Value := meData.FieldByName('id').AsInteger;

    if self.dgData.Selection.SelectionType in [gstAll, gstRecordBookmarks] then
    begin
      if Assigned(qrReport.Parameters.FindParam('guid')) then
      begin
        qrReport.Parameters.ParamByName('guid').DataType := TFieldType(1);
        qrReport.Parameters.ParamByName('guid').Value := g;
      end;
    end else
    begin
      if Assigned(qrReport.Parameters.FindParam('guid')) then
      begin
        qrReport.Parameters.ParamByName('guid').DataType := TFieldType(1);
        qrReport.Parameters.ParamByName('guid').Value := '-';
      end;
    end;

    qrReport.Open;
    exReport.Template := ExtractFilePath(Application.ExeName) +'/templates/'+FormSelectDocForm.qrTemplates.FieldByName('template').AsString;
    groupfield := FormSelectDocForm.qrTemplates.FieldByName('group_field').AsString;
    exReport.Show('', false, groupfield, '');

    dm.spClearSelectList.Parameters.ParamByName('@guid').Value := g;
    dm.spClearSelectList.ExecProc;

    meData.Edit;
    meData.FieldByName('isprinted').Value := 1;
    try
       meData.Post;
       RefreshRecord(meData);
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;

  end;

end;

procedure TFormPasses.btFilterClick(Sender: TObject);
begin
  FormFilterLite.ShowModal;
  if FormFilterLite.ModalResult in [mrOk, mrYes] then
  begin
    SetFilter('');
    CreateGuid(gu);
    g := GuidToString(gu);
    SetFilter(FormFilterLite.conditions);
  end;
end;

procedure TFormPasses.btFlowClick(Sender: TObject);
var newsystem_section: string;
begin

    if meData.FieldByName('pass_type').AsInteger <> 0 then
    begin
      ShowMessage('Создание документов возможно только по пропуску с типом Приемка/Выдача КТК');
      exit;
    end;

    meSpec.Close;
    meSpec.Open;
    meSpec.First;

    while not meSpec.Eof do
    begin
      if meSpec.FieldByName('doctext').AsString = '' then break;
      meSpec.Next;
    end;

    if meSpec.FieldByName('task_id').AsInteger = 0 then
    begin
      if fQYN('Создать Акт приемки ?') then
      begin

        qrAux.Close;
        qrAux.SQL.Clear;
        qrAux.SQL.Add('select system_section from doctypes where global_section = ''output-store''');
        qrAux.Open;

        newsystem_section := qrAux.Fields[0].AsString;

        FormSelectFolder.meFolders.Close;
        FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('folder_section').Value := newsystem_section;
        FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('startid').Value := 0;
        FormSelectFolder.meFolders.Open;
        FormSelectFolder.meFolders.SortOrder := 'folder_name desc';
        FormSelectFolder.ShowModal;

        if FormSelectFolder.ModalResult = mrOk then
        begin
          qrAux.Close;
          qrAux.SQL.Clear;
          qrAux.SQL.Add('exec CreateDocFromPass :passopid, :folderid, :userid');
          qrAux.Parameters.ParamByName('passopid').Value := meSpec.FieldByName('id').AsInteger;
          qrAux.Parameters.ParamByName('folderid').Value := FormSelectFolder.meFolders.FieldByName('id').AsInteger;
          qrAux.Parameters.ParamByName('userid').Value := FormMain.currentUserId;
          qrAux.ExecSQL;
        end else
        begin
          ShowMessage('Отменено.');
        end;

      end;

      exit;

    end;

    FormDocTypeSelect.dsLocal.DataSet := nil;
    dm.spGetDocumentTypeFromPassOp.Close;
    dm.spGetDocumentTypeFromPassOp.Parameters.ParamByName('@passopid').Value := meSpec.FieldByName('id').AsInteger;
    dm.spGetDocumentTypeFromPassOp.Parameters.ParamByName('@doctypeid').Value := meSpec.FieldByName('end_doctype_id').AsInteger;

    try
      dm.spGetDocumentTypeFromPassOp.Open;
    except
      on E: Exception
      do begin
        ShowMessage(E.Message);
        exit;
      end;
    end;

    if dm.spGetDocumentTypeFromPassOp.RecordCount = 0 then
    begin
      ShowMessage('Окончание маршрута документооборота, либо нет разрешенных маршрутов.');
      exit;
    end;

    FormDocTypeSelect.dsLocal.DataSet := dm.spGetDocumentTypeFromPassOp;
    FormDocTypeSelect.ShowModal;

    if FormDocTypeSelect.ModalResult <> mrOk then
    begin
      exit;
    end;

    FormTransformDocuments.dsData.DataSet := nil;
    dm.spPrepareTransformFromPassOp.Close;
    dm.spPrepareTransformFromPassOp.Parameters.ParamByName('@passopid').Value := meSpec.FieldByName('id').AsInteger;
    dm.spPrepareTransformFromPassOp.Parameters.ParamByName('@docrouteid').Value := dm.spGetDocumentTypeFromPassOp.FieldByName('id').AsInteger;
    dm.spPrepareTransformFromPassOp.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
    dm.spPrepareTransformFromPassOp.ExecProc;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('delete from temp_filter where doctypes = ''-1'' and userid = '+IntToStr(FormMain.currentUserId));
    qrAux.ExecSQL;

    FormTransformDocuments.g := meSpec.FieldByName('id').AsString;
    FormTransformDocuments.qrData.Close;
    FormTransformDocuments.qrData.Parameters.ParamByName('selectguid').Value := meSpec.FieldByName('id').AsString;
    FormTransformDocuments.qrData.Parameters.ParamByName('userid').Value := FormMain.currentUserId;
    FormTransformDocuments.qrData.Parameters.ParamByName('userid1').Value := FormMain.currentUserId;
    FormTransformDocuments.qrData.Open;

    if FormTransformDocuments.qrData.RecordCount = 0 then
    begin
      ShowMessage('Нет объектов, доступных для включения в новый документ.'+#10+'Вероятно, уже создан документ на основании данного документа.');
      exit;
    end;

    if (FormTransformDocuments.qrData.RecordCount > 1) then
    begin
      FormTransformDocuments.dsData.DataSet := FormTransformDocuments.qrData;
      FormTransformDocuments.ShowModal;
    end else
    begin
      FormTransformDocuments.ModalResult := mrOk;
    end;

    if FormTransformDocuments.ModalResult = mrOk then
    begin

      try
        dm.spCheckNonConfirmedChildDocs.Close;
        dm.spCheckNonConfirmedChildDocs.Parameters.ParamByName('@selectguid').Value := meSpec.FieldByName('id').AsString;
        dm.spCheckNonConfirmedChildDocs.ExecProc;
      except
        On E:Exception do
        begin
          ShowMessage(E.Message);
          exit;
        end;
      end;


      qrAux.Close;
      qrAux.SQL.Clear;
      qrAux.SQL.Add('select system_section from doctypes where id = '+dm.spGetDocumentTypeFromPassOp.FieldByName('end_doctype_id').AsString);
      qrAux.Open;

      newsystem_section := qrAux.Fields[0].AsString;

      FormSelectFolder.meFolders.Close;
      FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('folder_section').Value := newsystem_section;
      FormSelectFolder.drvFolders.SelectCommand.Parameters.ParamByName('startid').Value := 0;
      FormSelectFolder.meFolders.Open;
      FormSelectFolder.meFolders.SortOrder := 'folder_name desc';
      FormSelectFolder.ShowModal;

      if FormSelectFolder.ModalResult = mrOk then
      begin
        dm.spTransformDocuments.Parameters.ParamByName('@routeid').Value := dm.spGetDocumentTypeFromPassOp.FieldByName('id').AsInteger;;
        dm.spTransformDocuments.Parameters.ParamByName('@newdoctypeid').Value := dm.spGetDocumentTypeFromPassOp.FieldByName('end_doctype_id').AsInteger;
        dm.spTransformDocuments.Parameters.ParamByName('@folderid').Value := FormSelectFolder.meFolders.FieldByName('id').AsInteger;
        dm.spTransformDocuments.Parameters.ParamByName('@userid').Value := FormMain.currentUserId;
        dm.spTransformDocuments.ExecProc;

        FullRefresh(meData);

      end else
        ShowMessage('Отменено.');

    end else
    begin
      ShowMessage('Создание документов не выполнено.');
    end;

    meDataAfterScroll(meData);

end;

procedure TFormPasses.btRequeryClick(Sender: TObject);
begin
  meData.Close;
  meData.Open;
end;

function TFormPasses.CheckBeforeAppend: boolean;
begin
 result := true;
 if FormPassTypeSelect.passType < 0 then
 result := false;
end;


function TFormPasses.CheckBeforePrint: boolean;
begin

    result := true;

    qrAux.Close;
    qrAux.SQL.Clear;
    qrAux.SQL.Add('select 1 from passoperations po, passoperationkinds pok');
    qrAux.SQL.Add('where po.parent_id = :id and pok.id = po.passoperation_id and isnull(higher_id,0)=0');
    qrAux.SQL.Add('group by pok.direction having count(*)>1');
    qrAux.Parameters.ParamByName('id').Value := meData.FieldByName('id').AsInteger;
    qrAux.Open;

    if qrAux.RecordCount>0 then
    begin
      result := false;
    end;

end;


procedure TFormPasses.SetFilter(conditions: string);
begin

    dm.spCreateFilter.Parameters.ParamByName('@userid').Value := 0;
    dm.spCreateFilter.Parameters.ParamByName('@doctypes').Value := g;
    dm.spCreateFilter.Parameters.ParamByName('@conditions').Value := conditions;
    dm.spCreateFilter.ExecProc;

    meData.Close;
    meData.Open;

    if conditions = '' then
    begin
      plStatus.Caption := ' Фильтр снят.';
      g := '';
    end else
    begin
      plStatus.Caption := ' Установлен фильтр по списку номеров.';
    end;

end;


end.
