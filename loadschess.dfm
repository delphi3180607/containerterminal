﻿inherited FormLoadsChess: TFormLoadsChess
  Caption = #1044#1080#1089#1083#1086#1082#1072#1094#1080#1103
  ClientHeight = 628
  ClientWidth = 929
  Scaled = False
  ExplicitWidth = 945
  ExplicitHeight = 666
  PixelsPerInch = 96
  TextHeight = 13
  inherited plBottom: TPanel
    Top = 588
    Width = 929
    Height = 40
    Constraints.MaxHeight = 75
    Visible = True
    ExplicitTop = 588
    ExplicitWidth = 929
    ExplicitHeight = 40
    inherited btnOk: TButton
      Left = 694
      Height = 34
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ExplicitLeft = 694
      ExplicitHeight = 34
    end
    inherited btnCancel: TButton
      Left = 813
      Height = 34
      Visible = False
      ExplicitLeft = 813
      ExplicitHeight = 34
    end
    object nuLen: TDBNumberEditEh
      AlignWithMargins = True
      Left = 576
      Top = 3
      Width = 105
      Height = 34
      Margins.Right = 10
      Align = alRight
      DisplayFormat = '### ##0.00'
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      StyleElements = [seClient, seBorder]
      TabOrder = 2
      Value = 0.000000000000000000
      Visible = True
      ExplicitHeight = 31
    end
    object nuLen1: TDBNumberEditEh
      AlignWithMargins = True
      Left = 472
      Top = 3
      Width = 91
      Height = 34
      Margins.Right = 10
      Align = alRight
      DisplayFormat = '### ##0.00'
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      StyleElements = [seClient, seBorder]
      TabOrder = 3
      Value = 0.000000000000000000
      Visible = True
      ExplicitHeight = 31
    end
    object nuAmount: TDBNumberEditEh
      AlignWithMargins = True
      Left = 368
      Top = 3
      Width = 91
      Height = 34
      Margins.Right = 10
      Align = alRight
      DecimalPlaces = 0
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      StyleElements = [seClient, seBorder]
      TabOrder = 4
      Value = 0.000000000000000000
      Visible = True
      ExplicitHeight = 31
    end
    object dgOrderInfo: TDBGridEh
      Left = 0
      Top = 0
      Width = 365
      Height = 40
      Align = alClient
      Color = clSkyBlue
      Constraints.MaxHeight = 48
      DataSource = dsOrderInfo
      DynProps = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = []
      GridLineParams.BrightColor = clSkyBlue
      GridLineParams.DarkColor = clGray
      GridLineParams.DataHorzColor = clSkyBlue
      GridLineParams.DataVertColor = clSkyBlue
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      HorzScrollBar.VisibleMode = sbNeverShowEh
      IndicatorOptions = []
      IndicatorParams.FillStyle = cfstThemedEh
      Options = [dgEditing, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind]
      ParentFont = False
      TabOrder = 5
      TitleParams.BorderInFillStyle = True
      TitleParams.FillStyle = cfstSolidEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clBlue
      TitleParams.Font.Height = -13
      TitleParams.Font.Name = 'Calibri'
      TitleParams.Font.Style = [fsBold]
      TitleParams.HorzLineColor = clSilver
      TitleParams.ParentFont = False
      TitleParams.VertLineColor = clSilver
      VertScrollBar.VisibleMode = sbNeverShowEh
      OnDrawColumnCell = dgOrderInfoDrawColumnCell
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 83
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 87
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1082#1086#1085#1090'.'
          Width = 60
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          Title.Caption = #1042#1083#1072#1076#1077#1083#1077#1094
          Width = 147
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'amount'
          Footers = <>
          Title.Caption = #1050#1086#1083'-'#1074#1086
          Width = 49
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'rest_amount'
          Footers = <>
          Title.Caption = #1054#1089#1090#1072#1090#1086#1082
          Width = 49
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'station_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1085#1094#1080#1103' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1103
          Width = 144
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 158
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer_code'
          Footers = <>
          Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
          Width = 158
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Width = 250
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  inherited plAll: TPanel
    Width = 929
    Height = 588
    ExplicitWidth = 929
    ExplicitHeight = 588
    inherited plTop: TPanel
      Width = 929
      Margins.Bottom = 0
      ExplicitWidth = 929
      inherited sbDelete: TPngSpeedButton
        Left = 148
        Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1072#1075#1086#1085'('#1099')'
        Action = nil
        ExplicitLeft = 148
        ExplicitHeight = 27
      end
      inherited btFilter: TPngSpeedButton
        Left = 336
        OnClick = btFilterClick
        ExplicitLeft = 336
        ExplicitHeight = 29
      end
      inherited btExcel: TPngSpeedButton
        Left = 296
        ExplicitLeft = 296
        ExplicitHeight = 29
      end
      inherited sbAdd: TPngSpeedButton
        Left = 34
        Hint = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1074#1072#1075#1086#1085#1086#1074
        ExplicitLeft = 31
        ExplicitHeight = 29
      end
      inherited sbEdit: TPngSpeedButton
        Left = 74
        Hint = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1087#1086#1075#1088#1091#1079#1082#1091
        Enabled = False
        Visible = False
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000140803000000A06B0D
          B10000000373424954080808DBE14FE0000000AB504C5445596D81A3D0FD83B0
          DCFFFFFF7599BDA3CAF2C5E1FF6C8CAB8DBBE97FA9D3B8D2EDCFE7FF647F9AB5
          D9FF95C3F179A0C66B94B5D8ECFF8AA9C891B6DC7DA5CE8DBCEBC4DCF5A5CDF5
          7289A1BDDEFF617C956B849C8BB5E19DCAF7C1D9F285A1BD9EC1E2ADD6FF7D95
          AEB5DEFFC6E3FFADD3F96B87A3739CC58CB7E3D6E6FF7497BBDDEEFFADCAE87A
          A1C9B4D6F95C73898BAED192C0EDBBD8F582ADD79DC7F185B2E093BEE965819C
          A5D6FFFC203F71000000097048597300000B1200000B1201D2DD7EFC0000001C
          74455874536F6674776172650041646F62652046697265776F726B7320435335
          71B5E33600000016744558744372656174696F6E2054696D650030392F33302F
          31323BF0880E000000BC4944415478DAB5D14B0FC1401885E153D531DA925022
          21EA92103BADFFFF231A11425C4BA61B6161884EDD33CBB1E45B9E67F3269FA6
          E3FB69BF05F76A01671A9AF1677178699D9260C60611F9373C082012C7BE3209
          0FBB1225BD237373E91DDDB7F9291B49A8528E32B90FFD553DCCE917DD5A3E25
          58DDEDC167888AAE98E8DE16B6369750448BB14CAC29554613D6185FA033F337
          40235072072352035642C9F5F8DA40D21D2BB9859BC9FAF4345572496BE121A0
          5CC9FDF73F5EDE9354510CCE50F50000000049454E44AE426082}
        ExplicitLeft = 74
      end
      inherited Bevel2: TBevel
        Left = 144
        Width = 2
        Margins.Left = 2
        Margins.Right = 2
        ExplicitLeft = 137
        ExplicitTop = 0
        ExplicitWidth = 2
        ExplicitHeight = 29
      end
      inherited btTool: TPngSpeedButton
        Left = 900
        Width = 26
        ExplicitLeft = 888
        ExplicitWidth = 26
      end
      object sbMassAdd: TPngSpeedButton [7]
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 34
        Height = 29
        Hint = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1074#1072#1075#1086#1085#1086#1074' '#1089#1087#1080#1089#1082#1086#1084
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        Visible = False
        OnClick = sbMassAddClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000002984944415478DAA55369485451183DF7CD38630E6F2C35
          9C96B1CD46CA30A2ED4F98086D5264E512988AE58F40CB446D512ACAB4140B6B
          5A08A42C93C1054B8A12827E474964398DA213166A92A9A5CEBC69DE7BF77667
          E88FFE29EBC2C7E1F13DCEE59C730F618CE17F0ED993DF90B3296E5DC5A89B19
          FC5C9482F151A9CA918132FEADAA1C991F55BE33EA6497C33170A2E54ACA0D52
          D6EC5445D120CCF466A7A39B56E7C56A4889ADF79F340CF7F7E376E166428A1F
          F4B0B2B4C81913645F7A819A93F1849CAAEB66E5072CF02A5C335F28136FE019
          B472BD0A1F0AAA503FAAB2CCF74190175DC0928865C82A7F8EBBC55B38C1FD2E
          569E1E05999B036E9C34608540BF401330174C56FCA37A2548635D60A60C9090
          AD080F3520F37C1BEE9DD94EC8F15A3BABC85CE97757F50E61DC7E10E2829D50
          7EB443085C03C53D0269C40EC1B41F5A5322549E4AA8A847FAD9A7A83B974048
          D19D4E569915CD0964B8076AC1A40EE88DD1F00EB54210E331E67C88594B7311
          684EE551FA6206C4202DD24E3F467DE92E420A6A3A58D5A118FC9CB463FC4311
          8CE66DFEBCE5B1F710022231DAD786405302D71F002A4B6082116151C9C8B8F8
          1AB6B2DD84E4DD6A67D587D762E45D36B47A03D7ECF51BA70B8A00A15AA81E17
          97D0C34DF4627CF02D423694728244A4963C4353E53E428E5E7FC5AEE6AC87AB
          BF1E8AE71B1813C0142F263FB5C2382F0E8AF41DEE610784E0D59813930BAD68
          864EA3415261139AAB9209C9BDF692598F6C9C92315526F1F9492CF486C5F04C
          7C45C8AA6308B6244DF9676F41235A2EA7105F171AC2CDF353A8EFBDFBF2E698
          B6D08AA83017DAFB66A375281992A2E3BEFCEE054FCB97383F8DBC0BA9647A1B
          3F36AFD84165F52653D4FCE5E9CE477F6CE374825E9B258613504B86B3F36F9E
          F42FA3F253F0D8F7B7020000000049454E44AE426082}
      end
      object Bevel1: TBevel [8]
        AlignWithMargins = True
        Left = 252
        Top = 0
        Width = 2
        Height = 29
        Margins.Left = 2
        Margins.Top = 0
        Margins.Right = 2
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 248
      end
      object Bevel3: TBevel [9]
        AlignWithMargins = True
        Left = 70
        Top = 0
        Width = 2
        Height = 29
        Margins.Left = 2
        Margins.Top = 0
        Margins.Right = 2
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 64
      end
      object spWaste: TPngSpeedButton [10]
        AlignWithMargins = True
        Left = 182
        Top = 0
        Width = 34
        Height = 29
        Hint = #1054#1082#1086#1085#1095#1072#1090#1077#1083#1100#1085#1086' '#1091#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1080' '#1087#1086#1084#1077#1095#1077#1085#1085#1099#1077' '#1085#1072' '#1091#1076#1072#1083#1077#1085#1080#1077
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        OnClick = spWasteClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D494844520000000D000000100806000000CDA3F5
          390000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000001A84944415478DA7592BF2F435114C7
          CFB95EF35A2D0684B41D2424C4A2268B7FA07E25AC368384894448CC6220115B
          67039358D48F7FC062A09358249256A36210DAB44FDB7B7DEFABCB53ED4BBE39
          E7DD7B3EE77DEF79979552D4EC494E8E6E2324A6CE6ED3DE756E04A1388EB001
          F9A07140AA2984E210C20134F7BDB40C2051DFF40702D08970050D7AF667A021
          80BBFF200002F90D3452D77416DA87D6001ED743ABC8F71ACCE2047A8316A069
          8049173A9D887520A6A0BE3FE5DA35BBD9A3676F0CE0B586B6F0B2694A94AC9D
          5158826445B9340B36ADEE35A8A10C928801ACA045811E1B1093AC2A2AE61CAA
          E42B5EF04843CAD8611F537B7F90849F89434C2AAF483A8A3E1E0A243F95B1EB
          9E49FFEDA842577FB74D81B04DBEA820F6A34F89A89C96547C76A8F4E210B7B8
          5456438BFAAA001206B22282440050115046BA164B391792A85D32233F87BDB8
          80BDB601D8B3612F087B05450AF6DE7FED5D627A7103E941A430882E3D88D65E
          1B5D05A96AED2BE5DA205E511303F4E4BD4661FDF701C6993106F8D7E7C47E1E
          C005F65600641BDE72C0F308EB581E66A63BE43B283EF4D67C01F722CCCEB042
          74220000000049454E44AE426082}
      end
      object sbRestore: TPngSpeedButton [11]
        AlignWithMargins = True
        Left = 216
        Top = 0
        Width = 34
        Height = 29
        Hint = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1080', '#1087#1086#1084#1077#1095#1077#1085#1085#1099#1077' '#1085#1072' '#1091#1076#1072#1083#1077#1085#1080#1077
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        OnClick = sbRestoreClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D494844520000001200000012080600000056CE8E
          570000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E3360000026F4944415478DA9592CF6B134114C7
          DFCCBC9DDD4DDAEE9A543126AD05FF050FE251FF01451141848AA807EFA2E0DD
          1F672F1611410A4514FD0754F0502CA8F8A3A44AABA88DF6D2269B3431BB9B64
          D737B3C6B634697587E1CDCECCFBCCF77D67581CC7B0D5373E992B98162E067E
          7BE4DEA9A552BF7D6C2BD0D9A9FC21438AA743AE09352F80306C1FBE7BF2E7B3
          FF029D7B50B88C925FCB646DB05306347FB5A0B2E22BD8953B277E5CFF27D0F9
          8785C7D2C42399611BA4141047B49103413AE02D37C10FDA4F268E978EF6059D
          99CCB9D2C257968DFBDC8C0D8621A01375A0DAF2C0315C109CA01D00AFDC847A
          23F85C5D6DEC9F1A2F7B1B40171E8D1CE0824FA707251B7224706A6114422DF4
          08161184836366C01274008D5757032857EA71D86A1FBC7D6CF1A50611E406B1
          2E2129B0D308B68510618B20353A8925D2A971CE60D074C0ECA45479D068F8D0
          68FA6AF9661774957EBE469D887C88209536EEF36C9D20A44B99439F8A820970
          2D17964A75F0AA8DD3A6698034502D8FF5333BDE3D92D6632FA86840C61A06A4
          68611AE616BE0199CDB6BD35521817461D3D5EF197352897CA27AAC8F0E2C277
          205FB607294585BD8E72055682654032D7953B60001D321AE1FDFC97CD204ABA
          D56E451B40F4002FEEDA33A095B4E9E604252BB3932780F0F6D37C4F509CDD99
          DA009212015168906406251BA40A7559AABF9EFBD81B541873D62674E7BA2B73
          05531041252530A438539CED0DCA8F0EAD03A9F74209C0FF2A41861AA4BA5239
          5D7CB7358831822815D45097D14D46907FA241F32F66DFF40729258C20C8B856
          9040D6A22A09F501063CFF30D31B24045F9B60DD02BB8E254AD7CF06AD7013E8
          37A32DFA448D2307260000000049454E44AE426082}
        ExplicitLeft = 228
      end
      object sbFixedSelect: TPngSpeedButton [12]
        AlignWithMargins = True
        Left = 256
        Top = 0
        Width = 34
        Height = 29
        Hint = #1060#1080#1082#1089#1080#1088#1086#1074#1072#1085#1085#1099#1081' '#1074#1099#1073#1086#1088' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1077#1085#1085#1086#1081' '#1079#1072#1103#1074#1082#1080
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        OnClick = sbFixedSelectClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000002B94944415478DAA5936D48935114C7FFD7676AB6564C8D
          CDC190401B2DD6978A3E386A058104C22450C944A4C2CA4A7A592F4645859619
          E17A214A49734A08E184308AC598A11135C130F3A5EC8BD369F3A5701BEAF63C
          B7FBDCF9A13E5A170EE79C7B9FF33B2FCFBD84528AFF5924F7646B99D9B2A57A
          264C959C2549A04C4449649A42A2CC1745A629D7223B5B9D10090D0C8C9D6BBB
          93F780543E1F11552A65DC72338F0C0C49B5E5DB0572F1D9B77FEA21E0F3E1D1
          991D8454347FA5958519CB061CBCE946FDF95D845C700CD1AAFDEBB118653DB3
          03B96F5162A3609EC46D368F288D099BC7A248B14EAB4249950B0D15BB19A069
          9056151910611F421E14A388B28EB9CC061FA828C5F62406D2A424A1F8DA2B3C
          BD9C4DC8D9C67E5A5D6CE4997805B25079FAE015C8102E123844AE22459588A2
          2B2FE1B8BA8710DB93CFF456C9C65856266409222D81282B216683436547B552
          81C24B2FD0723D8790D3F59FE8ED039B38405EF3F3F3E8EAEA427F5F1FD46A35
          A6A7A761CDCDC51A75329A1A1B90CCF6666767D1372EE0FBC7B655A4FCA197D6
          1EDE0C9105472311B4343723383787FCFC7C844221F8D8EFEAEDEDC58AA424EC
          B458200802FC7E3F7A7A7AE072B96AC889FB1FA8BD6C2BCFEEE9ECC41B970B7B
          0B0AF0BAA303EFBABBA1D56A61329978D0D4D41426262690959505B3D90CA7D3
          F98B1CBBFB9EDE3BBE8D03EC763BC2E130B2737260CCCC84D56A85CD66E3AD04
          8341040201D4D5D5A1BDBD1D5EAF176EB77B5E7E0BAD1ABD2E4F62F7DC3FF816
          BA841F283D5A8A1B358F31E5FB82788500A552C913C82D45D87D51A71970EAC8
          3E381C8E9FE4CFD768341A37646464B4592C16AD5EAF97140A45F2E4E4248687
          8791989888F4F47468341A44A3D199D1D1D1388FC733F117C060300869696926
          96D1A9D3E9D6B220816DC7B1DE0F313D969A9AEA643A7E6161411C1F1F0FB08A
          727F036CB978CDCA4EEF2A0000000049454E44AE426082}
        ExplicitHeight = 27
      end
      object Bevel4: TBevel [13]
        AlignWithMargins = True
        Left = 292
        Top = 0
        Width = 2
        Height = 29
        Margins.Left = 2
        Margins.Top = 0
        Margins.Right = 2
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 287
      end
      object sbClearFilter: TPngSpeedButton [14]
        AlignWithMargins = True
        Left = 370
        Top = 0
        Width = 34
        Height = 29
        Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1092#1080#1083#1100#1090#1088
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        ParentShowHint = False
        ShowHint = True
        OnClick = sbClearFilterClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000002334944415478DA63FCFFFF3F032580
          91A1FD0A7E13FEA3A8C66A8005903E7E26499541969785285B1F7FFEC36032EF
          3688690931136AC8C9445506691EA021606F01A518FF33349F7C0F56526326C8
          C008147AFAE50F83F9BC3B20694B864A9D130847810CF9FBF7F8E1247506492E
          16B873BBCF400C2833166478F6ED0F83EDBC9B0C0CCCCC60CD98BE0219F2F3C7
          F13D49DA0CE2DC2C60C9C6A3AFC02E4A3714667059709D8181950DAE197BB080
          0CF9F2F9F896140386EBA7CF322C58B78FE1C6DD67C0B08484A68682C4774B5D
          A56573EBE253188CD3B0852BC4109B67878F5FBD729321CEDB92212DD096414A
          949FE1DAFDE70CF3361E6558B8E5388393A9C69D5D27AEA9623520A676EEFA95
          BBCE041C9C55C2A0F6EF13039B800003AF820258EED981030CA7BFB13244B7AF
          640872345C8FD50059AFF2DFD9618E2CE5F1EE0C6F2F5C60D81910C0E0BE6103
          C3CD050B187E7DF8C0E000A40BFB5631ACDF7FE10F5603184DD2FFBFDFDFCFC0
          CFC309E6830CD9ECE0C02005C46E408340E0FCCDC70CA6B16D0C380D78BABD93
          4152841FCC3F909000B6F90DD020904B840D0C188E5DBACB6097D2C380D30BA1
          2EC62CBD85A160CD20007236B2774A365E64D877FAC61F9C81B8E9E0C580355D
          190CAEE69A28727FFFFD6398B1E61043C594750C810E38021114BF36663AFFCF
          5EBCC9E065ADCB90E46FCDA02E2FCE70F9CE538659EB0E3300A38FC1D90C1A8D
          D8B233300CC074B2BFF5ECE397EF45DF78F0821326A72E2FF1DD4A4F69293021
          A5822C02009244E8A4AC1C3E1E0000000049454E44AE426082}
      end
      object sbSpeedMove: TPngSpeedButton [15]
        AlignWithMargins = True
        Left = 108
        Top = 0
        Width = 34
        Height = 29
        Hint = #1041#1099#1089#1090#1088#1086#1077' '#1088#1072#1079#1084#1077#1097#1077#1085#1080#1077' '#1074#1072#1075#1086#1085#1086#1074
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        OnClick = sbSpeedMoveClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C000002934944415478DAA593EF4B535118C7BFDBDAD5EE36C5B5
          A6C898A4B3226515444A8AFD422305A11F145111D18BFE800C7A11BE497C5341
          7F43D08B88240AA35F8689F85BA745C3663A5DFEDEA64EB739CFBDE79CCE9D48
          CAF6AE0B0FE7DC87FBFD3CCFFD9EE7E838E7F89F47B71D10F69C7ECE19BFA9E5
          386300132BDDDC27636B4F59C0716EA82005101A3C45B3F75DD70B0840B50FE9
          BF5029E2A17E2422635121B6A47420C48F244BF1C3DDD6E320E1167045055314
          70A2C068BB88C4B217F1B0870AF1AEB4BF10ECAFDAC82EB822B1841FEADA0F21
          DC0418F7D4834467110B0E82925597B376783C0510ECABBA6C34395FC9F62A28
          A116305155131BE4A3803E07D1857E0199A973D60DBF4F6BE2624F65D49C5F6D
          3218B304A0558845754220EDBD84D8E200D697BCF79D75234FD29EC242774599
          41B2F5581CE741426F93D5B9A8CE120948B917101E7BC185EBE4D9AA2923A612
          DC93E31B86A4C12C527CE377AE6EBEF3C4826CAFB04B66A79614A1261D87B086
          AB0C4CD5DE153CF8DE8A3CEB7E786786D06852E20685BA0FDCF68FEBE63ACA57
          C4D9666E3BE3E4FA74392B833302C2541001CCB716C3EDA844DFE4578C4CF7B0
          844AB23B1A96A2BA749338F5CEDDFE38229FAC2EB905CA9918090A81C57C641A
          39263BBA27BE6030F04D8358D30226DF94F2A61509B5EE3B980A8F41115DA84C
          814215E8F512EC16073A273EA2D7DF49530042ECC9B4B88E34F87DA83F7C5708
          6932B44EE6220158CD79F0FCE9C2E7D10F41312A353B00426C06D7AF693E3484
          E258278413E183681545B6125D79610D06025D68F37D9A17E2B3B3CDDCBB03E0
          7F7DE827A3CC260045AE6BBEE8F6CE4A9BF4B163CE3372BBAF6D86B0A4F857CA
          651A7F79B0ACE8EA686FBA81296CD4C52833C8EB8C162E3673FF56FE2F0F1BBA
          1D5DA98AF10000000049454E44AE426082}
      end
      object Bevel6: TBevel [16]
        AlignWithMargins = True
        Left = 446
        Top = 0
        Width = 2
        Height = 29
        Margins.Left = 2
        Margins.Top = 0
        Margins.Right = 2
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 512
      end
      object btMark: TPngSpeedButton [17]
        AlignWithMargins = True
        Left = 410
        Top = 0
        Width = 34
        Height = 29
        Hint = #1054#1090#1084#1077#1090#1080#1090#1100' '#1074#1072#1075#1086#1085#1099
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = btMarkClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000010000000100804000000B5FA37
          EA0000000467414D410000AFC837058AE90000001974455874536F6674776172
          650041646F626520496D616765526561647971C9653C000001404944415478DA
          95D14F28C37118C7F1EFAEE6CF455BDAC146CB615294CDC1142592A39313E5EA
          C0C9056D38EC20B9319385A62CA295B6FD66535316357F96839B95A2AD96C8F3
          79D67EBFC661B199C8737E3D87E7FDA8F2E2F75195038F0E714E62126FD3973F
          806D03C790613525AA07EE6797EDDFC0960EA77C033DD96CFBE31D08BB2A4A80
          BB367B8E5B92D96D3F1462D4C4A19DBA22B0A1C5DDC77E1A3E0AF01145D14A6D
          BE2FB05ECF714491A20BDE231FA254C9BDB9CE40BA005C46445822C619BC74CD
          07A445B76C093E16AE701A38023FBDF2098571451E3472BFD21E4C163AAC3621
          467E1E262F4C54C34EE8B3837283F4F2196AB18FD7609E7F1262C4810459733D
          8A45CA14959C6B21899B97D2420C1979266756ACA15449EA29357629CF13A4C1
          8AAC56BA8E1FCA7E315645562CB04671C89BE1E73F9FF54FF00E785CBD01186A
          563C0000000049454E44AE426082}
      end
      object Bevel8: TBevel [18]
        AlignWithMargins = True
        Left = 406
        Top = 0
        Width = 2
        Height = 29
        Margins.Left = 2
        Margins.Top = 0
        Margins.Right = 2
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 402
      end
      object sbWagon: TPngSpeedButton [19]
        AlignWithMargins = True
        Left = 450
        Top = 0
        Width = 34
        Height = 29
        Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100' '#1074#1072#1075#1086#1085
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = N21Click
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C08648800000009704859730000004E000000
          4E01B1CD1F330000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000002964944415478DA63FCFFFF3F032580
          515694EB9A102FDBBE8BF73EE4800454657882248439EBB8D958FE56C6689F61
          60F8CFD0B6E49AC9D71F7F985FBDFFD178EBC9E7F52075FA4A0253DE7DF9EDC8
          28CECFF59EE11F0383381BCF1990C48B1F5F746CD4C599589999509CF6FBEF3F
          C623375FFE93E0E0B902E2BFFCF3C58481F13F03A3B7BCF2C3024373C95B5F3E
          FE26C5E96A3CFCAC13CE9F7C0E36405A5787C1D4DF47EEE8D1A30C5FBE7C6108
          0A0A6298376F1E83A5A52558F1F1E3C71992929218D6AD5BC7C0C3C3C3606D6D
          CDB07AE6ECD7ACAFDF7E071B909696FE4ED1D7C360C3860D0C1F3F7E64282828
          60A8AAAA62F0F5F5051BB079F36686B6B6368609132630F0F3F33304040430D4
          6466BFF8FBE4D92FB0016D75F5CF194D0DCCC9368059468AAD65FA5489813340
          405C8CE70327FBDFA7AF5EB2BD78FB9A5D535DE9EBBB771F598585F9C131F3F6
          ED47562121FEDFD76FDEE3961016FD292D26FE4BE0FB4F66FE6F3F5F820DD8E2
          152277E0F9E33FBB5F3EFFACCF27C03FFDC1C977F31A8C45142579C02EB8F7EC
          0B434AE3D9B7190AE682173F7DF8E8262EC56B2F29C3B2E1FEADC36003025575
          8416FDF9FD4741555BE0F299A3EF2AC42478D7FD38FB7145A39508C880F0BA63
          6F82388CF9BBDFBEFCAC6D642574FFF6958F896CECCC860282C719C539B99F33
          7370B2D9B87BB1BDB8BE9351DE34866BEFEA791F7EFFFBC168A927FC0E9C0E2E
          BD156261E2F8EF129A24F0F0F4926F129AEEFF0FEFDCF6EBDFCFEF3F19410A84
          452517FA06C7C73CBF73F23D9F8426CF9EEDAB1BDFBF7DDD8E9CF2048545AB5D
          BDC26B3F3EBFFA4542C95470CBFA458BDEBE7E91C80895E71111935ACCCDCDA3
          044C8967DEBE7E960614FB8B967A998545A5660353A2F1F7AF5FEEBE7AF52C16
          28F69591D2EC0C0084EB53DA59EB79510000000049454E44AE426082}
      end
      object Bevel7: TBevel [20]
        AlignWithMargins = True
        Left = 332
        Top = 0
        Width = 2
        Height = 29
        Margins.Left = 2
        Margins.Top = 0
        Margins.Right = 2
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 330
        ExplicitTop = -3
      end
      object Bevel9: TBevel [21]
        AlignWithMargins = True
        Left = 486
        Top = 0
        Width = 2
        Height = 29
        Margins.Left = 2
        Margins.Top = 0
        Margins.Right = 2
        Margins.Bottom = 0
        Align = alLeft
        ExplicitLeft = 540
      end
      inherited plCount: TPanel
        Left = 703
        TabOrder = 2
        ExplicitLeft = 703
      end
      object cbEmptyOnly: TDBCheckBoxEh
        AlignWithMargins = True
        Left = 510
        Top = 3
        Width = 168
        Height = 23
        Margins.Left = 20
        Align = alLeft
        Caption = #1058#1086#1083#1100#1082#1086' '#1074#1072#1075#1086#1085#1099' '#1073#1077#1079' '#1087#1086#1075#1088#1091#1079#1082#1080
        DynProps = <>
        TabOrder = 0
        OnClick = cbEmptyOnlyClick
      end
      object cdEnlightEqual: TDBCheckBoxEh
        AlignWithMargins = True
        Left = 691
        Top = 3
        Width = 202
        Height = 23
        Margins.Left = 10
        Align = alLeft
        Caption = #1055#1086#1076#1089#1074#1077#1095#1080#1074#1072#1090#1100' '#1086#1076#1080#1085#1072#1082#1086#1074#1099#1077' '#1079#1072#1103#1074#1082#1080
        DynProps = <>
        TabOrder = 1
        OnClick = cdEnlightEqualClick
      end
    end
    inherited dgData: TDBGridEh
      Top = 47
      Width = 929
      Height = 493
      AllowedOperations = [alopUpdateEh]
      AutoFitColWidths = True
      Border.EdgeBorders = [ebLeft, ebTop, ebRight]
      ColumnDefValues.Title.TitleButton = False
      DataGrouping.Active = True
      DataGrouping.DefaultStateExpanded = True
      DataGrouping.GroupLevels = <
        item
          ColumnName = 'Column_0_path_code'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end>
      DefaultDrawing = False
      GridLineParams.DataHorzColor = 13488071
      GridLineParams.DataVertColor = clDefault
      GridLineParams.DataVertLines = False
      IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh, gioShowRowselCheckboxesEh]
      IndicatorParams.VertLineColor = 13488071
      Options = [dgEditing, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      OptionsEh = [dghAutoSortMarking, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize]
      ReadOnly = False
      RowHeight = 20
      SearchPanel.ShortCut = 32838
      SelectionDrawParams.SelectionStyle = gsdsDefaultEh
      SelectionDrawParams.DrawFocusFrame = True
      STFilter.HorzLineColor = 13488071
      STFilter.VertLineColor = 13488071
      TitleParams.Font.Height = -12
      TitleParams.Font.Name = 'Calibri'
      TitleParams.HorzLineColor = clMedGray
      TitleParams.MultiTitle = True
      OnCellClick = dgDataCellClick
      OnDblClick = nil
      OnTitleBtnClick = nil
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'path_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = ':'
          Visible = False
          Width = 97
          WordWrap = False
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'order_on_path'
          Footers = <>
          MaxWidth = 32
          MinWidth = 32
          ReadOnly = True
          Title.Caption = 'N'
          Visible = False
          Width = 32
          WordWrap = False
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num_source'
          Footers = <>
          MinWidth = 60
          Title.Caption = #1050#1055'/'#1087#1088#1080#1073
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Calibri'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 60
          WordWrap = False
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Footers = <>
          MinWidth = 60
          Title.Caption = #1050#1055'/'#1086#1090#1087#1088
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Calibri'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 60
          WordWrap = False
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'mark'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          Footers = <>
          MinWidth = 10
          Title.Caption = #1054#1090#1084#1077#1090#1082#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = 'Calibri'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 80
          WordWrap = False
        end
        item
          AutoFitColWidth = False
          ButtonStyle = cbsNone
          CellButtons = <
            item
              OnClick = dgDataColumns5CellButtons0Click
            end>
          DynProps = <>
          EditButton.Visible = False
          EditButtons = <>
          FieldName = 'pnum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Footers = <>
          MinWidth = 68
          ReadOnly = True
          Title.Caption = #1042#1072#1075#1086#1085
          Width = 68
          WordWrap = False
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
          Width = 78
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          Title.Caption = #1058#1080#1087
          Width = 48
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'model_code'
          Footers = <>
          Title.Caption = #1052#1086#1076#1077#1083#1100
          Width = 54
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'date_income'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1087#1086#1076#1072#1095#1080
          Width = 74
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButton.Style = ebsMinusEh
          EditButton.Visible = True
          EditButton.OnClick = dgDataColumns2EditButtonClick
          EditButtons = <>
          FieldName = 'container1'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088' 1'
          Width = 154
          WordWrap = False
          OnEditButtonClick = dgDataColumns2EditButtonClick
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButton.Style = ebsMinusEh
          EditButton.Visible = True
          EditButton.OnClick = dgDataColumns2EditButtonClick
          EditButtons = <>
          FieldName = 'container2'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088' 2'
          Width = 131
          WordWrap = False
          OnEditButtonClick = dgDataColumns2EditButtonClick
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButton.Style = ebsMinusEh
          EditButton.Visible = True
          EditButton.OnClick = dgDataColumns2EditButtonClick
          EditButtons = <>
          FieldName = 'container3'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088' 3'
          Width = 143
          WordWrap = False
          OnEditButtonClick = dgDataColumns2EditButtonClick
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButton.Style = ebsMinusEh
          EditButton.Visible = True
          EditButton.OnClick = dgDataColumns2EditButtonClick
          EditButtons = <>
          FieldName = 'container4'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088' 4'
          Width = 129
          WordWrap = False
          OnEditButtonClick = dgDataColumns2EditButtonClick
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_length'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsItalic]
          Footers = <>
          MaxWidth = 48
          MinWidth = 48
          ReadOnly = True
          Title.Caption = 'L'
          Width = 48
          WordWrap = False
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          MaxWidth = 150
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Visible = False
          Width = 100
          WordWrap = False
        end>
    end
    inherited plHint: TPanel
      Width = 273
      Height = 41
      TabOrder = 4
      ExplicitWidth = 273
      ExplicitHeight = 41
      inherited lbText: TLabel
        Width = 265
        Height = 33
      end
    end
    object dgOrder: TDBGridEh
      Left = 0
      Top = 540
      Width = 929
      Height = 48
      Align = alBottom
      Constraints.MaxHeight = 48
      DataSource = dsOrder
      DynProps = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      GridLineParams.BrightColor = clSilver
      GridLineParams.DarkColor = clGray
      GridLineParams.DataHorzColor = 13488071
      GridLineParams.DataVertColor = 13488071
      GridLineParams.VertEmptySpaceStyle = dessNonEh
      HorzScrollBar.VisibleMode = sbNeverShowEh
      IndicatorOptions = []
      IndicatorParams.FillStyle = cfstThemedEh
      Options = [dgEditing, dgTitles, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind]
      ParentFont = False
      TabOrder = 2
      TitleParams.FillStyle = cfstThemedEh
      TitleParams.Font.Charset = DEFAULT_CHARSET
      TitleParams.Font.Color = clWindowText
      TitleParams.Font.Height = -13
      TitleParams.Font.Name = 'Calibri'
      TitleParams.Font.Style = [fsBold]
      TitleParams.HorzLineColor = 13488071
      TitleParams.ParentFont = False
      TitleParams.VertLineColor = 13488071
      OnDrawColumnCell = dgOrderDrawColumnCell
      Columns = <
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088
          Width = 83
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072
          Width = 87
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1082#1086#1085#1090'.'
          Width = 60
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          Title.Caption = #1042#1083#1072#1076#1077#1083#1077#1094
          Width = 147
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'amount'
          Footers = <>
          Title.Caption = #1050#1086#1083'-'#1074#1086
          Width = 49
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'rest_amount'
          Footers = <>
          Title.Caption = #1054#1089#1090#1072#1090#1086#1082
          Width = 49
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'station_code'
          Footers = <>
          Title.Caption = #1057#1090#1072#1085#1094#1080#1103' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1103
          Width = 144
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 158
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'payer_code'
          Footers = <>
          Title.Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
          Width = 158
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Width = 250
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object plStatus: TPanel
      Left = 0
      Top = 29
      Width = 929
      Height = 18
      Align = alTop
      BevelOuter = bvNone
      Color = 15921906
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 16580608
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 3
      StyleElements = [seBorder]
    end
  end
  inherited pmGrid: TPopupMenu
    object N24: TMenuItem [0]
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1103#1095#1077#1081#1082#1080
      ShortCut = 46
      OnClick = N24Click
    end
    object N23: TMenuItem [1]
      Caption = '-'
    end
    inherited N3: TMenuItem
      Enabled = False
      Visible = False
    end
    object N21: TMenuItem [5]
      Caption = #1055#1088#1072#1074#1080#1090#1100' '#1074#1072#1075#1086#1085
      OnClick = N21Click
    end
    object N20: TMenuItem [6]
      Caption = '-'
    end
    object N8: TMenuItem [7]
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1046#1044' '#1087#1091#1090#1100
      OnClick = sbPathClick
    end
    object N22: TMenuItem [8]
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1082#1086#1083#1086#1085#1082#1091' ('#1087#1086' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1084')'
      OnClick = N22Click
    end
    object N19: TMenuItem [9]
      Caption = #1055#1088#1086#1089#1090#1072#1074#1080#1090#1100' '#1086#1089#1084#1086#1090#1088' ('#1087#1086' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1084')'
      OnClick = N19Click
    end
    object N12: TMenuItem [10]
      Caption = '-'
    end
    object N13: TMenuItem [11]
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1082#1086#1085#1090#1077#1081#1085#1077#1088' '#1089' '#1085#1086#1084#1077#1088#1086#1084' '#1080#1079' '#1079#1072#1103#1074#1082#1080
      ShortCut = 16465
      OnClick = N13Click
    end
    object N11: TMenuItem [12]
      Caption = '-'
    end
    object N9: TMenuItem [13]
      Caption = #1057#1084#1077#1089#1090#1080#1090#1100' '#1074#1087#1088#1072#1074#1086' '#1074#1089#1077
      ShortCut = 16423
      OnClick = N9Click
    end
    object N10: TMenuItem [14]
      Caption = #1057#1084#1077#1089#1090#1080#1090#1100' '#1074#1083#1077#1074#1086' '#1074#1089#1077
      ShortCut = 16421
      OnClick = N10Click
    end
    object N14: TMenuItem [15]
      Caption = #1057#1084#1077#1089#1090#1080#1090#1100' '#1074#1087#1088#1072#1074#1086' '#1082#1086#1085#1090#1077#1081#1085#1077#1088
      ShortCut = 24615
      OnClick = N14Click
    end
    object N15: TMenuItem [16]
      Caption = #1057#1084#1077#1089#1090#1080#1090#1100' '#1074#1083#1077#1074#1086' '#1082#1086#1085#1090#1077#1081#1085#1077#1088
      ShortCut = 24613
      OnClick = N15Click
    end
    object N16: TMenuItem [17]
      Caption = '-'
    end
    object N17: TMenuItem [18]
      Caption = #1057#1084#1077#1089#1090#1080#1090#1100' '#1074#1072#1075#1086#1085' '#1074#1074#1077#1088#1093
      ShortCut = 16422
      OnClick = N17Click
    end
    object N18: TMenuItem [19]
      Caption = #1057#1084#1077#1089#1090#1080#1090#1100' '#1074#1072#1075#1086#1085' '#1074#1085#1080#1079
      ShortCut = 16424
      OnClick = N18Click
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'declare @foots varchar(50), @stations varchar(50), @onlyempty bi' +
        't, @guid varchar(40);'
      ''
      
        'select @foots = :foots, @stations = :stations, @onlyempty = :onl' +
        'yempty, @guid = :guid;'
      ''
      'select '
      
        'id, folder_id, train_num,  train_num_source, load_plan_id, isdel' +
        'eted, mark, note, carriage_id, '
      
        'pnum, need_inspection, order_on_path, path_code, isdefective, da' +
        'te_next_repair, urgent_repair_sign, '
      
        'owner_code, kind_code, model_code, date_income, car_length, car_' +
        'code,'
      
        'SUBSTRING(container1,1,2) as cont1marks, SUBSTRING(container1,3,' +
        '255) as container1, orderspecid1,'
      
        'SUBSTRING(container2,1,2) as cont2marks, SUBSTRING(container2,3,' +
        '255) as container2, orderspecid2,'
      
        'SUBSTRING(container3,1,2) as cont3marks, SUBSTRING(container3,3,' +
        '255) as container3, orderspecid3,'
      
        'SUBSTRING(container4,1,2) as cont4marks, SUBSTRING(container4,3,' +
        '255) as container4, orderspecid4'
      'from'
      '('
      
        'select j.id, j.folder_id,  j.train_num, j.train_num_source, j.lo' +
        'ad_plan_id, j.isdeleted, j.order_on_path, j.mark, j.note, '
      
        'car.id as carriage_id, car.pnum, car.need_inspection, car.date_n' +
        'ext_repair, (case when DATEDIFF(M, getdate(), car.date_next_repa' +
        'ir)<2 or isnull(car.note,'#39#39')<>'#39#39' then 1 else 0 end) as urgent_re' +
        'pair_sign, '
      
        'ph.code as path_code, dbo.grtint(isnull(car.isdefective,0), isnu' +
        'll(tc.isdefective,0)) as isdefective,'
      
        '(select code from counteragents cr where cr.id = o.owner_id) as ' +
        'owner_code,'
      'k.code as kind_code,'
      'm.model_code,'
      'm.car_length,'
      'car.date_income,'
      
        'car.pnum +'#39' ( '#39'+isnull(k.code,'#39#39')+'#39' ), '#39'+isnull(m.model_code,'#39#39')' +
        '+'#39', '#39'+isnull(convert(varchar(20), date_income, 104),'#39#39') as car_c' +
        'ode,'
      '('
      '  select dbo.GetContainerInfo(l.id)'
      '  from docload l where l.joint_id = j.id and l.order_num = 1'
      ') as container1,'
      
        '(select orderspec_id from docload l where l.joint_id = j.id and ' +
        'l.order_num = 1) as orderspecid1,'
      '('
      '  select dbo.GetContainerInfo(l.id) '
      ' from docload l where l.joint_id = j.id and l.order_num = 2'
      ') as container2,'
      
        '(select orderspec_id from docload l where l.joint_id = j.id and ' +
        'l.order_num = 2) as orderspecid2,'
      '('
      '  select dbo.GetContainerInfo(l.id) '
      '  from docload l where l.joint_id = j.id and l.order_num = 3'
      ') as container3,'
      
        '(select orderspec_id from docload l where l.joint_id = j.id and ' +
        'l.order_num = 3) as orderspecid3,'
      '('
      '  select dbo.GetContainerInfo(l.id) '
      '  from docload l where l.joint_id = j.id and l.order_num = 4'
      ') as container4,'
      
        '(select orderspec_id from docload l where l.joint_id = j.id and ' +
        'l.order_num = 4) as orderspecid4'
      'from dbo.docloadjoint j'
      'left outer join paths ph on (ph.id = j.path_id),'
      'objects o, carriages car'
      'left outer join carriagekinds k on (car.kind_id = k.id)'
      'left outer join carriagemodels m on (car.model_id = m.id)'
      'left outer join techconditions tc on (car.techcond_id = tc.id)'
      'where o.id = car.id and j.carriage_id = car.id'
      ''
      
        'and (@foots = '#39#39' or charindex('#39','#39'+convert(varchar(10), car.kind_' +
        'id)+'#39','#39', @foots)>0)'
      ''
      'and ('
      '   @stations = '#39#39' '
      '   or '
      
        '   exists (select 1 from docload l1 where l1.joint_id = j.id and' +
        ' charindex('#39','#39'+convert(varchar(10), l1.station_id)+'#39','#39', @station' +
        's)>0)'
      ')'
      ''
      'and ('
      '   @onlyempty = 0 '
      '   or '
      
        '   not exists (select 1 from docload l1 where l1.joint_id = j.id' +
        ')'
      ')'
      '/*'
      'and ('
      
        '   exists  (select 1 from docload l left outer join v_lastobject' +
        'states ls on (ls.task_id = l.task_id and ls.object_id = l.contai' +
        'ner_id) where l.joint_id = j.id and  isnull(ls.object_state_id,0' +
        ') <> 12)'
      '   or '
      '   not exists (select 1 from docload l where l.joint_id = j.id)'
      ')'
      '*/'
      ''
      'and isnull(isactive,0) = 1'
      ''
      'and'
      '('
      '  not exists (select 1 from dbo.f_GetFilterList(0, @guid) fl1)'
      
        '  or exists (select 1 from dbo.f_GetFilterList(0, @guid) fl wher' +
        'e charindex(fl.v, car.pnum)>0) '
      
        '  or exists (select 1 from docload l, containers ct,dbo.f_GetFil' +
        'terList(0, @guid) fl where l.joint_id = j.id and l.container_id ' +
        '= ct.id and charindex(fl.v, ct.cnum)>0) '
      ')'
      ''
      ') s1'
      'order by path_code, order_on_path')
    SelectCommand.Parameters = <
      item
        Name = 'foots'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'stations'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'onlyempty'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'guid'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    UpdateCommand.CommandText.Strings = (
      'update docloadjoint'
      'set'
      ' carriage_id = :carriage_id,'
      ' isdeleted = :isdeleted,'
      ' train_num = :train_num,'
      ' train_num_source = :train_num_source,'
      ' mark = :mark,'
      ' note = :note'
      'where'
      '  id = :id'
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'carriage_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isdeleted'
        Size = -1
        Value = Null
      end
      item
        Name = 'train_num'
        Size = -1
        Value = Null
      end
      item
        Name = 'train_num_source'
        Size = -1
        Value = Null
      end
      item
        Name = 'mark'
        Size = -1
        Value = Null
      end
      item
        Name = 'note'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      
        'declare @carriage_id int, @folder_id int, @train_num_source varc' +
        'har(50); '
      ''
      
        'select @carriage_id = :carriage_id , @folder_id = :folder_id, @t' +
        'rain_num_source = :train_num_source; '
      ''
      
        'insert into docloadjoint (carriage_id, folder_id, train_num_sour' +
        'ce)'
      'select @carriage_id, @folder_id, @train_num_source'
      'where not exists ('
      
        'select 1 from docloadjoint j, docload l left outer join v_lastob' +
        'jectstates ls on (ls.task_id = l.task_id and ls.object_id = l.co' +
        'ntainer_id) where j.carriage_id = @carriage_id and l.joint_id = ' +
        'j.id and isnull(ls.object_state_id,0) <> 12'
      ')'
      
        'and not exists (select 1 from docloadjoint j where j.carriage_id' +
        ' = @carriage_id and not exists (select 1 from docload l where l.' +
        'joint_id = j.id));'
      '')
    InsertCommand.Parameters = <
      item
        Name = 'carriage_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'train_num_source'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from docloadjoint where id = :id and isdeleted=1;')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select '
      
        'id, folder_id, train_num,  train_num_source, load_plan_id, isdel' +
        'eted, mark, note, carriage_id, '
      
        'pnum, need_inspection, order_on_path, path_code, isdefective, da' +
        'te_next_repair, urgent_repair_sign, '
      
        'owner_code, kind_code, model_code, date_income, car_length, car_' +
        'code,'
      
        'SUBSTRING(container1,1,2) as cont1marks, SUBSTRING(container1,3,' +
        '255) as container1, orderspecid1,'
      
        'SUBSTRING(container2,1,2) as cont2marks, SUBSTRING(container2,3,' +
        '255) as container2, orderspecid2,'
      
        'SUBSTRING(container3,1,2) as cont3marks, SUBSTRING(container3,3,' +
        '255) as container3, orderspecid3,'
      
        'SUBSTRING(container4,1,2) as cont4marks, SUBSTRING(container4,3,' +
        '255) as container4, orderspecid4'
      'from'
      '('
      
        'select j.id, j.folder_id,  j.train_num, j.train_num_source, j.lo' +
        'ad_plan_id, j.isdeleted, j.order_on_path, j.mark, j.note, '
      
        'car.id as carriage_id, car.pnum, car.need_inspection, car.date_n' +
        'ext_repair, (case when DATEDIFF(M, getdate(), car.date_next_repa' +
        'ir)<2 or isnull(car.note,'#39#39')<>'#39#39' then 1 else 0 end) as urgent_re' +
        'pair_sign, '
      
        'ph.code as path_code, dbo.grtint(isnull(car.isdefective,0), isnu' +
        'll(tc.isdefective,0)) as isdefective,'
      
        '(select code from counteragents cr where cr.id = o.owner_id) as ' +
        'owner_code,'
      'k.code as kind_code,'
      'm.model_code,'
      'm.car_length,'
      'car.date_income,'
      
        'car.pnum +'#39' ( '#39'+isnull(k.code,'#39#39')+'#39' ), '#39'+isnull(m.model_code,'#39#39')' +
        '+'#39', '#39'+isnull(convert(varchar(20), date_income, 104),'#39#39') as car_c' +
        'ode,'
      '('
      '  select dbo.GetContainerInfo(l.id)'
      '  from docload l where l.joint_id = j.id and l.order_num = 1'
      ') as container1,'
      
        '(select orderspec_id from docload l where l.joint_id = j.id and ' +
        'l.order_num = 1) as orderspecid1,'
      '('
      '  select dbo.GetContainerInfo(l.id) '
      ' from docload l where l.joint_id = j.id and l.order_num = 2'
      ') as container2,'
      
        '(select orderspec_id from docload l where l.joint_id = j.id and ' +
        'l.order_num = 2) as orderspecid2,'
      '('
      '  select dbo.GetContainerInfo(l.id) '
      '  from docload l where l.joint_id = j.id and l.order_num = 3'
      ') as container3,'
      
        '(select orderspec_id from docload l where l.joint_id = j.id and ' +
        'l.order_num = 3) as orderspecid3,'
      '('
      '  select dbo.GetContainerInfo(l.id) '
      '  from docload l where l.joint_id = j.id and l.order_num = 4'
      ') as container4,'
      
        '(select orderspec_id from docload l where l.joint_id = j.id and ' +
        'l.order_num = 4) as orderspecid4'
      'from dbo.docloadjoint j'
      'left outer join paths ph on (ph.id = j.path_id),'
      'objects o, carriages car'
      'left outer join carriagekinds k on (car.kind_id = k.id)'
      'left outer join carriagemodels m on (car.model_id = m.id)'
      'left outer join techconditions tc on (car.techcond_id = tc.id)'
      'where o.id = car.id and j.carriage_id = car.id'
      'and j.id = :current_id'
      ') s1'
      '')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 96
  end
  inherited meData: TMemTableEh
    AfterDelete = meDataAfterDelete
  end
  object meOrder: TMemTableEh
    Params = <>
    DataDriver = drvOrder
    Left = 224
    Top = 480
  end
  object dsOrder: TDataSource
    DataSet = meOrder
    Left = 280
    Top = 480
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select * from docload l where joint_id = :joint_id and order_num' +
        ' = :order_num')
    SelectCommand.Parameters = <
      item
        Name = 'joint_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'order_num'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'update docload'
      'set'
      '  joint_id = :joint_id,'
      '  order_num = :order_num,'
      '  container_id = :container_id,'
      '  isfreeload = :isfreeload,'
      '  kind_id = :kind_id,'
      '  owner_id = :owner_id,'
      '  station_id = :station_id,'
      '  note = :note,'
      '  task_id = :task_id,'
      '  orderspec_id = :orderspec_id'
      'where'
      '  id = :id;')
    UpdateCommand.Parameters = <
      item
        Name = 'joint_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'order_num'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isfreeload'
        DataType = ftBoolean
        Size = -1
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'station_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'task_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'orderspec_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'begin'
      ''
      '  declare @newid int, @doctypeid int;'
      ''
      
        '  select @doctypeid = id from doctypes where system_section='#39'out' +
        'come_loads'#39';'
      ''
      
        '  insert into documents (doctype_id, folder_id) values (@doctype' +
        'id, :folder_id);'
      ''
      '  select @newid = IDENT_CURRENT('#39'documents'#39');'
      ''
      'insert into docload'
      
        '  (id, joint_id, order_num, container_id, isfreeload, kind_id, o' +
        'wner_id, station_id, note, task_id, orderspec_id)'
      'values'
      
        '  (@newid, :joint_id, :order_num, :container_id, :isfreeload, :k' +
        'ind_id, :owner_id, :station_id, :note, :task_id, :orderspec_id);'
      ''
      'end;')
    InsertCommand.Parameters = <
      item
        Name = 'folder_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'joint_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'order_num'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'isfreeload'
        DataType = ftBoolean
        Size = -1
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'station_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 250
        Value = Null
      end
      item
        Name = 'task_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'orderspec_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'declare @id int;'
      ''
      
        'select @id = id from docload where joint_id = :joint_id and orde' +
        'r_num = :order_num;'
      ''
      'delete from documents where id = @id; ')
    DeleteCommand.Parameters = <
      item
        Name = 'joint_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'order_num'
        Size = -1
        Value = Null
      end>
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 350
    Top = 480
  end
  object meSpec: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvSpec
    AfterPost = meSpecAfterPost
    Left = 408
    Top = 480
  end
  object drvOrder: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select  s.id as id, o.*,  d.doc_number, d.doc_date,  s.container' +
        '_kind_id, s.container_owner_id,'
      
        '(select code from counteragents cr where cr.id = s.container_own' +
        'er_id) as owner_code,'
      
        '(select code from containerkinds k where k.id  = s.container_kin' +
        'd_id) as kind_code,'
      
        '(select name from stations st where st.id = o.station_id) as sta' +
        'tion_code,'
      
        '(select code from counteragents c where c.id = o.consignee_id) a' +
        's consignee_code,'
      
        '(select code from counteragents c1 where c1.id = o.payer_id) as ' +
        'payer_code,'
      'o.note as task_note,'
      
        'isnull(s.amount,0) as amount, isnull(s.amount,0)-isnull(s.used_a' +
        'mount,0) as rest_amount'
      'from docorder o, docorderspec s, documents d'
      'where s.doc_id = o.id and d.id = o.id'
      'and s.id = :current_id')
    SelectCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'joint_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 160
    Top = 480
  end
  object drvCarriages: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select c.*, o.start_datetime,  o.id as object_id, '
      
        'isnull(c.train_num, (select train_num from doccargosheet sh wher' +
        'e sh.id = (select max(id) from doccargosheet sh1 where sh1.carri' +
        'age_num = c.pnum))) as train_num,'
      
        '(select convert(varchar, pl.plan_date, 104)+'#39', '#39'+isnull(pl.train' +
        '_num,'#39#39')+'#39', '#39'+ isnull((select code from dispatchkinds k where k.' +
        'id = pl.dispatch_kind_id),'#39#39') from loadplan pl where pl.id = c.c' +
        'urrent_load_id) as load_plan_text,'
      
        '(select code from counteragents c1 where c1.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from carriagekinds k where k.id = c.kind_id) as kin' +
        'd_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select model_code from carriagemodels m where m.id = c.model_id' +
        ') as model_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code'
      
        'from objects o, carriages c where o.id = c.id and o.object_type ' +
        '= '#39'carriage'#39
      'and c.id = :carriage_id')
    SelectCommand.Parameters = <
      item
        Name = 'carriage_id'
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update carriages'
      'set'
      '  pnum = :pnum,'
      '  kind_id = :kind_id,'
      '  techcond_id = :techcond_id,'
      '  model_id = :model_id,'
      '  note = :note,'
      '  need_inspection = :need_inspection,'
      '  isdefective = :isdefective, '
      '  date_next_repair = :date_next_repair'
      'where'
      '  id = :id'
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'pnum'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'techcond_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'model_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'need_inspection'
        Size = -1
        Value = Null
      end
      item
        Name = 'isdefective'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'date_next_repair'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.CommandText.Strings = (
      'select c.*, o.start_datetime,  o.id as object_id, '
      
        'isnull(c.train_num, (select train_num from doccargosheet sh wher' +
        'e sh.id = (select max(id) from doccargosheet sh1 where sh1.carri' +
        'age_num = c.pnum))) as train_num,'
      
        '(select convert(varchar, pl.plan_date, 104)+'#39', '#39'+isnull(pl.train' +
        '_num,'#39#39')+'#39', '#39'+ isnull((select code from dispatchkinds k where k.' +
        'id = pl.dispatch_kind_id),'#39#39') from loadplan pl where pl.id = c.c' +
        'urrent_load_id) as load_plan_text,'
      
        '(select code from counteragents c1 where c1.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from carriagekinds k where k.id = c.kind_id) as kin' +
        'd_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select model_code from carriagemodels m where m.id = c.model_id' +
        ') as model_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code'
      
        'from objects o, carriages c where o.id = c.id and o.object_type ' +
        '= '#39'carriage'#39
      'and o.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 216
    Top = 296
  end
  object meCarriages: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvCarriages
    Left = 288
    Top = 296
  end
  object drvOrderInfo: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      
        'select  s.id as id, o.*,  d.doc_number, d.doc_date,  s.container' +
        '_kind_id, s.container_owner_id,'
      
        '(select code from counteragents cr where cr.id = s.container_own' +
        'er_id) as owner_code,'
      
        '(select code from containerkinds k where k.id  = s.container_kin' +
        'd_id) as kind_code,'
      
        '(select name from stations st where st.id = o.station_id) as sta' +
        'tion_code,'
      
        '(select code from counteragents c where c.id = o.consignee_id) a' +
        's consignee_code,'
      
        '(select code from counteragents c1 where c1.id = o.payer_id) as ' +
        'payer_code,'
      'o.note as task_note,'
      
        'isnull(s.amount,0) as amount, isnull(s.amount,0)-isnull(s.used_a' +
        'mount,0) as rest_amount'
      'from docorder o, docorderspec s, documents d'
      'where s.doc_id = o.id and d.id = o.id'
      
        'and s.id = (select orderspec_id from docload l where l.joint_id ' +
        '= :jointid and l.order_num = :ordernum)')
    SelectCommand.Parameters = <
      item
        Name = 'jointid'
        Size = -1
        Value = Null
      end
      item
        Name = 'ordernum'
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'joint_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.Parameters = <>
    Left = 504
    Top = 480
  end
  object meOrderInfo: TMemTableEh
    Params = <>
    DataDriver = drvOrderInfo
    Left = 568
    Top = 480
  end
  object dsOrderInfo: TDataSource
    DataSet = meOrderInfo
    Left = 624
    Top = 480
  end
end
