﻿inherited FormEditOder: TFormEditOder
  Caption = #1047#1072#1103#1074#1082#1072' '#1075#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1103
  ClientHeight = 594
  ClientWidth = 651
  OnActivate = FormActivate
  ExplicitWidth = 657
  ExplicitHeight = 622
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 553
    Width = 651
    ExplicitTop = 553
    ExplicitWidth = 814
    inherited btnCancel: TButton
      Left = 535
      ExplicitLeft = 698
    end
    inherited btnOk: TButton
      Left = 416
      ExplicitLeft = 579
    end
  end
  object plTop: TPanel [1]
    Left = 0
    Top = 0
    Width = 651
    Height = 335
    Align = alTop
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 814
    object Label8: TLabel
      Left = 19
      Top = 218
      Width = 116
      Height = 16
      Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
    end
    object sbConsignee: TSpeedButton
      Left = 573
      Top = 239
      Width = 52
      Height = 24
      Caption = '...'
      OnClick = sbConsigneeClick
    end
    object Label9: TLabel
      Left = 19
      Top = 276
      Width = 83
      Height = 16
      Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082
    end
    object sbPayer: TSpeedButton
      Left = 573
      Top = 297
      Width = 52
      Height = 24
      Caption = '...'
      OnClick = sbPayerClick
    end
    object Label2: TLabel
      Left = 19
      Top = 161
      Width = 123
      Height = 16
      Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
    end
    object sbForwarder: TSpeedButton
      Left = 573
      Top = 182
      Width = 52
      Height = 24
      Caption = '...'
      OnClick = sbForwarderClick
    end
    object Label1: TLabel
      Left = 19
      Top = 33
      Width = 119
      Height = 16
      Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    end
    object Label3: TLabel
      Left = 201
      Top = 33
      Width = 110
      Height = 16
      Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    end
    object Label4: TLabel
      Left = 353
      Top = 33
      Width = 101
      Height = 16
      Caption = #1055#1083#1072#1085#1086#1074#1072#1103' '#1076#1072#1090#1072
    end
    object Label5: TLabel
      Left = 505
      Top = 33
      Width = 126
      Height = 16
      Caption = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1072#1103' '#1076#1072#1090#1072
    end
    object Bevel1: TBevel
      Left = 0
      Top = 92
      Width = 873
      Height = 1
    end
    object Label6: TLabel
      Left = 19
      Top = 104
      Width = 89
      Height = 16
      Caption = #1053#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077
    end
    object SpeedButton1: TSpeedButton
      Left = 281
      Top = 126
      Width = 49
      Height = 24
      Caption = '...'
      OnClick = sbForwarderClick
    end
    object SpeedButton2: TSpeedButton
      Left = 573
      Top = 126
      Width = 52
      Height = 24
      Caption = '...'
      OnClick = sbForwarderClick
    end
    object Label10: TLabel
      Left = 334
      Top = 104
      Width = 141
      Height = 16
      Caption = #1057#1090#1072#1085#1094#1080#1103' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1103
    end
    object luConsignee: TDBLookupComboboxEh
      Left = 19
      Top = 240
      Width = 548
      Height = 24
      DynProps = <>
      DataField = 'consignee_id'
      DataSource = dsLocal
      EditButtons = <>
      KeyField = 'id'
      ListField = 'code'
      ListSource = FormCounteragents.dsData
      TabOrder = 0
      Visible = True
    end
    object luPayer: TDBLookupComboboxEh
      Left = 19
      Top = 298
      Width = 548
      Height = 24
      DynProps = <>
      DataField = 'payer_id'
      DataSource = dsLocal
      EditButtons = <>
      KeyField = 'id'
      ListField = 'code'
      ListSource = FormCounteragents.dsData
      TabOrder = 1
      Visible = True
    end
    object luForwarder: TDBLookupComboboxEh
      Left = 19
      Top = 183
      Width = 548
      Height = 24
      DynProps = <>
      DataField = 'forwarder_id'
      DataSource = dsLocal
      EditButtons = <>
      KeyField = 'id'
      ListField = 'code'
      ListSource = FormCounteragents.dsData
      TabOrder = 2
      Visible = True
    end
    object edDocNumber: TDBEditEh
      Left = 19
      Top = 55
      Width = 147
      Height = 24
      DataField = 'doc_number'
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      TabOrder = 3
      Visible = True
    end
    object dtDocDate: TDBDateTimeEditEh
      Left = 201
      Top = 55
      Width = 121
      Height = 24
      DataField = 'doc_date'
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      Kind = dtkDateEh
      TabOrder = 4
      Visible = True
    end
    object dtPlanDate: TDBDateTimeEditEh
      Left = 353
      Top = 55
      Width = 121
      Height = 24
      DataField = 'doc_plan'
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      Kind = dtkDateEh
      TabOrder = 5
      Visible = True
    end
    object dtFactDate: TDBDateTimeEditEh
      Left = 505
      Top = 55
      Width = 121
      Height = 24
      DataField = 'doc_fact'
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      Kind = dtkDateEh
      TabOrder = 6
      Visible = True
    end
    object stDocType: TDBEditEh
      Left = 0
      Top = 0
      Width = 651
      Height = 24
      Align = alTop
      DataSource = dsLocal
      DynProps = <>
      EditButtons = <>
      TabOrder = 7
      Visible = True
      ExplicitWidth = 814
    end
    object edObjectId: TDBLookupComboboxEh
      Left = 631
      Top = 240
      Width = 116
      Height = 24
      DynProps = <>
      DataField = ''
      EditButtons = <>
      TabOrder = 8
      Visible = True
    end
    object luDirection: TDBLookupComboboxEh
      Left = 19
      Top = 126
      Width = 256
      Height = 24
      DynProps = <>
      DataField = 'forwarder_id'
      DataSource = dsLocal
      EditButtons = <>
      KeyField = 'id'
      ListField = 'code'
      ListSource = FormCounteragents.dsData
      TabOrder = 9
      Visible = True
    end
    object luEndStation: TDBLookupComboboxEh
      Left = 334
      Top = 126
      Width = 233
      Height = 24
      DynProps = <>
      DataField = 'forwarder_id'
      DataSource = dsLocal
      EditButtons = <>
      KeyField = 'id'
      ListField = 'code'
      ListSource = FormCounteragents.dsData
      TabOrder = 10
      Visible = True
    end
  end
  object plTool: TPanel [2]
    Left = 0
    Top = 335
    Width = 651
    Height = 35
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitLeft = 8
    ExplicitTop = 8
    ExplicitWidth = 806
    object sbAddSpec: TPngSpeedButton
      AlignWithMargins = True
      Left = 1
      Top = 1
      Width = 28
      Height = 31
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100
      Margins.Left = 1
      Margins.Top = 1
      Margins.Right = 1
      Align = alLeft
      Flat = True
      Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      OnClick = sbAddSpecClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
        610000000473424954080808087C0864880000027E4944415478DA75934F6813
        5110C6E765379B6C9B5453A2A68A076911F1107B502C8855C845A820251EBC89
        083D14D4A207A18A9E7A1005BD2854144FB607050551C4D47AA8ED6A6D7B6804
        D14BC13F4D9A9AEC66B3D924BBD9B7CE8BCD33ADEDC0B061BE995FDEFB6697C0
        DF0861466083E8ECEC944686878E6CAD7CD9EB48AD9AB1A5FB697B7BC70CD3C8
        4ACFC19CAA7E208400CF9A4AC0711C9724EF817FF60ED17D5190B23F80CADB68
        FE40FFB55DDDA786EA802E55D51449F282AAAA50AD56F9BFFB53634E68EAA240
        2D027ACF13F04F3F049A1C073BB4DB4D1DBF7D940332CBCB8AECF7432E9703DB
        B66B4504396D1F073C56F627A158124EDC073AFD184A9F2740344C300EF73DE0
        8042A1A00882004B4B4B1C804F7BC7D8192F3D390AC1E0262072108B1524DB60
        BEBC0BBF34F305071886A1783C1ED0348D5F01E79DA6F1EB44FEFADA03147DE9
        7B0EE4ED30946713406D01BEF75CBDF11F607171112CCBE21E54D2DFAC9DAF2E
        49A4688078EE19B86F103093007D4F2C3FB13DB68F038AC5A2C2DCC7AB30E739
        C0E7F3D15472CA0ABC7FE4F5E9BF05C715A1D07188BEAB848FF59FBF90E08065
        34D18F269AA6B90A500FDC4EC52C95DC70382CB6452202F6B66259E3001C54D8
        8F743ACD4D5C2F229108C8B20C922485D6053013D79E80794329ADE5CAB0BB19
        0325FD1FA05452C0756B1EB0C6C6E1402000994C060D4E4173731364B3D95BB1
        58EC32CA94034A087011A0EBFAAA133000F326994C42341A85B9B9B9D1DEDEDE
        D3F97CDE6EFC16BA908AAFB20468E6AA358AA2082D2D2DB0B0B000E572796470
        70F0ECE4E464B9AED701FB51FCC44EC056D918EC3AE88F3B3F3F7F331E8F5FC1
        6D541BF57AB70F53868D839952C074D70A7F002AF04A8E29B489580000000049
        454E44AE426082}
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitHeight = 43
    end
    object sbDeleteSpec: TPngSpeedButton
      AlignWithMargins = True
      Left = 33
      Top = 3
      Width = 32
      Height = 29
      Hint = #1059#1076#1072#1083#1080#1090#1100
      Align = alLeft
      Flat = True
      OnClick = sbDeleteSpecClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
        0D0000000473424954080808087C086488000000097048597300000B1200000B
        1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
        65776F726B732043533571B5E336000003804944415478DACD944F6C54451CC7
        BF33F3DEDB7DDBA5348B85A5DD5AFB0FDBAE848558A38D012F280703C40BE1A2
        3151C28518AA41A4468B89D58493A40749058D87CA81C6A01863A307046CB445
        AB586C698A6D714B6DD7EED276DFEEFB3333CE839274578F983893F726BF99DF
        7CF2FBFDE63B43A494B8978DFC67C0AEDADA86CDBB769EAAA88EC5FA4F7EF8D1
        1F63D78F72D7052780498180F271D4C785F47FD0198D94D7D6764637343C79FD
        527FCF8BE9F4EB05C08BAFB48D6CD9B1FD41E3D65F485EBCE05EE83D776AFCC6
        CDFD94518456006D9723C0D8A38D894D1FAC8DC737DC9C9C9A1CFBBEFFB5B69C
        7DA60038D87E78FAA1CDF1F5C18539606A1CCED5217C7D7EE8B781B97C6B8946
        323E3027048DAE2A3D934824766728B586AF5C39E8E6ED6ED7B5F172CE2E4CF9
        7845054C5D3BF8F0C6FAF6A6B0B786252760CD66F0C34C3E75D9124D26416393
        AE9FADACAC8C0C5BB981D1E4F4E3A6613825A5A5B09616FE097CBFBE0ED65C0A
        9A6323A2E3F8C630DB7F9FE0BA630B8CE6F892C96186358D0D0AF171DAE3CF11
        42A11B06CC92927F079E68A8879D4E23E8B9C82E65A1CEA22AAAD3CF1BA5D8C4
        3CA9D295B8C4D8B145C843868241D9011F180EC35ABC85B66260D703F7233B3F
        0FEA79D03C0E8B2054C5E5405CF06628179D11FCC8E8E0A8202DBABF41CD6994
        C0D095A582386C3B85C08EA0A1D4A0E440084C89AA06217EAAE362CD82B2AF32
        FA45A381A7AA4354EB5D44D78C230F68F40E54AA4E547F9BBB85C037033A841A
        35605DC2E3233542967115D5778C9E9E05D9AB4BECDABD4EEB0D9B8C9E18B713
        6AD72F7485A03B8B811D0AC801DACCC564B310315D27F899D0895F25A909A9F5
        BC5A5492DCB3AF2E787A68DE9B383FE7D40454CA77DB5BAA5405C0A3868E72C1
        7B5AA5DC6B06186628F1BE7110574BD7343F3B81DB07130BD2779F8E065EFD74
        3ABF2FE3CA6EB6CC7CA318D8A1D1B5DB2265C92A3BAB714DE05C16EFA43C1CD1
        88EFA442C71D3FA5223CB25A1B5D4D49F9B71927E2D7DC6FEDC5C0F7CA4A5FD8
        B9FD89EE55532398FF3329FB526E3525E486EFEE5FDF94C3C197DF112651B735
        AC0D8F587C8F2D71D6F779C92DAA6167C8DCF1FC91B62FD7F30CAEF5F5F1CF2E
        FF1E550F40EA6E8D2C5740C9D1D7E7EDB19C91633182B48AABD39F7B265F043C
        A442DFB2ADB5FFB1AD2D2D5F9DFCA4676C7AF6597D19E67B988C4157872057CC
        8509A82F71DF3E601701EFF97BF8BF05FE0DBECDBAE86B24A614000000004945
        4E44AE426082}
      ExplicitLeft = 76
      ExplicitTop = 4
      ExplicitHeight = 41
    end
    object sbEditSpec: TPngSpeedButton
      AlignWithMargins = True
      Left = 69
      Top = 1
      Width = 26
      Height = 31
      Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100
      Margins.Left = 1
      Margins.Top = 1
      Margins.Right = 1
      Align = alLeft
      Flat = True
      Layout = blGlyphTop
      ParentShowHint = False
      ShowHint = True
      PngImage.Data = {
        89504E470D0A1A0A0000000D4948445200000014000000140803000000BA57ED
        3F0000000373424954080808DBE14FE00000012F504C5445FFFFFFFEFEFEFDFD
        FDFCFCFCFBFBFBFAFAFAF9F9F9F8F8F8F7F7F7F6F6F6F5F5F5FFF3E5FDF7C7F4
        F4F4F3F3F3FDF6C1FFF2E0FDF6C3F2F2F2F1F1F1F0F0F0EFEFEFF6EEE4F5EBDF
        F1E9E0F2E9DEEAEAEAE9E9E9F3E6D8F5E5D4F9E3B1F2E5BCF3E4AEE3E3E3EEE2
        D4FCE0C3F0E3B9E2E2E2DFDFDFF8E25CF7E25AF7DF62F7E159F5DF5AF6DD6BEE
        D8B0F5DC5DE8D9A7F5DC5EF6D3ADF0D2B3F2D679EFD2ABE4D5ABE5D5B1E5D4AE
        EED680EED47CF1D47AEED378F0D378F9C990EDD077F9C793CCCCCCCBCBCBEDCA
        6CE7CB76C9C9C9E4C954D7C2A4E7BF8AC2C2C2E7BD76C9BDAFD7B975D1B68FD5
        B579DBB084CBA76ED8A457D7A262E2A150D4A16ADD9C50D29E65D89953DD994A
        D09C45DC9748CC9558C49748C78D47BD894BC98440A98842BD7919AD6A258466
        2D88602A5942173C25EF5A000000097048597300000B1200000B1201D2DD7EFC
        0000001C74455874536F6674776172650041646F62652046697265776F726B73
        2043533571B5E336000000D54944415478DA6364C0021871092ACAB340798FEE
        FC83093A1CFFFB1F2CC6E2780F2C0A127439C0F99781819381E38591F43FAEAB
        972182FB5981241B1068BEBCA9A9B60C2AC80C24993965399F69BEFA64B0162A
        08365056F486B4183FCBC1574882323217557E70CBFFD8FE1F2168C27352E98F
        80EC31F5BD084163D12352FF44650FBC7244083AB3FFFFF94C58ECF0873F4882
        311BDDB87E331E78CD802C187F97419EF9E06320C71E2198F8FFD7A7936F408E
        7040B2080E1CF740FCEE78F42F5C8CD9EA0044505586192EF8E7E96D3C818C01
        004FE4561533982BD20000000049454E44AE426082}
      ExplicitLeft = 60
      ExplicitTop = 0
      ExplicitHeight = 43
    end
  end
  object dgData: TDBGridEh [3]
    Left = 0
    Top = 370
    Width = 651
    Height = 183
    Align = alClient
    DataSource = dsSpec
    DynProps = <>
    Flat = True
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    IndicatorOptions = [gioShowRowIndicatorEh]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghColumnResize, dghColumnMove]
    TabOrder = 3
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -13
    TitleParams.Font.Name = 'Verdana'
    TitleParams.Font.Style = [fsBold]
    TitleParams.ParentFont = False
    Columns = <
      item
        DynProps = <>
        EditButtons = <>
        Footers = <>
        Width = 148
      end
      item
        DynProps = <>
        EditButtons = <>
        Footers = <>
        Width = 141
      end
      item
        DynProps = <>
        EditButtons = <>
        Footers = <>
        Width = 138
      end
      item
        DynProps = <>
        EditButtons = <>
        Footers = <>
        Width = 140
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited dsLocal: TDataSource
    Left = 392
    Top = 400
  end
  inherited qrAux: TADOQuery
    Left = 400
    Top = 504
  end
  object drvSpec: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select s.*, o.cnum, o.kind_code,'
      
        '(select code from statekinds k where k.id = o.state_id) as state' +
        '_code,'
      
        '(select code from statekinds k where k.id = s.proper_state_id) a' +
        's proper_state_code'
      'from doccargospecs s, v_objects o '
      'where s.object_id = o.id and s.object_type = o.object_type'
      '')
    SelectCommand.Parameters = <>
    UpdateCommand.CommandText.Strings = (
      'update doccargospecs'
      'set'
      '  proper_state_id = :proper_state_id'
      'where'
      '  id = :id')
    UpdateCommand.Parameters = <
      item
        Name = 'proper_state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.CommandText.Strings = (
      'insert into doccargospecs'
      
        '  (dochead_id, object_type, typed_object_id, parentobject_id, pr' +
        'oper_state_id)'
      'values'
      
        '  (:dochead_id, :object_type, :typed_object_id, :parentobject_id' +
        ', :proper_state_id)')
    InsertCommand.Parameters = <
      item
        Name = 'dochead_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'object_type'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'typed_object_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'parentobject_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'proper_state_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'delete from doccargospecs where id = :id')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      'select s.*, o.cnum, o.kind_code,'
      
        '(select code from statekinds k where k.id = o.state_id) as state' +
        '_code,'
      
        '(select code from statekinds k where k.id = s.proper_state_id) a' +
        's proper_state_code'
      'from doccargospecs s, v_objects o '
      'where s.object_id = o.id and s.object_type = o.object_type'
      'and s.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 128
    Top = 464
  end
  object meSpec: TMemTableEh
    Params = <>
    DataDriver = drvSpec
    Left = 176
    Top = 464
  end
  object dsSpec: TDataSource
    DataSet = meSpec
    Left = 232
    Top = 464
  end
end
