﻿inherited FormLoads: TFormLoads
  ActiveControl = dgData
  Caption = #1055#1086#1075#1088#1091#1079#1082#1072
  ClientHeight = 656
  ClientWidth = 1384
  ExplicitWidth = 1400
  ExplicitHeight = 694
  PixelsPerInch = 96
  TextHeight = 13
  inherited SplitterVert: TSplitter
    Left = 153
    Height = 623
    Visible = False
    ExplicitLeft = 153
    ExplicitHeight = 623
  end
  inherited plBottom: TPanel
    Top = 623
    Width = 1384
    ExplicitTop = 623
    ExplicitWidth = 1384
    inherited btnOk: TButton
      Left = 1149
      ExplicitLeft = 1149
    end
    inherited btnCancel: TButton
      Left = 1268
      ExplicitLeft = 1268
    end
  end
  inherited plAll: TPanel
    Left = 160
    Width = 1224
    Height = 623
    ExplicitLeft = 160
    ExplicitWidth = 1224
    ExplicitHeight = 623
    inherited SplitterHor: TSplitter
      Top = 531
      Width = 1224
      Color = 15790320
      ParentColor = False
      ExplicitTop = 540
      ExplicitWidth = 778
    end
    inherited plTop: TPanel
      Width = 1224
      ExplicitWidth = 1224
      inherited sbDelete: TPngSpeedButton
        Left = 168
        ExplicitLeft = 174
      end
      inherited btFilter: TPngSpeedButton
        Left = 307
        ExplicitLeft = 315
      end
      inherited btExcel: TPngSpeedButton
        Left = 665
        ExplicitLeft = 675
      end
      inherited sbAdd: TPngSpeedButton
        Left = 68
        Visible = False
        ExplicitLeft = 68
      end
      inherited sbEdit: TPngSpeedButton
        Left = 100
        Width = 34
        ExplicitLeft = 102
        ExplicitWidth = 34
      end
      inherited Bevel2: TBevel
        Left = 270
        ExplicitLeft = 139
      end
      inherited btTool: TPngSpeedButton
        Left = 1187
        ExplicitLeft = 723
      end
      inherited Bevel1: TBevel
        Left = 375
        ExplicitLeft = 238
      end
      inherited btFlow: TPngSpeedButton
        Left = 485
        Width = 34
        ExplicitLeft = 495
        ExplicitWidth = 34
      end
      inherited sbHistory: TPngSpeedButton
        Left = 519
        Width = 34
        ExplicitLeft = 529
        ExplicitWidth = 34
      end
      inherited sbRestrictions: TPngSpeedButton
        Left = 592
        Width = 34
        ExplicitLeft = 602
        ExplicitWidth = 34
      end
      inherited sbConfirm: TPngSpeedButton
        Left = 378
        Width = 34
        ExplicitLeft = 388
        ExplicitWidth = 34
      end
      inherited sbCancelConfirm: TPngSpeedButton
        Left = 412
        Width = 34
        ExplicitLeft = 422
        ExplicitWidth = 34
      end
      inherited Bevel5: TBevel
        Left = 628
        ExplicitLeft = 492
      end
      inherited sbReport: TPngSpeedButton
        Left = 631
        Width = 34
        ExplicitLeft = 641
        ExplicitWidth = 34
      end
      inherited Bevel6: TBevel
        Left = 482
        ExplicitLeft = 330
      end
      inherited sbImport2: TPngSpeedButton
        Left = 736
        Width = 34
        Hint = #1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1080#1074#1085#1099#1081' '#1080#1085#1090#1077#1088#1092#1077#1081#1089' '#1087#1086#1075#1088#1091#1079#1082#1080
        Visible = False
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
          0D0000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000003844944415478DAA5947F4C955518C7
          3FE7BEF7051CC82FA3FCB5C0CCE51FA4539915BA2905B8369AD5729681BAC922
          FB81AE68CD6CE5E672CDCD287094547F44B605F8A33F8A9AB281E69C996542AC
          B5522C420DC74D2214BA3FDEBEF7BEEF254A716D9DEDF3DE739EFB9CEF79CE73
          9E730CD76A479D090CF1B07A0F8AB962AA70C43971024333161F516086355E20
          4AC4EB62C05C2576D059ADEFAB620AD76B43977AD9B8B88BB35D451A45753AC5
          9CBF059B1C8B4CDE521CE50C06A07537B1DF789B7D072CBCD7ED7FDDAA25CB46
          F8237081E09FEB64794F8445B61913D92E7D1F8BF52BF38739F34D2F91B02352
          B0137F26189CC7F30D36A7DAC2B4BCEDC3985A22914D58560DF84A09074F6AE6
          5DAEE0B2B56F52B0EA711614B9E24531F3DD6289581AC358FBF1FBEFD30ECE13
          1A2995ED90A8C067D551BCC6C7F1962E0217720D6D4E120DDBFA68DA3E91C6F3
          9038212E58302AE6F28478434C16FD6211C67788CA3A8B80E69D3830CCFB4753
          0D079C324E9F6C60FD7CA8FE1CB2A643E98CAB2384144FE87E710ABFDD41D19A
          0C9ED1F61BB6C8D20EAFB53F6228A9E8E4E35DB9E39CE5B7C216F7885EE5ED33
          1CE72C76421EB7CCB99DEA2309CA2FA3823BDA1B0D961D5242D7793989B79BC5
          6DE22191EF45D7257E8D8D532759D477D84C9AEA7AD76D84EF8E41EDB1D3C62B
          D868BEDAC789D2F28AB758AC1533D9FCA192B0D2FD371A5955815B52AFB48CFC
          17C1B12D4774B3BB1B6ECA712DDB56C1C44C585FAD4A0C5FF9FF829FD443FD73
          AEE89D25BF1B92D37E6368203D9A0971F1BA723EDF642C7F39FB021649C9AECD
          513C5F7EAAAB38A0EC2EDF63787AE78BD43EB595B41BC24C9B158C3975772630
          EDD61057860C97077D4C99E1DA93922D5654D9E42D1B6FC9470D353F24D273A4
          87BDD559A464B85B39DC04F3756B2EF5A1EA87DCC5F0CBF7C44AA46287EEF5C2
          6B899D23839C680E37ABE25F50ECFD0A3FFAC2445F8D8E318E519F4291AE2D4B
          DD64B3BDD562EED27FCA397AEA8ACD7EE3E56D937847940B1D175F796E97C5CB
          E2B898257E54223F60D1F2156CD9678F11AB91D886F8EA72A251BC24B68AD59E
          785CB0599C11D1C41DD6953B4861593ECFBEEBF37C6A15FF06DD22272E58E24D
          4AF0C6177592414DF48F46100AA6100E256B5284B4AC203BBF4854AE7F927715
          8566CFD89DC7DFC36CF124EED5EAC7B67D2C5999C7F4D9F348CD9C893F2195BE
          1E8B486890072ADB48BF71AF0EA0993C13FCF7C9FC056A241A36B05AB5FF0000
          000049454E44AE426082}
        ExplicitLeft = 748
        ExplicitWidth = 34
      end
      inherited sbSearch: TPngSpeedButton
        Left = 273
        Width = 34
        ExplicitLeft = 281
        ExplicitWidth = 34
      end
      inherited Bevel7: TBevel
        Left = 589
        ExplicitLeft = 632
      end
      inherited sbClearFilter: TPngSpeedButton
        Left = 339
        Width = 34
        ExplicitLeft = 349
        ExplicitWidth = 34
      end
      inherited Bevel3: TBevel
        Left = 699
        ExplicitLeft = 580
      end
      inherited sbClock: TPngSpeedButton
        Left = 702
        Width = 34
        ExplicitLeft = 714
        ExplicitWidth = 34
      end
      inherited sbBarrel: TPngSpeedButton
        Left = 446
        Width = 34
        Enabled = False
        Visible = False
        ExplicitLeft = 456
        ExplicitWidth = 34
      end
      object sbFilterPlan: TPngSpeedButton [23]
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 34
        Height = 26
        Hint = #1054#1090#1073#1086#1088' '#1087#1086' '#1087#1083#1072#1085#1091' '#1087#1086#1075#1088#1091#1079#1082#1080
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        AllowAllUp = True
        GroupIndex = 1
        ParentShowHint = False
        ShowHint = True
        Visible = False
        OnClick = sbFilterPlanClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C08648800000009704859730000004E000000
          4E01B1CD1F330000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000001FA4944415478DA636440020D0C0D4C
          B7CDDFF22C3D39F91303918011C6C8B5CD155510FEB74E41EC9FD2F5278C6B6A
          B64DCB27DA8042CB642105498E1D0196BF4D999919186E3D657ABFE71C4B64DB
          FE293B091A90669CC6A529C7BA3FC8EAB71948330CAC3BCA72FACA833FD6B3CE
          CEFA8DD7801A97BCDC28879F93F8B8FEA3487CFECEF86FF5619609F7DFFE9FC0
          C1CCA4C3CAC4F178D2B19E2B1806E4D995AA1ACB7FDDE968F04791891155F2E6
          63A637DFFF30328AF1FF17FEFC8DE1F3E5074C979FBC6049EF3936096E10584B
          A955AEB298F0FF690E7A7F6D2585FF72E272EEBD678C1FF65F646EA9DF3DAD17
          231640A0CE25375349F26F9EA3DE1F0DE4F0006B7EC1F4F9D065E69E9AED539B
          B046230C94DBE7E4C6B9FC9EC4CF8D08937BCF993EED3DC73CA571EFD46A9CE9
          0006DA7DB3E7C73AFE4E80F11FBC64FAF4418CFBEBED5BFF2E7F9CC9EF094C6C
          FFF01AD0E19B332FC6F157224CF3EE6BEC17420AB80C844499B816F57C9B5AB9
          A8BF00AF01558E7961C1363F177FFFC5F863CF4596FE861D531AEAC28A52CD9D
          58AA24E598F8372FF9915FBF6CC2629C0680C45A3C73B6FEF9CF700AA4196EB0
          7BB691A201FB6C7E616691F3FB7F78B66F9F7C0D97013841927A19AFA6CDEF15
          BF7EFE57B97189C568F1A59EAF24190073614348C1C4EF5FFF6B756E9FE8428E
          016050E19117F8FECBFF7B006B30C9507591C2870000000049454E44AE426082}
      end
      object sbAppendDoc: TPngSpeedButton [24]
        AlignWithMargins = True
        Left = 553
        Top = 0
        Width = 34
        Height = 26
        Hint = #1044#1086#1087#1086#1083#1085#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbAppendDocClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E3360000027F4944415478DAA5935B48145118C7
          FFA3CEACE5EEB86C45EA6ED243B9DECA92D290F59A1B5E588D02B7D407310A7A
          0D22F4A1881EACB71EA297A82809122A44744BBB78C32E2BEA265D75AD10532C
          5717DB5BEE99399D9D6C5F2A68E90C3F0E9C99F3E39CFFF70D4729C5FF0C2E12
          C1A1D6EA07C120A9BADBD8B512B1C07AB36A0FDBDC4B56C874C7F11E635870AC
          AB366C9025594122F2333637232A6A3F09923286314E5043AFD363C1BD00E7F4
          94E3D1C9C19D6141FDF606D0D0C34E233326BF4CE0F5CC2B1099C0684845ACA0
          020989A90489AD8DBD71C0F9716A68E8ECB0893BDA7998D6653101955705B222
          9977CFC1366A43F9EE0A18D765205EA585CBEFC2F8FC18064607E0FCE0B4DB5B
          1CB9DC91762B9594634B8892A3B175430A32F499E079016D7DB751676A8086D7
          C0EFF7634DEC5ACC2ECDE0C29D96F1E1F32FB37E0BD17ADDA22641A9292D29BD
          39C7980B87730CA6E4629A9A94DE9FA43314758FD8282F08DE92CCBD9A3F56A1
          E686451D5C214D5A689B4F549F82A88AC7E4A7099A66C8E8D7AFDF5474DFDE49
          F968C15B9A6DEE609FBF675CE3EADB0E52963242504291A04B44C9E67D484948
          85264E0D8FCF8345B71BF9E98578FA7650C927E00DB84B77996D2109676DADA2
          056945E184892CA134B91C9EE56F30B1F5BF8D872F7A5CE61C733777E06A052D
          D856C8364B6132B53BA022B110E344881A11BCC0638BCE8809D73BB05261F6F3
          1C827EF2D59C6BBEC4592E9B952BB0BB83751958B761A32661E45C4DCB78C01F
          C85B5C7625FA0301D162AA86DDF91C3EAF0F01CFF7A5B2BCB28B4A06FFD0CAB7
          7EC6CDD5B6F7DE93F818C15769AA1423F9174E3394DE7F627F5C6CCACEBF22C4
          0867221118188DAB12A5748C995F2F7F008C054FC934F774380000000049454E
          44AE426082}
        ExplicitLeft = 563
      end
      object sbAll: TPngSpeedButton [25]
        AlignWithMargins = True
        Left = 34
        Top = 0
        Width = 34
        Height = 26
        Hint = #1055#1086#1082#1072#1079#1072#1090#1100' '#1074#1089#1077' '#1087#1086#1075#1088#1091#1079#1082#1080' ('#1089' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1077#1084' '#1087#1086' '#1087#1077#1088#1080#1086#1076#1091')'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        AllowAllUp = True
        GroupIndex = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = sbAllClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000140000001404030000007FA700
          3E0000000373424954080808DBE14FE000000024504C5445DE8021E9AC6EF4D5
          B6E39343EEBF90FAEBDCDF8327ECB47CF1CAA3FDF6EFF7E0C9E0883021A979D0
          0000000C74524E53FFFFFFFFFFFFFFFFFF00FFFF814515AE0000000970485973
          00000AC300000AC301342924AB0000001C74455874536F667477617265004164
          6F62652046697265776F726B732043533571B5E3360000005D4944415478DA63
          9CC900038C4431FFCD4E87317F154E8530BF5774174ECE990662FEC8DFE6FB97
          652A58C1AEB50C8C33A06AA75FA8548030971F60605BFC01C4FC9B55DDA67F19
          ACEDAFF2ADC269C72DD0CC6560F8B9398468976130014B4C2B09A437A9450000
          000049454E44AE426082}
      end
      object sbInspection: TPngSpeedButton [26]
        AlignWithMargins = True
        Left = 200
        Top = 0
        Width = 34
        Height = 26
        Hint = #1055#1086#1089#1090#1072#1074#1080#1090#1100'/'#1089#1085#1103#1090#1100'  '#1086#1089#1084#1086#1090#1088#1099
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        Layout = blGlyphRight
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbInspectionClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C086488000000097048597300000B1200000B
          1201D2DD7EFC0000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000002C94944415478DA6D934B4C13511486
          CF3803AD16DAF2D6C69080424423625B09261AD8181FF1B14164E302129085BA
          10E323808921C415C6B85253A2C6958961011B4490A242213C95441303891A01
          05FAA0740AEDCCBDD7338F164427397333F7CEF79F73EE3987638C41FCE1381E
          DF26340B9A51D9D14F949F2268CB6821608CC491B88006A7869B2F5691D2C2AB
          CCB42D0D6449E68002E5A800C190971FF8E432BE1878264CFF9E4511695D4087
          C53B17AAB9F263B70DBBF32C7C02EE10197D533CA740A204C23FA68176B95D96
          EBED4D78BAA844C221AE796EACA866E74A6F1AF715A6F0FC3700691E8022AC38
          9225607C1A7EE6803839ECE53AFA5B931F7C7021E75304CCE2DDCA4B70B6ACC1
          98BFC7A2C2D15984F18460AA54D28CAC01081940201F563E8F2E739DEF5A2CAD
          438F1581ECA0BBD9637296D8F82DDF3558B922A68332A641658C36029C8C2209
          5940B83CF00F76CDA59F7F755811C80B0CDD9BB21E2832C0EA20C24C8389A481
          348AA02602348242618CF9242C8DF44532CEBCDCAF08E407FA9BC6AD079D2658
          1BD02E6E23A847C21441867B980A438185E15E31ABBCD3AE0AF87B6F8DA538EC
          49107AAF56E72F307E07B84FA2B8A2A59E825F9E9ED0F6CA370E35059FFBDA54
          6A91C300FE6EAD65621E7598A351BDA4B8CA22406605CC7BBA233B2ADD6A0AD9
          DE8E2A8FB5B8CCC64B1F81ADCE20A4F5C03A188B420466C805B2D5090BFDED73
          B69A49F512CDCBF587EAD889E286E4BD7633BF3A0C207ED5BA3306633454F16C
          CA05C970140253EE20FF7ABC25DDB5F428DE4881CB05B5F4B8A3DE5C58922284
          B11AA12FEA08306C2282E563C65D40928E4070B4CF073D13F7339F079E688DB4
          A195FDB53935ECB4F386B9C06E11041941499D1B8AA5956506C19931A06F47DA
          6C0FFD8DEBADBC69987C753BAB23F6AC2B909498C624C494FAF34C602BA23771
          62B1CDDA157A9AF853DE344CFF8EB315CDF09F710EEAE32CC7903F176BA3F800
          71B3860000000049454E44AE426082}
        ExplicitLeft = 208
      end
      object sbWagon: TPngSpeedButton [27]
        AlignWithMargins = True
        Left = 234
        Top = 0
        Width = 34
        Height = 26
        Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100' '#1074#1072#1075#1086#1085
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alLeft
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        OnClick = sbWagonClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          610000000473424954080808087C08648800000009704859730000004E000000
          4E01B1CD1F330000001C74455874536F6674776172650041646F626520466972
          65776F726B732043533571B5E336000002964944415478DA63FCFFFF3F032580
          515694EB9A102FDBBE8BF73EE4800454657882248439EBB8D958FE56C6689F61
          60F8CFD0B6E49AC9D71F7F985FBDFFD178EBC9E7F52075FA4A0253DE7DF9EDC8
          28CECFF59EE11F0383381BCF1990C48B1F5F746CD4C599589999509CF6FBEF3F
          C623375FFE93E0E0B902E2BFFCF3C58481F13F03A3B7BCF2C3024373C95B5F3E
          FE26C5E96A3CFCAC13CE9F7C0E36405A5787C1D4DF47EEE8D1A30C5FBE7C6108
          0A0A6298376F1E83A5A52558F1F1E3C71992929218D6AD5BC7C0C3C3C3606D6D
          CDB07AE6ECD7ACAFDF7E071B909696FE4ED1D7C360C3860D0C1F3F7E64282828
          60A8AAAA62F0F5F5051BB079F36686B6B6368609132630F0F3F33304040430D4
          6466BFF8FBE4D92FB0016D75F5CF194D0DCCC9368059468AAD65FA5489813340
          405C8CE70327FBDFA7AF5EB2BD78FB9A5D535DE9EBBB771F598585F9C131F3F6
          ED47562121FEDFD76FDEE3961016FD292D26FE4BE0FB4F66FE6F3F5F820DD8E2
          152277E0F9E33FBB5F3EFFACCF27C03FFDC1C977F31A8C45142579C02EB8F7EC
          0B434AE3D9B7190AE682173F7DF8E8262EC56B2F29C3B2E1FEADC36003025575
          8416FDF9FD4741555BE0F299A3EF2AC42478D7FD38FB7145A39508C880F0BA63
          6F82388CF9BBDFBEFCAC6D642574FFF6958F896CECCC860282C719C539B99F33
          7370B2D9B87BB1BDB8BE9351DE34866BEFEA791F7EFFFBC168A927FC0E9C0E2E
          BD156261E2F8EF129A24F0F0F4926F129AEEFF0FEFDCF6EBDFCFEF3F19410A84
          452517FA06C7C73CBF73F23D9F8426CF9EEDAB1BDFBF7DDD8E9CF2048545AB5D
          BDC26B3F3EBFFA4542C95470CBFA458BDEBE7E91C80895E71111935ACCCDCDA3
          044C8967DEBE7E960614FB8B967A998545A5660353A2F1F7AF5FEEBE7AF52C16
          28F69591D2EC0C0084EB53DA59EB79510000000049454E44AE426082}
        ExplicitLeft = 242
      end
      inherited sbPass: TPngSpeedButton
        Left = 134
        Width = 34
        ExplicitLeft = 140
        ExplicitWidth = 34
      end
      inherited plCount: TPanel
        Left = 1023
        ExplicitLeft = 1023
        inherited edCount: TDBEditEh
          ControlLabel.ExplicitLeft = 16
          ControlLabel.ExplicitTop = -16
          ControlLabel.ExplicitWidth = 103
        end
      end
    end
    inherited dgData: TDBGridEh
      Width = 1224
      Height = 468
      DataGrouping.Active = True
      DataGrouping.DefaultStateExpanded = True
      DataGrouping.Font.Height = -13
      DataGrouping.Font.Style = [fsBold]
      DataGrouping.GroupLevels = <
        item
          Color = clWindow
          ColumnName = 'Column_1_path_code'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 3947580
          Font.Height = -16
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end>
      DataGrouping.ParentFont = False
      FrozenCols = 3
      GridLineParams.BrightColor = clCream
      GridLineParams.DataHorzColor = 10809582
      GridLineParams.DataVertColor = clSilver
      ReadOnly = False
      RowHeight = 22
      SortLocal = False
      STFilter.HorzLineColor = 12566463
      TitleParams.MultiTitle = True
      OnColEnter = dgDataColEnter
      OnDblClick = dgDataDblClick
      OnEnter = dgDataColEnter
      OnTitleBtnClick = nil
      Columns = <
        item
          Alignment = taCenter
          CellButtons = <>
          CellDataIsLink = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'fulfilled'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = '*'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -21
          Title.Font.Name = 'Calibri'
          Title.Font.Style = [fsBold]
          Title.Hint = #1044#1074#1086#1081#1085#1086#1081' '#1082#1083#1080#1082' '#1084#1099#1096#1082#1086#1081', '#1095#1090#1086#1073#1099' '#1087#1086#1089#1090#1072#1074#1080#1090#1100'/'#1089#1085#1103#1090#1100' '#1086#1090#1084#1077#1090#1082#1091
          ToolTips = False
          Width = 24
          OnHintShowPause = dgDataColumns0HintShowPause
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'path_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = ':'
          Visible = False
          WordWrap = False
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'state_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1057#1090#1072#1090#1091#1089
          Width = 46
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'mark'
          Footers = <>
          Title.Caption = #1054#1090#1084#1077#1090#1082#1072
          Width = 64
          WordWrap = False
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'need_inspection'
          Footers = <>
          Title.Caption = #1058#1088#1077#1073'. '#1086#1089#1084'.'
          Width = 43
          OnHintShowPause = dgDataColumns4HintShowPause
        end
        item
          CellButtons = <>
          Color = 5234414
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_note'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084'. '#1082' '#1074#1072#1075#1086#1085#1091
          Visible = False
          Width = 98
          WordWrap = False
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_date'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1044#1072#1090#1072' '#1076#1086#1082'.'
          Width = 72
          WordWrap = False
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'docexists'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1057#1086#1079#1076'. '#1091#1074#1077#1076'.'
          Width = 43
          WordWrap = False
        end
        item
          CellButtons = <>
          Color = 15593971
          DynProps = <>
          EditButtons = <>
          FieldName = 'container_state'
          Footers = <>
          Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
          Width = 69
          WordWrap = False
        end
        item
          CellButtons = <>
          Color = 14408149
          DynProps = <>
          EditButtons = <>
          FieldName = 'station_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1057#1090#1072#1085#1094#1080#1103
          Width = 104
          WordWrap = False
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num_source'
          Footers = <>
          Title.Caption = #1050#1055' '#1087#1088#1080#1073'.'
          Width = 64
          WordWrap = False
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'train_num'
          Footers = <>
          Title.Caption = #1050#1055' '#1086#1090#1087#1088'.'
          Width = 67
          WordWrap = False
        end
        item
          CellButtons = <>
          Color = 15655112
          DynProps = <>
          EditButtons = <>
          FieldName = 'pnum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Footers = <>
          ReadOnly = True
          Title.Caption = #1042#1072#1075#1086#1085
          Width = 78
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'carkind_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1060#1091#1090'.'
          Width = 48
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'car_owner_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
          Width = 108
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'model_code'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1052#1086#1076#1077#1083#1100
          Width = 93
          WordWrap = False
        end
        item
          CellButtons = <>
          CellDataIsLink = True
          Color = 5761158
          DynProps = <>
          EditButtons = <>
          FieldName = 'cnum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Footers = <>
          ReadOnly = True
          Title.Caption = #1050#1086#1085#1090#1077#1081#1085#1077#1088
          Width = 91
          WordWrap = False
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          Color = 6545406
          DynProps = <>
          EditButtons = <>
          FieldName = 'weight_fact'
          Footers = <>
          Title.Caption = #1042#1077#1089', '#1082#1075'.'
          Width = 47
        end
        item
          Alignment = taCenter
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'seal_number'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = [fsItalic]
          Footers = <>
          ReadOnly = True
          Title.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
          Width = 78
        end
        item
          CellButtons = <>
          Checkboxes = True
          DynProps = <>
          EditButtons = <>
          FieldName = 'isfreeload'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1057#1074#1086#1073'. '#1087#1086#1075#1088'.'
          Width = 35
          WordWrap = False
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'isempty'
          Footers = <>
          Title.Caption = #1055#1086#1088'- '#1086#1078#1085'.?'
          Width = 46
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'kind_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1058#1080#1087' '#1050#1058#1050
          Width = 39
          WordWrap = False
        end
        item
          AutoFitColWidth = False
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'note'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
          Width = 123
          WordWrap = False
        end
        item
          Alignment = taCenter
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'amount_on_car'
          Footers = <>
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1050#1086#1083'. '#1082#1090#1082'/'#1074#1072#1075'.'
          WordWrap = False
        end
        item
          AutoFitColWidth = False
          ButtonStyle = cbsNone
          CellButtons = <>
          DynProps = <>
          EditButton.Visible = False
          EditButton.OnClick = dgDataColumns15EditButtonClick
          EditButtons = <>
          FieldName = 'dispatch_numbers'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1086#1090#1087#1088#1072#1074#1082#1080
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Calibri'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 83
          WordWrap = False
          OnEditButtonClick = dgDataColumns15EditButtonClick
        end
        item
          ButtonStyle = cbsEllipsis
          CellButtons = <>
          DynProps = <>
          EditButton.Style = ebsEllipsisEh
          EditButton.Visible = True
          EditButton.OnClick = dgDataColumns15EditButtonClick
          EditButtons = <>
          FieldName = 'receipt_number'
          Footers = <>
          Title.Caption = #1053#1086#1084#1077#1088' '#1082#1074#1080#1090#1072#1085#1094#1080#1080
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Calibri'
          Title.Font.Style = [fsBold, fsUnderline]
          Visible = False
          Width = 68
          WordWrap = False
          OnEditButtonClick = dgDataColumns15EditButtonClick
        end
        item
          ButtonStyle = cbsNone
          CellButtons = <>
          DisplayFormat = '### ##0.00'
          DynProps = <>
          EditButton.Style = ebsEllipsisEh
          EditButton.Visible = False
          EditButton.OnClick = dgDataColumns15EditButtonClick
          EditButtons = <>
          FieldName = 'receipt_summa'
          Footers = <>
          Title.Caption = #1057#1091#1084#1084#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Calibri'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 70
          WordWrap = False
          OnEditButtonClick = dgDataColumns15EditButtonClick
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'mtu_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1052#1058#1059
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Calibri'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 82
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'picture_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1056#1080#1089#1091#1085#1086#1082
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Calibri'
          Title.Font.Style = [fsBold, fsUnderline]
          Width = 89
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'owner_code'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
          Width = 100
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          EditMask = 'forw'
          FieldName = 'forwarder_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          Width = 116
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'consignee_code'
          Footers = <>
          Title.Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Width = 108
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'docorder_text'
          Footers = <>
          Title.Caption = #1047#1072#1103#1074#1082#1072
          Width = 114
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'doc_number'
          Footers = <>
          MaxWidth = 60
          ReadOnly = True
          TextEditing = False
          Title.Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082'.'
          Width = 60
          WordWrap = False
        end
        item
          CellButtons = <>
          DynProps = <>
          EditButtons = <>
          FieldName = 'task_id'
          Footers = <>
          ReadOnly = True
          WordWrap = False
        end>
    end
    inherited plHint: TPanel
      Height = 81
      ExplicitHeight = 81
      inherited lbText: TLabel
        Width = 185
        Height = 73
      end
    end
    inherited plStatus: TPanel
      Width = 1224
      ExplicitWidth = 1224
    end
    inherited plLinkedObjects: TPanel
      Top = 541
      Width = 1218
      Height = 79
      ExplicitTop = 541
      ExplicitWidth = 1218
      ExplicitHeight = 79
      inherited dgLinkedObjects: TDBGridEh
        Width = 1218
        Height = 79
        Font.Height = -12
      end
    end
    inherited plSearch: TPanel
      Width = 1224
      ExplicitWidth = 1224
      inherited Label1: TLabel
        Height = 14
      end
      inherited edSearch: TEdit
        Width = 1042
        ExplicitWidth = 1042
      end
    end
  end
  inherited plLeft: TPanel
    Width = 153
    Height = 623
    Visible = False
    ExplicitWidth = 153
    ExplicitHeight = 623
    inherited Bevel4: TBevel
      Left = 152
      Height = 598
      ExplicitLeft = 152
      ExplicitHeight = 590
    end
    inherited dgFolders: TDBGridEh
      Width = 152
      Height = 598
    end
    inherited plTool: TPanel
      Width = 153
      ExplicitWidth = 153
      inherited sbWrap: TSpeedButton
        Left = 153
        Width = 0
        ExplicitLeft = 153
        ExplicitWidth = 0
      end
      inherited cbAll: TCheckBox
        Width = 147
        ExplicitWidth = 147
      end
    end
  end
  inherited drvData: TADODataDriverEh
    SelectCommand.CommandText.Strings = (
      
        'declare @userid int, @planid int, @doctypeid int, @datebegin dat' +
        'etime, @dateend datetime, @isconfirmed int, @folder_id int, @sho' +
        'wall bit;'
      ''
      
        'select @userid = :userid, @planid = :planid, @folder_id = :folde' +
        'r_id, @showall = :showall;'
      ''
      
        'select @doctypeid = min(id) from doctypes where system_section =' +
        ' '#39'outcome_loads'#39';'
      ''
      
        'select @datebegin = datebegin, @dateend  = dateend, @isconfirmed' +
        '  = isconfirmed '
      'from dbo.f_GetFilterParams(@userid,@doctypeid);'
      ''
      
        'select d.id as id, k.id as joint_id, d.doc_date, d.doc_number, d' +
        '.doctype_id, k.folder_id, d.isconfirmed, d.date_confirm, d.isdel' +
        'eted, d.isabolished, d.inwait, d.datestopinwait,  '
      
        'l.fulfilled, l.receipt_number, l.receipt_summa, l.mtukind_id, l.' +
        'picturekind_id,'
      
        'l.order_num, l.container_id, l.task_id, l.orderspec_id, l.isfree' +
        'load, l.kind_id, ck.footsize, l.owner_id, l.station_id, l.note, ' +
        'l.dispatch_numbers, l.temp_move_sign, '
      
        '(case when cg.seal_number like '#39'{%'#39' then '#39#39' else cg.seal_number ' +
        'end) as seal_number, b1.isempty, cg.weight_fact, '
      
        'v.cnum, k.load_plan_id, ph.code as path_code, k.order_on_path, k' +
        '.mark, k.note as car_note,'
      
        '(case when DATEDIFF(M, getdate(), cr.date_next_repair)<2 or isnu' +
        'll(cr.note,'#39#39')<>'#39#39' then 1 else 0 end) as urgent_repair_sign,'
      
        '(select code from counteragents ct, objects o1 where o1.id = k.c' +
        'arriage_id and o1.owner_id = ct.id) as car_owner_code,'
      
        '(select count(*) from docload s1 where s1.joint_id = k.id and is' +
        'null(s1.container_id,0)<>0) as amount_on_car,'
      
        'cr.id as carriage_id, cr.pnum as pnum, cr.need_inspection, dbo.g' +
        'rtint(isnull(cr.isdefective,0), isnull(tc.isdefective,0)) as isd' +
        'efective,'
      
        '(select code from carriagekinds kr where kr.id = cr.kind_id) as ' +
        'carkind_code,'
      
        '(select model_code from carriagemodels m where m.id = cr.model_i' +
        'd) as model_code,'
      
        '(select name from stations st where st.id = l.station_id) as sta' +
        'tion_code,'
      'k.train_num, k.train_num_source, '
      
        '(select count(*) where exists (select 1 from f_GetLinkedDispatch' +
        '(d.id))) as docexists,'
      
        '(case when d.isconfirmed = 1 then '#39' +'#39' else '#39#39' end) as state_cod' +
        'e,'
      'los1.object_state_id,'
      
        '(select sk1.code from objectstatekinds sk1 where  sk1.id = los1.' +
        'object_state_id) container_state,'
      
        '(case when l.isfreeload=1 then (select code from counteragents c' +
        'r where cr.id = l.owner_id) else (select code from counteragents' +
        ' cr where cr.id = v.owner_id) end) as owner_code,'
      
        '(case when l.isfreeload=1 then (select code from containerkinds ' +
        'k where k.id = l.kind_id) else (select code from containerkinds ' +
        'k where k.id = v.kind_id) end) as kind_code,'
      
        '(select name from mtukinds m where m.id = l.mtukind_id) as mtu_c' +
        'ode,'
      
        '(select name from picturekinds p where p.id = l.picturekind_id) ' +
        'as picture_code,'
      'b1.docorder_text, '
      
        '(select isnull(code, name) from counteragents c4 where c4.id = b' +
        '1.consignee_id) as consignee_code,'
      
        '(select isnull(code, name) from counteragents c5 where c5.id = b' +
        '1.forwarder_id) as forwarder_code'
      'from docloadjoint k'
      'left outer join paths ph on (ph.id = k.path_id)'
      'left outer join docload l on (k.id = l.joint_id)'
      'left outer join containerkinds ck on (ck.id = l.kind_id)'
      'left outer join '
      '('
      ' select s.id, s.isempty,'
      
        ' d.doc_number+'#39' '#1086#1090' '#39'+convert(varchar(10), d.doc_date, 104)+'#39'/'#39'+c' +
        'onvert(varchar(10),isnull(s.amount,1)) as docorder_text,'
      ' o.consignee_id, o.forwarder_id'
      ' from documents d, docorder o, docorderspec s '
      ' where s.doc_id = d.id and o.id = d.id'
      ') as b1 on (b1.id = l.orderspec_id)'
      'left outer join documents d on  (l.id = d.id )'
      
        'left outer join (select c1.id, c1.cnum, c1.kind_id, o1.owner_id ' +
        'from containers c1, objects o1 where c1.id = o1.id) v on (v.id =' +
        ' l.container_id)'
      
        'left outer join v_lastobjectstates los1 on (los1.object_id = l.c' +
        'ontainer_id and los1.task_id = l.task_id)'
      'left outer join tasks t on (t.id = l.task_id)'
      'left outer join cargos cg on (cg.id = t.object_id),'
      'carriages cr '
      'left outer join techconditions tc on (cr.techcond_id = tc.id)'
      'where cr.id = k.carriage_id'
      'and isnull(k.isdeleted,0) = 0 '
      'and'
      '('
      '   (@showall = 0) or '
      
        '  ((@datebegin is null or d.doc_date >=@datebegin) and (@dateend' +
        ' is null or  d.doc_date <@dateend+1))'
      ')'
      'and'
      '('
      '   (@showall = 1) or '
      '   --isnull(los1.object_state_id,0) <> 12'
      '  k.isactive = 1'
      ')'
      'and'
      '('
      
        '  not exists (select 1 from dbo.f_GetFilterList(@userid, @doctyp' +
        'eid) fl1)'
      
        '  or exists (select 1 from dbo.f_GetFilterList(@userid, @doctype' +
        'id) fl where charindex(fl.v, cr.pnum)>0) '
      ')'
      'order by path_code, order_on_path, pnum, l.order_num')
    SelectCommand.Parameters = <
      item
        Name = 'userid'
        DataType = ftInteger
        Size = -1
        Value = 1
      end
      item
        Name = 'planid'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'folder_id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'showall'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    UpdateCommand.CommandText.Strings = (
      'declare @allowedit int, @id int;'
      ''
      'select @allowedit = :allowedit;'
      ''
      'if @allowedit =1 SET CONTEXT_INFO 0x001'
      'else SET CONTEXT_INFO 0x0;'
      ''
      'select @id = :id;'
      ''
      'update docload'
      'set'
      '  container_id = :container_id,'
      '  mtukind_id = :mtukind_id,'
      '  picturekind_id = :picturekind_id,'
      '  owner_id = :owner_id,'
      '  dispatch_numbers = :dispatch_numbers,'
      '  receipt_number = :receipt_number,'
      '  receipt_summa =   :receipt_summa,'
      '  fulfilled  = :fulfilled '
      'where'
      '  id = @id;'
      ''
      
        'update carriages set need_inspection = :need_inspection where id' +
        ' = (select j.carriage_id from docloadjoint j, docload l where l.' +
        'joint_id = j.id and l.id = @id)'
      ''
      ''
      'SET CONTEXT_INFO 0x0;')
    UpdateCommand.Parameters = <
      item
        Name = 'allowedit'
        Size = -1
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'container_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'mtukind_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'picturekind_id'
        Size = -1
        Value = Null
      end
      item
        Name = 'owner_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dispatch_numbers'
        Size = -1
        Value = Null
      end
      item
        Name = 'receipt_number'
        Size = -1
        Value = Null
      end
      item
        Name = 'receipt_summa'
        Size = -1
        Value = Null
      end
      item
        Name = 'fulfilled'
        Size = -1
        Value = Null
      end
      item
        Name = 'need_inspection'
        Size = -1
        Value = Null
      end>
    DeleteCommand.CommandText.Strings = (
      'declare @id int, @jointid int;'
      'select @id = :id;'
      
        'select @jointid = isnull(joint_id,0) from docload where id = @id' +
        ';'
      'if @jointid>0'
      
        'delete from documents where id = @id and isnull(isconfirmed,0)=0' +
        ';')
    DeleteCommand.Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    GetrecCommand.CommandText.Strings = (
      
        'select d.id as id, k.id as joint_id, d.doc_date, d.doc_number, d' +
        '.doctype_id, k.folder_id, d.isconfirmed, d.date_confirm, d.isdel' +
        'eted, d.isabolished, d.inwait, d.datestopinwait,  '
      
        'l.fulfilled, l.receipt_number, l.receipt_summa, l.mtukind_id, l.' +
        'picturekind_id,'
      
        'l.order_num, l.container_id, l.task_id, l.orderspec_id, l.isfree' +
        'load, l.kind_id, ck.footsize, l.owner_id, l.station_id, l.note, ' +
        'l.dispatch_numbers, l.temp_move_sign, '
      
        '(case when cg.seal_number like '#39'{%'#39' then '#39#39' else cg.seal_number ' +
        'end) as seal_number, b1.isempty, cg.weight_fact, '
      
        'v.cnum, k.load_plan_id, ph.code as path_code, k.order_on_path, k' +
        '.mark, k.note as car_note,'
      
        '(case when DATEDIFF(M, getdate(), cr.date_next_repair)<2 or isnu' +
        'll(cr.note,'#39#39')<>'#39#39' then 1 else 0 end) as urgent_repair_sign,'
      
        '(select code from counteragents ct, objects o1 where o1.id = k.c' +
        'arriage_id and o1.owner_id = ct.id) as car_owner_code,'
      
        '(select count(*) from docload s1 where s1.joint_id = k.id and is' +
        'null(s1.container_id,0)<>0) as amount_on_car,'
      
        'cr.id as carriage_id, cr.pnum as pnum, cr.need_inspection, dbo.g' +
        'rtint(isnull(cr.isdefective,0), isnull(tc.isdefective,0)) as isd' +
        'efective,'
      
        '(select code from carriagekinds kr where kr.id = cr.kind_id) as ' +
        'carkind_code,'
      
        '(select model_code from carriagemodels m where m.id = cr.model_i' +
        'd) as model_code,'
      
        '(select name from stations st where st.id = l.station_id) as sta' +
        'tion_code,'
      'k.train_num, k.train_num_source, '
      
        '(select count(*) where exists (select 1 from f_GetLinkedDispatch' +
        '(d.id))) as docexists,'
      
        '(case when d.isconfirmed = 1 then '#39' +'#39' else '#39#39' end) as state_cod' +
        'e,'
      'los1.object_state_id,'
      
        '(select sk1.code from objectstatekinds sk1 where  sk1.id = los1.' +
        'object_state_id) container_state,'
      
        '(case when l.isfreeload=1 then (select code from counteragents c' +
        'r where cr.id = l.owner_id) else (select code from counteragents' +
        ' cr where cr.id = v.owner_id) end) as owner_code,'
      
        '(case when l.isfreeload=1 then (select code from containerkinds ' +
        'k where k.id = l.kind_id) else (select code from containerkinds ' +
        'k where k.id = v.kind_id) end) as kind_code,'
      
        '(select name from mtukinds m where m.id = l.mtukind_id) as mtu_c' +
        'ode,'
      
        '(select name from picturekinds p where p.id = l.picturekind_id) ' +
        'as picture_code,'
      'b1.docorder_text, '
      
        '(select isnull(code, name) from counteragents c4 where c4.id = b' +
        '1.consignee_id) as consignee_code,'
      
        '(select isnull(code, name) from counteragents c5 where c5.id = b' +
        '1.forwarder_id) as forwarder_code'
      'from docloadjoint k'
      'left outer join paths ph on (ph.id = k.path_id)'
      'left outer join docload l on (k.id = l.joint_id)'
      'left outer join containerkinds ck on (ck.id = l.kind_id)'
      'left outer join '
      '('
      ' select s.id, s.isempty,'
      
        ' d.doc_number+'#39' '#1086#1090' '#39'+convert(varchar(10), d.doc_date, 104)+'#39'/'#39'+c' +
        'onvert(varchar(10),isnull(s.amount,1)) as docorder_text,'
      ' o.consignee_id, o.forwarder_id'
      ' from documents d, docorder o, docorderspec s '
      ' where s.doc_id = d.id and o.id = d.id'
      ') as b1 on (b1.id = l.orderspec_id)'
      'left outer join documents d on  (l.id = d.id )'
      
        'left outer join (select c1.id, c1.cnum, c1.kind_id, o1.owner_id ' +
        'from containers c1, objects o1 where c1.id = o1.id) v on (v.id =' +
        ' l.container_id)'
      
        'left outer join v_lastobjectstates los1 on (los1.object_id = l.c' +
        'ontainer_id and los1.task_id = l.task_id)'
      'left outer join tasks t on (t.id = l.task_id)'
      'left outer join cargos cg on (cg.id = t.object_id),'
      'carriages cr '
      'left outer join techconditions tc on (cr.techcond_id = tc.id)'
      'where cr.id = k.carriage_id'
      'and d.id = :current_id'
      '')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
  end
  inherited meData: TMemTableEh
    AfterOpen = meDataAfterOpen
  end
  inherited drvForms: TADODataDriverEh
    Top = 202
  end
  inherited meForms: TMemTableEh
    Top = 202
  end
  inherited dsForms: TDataSource
    Top = 202
  end
  inherited pmDocFlow: TPopupMenu
    object N24: TMenuItem [0]
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1103#1095#1077#1081#1082#1080
      ShortCut = 46
      OnClick = N24Click
    end
    object N23: TMenuItem [1]
      Caption = '-'
    end
    inherited MenuItem1: TMenuItem
      Visible = False
    end
    inherited MenuItem3: TMenuItem [3]
    end
    inherited MenuItem2: TMenuItem [4]
    end
    object N15: TMenuItem [5]
      Caption = '-'
    end
    object N21: TMenuItem [6]
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1046#1044' '#1087#1091#1090#1100' ('#1087#1086' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1084')'
      OnClick = N21Click
    end
    object N22: TMenuItem [7]
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1052#1058#1059' '#1080' '#1056#1080#1089#1091#1085#1086#1082' ('#1087#1086' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1084')'
      OnClick = N22Click
    end
    object N25: TMenuItem [8]
      Caption = #1055#1088#1086#1089#1090#1072#1074#1080#1090#1100' '#1086#1089#1084#1086#1090#1088' ('#1087#1086' '#1086#1090#1084#1077#1095#1077#1085#1085#1099#1084')'
    end
    object N16: TMenuItem [9]
      Caption = '-'
    end
    object N17: TMenuItem [10]
      Caption = #1055#1086#1084#1077#1085#1103#1090#1100' '#1074#1072#1075#1086#1085#1099
      OnClick = N17Click
    end
    object N26: TMenuItem [11]
      Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1079#1072#1103#1074#1082#1091
      OnClick = N26Click
    end
    object N20: TMenuItem [16]
      Caption = #1057#1074#1077#1088#1085#1091#1090#1100' '#1074#1089#1077
      ShortCut = 16471
      OnClick = N20Click
    end
  end
  inherited al: TActionList
    inherited aAdd: TAction
      Enabled = False
    end
  end
  inherited drvLinkedObjects: TADODataDriverEh
    Left = 313
    Top = 522
  end
  inherited qrReport: TADOQuery
    Left = 688
    Top = 200
  end
  inherited IL: TPngImageList
    Bitmap = {}
  end
  object drvCarriages: TADODataDriverEh
    ADOConnection = dm.connMain
    DynaSQLParams.Options = []
    KeyFields = 'id'
    MacroVars.Macros = <>
    SelectCommand.CommandText.Strings = (
      'select c.*, o.start_datetime,  o.id as object_id, '
      
        'isnull(c.train_num, (select train_num from doccargosheet sh wher' +
        'e sh.id = (select max(id) from doccargosheet sh1 where sh1.carri' +
        'age_num = c.pnum))) as train_num,'
      
        '(select convert(varchar, pl.plan_date, 104)+'#39', '#39'+isnull(pl.train' +
        '_num,'#39#39')+'#39', '#39'+ isnull((select code from dispatchkinds k where k.' +
        'id = pl.dispatch_kind_id),'#39#39') from loadplan pl where pl.id = c.c' +
        'urrent_load_id) as load_plan_text,'
      
        '(select code from counteragents c1 where c1.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from carriagekinds k where k.id = c.kind_id) as kin' +
        'd_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select model_code from carriagemodels m where m.id = c.model_id' +
        ') as model_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code'
      
        'from objects o, carriages c where o.id = c.id and o.object_type ' +
        '= '#39'carriage'#39
      'and c.id = :carriage_id')
    SelectCommand.Parameters = <
      item
        Name = 'carriage_id'
        Size = -1
        Value = Null
      end>
    UpdateCommand.CommandText.Strings = (
      'update carriages'
      'set'
      '  pnum = :pnum,'
      '  kind_id = :kind_id,'
      '  techcond_id = :techcond_id,'
      '  model_id = :model_id,'
      '  note = :note,'
      '  need_inspection = :need_inspection,'
      '  isdefective = :isdefective'
      'where'
      '  id = :id'
      '')
    UpdateCommand.Parameters = <
      item
        Name = 'pnum'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'kind_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'techcond_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'model_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'note'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 450
        Value = Null
      end
      item
        Name = 'need_inspection'
        Size = -1
        Value = Null
      end
      item
        Name = 'isdefective'
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    InsertCommand.Parameters = <>
    DeleteCommand.Parameters = <>
    GetrecCommand.CommandText.Strings = (
      'select c.*, o.start_datetime,  o.id as object_id, '
      
        'isnull(c.train_num, (select train_num from doccargosheet sh wher' +
        'e sh.id = (select max(id) from doccargosheet sh1 where sh1.carri' +
        'age_num = c.pnum))) as train_num,'
      
        '(select convert(varchar, pl.plan_date, 104)+'#39', '#39'+isnull(pl.train' +
        '_num,'#39#39')+'#39', '#39'+ isnull((select code from dispatchkinds k where k.' +
        'id = pl.dispatch_kind_id),'#39#39') from loadplan pl where pl.id = c.c' +
        'urrent_load_id) as load_plan_text,'
      
        '(select code from counteragents c1 where c1.id = o.owner_id) as ' +
        'owner_code,'
      
        '(select code from carriagekinds k where k.id = c.kind_id) as kin' +
        'd_code,'
      
        '(select code from objectstatekinds s where s.id = o.state_id) as' +
        ' state_code,'
      
        '(select model_code from carriagemodels m where m.id = c.model_id' +
        ') as model_code,'
      
        '(select code from techconditions t where t.id = c.techcond_id) a' +
        's techcond_code'
      
        'from objects o, carriages c where o.id = c.id and o.object_type ' +
        '= '#39'carriage'#39
      'and o.id = :current_id')
    GetrecCommand.Parameters = <
      item
        Name = 'current_id'
        Size = -1
        Value = Null
      end>
    Left = 216
    Top = 296
  end
  object meCarriages: TMemTableEh
    FetchAllOnOpen = True
    Params = <>
    DataDriver = drvCarriages
    Left = 288
    Top = 296
  end
end
