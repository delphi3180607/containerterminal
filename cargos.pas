﻿unit cargos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, objects, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Data.DB, Data.Win.ADODB,
  MemTableEh, DataDriverEh, ADODataDriverEh, Vcl.Menus, Vcl.ExtCtrls,
  Vcl.Buttons, PngSpeedButton, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, EXLReportExcelTLB, EXLReportBand,
  EXLReport, Vcl.Mask, DBCtrlsEh;

type
  TFormCargos = class(TFormObjects)
    Panel4: TPanel;
    dgTasks: TDBGridEh;
    drvTasks: TADODataDriverEh;
    meTasks: TMemTableEh;
    dsTasks: TDataSource;
    procedure meDataAfterScroll(DataSet: TDataSet);
    procedure N5Click(Sender: TObject);
    procedure dgDataEnter(Sender: TObject);
    procedure dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
  private
    procedure AssignSelField(colname: string); override;
  public
    procedure Init; override;
  end;

var
  FormCargos: TFormCargos;

implementation

{$R *.dfm}

uses editcargo;


procedure TFormCargos.AssignSelField(colname: string);
begin

  selfield := '';
  if colname = 'issealwaste' then selfield := 'issealwaste';

end;


procedure TFormCargos.dgDataDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
begin

  if Column.FieldName = 'type_code' then
    if meData.FieldByName('type_code').AsString = '{порожний}' then
      dgData.Canvas.Brush.Color := $00E2FBE5;

  inherited;

   dgData.DefaultDrawColumnCell(Rect, DataCol, Column, State);

end;

procedure TFormCargos.dgDataEnter(Sender: TObject);
begin
  inherited;
  self.meDataAfterScroll(meData);
end;

procedure TFormCargos.Init;
begin
  inherited;
  self.formEdit := FormEditCargo;
  self.meDataAfterScroll(meData);
end;


procedure TFormCargos.meDataAfterScroll(DataSet: TDataSet);
begin
  if not meData.Active then exit;
  inherited;
  drvTasks.SelectCommand.Parameters.ParamByName('object_id').Value := meData.FieldByName('id').AsInteger;
  meTasks.Close;
  meTasks.Open;
end;

procedure TFormCargos.N5Click(Sender: TObject);
begin
  inherited;
  self.meDataAfterScroll(meData);
end;

end.
