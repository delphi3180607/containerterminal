﻿inherited FormMoveOrderSpec: TFormMoveOrderSpec
  BorderStyle = bsSizeToolWin
  Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1086#1074' '#1080#1079' '#1079#1072#1103#1074#1082#1080
  ClientHeight = 341
  ClientWidth = 893
  ExplicitWidth = 909
  PixelsPerInch = 96
  TextHeight = 16
  inherited plBottom: TPanel
    Top = 300
    Width = 893
    ExplicitTop = 300
    ExplicitWidth = 893
    inherited btnCancel: TButton
      Left = 777
      OnClick = btnCancelClick
      ExplicitLeft = 777
    end
    inherited btnOk: TButton
      Left = 658
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
      OnClick = btnOkClick
      ExplicitLeft = 658
    end
  end
  object dgData: TDBGridEh [1]
    Left = 0
    Top = 60
    Width = 893
    Height = 240
    Align = alClient
    AllowedOperations = []
    Border.Color = clSilver
    Ctl3D = False
    DataSource = dsData
    DynProps = <>
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    GridLineParams.DataHorzColor = 15263453
    GridLineParams.DataVertColor = 15263453
    GridLineParams.VertEmptySpaceStyle = dessNonEh
    IndicatorParams.HorzLineColor = 15131609
    IndicatorParams.VertLineColor = 15131609
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
    ParentCtl3D = False
    ParentFont = False
    SelectionDrawParams.SelectionStyle = gsdsClassicEh
    SelectionDrawParams.DrawFocusFrame = False
    SelectionDrawParams.DrawFocusFrameStored = True
    STFilter.HorzLineColor = 15263453
    STFilter.VertLineColor = 15263453
    TabOrder = 1
    TitleParams.FillStyle = cfstThemedEh
    TitleParams.Font.Charset = DEFAULT_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -11
    TitleParams.Font.Name = 'Verdana'
    TitleParams.Font.Style = [fsBold]
    TitleParams.HorzLineColor = 15131609
    TitleParams.MultiTitle = True
    TitleParams.ParentFont = False
    TitleParams.VertLineColor = 15131609
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'container_num'
        Footers = <>
        ReadOnly = True
        Title.Caption = #8470' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1072
        Width = 110
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'kind_code'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1058#1080#1087
        Width = 48
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'owner_code'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
        Width = 115
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'amount'
        Footers = <>
        Title.Caption = #1050#1086#1083'-'#1074#1086
        Width = 53
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'used_amount'
        Footers = <>
        Title.Caption = #1055#1086#1075#1088#1091#1078#1077#1085#1086
        Width = 80
      end
      item
        CellButtons = <>
        Checkboxes = True
        DynProps = <>
        EditButtons = <>
        FieldName = 'isempty'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1088#1086#1078#1085#1080#1081'?'
        Width = 77
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'seal_number'
        Footers = <>
        Title.Caption = #1053#1086#1084#1077#1088' '#1087#1083#1086#1084#1073#1099
        Width = 112
      end
      item
        Alignment = taCenter
        AutoFitColWidth = False
        CellButtons = <>
        Checkboxes = False
        DynProps = <>
        EditButtons = <>
        FieldName = 'issealwaste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        Footers = <>
        ReadOnly = True
        Title.Caption = #1043#1086#1076'- '#1085#1072#1103'?'
        Width = 49
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'weight_container'
        Footers = <>
        Title.Caption = #1042#1077#1089' '#1085#1077#1090#1090#1086
        Visible = False
        Width = 79
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'weight_cargo'
        Footers = <>
        Title.Caption = #1042#1077#1089' '#1073#1088#1091#1090#1090#1086
        Width = 81
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'external_order_num'
        Footers = <>
        Title.Caption = #1053#1086#1084#1077#1088' '#1043#1059'-12'
        Width = 100
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'state_code'
        Footers = <>
        Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
        Width = 100
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 0
    Width = 893
    Height = 30
    Align = alTop
    BevelOuter = bvLowered
    Caption = 
      #1042#1099#1073#1077#1088#1080#1090#1077' '#1085#1086#1074#1091#1102' '#1079#1072#1103#1074#1082#1091', '#1085#1077' '#1079#1072#1082#1088#1099#1074#1072#1103' '#1101#1090#1086' '#1086#1082#1085#1086', '#1080' '#1079#1072#1090#1077#1084' '#1085#1072#1078#1084#1080#1090#1077' '#1042#1099#1087 +
      #1086#1083#1085#1080#1090#1100'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    StyleElements = [seClient, seBorder]
  end
  object Panel2: TPanel [3]
    Left = 0
    Top = 30
    Width = 893
    Height = 30
    Align = alTop
    BevelOuter = bvLowered
    Caption = #1042#1099' '#1087#1077#1088#1077#1085#1086#1089#1080#1090#1077' '#1082#1086#1085#1090#1077#1081#1085#1077#1088#1099' '#1074' '#1076#1088#1091#1075#1091#1102' '#1079#1072#1103#1074#1082#1091
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    StyleElements = [seClient, seBorder]
    ExplicitTop = 8
  end
  inherited dsLocal: TDataSource
    Left = 136
    Top = 288
  end
  inherited qrAux: TADOQuery
    Left = 80
    Top = 296
  end
  object meData: TMemTableEh
    Params = <>
    Left = 384
    Top = 160
  end
  object dsData: TDataSource
    DataSet = meData
    Left = 448
    Top = 160
  end
end
