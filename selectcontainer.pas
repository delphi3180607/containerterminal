﻿unit SelectContainer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, edit, Vcl.StdCtrls, Vcl.Mask, DBCtrlsEh,
  Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFormSelectContainer = class(TFormEdit)
    edContainerNum: TDBEditEh;
    lbMark: TLabel;
    t1: TTimer;
    lbExplain: TLabel;
    procedure edContainerNumChange(Sender: TObject);
    procedure t1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSelectContainer: TFormSelectContainer;

implementation

{$R *.dfm}

procedure TFormSelectContainer.edContainerNumChange(Sender: TObject);
begin
  lbMark.Caption := 'Ищу...';
  t1.Enabled := false;
  t1.Enabled := true;
  btnOk.Enabled := false;
end;

procedure TFormSelectContainer.t1Timer(Sender: TObject);
begin

  t1.Enabled := false;

  qrAux.Close;
  qrAux.Parameters.ParamByName('cnum').Value := trim(edContainerNum.Text);
  qrAux.Open;

  if qrAux.RecordCount>0 then
  begin
    lbMark.Caption := qrAux.FieldByName('cnum').AsString;
    btnOk.Enabled := true;
  end else
  begin
    lbMark.Caption := 'Не найден';
    btnOk.Enabled := false;
  end;

end;

end.
