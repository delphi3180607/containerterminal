﻿inherited FormEditContract: TFormEditContract
  Caption = #1050#1086#1085#1090#1088#1072#1082#1090
  ClientHeight = 260
  ExplicitWidth = 543
  ExplicitHeight = 288
  PixelsPerInch = 96
  TextHeight = 16
  object Label3: TLabel [0]
    Left = 8
    Top = 6
    Width = 47
    Height = 16
    Caption = #1050#1083#1080#1077#1085#1090
  end
  object sbOwner: TSpeedButton [1]
    Left = 460
    Top = 27
    Width = 52
    Height = 24
    Caption = '...'
    OnClick = sbOwnerClick
  end
  object Label1: TLabel [2]
    Left = 8
    Top = 74
    Width = 86
    Height = 16
    Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
  end
  object Label2: TLabel [3]
    Left = 152
    Top = 74
    Width = 110
    Height = 16
    Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
  end
  object Label4: TLabel [4]
    Left = 8
    Top = 146
    Width = 42
    Height = 16
    Caption = #1053#1086#1084#1077#1088
  end
  inherited plBottom: TPanel
    Top = 219
    TabOrder = 4
    ExplicitTop = 219
  end
  object luCustomer: TDBLookupComboboxEh [6]
    Left = 8
    Top = 28
    Width = 449
    Height = 24
    DynProps = <>
    DataField = 'customer_id'
    DataSource = FormContracts.dsData
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    KeyField = 'id'
    ListField = 'code'
    ListSource = FormCounteragents.dsData
    ParentFont = False
    TabOrder = 0
    Visible = True
  end
  object dtBegin: TDBDateTimeEditEh [7]
    Left = 8
    Top = 96
    Width = 121
    Height = 24
    DataField = 'cdate'
    DataSource = FormContracts.dsData
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 1
    Visible = True
  end
  object dtEnd: TDBDateTimeEditEh [8]
    Left = 152
    Top = 96
    Width = 121
    Height = 24
    DataField = 'end_date'
    DataSource = FormContracts.dsData
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    Kind = dtkDateEh
    ParentFont = False
    TabOrder = 2
    Visible = True
  end
  object edNumber: TDBEditEh [9]
    Left = 8
    Top = 168
    Width = 265
    Height = 24
    DataField = 'cnum'
    DataSource = FormContracts.dsData
    DynProps = <>
    EditButtons = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    Visible = True
  end
end
